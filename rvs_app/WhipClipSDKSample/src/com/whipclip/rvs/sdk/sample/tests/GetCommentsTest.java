package com.whipclip.rvs.sdk.sample.tests;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;

import org.jdeferred.DoneCallback;
import org.slf4j.Logger;

import java.util.List;

/**
 * Created by iliya on 7/23/14.
 */
public class GetCommentsTest {

    Logger logger = org.slf4j.LoggerFactory.getLogger(GetCommentsTest.class);

    private RVSSdk sdk;
    RVSPromise trendingPromise;

    public void getCommnetsForPost(RVSSdk sdk) {

        RVSAsyncList comments = sdk.commentsForPost("90f8837ba6044e04968019c4ce2f1bb5");
        RVSPromise commentsPromise = comments.next(5);
        commentsPromise.then(new DoneCallback<List<RVSComment>>() {

            @Override
            public void onDone(List<RVSComment> result) {
                logger.debug("comments results: " + result);

            }
        });

    }

    public void getComments(final RVSSdk sdk)
    {

        final RVSAsyncList trending = sdk.trendingPosts();
        trendingPromise = trending.next(5);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);


                for (RVSPost rvsPost : result) {

//                if (result.size() > 0) {
//                    RVSPost rvsPost = result.get(0);
                    logger.info("post id: " + rvsPost.getPostId());

                    RVSAsyncList comments = sdk.commentsForPost(rvsPost.getPostId());
                    RVSPromise commentsPromise = comments.next(5);
                    commentsPromise.then(new DoneCallback<List<RVSComment>>() {

                        @Override
                        public void onDone(List<RVSComment> result) {
                            logger.debug("comments results: " + result);

                        }
                    });
                }
            }
        });
    }

}
