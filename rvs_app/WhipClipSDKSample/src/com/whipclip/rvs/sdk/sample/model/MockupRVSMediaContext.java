package com.whipclip.rvs.sdk.sample.model;

import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSMediaContextImp;

/**
 * Created by iliya on 8/1/14.
 *
 * DON"T EXTEND RVSMediaContextImp !!!!!!!!
 * DON"T EXTEND RVSMediaContextImp !!!!!!!!
 * DON"T EXTEND RVSMediaContextImp !!!!!!!!
 */
public class MockupRVSMediaContext extends RVSMediaContextImp {

    private long duration = 65;

    @Override
    public String clipUrlWithMediaType(String mediaType, long startOffset, long duration) {

//        http://www.playon.tv/online/iphone5/main.m3u8
//        http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8 (Nasa TV)
//        http://mediamotiononline.ios.internapcdn.net/mediamotiononline/inapcms/CMS16042/flash/16042_adaptive2.mp4.m3u8
//        http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8
//        http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8
//        http://devimages.apple.com/iphone/samples/bipbop/gear1/prog_index.m3u8
//       return "http://www.playon.tv/online/iphone5/main.m3u8";
        return "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";
    }

    @Override
    public long getDuration() {
        return duration;
    }

}
