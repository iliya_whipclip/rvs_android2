package com.whipclip.rvs.sdk.sample.tests;

/**
 * Created by iliya on 8/4/14.
 */
public class BaseTest {

    protected OnCompletionListener onCompletionListener;

    public interface OnCompletionListener {

        public void onCompletion(Object object);
    }

    public void setOnCompletionListener(OnCompletionListener l)
    {
        onCompletionListener = l;
    }
}
