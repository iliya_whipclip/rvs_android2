package com.whipclip.rvs.sdk.sample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSErrorCode;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.util.Crypto;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by iliya on 6/29/14.
 */
public class AuthenticatedSdk {

    Logger logger = LoggerFactory.getLogger(AuthenticatedSdk.class);

    private RVSSdk sdk;
    private Handler handler;

    @SuppressWarnings("unchecked")
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            logger.info("Got message: " + action);

            if (action.equals(RVSServiceManager.RVSServiceSignInErrorNotification)) {
                String error = intent.getStringExtra("error");
                logger.info("sign in error:" + error);
            } else if (action.equals(RVSServiceManager.RVSServiceSignedInNotification)) {

                logger.info("signed in");

                RVSPromise followPromise = sdk.followUser("barakh");
                followPromise.then(new DoneCallback() {
                    @Override
                    public void onDone(Object result) {
                        logger.info("following barakh");
                    }
                }).fail(new FailCallback<Throwable>() {
                    @Override
                    public void onFail(Throwable error) {
                        String errorCode = "";
                        if (error instanceof RVSError) {
                            errorCode = ((RVSError) error).getErrorCode();
                        }
                        logger.info("follow error:" + error.getMessage() + ", errorCode: " + errorCode);
                    }
                });

                handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RVSPromise followPromise = sdk.followUser("barakh");
                        followPromise.then(new DoneCallback() {
                            @Override
                            public void onDone(Object result) {
                                logger.info("following barakh");
                            }
                        }).fail(new FailCallback<Throwable>() {
                            @Override
                            public void onFail(Throwable error) {
                                String errorCode = "";
                                if (error instanceof RVSError) {
                                    errorCode = ((RVSError) error).getErrorCode();
                                }
                                logger.info("follow error:" + error.getMessage() + ", errorCode: " + errorCode);
                            }
                        });
                    }
                }, 180 * 1000);
            }
        }
    };

    public AuthenticatedSdk(RVSSdk sdk) {

        this.sdk = sdk;

        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignInErrorNotification));
    }

    public void testSignInWithUserName() {

        String pwd = "nWY8NfzeAc7T5XFEpdJI";
        String password = "5137";
        String token = null;
        String  pwdDecrypted = null;
        try {
            token = Crypto.encrypt(pwd, password);
            pwdDecrypted = Crypto.decrypt(pwd, token);
        } catch (Exception e) {
            e.printStackTrace();
        };

        logger.info("pwdDecrypted " + pwdDecrypted);



        // test sing in with user name & password
       sdk.signInWithUserName("ost", "5137");
    }

    public void testFacebook(RVSSdk sdk) {

        this.sdk = sdk;

       // test sing in with facebook
        sdk.signInWithFacebookToken("enter facebook token");
    }

    public void testTwitter(RVSSdk sdk) {

        this.sdk = sdk;

        // test sing in with user name & password
        sdk.signInWithTwitterToken("2554349400-6IQCjb7f0L7dyTtX8cJw59BMwHNhdRwHpnyEQBW", "WZnXTJvI2E90Y7xkRRlnCQ2QTDq9Addzfprluzwp3OnQn");

    }

}
