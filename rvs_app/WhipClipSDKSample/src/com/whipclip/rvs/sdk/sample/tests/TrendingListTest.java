package com.whipclip.rvs.sdk.sample.tests;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaPlayer;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;

import org.jdeferred.DoneCallback;
import org.slf4j.Logger;

import java.util.List;

/**
 * Created by iliya on 7/11/14.
 */
public class TrendingListTest extends BaseTest {

    Logger logger = org.slf4j.LoggerFactory.getLogger(TrendingListTest.class);

    private RVSSdk sdk;
    RVSAsyncList trendingList;
    RVSPromise trendingPromise;


    public TrendingListTest(RVSSdk sdk) {
        this.sdk = sdk;
    }

    public void trendingPosts(int count)
    {
        trendingList = sdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        trendingPromise = trending.next(count);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion(result);
                } else {
                    RVSPost rvsPost = result.get(0);
                    RVSProgram program = rvsPost.getProgram();
                    if (program != null) {

                        RVSShow show = program.getShow();
                        logger.info("show title: " + show.getName());
                        if (rvsPost.getProgram().getShow().getName() == null) {
                            logger.info("show name is null");
                        }

                        for (RVSPost post : result) {

                            if (post.getLatestComments() != null) {
                                List<RVSComment> latestComments = post.getLatestComments();
                                logger.info("latest comment not null");
                            }
                        }
                    } else {

                        logger.info("program is null");
                    }
                }


//                trendingPromise = trending.next(5);
//                trendingPromise.then(new DoneCallback<List<RVSPost>>() {
//
//                    @Override
//                    public void onDone(List<RVSPost> result) {
//                        logger.debug("onDone 1" + result);
//
//                    }
//                });
            }
        });
    }


    public void trendingPostsEndOflistTest(int count)
    {
        trendingList = sdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        trendingPromise = trending.next(count);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                if (!trendingList.getDidReachEnd()) {

                    trendingPromise = trending.next(2);
                    trendingPromise.then(new DoneCallback<List<RVSPost>>() {
                        @Override
                        public void onDone(List<RVSPost> result) {

                            logger.debug("onDone 2 " + result);

                        }
                    });
                }
            }
        });
    }


    public void getImageWithSize()
    {
        final RVSAsyncList trending = sdk.trendingPosts();
        trendingPromise = trending.next(1);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion(result);
                } else {
                    RVSPost rvsPost = result.get(0);
                    RVSAsyncImage coverImage = rvsPost.getCoverImage();

                    String imageUrlForSize = coverImage.getImageUrlForSize(new Point(480, 320));
                    RVSPromise imagePromise = coverImage.getImageWithSize(new Point(480, 320));

                    imagePromise.then(new DoneCallback<Bitmap>() {

                        @Override
                        public void onDone(Bitmap result) {
                            logger.debug("image downloaded");

                        }
                    });
                }
            }
        });
    }



}
