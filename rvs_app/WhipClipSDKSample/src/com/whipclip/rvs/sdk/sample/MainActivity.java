package com.whipclip.rvs.sdk.sample;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.sample.tests.GetImageWithSizeTest;

import org.slf4j.Logger;

/**
 * Created by iliya on 6/17/14.
 */
public class MainActivity extends Activity {

    private Logger logger  = org.slf4j.LoggerFactory.getLogger(MainActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        Context context = getApplicationContext();
        try {
            Log.d("BB", "" + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RVSSdk sdk = RVSSdk.sharedRVSSdk();
        ImageView image = (ImageView)findViewById(R.id.image);
//        new GetImageWithSizeTest().getImageWithSizeTest(sdk, image);
    }


}
