package com.whipclip.rvs.sdk.sample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnail;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnailList;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;

import org.jdeferred.DoneCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by iliya on 7/29/14.
 */
public class CreatePostTest {

    Logger logger = LoggerFactory.getLogger(CreatePostTest.class);

    private static String RVSTestUserId         = "_test";
    private static String RVSTestUserPassword   = "kalisher30";

    RVSSdk rvsSdk;

    public CreatePostTest(RVSSdk sdk) {

        this.rvsSdk = sdk;
        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignInErrorNotification));

    }

    @SuppressWarnings("unchecked")
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            logger.info("Got message: " + action);

            if (action.equals(RVSServiceManager.RVSServiceSignInErrorNotification)) {
                String error = intent.getStringExtra("error");
                logger.info("sign in error:" + error);
            } else if (action.equals(RVSServiceManager.RVSServiceSignedInNotification)) {

                logger.info("signed in");
                posting();
            }
        }
    };

    public void createPost() {

        // login first
        if (!rvsSdk.isSignedOut()) {
            rvsSdk.signOut();
        }

        rvsSdk.signInWithUserName(RVSTestUserId, RVSTestUserPassword);
//        } else {
//            posting();;
//        }
//

    }

    @SuppressWarnings("unchecked")
    public void posting() {

        final RVSChannel[] testChannel = new RVSChannel[1];
        final RVSPost testPost;
        final RVSMediaContext[] testMediaContext = {null};
        final RVSPromise[] mediaContextForChannel = new RVSPromise[1];

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

//      channel_id =   "us_cnnhd"
        final String testText = String.format("test %s", dateFormat.format(date));
        rvsSdk.channelForCallSign("CNNHD").then(new AndroidDoneCallback<List<RVSChannelSearchResult>>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(List<RVSChannelSearchResult> result) {

                if (result.size() > 0) {
                    testChannel[0] = result.get(0).getChannel();
                    mediaContextForChannel[0] = rvsSdk.getMediaContextForChannel(testChannel[0].getChannelId());
                    mediaContextForChannel[0].then(new AndroidDoneCallback<RVSMediaContext>() {
                        @Override
                        public AndroidExecutionScope getExecutionScope() {
                            return AndroidExecutionScope.UI;
                        }

                        @Override
                        public void onDone(RVSMediaContext result) {
                            testMediaContext[0] = result;
                            RVSPromise rvsPromise = testMediaContext[0].thumbnailsWithStartOffset(0, testMediaContext[0].getDuration());
                            rvsPromise.then(new AndroidDoneCallback<RVSThumbnailList>() {
                                @Override
                                public void onDone(RVSThumbnailList result) {
                                    String thumbnailId = result.getImages().get(0).getThumbnailId();

                                        RVSPromise postWithTextPromise = rvsSdk.createPostWithText(testText, testMediaContext[0], 0, testMediaContext[0].getDuration(), thumbnailId);
                                    postWithTextPromise.then(new DoneCallback<RVSPost>() {
                                        @Override
                                        public void onDone(RVSPost result) {
                                            logger.info("post created");
                                        }
                                    });
                                }

                                @Override
                                public AndroidExecutionScope getExecutionScope() {
                                    return AndroidExecutionScope.UI;
                                }
                            });
                        }
                    });
                }
            }
        });
        //        }, nil).thenOnMain(^id(NSArray *thumbnails) {
//        }, nil).thenOnMain(^id(NSObject<RVSPost>* post) {
//            testPost = post;
//            return [post.mediaContext clipUrlWithMediaType:nil startOffset:0 duration:post.mediaContext.duration];
//        }, nil).thenOnMain(^id(NSURL *contentUrl) {
//            NSLog(@"content url:%@", contentUrl);
//            player = [[MPMoviePlayerController alloc] initWithContentURL:contentUrl];
//            return nil;
//        }, nil);

    }
}
