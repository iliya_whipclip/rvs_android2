package com.whipclip.rvs.sdk.sample;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;
import com.whipclip.rvs.sdk.managers.RVSServiceNetworkStatus;
import com.whipclip.rvs.sdk.sample.model.Model;
import com.whipclip.rvs.sdk.sample.tests.BaseTest;
import com.whipclip.rvs.sdk.sample.tests.GetCommentsTest;
import com.whipclip.rvs.sdk.sample.tests.PostsTest;
import com.whipclip.rvs.sdk.sample.tests.SearchTest;
import com.whipclip.rvs.sdk.sample.tests.TrendingListTest;
import com.whipclip.rvs.sdk.sample.tests.UserTest;
import com.whipclip.rvs.sdk.util.ConfigureLog4J;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by iliya on 6/17/14.
 */
public class SampleApplication extends Application implements RVSServiceManager.NetworkConnectivityChanged {

    Logger logger = LoggerFactory.getLogger(SampleApplication.class);

    private String[] credentials = {"IliyaG", "5137"};
    private TrendingListTest trendingListTest;
    private UserTest userTest;


    RVSSdk sdk;

    @SuppressWarnings("unchecked")
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            logger.info("Got message: " + action);

            if (action.equals(RVSServiceManager.RVSServiceSignInErrorNotification)) {
                String error = intent.getStringExtra("error");
                logger.info("sign in error:" + error);
            } else if (action.equals(RVSServiceManager.RVSServiceSignedInNotification)) {

                logger.info("signed in");

                // test required authorization

//                userTest.notificationsForUser(credentials[0]);
//                userTest.notificationsCountForUser(credentials[0]);
//                userTest.newsFeedCountForUser(credentials[0]);

                /**
                // [+] delete post test
                test.setOnCompletionListener(new BaseTest.OnCompletionListener() {

                    @Override
                    public void onCompletion(Object object) {
                    RVSPost post = (RVSPost)object;
                    test.deletePost(post.getPostId());
                    }
                });
                 test.createPost();
                // [-] delete post test
                /**/


                /**
                // [+] createRepostForPost & repostingUsersForPost test
                final String postId = "404c8000ab9c4f94ba5b4f369f81397c";
                test.setOnCompletionListener(new BaseTest.OnCompletionListener() {

                    @Override
                    public void onCompletion(Object object) {
                        test.repostingUsersForPost(postId);
                    }
                });
                boolean deleteRepost = false;
                test.createRepostForPost(postId, deleteRepost);
                // [-] createRepostForPost & repostingUsersForPost
                /**/


                final PostsTest test = new PostsTest(sdk);
//              test.createCommentWithMessage("Iliya test comment 2");
//              test.postForPostId();
//              test.createLikeForPost("404c8000ab9c4f94ba5b4f369f81397c");
//              test.deleteLikeForPost("404c8000ab9c4f94ba5b4f369f81397c");
//              test.deleteCommentTest();
//              test.deleteRepostForPost("404c8000ab9c4f94ba5b4f369f81397c");


                /**
                // [+] video playback test
                trendingListTest.trendingPosts(15);
                trendingListTest.setOnCompletionListener(new BaseTest.OnCompletionListener() {
                    @Override
                    public void onCompletion(Object obj) {

                        List<RVSPost> posts = (List<RVSPost>) obj;
                        if (posts.size() > 2) {
                            RVSPost post = posts.get(0);
                            checkVideoPlayer(post);
                        }
                    }
                });
                // [-] video playback test
                /**/

            }
        }
    };

    @SuppressWarnings("unchecked")
    public void onCreate() {


        ConfigureLog4J.configure();
        Globals.setModel(new Model());
//        setNetworkConnectivityChangedListener

        sdk = RVSSdk.createRVSCore(this);

        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
        LocalBroadcastManager.getInstance(RVSSdk.getContext()).registerReceiver(mMessageReceiver, new IntentFilter(RVSServiceManager.RVSServiceSignInErrorNotification));

       
//        sdk.setNetworkConnectivityChangedListener(this);

         trendingListTest = new TrendingListTest(sdk);
//        trendingListTest.trendingPosts();
//        trendingListTest.trendingPostsEndOflistTest(3);
//        trendingListTest.getImageWithSize();

        // test
        userTest = new UserTest(sdk);
        userTest.userForUserId("barakH");
//        userTest.signInWithUserName(credentials[0], credentials[1]);
//        userTest.followersOfUser("_test");
//        userTest.feedForUser("_test1", true);
//        newsFeedForUser
//        userTest.feedForUser("_test1", false);
        // userFeedForUser



        PostsTest test = new PostsTest(sdk);
//        test.sharingUrlForPost();
//        test.likingUsersForPost();

//        AuthenticatedSdk authTest = new AuthenticatedSdk(sdk);
//        authTest.testSignInWithUserName(sdk);
//        authTest.testTwitter(sdk);
//        authTest.testFacebook(sdk);

        GetCommentsTest getCommentsTest = new GetCommentsTest();
//        getCommentsTest.getComments(sdk);
//        getCommentsTest.getCommnetsForPost(sdk);

        SearchTest searchTest = new SearchTest(sdk);
//        searchTest.searchUserTest();
//        searchTest.searchContent();
//        searchTest.searchChannels();
//        searchTest.searchSuggestions("barak");



//        testVideoPlaybackOfCreatedPost();






//        // check video playback
//        trendingListTest.trendingPosts(15);
//        trendingListTest.setOnCompletionListener(new TrendingListTest.OnCompletionListener() {
//            @Override
//            public void onCompletion(List<RVSPost> posts) {
//
////                for (RVSPost post : posts) {
////
////                    if (post.getMediaContext() != null) {
//                checkVideoPlayer(posts);
//            }
//        });

    }

    private void testVideoPlaybackOfCreatedPost() {
        PostsTest createPostTest = new PostsTest(sdk);
        createPostTest.setOnCompletionListener(new BaseTest.OnCompletionListener() {
            @Override
            public void onCompletion(Object object) {
                RVSPost post = (RVSPost)object;

                checkVideoPlayer(post);


            }
        });
        createPostTest.createPost();
    }

    @Override
    public void onNetworkConnectivityChanged(RVSServiceNetworkStatus networkStatus) {
        logger.info("network status chagged " + networkStatus);
    }

    private void checkVideoPlayer(RVSPost post) {

        Globals.getModel().mediaContext = post.getMediaContext();

        Intent intent = new Intent(SampleApplication.this, PlayerTestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }
}
