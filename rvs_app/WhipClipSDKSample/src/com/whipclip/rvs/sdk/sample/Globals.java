package com.whipclip.rvs.sdk.sample;

import android.content.Context;

import com.whipclip.rvs.sdk.sample.model.Model;

/**
 * Created by iliya on 8/1/14.
 */
public class Globals
{
    private static Context context;
    private static Model model;

    public static void setContext(Context context)
    {
        if(Globals.context == null)
        {
            Globals.context = context;
        }
    }

    public static Context getContext()
    {
        return context;
    }

    public static void setModel(Model model)
    {
        if(Globals.model == null)
        {
            Globals.model = model;
        }
    }

    public static Model getModel()
    {
        return model;
    }

}
