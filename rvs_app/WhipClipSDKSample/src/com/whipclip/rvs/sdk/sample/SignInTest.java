package com.whipclip.rvs.sdk.sample;

import com.whipclip.rvs.sdk.RVSSdk;

/**
 * Created by iliya on 6/26/14.
 */
public class SignInTest {

    private RVSSdk sdk;

    public SignInTest() {


    }

    public SignInTest(RVSSdk sdk) {
         this.sdk = sdk;
    }

    public void signInWithUserName(String username, String password)
    {
        sdk.signInWithUserName(username, password);

    }
}
