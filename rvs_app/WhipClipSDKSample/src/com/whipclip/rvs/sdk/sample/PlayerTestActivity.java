package com.whipclip.rvs.sdk.sample;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.whipclip.rvs.sdk.dataObjects.RVSClipView;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.sample.model.MockupRVSMediaContext;

public class PlayerTestActivity extends Activity {

    RVSClipView clipView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_test);

        clipView =(RVSClipView)findViewById(R.id.clipView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        RVSMediaContext mediaContext = Globals.getModel().mediaContext;
        if (mediaContext == null) {
            mediaContext = new MockupRVSMediaContext();
        }

        clipView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // Ensure you call it only once :
//                if (Build.VERSION.SDK_INT < 16) {
//                    clipView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                } else {
//                    clipView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                }

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) clipView.getLayoutParams();
//                params.width = width;
                 params.width = clipView.getWidth();
                 params.height = clipView.getWidth() * 3/4;
                clipView.setLayoutParams(params);
            }
        });

        clipView.setClipWithMediaContext(mediaContext, 0, mediaContext.getDuration());
        // don't use addMediaController(), using only for my tests, will remove it later
        clipView.addMediaController();
        clipView.play();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.player_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setMediaContext(RVSMediaContext mediaContext) {

        clipView.setClipWithMediaContext(mediaContext, 0, mediaContext.getDuration());
    }
}
