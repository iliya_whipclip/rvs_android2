package com.whipclip.rvs.sdk.sample.tests;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnailList;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

import com.whipclip.rvs.sdk.managers.RVSServiceManager;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.text.DateFormat;
import java.util.List;

/**
 * Created by iliya on 7/29/14.
 */
public class PostsTest extends BaseTest {

    Logger logger = LoggerFactory.getLogger(PostsTest.class);

    private static String RVSTestUserId = "_test";
    private static String RVSTestUserPassword = "kalisher30";

    RVSSdk rvsSdk;

    public PostsTest(RVSSdk sdk) {

        this.rvsSdk = sdk;
    }

    @SuppressWarnings("unchecked")
    public void createPost() {

        final RVSChannel[] testChannel = new RVSChannel[1];
        final RVSPost testPost;
        final RVSMediaContext[] testMediaContext = {null};
        final RVSPromise[] mediaContextForChannel = new RVSPromise[1];

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

//      channel_id =   "us_cnnhd"
        final String testText = String.format("test %s", dateFormat.format(date));
        rvsSdk.channelForCallSign("CNNHD").done(new AndroidDoneCallback<List<RVSChannelSearchResult>>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(List<RVSChannelSearchResult> result) {

                if (result.size() > 0) {
                    testChannel[0] = result.get(0).getChannel();
                    mediaContextForChannel[0] = rvsSdk.getMediaContextForChannel(testChannel[0].getChannelId());
                    mediaContextForChannel[0].done(new AndroidDoneCallback<RVSMediaContext>() {
                        @Override
                        public AndroidExecutionScope getExecutionScope() {
                            return AndroidExecutionScope.UI;
                        }

                        @Override
                        public void onDone(RVSMediaContext result) {
                            testMediaContext[0] = result;
                            RVSPromise rvsPromise = testMediaContext[0].thumbnailsWithStartOffset(0, testMediaContext[0].getDuration());
                            rvsPromise.done(new AndroidDoneCallback<RVSThumbnailList>() {
                                @Override
                                public void onDone(RVSThumbnailList result) {

                                    String thumbnailId = result.getImages().get(0).getThumbnailId();

                                    RVSPromise postWithTextPromise = rvsSdk.createPostWithText(testText, testMediaContext[0], 0, testMediaContext[0].getDuration(), thumbnailId);
                                    postWithTextPromise.done(new AndroidDoneCallback<RVSPost>() {
                                        @Override
                                        public void onDone(RVSPost post) {
                                            logger.info("post created");

                                            String url = post.getMediaContext().clipUrlWithMediaType(null, 0, post.getMediaContext().getDuration());
                                            logger.info("clip url: " + url);

                                            if (onCompletionListener != null) {
                                                onCompletionListener.onCompletion(post);
                                            }
                                            // example of url:
                                            // https://api-alpha3-s.whipclip.com/v1/media/video/types/hls?apiVersion%3D2.5%26clientVersion%3D1.0&mediaContext=a.dXNfY25uaGR8MTQwNjc2OTI2MzAwMHwxNDA2NzY5MzgzMDAwfDE0MDY3OTEwNTgxMDM.769a7ef885c829fbd8aa6458972562bab61bcd500b4baef67194f0b10f593ccb&endOffset=120000&startOffset=0
                                        }

                                        @Override
                                        public AndroidExecutionScope getExecutionScope() {
                                            return AndroidExecutionScope.UI;
                                        }
                                    }).fail(new FailCallback<Throwable>() {
                                        @Override
                                        public void onFail(Throwable result) {
                                            logger.error("failed to create post " + result.getMessage());
                                        }
                                    });
                                }

                                @Override
                                public AndroidExecutionScope getExecutionScope() {
                                    return AndroidExecutionScope.UI;
                                }
                            });
                        }
                    });
                }
            }
        });
        //        }, nil).thenOnMain(^id(NSArray *thumbnails) {
//        }, nil).thenOnMain(^id(NSObject<RVSPost>* post) {
//            testPost = post;
//            return [post.mediaContext clipUrlWithMediaType:nil startOffset:0 duration:post.mediaContext.duration];
//        }, nil).thenOnMain(^id(NSURL *contentUrl) {
//            NSLog(@"content url:%@", contentUrl);
//            player = [[MPMoviePlayerController alloc] initWithContentURL:contentUrl];
//            return nil;
//        }, nil);

    }


    public void createCommentWithMessage(final String message) {

        final RVSAsyncList trendingList = rvsSdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        RVSPromise trendingPromise = trending.next(1);
//        trendingPromise.done(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.done(new DoneCallback<List<RVSPost>>() {
                                 @Override
                                 public void onDone(List<RVSPost> result) {
                                     logger.debug("onDone " + result);

                                     if (result.size() > 0) {
                                         RVSPost post = result.get(0);
                                         RVSPromise promise = rvsSdk.createCommentWithMessage(message, post.getPostId());
                                         promise.done(new DoneCallback<RVSComment>() {
                                             @Override
                                             public void onDone(RVSComment comment) {
                                                 logger.info("comment created id: " + comment.getCommentId());

                                             }
                                         });

                                     }
                                 }
                             }
        );
    }

    public void postForPostId() {

        final RVSAsyncList trendingList = rvsSdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        RVSPromise trendingPromise = trending.next(1);
//        trendingPromise.done(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.done(new DoneCallback<List<RVSPost>>() {
                                 @Override
                                 public void onDone(List<RVSPost> result) {
                                     logger.debug("onDone " + result);

                                     if (result.size() > 0) {
                                         RVSPost post = result.get(0);
                                         RVSPromise postForPostIdPromise = rvsSdk.postForPostId(post.getPostId());

                                         postForPostIdPromise.done(new DoneCallback<RVSPost>() {
                                             @Override
                                             public void onDone(RVSPost post) {
                                                 logger.info("postForPostId done");

                                             }
                                         });

                                     }
                                 }
                             }
        );
    }

    public void sharingUrlForPost() {

        final RVSAsyncList trendingList = rvsSdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        RVSPromise trendingPromise = trending.next(1);
//        trendingPromise.done(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.done(new DoneCallback<List<RVSPost>>() {
                                 @Override
                                 public void onDone(List<RVSPost> result) {
                                     logger.debug("onDone " + result);

                                     if (result.size() > 0) {
                                         RVSPost post = result.get(0);
                                         RVSPromise testPromise = rvsSdk.sharingUrlsForPost(post.getPostId());

                                         testPromise.done(new DoneCallback<RVSSharingUrls>() {
                                             @Override
                                             public void onDone(RVSSharingUrls url) {
                                                 logger.info("sharingUrlForPost url" + url.getUrl());

                                             }
                                         });

                                     }
                                 }
                             }
        );
    }

    public void createLikeForPost(final String postId) {
        RVSPromise likeForPost = rvsSdk.createLikeForPost(postId);
        likeForPost.done(new DoneCallback<String>() {
            @Override
            public void onDone(String result) {
                logger.info("createLikeForPost done, postId: " + postId);

            }
        });

    }

    @SuppressWarnings("unchecked")
    public void deleteRepostForPost(final String postId) {
        RVSPromise likeForPost = rvsSdk.deleteRepostForPost(postId);
        likeForPost.done(new DoneCallback<Object>() {
            @Override
            public void onDone(Object result) {
                logger.info("deleteRepostForPost done, postId: " + postId);

            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
                logger.debug("deleteRepostForPost ERROR: " + result.getMessage());

            }
        });;
    }

    @SuppressWarnings("unchecked")
    public void deletePost(final String postId) {
        RVSPromise likeForPost = rvsSdk.deletePost(postId);
        likeForPost.done(new DoneCallback<Object>() {
            @Override
            public void onDone(Object result) {
                logger.info("deletePost done, postId: " + postId);

            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
               try {
                    RVSError rvsError = (RVSError)result;
                    logger.debug("error httpCode: " + rvsError.getHttpCode() + ", errorCode: " + rvsError.getErrorCode());

                } catch (ClassCastException e) {
                    logger.debug("deletePost ERROR: " + result.getMessage());
                }

            }
        });;
    }

    @SuppressWarnings("unchecked")
    public void deleteCommentTest() {

        final String postId =  "404c8000ab9c4f94ba5b4f369f81397c";
//        final RVSPromise[] deleteComment = new RVSPromise[1];

        RVSPromise createPromise = rvsSdk.createCommentWithMessage("Test remove comment", postId);
        createPromise.done(new AndroidDoneCallback<RVSComment>() {
            @Override
            public void onDone(RVSComment comment) {
                String commentId = comment.getCommentId();

                logger.info("comment created id: " + commentId);

                RVSPromise deleteComment  = rvsSdk.deleteComment(commentId, postId);
                deleteComment.done(new AndroidDoneCallback<Object>() {
                    @Override
                    public void onDone(Object result) {
                        logger.info("deleteComment done, postId: " + postId);

                    }

                    @Override
                    public AndroidExecutionScope getExecutionScope() {
                        return AndroidExecutionScope.UI;
                    }

                }).fail(new FailCallback<Throwable>() {
                    @Override
                    public void onFail(Throwable result) {
                        logger.debug("deleteComment ERROR: " + result.getMessage());

                    }
                });
                ;
            }

            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void createRepostForPost(final String postId, final boolean deleteRepost) {
        RVSPromise likeForPost = rvsSdk.createRepostForPost(postId);
        likeForPost.done(new AndroidDoneCallback<Object>() {
            @Override
            public void onDone(Object result) {
                logger.info("createLikeForPost done, postId: " + postId);

                if (deleteRepost) {
                    RVSPromise deletePromise = rvsSdk.deleteRepostForPost(postId);
                    deletePromise.done(new DoneCallback() {
                        @Override
                        public void onDone(Object result) {
                            logger.info("deleteRepostForPost " + postId);
                        }
                    });
                }

                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion("");
                }
            }

            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
                logger.debug("ERROR: " + result.getMessage());

            }
        });;
    }


   @SuppressWarnings("unchecked")
    public void deleteLikeForPost(final String postId) {
        RVSPromise deletPromise = rvsSdk.deleteLikeForPost(postId);
        deletPromise.done(new DoneCallback<String>() {
            @Override
            public void onDone(String result) {

                logger.info("deleteLikeForPost done, postId " + postId);
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
                logger.debug("ERROR: " + result.getMessage());

            }
        });
    }

    public void likingUsersForPost() {

        final RVSAsyncList trendingList = rvsSdk.trendingPosts();
        final RVSAsyncList trending = trendingList;
        RVSPromise trendingPromise = trending.next(20);
        trendingPromise.done(new DoneCallback<List<RVSPost>>() {
                                 @Override
                                 public void onDone(List<RVSPost> result) {
                                     logger.debug("onDone " + result);

                                     for (RVSPost post : result) {

                                         if (post.getLikeCount() > 0) {
                                             final String postId = post.getPostId();
                                             final RVSAsyncList userList = rvsSdk.likingUsersForPost(postId);
                                             RVSPromise testPromise = userList.next(post.getLikeCount());

                                             testPromise.done(new DoneCallback<List<RVSUser>>() {
                                                 @Override
                                                 public void onDone(List<RVSUser> users) {

                                                     logger.info("likingUsersForPost size " + users.size());


                                                     if (users.size() > 0) {
                                                         logger.info("1st liking user name: " + users.get(0).getName());


                                                     }

                                                 }
                                             });
                                         }

                                         break;
                                     }

                                 }
                             }
        );
    }

    @SuppressWarnings("unchecked")
    public void repostingUsersForPost(final String postId) {
        RVSAsyncList list = rvsSdk.repostingUsersForPost(postId);
        RVSPromise promise = list.next(5);
        promise.done(new DoneCallback<List<RVSUser>>() {
             @Override
             public void onDone(List<RVSUser> result) {
                 logger.debug("onDone " + result);
             }
         }).fail(new FailCallback<Throwable>() {

            @Override
            public void onFail(Throwable result) {
                try {
                    RVSError rvsError = (RVSError)result;
                    logger.debug("repostingUsersForPost error httpCode: " + rvsError.getHttpCode() + ", errorCode: " + rvsError.getErrorCode());

                } catch (ClassCastException e) {
                    logger.debug("repostingUsersForPost ERROR: " + result.getMessage());
                }
            }
        });

    }
}
