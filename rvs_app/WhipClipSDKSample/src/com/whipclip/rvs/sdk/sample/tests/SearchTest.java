package com.whipclip.rvs.sdk.sample.tests;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchCompositeResultList;
import com.whipclip.rvs.sdk.dataObjects.RVSUserSearchResult;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class SearchTest {

    Logger logger = LoggerFactory.getLogger(SearchTest.class);

    private RVSSdk sdk;

    public SearchTest(RVSSdk sdk) {
        this.sdk = sdk;
    }


    public void searchUserTest() {

        // there are two "ost" user in server !!! :)
        RVSAsyncList users = sdk.searchUser("ost");
        RVSPromise promise = users.next(5);
        promise.then(new DoneCallback<List<RVSUserSearchResult>>() {

            @Override
            public void onDone(List<RVSUserSearchResult> result) {

                logger.debug("found users: " + result.size());
                for (RVSUserSearchResult obj : result) {
                    logger.debug("found user " + obj.getUser().getName() + ", id: " + obj.getUser().getUserId());
                }
                logger.debug("");

            }
        });
    }

    public void searchContent() {

        // there are two "ost" user in server !!! :)
        RVSAsyncList content = sdk.searchContent("boss", null, -1);
        RVSPromise promise = content.next(5);
        promise.then(new DoneCallback<List<RVSContentSearchResult>>() {

            @Override
            public void onDone(List<RVSContentSearchResult> result) {

                logger.debug("found content: " + result.size());

            }
        });
    }

    public void searchChannels() {

        // there are two "ost" user in server !!! :)
        RVSAsyncList content = sdk.searchChannel("cnn");
        RVSPromise promise = content.next(5);
        promise.then(new DoneCallback<List<RVSChannelSearchResult>>() {

            @Override
            public void onDone(List<RVSChannelSearchResult> result) {

                logger.debug("found content: " + result.size());
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void searchSuggestions(String string) {

        RVSPromise promise = sdk.searchSuggestions(string);
        promise.then(new DoneCallback<RVSSearchCompositeResultList>() {

            @Override
            public void onDone(RVSSearchCompositeResultList result) {

                logger.debug("done suggestions: ");
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
                logger.debug("error: " + result.getMessage());
            }
        });
    }
}

