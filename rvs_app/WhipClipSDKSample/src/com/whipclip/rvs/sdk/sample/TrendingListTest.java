package com.whipclip.rvs.sdk.sample;

import android.graphics.Bitmap;
import android.graphics.Point;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;

import org.jdeferred.DoneCallback;
import org.slf4j.Logger;

import java.util.List;

/**
 * Created by iliya on 7/11/14.
 */
public class TrendingListTest {

    Logger logger = org.slf4j.LoggerFactory.getLogger(TrendingListTest.class);

    private RVSSdk sdk;
    RVSPromise trendingPromise;

    public TrendingListTest(RVSSdk sdk) {
        this.sdk = sdk;
    }

    public void trendingPosts()
    {
        final RVSAsyncList trending = sdk.trendingPosts();
        trendingPromise = trending.next(15);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                RVSPost rvsPost = result.get(0);
                logger.info("show title: " + rvsPost.getProgram().getShow().getName());

                for (RVSPost post : result) {

                    if (post.getLatestComments() != null) {
                        List<RVSComment> latestComments = post.getLatestComments();
                        logger.info("latest comment not null");
                    }
                }


//                trendingPromise = trending.next(5);
//                trendingPromise.then(new DoneCallback<List<RVSPost>>() {
//
//                    @Override
//                    public void onDone(List<RVSPost> result) {
//                        logger.debug("onDone 1" + result);
//
//                    }
//                });
            }
        });
    }

    public void getImageWithSize()
    {
        final RVSAsyncList trending = sdk.trendingPosts();
        trendingPromise = trending.next(1);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                RVSPost rvsPost = result.get(0);
                RVSAsyncImage coverImage = rvsPost.getCoverImage();

                String imageUrlForSize = coverImage.getImageUrlForSize(new Point(480, 320));
                RVSPromise imagePromise = coverImage.getImageWithSize(new Point(480, 320));

                imagePromise.then(new DoneCallback<Bitmap>() {

                    @Override
                    public void onDone(Bitmap result) {
                        logger.debug("image downloaded");

                    }
                });
            }
        });
    }
}
