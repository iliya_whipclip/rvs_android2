package com.whipclip.rvs.sdk.sample.tests;

import android.graphics.Bitmap;
import android.graphics.Point;

import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSFeedCountDetails;
import com.whipclip.rvs.sdk.dataObjects.RVSNotification;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * Created by iliya on 8/4/14.
 */
public class UserTest extends BaseTest {

    Logger logger = LoggerFactory.getLogger(UserTest.class);

    private RVSSdk sdk;

    public UserTest(RVSSdk sdk) {
        this.sdk = sdk;
    }

    public void signInWithUserName(String username, String password)
    {
        // login first
        if (!sdk.isSignedOut()) {
            sdk.signOut();
        }

        sdk.signInWithUserName(username, password);

    }

    public void followersOfUser(String userId) {

        RVSAsyncList followersList = sdk.followersOfUser(userId);
        RVSPromise promise = followersList.next(1);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        promise.done(new DoneCallback<List<RVSReferringUser>>() {
            @Override
            public void onDone(List<RVSReferringUser> result) {
                logger.debug("onDone " + result);

                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion(result);
                } else {

                    for (RVSReferringUser referringUser : result) {

                        RVSUser user = referringUser.getUser();
                        logger.info("referring user " + user.getName());
                    }
                }
            }
        });
    }

     public void feedForUser(String userId, boolean userFeed) {

        RVSAsyncList feedsList = null;

        if (userFeed) {
            feedsList = sdk.userFeedForUser(userId);
        } else {
            feedsList = sdk.newsFeedForUser(userId);

        }

        RVSPromise promise = feedsList.next(1);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        promise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {

                logger.debug("onDone " + result);


                for (RVSPost rvsPost : result) {

                    logger.info("post id: " + rvsPost.getPostId());
                }
               }
         });
    }

    @SuppressWarnings("unchecked")
    public void notificationsCountForUser(String userId) {
//        [[[RVSSdk sharedSdk] notificationsCountForUser:[RVSSdk sharedSdk].myUserId fromStartTime:[lastImpression timeIntervalSince1970]] thenOnMain:^(NSNumber *count) {
//            [promise fulfillWithValue:cou

        Date date = new Date();
        RVSPromise promise = sdk.notificationsCountForUser(userId,date.getTime());
        promise.done(new DoneCallback<RVSFeedCountDetails>() {
            @Override
            public void onDone(RVSFeedCountDetails result) {
                logger.info("notificationsCountForUser count: " + result.getCount());

            }
        }).fail(new FailCallback<Throwable>() {

            @Override
            public void onFail(Throwable result) {
                try {
                    RVSError rvsError = (RVSError) result;
                    logger.debug("notificationsCountForUser error httpCode: " + rvsError.getHttpCode() + ", errorCode: " + rvsError.getErrorCode());

                } catch (ClassCastException e) {
                    logger.debug("notificationsCountForUser ERROR: " + result.getMessage());
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void newsFeedCountForUser(String userId) {
//        [[[RVSSdk sharedSdk] notificationsCountForUser:[RVSSdk sharedSdk].myUserId fromStartTime:[lastImpression timeIntervalSince1970]] thenOnMain:^(NSNumber *count) {
//            [promise fulfillWithValue:cou

        Date date = new Date();
        RVSPromise promise = sdk.newsFeedCountForUser(userId,date.getTime());
        promise.done(new DoneCallback<RVSFeedCountDetails>() {
            @Override
            public void onDone(RVSFeedCountDetails result) {
                logger.info("newsFeedCountForUser count: " + result.getCount());

            }
        }).fail(new FailCallback<Throwable>() {

            @Override
            public void onFail(Throwable result) {
                try {
                    RVSError rvsError = (RVSError) result;
                    logger.debug("newsFeedCountForUser error httpCode: " + rvsError.getHttpCode() + ", errorCode: " + rvsError.getErrorCode());

                } catch (ClassCastException e) {
                    logger.debug("newsFeedCountForUser ERROR: " + result.getMessage());
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void notificationsForUser(String userId) {

        RVSAsyncList list = sdk.notificationsForUser(userId);
        RVSPromise promise = list.next(5);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        promise.then(new DoneCallback<List<RVSNotification>>() {
            @Override
            public void onDone(List<RVSNotification> result) {

                logger.debug("onDone notificationsForUser " + result.size());

            }
        }).fail(new FailCallback<Throwable>() {

            @Override
            public void onFail(Throwable result) {
                try {
                    RVSError rvsError = (RVSError) result;
                    logger.debug("notificationsForUser error httpCode: " + rvsError.getHttpCode() + ", errorCode: " + rvsError.getErrorCode());

                } catch (ClassCastException e) {
                    logger.debug("notificationsForUser ERROR: " + result.getMessage());
                }
            }
        });
    }

    // barakH
    @SuppressWarnings("unchecked")
    public void userForUserId(String userName) {
    	
    	
    	RVSPromise promise = sdk.userForUserId(userName);
//		  promise.then(new DoneCallback<RVSUser>() {
//		      @Override
//		      public void onDone(RVSUser result) {
//		
//		          logger.debug("onDone userName "  + result.getName());
//		      }
//		  }).fail(new FailCallback<Throwable>() {
//		      @Override
//		      public void onFail(Throwable error) {
//		
//		          logger.debug("onDone userName "  + error.getMessage());
//		      }
//		  });
//		  
	}

}
