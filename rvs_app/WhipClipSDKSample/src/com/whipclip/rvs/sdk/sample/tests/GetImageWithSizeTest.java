package com.whipclip.rvs.sdk.sample.tests;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.widget.ImageView;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;

import org.jdeferred.DoneCallback;
import org.slf4j.Logger;

import java.util.List;

/**
 * Created by iliya on 7/24/14.
 */
public class GetImageWithSizeTest {

    private Logger logger  = org.slf4j.LoggerFactory.getLogger(GetImageWithSizeTest.class);

    public void getImageWithSizeTest(RVSSdk sdk, final ImageView imageView) {

        final RVSAsyncList trending = sdk.trendingPosts();
        RVSPromise trendingPromise = trending.next(1);
//        trendingPromise.then(new DoneCallback<List<RVSPostImp>>() {
        trendingPromise.then(new DoneCallback<List<RVSPost>>() {
            @Override
            public void onDone(List<RVSPost> result) {
                logger.debug("onDone " + result);

                RVSPost rvsPost = result.get(0);
                RVSAsyncImage coverImage = rvsPost.getCoverImage();


                String imageUrlForSize = coverImage.getImageUrlForSize(new Point(480, 320));
//                RVSPromise imagePromise = coverImage.getImageWithSize(new Point(480, 320));
                RVSPromise imagePromise = rvsPost.getAuthor().getPofileImage().getImageWithSize(new Point(480, 320));

                imagePromise.then(new DoneCallback<Bitmap>() {

                    @Override
                    public void onDone(Bitmap result) {
                        logger.debug("image downloaded");
                        imageView.setImageBitmap(result);

                    }
                });
            }
        });
    }}
