package com.whipclip.rvs.sdk.dataObjects;

public interface RVSTranscriptItem {

	/**
	 *  Start time in milliseconds since 1970
	 */
	public long getStartTime();

	/**
	 *  End time in milliseconds since 1970
	 */
	public long getEndTime();

	/**
	 *  The transcript
	 */
	public String getText();
	
}
