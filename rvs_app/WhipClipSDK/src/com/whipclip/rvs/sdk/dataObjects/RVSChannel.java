package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/11/14.
 */
public interface RVSChannel {

    /**
     * Channel ID
     **/
    public String getChannelId();

    /**
     * Channel display name
     **/
    public String getName();

    /**
     * Channel logo
     **/
    public RVSAsyncImage getLogo();

    /**
     * Channel watermark logo to display on channel video
     **/
    public RVSAsyncImage getWatermarkLogo();

    /**
     * Channel official site URL
     **/
    public String getOfficialUrl();

    /**
     *  Channel call sign
     */
    public String getCallSign();

    /**
     *  Current program if available
     */
    public RVSProgram getCurrentProgram();

    /**
     *  Channel pre-roll
     */
    public String getPreRollUrl();

}


