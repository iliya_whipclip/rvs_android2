package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 8/17/14.
 */
public interface RVSSearchCompositeResultList {

    /**
     * Array of RVSSearchContentSuggestionResult
     */
    public List<RVSSearchContentSuggestionResult> getContentResults();


    /**
     * Array of RVSSearchChannelSuggestionResult
     */
    public List<RVSSearchChannelSuggestionResult> getChannelResults();


    /**
     * Array of RVSSearchUserSuggestionResult
     */
    public List<RVSSearchUserSuggestionResult> getUserResults();

}
