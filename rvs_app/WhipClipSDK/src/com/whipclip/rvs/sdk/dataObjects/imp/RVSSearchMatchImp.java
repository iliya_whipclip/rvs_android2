package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSCoverImage;
import com.whipclip.rvs.sdk.dataObjects.RVSFragmentRange;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchMatch;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSSearchMatchImp implements RVSSearchMatch {

    private String fieldName;
    private String valueFragment;
    private Number referenceTime;
    private RVSMediaContextImp mediaContext;
    private RVSAsyncImage coverImage;
    private RVSCoverImage postCoverImage;

    private List<RVSFragmentRange> fragmentRanges;

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public String getValueFragment() {
        return valueFragment;
    }

    @Override
    public List<RVSFragmentRange> getFragmentRanges() {
        return fragmentRanges;
    }

    @Override
    public long getReferenceTime() {
        return referenceTime.longValue();
    }

    @Override
    public RVSMediaContext getMediaContext() {
        return mediaContext;
    }

    @Override
    public RVSAsyncImage getCoverImage() {
        if (coverImage != null)
            return coverImage;

        if (postCoverImage != null)
        {
            coverImage = new RVSImageFromUrlOrArray(postCoverImage.getImages());
            return coverImage;
        }

        return null;
    }

    // JSON MAPPER
    private void setFragmentRanges(List<RVSFragmentRange> fragmentRanges) { this.fragmentRanges = fragmentRanges; }
    private void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    private void setValueFragment(String valueFragment) {
        this.valueFragment = valueFragment;
    }
    private void setReferenceTime(Number referenceTime) {
        this.referenceTime = referenceTime;
    }
    private void setMediaContext(RVSMediaContextImp mediaContext) { this.mediaContext = mediaContext; }
    private void setCoverImage(RVSCoverImage coverImage) { this.postCoverImage = coverImage; }

}
