package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;

import java.util.List;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSPostList extends RVSPagingListBase {

    private List<RVSPostImp> posts;

    public List<RVSPostImp> getPosts() {
        return posts;
    }

    /**
     * Used via reflection
     * @param posts
     */
    private void setPosts(List<RVSPostImp> posts) {
        this.posts = posts;
    }
}
