package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/28/14.
 */
public interface RVSUserSearchResult {
    /**
     *  User found or nil
     */
    public RVSUser getUser();

}
