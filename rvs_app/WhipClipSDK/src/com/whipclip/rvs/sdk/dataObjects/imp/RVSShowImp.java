package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSShowImp implements RVSShow{
	// todo RVSCastMember
    private List castMembers;
    
    private List<RVSImage> images;
    private String name;
    private String showId;
    private String synopsis;
    private String officialUrl;
    private RVSAsyncImage image;
	private RVSChannelImp channel;
	private RVSProgramImp nextProgram;
    private Number episodesCount;


    @Override
    public String getShowId() {
        return showId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSynopsis() {
        return synopsis;
    }

    @Override
    public List getCastMembers() {
        return castMembers;
    }

    @Override
    public RVSAsyncImage getImage() {

        if (image != null)
            return image;

        if (images.size() > 0)
        {
            image = new RVSImageFromUrlOrArray(images);
            return image;
        }

        return null;
    }

    @Override
    public String getOfficialUrl() {
        return officialUrl;
    }
    
    @Override
	public RVSChannel getChannel() {
		return channel;
	}

	@Override
	public RVSProgram getNextProgram() {
		return nextProgram;
	}
	
	@Override
	public int getEpisodesCount() {
		return episodesCount.intValue();
	}
	

    // JSON MAPPER
    private void setCastMembers(List castMembers){ this.castMembers = castMembers; }
    private List<RVSImage> getImages(){ return this.images; }
    private void setImages(List<RVSImage> images){ this.images = images; }
    private void setName(String name){ this.name = name; }
    private void setId(String showId){ this.showId = showId; }
    private void setSynopsis(String synopsis){ this.synopsis = synopsis; }
    private void setOfficialUrl(String officialUrl){ this.officialUrl = officialUrl; }
    private void setChannel(RVSChannelImp channel) { this.channel = channel; }
    private void setNextProgram(RVSProgramImp nextProgram) { this.nextProgram = nextProgram; }
    public void setEpisodesCount(Number episodesCount) { this.episodesCount = episodesCount; }
}
