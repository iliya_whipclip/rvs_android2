package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/16/14.
 */
public interface RVSEpisode {

	/**
     * season number
     */
	public String getName();
	
    /**
     * season number
     */
    public int getSeasonNumber();

    /**
     * episode number
     */
    public int getEpisodeNumber();
    
    /**
     *  image for episode
     */
   public RVSAsyncImage getImage();
}
