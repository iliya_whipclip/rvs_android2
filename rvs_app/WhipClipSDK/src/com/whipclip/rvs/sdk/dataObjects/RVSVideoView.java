package com.whipclip.rvs.sdk.dataObjects;

import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;

final class RVSVideoView extends VideoView implements OnCompletionListener, OnErrorListener, OnPreparedListener
{
    Logger logger = LoggerFactory.getLogger(RVSVideoView.class);

    private RVScalingMode m_scalingMode = RVScalingMode.RVScalingModeAspectFit;

    public enum RVScalingMode
    {
        RVScalingModeAspectFit,
        RVScalingModeFill
    }

    public static final int STATE_IDLE         = 0;
    public static final int STATE_INITIALIZED  = 1;
    public static final int STATE_PREPARED     = 2;
    public static final int STATE_STARTED      = 3;
    public static final int STATE_PAUSED       = 4;
    public static final int STATE_STOPPED      = 5;
    public static final int STATE_COMPLETED    = 6;
    public static final int STATE_RECOVERY     = 7;
    public static final int STATE_ERROR        = 8;

//    private static final String TAG = "RVSVideoView";

    // difference in seconds between playback position to duration in order to
    // detect real end of movie for cases when movie ends early but without
    // error
    static final int SecondsDifferenceBetweenPositionAndDurationForEndOfMovie = 1 * 1000;

    // time to wait when movie player in playback state, but buffering, before
    // retrying
    static final long SecondsUntilRetryingWhenPlaybackStuck = 15 * 1000;

    // time to wait from start of playing until retrying if not loaded yet
    static final long SecondsUntilRetryingWhenNotLoaded = 30 * 1000;

    // time to wait after movie finished (not at the end and not because of
    // user) before retrying
    static final int SecondsUntilRetryingAfterFinished = 5 * 1000;

    private long m_bufferingStartTime = -1;

    private Timer m_playbackTimer;

    private PlaybackTimerTask m_playbackTimerTask;

    private int m_lastPlaybackTime;

    private Uri m_uri;

    private OnErrorListener m_onErrorListener;

    private OnCompletionListener m_onCompletionListener;

    private MediaPlayer.OnPreparedListener m_onPreparedListener;

    private OnVideoViewListener m_onVideoViewListener;

    private String m_subscriptionToken;

    private long m_internetAvailableTime;

    private long m_setContentUrlTime;

    private long m_lastErrorTime;

    private Handler m_handler;

    private int m_currentState = STATE_IDLE;
    private int m_prevState;

    boolean m_videoSizeIsSet = false;

    private int     m_initialPlaybackTime = -1;

	private int m_backColor = -1;

    private class PlaybackTimerTask extends TimerTask
    {
        @Override
        public void run()
        {

            if (isNetworkAvailable())
            {
//                logger.info("NetworkAvailable True");
                if (m_internetAvailableTime <= 0)
                {
                    m_internetAvailableTime = System.currentTimeMillis();
                }
            } else
            {
//                logger.info("NetworkAvailable False");
                m_internetAvailableTime = -1;
            }

            try
            {
                if (m_lastErrorTime > 0 || (!isPrepared() && m_uri != null && m_internetAvailableTime > 0))
                {
                    if (m_setContentUrlTime > 0 )
                    {
                        // no point in retrying if already loaded (going back to not
                        // loaded is handled in loadStateChanged) or no internet
                        long loadTime = System.currentTimeMillis() - m_setContentUrlTime;
                        // if not loaded for a long time and there is a network then
                        // retry loading
                        if (loadTime > SecondsUntilRetryingWhenNotLoaded)
                        {
                            logger.info("Recovery playback since movie did not load for " + loadTime + " seconds");
                            setState(STATE_RECOVERY);
                        }
                    } else if (m_lastErrorTime > 0)
                    {
                        // if finished with player error and enough time passed then retry loading
                        long finishTime = System.currentTimeMillis() - m_lastErrorTime;
                        if (finishTime > SecondsUntilRetryingAfterFinished)
                        {
                            logger.info("Recovery playback since finished " + finishTime / 1000 + " seconds ago with error");
                            setState(STATE_RECOVERY);
                        }
                    }
                } else if (isPrepared())// &&  (isPlaying() || m_currentState == STATE_STARTED || m_currentState == STATE_STARTED)))
                {
                    int currentPosition = getCurrentPosition();
                    logger.info("current position: " + currentPosition + ", m_lastPlaybackTime: " + m_lastPlaybackTime);
                    if (currentPosition == m_lastPlaybackTime || (m_currentState == STATE_STARTED && currentPosition == 0))
                    {
                        // if buffering for a long time and there is internet
                        // for a long time then maybe player is stuck so try to
                        // Recovery
                        // logger.info("Buffering (timer) ...");
                        if (m_bufferingStartTime == -1)
                        {
                        	if (m_currentState != STATE_COMPLETED) {
                        		startBuffering();
                        	}
                        } else
                        {
                            long bufferingTime = System.currentTimeMillis() - m_bufferingStartTime;
                            if (bufferingTime > SecondsUntilRetryingWhenPlaybackStuck)
                            {
                                long internetTime = 0;
                                if (m_internetAvailableTime > 0)
                                {
                                    internetTime = System.currentTimeMillis() - m_internetAvailableTime;
                                }

                                if (internetTime > SecondsUntilRetryingWhenPlaybackStuck)
                                {
                                    logger.info("Recovery playback since playback was stuck " + bufferingTime + " seconds ago");
                                    setState(STATE_RECOVERY);
                                }
                            }
                        }
                    } else
                    if (currentPosition != 0 && m_lastPlaybackTime != -1)
                    {
                        stopBuffering();
                    }

                    if (isPlaying() && currentPosition > -1)
                    {
                        setLastPlaybackTime(currentPosition);

                        if (m_onVideoViewListener != null) {
                            m_onVideoViewListener.onDidProgressWithCurrentDuration();
                        }
                    }
                }
                else if (isPrepared() && !isPlaying()) // paused
                {
                    stopBuffering();
                }
            } catch (Exception e)
            {
                logger.error(e.toString());
            }
            // if buffering started (but not just now) send another buffering notification
            if (m_bufferingStartTime >= 1000)
            {
                long bufferingTime = System.currentTimeMillis() - m_bufferingStartTime;
                notifyBuffering(bufferingTime / 1000);
            }
        }
    };

    public interface OnVideoViewListener
    {
        //        public void onPlayPauseStateChanged(boolean playing);
        public void onBufferingStateChanged(boolean buffering);
        public void onDidProgressWithCurrentDuration();
        public void onStateChanged(int state);
    }


    RVSVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setOnErrorListener(this);
        setOnPreparedListener(this);
        setOnCompletionListener(this);

        m_handler = new Handler(Looper.getMainLooper());

    }

    public int getLastPlaybackTime()
    {
        return m_lastPlaybackTime;
    }

    private void setLastPlaybackTime(int lastPlaybackTime)
    {
        m_lastPlaybackTime = lastPlaybackTime;
    }

    private void doneWithCurrentContent()
    {
    	logger.debug("doneWithCurrentContent state " +  this.m_currentState);
    	
        clearPlaybackData();

        if ((isPlaying() && this.m_currentState != STATE_PAUSED) || this.m_currentState == STATE_INITIALIZED || this.m_currentState == STATE_STARTED)
        {
            stopPlayback();
        }

        stopBuffering();
//        setVideoViewBackgroundColor(Color.BLACK);
        setState(STATE_IDLE);
        super.setVideoURI(null);
        

        setLastErrorTime(-1);
    }

    private void clearPlaybackData()
    {
        releasePlaybackTimer();
        setLastPlaybackTime(-1);
        setInitialPlaybackTime(-1);
    }

    @Override
    public void stopPlayback()
    {
        logger.info("stopPlayback");

        if (m_currentState > STATE_PREPARED) {
        	logger.info("stopPlayback native call");
        	super.stopPlayback();
        }

        logger.info("stopPlayback finished");

//        setIsPlaying(false, false);
//        setIsPrepared(false);

//        logger.info("stopPlayback finished 1");
    }

//    private void setIsPlaying(boolean playing)
//    {
//        this.m_playing = playing;
//    }

//    private void setIsPrepared(boolean prepared)
//    {
//        this.m_isPrepared = prepared;
//    }

    private void setLastErrorTime(long lastErrorTime)
    {
        this.m_lastErrorTime = lastErrorTime;
    }

    @Override
    public void setVideoPath(String path)
    {
    	if (path == null) {
    		setVideoURI(null);
    	} else {
    		setVideoURI(Uri.parse(path));
    	}
    }

    private void setVideoUriInternal(Uri uri)
    {
        logger.info("setVideoUriInternal: " + ((uri == null) ? "null" : uri.toString()));

        m_uri = uri;

//        setIsPlaying(false, false);
//        setIsPrepared(false);

        m_setContentUrlTime = System.currentTimeMillis();

        // get rights if a widevine URL before playing it

        setState(STATE_INITIALIZED);
        super.setVideoURI(uri);

        if (uri != null)
        {
            startPlaybackTimer();
//            startBuffering();
        }
    }

    public void setVideoURI(Uri uri)
    {
        doneWithCurrentContent();
        setVideoUriInternal(uri);

    }

    @Override
    public void setOnErrorListener(OnErrorListener l)
    {
        if (l == this)
        {
            super.setOnErrorListener(l);
        } else
        {
            m_onErrorListener = l;
        }
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener l)
    {
        if (l == this)
        {
            super.setOnCompletionListener(l);
        } else
        {
            m_onCompletionListener = l;
        }

    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l)
    {
        if (l == this)
        {
            super.setOnPreparedListener(l);
        } else
        {
            m_onPreparedListener = l;
        }
    }

    public void setOnVideoViewListener(OnVideoViewListener l)
    {
        m_onVideoViewListener = l;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        String msg = String.format("onError(%s,%s), lastPlaybackTime %s", what, extra, getLastPlaybackTime());
        logger.error(msg);

        if (m_onErrorListener != null)
        {
            m_onErrorListener.onError(mp, what, extra);
        }
//        mp.reset();
        setState(STATE_ERROR);
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp)
    {
        logger.info("onCompletion");
        if (m_onCompletionListener != null)
        {
            m_onCompletionListener.onCompletion(mp);
        }
        setState(STATE_COMPLETED);
    }

    @Override
    public void start()
    {
        logger.info("start");
        super.start();
        
        if (m_currentState >= STATE_PREPARED) {
        	setState(STATE_STARTED);
        } 
    }

    private void startPlaybackTimer()
    {
        logger.info("startPlaybackTimer");
        if (m_playbackTimer == null)
        {
            m_playbackTimer = new Timer();
            m_playbackTimerTask = new PlaybackTimerTask();
            m_playbackTimer.scheduleAtFixedRate(m_playbackTimerTask, 0, 200);
        }
    }

    private void releasePlaybackTimer()
    {
        logger.info("releasePlaybackTimer");
        if (m_playbackTimer != null)
        {
            m_playbackTimerTask.cancel();
            m_playbackTimerTask = null;

            m_playbackTimer.cancel();
            m_playbackTimer = null;
        }
    }

    private void recoveryPlayback()
    {
        logger.info("Recovery playback");
//        final long lastErrorTime = m_lastErrorTime;
        setLastErrorTime(-1);
        m_internetAvailableTime = -1;
        releasePlaybackTimer();
        // we must call videoview methods from main/ui thread
//        boolean added = post(new Runnable()
        boolean added = m_handler.postAtFrontOfQueue(new Runnable()
        {
            @Override
            public void run()
            {
                logger.debug("Recovery playback, run");
//                if (isPlaying() || lastErrorTime > 0)
//                {
//                    stopPlayback();
//                }
                if (m_prevState == STATE_ERROR)
                {
                    start();
                }
                setVideoUriInternal(m_uri);

            }
        });

        logger.debug("Recovery playback, added: " + added);
    }

    private void seekToInitialPlaybackTime() {
        logger.debug("seekToInitialPlaybackTime time: " + m_initialPlaybackTime);

        if (m_initialPlaybackTime != -1)//&& mp.getVideoHeight() !=0 && mp.getVideoWidth()!=0)
        {
            seekTo(m_initialPlaybackTime);
            setInitialPlaybackTime(-1);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp)
    {
        logger.info("onPrepared");
        m_videoSizeIsSet = false;
        
//		setVideoViewBackgroundColor(Color.TRANSPARENT);
        
        if (m_onPreparedListener != null)
        {
            m_onPreparedListener.onPrepared(mp);
        }

        mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener()
        {

            @Override
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height)
            {
                logger.info("onVideoSizeChanged width/height " + width + "/" + height );
                setVideoSize(width,height);

                if (width == 0 || height == 0) {
                    logger.debug("invalid video width(" + width + ") or height(" + height + ")");
                } else {

                    m_videoSizeIsSet = true;
                    logger.debug("video size set");

                    if (m_currentState > STATE_PREPARED && m_initialPlaybackTime != -1) {

                        seekToInitialPlaybackTime();
                    }
                }
//                setVideoSize(getMeasuredWidth(),getMeasuredHeight());
            }
        });


        if (m_videoSizeIsSet) {
            seekToInitialPlaybackTime();
        }

        setState(STATE_PREPARED);

    }
    
    private void setVideoViewBackgroundColor(final int color) {
    	
    	if (this.m_backColor != -1 && this.m_backColor == color) return;
    	
    	this.m_backColor = color;
    	if (color == Color.TRANSPARENT) {
	    	 m_handler.post(new Runnable() {
	 			
	 			@Override
	 			public void run() {
	 		        setBackgroundColor(color);
	 			}
	 		});
    	}
    }

    private void startBuffering()
    {
        logger.info("startBuffering");

        // already started buffering
        if (m_bufferingStartTime > 0)
        {
            return;
        }

        m_bufferingStartTime = System.currentTimeMillis();
        notifyBuffering(0);
    }

    private void stopBuffering()
    {
//    	setVideoViewBackgroundColor(Color.TRANSPARENT);
        // not buffering so nothing to stop
        if (m_bufferingStartTime == -1)
        {
            return;
        }

        m_bufferingStartTime = -1;

        if (m_onVideoViewListener != null) {
            m_onVideoViewListener.onBufferingStateChanged(false);
        }

//        logger.info("Posting buffering stop notification");
//        HashMap<String, Object> userInfo = new HashMap<String, Object>();
//        userInfo.put(RV_MEDIA_PLAYER_BUFFERING_USER_INFO_KEY, false);
//        RVCore.sharedRVCore().getNotificationCenter().postNotification(RV_MEDIA_PLAYER_BUFFERING_NOTIFICATION, this, userInfo);
    }

    private void notifyBuffering(long seconds)
    {
        logger.info("buffering " + seconds + " seconds");
        // only notify start buffering
        if (m_onVideoViewListener != null && seconds == 0) {
            m_onVideoViewListener.onBufferingStateChanged(true);
        }

//        // logger.info("Posting buffering notification, buffering for " + seconds + " seconds");
//        HashMap<String, Object> userInfo = new HashMap<String, Object>();
//        userInfo.put(RV_MEDIA_PLAYER_BUFFERING_USER_INFO_KEY, true);
//        userInfo.put(RV_MEDIA_PLAYER_BUFFERING_TIME_USER_INFO_KEY, seconds);
//        RVCore.sharedRVCore().getNotificationCenter().postNotification(RV_MEDIA_PLAYER_BUFFERING_NOTIFICATION, this, userInfo);
    }

    @Override
    public void pause()
    {
        super.pause();

        setState(STATE_PAUSED);
    }

    public boolean isPrepared()
    {
        return (m_currentState >= STATE_PREPARED);
    }

    private boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setState(int state)
    {
        if (m_currentState != state)
        {
            logger.info("setState " + getStateName(state));

            m_prevState = m_currentState;
            m_currentState = state;
            switch (m_currentState)
            {
                case STATE_PREPARED:

                    m_setContentUrlTime = -1;
                    break;

                case STATE_STARTED:
                    startPlaybackTimer();
                case STATE_PAUSED:
//                    if (m_onVideoViewListener != null)
//                    {
//                    	m_onVideoViewListener.onPlayPauseStateChanged(m_currentState == STATE_STARTED ? true : false);
//                    }

                    if (m_currentState == STATE_PAUSED) {
                        releasePlaybackTimer();
                        stopBuffering();
                    }
                    break;

                case STATE_COMPLETED:
                    int lastPlaybackTime = getLastPlaybackTime();
                    int duration = getDuration();

//                    if ((duration - lastPlaybackTime) < SecondsDifferenceBetweenPositionAndDurationForEndOfMovie && lastPlaybackTime > 0)
//                    {
//                        logger.info("Playback was finished at " + lastPlaybackTime + " and duration is " + duration + ", diff: " + (duration - lastPlaybackTime));
//                        setLastPlaybackTime(-1);
//                    }
                    break;

                case STATE_ERROR:
                    setLastErrorTime(System.currentTimeMillis());
                    break;
                    
                case STATE_RECOVERY:
                    recoveryPlayback();
                    break;                    

            }
        }
        
        if (m_onVideoViewListener != null) {
        	m_onVideoViewListener.onStateChanged(state);
        }
        	
    }

    private String getStateName(int state)
    {
        String name = "" + state;
        switch (state)
        {

            case STATE_IDLE:        return "STATE_IDLE";
            case STATE_INITIALIZED: return "STATE_INITIALIZED";
            case STATE_PREPARED:    return "STATE_PREPARED";
            case STATE_STARTED:     return "STATE_STARTED";
            case STATE_PAUSED:      return "STATE_PAUSED";
            case STATE_STOPPED:     return "STATE_STOPPED";
            case STATE_COMPLETED:   return "STATE_COMPLETED";
            case STATE_ERROR:       return "STATE_ERROR";

        }

        return name;
    }

    private int m_videoWidth = 0;
    private int m_videoHeight = 0;

    private int width = 0;
    private int height = 0;

    public void setSize(int widht, int height)
    {
        setMeasuredDimension(widht, height);
    }

    public void setVideoSize(int videoWidth, int videoHeight)
    {
        // Set the new video size
        m_videoWidth = videoWidth;
        m_videoHeight = videoHeight;
    }

    public void setScalingMode1(RVScalingMode mode)
    {
        if (mode != m_scalingMode)
        {
            m_scalingMode = mode;
        }
    }

    public void setScalingMode(RVScalingMode mode)
    {
        if (mode != m_scalingMode)
        {
            m_scalingMode = mode;
            onScalingModeChangde();
        }
    }



    private void onScalingModeChangde() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        if (m_scalingMode == RVScalingMode.RVScalingModeAspectFit)
        {
            if(m_videoWidth <= 0 || m_videoHeight <= 0)
            {
                return;
            }

            logger.info("onMeasure width/height: " +  getWidth() + "/" + getHeight());

            int newHeight;
            int newWidth;

            if (width == 0) width = getWidth();
            if (height == 0) height = getHeight();

            if (m_scalingMode ==  RVScalingMode.RVScalingModeAspectFit)
            {
                logger.info("onMeasure mode aspect");

                float heightRatio = (float) m_videoHeight / height;
                float widthRatio = (float) m_videoWidth / width;


                if (heightRatio > widthRatio) {
                    newHeight = (int) Math.ceil(m_videoHeight / heightRatio);
                    newWidth = (int) Math.ceil(m_videoWidth / heightRatio);
                } else {
                    newHeight = (int) Math.ceil(m_videoHeight / widthRatio);
                    newWidth = (int) Math.ceil(m_videoWidth / widthRatio);
                }

                params.width = newWidth;
                params.height = newHeight;

            }
        }
        else
        {
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }

        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        setLayoutParams(params);

    }

    public RVScalingMode getScalingMode()
    {
        return m_scalingMode;
    }

    public void setInitialPlaybackTime(int initialPlaybackTime) {
        m_initialPlaybackTime = initialPlaybackTime;
    }

    public void destroy() {
    	logger.debug("destroy");
        doneWithCurrentContent();
    }

	public void startMoviePlayback() {
		
		startPlaybackTimer();
		startBuffering();
		
		this.start();
	}

}
