package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 7/16/14.
 */
public interface RVSShow {

    /**
     * show ID
     */
    public String getShowId();

    /**
     * show display name
     */
    public String getName();

    /**
     * show synopsis
     */
    public String getSynopsis();

    /**
     * array of RVSCastMember participating in the show
     */
    public List getCastMembers();

    /**
     * image for show
     */
    public RVSAsyncImage getImage();

    /**
     * official show site URL
     */
    public String getOfficialUrl();
    
    /**
     *  The channel owner of the show
     */
    public RVSChannel getChannel();

    /**
     *  Next (or current if live) program
     */
    public RVSProgram getNextProgram();
    
    /**
     *  show episode count
     */
    public int getEpisodesCount();

}
