package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/17/14.
 */
public interface RVSSearchContentSuggestionResult {

    /**
     * The search text
     */
    public String getSearchText();
}
