package com.whipclip.rvs.sdk.dataObjects;

public interface RVSEndCard {

	/**
	 * EndCard ID
	 */
	public String getEndCardId();

	/**
	 * Tune Information
	 */
	public String getTuneInInformation();

	/**
	 * Link URL
	 */
	public String getLinkUrl();

	/**
	 * Link Description
	 */
	public String getLinkDescription();
	
	/**
	 *  Background Image
	 */
	public RVSAsyncImage getImage();

	
}
