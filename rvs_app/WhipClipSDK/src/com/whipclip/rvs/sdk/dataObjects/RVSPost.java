package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 7/16/14.
 */
public interface RVSPost {

    /**
     * Post ID
     */
    public String getPostId();

    /**
     * Post text
     */
    public String getText();

    /**
     * Post author user
     */
    public RVSUser getAuthor();

    /**
     * Time posted in milliseconds since 1970
     */
    public long getCreationTime();

    /**
     * Channel the post is about
     */
    public RVSChannel getChannel();

    /**
     * Program the post is about
     */
    public RVSProgram getProgram();

    /**
     * Post clip
     */
    public RVSMediaContext getMediaContext();

    /**
     * Post cover image
     */
    public RVSAsyncImage getCoverImage();

    /**
     * YES if post is favorited by me
     */
    public int getLikedByCaller();

    /**
     * YES if post is reposted by me
     */
    public int getRepostedByCaller();

    /**
     * Favorite count
     */
    public int getLikeCount();

    /**
     * Repost count
     */
    public int getRepostCount();

    /**
     * Reposting refer if a repost
     */
    public RVSReferringUser getRepostingUser();

    /**
     * Referring user if returned due to a comment
     */
    public RVSReferringUser getCommentingUser();
   
    /**
     * the number of comments made on this post
     */
    public int getCommentCount();

    /**
     * the latest comments on this post
     */
    public List<RVSComment> getLatestComments();
    
    /**
     *  did the logged in user comment on this post
     */
    public boolean isCommentedByCaller();
    
    /**
     * @return post is spoiler 
     */
    public Boolean isSpoiler();
    
    /**
     * @return post is inappropriate 
     */
    public Boolean isInappropriate();
}
