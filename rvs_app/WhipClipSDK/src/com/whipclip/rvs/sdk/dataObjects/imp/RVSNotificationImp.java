package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSCommentEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSFollowEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSLikeEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSNotification;
import com.whipclip.rvs.sdk.dataObjects.RVSRepostEvent;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSNotificationImp implements RVSNotification {

    private RVSLikeEventImp likeEvent;
    private RVSCommentEventImp commentEvent;
    private RVSFollowEventImp followEvent;
    private RVSRepostEventImp repostEvent;
    private Number timestamp;
    
    @Override
    public long getTimestamp() {
        return timestamp.longValue();
    }

    @Override
    public RVSLikeEvent getLikeEvent() {
        return likeEvent;
    }

    @Override
    public RVSFollowEvent getFollowEvent() {
        return followEvent;
    }

    @Override
    public RVSCommentEvent getCommentEvent() {
        return commentEvent;
    }

    @Override
    public RVSRepostEvent getRepostEvent() {
        return repostEvent;
    }

    // JSON MAPPER
    private void setTimestamp(Number timestamp) { this.timestamp = timestamp; }
    private void setLikeEvent(RVSLikeEventImp likeEvent) { this.likeEvent = likeEvent; }
    private void setFollowEvent(RVSFollowEventImp followEvent) { this.followEvent = followEvent; }
    private void setCommentEvent(RVSCommentEventImp commentEvent) { this.commentEvent = commentEvent; }
    private void setRepostEvent(RVSRepostEventImp repostEvent) { this.repostEvent = repostEvent; }
}
