package com.whipclip.rvs.sdk.dataObjects.imp.helpers;


/**
 * Created by iliya on 6/24/14.
 */
public class Images{
    private Number height;
    private String url;
    private Number width;

    public Number getHeight(){
        return this.height;
    }
    public void setHeight(Number height){
        this.height = height;
    }
    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public Number getWidth(){
        return this.width;
    }
    public void setWidth(Number width){
        this.width = width;
    }
}
