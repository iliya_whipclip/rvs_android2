package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSCommentEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSCommentEventImp implements RVSCommentEvent {

    RVSCommentImp comment;
    RVSPostImp post;
    
    @Override
    public RVSComment getComment() {
        return comment;
    }

    @Override
    public RVSPost getPost() {
        return post;
    }

    // JSON MAPPER
    private void setComment(RVSCommentImp comment) { this.comment = comment; }
    private void setPost(RVSPostImp post) { this.post = post; }
}
