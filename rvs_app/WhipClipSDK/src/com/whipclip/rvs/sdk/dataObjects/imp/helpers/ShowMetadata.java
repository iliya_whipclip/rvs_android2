package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class ShowMetadata {
    private List castMembers;
    private List images;
    private String name;
    private String showId;
    private String synopsis;

    public List getCastMembers(){
        return this.castMembers;
    }
    public void setCastMembers(List castMembers){
        this.castMembers = castMembers;
    }
    public List getImages(){
        return this.images;
    }
    public void setImages(List images){
        this.images = images;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getShowId(){
        return this.showId;
    }
    public void setShowId(String showId){
        this.showId = showId;
    }
    public String getSynopsis(){
        return this.synopsis;
    }
    public void setSynopsis(String synopsis){
        this.synopsis = synopsis;
    }
}
