package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public interface RVSSearchMatch {
    /**
     * The field name that the highlight resides
     */
    public String getFieldName();

    /**
     * An excerpt for the highlight
     */
    public String getValueFragment();

    /**
     * An array of RVSFragmentRange
     */
    public List getFragmentRanges();

    /**
     * Applies only when the fieldName is captions, it holds the start time for where the match starts
     */
    public long getReferenceTime();

    /**
     * Match media context
     */
    public RVSMediaContext getMediaContext();

    /*!
     Post cover image
     */
    public RVSAsyncImage getCoverImage();

}
