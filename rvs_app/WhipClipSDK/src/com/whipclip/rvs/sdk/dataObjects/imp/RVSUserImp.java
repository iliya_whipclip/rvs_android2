package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUserProfile;

/**
 * Created by iliya on 6/10/14.
 * Package private clase (visible only in sdk)
 *
 * jakson
 * http://wiki.fasterxml.com/JacksonInFiveMinutes
 * http://wiki.fasterxml.com/JacksonDocumentation
 *
 *
 * JSON Type    |   Java Type
 * ----------------------------------------------
 * object       |   LinkedHashMap<String,Object>
 * array        |   ArrayList<Object>
 * string       |   String
 * number       |   Integer, Long or BigInteger (smallest applicable)
 * number       |   Double (configurable to use BigDecimal)
 * true|false   |   Boolean
 * null         |   null
 */
public class RVSUserImp implements RVSUser {

    private boolean followedByCaller;
    private Number followerCount; // optional
    private Number followingCount;
    private Number postCount;
    private Number likeCount;
    private Number repostCount;

    private RVSUserProfile mUserProfile;
    private RVSAsyncImage profileImage;
	private String handle;
	private String displayHandle;

    @Override
    public String getUserId() { return mUserProfile.getId(); }

    @Override
    public String getName() {
        return mUserProfile.getName();
    }

    @Override
    public boolean isVerified() { return mUserProfile.isVerified(); }

    @Override 
    public boolean isEditor() { return mUserProfile.isEditor(); }
    
    @Override
    public RVSAsyncImage getPofileImage() {
        if (profileImage != null)
            return profileImage;

        if (mUserProfile.getImages() != null)
        {
            profileImage = new RVSImageFromUrlOrArray(mUserProfile.getImages());
            return profileImage;
        }

        return null;
    }

    @Override
    public boolean isFollowedByMe() {  return this.followedByCaller; }

    @Override
    public int getFollowerCount() { return this.followerCount == null? 0 : this.followerCount.intValue(); }

    @Override
    public int getFollowingCount() {
        return this.followingCount == null ? 0 : this.followingCount.intValue();
    }

    @Override  
    public int getPostCount() { return this.postCount == null ? 0 : this.postCount.intValue(); }

    @Override
    public int getLikeCount() { return this.likeCount == null ? 0 : this.likeCount.intValue(); }

    @Override
    public int getRepostCount() { return this.repostCount == null ? 0 : this.repostCount.intValue(); }
    
    @Override
	public String getHandle() { return mUserProfile.getHandle(); }

	@Override
	public String getDisplayHandle() { return mUserProfile.getHandle() == null ? "" : "@" + mUserProfile.getHandle();  }

    /**
     * JSON MAPPER
     */
    void setUserProfile(RVSUserProfile n) { mUserProfile = n; }
    void setFollowedByCaller(boolean followedByCaller) { this.followedByCaller = followedByCaller; }
    void setFollowerCount(int followerCount) { this.followerCount = followerCount; }
    void setFollowingCount(int followingCount) { this.followingCount = followingCount; }
    void setPostCount(int postCount) { this.postCount = postCount; }
    void setLikeCount(int likeCount) { this.likeCount = likeCount; }
    void setRepostCount(int repostCount) { this.repostCount = repostCount; }

	

}
