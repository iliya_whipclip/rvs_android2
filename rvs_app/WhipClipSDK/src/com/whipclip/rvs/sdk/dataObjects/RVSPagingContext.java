package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSPagingContext {

    private String forwardCookie;
    public String getForwardCookie() { return forwardCookie; }
    public void setForwardCookie(String forwardCookie) { this.forwardCookie = forwardCookie; }
}
