package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

/**
 * Created by iliya on 7/23/14.
 */
public class RVSCommentImp implements RVSComment{

    private String id;
    private String message;
    private RVSUser author;
    private Number creationTime;


    @Override
    public String getCommentId() {
        return id;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public RVSUser getAuthor() {
        return author;
    }

    @Override
    public long getCreationTime() {
        return creationTime.longValue();
    }

    // JSON MAPPER
    private void setId(String id) { this.id = id; }
    private void setMessage(String message) { this.message = message; }
    private void setAuthor(RVSUserImp author){
        this.author = author;
    }
    private void setCreationTime(Number creationTime){
        this.creationTime = creationTime;
    }

}
