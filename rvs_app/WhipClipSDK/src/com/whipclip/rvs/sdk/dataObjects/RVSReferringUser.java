package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/16/14.
 */
public interface RVSReferringUser {

    /**
     * Post referring user
     */
    public RVSUser getUser();

    /**
     * Time re-posted in milliseconds since 1970
     */
    public long getTimestamp();
}
