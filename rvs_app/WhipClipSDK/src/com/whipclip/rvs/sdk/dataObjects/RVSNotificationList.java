package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSNotificationImp;

import java.util.List;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSNotificationList extends RVSPagingListBase {

    private List<RVSNotificationImp> notifications;

    public List<RVSNotificationImp> getNotifications() {
        return notifications;
    }

    /**
     * Used via reflection
     * @param notifications
     */
    private void setNotifications(List<RVSNotificationImp> notifications) {
        this.notifications = notifications;
    }
}