package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.RVSPromise;

/**
 * Created by iliya on 6/23/14.
 */
public interface RVSAsyncList {
    /**
     * Get more object
     * The returned promise is retained by the async list. To cancel the request just release the async list
     * @param count desired for number of posts to receive, actual received number can be different
     * @return promise for NSArray
     **/
    public RVSPromise next(int count);

    /**
     *  Flag if list reached its end
     */
    public boolean getDidReachEnd();
    public void setDidReachEnd(boolean reachEnd);

}
