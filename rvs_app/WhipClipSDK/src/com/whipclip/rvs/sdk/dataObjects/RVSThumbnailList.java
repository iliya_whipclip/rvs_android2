package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSThumbnailImp;

import java.util.List;

/**
 * Created by iliya on 6/30/14.
 */
public class RVSThumbnailList extends RVSPagingListBase{

   private List<RVSThumbnailImp> images;

   public List<RVSThumbnail> getImages() { return (List<RVSThumbnail>)(List<?>)images; }
   public void setImages(List<RVSThumbnailImp> images) { this.images = images; }

}
