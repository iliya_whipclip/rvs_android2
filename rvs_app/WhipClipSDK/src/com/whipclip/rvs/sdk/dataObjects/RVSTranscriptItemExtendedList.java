package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSTranscriptItemImp;

public class RVSTranscriptItemExtendedList extends RVSPagingListBase {

	   private List<RVSTranscriptItemImp> transcriptItems;

	   public List<RVSTranscriptItem> getTranscriptItems() { return (List<RVSTranscriptItem>)(List<?>)transcriptItems; }
	   public void setTranscriptItems(List<RVSTranscriptItemImp> transcriptItems) { this.transcriptItems = transcriptItems; }

}
