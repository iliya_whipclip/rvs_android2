package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSFollowEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSFollowEventImp implements RVSFollowEvent{

    private RVSUserImp user;

    @Override
    public RVSUser getUser() {
        return user;
    }

    // JSON MAPPER
    private void setUser(RVSUserImp user) { this.user = user; }
}
