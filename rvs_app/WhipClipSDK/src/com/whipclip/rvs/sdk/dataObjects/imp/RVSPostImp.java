package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSCoverImage;

import java.util.List;

public class RVSPostImp implements RVSPost {

    private RVSUserImp author;
    private RVSChannelImp channel;
    private Number commentCount;
    private RVSAsyncImage coverImage;
    private Number creationTime;
    private String id;
    private List<RVSCommentImp> latestComments;
    private Number likeCount;
    private boolean likedByCaller;
    private List media;
    private RVSMediaContextImp mediaContext;
    private String message;
    private RVSProgramImp program;
    private Number repostCount;
    private boolean repostedByCaller;
    private RVSCoverImage postCoverImage;
    private RVSReferringUserImp repostingUser;
    private RVSReferringUserImp commentingUser;
    private Boolean isSpoiler;
    private Boolean isInappropriate;
    private boolean commentedByCaller;

    @Override
    public String getPostId() {
        return id;
    }

    @Override
    public String getText() {
        return message;
    }

    @Override
    public RVSUser getAuthor() {
        return this.author;
    }

    @Override
    public long getCreationTime() {
        return creationTime.longValue();
    }

    @Override
    public RVSChannel getChannel() {
        return this.channel;
    }

    @Override
    public RVSProgram getProgram() {
        return this.program;
    }

    @Override
    public RVSMediaContext getMediaContext() {
        return mediaContext;
    }

    @Override
    public RVSAsyncImage getCoverImage() {
        if (coverImage != null)
            return coverImage;

        if (postCoverImage != null)
        {
            coverImage = new RVSImageFromUrlOrArray(postCoverImage.getImages());
            return coverImage;
        }

        return null;
    }

    @Override
    public int getLikedByCaller() {
        return likedByCaller ? 1 : 0;
    }

    @Override
    public int getRepostedByCaller() {
        return repostedByCaller ? 1 : 0;
    }

    @Override
    public int getLikeCount() {
        return likeCount.intValue();
    }

    @Override
    public int getRepostCount() {
        return repostCount.intValue();
    }

    @Override
    public RVSReferringUser getRepostingUser() {
        return repostingUser;
    }
    
	@Override
	public RVSReferringUser getCommentingUser() {
		return commentingUser;
	}


    @Override
    public int getCommentCount() {
        return commentCount.intValue();
    }

    @Override
    public List<RVSComment> getLatestComments() {
        return (List<RVSComment>)(List<?>)latestComments;
    }
    
	@Override
	public Boolean isSpoiler() {
		return isSpoiler;
	}

	@Override
	public Boolean isInappropriate() {
		return isInappropriate;
	}
	
	@Override
	public boolean isCommentedByCaller() {
		return commentedByCaller;
	}


    /**
     * JSON MAPPER
     **/
    private void setAuthor(RVSUserImp author){ this.author = author; }
    private void setChannel(RVSChannelImp channel){ this.channel = channel; }
    private void setCommentCount(int commentCount){ this.commentCount = commentCount; }
    private void setCoverImage(RVSCoverImage coverImage){ this.postCoverImage = coverImage; }
    private void setCreationTime(Number creationTime){ this.creationTime = creationTime; }
    private void setId(String id){ this.id = id; }
    private void setLatestComments(List<RVSCommentImp> latestComments){ this.latestComments = latestComments; }
    private void setLikeCount(int likeCount){ this.likeCount = likeCount; }
    private void setLikedByCaller(boolean likedByCaller){ this.likedByCaller = likedByCaller; }
    private List getMedia(){ return this.media; }
    private void setMedia(List media){ this.media = media; }
    private void setMediaContext(RVSMediaContextImp mediaContext){ this.mediaContext = mediaContext; }
    private void setMessage(String message){ this.message = message; }
    private void setProgram(RVSProgramImp program){ this.program = program; }
    private void setRepostCount(Number repostCount){ this.repostCount = repostCount; }
    private void setRepostedByCaller(boolean repostedByCaller){ this.repostedByCaller = repostedByCaller; }
    private void setIsSpoiler(Boolean isSpoiler){ this.isSpoiler = isSpoiler; }
    private void setIsInappropriate(Boolean isInappropriate){ this.isInappropriate = isInappropriate; }
    private void setCommentedByCaller(boolean commentedByCaller){ this.commentedByCaller = commentedByCaller; }
    private void setCommentingUser(RVSReferringUserImp commentingUser) { this.commentingUser = commentingUser; }
    private void setReportingUser(RVSReferringUserImp repostingUser) { this.repostingUser = repostingUser; }

}
