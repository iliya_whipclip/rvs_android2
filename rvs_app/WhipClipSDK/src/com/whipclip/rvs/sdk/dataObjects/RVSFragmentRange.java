package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSFragmentRange {

    private Number matchCharacterOffset;
    private Number matchCharacterCount;

    public long getMatchCharacterOffset() {
        return matchCharacterOffset.longValue();
    }

    public long getMatchCharacterCount() {
        return matchCharacterCount.longValue();
    }

    // JSON MAPPER
    private void setMatchCharacterOffset(Number matchCharacterOffset) {
        this.matchCharacterOffset = matchCharacterOffset;
    }

    private void setMatchCharacterCount(Number matchCharacterCount) {
        this.matchCharacterCount = matchCharacterCount;
    }



}
