package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSChannelImp implements RVSChannel{

    private String channelId;
    private String name;
    private RVSAsyncImage logo;
    private RVSAsyncImage watermarkLogo;

    private String callSign;
    private RVSProgramImp currentProgram;
    private String officialUrl;
    private String preRollUrl;

    private String akamaiUrl;
    private List<RVSImage> logoImages;
    private List media;
    private List<RVSImage> watermarkLogoImages;

    public String getAkamaiUrl(){
        return this.akamaiUrl;
    }
    public void setAkamaiUrl(String akamaiUrl){
        this.akamaiUrl = akamaiUrl;
    }
    public void setCallSign(String callSign){
        this.callSign = callSign;
    }
    public void setOfficialUrl(String officialUrl) { this.officialUrl = officialUrl; }
    public void setPreRollUrl(String preRollUrl) { this.preRollUrl = preRollUrl; }
    public void setCurrentProgram(RVSProgramImp currentProgram) { this.currentProgram = currentProgram; }
    public void setId(String id){
        this.channelId = id;
    }
    public void setImages(List<RVSImage> images){
        this.logoImages = images;
    }
    public List getMedia(){
        return this.media;
    }
    public void setMedia(List media){
        this.media = media;
    }
    public void setName(String name){ this.name = name; }
    public void setWatermarkImages(List<RVSImage> watermarkImages){ this.watermarkLogoImages = watermarkImages; }

    @Override
    public String getCallSign(){
        return this.callSign;
    }

    @Override
    public RVSProgram getCurrentProgram() {
        return currentProgram;
    }

    @Override
    public String getPreRollUrl() {
        return this.preRollUrl;
    }

    @Override
    public String getChannelId() {
        return this.channelId;
    }

    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public RVSAsyncImage getLogo() {

        if (logo != null)
            return logo;

        if (logoImages != null)
        {
            logo = new RVSImageFromUrlOrArray(logoImages);
            return logo;
        }

        return null;
    }

    @Override
    public RVSAsyncImage getWatermarkLogo() {

        if (watermarkLogo != null)
            return watermarkLogo;

        if (watermarkLogoImages != null)
        {
            watermarkLogo = new RVSImageFromUrlOrArray(watermarkLogoImages);
            return watermarkLogo;
        }

        return null;
    }

    @Override
    public String getOfficialUrl() {
        return officialUrl;
    }
}
