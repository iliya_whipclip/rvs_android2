package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnail;

import java.util.List;

/**
 * Created by iliya on 7/30/14.
 */
public class RVSThumbnailImp implements RVSThumbnail {

    private String id;
    private Number startOffset;
    private List<RVSImage> images;
    private RVSAsyncImage asyncImage;

    @Override
    public String getThumbnailId() {
        return id;
    }

    @Override
    public long getStartOffset() {
        return startOffset.longValue();
    }

    @Override
    public RVSAsyncImage getAsyncImage() {
        if (asyncImage != null)
            return asyncImage;

        if (images != null)
        {
            asyncImage = new RVSImageFromUrlOrArray(images);
            return asyncImage;
        }

        return null;
    }

    // JSON MAPPER
    public void setId(String id) { this.id = id; }
    public void setStartOffset(Number startOffset) { this.startOffset = startOffset; }
    public void setRepresentations(List<RVSImage> representations){ this.images = representations; }
}
