package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSCoverImage;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSProgramExcerptImp implements RVSProgramExcerpt {

    private RVSChannelImp channel;
    private RVSProgramImp program;
    private RVSMediaContextImp mediaContext;
    private String transcript;
    private RVSAsyncImage coverImage;
    private RVSCoverImage postCoverImage;

    @Override
    public RVSChannel getChannel() {
        return channel;
    }

    @Override
    public RVSProgram getProgram() {
        return program;
    }

    @Override
    public RVSMediaContext getMediaContext() {
        return mediaContext;
    }

    @Override
    public RVSAsyncImage getCoverImage() {
        if (coverImage != null)
            return coverImage;

        if (postCoverImage != null)
        {
            coverImage = new RVSImageFromUrlOrArray(postCoverImage.getImages());
            return coverImage;
        }

        return null;
    }

    @Override
    public String getTranscript() {
        return transcript;
    }

    // JSON MAPPER
    private void setCoverImage(RVSCoverImage coverImage){ this.postCoverImage = coverImage; }
    private void setChannel(RVSChannelImp channel) { this.channel = channel; }
    private void setProgram(RVSProgramImp program) { this.program = program; }
    private void setMediaContext(RVSMediaContextImp mediaContext) { this.mediaContext = mediaContext; }
    private void setTranscript(String transcript) { this.transcript = transcript; }
}
