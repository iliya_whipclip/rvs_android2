package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSImage;

import java.util.ArrayList;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSUserProfile {
	
    protected String id;
    protected String name;
    protected String handle;
    protected Boolean isVerified;
    protected Boolean isEditor;
    
    private ArrayList<RVSImage> images = new ArrayList<RVSImage>();

    void setId(String id) { this.id = id; }
    void setName(String name) { this.name = name; }
    void setHandle(String handle) { this.handle = handle; }
    void setVerified(Boolean isVerified) { this.isVerified = isVerified; }
    void setIsEditor(Boolean isEditor) { this.isEditor = isEditor; }
    void setImages(ArrayList<RVSImage> images) { this.images = images; }

    public String getId() { return id; }
    public String getName() { return name; }
    public String getHandle() { return handle; }
    public boolean isVerified() { return isVerified; }
    public ArrayList<RVSImage> getImages() { return images; }
	public boolean isEditor() { return isEditor == null ? false : isEditor; }
}