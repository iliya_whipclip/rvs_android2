package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSShowImp;

public class RVSShowList extends RVSPagingListBase {

    private List<RVSShowImp> shows;

    public List<RVSShowImp> getShows() {
        return shows;
    }

    /**
     * Used via reflection
     * @param shows
     */
    private void setShows(List<RVSShowImp> shows) {
        this.shows = shows;
    }
}
