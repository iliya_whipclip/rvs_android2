package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/17/14.
 */
public interface RVSSearchChannelSuggestionResult {

    /**
     * the channel name suggestion
     */
    public String getChannelName();

}
