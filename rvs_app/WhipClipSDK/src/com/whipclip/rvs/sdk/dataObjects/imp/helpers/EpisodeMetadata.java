package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class EpisodeMetadata {
    private List<CastMembers> castMembers;
    private Number episodeNumber;
    private List images;
    private String name;
    private Number seasonNumber;
    private String synopsis;

    public List getCastMembers(){
        return this.castMembers;
    }
    public void setCastMembers(List castMembers){
        this.castMembers = castMembers;
    }
    public Number getEpisodeNumber(){
        return this.episodeNumber;
    }
    public void setEpisodeNumber(Number episodeNumber){
        this.episodeNumber = episodeNumber;
    }
    public List getImages(){
        return this.images;
    }
    public void setImages(List images){
        this.images = images;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Number getSeasonNumber(){
        return this.seasonNumber;
    }
    public void setSeasonNumber(Number seasonNumber){
        this.seasonNumber = seasonNumber;
    }
    public String getSynopsis(){
        return this.synopsis;
    }
    public void setSynopsis(String synopsis){
        this.synopsis = synopsis;
    }
}
