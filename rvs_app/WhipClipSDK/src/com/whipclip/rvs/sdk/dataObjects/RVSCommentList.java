package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;
import java.util.List;

/**
 * Created by iliya on 7/23/14.
 */
public class RVSCommentList extends RVSPagingListBase {

    private List<RVSCommentImp> comments;

    public List<RVSCommentImp> getComments() {
        return comments;
    }

    /**
     * Used via reflection
     * @param comments
     */
    private void setComments(List<RVSCommentImp> comments) {
        this.comments = comments;
    }
}
