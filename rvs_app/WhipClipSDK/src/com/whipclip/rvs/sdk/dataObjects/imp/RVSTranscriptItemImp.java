package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSTranscriptItem;

public class RVSTranscriptItemImp implements RVSTranscriptItem{

	private Number endTime;
	private Number startTime;
	private String text;
	
	@Override
	public long getStartTime() {
		return startTime.longValue();
	}

	@Override
	public long getEndTime() {
		return endTime.longValue();
	}

	@Override
	public String getText() {
		return text;
	}
	
	// JSON MAPPER
	private void setEndTime(Number endTime) { this.endTime = endTime; }
	private void setStartTime(Number startTime) { this.startTime = startTime; }
	private void setText(String text) { this.text = text; }
	

}
