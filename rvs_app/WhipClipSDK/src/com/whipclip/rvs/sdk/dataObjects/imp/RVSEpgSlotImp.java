package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSEpgSlot;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSCoverImage;

import java.util.List;

public class RVSEpgSlotImp implements RVSEpgSlot {

    private RVSChannelImp channel;
	private RVSProgramImp program;
	private RVSShowImp show;

    @Override
    public RVSChannel getChannel() {
        return this.channel;
    }

    @Override
    public RVSProgram getProgram() {
        return this.program;
    }
    
    @Override
	public RVSShow getShow() {
		return this.show;
	}

    /**
     * JSON MAPPER
     **/
    private void setChannel(RVSChannelImp channel){ this.channel = channel; }
    private void setProgram(RVSProgramImp program){ this.program = program; }
    private void setShow(RVSShowImp show){ this.show = show; }

	

}
