package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSSearchContentSuggestionResult;

/**
 * Created by iliya on 8/17/14.
 */
public class RVSSearchContentSuggestionResultImp implements RVSSearchContentSuggestionResult {

    private String searchText;

    @Override
    public String getSearchText() { return searchText;  }

    // JSON MAPPER
    private void setSearchText(String searchText) { this.searchText = searchText; }

}
