package com.whipclip.rvs.sdk.dataObjects;

public interface RVSEpgSlot {

	/**
	 *  Program
	 */
	public RVSProgram getProgram();

	/**
	 *  Show
	 */
	public RVSShow getShow();

	/**
	 *  Channel
	 */
	public RVSChannel getChannel();
	
}
