package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSChannelSearchResultImp;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSChannelSearchResults extends RVSPagingListBase {
    private List<RVSChannelSearchResultImp> results;

    /**
     * Used via reflection
     * @param results
     */
    private void setResults(List<RVSChannelSearchResultImp> results) {
        this.results = results;
    }
}
