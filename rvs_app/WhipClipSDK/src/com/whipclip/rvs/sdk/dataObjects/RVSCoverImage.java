package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSCoverImage {
    private List<RVSImage> images;

    public List<RVSImage> getImages(){
        return this.images;
    }
    public void setRepresentations(List<RVSImage> representations){ this.images = representations; }
}
