package com.whipclip.rvs.sdk.dataObjects;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.MediaController;
import android.widget.RelativeLayout;

import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSErrorCode;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.RVSPromise.RVSPromiseExecutionScope;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSMediaContextImp;
import com.whipclip.rvs.sdk.util.Utils;

import org.apache.http.client.HttpResponseException;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.Util;

/**
 * Created by iliya on 6/29/14.
 */
public class RVSClipView extends RelativeLayout implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, RVSVideoView.OnVideoViewListener {

    private Logger logger = LoggerFactory.getLogger(RVSClipView.class);

    private RelativeLayout videoViewParent;
    private RVSVideoView videoView;

    private boolean shouldPlayPreRoll;
    private boolean notifiedPlaybackStart;
    private RVSMediaContext mediaContext;
    private long startOffset;
    private long duration;
    private boolean isScrubbing;
    
    public enum State {
        UNKNOWN,
        LOADING,
        READY,
        PLAYING,
        PAUSED,
        PLAYBACK_ENDED,
        FAILED,
    }
    
    public enum FailReason {
    	// Internal Error
    	INTERNAL_ERROR,
        // Playback restricted by service
        REASON_RESTRICTED,
        // Media suppresssed at channel level
        SUPPERESSED_CHANNEL,
        // Media suppressed at show level
        SUPPERESED_PART_OF_SHOW,
        // Media suppressed for entire show
        SUPPERESSED_ENTIRE_SHOW,
        // Unknown reason
        UNKNOWN

    }

    public interface RVSClipViewStateChangedCallback {
        /**
         * Called when clip playback state changed
         * @param clipView clip view for which the state was changed
         */
        public void onClipViewStateDidChange(RVSClipView clipView);
        /**
         *  Called when player head changes position, post it to UI thread
         *
         *  @param clipView    clip view object
         *  @param currentTime current time
         *  @param duration    duration of playing item
         */
        public void onDidProgressWithCurrentDuration(RVSClipView clipView, int currentTime, long duration);
        /**
         *  Called when buffering state changes
         *
         *  @param clipView    clip view object
         *  @param isBuffering is buffering flag
         */
        void onBufferingStateChanged(RVSClipView clipView, boolean isBuffering);
    }

    /**
     * Playback state
     **/
    private State state;

    /**
     * Buffering state
     */
    private boolean isBuffering;

    /**
     *  Playing state
     */
//    private boolean isPlaying;

    /**
     * Toggle fullscreen mode, affects background view, video view and overlay view
     **/
    private boolean fullscreen;

    /**
     *  A customizable view that is displayed behind the movie content
     **/
    private View backgroundView;

    /**
     *  A customizable transparent view that is displayed above the movie content
     */
    private View overlayView;

    RVSClipViewStateChangedCallback  stateChangedListener;

	private boolean wasPlayingBeforeScrubbing;

	private String contentUrl;

	// 
	private RVSPromise<FailReason, Throwable, FailReason> failReason;

   	public RVSClipView(Context context) {

        super(context);

        Utils.assertCalledNotFromMainThread();
        initRVSClipView();
    }

    public RVSClipView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Utils.assertCalledNotFromMainThread();
        initRVSClipView();
    }

    public RVSClipView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        Utils.assertCalledNotFromMainThread();
        initRVSClipView();
    }

    public RVSClipViewStateChangedCallback getStateChangedListener() {
        return stateChangedListener;
    }

    public void setStateChangedListener(RVSClipViewStateChangedCallback stateChangedListener) {
        this.stateChangedListener = stateChangedListener;
    }

    /**
     * Play clip
     **/
    public void play() {
        Utils.assertCalledNotFromMainThread();
        Utils.assertObjectNotValid(mediaContext);

        if (state == State.PLAYBACK_ENDED) {
            rewind();
        }

        if (mediaContext.getStatus().equalsIgnoreCase(RVSMediaContext.STATUS_OK)) {
        	startMoviePlayback();
        }
    }

    private void startMoviePlayback() {

        setState(State.LOADING);

        notifiedPlaybackStart = true;

        RVSSdk.sharedRVSSdk().informClipPlayback(true);

        if (videoView == null)
        {
            // create player first time
//            logger.info("Create AVPlayer with contentItem: %s preRollItem: %s", contentItem, preRollItem);

            // todo optimizations
        	if (this.contentUrl != null)
 	        {
 	            createVideoView();
 	            videoView.setVideoPath(this.contentUrl);
 	        }
 	        else
 	        {
 	            videoView = null;
 	        }
        }
//
        setState(State.PLAYING);
        
        videoView.startMoviePlayback();
        //updated beteween clips
        reportProgress();

    }

    private void setState(State state) {

        Utils.assertCalledNotFromMainThread();


        if (this.state == state)
            return;

        logger.info("Player set state: " + state.name());
        this.state = state;

        if (state == State.UNKNOWN || state == State.PLAYBACK_ENDED)
        {
            if (notifiedPlaybackStart)
            {
                notifiedPlaybackStart = false;
                RVSSdk.sharedRVSSdk().informClipPlayback(false);
            }
        }

        if (state == State.LOADING)
        {
            isBuffering = true;
        }
        else
        {
            isBuffering = false;
        }

        if (state == State.PLAYING)
        {
        	mediaContext.reportViewWithStartOffset(this.startOffset, this.duration);
            //addProgressTimeObserver();
        }
        else
        {
//            this removeProgressTimeObserver];
        }

        if (state == State.PAUSED) {
        	
        	if (videoView != null) {
                videoView.pause();
            }
        }
        
        //failed?
        if (state == State.FAILED)
        {   
        	if (videoView != null) {
        		videoView.destroy();
        	}
            setFailReason();
        }

        if (stateChangedListener != null ) {
            stateChangedListener.onClipViewStateDidChange(this);
        }
    }

   /* remove 
    private void updateFailPromise() {
		RVSPromise<FailReason, FailReason, FailReason> reasonPromise = getFailReason();
		reasonPromise.done(new AndroidDoneCallback<RVSClipView.FailReason>()
		{
			
			@Override
			public void onDone(FailReason result)
			{				
				logger.error(result.name());
			}

			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}
		}).fail(new AndroidFailCallback<RVSClipView.FailReason>() {

			@Override
			public void onFail(FailReason result)
			{
				logger.error(result.name());
			}

			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}
			
		});
		
	}
	*/

	public RVSPromise<FailReason, Throwable, FailReason> getFailReason() {
    	return failReason;
    }
    
    @SuppressWarnings("unchecked")
    private void setFailReason() {
    	
    	failReason = RVSPromise.createPromise(RVSPromiseExecutionScope.UI);
    	
    	if (this.contentUrl != null)
	    {
	        RVSPromise request = RVSSdk.sharedRVSSdk().requestManager().getURL(this.contentUrl,null);
	        final RVSPromise weakFailReason = this.failReason;
	        request.done(new DoneCallback<String>() {
	        	
	        	@Override
	        	public void onDone(String response){
	        		logger.info("response: "+ response);
	        		
    	            //got playlist - internal error
	        		weakFailReason.resolve(FailReason.INTERNAL_ERROR);
	        	}
			}).fail(new FailCallback<Throwable>() {
				
				@Override
				public void onFail(Throwable error) {
					
					if (error instanceof RVSError) {
						if (((RVSError) error).getErrorCode().equals("PROGRAM_VIEW_LIMIT_REACHED")) {
							weakFailReason.resolve(FailReason.REASON_RESTRICTED);
						}
					} else if (error instanceof HttpResponseException){
							logger.error("status code: " + ((HttpResponseException) error).getStatusCode());
							logger.error("error message: " + ((HttpResponseException) error).getMessage());
							weakFailReason.resolve(FailReason.INTERNAL_ERROR);
					} else {
						
						if (error instanceof RVSError) {
							if (((RVSError) error).getErrorCode().equals("PROGRAM_VIEW_LIMIT_REACHED")) {
								weakFailReason.resolve(FailReason.REASON_RESTRICTED);
							}
						} else {
							weakFailReason.resolve(FailReason.UNKNOWN);
						}
					
//						weakFailReason.resolve(FailReason.INTERNAL_ERROR);
					}
				}
			
			});
	    }
	    else if (this.mediaContext.getStatus().compareTo(RVSMediaContextImp.STATUS_SUPPRESSED_ENTIRE_SHOW) == 0)
	    {
	        this.failReason.resolve(FailReason.SUPPERESSED_ENTIRE_SHOW);
	    }
	    else if (this.mediaContext.getStatus().compareTo(RVSMediaContextImp.STATUS_SUPPRESSED_PART_OF_SHOW) == 0)
	    {
	        this.failReason.resolve(FailReason.SUPPERESED_PART_OF_SHOW);
	    }
	    else if (this.mediaContext.getStatus().compareTo(RVSMediaContextImp.STATUS_SUPPRESSED_CHANNEL) == 0)
	    {
	        this.failReason.resolve(FailReason.SUPPERESSED_CHANNEL);
	    }
	    else
	    {
	        this.failReason.resolve(FailReason.UNKNOWN);
	    }
	}

	public State getState() {
        return state;
    }

    private void createVideoView() {
    	if (videoView == null) {
	        videoView = new RVSVideoView(getContext(), null, 0);
	        videoView.setOnErrorListener(this);
	        videoView.setOnCompletionListener(this);
	        videoView.setOnVideoViewListener(this);
    	

	        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	        videoView.setLayoutParams(layoutParams);
	        videoViewParent.addView(videoView);
	        videoView.setScalingMode(RVSVideoView.RVScalingMode.RVScalingModeFill);
    	}

    }

    /**
     * Used for test only, don't call this method, will be deprecated later
     */
    public void addMediaController() {
        videoView.setMediaController(new MediaController(getContext()));
    }

    private void reportProgress() {

//    	Utils.assertCalledNotFromMainThread();
    	
        if (stateChangedListener != null && videoView != null)
        {
            long currentPosition = videoView.getCurrentPosition();
            logger.info("reportProgress currentPosition/duration " + currentPosition + "/" + this.duration);
            
            
            if (Looper.getMainLooper() != Looper.myLooper()) {
            	getHandler().post(new Runnable() {
            		@Override
            		public void run() {
            			 if (stateChangedListener != null && videoView != null)
            		     {
            				 stateChangedListener.onDidProgressWithCurrentDuration(RVSClipView.this, videoView.getCurrentPosition(), RVSClipView.this.duration);
            		     }
            		}
            	});
            } else {
            	stateChangedListener.onDidProgressWithCurrentDuration(this, videoView.getCurrentPosition(), this.duration);
            }
        
            
        }
    }

    /**
     * Stop playback
     **/
    public void stop() {

        Utils.assertCalledNotFromMainThread();
        // todo
//        this.contentItem.asset cancelLoading];
//        this.preRollItem.asset cancelLoading];
//        self.contentItem = nil;
//        self.preRollItem = nil;

        
        logger.debug("stop called, state: " + this.state);
        
        reportProgress();

        if (videoView != null) {
            videoView.destroy();
            
            videoViewParent.removeView(videoView);
            videoView  = null;
            
//            System.gc();
        }
    }


    /**
     * Pause playback
     **/
    public void pause() {

        Utils.assertCalledNotFromMainThread();

        setState(State.PAUSED);
    }

    /**
     *  Rewind and play
     */
    public void rewind() {
    	if (videoView != null) {
    		videoView.seekTo(0);
    	}
    }

    /**
     *  Set clip to play
     *
     *  @param mediaContext media context to take clip from
     *  @param startOffset  clip start offset within the media context
     *  @param duration     clip duration
     **/
    public void setClipWithMediaContext(RVSMediaContext mediaContext, long startOffset, long duration) {
        setClipWithMediaContext(mediaContext, startOffset, duration, null);
    }

    private void setClipWithMediaContext(RVSMediaContext mediaContext, long startOffset, long duration, String preRollUrl) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertObjectNotValid(mediaContext);

        this.mediaContext = mediaContext;
        this.startOffset = startOffset;
        this.duration = duration;

        if (this.mediaContext.getStatus().compareTo(RVSMediaContextImp.STATUS_OK) != 0)
        {
            this.contentUrl = null;
            setState(State.FAILED);
        }
        else
        {
        	
	       
	//        todo
	//        if (preRollUrl)
	//        {
	//            self.preRollItem = [AVPlayerItem playerItemWithAsset:this preloadingAssetWithURL:preRollUrl]];
	//            self.shouldPlayPreRoll = YES;
	//        }
	//        else
	//        {
	        shouldPlayPreRoll = false;
	//          preRollItem = false;
	//        }
	
	        this.contentUrl = mediaContext.clipUrlWithMediaType(null, startOffset, duration);
//	        this.contentUrl = "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";
	
//	        if (this.contentUrl != null)
//	        {
//	            createVideoView();
//	            videoView.setVideoPath(this.contentUrl);
//	        }
//	        else
//	        {
//	            videoView = null;
//	        }
        }

        notifiedPlaybackStart = false;
    }

    // [+] Scrub
    /**
     *  Hint player that scrubbing will begin
     */
    public void beginScrubbing() {
    	
    	isScrubbing = true;
    	
    	wasPlayingBeforeScrubbing = isPlaying();
        
//        if (state == State.UNKNOWN) {
//            this.play();
//        }
        
        if (wasPlayingBeforeScrubbing) {
        	this.pause();
        }
    }

    /**
     *  Hint player that scrubbing has ended
     */
    public void endScrubbing() {

    	isScrubbing = false;
    	
        if (wasPlayingBeforeScrubbing) {
        	this.play();
        }

    }

    /**
     *  Preform scrubbing
     *  Must called after beginScrubbing and before endScrubbing
     *
     *  @param value    value to scrub to
     *  @param minValue min allowed value
     *  @param maxValue max allowed value
     **/
    public void scrubToValue(double value, double minValue, double maxValue) {
		
    	int time = (int) (this.duration * (value - minValue) / (maxValue - minValue));
    	time = time * 1000;
        
    	 if (state == State.UNKNOWN) 
    	 {
    		 videoView.setInitialPlaybackTime(time);
    	 }
    	 else 
    	 {
        
        	videoView.seekTo(time);
        	if (wasPlayingBeforeScrubbing) {
        		this.play();
        	}
        } 
    }
    
    // [-] Scrub

    private void initRVSClipView() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        backgroundView = new RelativeLayout(getContext());
        backgroundView.setLayoutParams(params);

        videoViewParent = new RelativeLayout(getContext());
        backgroundView.setLayoutParams(params);

        overlayView = new RelativeLayout(getContext());
        backgroundView.setLayoutParams(params);

//        [backgroundView setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [videoView setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [overlayView setTranslatesAutoresizingMaskIntoConstraints:NO];

        addView(backgroundView);
        addView(videoViewParent);
        addView(overlayView);

        subViewsDidMoveToNewParent();

        setState(State.UNKNOWN);
    }

    // todo
    private void subViewsDidMoveToNewParent() {

    }

    public boolean isPlaying() {
    	
        return (state == State.PLAYING);
    }
    
    public boolean isScrubbing() {
    	return isScrubbing;
    }

    public void release() {
    	if (videoView != null) {
    		videoView.destroy();
    	}
    }

    // [+] RVSVideoView listeners
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        logger.info("video complete");
        if (stateChangedListener != null) {
        	 if (Looper.getMainLooper() != Looper.myLooper()) {
             	getHandler().post(new Runnable() {
             		@Override
             		public void run() {
             			stateChangedListener.onClipViewStateDidChange(RVSClipView.this);
             		}
             	});
        	 } else {
        			stateChangedListener.onClipViewStateDidChange(RVSClipView.this);
        	 }
        }
    }

    @Override
    public void onBufferingStateChanged(final boolean isBuffering) {
        logger.info("buffering changed: " + isBuffering);
        if (stateChangedListener != null) {
        	 if (Looper.getMainLooper() != Looper.myLooper()) {
        		 Handler handler = getHandler();
        		 if (handler != null) {
	              	getHandler().post(new Runnable() {
	              		@Override
	              		public void run() {
	              			if (stateChangedListener != null) {
	              				stateChangedListener.onBufferingStateChanged(RVSClipView.this, isBuffering);
	              			}
	              		}
	              	});
        		 }
        	 } else {
       			stateChangedListener.onBufferingStateChanged(this, isBuffering);

        	 }
        }
    }

    @Override
    public void onDidProgressWithCurrentDuration() {
        reportProgress();
    }

	@Override
	public void onStateChanged(int state) {
		 
		switch (state) {
			case RVSVideoView.STATE_PREPARED:
//				setState(State.READY);
				break;
				
			case RVSVideoView.STATE_COMPLETED:
				setState(State.PLAYBACK_ENDED);
				break;
				
			case RVSVideoView.STATE_ERROR:
				setState(State.FAILED);
	            break;

				
		}
		
		
	}

    // [-] RVSVideoView listeners
	
	/**
	 * For tests only
	 */
    public void setVideoPlah(String path, long startOffset, long duration) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertObjectNotValid(mediaContext);

        stop();
//        this.mediaContext = mediaContext;
        this.startOffset = startOffset;
        this.duration = duration;

        shouldPlayPreRoll = false;

        this.contentUrl = path; 
        
        if (this.contentUrl != null)
        {
            createVideoView();
            videoView.setVideoPath(this.contentUrl);
        }
        else
        {
            videoView = null;
        }

        notifiedPlaybackStart = false;
    }

}
