package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSSearchMatchImp;

import java.util.List;

/**
 * Created by iliya on 7/27/14.
 */
public interface RVSContentSearchResult {

    /**
     * Program excerpt found or nil if result is not a program excerpt
     */
    public RVSProgramExcerpt getProgramExcerpt();

    /**
     * Post found or nil if result is not a post
     */
    public RVSPost getPost();

    /**
     * @return a list of search matches
     */
    public List<RVSSearchMatch> getSearchMatches();

}
