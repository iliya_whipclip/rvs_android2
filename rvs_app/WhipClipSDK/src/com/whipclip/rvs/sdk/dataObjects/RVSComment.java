package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/16/14.
 */
public interface RVSComment {

    /**
     * Comment ID
     */
    public String getCommentId();

    /**
     * Comment text
     */
    public String getMessage();

    /**
     * Comment author user
     */
    public RVSUser getAuthor();

    /**
     * Time posted in milliseconds since 1970
     */
    public long getCreationTime();

}
