package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnailList;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.util.Utils;

import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSMediaContextImp implements RVSMediaContext {

    Logger logger = LoggerFactory.getLogger(RVSMediaContextImp.class);

    private String context;
    private String status;
    private Number endTime;
    private Number startTime;
    private Number maxPostTime;
    private String suppressionReason;

    public String getContext(){
        return this.context;
    }

    /**
     * Get partial clip URL for playback using native player
     *
     * @param mediaType   mediaType for the clip, nil for default media type
     * @param startOffset start offset in seconds relative to media context start
     * @param duration    clip duration
     * @return clip URL
     */
    @Override
    public String clipUrlWithMediaType(String mediaType, long startOffset, long duration) {

        Utils.assertCalledNotFromMainThread();

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("mediaContext", context);
        parameters.put("startOffset", String.valueOf(startOffset * 1000));
        parameters.put("endOffset", String.valueOf((startOffset + duration) * 1000));
        parameters.put("deviceId", getDeviceId());

        String myUserId = RVSSdk.sharedRVSSdk().getMyUserId();
        if (myUserId != null && myUserId.length() > 0) {
            parameters.put("userId", myUserId);
        }

        String mediaSource = RVSSdk.sharedRVSSdk().getSharedPreferences().getString("mediaSource", null);
        if (mediaSource != null)
            parameters.put("mediaSource", mediaSource);

        return RVSSdk.sharedRVSSdk().requestManager().completeURLForMethod("media/video/types/hls.m3u8", parameters);

    }

    /**
     * Get thumbnails for part of the media context
     *
     * @param startOffset start offset in seconds relative to media context start
     * @param duration    duration of the media context part
     * @return promise for NSArray with RVSThumbnail objects
     */
    @Override
    public RVSPromise thumbnailsWithStartOffset(long startOffset, long duration) {

        Utils.assertCalledNotFromMainThread();

        HashMap parameters = new HashMap();
        parameters.put("mediaContext", context);
        parameters.put("startOffset", String.valueOf(startOffset * 1000));
        parameters.put("endOffset", String.valueOf((startOffset + duration) * 1000));


        String imageSource = RVSSdk.sharedRVSSdk().getSharedPreferences().getString("imageSource", null);
        if (imageSource != null) {
            parameters.put("imageSource", imageSource);
        }

        return RVSSdk.sharedRVSSdk().requestManager().getJsonObjectProperty("media/thumbnails", parameters, RVSThumbnailList.class, "images");
    }

    /**
     * Start time in milliseconds since 1970
     */
    @Override
    public long getStart() {
        return this.startTime.longValue();
    }

    /**
     * End time in milliseconds since 1970
     */
    @Override
    public long getEnd() {
        return this.endTime.longValue();
    }

    /**
     * Media context duration
     */
    @Override
    public long getDuration() {
        return (int) ((endTime.doubleValue() - startTime.doubleValue()) / 1000);
    }

    @Override
    public long getMaxPostTime() {
        return maxPostTime.longValue();
    }

    @Override
    public String getStatus() {
        if (this.status == null || this.status.length() == 0) {
            this.status = STATUS_OK;
        }

        return this.status;
    }

    @Override
    public String getSuppressionReason() {
        return suppressionReason;
    }

    public String getDeviceId() {
        return RVSSdk.sharedRVSSdk().serviceManager().getUniqueDeviceIdentifier();
    }

    @Override
    public RVSPromise reportViewWithStartOffset(long startOffset, long duration) {

    	return RVSSdk.sharedRVSSdk().reportViewWithStartOffset(context, startOffset, duration);
    }

    // JSON MAPPER
    private void setContext(String context){
        this.context = context;
    }
    private void setEndTime(Number endTime){ this.endTime = endTime; }
    private void setStartTime(Number startTime){  this.startTime = startTime; }
    private void setMaxPostTime(Number maxPostTime){
        this.maxPostTime = maxPostTime;
    }
    private void setStatus(String status){
        this.status = status;
    }
    private void setSuppressionReason(String suppressionReason){ this.suppressionReason = suppressionReason; }

}
