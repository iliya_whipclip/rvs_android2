package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSPagingListBase {
    private RVSPagingContext pagingContext;

    public void setPagingContext(RVSPagingContext pagingContext) { this.pagingContext = pagingContext; }
    public RVSPagingContext getPagingContext() { return this.pagingContext; }
}
