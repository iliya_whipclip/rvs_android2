package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUserSearchResult;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSUserSearchResultImp implements RVSUserSearchResult {

    private Number score;
    private RVSUserImp user;

    @Override
    public RVSUser getUser() {
        return user;
    }

    // JSON MAPPER
    private Number getScore() {
        return score;
    }
    private void setScore(Number score) {
        this.score = score;
    }
    private void setUser(RVSUserImp user) {
        this.user = user;
    }

}
