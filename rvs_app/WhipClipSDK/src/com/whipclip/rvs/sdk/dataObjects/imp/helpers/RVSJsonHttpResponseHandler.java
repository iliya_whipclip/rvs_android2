package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.whipclip.rvs.sdk.RVSPromise;

import org.apache.http.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by iliya on 8/7/14.
 */
public class RVSJsonHttpResponseHandler<T> extends BaseJsonHttpResponseHandler<T> {

    Logger logger = LoggerFactory.getLogger(RVSJsonHttpResponseHandler.class);

    final RVSPromise promise;
    final Class<T> objectClass;

    public RVSJsonHttpResponseHandler(RVSPromise promise, Class<T> objectClass) {
        this.promise = promise;
        this.objectClass = objectClass;
    }


    @Override
    public void onStart() {
        logger.debug("onStart");
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, T response) {
        promise.resolve(response);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, T errorResponse) {

        promise.reject(throwable);
    }


    @Override
    protected T parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        T result = null;

        try {
            result = mapper.readValues(new JsonFactory().createParser(rawJsonData), objectClass).next();
        } catch (JsonProcessingException e) {
            logger.error("failed to deserialize json error");
            promise.reject(e);
        }

        return result;
    }

}
