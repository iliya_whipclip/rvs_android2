package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSContentSearchResultImp;

import java.util.List;

/**
 * Created by iliya on 7/27/14.
 */
public class RVSContentSearchResults extends RVSPagingListBase {

    private List<RVSContentSearchResultImp> results;

    /**
     * Used via reflection
     * @param results
     */
    private void setResults(List<RVSContentSearchResultImp> results) {
        this.results = results;
    }



}
