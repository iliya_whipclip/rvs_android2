package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchMatch;

import java.util.List;

/**
 * Created by iliya on 7/27/14.
 */
public class RVSContentSearchResultImp implements RVSContentSearchResult {

    private RVSPostImp post;
    private Number score;
    private List<RVSSearchMatchImp> searchMatches;
    private RVSProgramExcerptImp programExcerpt;

    @Override
    public RVSProgramExcerpt getProgramExcerpt() {
        return programExcerpt;
    }

    @Override
    public RVSPost getPost() {
        return post;
    }

    @Override
    public List<RVSSearchMatch> getSearchMatches() {
        return (List<RVSSearchMatch>)(List<?>)searchMatches;
    }

    // JSON MAPPER
    private void setPost(RVSPostImp post){
        this.post = post;
    }
    private void setScore(Number score){
        this.score = score;
    }
    private void setSearchMatches(List<RVSSearchMatchImp> searchMatches){ this.searchMatches = searchMatches; }
    private void setProgramExcerpt(RVSProgramExcerptImp programExcerpt){ this.programExcerpt = programExcerpt; }

}
