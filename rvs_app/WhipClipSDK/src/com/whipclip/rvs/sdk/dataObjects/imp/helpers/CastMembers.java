package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

/**
 * Created by iliya on 6/24/14.
 */
public class CastMembers {
    private String actor;
    private String character;

    public String getActor(){
        return this.actor;
    }
    public void setActor(String actor){
        this.actor = actor;
    }
    public String getCharacter(){
        return this.character;
    }
    public void setCharacter(String character){
        this.character = character;
    }
}
