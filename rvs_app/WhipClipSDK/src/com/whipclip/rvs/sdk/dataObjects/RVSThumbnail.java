package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/29/14.
 */
public interface RVSThumbnail {

    /**
     * Thumbnail ID
     */
    public String getThumbnailId();

    /**
     * Start offset in seconds relative to the media context
     */
    public long getStartOffset();

    /**
     * Async image for getting actual images with different resolutions
     */
    public RVSAsyncImage getAsyncImage();
}
