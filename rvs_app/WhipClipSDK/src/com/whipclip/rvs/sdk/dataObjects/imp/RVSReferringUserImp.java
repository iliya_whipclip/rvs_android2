package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

/**
 * Created by iliya on 7/16/14.
 */
public class RVSReferringUserImp implements RVSReferringUser {

    private RVSUserImp user;
    private Number timestamp;

    @Override
    public RVSUser getUser() {
        return user;
    }

    @Override
    public long getTimestamp() {
        return timestamp.longValue();
    }

    // JSON MAPPER
    private void setUser(RVSUserImp user) { this.user = user; }
    private void setTimestamp(Number timestamp) { this.timestamp = timestamp; }

}
