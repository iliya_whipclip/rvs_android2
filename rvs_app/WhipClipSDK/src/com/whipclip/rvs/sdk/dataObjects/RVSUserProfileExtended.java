package com.whipclip.rvs.sdk.dataObjects;


/**
 * Created by iliya on 6/10/14.
 * Package private clase (visible only in sdk)
 */
public class RVSUserProfileExtended extends RVSUserProfile {
	
    private String email;
    private Boolean disabled;

    /**
     * JSON MAPPER
     */
    void setEmail(String email) { this.email = email; }
    void setDisabled(Boolean disabled) { this.disabled = disabled; }

}
