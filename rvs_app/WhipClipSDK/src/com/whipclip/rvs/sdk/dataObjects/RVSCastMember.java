package com.whipclip.rvs.sdk.dataObjects;

public class RVSCastMember {
	
	private String character;
	private String actor;
	
	/**
	 *  character
	 */
	public String getCharacter() {
		return character;	
	}

	/**
	 *  actor playing the character
	 */
	public String getActor() {
		return actor;
	}
	
	// JSON MAPPER
	private void setCharacter(String character) { this.character = character; }
	private void setActor(String actor) { this.actor = actor; }

}
