package com.whipclip.rvs.sdk.dataObjects;

import java.util.Comparator;

/**
 * Created by iliya on 7/24/14.
 */
public class RVSImage {

	public static final String RVSImageTypeShowImage          = "SHOW_IMAGE";
	public static final String RVSImageTypeChannelLogo        = "CHANNEL_LOGO";
	public static final String RVSImageTypeChannelWatermark   = "CHANNEL_WATERMARK";
	public static final String RVSImageTypeUserProfile        = "USER_PROFILE";
	public static final String RVSImageTypeEndCard            = "END_CARD";
	//Default (fallback)
	public static final String RVSImageTypeDefaultShowImage   = "Banner";
	
    public static Comparator<RVSImage> heightComparator = new Comparator<RVSImage>() {

        public int compare(RVSImage image1, RVSImage image2) {

            //ascending order
            return image1.getHeight() - image2.getHeight();
        }
    };

    private String url;
    private String id;
    private Number width = 0;
    private Number height = 0;
    private String type;

    public void setId(String id) { this.id = id; }
    public String getId() { return id; }
    
    public void setUrl(String url) { this.url = url; }
    public String getUrl() { return url; }

    public void setWidth(Number width) { this.width = width; };
    public int getWidth() { return width.intValue(); };

    public void setHeight(Number height) { this.height = height; };
    public int getHeight() { return height.intValue(); };
    
    public void setType(String type) { this.type = type; };
    public String getType() { return type; };
}
