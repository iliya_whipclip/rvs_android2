package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserSearchResultImp;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSUserSearchResults extends RVSPagingListBase {
    private List<RVSUserSearchResultImp> results;

    public List<RVSUserSearchResultImp> getUsers() {

        return results;
    }

    /**
     * Used via reflection
     * @param results
     */
    private void setResults(List<RVSUserSearchResultImp> results) {
        this.results = results;
    }

}
