package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSEpisode;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.imp.helpers.CastMembers;

import java.util.List;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSEpisodeImp implements RVSEpisode {
    private List<CastMembers> castMembers;
    private Number episodeNumber;
    private List<RVSImage> images;
    private String name;
    private Number seasonNumber;
    private String synopsis;
    private RVSAsyncImage image;

    @Override
    public int getSeasonNumber() {
        return this.episodeNumber.intValue();
    }

    @Override
    public int getEpisodeNumber() {
        return this.seasonNumber.intValue();
    }

    @Override
    public String getName(){
        return this.name;
    }
    
    @Override
	public RVSAsyncImage getImage() {
		if (image != null)
            return image;

        if (images.size() > 0)
        {
            image = new RVSImageFromUrlOrArray(images);
            return image;
        }

        return null;
	}

    // JSON MAPPER
    private List getCastMembers(){ return this.castMembers; }
    private void setCastMembers(List castMembers){ this.castMembers = castMembers; }
    private void setEpisodeNumber(Number episodeNumber){ this.episodeNumber = episodeNumber; }
    private List<RVSImage> getImages(){ return this.images; }
    private void setImages(List<RVSImage> images){ this.images = images; }
    private void setName(String name){ this.name = name; }
    private void setSeasonNumber(Number seasonNumber){ this.seasonNumber = seasonNumber; }
    private String getSynopsis(){ return this.synopsis; }
    private void setSynopsis(String synopsis){ this.synopsis = synopsis; }

	
}
