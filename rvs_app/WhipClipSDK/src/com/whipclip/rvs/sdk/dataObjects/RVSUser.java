package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 6/10/14.
 */
public interface RVSUser {

    /**
     * @return User ID
     **/
    public String getUserId();

    /**
     * @return user name
     **/
    public String getName();

    /**
     * User handle
     **/
    public String  getHandle();

   /**
    * User display handle "@handle"
    */
    public String getDisplayHandle();
   
    /**
     * @return true if user is a verfied user
     **/
    public boolean isVerified();
    
    /**
    * YES if user is an editor
    **/
    public boolean isEditor();

    /**
     * Profile image
     **/
    public RVSAsyncImage getPofileImage();

    /**
     * @return tue is user if followed by me
     **/
    public boolean isFollowedByMe();

    /**
     * Number of users following this user
     **/
    public int getFollowerCount();

    /**
     * Number of users followed by this user
     **/
    public int getFollowingCount();

    /**
     * Number of posts created by this user
     **/
    public int getPostCount();

    /**
     *  Number of likes created by this user
     */
    public int getLikeCount();

    /**
     *  Number of likes reposts by this user
     */
    public int getRepostCount();

}
