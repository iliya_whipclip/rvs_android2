package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSEpgSlotImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;

import java.util.List;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSEpgSlotList extends RVSPagingListBase {

    private List<RVSEpgSlotImp> epgSlots;

    public List<RVSEpgSlotImp> getEpgSlosts() {
        return this.epgSlots;
    }

    /**
     * Used via reflection
     * @param posts
     */
    private void setEpgSlots(List<RVSEpgSlotImp> epgSlots) {
        this.epgSlots = epgSlots;
    }
}
