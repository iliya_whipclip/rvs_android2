package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 7/27/14.
 */
public interface RVSProgramExcerpt {

    /**
     * Channel
     */
    public RVSChannel getChannel();

    /**
     * Program
     */
    public RVSProgram getProgram();

    /**
     * Media context
     */
    public RVSMediaContext getMediaContext();

    /**
     * Excerpt cover image
     */
    public RVSAsyncImage getCoverImage();

    /*!
     Program transcript
     */
    public String getTranscript();


}
