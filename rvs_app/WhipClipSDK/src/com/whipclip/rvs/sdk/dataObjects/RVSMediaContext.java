package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.RVSPromise;

/**
 * Created by iliya on 6/29/14.
 */
public interface RVSMediaContext {

	 // all possible internal statuses
    public static final String STATUS_OK                               = "OK";
    public static final String STATUS_SUPPRESSED_CHANNEL               = "SUPPRESSED_CHANNEL";
    public static final String STATUS_SUPPRESSED_PART_OF_SHOW          = "SUPPRESSED_PART_OF_SHOW";
    public static final String STATUS_SUPPRESSED_ENTIRE_SHOW           = "SUPPRESSED_ENTIRE_SHOW";

    public static final String SUPPRESSIONREASON_SPOILER_ALERT         = "SPOILER_ALERT";
    public static final String SUPPRESSIONREASON_RESTRICTION           = "RIGHTS_RESTRICTION";
    
    /**
    *  Get partial clip URL for playback using native player
    *
    * @param mediaType mediaType for the clip, nil for default media type
    *  @param startOffset start offset in seconds relative to media context start
    *  @param duration clip duration
    *
    *  @return clip URL
    **/
    public String clipUrlWithMediaType(String mediaType, long startOffset, long duration);

    /**
     *  Get thumbnails for part of the media context
     *
     *  @param startOffset start offset in seconds relative to media context start
     *  @param duration duration of the media context part
     *
     *  @return promise with DoneCallback<RVSThumbnailList>
     */
    RVSPromise thumbnailsWithStartOffset(long startOffset, long duration);

    /**
     *  Start time in milliseconds since 1970
     */
    public long getStart();

    /**
     *  End time in milliseconds since 1970
     */
    public long getEnd();

    /**
     *  Media context duration
     */
    public long getDuration();

    /**
     *  Report media view
     *
     *  @param startOffset start offset in seconds relative to media context start
     *  @param duration duration of the media context part
     */
    public RVSPromise reportViewWithStartOffset(long startOffset, long duration);

    /**
     *  Max post time created from this media context in milliseconds
     */
   public long getMaxPostTime();

    /**
     *  Media context status, see RVSClipMediaStatus
     */
    public String getStatus();

    /**
     *  media context suppression reason, see RVSClipMediaSuppressionReason
     */
    public String  getSuppressionReason();

}
