package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSRepostEvent;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSRepostEventImp implements RVSRepostEvent {

    private RVSUserImp user;
    private RVSPostImp post;

    @Override
    public RVSUser getUser() {
        return user;
    }

    @Override
    public RVSPost getPost() {
        return post;
    }

    // JSON MAPPER
    private void setUser(RVSUserImp user) { this.user = user; }
    private void setPost(RVSPostImp post) {this.post = post; }
}
