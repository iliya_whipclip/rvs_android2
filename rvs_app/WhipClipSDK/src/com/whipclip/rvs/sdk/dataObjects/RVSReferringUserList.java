package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSReferringUserImp;

import java.util.List;

/**
 * Created by iliya on 8/4/14.
 */
public class RVSReferringUserList extends RVSPagingListBase {

    private List<RVSReferringUserImp> referringUsers;

    public List<RVSReferringUser> getReferringUsers() {
        return (List<RVSReferringUser>)(List<?>)referringUsers;
    }

    // JSON MAPPER
    private void setReferringUsers(List<RVSReferringUserImp> users) {
        this.referringUsers = users;
    }

}
