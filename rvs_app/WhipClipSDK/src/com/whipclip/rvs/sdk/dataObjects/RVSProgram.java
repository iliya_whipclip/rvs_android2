package com.whipclip.rvs.sdk.dataObjects;

import java.util.List;

/**
 * Created by iliya on 7/11/14.
 */
public interface RVSProgram {

    /**
     * program ID
     */
    public String getProgramId(); 

    /**
     * program start time in milliseconds since 1970
     */
    public long getStart();  

    /**
     * program end time in milliseconds since 1970
     */
    public long getEnd(); 
    
	/**
	 *  end card
	 */
    public RVSEndCard getEndCard(); 
    
    /**
     * content ID (same for re-runs)
     **/
    public String getContentId();  
    
    /**
     * Return time offset from now
     */
    public long getStartOffsetTime();
    
    // TMS changes
    /**
     *  display name
     */
    public String getName();
    
    /**
     *  show ID
     */
    public String getShowId();

	/**
	 *  array of RVSCastMember participating in the show
	 */
    public List<RVSCastMember> getCastMembers();

	/**
	 *  official show site URL
	 */
    public String getOfficialUrl();

    /**
     *  channel ID
     */
    public String getChannelId();
    
    /**
     *  synopsis
     */
    public String getSynopsis();

    /**
     *  tms type
     */
    public String getTmsType();

    /**
     *  tms sub-type
     */
    public String getTmsSubType();

    /**
     *  special show name
     */
    public String getSpecialShowName();
    /**
     *  episode object is program is an episode or nil if not an episode
     */
    public RVSEpisode getEpisodeMetadata();
    
    /**
     *  image for program
     */
    public RVSAsyncImage getImage();

}
