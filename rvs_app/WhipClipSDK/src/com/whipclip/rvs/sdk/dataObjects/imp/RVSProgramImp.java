package com.whipclip.rvs.sdk.dataObjects.imp;

import java.io.Serializable;
import java.util.List;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSCastMember;
import com.whipclip.rvs.sdk.dataObjects.RVSEndCard;
import com.whipclip.rvs.sdk.dataObjects.RVSEpisode;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;

/**
 * Created by iliya on 6/24/14.
 */
public class RVSProgramImp implements RVSProgram, Serializable {
    private Number endTime;
    private RVSEpisodeImp episode;
    private String id;
    private RVSShowImp show;
    private Number startTime;
	private List<RVSEndCardImp> endCard;
	private String contentId;
	private String name;
	private String showId;
	private List<RVSCastMember> castMembers;
	private String officialUrl;
	private String channelId;
	private String synopsis;
	private String tmsType;
	private String tmsSubType;
	private String specialShowName;
	private RVSAsyncImage image;
	private List<RVSImage> images;

    @Override
    public String getProgramId() {
        return id;
    }

    @Override
    public long getStart() {
        return startTime.longValue();
    }

    @Override
    public long getEnd() {
        return endTime.longValue();
    }

//    @Override
//    public RVSShow getShow() {
//        return show;
//    }
//
//    @Override
//    public RVSEpisode getEpisode() {
//        return episode;
//    }

    @Override
	public RVSEndCard getEndCard() {
		return endCard != null ? (endCard.size() > 0 ? endCard.get(0) : null) : null;
	}
    
   
	public String getContentId() {
		// backward compatibility, the contentId getter should return programId 
		// if it is not set (can be removed once feature will be added to the production server)
		if (contentId == null)
	        return getProgramId(); 
		
		return contentId;
	}
	
	@Override
	public long getStartOffsetTime() {
		
		long now = System.currentTimeMillis();
		return startTime.longValue()*1000 - now;
		// TODO UnitTest
//		return 3000;
	}
    
  // NEW MEW NEW

	@Override
	public String getName() { return name; }

	@Override
	public String getShowId() { return showId; }

	@Override
	public List<RVSCastMember> getCastMembers() { return castMembers; }

	@Override
	public String getOfficialUrl() { return officialUrl; }

	@Override
	public String getChannelId() { return channelId; }
	
	@Override
	public String getSynopsis() { return synopsis; }

	@Override
	public String getTmsType() {
		return tmsType;
	}

	@Override
	public String getTmsSubType() { return tmsSubType; }

	@Override
	public String getSpecialShowName() { return specialShowName; }

	@Override
	public RVSEpisode getEpisodeMetadata() { return episode; }

	@Override
	public RVSAsyncImage getImage() {
		 if (image != null)
	            return image;

	        if (images.size() > 0)
	        {
	            image = new RVSImageFromUrlOrArray(images);
	            return image;
	        }

	        return null;
	}
	
	  /****************
     * JSON MAPPER
     ****************/
    private void setEndTime(Number endTime) { this.endTime = endTime; }
    private void setEpisodeMetadata(RVSEpisodeImp episodeMetadata) { this.episode = episodeMetadata; }
    private void setId(String id) { this.id = id; }
    private void setShowMetadata(RVSShowImp show) { this.show = show; }
    private void setStartTime(Number startTime) { this.startTime = startTime; }
    private void setEndCards(List<RVSEndCardImp> endCard){ this.endCard = endCard; }
    private void setContentId(String contentId) { this.contentId = contentId; }
    // new
    private void setName(String name) { this.name = name; }
    private void setShowId(String showId) { this.showId = showId; }
    private void setCastMembers(List<RVSCastMember> castMembers){ this.castMembers = castMembers; }
    private void setOfficialUrl(String officialUrl) { this.officialUrl = officialUrl; }
    private void setChannelId(String channelId) { this.channelId = channelId; }
    private void setSynopsis(String synopsis) { this.synopsis = synopsis; }
    private void setTmsType(String tmsType) { this.tmsType = tmsType; }
    private void setTmsSubType(String tmsSubType) { this.tmsSubType = tmsSubType; }
    private void setSpecialShowName(String specialShowName) { this.specialShowName = specialShowName; }
    private void setImages(List<RVSImage> images){ this.images = images; }

    
}
