package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSSearchUserSuggestionResult;

/**
 * Created by iliya on 8/17/14.
 */
public class RVSSearchUserSuggestionResultImp implements RVSSearchUserSuggestionResult{

    private String userName;

    @Override
    public String getUserName() {
        return userName;
    }

    // JSON MAPPER
    public void setUserName(String userName) { this.userName = userName; }

}
