package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSSearchChannelSuggestionResult;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchCompositeResultList;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchContentSuggestionResult;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchUserSuggestionResult;

import java.util.List;

/**
 * Created by iliya on 8/17/14.
 */
public class RVSSearchCompositeResultListImp implements RVSSearchCompositeResultList {

    private List<RVSSearchContentSuggestionResultImp> contentResults;
    private List<RVSSearchChannelSuggestionResultImp> channelResults;
    private List<RVSSearchUserSuggestionResultImp> userResults;

    @Override
    public List<RVSSearchContentSuggestionResult> getContentResults() {
        return (List<RVSSearchContentSuggestionResult>)(List<?>)contentResults;
    }

    @Override
    public List<RVSSearchChannelSuggestionResult> getChannelResults() {
        return (List<RVSSearchChannelSuggestionResult>)(List<?>)channelResults;
    }

    @Override
    public List<RVSSearchUserSuggestionResult> getUserResults() {
        return (List<RVSSearchUserSuggestionResult>)(List<?>)userResults;
    }

    // JSON MAPPER
    private void setContentResults(List<RVSSearchContentSuggestionResultImp> contentResults) {
        this.contentResults = contentResults;
    }

    private void setChannelResults(List<RVSSearchChannelSuggestionResultImp> channelResults ) {
        this.channelResults = channelResults;
    }

    private void setUserResults(List<RVSSearchUserSuggestionResultImp> userResults) {
        this.userResults = userResults;
    }

}
