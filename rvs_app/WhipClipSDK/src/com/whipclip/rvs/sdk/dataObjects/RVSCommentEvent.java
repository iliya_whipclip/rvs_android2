package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/8/14.
 */
public interface RVSCommentEvent {
    /**
     * Event comment
     */
    public RVSComment getComment();

    /**
     * Event post
     */
    public RVSPost getPost();
}
