package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/8/14.
 */
public interface RVSNotification {
    /**
     * Time re-posted in milliseconds since 1970
     */
    public long getTimestamp();

    /**
     * Notification like event
     */
    public RVSLikeEvent getLikeEvent();

    /**
     * Notification follow event
     */
    public RVSFollowEvent getFollowEvent();

    /**
     * Notification comment event
     */
    public RVSCommentEvent getCommentEvent();

    /**
     * Notification repost event
     */
    public RVSRepostEvent getRepostEvent();
}
