package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

public class MediaViewedResult {

	private String action;
	
	public void SetAction(String action) {
		this.action = action;
	}
	
	public String getAction() {
		return action;
	}
	
}
