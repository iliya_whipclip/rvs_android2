package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/17/14.
 */
public interface RVSSearchUserSuggestionResult {

    /**
     * the username suggestion
     */
    public String getUserName();

}
