package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/8/14.
 */
public interface RVSLikeEvent {

    /**
     * Event user
     */
    public RVSUser getUser();

    /**
     * Event post
     */
    public RVSPost getPost();
}
