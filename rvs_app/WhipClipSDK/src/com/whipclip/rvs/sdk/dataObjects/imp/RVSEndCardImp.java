package com.whipclip.rvs.sdk.dataObjects.imp;

import java.util.List;

import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSEndCard;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;

public class RVSEndCardImp implements RVSEndCard{

	private String id;
	private String tuneInInformation;
	private String linkUrl;
	private String linkDescription;
    private List<RVSImage> images;
    private RVSAsyncImage image;


	@Override
	public String getEndCardId() { return id; }

	@Override
	public String getTuneInInformation() { return tuneInInformation; }

	@Override
	public String getLinkUrl() { return linkUrl; }

	@Override
	public String getLinkDescription() { return linkDescription; }
	
	@Override
	public RVSAsyncImage getImage() {
		
		 if (image != null)
            return image;

        if (images != null && images.size() > 0)
        {
            image = new RVSImageFromUrlOrArray(images);
            return image;
        }

        return null;

	}	
	
	// JSON MAPPER
	public void setId(String id) { this.id = id; }
	public void setTuneInInformation(String tuneInInformation) { this.tuneInInformation = tuneInInformation; }
	public void setLinkUrl(String linkUrl) { this.linkUrl = linkUrl; }
	public void setLinkDescription(String linkDescription) { this.linkDescription = linkDescription; }
	public void setImages(List<RVSImage> images){ this.images = images; }



	

}
