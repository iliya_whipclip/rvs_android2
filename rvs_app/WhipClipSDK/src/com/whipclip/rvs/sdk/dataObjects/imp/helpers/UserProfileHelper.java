package com.whipclip.rvs.sdk.dataObjects.imp.helpers;

import com.whipclip.rvs.sdk.dataObjects.RVSImage;

import java.util.ArrayList;

/**
 * Created by iliya on 6/24/14.
 */
public class UserProfileHelper {
    private String id;
    private String name;

    private boolean isVerified;

    private ArrayList<RVSImage> images = new ArrayList<RVSImage>();

    void setId(String id) { this.id = id; }
    void setName(String name) { this.name = name; }
    void setVerified(boolean isVerified) { this.isVerified = isVerified; }
    void setImages(ArrayList<RVSImage> images) { this.images = images; }

    public String getId() { return id; }
    public String getName() { return name; }
    public boolean isVerified() { return isVerified; }
    public ArrayList<RVSImage> getImages() { return images; }
}