package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;

import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSChannelSearchResultImp implements RVSChannelSearchResult {

    private RVSChannelImp channel;
    private Number score;
    private List<RVSSearchMatchImp> searchMatches;

    @Override
    public RVSChannel getChannel() {
        return channel;
    }

    // JSON MAPPER
    private void setChannel(RVSChannelImp channel){this.channel = channel;}
    private void setScore(Number score){
        this.score = score;
    }
    private void setSearchMatches(List<RVSSearchMatchImp> searchMatches){ this.searchMatches = searchMatches; }

}
