package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSFeedCountDetails {

    private Number count;
    private Number startTime;

    /**
     * Total new notifications count between startTime and now
     */
    public int getCount() { return count.intValue(); }

    /**
     * Specifies from what time to calculate all new notifications
     */
    public long getStartTime() { return startTime.longValue(); }

    // JSON MAPPER
    public void setCount(Number count) { this.count = count; }
    public void setStartTime(Number startTime) { this.startTime = startTime; }
}
