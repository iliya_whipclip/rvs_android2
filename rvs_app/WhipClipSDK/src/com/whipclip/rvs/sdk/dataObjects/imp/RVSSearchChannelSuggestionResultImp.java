package com.whipclip.rvs.sdk.dataObjects.imp;

import com.whipclip.rvs.sdk.dataObjects.RVSSearchChannelSuggestionResult;

/**
 * Created by iliya on 8/17/14.
 */
public class RVSSearchChannelSuggestionResultImp implements RVSSearchChannelSuggestionResult {

    private String channelName;

    @Override
    public String getChannelName() { return channelName; }

    // JSON MAPPER
    private void setChannelName(String channelName) { this.channelName = channelName; }
}
