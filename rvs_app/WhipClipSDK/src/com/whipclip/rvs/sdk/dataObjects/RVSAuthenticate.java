package com.whipclip.rvs.sdk.dataObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserImp;

import android.service.textservice.SpellCheckerService.Session;

public class RVSAuthenticate {
	
	private String sessionToken;
    private String userId;
    private Boolean isNewExternalUser;
    private Boolean isEmailEmpty;
    private Boolean isHandleRequired;
    private String role;
    private String email;
    
    private RVSUserImp expandedUser;
    private RVSUserProfileExtended userProfileExtended;
    
    public String getSessionToken() { return this.sessionToken; }
    public String getUserId() { return this.userId; }
    public String getEmail() { return this.email; }
    public Boolean isNewExternalUser() { return this.isNewExternalUser; }
    public Boolean isEmailEmpty() { return this.isEmailEmpty; }
    public Boolean isHandleRequired() { return this.isHandleRequired; }
    public RVSUser getUserExtended() { return this.expandedUser; }
    public RVSUserProfileExtended getUserProfileExtended() { return this.userProfileExtended; }
    
    // JSON MAPPER
    private void setSessionToken(String sessionToken) { this.sessionToken = sessionToken; }
    private void setUserId(String userId) { this.userId = userId; }
    private void setEmail(String email) { this.email = email; }
    private void setRole(String role) { this.role = role; }
//    private void setIsNewUser(Boolean isNewUser) { this.isNewUser = isNewUser; }
    private void setIsNewExternalUser(Boolean isNewExternalUser) { this.isNewExternalUser = isNewExternalUser; }
    private void setIsEmailEmpty(Boolean isEmailEmpty) { this.isEmailEmpty = isEmailEmpty; }
    private void setIsHandleRequired(Boolean isHandleRequired) { this.isHandleRequired = isHandleRequired; }
    private void setUserProfileExtended(RVSUserProfileExtended userProfileExtended) { this.userProfileExtended = userProfileExtended; }
    private void setExpandedUser(RVSUserImp expandedUser) { this.expandedUser = expandedUser; }
	
    


}
