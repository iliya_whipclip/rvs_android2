package com.whipclip.rvs.sdk.dataObjects;

import android.graphics.Point;
import com.whipclip.rvs.sdk.RVSPromise;

/**
 * Created by iliya on 7/11/14.
 */
public interface RVSAsyncImage {

    /**
     * Get image for given size
     * @param size required image size in UI points
     * @return promise with android.graphics.Bitmap in DoneCallback
     **/
    RVSPromise getImageWithSize(Point size);

    /**
     * Get image for given size and type
     * @param size required image size in UI points
     * @param type type constant
     * @return promise for UIImage
     **/
    RVSPromise getImageWithSize(Point size, String type);
   

    /**
     * Get image URL for given size
     * @param size size required image size in UI points
     * @return string url of image
     */
    String getImageUrlForSize(Point size);
    
    /**
     * Get image URL for given size
     * @param size size required image size in UI points
     *  @param type type constant
     * @return string url of image
     */
    String getImageUrlForSize(Point dpSize, String type);

    
   /**
    *   Get the overlay image URL for given size
    *
    *  @param size size required image size in UI points
    *
    *  @return string url of image
    */
    String getOverlayedImageUrlForSize(Point size, String channelId);


   /**
    *  Checks if image contains images from a type
    *
    *  @param type type to check
    *
    *  @return contains images with that type
    */
   boolean isContainsType(String type);
   
   

}
