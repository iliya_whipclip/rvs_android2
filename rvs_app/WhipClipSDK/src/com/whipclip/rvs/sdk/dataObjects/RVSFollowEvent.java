package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/8/14.
 */
public interface RVSFollowEvent {

    public RVSUser getUser();
}
