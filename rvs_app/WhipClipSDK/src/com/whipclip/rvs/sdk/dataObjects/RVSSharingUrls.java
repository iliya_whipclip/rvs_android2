package com.whipclip.rvs.sdk.dataObjects;

/**
 * Created by iliya on 8/7/14.
 */
public class RVSSharingUrls {

    private String sharingUrl;
    private String coverWithOverlayUrl;

    public String getUrl() { return sharingUrl; }
    public String getCoverWithOverlayUrl() { return coverWithOverlayUrl; }

    // JSON MAPPER
    private void setSharingUrl(String sharingUrl) { this.sharingUrl = sharingUrl; }
    private void setCverWithOverlayUrl(String coverWithOverlayUrl) { this.coverWithOverlayUrl = coverWithOverlayUrl; }

}
