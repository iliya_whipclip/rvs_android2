package com.whipclip.rvs.sdk;

import android.os.Looper;

import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;
import org.jdeferred.Promise;
import org.jdeferred.android.AndroidDeferredObject;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.impl.DeferredObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.loopj.android.http.RequestHandle;
import com.whipclip.rvs.sdk.managers.RVSRequestManager;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Created by iliya on 6/10/14.
 */
public class RVSPromise<D, F, P>   extends AndroidDeferredObject<D, F, P>  {

    Logger logger = LoggerFactory.getLogger(RVSPromise.class);

    private static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

    public interface CancelCallback {
        public void onCancel();
    }

    private boolean isCancelled = false;
    private CancelCallback cancelListener;
	private WeakReference<RequestHandle> requestHandler;

	private String url;

    public enum RVSPromiseExecutionScope {
        BACKGROUND, UI;
    }

    private RVSPromise(Promise promise) {
        super(promise);
    }

    private RVSPromise(Promise promise, AndroidExecutionScope executionScope) {
        super(promise, executionScope);
    }

    public static RVSPromise createPromise() {
        return RVSPromise.createPromise(RVSPromiseExecutionScope.BACKGROUND);
    }

    public static RVSPromise createPromise(RVSPromiseExecutionScope scope) {
        AndroidExecutionScope executionScope = AndroidExecutionScope.BACKGROUND;
        if (scope != null && scope == RVSPromiseExecutionScope.UI) {
            executionScope = AndroidExecutionScope.UI;
        }

        DeferredObject deferred = new DeferredObject<DoneCallback<Object>, FailCallback<Object>, ProgressCallback<Object>>();
        return  new RVSPromise(deferred,executionScope);
    }

    // if promise has AndroidExecutionScope.Backround scope, run done callback in new thread
    @Override
    protected void triggerDone(final DoneCallback<D> callback,final D resolved) {
        if (determineAndroidExecutionScope(callback) == AndroidExecutionScope.UI || Looper.myLooper() != Looper.getMainLooper()) {
            super.triggerDone(callback, resolved);
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    callback.onDone(resolved);
                }
            }).start();
        }
    };

    @Override
    protected void triggerFail(final FailCallback<F> callback,final F rejected) {
        if (determineAndroidExecutionScope(callback) == AndroidExecutionScope.UI || Looper.myLooper() != Looper.getMainLooper()) {
            super.triggerFail(callback, rejected);
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    callback.onFail(rejected);
                }
            }).start();
        }
    };


    public boolean isCancelled() {
        return isCancelled;
    }

    
    /**
     * todo Cancel functionality is missing:
     * Missing State.CANCELED with cancel() method and onCancel listener. If promise is in pending state and cancel method was called, move the state to CANCELED, call onCancel method.
     * call promise = null right now immediately after calling cancel method
     */
    public void cancel() {
    
        isCancelled = true;

        // cancel http request 
        if (requestHandler != null && requestHandler.get() != null) {
        	logger.debug("Try to cancel promise requestHandler");
        	
        	if (Looper.myLooper() == Looper.getMainLooper()) {
        		new Thread(new Runnable() {
                    @Override
                    public void run() {
                    	if (requestHandler != null && requestHandler.get() != null) {
                    		boolean canceled = requestHandler.get().cancel(false);
                    		logger.debug("Request canceled <"+ canceled + "> moved from ui to non ui thread, url " + url);
                    	}
                    	else {
                    		logger.debug("Request canceled, moved from ui to non ui, requestHandler has been released");
                    	}
                    	
                    }
                    
                }).start();
        		
            } else {
            	boolean canceled = requestHandler.get().cancel(false);
            	logger.debug("Request canceled not from ui tread <" + canceled + "> , url " + url);
            }
        }
        
        if (cancelListener != null)  {
            cancelListener.onCancel();
        }
    }

    public void rejectWithDelay(final F error, int milliseconds) {

        worker.schedule(new Runnable() {
            @Override
            public void run() {
                reject(error);
            }
        }, milliseconds, TimeUnit.MILLISECONDS);
    }

    public void setCancelListener(CancelCallback listener) {
        cancelListener = listener;
    }

    @Override
    public Deferred<D, F, P> resolve(final D resolve) {

        if (isCancelled()) {
            return null;
        }

        return super.resolve(resolve);
    }

	public void setRequestHandler(RequestHandle requestHandler, String url) {
		this.requestHandler = new WeakReference<RequestHandle>(requestHandler);
		this.url = url;
	}
}
