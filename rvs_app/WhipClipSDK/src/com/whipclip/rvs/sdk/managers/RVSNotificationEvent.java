package com.whipclip.rvs.sdk.managers;

import java.util.Map;

public final class RVSNotificationEvent
{	
	private String m_name;
	private Object m_object;
	private Map<String, Object> m_userInfo;
	
	RVSNotificationEvent(String name, Object sender, Map<String, Object> userInfo)
	{
		m_name = name;
		m_object = sender;
		m_userInfo = userInfo;
	}
	
	public String getName()
	{
		return m_name;
	}
	
	public Object getObject()
	{
		return m_object;
	}
	
	public Map<String, Object> getUserInfo()
	{
		return m_userInfo;
	}
}
