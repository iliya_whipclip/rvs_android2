package com.whipclip.rvs.sdk.managers;

public enum RVSServiceNetworkStatus
{
    // first time, no network determined
    RVSServiceNetworkStatusNone,

    // there's no network so service is disconnected and cannot perform any network operation
    RVSServiceNetworkStatusNoNetwork,

    // Disconnected from service because user is signed or because of a network problem
    RVSServiceNetworkStatusDisconnected,

    // Trying to connect to the service
    RVSServiceNetworkStatusConnecting,

    // Connected to the service
    RVSServiceNetworkStatusConnected,
}
