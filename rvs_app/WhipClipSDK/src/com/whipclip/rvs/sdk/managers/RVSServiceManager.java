package com.whipclip.rvs.sdk.managers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.UUID;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;

import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSErrorCode;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.aysncObjects.RVSUpdateUserProfile;
import com.whipclip.rvs.sdk.dataObjects.RVSAuthenticate;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.receiver.RVSConnectionChangeReceiver;
import com.whipclip.rvs.sdk.util.Crypto;
import com.whipclip.rvs.sdk.util.Utils;

/**
 * To make connectivity update work, add to Manifest.xml of application
 *      <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 *
 *      and
 *
 *      <receiver android:name="com.whipclip.rvs.sdk.receiver.ConnectionChangeReceiver" android:label="NetworkConnection">
 *          <intent-filter>
 *              <action android:name="android.net.conn.CONNECTIVITY_CHANGE"/>
 *          </intent-filter>
 *      </receiver>
 *
 * Created by iliya on 6/10/14.
 */
public class RVSServiceManager implements RVSConnectionChangeReceiver.NetworkChangeListener {

    /**
     * Notification that will be posted when user was signed in succesfully to the service.
     */
    public static final String RVSServiceSignedInNotification = "RVSServiceSignedInNotification";
    /**
     * Notification that will be posted when user can sign in successfully to the service but awaiting profile update.
     */
    public static final String RVSServiceSignedInPendingNotification = "RVSServiceSignInPendingNotification";
    
    /**
     * Notification that will be posted when user failed to sign in to the service.
     * the notification user info will contain the error information
     * Does not occur as a result of signout
     */
    public static final String RVSServiceSignInErrorNotification = "RVSServiceSignInErrorNotification";
    /**
     * Notification that will be posted when the user was signed out from the service.
     * This can happen when calling signOut, authentication failure or service decision to sign use out
     * Unless the signOut was called, the notification user info will contain the error information
     * Can not occur as a result of calling signIn
     */
    public static final String RVSServiceSignedOutNotification = "RVSServiceSignedOutNotification";
    /**
     * Notification that will be posted when the service network status changes.
     */
    public static final String RVSServiceNetworkStatusChangedNotification = "RVSServiceNetworkStatusChangedNotification";
    /**
     * NSError key in signed out notification user info
     */
    private static final String RVSServiceErrorUserInfoKey = "RVSServiceErrorUserInfoKey";
    
    private static String RVSServiceUserName = "RVSServiceUserName";
    
//    private static final String RVSServiceManagerSharedPreferencesKye = "RVSServiceManagerSharedPreferencesKye";
    private final String pwd = "nWY8NfzeAc7T5XFEpdJI";
    private Handler handler = null;
    private RVSPromise authenticateRequestPromise;


    protected volatile static UUID uniqueUserId;
    // all affected devices have same ANDROID_ID, which is 9774d56d682e549c.
    // the same device id reported by the emulator
    private final static String PROBLEMAITC_DEVICE_ID = "9774d56d682e549c";
    protected static final String PREFS_FILE = "device_id_file";
    protected static final String PREFS_DEVICE_ID = "device_id";
    private String uniqueUserIdStr;


    public enum SavedEncryptedType {
        FacebokToken,
        TwitterToken,
        TwitterSecret,
        Password
    }

    public interface NetworkConnectivityChanged {
        public void onNetworkConnectivityChanged(RVSServiceNetworkStatus networkStatus);
    }

    private interface UrlConnectionExistTaskCompleteListener {
        public void onTaskComplete(boolean isConnected);
    }

    private class UrlConnectionExistAsyncTask extends AsyncTask<String, Void, Boolean> {
        UrlConnectionExistTaskCompleteListener callback;

        public UrlConnectionExistAsyncTask(UrlConnectionExistTaskCompleteListener callback) {
            this.callback = callback;
        }

        protected Boolean doInBackground(String... params) {
            try {
                boolean isConnected = false;
                HttpURLConnection urlc = (HttpURLConnection) (new URL(params[0]).openConnection());
                urlc.setConnectTimeout(5000);
                urlc.connect();
                isConnected = true;
                urlc.disconnect();
                ;
                return isConnected;
            } catch (IOException e) {
                return false;
            } catch (Exception e) {

            }

            return false;
        }

        protected void onPostExecute(Boolean result) {
            callback.onTaskComplete(result.booleanValue());
        }
    }


    private Logger logger = LoggerFactory.getLogger(RVSServiceManager.class);

    private final String domain;
    private RVSServiceNetworkStatus networkStatus;
    private RVSAuthenticate authenticateResult;
//    private String sessionToken;
//    private String userId;
    private RVSPromise authenticate;
    private RVSPromise configuration;
    private RVSPromise authenticatePromise;

    private NetworkConnectivityChanged networkConnectivityChangedListener;


    public void onEventMainThread(Object object) {
        logger.info("onEvent");
    }

    public RVSServiceManager(String domain) {

        logger.info("starting service manager");

//        sessionToken = null;
        authenticateResult = null;
        this.domain = domain;
        networkStatus = RVSServiceNetworkStatus.RVSServiceNetworkStatusNone;

        RVSConnectionChangeReceiver.setNetworkListener(this);
        checkHTTPConnection();

        if (!this.isSignedOut()) {
            handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {

                    authenticateAndNotify();
                }
            });

        }
    }

    void updateNetworkStatus(boolean isConnected) {
        RVSServiceNetworkStatus networkStatus = RVSServiceNetworkStatus.RVSServiceNetworkStatusNone;

        if (!isConnected)
            networkStatus = RVSServiceNetworkStatus.RVSServiceNetworkStatusNoNetwork;
        else if (isSignedOut())
            networkStatus = RVSServiceNetworkStatus.RVSServiceNetworkStatusDisconnected;
        else
            networkStatus = RVSServiceNetworkStatus.RVSServiceNetworkStatusConnected;

        if (networkStatus != this.networkStatus) {
            this.networkStatus = networkStatus;
            logger.info("network status is " + networkStatus);

            if (networkConnectivityChangedListener != null) {
                networkConnectivityChangedListener.onNetworkConnectivityChanged(networkStatus);
            }
        }
    }


    private boolean isNetowrkConnected() {

        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) RVSSdk.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNerworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((mobileNerworkInfo != null && mobileNerworkInfo.getState() == NetworkInfo.State.CONNECTED) ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else {
            connected = false;
        }

        return connected;
    }

    /**
     * Check internet http request by domain
     */
    public void checkHTTPConnection() {

        if (!isNetowrkConnected()) {
            updateNetworkStatus(false);
        } else {
            UrlConnectionExistAsyncTask task = new UrlConnectionExistAsyncTask(new UrlConnectionExistTaskCompleteListener() {
                @Override
                public void onTaskComplete(boolean isConnected) {
                    updateNetworkStatus(isConnected);
                }
            });
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, domain);
        }
    }

    public String getSavedDencryptedValue(SavedEncryptedType type) {

        String key = null;
        switch (type) {
            case FacebokToken:
                key = "RVSServiceFacebookToken";
                break;
            case TwitterToken:
                key = "RVSServiceTwitterToken";
                break;
            case TwitterSecret:
                key = "RVSServiceTwitterSecret";
                break;
            case Password:
                key = "RVSServicePassword";
                break;
            default:
                logger.error("wrong saved encrypted type");
                return null;
        }

        String token = null;
        if (key != null) {

            SharedPreferences sharedPref = RVSSdk.getSharedPreferences(); 
            token = sharedPref.getString(key, null);

            if (token != null) {

                try {
                    token = Crypto.decrypt(pwd, token);
                } catch (Exception e) {
                    logger.error("decryption failed");
                    e.printStackTrace();
                    token = null;
                }
            }
        }

        return token;
    }

    /**
     * Sign in to the service using credentials.
     * Once signed in the service object itself will handle network disconnections.
     * RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
     * User credentials will be stored by the service object for automatic sign in when started.
     *
     * @param userName user name to use for signing in to the service
     * @param password password to use for signing in to the service
     */
    public void signInWithUserName(String userName, String password) {
        logger.info("sigining in with user " + userName);

        Utils.assertInEmptyString(userName);
        Utils.assertInEmptyString(password);
        if (!isSignedOut()) {
            throw new AssertionError("Must be sign out first");
        }


        clearSignInInfo();

        try {

            SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
            SharedPreferences.Editor editor = sharedPref.edit();
            // store user since request manager will need those to get my user
            editor.putString("RVSServiceUserName", userName);
            editor.putString("RVSServicePassword", Crypto.encrypt(pwd, password));
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        authenticateAndNotify();
    }

    @SuppressWarnings("unchecked")
    private void authenticateAndNotify() {
    	
        doAuthenticate();

        authenticatePromise.then(new AndroidDoneCallback<RVSAuthenticate>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(RVSAuthenticate result) {
                if (!authenticatePromise.isCancelled()) {

                    // todo workaround to send only one event of multiple authenticatePromise request
                    authenticatePromise.cancel();
                    authenticatePromise = null;
                    
                    // todo workaround, check why RVSServiceManager.this.autheticateResult is null
                    if (RVSServiceManager.this.authenticateResult == null) RVSServiceManager.this.authenticateResult = result;

                    Intent intent = null;
                    if (isEmailEmpty() || isHandleRequired() || isNewExternalUser())
                    {
                    	logger.info("signed in pending");
                        setIsPendingSignIn(true);
	                    intent = new Intent(RVSServiceSignedInPendingNotification);
                    }
                    else
                    {

	                    logger.info("signed in");
	                    
	                    RVSServiceManager.this.setIsSignedOut(false);
	                    intent = new Intent(RVSServiceSignedInNotification);
                    }
                    
                    LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);

                    


                } else {
                    logger.info("authenticatePromise was cancelled or intent has been already sent, skip done callback");
                }
            }

        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                logger.error("failed to sign in with error: " + error.getMessage());

                if (!authenticatePromise.isCancelled()) {

                    // todo workaround to send only one event of multiple authenticatePromise request
                    authenticatePromise.cancel();

                    Intent intent = new Intent(RVSServiceSignInErrorNotification);
                    if (error != null && error instanceof RVSError) {
                        intent.putExtra("error", ((RVSError) error).getErrorCode());
                    }
                    LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);

                } else {
                    logger.info("authenticatePromise cancelled, skip fail callback");
                }

            }
        });
    }

    private void setIsPendingSignIn(boolean isPending) {
    	
    	SharedPreferences.Editor preferencesEditor = RVSSdk.getSharedPreferences().edit();
        preferencesEditor.putBoolean("RVSServicePendingSignedIn", isPending);
        preferencesEditor.commit();
		
	}
    
    public Boolean isPendingSignIn() {
    	return RVSSdk.getSharedPreferences().getBoolean("RVSServicePendingSignedIn", false);
    }
    
    public void cancelPendingSignIn() {
    	
        logger.info("cancel pending sign in");

        clearSignInInfo();
        setIsPendingSignIn(false);
    }

    @SuppressWarnings("unchecked")
    public RVSPromise doAuthenticate() {

        // we synchoronize the singleton authenticate promise by allowing it only from main thread so no threading issues
        Utils.assertCalledNotFromMainThread();

        logger.info("doAuthenticate");
        // if already in the middle of authenticating just return the current promise
        if (authenticatePromise != null && authenticatePromise.isPending()) {

        	logger.info("doAuthenticate in process");
        	return authenticatePromise;
        }

        authenticateResult = null;
//        sessionToken = null;
//        authenticateResult = null;
        
        authenticatePromise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);

        String facebookToken = getSavedDencryptedValue(SavedEncryptedType.FacebokToken);
        if (facebookToken != null) {
            HashMap<String, String> parameters = new HashMap<String, String>();
            parameters.put("facebookToken", facebookToken);
//            authenticateRequestPromise = RVSSdk.sharedRVSSdk().requestManager().post("authenticate/facebook", parameters);
            authenticateRequestPromise = RVSSdk.sharedRVSSdk().requestManager().postJsonObject("authenticate/facebook", parameters, RVSAuthenticate.class);
            
        } else {
            String twitterToken = getSavedDencryptedValue(SavedEncryptedType.TwitterToken);
            String twitterSecret = getSavedDencryptedValue(SavedEncryptedType.TwitterSecret);

            if (twitterToken != null && twitterSecret != null) {
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("accessToken", twitterToken);
                parameters.put("accessTokenSecret", twitterSecret);

//                authenticateRequestPromise = RVSSdk.sharedRVSSdk().requestManager().post("authenticate/twitter", parameters);
                authenticateRequestPromise = RVSSdk.sharedRVSSdk().requestManager().postJsonObject("authenticate/twitter", parameters, RVSAuthenticate.class);

            } else {
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("username", getUserName());
                parameters.put("password", getSavedDencryptedValue(SavedEncryptedType.Password));

                authenticateRequestPromise = RVSSdk.sharedRVSSdk().requestManager().postJsonObject("authenticate", parameters, RVSAuthenticate.class);
            }
        }

        authenticateRequestPromise.then(new AndroidDoneCallback<RVSAuthenticate>() {
            @Override
            public void onDone(RVSAuthenticate result) {

                try {
//                    JSONObject json = new JSONObject(result);
                	RVSServiceManager.this.authenticateResult = result;
//                    RVSServiceManager.this.sessionToken = result.json.getString("sessionToken");
//                    RVSServiceManager.this.userId = json.getString("userId");
                    
                    
//                    weakSelf.user = [[RVSUser alloc] initWithDictionary:authenticationDetails[@"extendedUserProfile"] error:&error];
//                    
//                    weakSelf.sessionToken = authenticationDetails[@"sessionToken"];
//                    weakSelf.userId = authenticationDetails[@"userId"];
//                    weakSelf.isEmailEmpty = [authenticationDetails[@"isEmailEmpty"] boolValue];
//                    weakSelf.isNewUser = [authenticationDetails[@"isNewUser"] boolValue];
//                    weakSelf.isHandleRequired = [authenticationDetails[@"isHandleRequired"] boolValue];

                    
                    
                    logger.info("authenticated with user " + RVSServiceManager.this.authenticateResult.getUserId());
                    RVSServiceManager.this.authenticatePromise.resolve(result);

                } catch (Exception e) {
                    e.printStackTrace();
                    RVSServiceManager.this.authenticatePromise.reject(e);
                }
            }

            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {

                logger.error("failed to authenticate in with error: " + error.getMessage());
                if (authenticatePromise != null) {
                	authenticatePromise.reject(error);
                }

                // if we were signed in and error is bad credentials then signout
                if (error instanceof RVSError && ((RVSError) error).getRVSErrorCode() == RVSErrorCode.RVSErrorCodeSignInRequired) {
                    logger.info("was signed in so signing out");
                    signOutInternal(((RVSError) error));
                }

            }
        });

        authenticatePromise.fail(new FailCallback() {
            @Override
            public void onFail(Object result) {
                if (authenticatePromise.isCancelled()) {
                    authenticateRequestPromise.cancel();
                }
            }
        });

        return authenticatePromise;
    }

    private void signOutInternal(RVSError error) {

        setIsSignedOut(true);

        Intent intent = new Intent(RVSServiceSignedOutNotification);
        if (error != null) {
            intent.putExtra("error", error.getMessage());
        }

        LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);
    }

    private String getUserName() {

        SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
        return sharedPref.getString("RVSServiceUserName", "");
    }
    
    private void setUserName(String username) {
        SharedPreferences.Editor editor = RVSSdk.getSharedPreferences().edit();
        editor.putString("RVSServiceUserName", username);
        editor.commit();
    }

    private void clearSignInInfo() {

        // not valid anymore
    	authenticateResult = null;
//        userId = null;
//        sessionToken = null;

        // clear credentials
    	SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Sign in to the service using facebook token.
     * Once signed in the service object itself will handle network disconnections.
     * RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
     * Facebook token will be stored by the service object for automatic sign in when started.
     *
     * @param token facebook token to use for signing in to the service
     */
    public void signInWithFacebookToken(String token) {
        logger.info("signing in with facebook token " + token);

//        RVSParameterAssert(![token isEmpty]);
//        RVSAssert(self.isSignedOut);

        clearSignInInfo();

        try {

        	SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("RVSServiceFacebookToken", Crypto.encrypt(pwd, token));
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        authenticateAndNotify();
    }

    /**
     * Sign in to the service using twitter token.
     * Once signed in the service object itself will handle network disconnections.
     * RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
     * Twitter token will be stored by the service object for automatic sign in when started.
     *
     * @param token  twitter token to use for signing in to the service
     * @param secret twitter secret to use for signing in to the service
     */
    public void signInWithTwitterToken(String token, String secret) {
        logger.info("sigining in with twitter token " + token + " secret " + secret);

//        RVSParameterAssert(![token isEmpty]);
//        RVSParameterAssert(![secret isEmpty]);
//        RVSAssert(self.isSignedOut);

        clearSignInInfo();

        try {

        	SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
        	SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("RVSServiceTwitterToken", Crypto.encrypt(pwd, token));
            editor.putString("RVSServiceTwitterSecret", Crypto.encrypt(pwd, secret));
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        authenticateAndNotify();
    }
    
    /**
     * Register using email
     */
    @SuppressWarnings("unchecked")
    public RVSPromise registerWithEmail(final String email, final String password, final String name, String handle) {
        
    	final RVSPromise promise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);
    	
        HashMap<String, String> parameters = new HashMap<String, String>(); 
        parameters.put("email",email);
        parameters.put("password",password);
        parameters.put("name",name);
        parameters.put("handle",handle);
//        parameters.put("provisioningOperation","ADD");

//        RVSSdk.sharedRVSSdk().requestManager().post("users/registration", parameters);
        RVSPromise registrationPromise = RVSSdk.sharedRVSSdk().requestManager().postJsonObject("users/registration", parameters, RVSAuthenticate.class);
        registrationPromise.then(new DoneCallback<RVSAuthenticate>() {
            @Override
            public void onDone(RVSAuthenticate result) {
            	
                RVSServiceManager.this.authenticateResult = result;
                RVSServiceManager.this.setIsSignedOut(false);
                
                try {
	                // store username & password
	                SharedPreferences sharedPref = RVSSdk.getSharedPreferences();
	                SharedPreferences.Editor editor = sharedPref.edit();
	                // store user since request manager will need those to get my user
	                editor.putString("RVSServiceUserName", email);
	                editor.putString("RVSServicePassword", Crypto.encrypt(pwd, password));
	                editor.commit();
                } catch (Exception e) {
                	e.printStackTrace();
                }
                
                promise.resolve(result);
            }

        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                promise.reject(error);

            }
        });

        return promise;
    }

    /**
     * Sign out from the service and delete credentials
     */
    public void signOut() {
        logger.info("signing out");

        RVSError error = null;
        if (isSignedOut()) {
            error = new RVSError("Already signed out");
//            throw new AssertionError("Already signed out");
        }
        clearSignInInfo();
        signOutInternal(error);

    }

    public boolean isSignedOut() {
        return RVSSdk.getSharedPreferences().getBoolean("RVSServiceSignedIn", true);
    }

    public void setIsSignedOut(boolean isSignedOut) {
        SharedPreferences.Editor editor = RVSSdk.getSharedPreferences().edit();
        editor.putBoolean("RVSServiceSignedIn", isSignedOut);
        editor.commit();

        if (isSignedOut && authenticatePromise != null) {
            authenticatePromise.cancel();
            authenticatePromise = null;
        }
    }

    /**
     * Current network connection status
     *
     * @return true if got network
     */
    public boolean isNetworkConnected() {
        // TODO
        return false;
    }

    /**
     * My user ID, nil if not singed in
     */
    public String getMyUserId() {
        return authenticateResult == null ? null : authenticateResult.getUserId();
    }

    /**
     * My session token, nil if not singed in
     */
    public String getSessionToken() {
        return authenticateResult == null ? null : authenticateResult.getSessionToken();
    }

    /**
     * Get a configuration document for the client
     *
     * @return promise fulfilled with configuration (NSDictionary *)
     */
    public RVSPromise getConfiguration() {
        // TODO
        return RVSPromise.createPromise();
    }

    @Override
    public void onNetworkConnectivityChanged() {
        checkHTTPConnection();
    }

    public void setNetworkConnectivityChangedListener(NetworkConnectivityChanged networkConnectivityChangedListener) {
        this.networkConnectivityChangedListener = networkConnectivityChangedListener;
    }


    /**
     *  Get a unique device identifier
     *  @return device identifier
     */
    public String getUniqueDeviceIdentifier() {
        {
            if (uniqueUserId == null)
            {
                synchronized (RVSServiceManager.class)
                {
                    if (uniqueUserId == null)
                    {
                        Context context = RVSSdk.getContext();

                        final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, Activity.MODE_PRIVATE);
                        final String id = prefs.getString(PREFS_DEVICE_ID, null);
                        if (id != null)
                        {
                            // Use the ids previously computed and stored in the
                            // prefs file
                            uniqueUserId = UUID.fromString(id);
                        } else
                        {
                            final String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                            // Use the Android ID unless it's broken, in which case fallback on deviceId,
                            // unless it's not available, then fallback on a random number which we store to a prefs file
                            try
                            {
                                if (!PROBLEMAITC_DEVICE_ID.equals(androidId))
                                {
                                    uniqueUserId = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
                                } else
                                {
                                    String deviceId = null;
                                    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY))
                                    {
                                        deviceId = ((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                                    }
                                    uniqueUserId = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
                                }
                            } catch (UnsupportedEncodingException e)
                            {
                                throw new RuntimeException(e);
                            }

                            // Write the value out to the prefs file
                            prefs.edit().putString(PREFS_DEVICE_ID, uniqueUserId.toString()).commit();
                        }
                    }
                }
            }

            uniqueUserIdStr = uniqueUserId.toString().replaceAll("[^A-Za-z0-9]", "");
            return uniqueUserIdStr;
        }
    }

	public boolean isNewExternalUser() {
		return this.authenticateResult.isNewExternalUser();
	}

	public boolean isEmailEmpty() {
		return this.authenticateResult.isEmailEmpty();
	}
	
	public RVSUser getMyUser() {
		return this.authenticateResult != null ? this.authenticateResult.getUserExtended() : null;
	}

	public String getMyEmail() {
		return this.authenticateResult.getEmail();
	}
	
	public boolean isHandleRequired() {
		// hack for production env till back end release 
//		return this.authenticateResult == null ||  this.authenticateResult.isHandleRequired() == null ? false : this.authenticateResult.isHandleRequired();
		
		return this.authenticateResult.isHandleRequired();
	}

	public void retrySignIn() {
		if (!isSignedOut()) {
		    throw new AssertionError("User must be singed out");
		}
		  
		this.authenticateAndNotify();
	}

	@SuppressWarnings("unchecked")
	public RVSPromise updateUserProfileWithName(String name, String handle, String email) {
		
		Utils.assertCalledNotFromMainThread();
		Utils.assertInEmptyString(getUserId());
		   
		    
	    RVSUpdateUserProfile updateUserProfile = new RVSUpdateUserProfile(name, handle, email);
	    RVSPromise promise = updateUserProfile.updateProfile();
	    promise.then(new DoneCallback() {

			@Override
			public void onDone(Object result) {
		    	if (isPendingSignIn())
		        {
		            setIsPendingSignIn(false);
		            setIsSignedOut(false);
		            
                    Intent intent = new Intent(RVSServiceSignedInNotification);
                    LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);
		        }
//		        weakSelf.user = updatedUser;				
			}
		});
	    
	    return promise;
	}

	private String getUserId() {
		
		return this.authenticateResult.getUserId();	
	}


}
