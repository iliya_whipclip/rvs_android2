package com.whipclip.rvs.sdk.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


public class RVSNotificationCenter
{
    Logger logger = LoggerFactory.getLogger(RVSNotificationCenter.class);

    private class NotificationObserverWrapper
	{
		WeakReference<Object> identifier;
		String name;
		WeakReference<RVSNotificationObserver> observer;
		WeakReference<Object> sender;
		
		NotificationObserverWrapper(Object identifier, RVSNotificationObserver observer, String name, Object sender)
		{
			this.identifier = new WeakReference<Object>(identifier);
			this.observer = new WeakReference<RVSNotificationObserver>(observer);
			this.name = name;
			if (sender != null)
			{
				this.sender = new WeakReference<Object>(sender);
			}
		}
	}
	
	private Hashtable<String, CopyOnWriteArrayList<NotificationObserverWrapper>> observerHash;
	
	RVSNotificationCenter()
	{
		observerHash = new Hashtable<String, CopyOnWriteArrayList<NotificationObserverWrapper>>();
	}
	
	/**
	 * Adds an entry to the receiver's dispatch table with an identifier, observer, name and optional sender criteria
	 * @param identifier - key used to identify observer callback
	 * @param observer - specifies the callback when notification sent
	 * @param name - the name of the notification for which to register the observer
	 * @param sender [optional]  - the object whose notifications the observer wants to receive
	 */
	public void addObserver(Object identifier, RVSNotificationObserver observer, String name, Object sender)
	{
		logger.info("notify AddObserver identifier: " + identifier + ", name: " + name + ", sender: " + sender);
		
		synchronized (observerHash)
		{
			NotificationObserverWrapper observerWrapper = new NotificationObserverWrapper(identifier, observer, name, sender);
			CopyOnWriteArrayList<NotificationObserverWrapper> observerList = observerHash.get(name);
			
			if (observerList == null)
			{
				observerList = new CopyOnWriteArrayList<NotificationObserverWrapper>();
				observerHash.put(name, observerList);
			}
			else
			{
				// check if observer with current identifier for name notification already exists
				Iterator<NotificationObserverWrapper> iterator = observerList.iterator();
				while (iterator.hasNext())
				{
					NotificationObserverWrapper next = iterator.next();
					if (next.identifier.get() == identifier && next.name.equals(name))
					{
						observerList.remove(next);
						break;
					}
				}
			}

			observerList.add(observerWrapper);
		}
	}
	
	
	/**
	 * Removes matching entries from the receiver�s dispatch table by identifier, name and optional sender criteria
	 * @param identifier - key used to identify observer callback
	 * @param name - name of the notification to remove from dispatch table
	 * @param sender [optional] - sender to remove from dispatch table
	 */
	public void removeObserver(Object identifier, String name, Object sender)
	{
		logger.info("notify removeObserver identifier: " + identifier + "name: " + name + ", sender: " + sender);
		synchronized (observerHash)
		{
			if (name != null) // remove observer by identifier & name/sender
			{
				if (observerHash.contains(name) == false)
				{
					return;
				}
				
				CopyOnWriteArrayList<NotificationObserverWrapper> observerList = observerHash.get(name);
				
				Iterator <NotificationObserverWrapper> iterator = observerList.iterator();
				while(iterator.hasNext())
				{
					NotificationObserverWrapper observerWrapper = iterator.next();
					if (observerWrapper.identifier.get() == identifier)
					{
						if (sender == null)
						{
							observerList.remove(observerWrapper);
						}
						else if (sender.equals(sender))
						{
							observerList.remove(observerWrapper);
						}
						
						if (observerList.size() == 0)
						{
							observerHash.remove(name);
						}
					}
				}
			}
			else
			{
				// remove all observers only by identifier
				Set<String> keySet = observerHash.keySet();
				Iterator<String> kyeIterator = keySet.iterator();
				while(kyeIterator.hasNext())
				{
					String key = kyeIterator.next();
					CopyOnWriteArrayList<NotificationObserverWrapper> list = observerHash.get(key);
					Iterator<NotificationObserverWrapper> observerIterator = list.iterator();
					while(observerIterator.hasNext())
					{
						NotificationObserverWrapper observerWrapper = observerIterator.next();
						if (observerWrapper.identifier.get() == identifier)
						{
							list.remove(observerWrapper);
						}
					}
					
					if (list.size() == 0)
					{
						observerHash.remove(name);
					}
				}
			}
		}
	}
	
	/**
	 * Post a notification with a given notification and optional sender criteria.
	 * @param notification - notification to post 
	 * @param sender - the object posting the notification
	 */
	public void postNotification(String name, Object sender, Map<String, Object> userInfo)
	{
//		Logger.LOG_INFO("postNotification name: " + name + ", sender: " + sender);
		
		RVSNotificationEvent notification = new RVSNotificationEvent(name, sender, userInfo);
		
		CopyOnWriteArrayList<NotificationObserverWrapper> list;
		synchronized (observerHash)
		{
			list = observerHash.get(name);
		}
		
		if (list == null)
		{
			return;
		}
		
		for (NotificationObserverWrapper observerWrapper : list)
		{
			RVSNotificationObserver observer = observerWrapper.observer.get();
			if (observer == null)
			{
				logger.error("NotificationCenter.postNotification weakreference is null, notification name: " + observerWrapper.name);
			}
			else 
			{
//				observer.onNotification(notification);
				
				if (sender == null || observerWrapper.sender == null)
				{
					observer.onNotification(notification);
				}
				else if (observerWrapper.sender.get() == null)
				{
                    logger.error("NotificationCenter.postNotification sender weakreference is null, notification name: " + observerWrapper.name);
				}
				else if (observerWrapper.sender.get() == sender)
				{
					observer.onNotification(notification);
				}
			}
		}
	 }
}
