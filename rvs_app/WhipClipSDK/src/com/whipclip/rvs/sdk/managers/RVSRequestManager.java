package com.whipclip.rvs.sdk.managers;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;
import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSErrorCode;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.util.Utils;


/**
 * Created by iliya on 6/10/14.
 */
public class RVSRequestManager {

    Logger logger = LoggerFactory.getLogger(RVSRequestManager.class);
    Handler handler;

	private static String RVSSdkSettingsPrefName  = "RVSSdkSettingsPrefName";
	private static String RVSSdkAppVersionKey  = "RVSSdkAppVersionKey";
    public static String RVSSdkBaseUrlUserDefaultsKey = "RVSSdkBaseUrlUserDefaultsKey";
    public static String RVSSdkOverrideIPAddressUserDefaultsKey = "RVSSdkOverrideIPAddressUserDefaultsKey";
    
    public enum RVSRequestManagerHttpMethod {
        GET,
        PUT,
        POST,
        DELETE
    };

    private static String RVSSdkClientVersion = "0.0.0";
    private static String RVSSdkBackendApiVersion = "2.5";
    private int DEFAULT_HTTP_TIMEOUT = 30 * 1000;
    private int HTTP_MAX_CONNECTIONS = 15;
    
    private AsyncHttpClient asyncHttpClient;
    private SyncHttpClient syncHttpClient;
    private String baseUrlString;
	
    private String RVSdkOverrideIPAddress = "";

    public RVSRequestManager() {

        //update client version if available
        Context context = RVSSdk.getContext();
        handler = new Handler(Looper.getMainLooper());

        try
        {
            RVSSdkClientVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
//        	SharedPreferences sharedPref = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE);
//        	storeAppVersion = sharedPref.getString(RVSSdkAppVersionKey, "");
            
        }
        catch (PackageManager.NameNotFoundException e)
        {
            logger.error(e.getMessage());
        }

        baseUrlString = Utils.getConfigParameter("RVSSdkBaseUrlUserDefaultsKey");

        if (baseUrlString == null || baseUrlString.length() == 0) {
            baseUrlString = getBaseUrlFromReferences();

            if (baseUrlString == null || baseUrlString.length() == 0) {
                // set default (currently production)
//                baseUrlString = "https://api-alpha3-s.whipclip.com/v1/";
//                baseUrlString = "https://api-integration-s.whipclip.com/v1/";
                baseUrlString = "https://api-prod02-s.whipclip.com/v1/";
            }
        }

        if (baseUrlString != null && baseUrlString.length() > 0) {

            //verify ends with '/'
            if (!baseUrlString.endsWith("/")) {
                baseUrlString += "/";
            }
           
//            setBaseUrl(baseUrlString);
            
//            SharedPreferences.Editor preferencesEditor = RVSSdk.getSharedPreferences().edit();
//            preferencesEditor.putString(RVSSdkBaseUrlUserDefaultsKey, baseUrlString);
//            preferencesEditor.commit();
//            asyncHttpClient = new AsyncHttpClient();
            
//          https://github.com/loopj/android-async-http/issues/288
            asyncHttpClient = new AsyncHttpClient(true, 80, 443);
            asyncHttpClient.setTimeout(DEFAULT_HTTP_TIMEOUT);
            
            syncHttpClient  = new SyncHttpClient(true, 80, 443);
            syncHttpClient.setTimeout(DEFAULT_HTTP_TIMEOUT);
            syncHttpClient.setMaxConnections(HTTP_MAX_CONNECTIONS);
//            syncHttpClient.setMaxRetriesAndTimeout(3, DEFAULT_HTTP_TIMEOUT);
            
            
            try {
	            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	            trustStore.load(null, null);
	            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            asyncHttpClient.setSSLSocketFactory(sf);   
            }
            catch (Exception e) {   
            }
            
            //use override IP Address if exists
        	SharedPreferences sharedPref = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE);
        	RVSdkOverrideIPAddress = sharedPref.getString(RVSSdkOverrideIPAddressUserDefaultsKey, "");
            
        } else {
            throw new AssertionError("Base URL must be provided");
        }

//        self = [super initWithBaseURL:[NSURL URLWithString:baseUrlString]];

//        self.requestSerializer = [RVSRequestSerializer serializer];
//        self.responseSerializer = [RVSResponseSerializer serializer];
//        self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObject:@"application/x-mpegurl"];
    }

    public void storeAppVersion(String version) {
    	
    	logger.info("version: " + version);
    	
    	SharedPreferences.Editor preferencesEditor = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE).edit();
        preferencesEditor.putString(RVSSdkAppVersionKey, version);
        preferencesEditor.commit();
    }
  
    public String getBaseUrl() {
    	return baseUrlString;
    }
  
    public void storeBaseUrl(String url) {
    	
    	logger.info("serBaseUrl: " + url);
//    	baseUrlString = url;
    	
    	SharedPreferences.Editor preferencesEditor = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE).edit();
        preferencesEditor.putString(RVSSdkBaseUrlUserDefaultsKey, url);
        preferencesEditor.commit();
    }
    
    public void setOverrideIp(String ip) {
    	
    	SharedPreferences.Editor preferencesEditor = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE).edit();
        preferencesEditor.putString(RVSSdkOverrideIPAddressUserDefaultsKey, ip);
        preferencesEditor.commit();
    }
    
    private String getBaseUrlFromReferences() {
    	SharedPreferences sharedPref = RVSSdk.getContext().getSharedPreferences(RVSSdkSettingsPrefName, Context.MODE_PRIVATE);
    	return sharedPref.getString(RVSSdkBaseUrlUserDefaultsKey, null);
    }
    
    public String getBaseUrlString() {
        return baseUrlString;
    }


    public RVSPromise put(String method,  HashMap<String, String> parameters) {
        return requestWithHttpMethod(RVSRequestManagerHttpMethod.PUT, method, parameters);
    }

    public <T> RVSPromise putJsonObject(String method, HashMap<String, String> parameters, Class<T> objectClass) {
        return putJsonObjectProperty(method, parameters, objectClass, null);
    }
    
    public <T> RVSPromise putJsonObjectProperty(final String method,final HashMap<String, String> parameters,final Class<T> objectClass, final String propertyName) {

        final RVSPromise promise = RVSPromise.createPromise();
        final RVSPromise responsePromise = promise;
        // TODO add RVSPromise which return property name
//        if (propertyName != null) {
//            responsePromise = RVSPromise.createPromise();
//        }

        // post to ui thread, since JsonResponseHandler must be initialized in thread with looper (used  to handle callbacks via internal handler) !
        handler.post(new Runnable() {
            @Override
            public void run() {

                HashMap<String, Object> requestParams = new HashMap<String, Object>();
                requestParams.put("method", method);
                requestParams.put("httpMethod", RVSRequestManagerHttpMethod.PUT);
                requestParams.put("params", parameters);
                requestWithHttpMethod(RVSRequestManagerHttpMethod.PUT, method, parameters, getJsonResponseHandler(requestParams, responsePromise, objectClass, true), responsePromise);
            }
        });

        return promise;
    }
    
    public RVSPromise delete(String method, HashMap<String, String> parameters) {
        return requestWithHttpMethod(RVSRequestManagerHttpMethod.DELETE, method, parameters);
    }


    public RVSPromise getURL(String url, HashMap<String, String> parameters) {
    	
    	final RVSPromise promise = RVSPromise.createPromise();
        HashMap<String,Object> requestParams = new HashMap<String, Object>();
        requestParams.put("method", RVSRequestManagerHttpMethod.GET);
        requestParams.put("params", parameters);

        logger.info("getURL " + url);
        AsyncHttpResponseHandler responseHandler = getAsyncHttpResponseHandler(requestParams, promise);

        addSessionToken();

        Context context = RVSSdk.getContext();
        url =  appendRequestByCommonQueryString(url);
        logger.debug("requestWithHttpMethod url " + url);

        RequestParams params = null;
        if (parameters != null) {
            Utils.printHashMap(logger, "parameters", parameters);
            params = new RequestParams(parameters);
        }
        asyncHttpClient.get(context, url, params, responseHandler);
        
        return promise;
    }
    
    public RVSPromise get(String method, HashMap<String, String> parameters) {
        return requestWithHttpMethod(RVSRequestManagerHttpMethod.GET, method, parameters);
    }

    public RVSPromise post(String method, HashMap<String,String> parameters) {
        return requestWithHttpMethod(RVSRequestManagerHttpMethod.POST, method, parameters);
    }

    private RVSPromise requestWithHttpMethod(RVSRequestManagerHttpMethod httpMethod, String method, HashMap<String,String> parameters) {

        final RVSPromise promise = RVSPromise.createPromise();
        HashMap<String,Object> requestParams = new HashMap<String, Object>();
        requestParams.put("method", method);
        requestParams.put("httpMethod", httpMethod);
        requestParams.put("params", parameters);

        logger.info("requestWithHttpMethod AsyncHttpResponseHandler <" + httpMethod.name() +"> method " + method);

        requestWithHttpMethod(httpMethod, method, parameters, getAsyncHttpResponseHandler(requestParams, promise), promise);
        return promise;
    }

    private void requestWithHttpMethod(final RVSRequestManagerHttpMethod httpMethod, final String method, final HashMap<String,String> parameters, final ResponseHandlerInterface responseHandler, RVSPromise promise) {

        RequestParams params = null;
        StringEntity entity = null;
        
        addSessionToken();

        Context context = RVSSdk.getContext();
        String url = baseUrlString + method;
        url =  appendRequestByCommonQueryString(url);
        logger.debug("requestWithHttpMethod url " + url);

        RequestHandle requestHandler = null;
        
        switch (httpMethod) {
            case GET:
                if (parameters != null) {
                    Utils.printHashMap(logger, "parameters", parameters);
                    params = new RequestParams(parameters);
                }
                requestHandler = asyncHttpClient.get(context, url, params, responseHandler);
                break;
            case POST:
                try {
                    if (parameters != null) {
                        Utils.printHashMap(logger, "parameters", parameters);

                        JSONObject json = new JSONObject(parameters);
                        entity = new StringEntity(json.toString(), "UTF-8");
                    }
                    requestHandler = asyncHttpClient.post(context, url,  entity, RequestParams.APPLICATION_JSON, responseHandler);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    promise.reject(e);
                }
                break;
            case DELETE:
                // check this code in case we need to send params to delete method
//                if (parameters != null) {
//                    Utils.printHashMap(logger, "parameters", parameters);
//                    params = new RequestParams(parameters);
//                }
//                asyncHttpClient.delete(context, url, null, params, responseHandler);

            	requestHandler = asyncHttpClient.delete(context, url, responseHandler);

                break;
            case PUT:
            	 entity = null;
            	 try {
	                if (parameters != null) {
	                    Utils.printHashMap(logger, "parameters", parameters);
	                    params = new RequestParams(parameters);
	                    
	                    JSONObject json = new JSONObject(parameters);
	                    entity = new StringEntity(json.toString(), "UTF-8");
	                }
                    requestHandler = asyncHttpClient.put(context, url,  entity, RequestParams.APPLICATION_JSON, responseHandler);
            	 } catch (Exception e) {
            		  e.printStackTrace();
                      promise.reject(e);
				}
                break;
        }
        
        promise.setRequestHandler(requestHandler, url);
    }

    private void addSessionToken() {

        String sessionToken = RVSSdk.sharedRVSSdk().serviceManager().getSessionToken();
//        logger.info("add sessionToken " + sessionToken);
        if (sessionToken != null) {
            asyncHttpClient.addHeader("Authorization", String.format("Bearer %s",sessionToken));
        } else {
            asyncHttpClient.addHeader("Authorization", null);
        }
    }

    private String appendRequestByCommonQueryString(String request) {
        try
        {
            URL url = new URL(request);
            		
            String query = String.format("useSecureLinks=true&apiVersion=%s&clientVersion=%s", RVSSdkBackendApiVersion, RVSSdkClientVersion);
            
            if (RVSdkOverrideIPAddress != null && !RVSdkOverrideIPAddress.isEmpty()){
            	query = String.format(query + "&overrideIpAddress=%s", RVSdkOverrideIPAddress);
            }
            
            try {
                query = URLEncoder.encode(query, "utf-8");
                request = String.format("%s%s%s", request, url.getQuery() != null ?  "&" : "?" , query);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            logger.debug(e.getMessage());
            logger.debug("url was:" + request);
        }

        return request;
    }

    public <T> RVSPromise postJsonObject(String method, HashMap<String, String> parameters, Class<T> objectClass) {
        return postJsonObjectProperty(method, parameters, objectClass, null);
    }

    public <T> RVSPromise getJsonObject(String method, HashMap<String, String> parameters, Class<T> objectClass) {
        return getJsonObjectProperty(method, parameters, objectClass, null);
    }

    public <T> RVSPromise getJsonObjectProperty(final String method,final HashMap<String, String> parameters,final Class<T> objectClass, final String propertyName) {

        final RVSPromise promise = RVSPromise.createPromise();
//        final RVSPromise responsePromise = promise;
        // TODO add RVSPromise which return property name
//        if (propertyName != null) {
//            responsePromise = RVSPromise.createPromise();
//        }

        logger.info("requestWithHttpMethod JsonResponseHandler <GET> method " + method);
        final HashMap<String, Object> requestParams = new HashMap<String, Object>();
        requestParams.put("method", method);
        requestParams.put("httpMethod", RVSRequestManagerHttpMethod.GET);
        requestParams.put("params", parameters);
        requestParams.put("propertyName", propertyName);


        Looper myLopper = Looper.myLooper();
        if (myLopper != null && Looper.getMainLooper() == myLopper) { // in UI thread
            requestWithHttpMethod(RVSRequestManagerHttpMethod.GET, method, parameters, getJsonResponseHandler(requestParams, promise, objectClass, true), promise);

        } else {
            // post to ui thread, since JsonResponseHandler must be initialized in thread with looper (used  to handle callbacks via internal handler) !
            handler.post(new Runnable() {
                @Override
                public void run() {
                    requestWithHttpMethod(RVSRequestManagerHttpMethod.GET, method, parameters, getJsonResponseHandler(requestParams, promise, objectClass, true), promise);
                }
            });
        }

//        if (propertyName != null) {
//            responsePromise.then(new DoneCallback<Class<T>() {
//                @Override
//                public void onDone(Class<T> result) {
//
//                    try
//                    {
//                        String methodName = "get" + propertyName;
//                        Method method = result.getMethod(methodName);
//                        promise.resolve(method.invoke(this));
//                    }
//                    catch (Exception e)
//                    {
//                        promise.reject("No paramter");
//                    }
//                }
//            });
//        }

        return promise;
    }

    public <T> RVSPromise postJsonObjectProperty(final String method,final HashMap<String, String> parameters,final Class<T> objectClass, final String propertyName) {

        final RVSPromise promise = RVSPromise.createPromise();
        final RVSPromise responsePromise = promise;
        // TODO add RVSPromise which return property name
//        if (propertyName != null) {
//            responsePromise = RVSPromise.createPromise();
//        }

        // post to ui thread, since JsonResponseHandler must be initialized in thread with looper (used  to handle callbacks via internal handler) !
        handler.post(new Runnable() {
            @Override
            public void run() {

                HashMap<String, Object> requestParams = new HashMap<String, Object>();
                requestParams.put("method", method);
                requestParams.put("httpMethod", RVSRequestManagerHttpMethod.POST);
                requestParams.put("params", parameters);
                requestWithHttpMethod(RVSRequestManagerHttpMethod.POST, method, parameters, getJsonResponseHandler(requestParams, responsePromise, objectClass, true), responsePromise);
            }
        });

//        if (propertyName != null) {
//            responsePromise.then(new DoneCallback<Class<T>() {
//                @Override
//                public void onDone(Class<T> result) {
//
//                    try
//                    {
//                        String methodName = "get" + propertyName;
//                        Method method = result.getMethod(methodName);
//                        promise.resolve(method.invoke(this));
//                    }
//                    catch (Exception e)
//                    {
//                        promise.reject("No paramter");
//                    }
//                }
//            });
//        }

        return promise;
    }

    private <T> ResponseHandlerInterface getJsonResponseHandler(final HashMap<String,Object> requestParams, final RVSPromise promise, final Class<T> objectClass, final boolean retryAuthOnFail) {

        return new BaseJsonHttpResponseHandler<T>() {

            @Override
            public void onStart() {
                logger.debug("onStart");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, T response) {
                logger.info("request success Json");

//                debugHeaders(headers);
//                debugStatusCode(statusCode);
//                if (response != null) {
//                    debugResponse(rawJsonResponse);
//                }

                if (response instanceof String) {
                    logger.info("check - probably got string instead Mapper object");
                }


                promise.resolve(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, T errorResponse) {
                logger.info("request failed");

                debugHeaders(headers);
                debugStatusCode(statusCode);
                debugThrowable(throwable);
                if (errorResponse != null) {
                    debugResponse(rawJsonData);
                }

                if (retryAuthOnFail) {
                    onRequestFailure(promise, requestParams, statusCode, rawJsonData, throwable, objectClass);
                } else {



                    JSONObject responseJson = null;
                    try {
                        RVSError error = new RVSError();
                        if (rawJsonData != null) {
                            error = new RVSError(rawJsonData);
                            responseJson = new JSONObject(rawJsonData);
                            error.setErrorCode(responseJson.getString("errorCode"));
                            error.setHttpCode(responseJson.getString("httpCode"));
                            promise.reject(error);
                        }
                    } catch (Exception e) {

                        promise.reject(throwable);
                    }


                }
            }

            @Override
            protected T parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                logger.info("request parse start: " + rawJsonData);

                ObjectMapper mapper = new ObjectMapper();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                T result = mapper.readValues(new JsonFactory().createParser(rawJsonData), objectClass).next();
                logger.info("request parse end");

                return result;
            }
        };
    }

//    private <T> ResponseHandlerInterface getJsonResponseHandler(final HashMap<String,Object> requestParams, final RVSPromise promise, final Class<T> objectClass) {
//
//        return new BaseJsonHttpResponseHandler<T>() {

    @SuppressWarnings("unchecked")
    private <T> void onRequestFailure(final RVSPromise promise, final HashMap<String, Object> requestParams, int statusCode, String errorResponse, Throwable throwable, final Class<T> objectClass) {

        String detailMessage = "statusCode: " + statusCode + ", message: " + throwable.getMessage();
        logger.error(detailMessage);
        
        RVSError error = new RVSError();

        JSONObject responseJson = null;
        try {
            if (errorResponse != null) {

//                ObjectMapper mapper = new ObjectMapper();
//                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//                error = mapper.readValues(new JsonFactory().createParser(errorResponse), RVSError.class).next();
//                errorCode = error.getErrorCode();

                error = new RVSError(errorResponse);

                responseJson = new JSONObject(errorResponse);
                error.setErrorCode(responseJson.getString("errorCode"));
                error.setHttpCode(responseJson.getString("httpCode"));

                detailMessage = errorResponse;
            }
       

	        String errorCode = error.getErrorCode();
	        if (statusCode == 403 && errorCode != null &&
	                (errorCode.equalsIgnoreCase("USER_AUTHENTICATION_FAILED") || errorCode.equalsIgnoreCase("USER_NOT_AUTHENTICATED"))) {
	
	            RVSError tempError = error;
	            error = new RVSError(RVSErrorCode.RVSErrorCodeSignInRequired, detailMessage);
	            error.setErrorCode(tempError.getErrorCode());
	            error.setHttpCode(tempError.getHttpCode());
	
	            // if not authenticate request and failed because not authenticated then try to authenticate
	            RVSServiceManager serviceManager = RVSSdk.sharedRVSSdk().serviceManager();
	            final String method = (String) requestParams.get("method");
	            if (method.indexOf("authenticate") == -1 && !serviceManager.isSignedOut()) {
	
	                RVSPromise authenticatePromise = serviceManager.doAuthenticate();
	                authenticatePromise.then(new DoneCallback() {
	                    @Override
	                    public void onDone(Object result) {
	                        // retry the request
	                        RVSRequestManagerHttpMethod httpMethod = (RVSRequestManagerHttpMethod) requestParams.get("httpMethod");
	                        HashMap<String,String> httpParams = (HashMap<String, String>) requestParams.get("params");
	                        if (objectClass != null) {
	
	                            requestWithHttpMethod(RVSRequestManagerHttpMethod.GET, method, httpParams, getJsonResponseHandler(requestParams, promise, objectClass, false), promise);
	
	                        } else {
	                            RVSPromise retryPromise = RVSRequestManager.this.requestWithHttpMethod(httpMethod, method, httpParams);
	                            retryPromise.then(new DoneCallback() {
	
	                                @Override
	                                public void onDone(Object result) {
	                                    promise.resolve(result);
	                                }
	
	                            }).fail(new FailCallback<Throwable>() {
	
	
	                                @Override
	                                public void onFail(Throwable error) {
	                                    promise.reject(error);
	                                }
	
	                            });
	                        };
	                    }
	                }).fail(new FailCallback<Throwable>() {
	
	
	                    @Override
	                    public void onFail(Throwable error) {
	                        promise.reject(error);
	                    }
	
	                });
	            } else {
	                promise.reject(error);
	            }
	        } else {
                promise.reject(error);
            }
     
	    } catch (Exception e) {
	    	logger.error("error :" + e.getMessage());
//	        e.printStackTrace();
	    	
//	        promise.reject(throwable);
	    	
	    	promise.reject(e);
        }
    }

    private AsyncHttpResponseHandler getAsyncHttpResponseHandler(final HashMap<String, Object> requestParams, final RVSPromise promise) {

        return new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                logger.info("request success async http");

//                debugHeaders(headers);
//                debugStatusCode(statusCode);
                String responseStr = "";
                if (response != null) {
                    responseStr = new String(response);
                }
                debugResponse(responseStr);
                promise.resolve(new String(responseStr));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable throwable) {
                logger.info("request failed");


                debugHeaders(headers);
                debugStatusCode(statusCode);
                debugThrowable(throwable);
                String errorResponseStr = "";
                if (errorResponse != null) {
                    errorResponseStr = new String(errorResponse);
                }

                debugResponse(errorResponseStr);
                onRequestFailure(promise, requestParams, statusCode, errorResponseStr, throwable, null);

//                if (statusCode == 401 || statusCode == 403) {
//                    promise.reject(new RVSError(RVSErrorCode.RVSErrorCodeSignInRequired, null));
//                } else {
//                    promise.reject(new Error(String.valueOf(e.getMessage())));
//                }
            }
        };
    }

    protected final void debugHeaders(Header[] headers) {
        if (headers != null) {
            logger.debug("Return Headers:");
            StringBuilder builder = new StringBuilder();
            for (Header h : headers) {
                String _h = String.format(Locale.US, "%s : %s", h.getName(), h.getValue());
                logger.debug(_h);
                builder.append(_h);
                builder.append("\n");
            }
        }
    }

    protected final void debugStatusCode(int statusCode) {
        String msg = String.format(Locale.US, "Return Status Code: %d", statusCode);
        logger.debug(msg);

    }

    protected final void debugThrowable(Throwable t) {
        if (t != null) {
            logger.error("AsyncHttpClient returned error", t);
        }
    }

    protected final void debugResponse(String response) {
        if (response != null) {
            logger.debug("Response data:");
            logger.debug(response);
        }
    }

    public String completeURLForMethod(String method, HashMap<String, String> parameters) {

        String requestUrl = baseUrlString + method;
        String request = appendRequestByCommonQueryString(requestUrl);

        if (parameters != null)
        {
            StringBuffer urlString = new StringBuffer(request);
            for (HashMap.Entry<String, String> param : parameters.entrySet()) {
                String key = param.getKey();
                String value = param.getValue();
                urlString.append(String.format("&%s=%s", key, value));
            }

            return urlString.toString();
        }

        return request;

    }

    public void get(final String urlString, final BinaryHttpResponseHandler responseHandler) {
		
    	Utils.assertCalledNotFromMainThread();
    	
        asyncHttpClient.get(urlString, responseHandler);
        
	}

    /**
     * 
     * @param urlString
     * @param responseHandler
     */
	public void getSync(final String urlString, final BinaryHttpResponseHandler responseHandler) {
		
		if (Looper.getMainLooper() ==  Looper.myLooper()) {
			throw new AssertionError("Must be called from not main/ui thread");
		}
		
    	syncHttpClient.get(urlString, responseHandler);
	}
}
