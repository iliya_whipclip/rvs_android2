package com.whipclip.rvs.sdk.managers;

public interface RVSNotificationObserver {
	
	void onNotification(RVSNotificationEvent notification);
}
