package com.whipclip.rvs.sdk.managers;

import java.util.Map;

public final class RVSNotification
{	
	private String m_name;
	private Object m_object;
	private Map<String, Object> m_userInfo;
	
	RVSNotification(String name, Object sender, Map<String, Object> userInfo)
	{
		m_name = name;
		m_object = sender;
		m_userInfo = userInfo;
	}
	
	public String getName()
	{
		return m_name;
	}
	
	public Object getObject()
	{
		return m_object;
	}
	
	public Map<String, Object> getUserInfo()
	{
		return m_userInfo;
	}
}
