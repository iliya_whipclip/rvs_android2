package com.whipclip.rvs.sdk;

import java.util.HashMap;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.whipclip.rvs.sdk.aysncObjects.RVSCreateComment;
import com.whipclip.rvs.sdk.aysncObjects.RVSCreatePost;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetCommentList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetEpgSlotList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetMediaContext;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetNotificationList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetPostList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetSearchChannel;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetSearchContent;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetSearchUser;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetShowList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetTranscriptItemExtendedList;
import com.whipclip.rvs.sdk.aysncObjects.RVSGetUserList;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSFeedCountDetails;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSProgramImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSSearchCompositeResultListImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSShowImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserImp;
import com.whipclip.rvs.sdk.dataObjects.imp.helpers.MediaViewedResult;
import com.whipclip.rvs.sdk.managers.RVSRequestManager;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;
import com.whipclip.rvs.sdk.util.Utils;

;

/**
 * Created by iliya on 6/10/14.
 */
public class RVSSdk {

    public static final String RVSSdkClipPlaybackStartingNotification = "RVSSdkClipPlaybackStartingNotification";
    public static final String RVSSdkClipPlaybackEndedNotification = "RVSSdkClipPlaybackEndedNotification";

    private Logger logger = LoggerFactory.getLogger(RVSSdk.class);
    private int playingClipCount = 0;
    
    private final static String PROBLEMAITC_DEVICE_ID = "9774d56d682e549c";
    private static final String PREFS_DEVICE_ID = "device_id";


    public static class ContentSearchType {
        public static final String PROGRAM = "PROGRAM";
        public static final String POST = "POST";
        public static final String POST_OR_PROGRAM = "POST_OR_PROGRAM";
    }

    public static class PostReportType {
        public static final String SPOILER = "SPOILER";
        public static final String INAPPROPRIATE = "INAPPROPRIATE";
    }
    
    private static String RVSSdkSharedPreferencesName = "RVSSdkSharedPreferencesName";

    private static Context context;

    static private RVSSdk sharedRVSSdk = null;

    private RVSRequestManager requestManager;

    private RVSServiceManager serviceManager;

    private RVSSdk(Context context) {

//        System.out.println("[aaa111] " + "bbb");

        logger.info("************* RVS SDK started ****************");

        RVSSdk.context = context;

        requestManager = new RVSRequestManager();
        serviceManager = new RVSServiceManager(requestManager.getBaseUrlString());

    }

    static public RVSSdk sharedRVSSdk() {
        return sharedRVSSdk;
    }

    static public RVSSdk createRVSCore(Context context) {

        if (sharedRVSSdk != null) {
            return null;
        }

        sharedRVSSdk = new RVSSdk(context);

        return sharedRVSSdk;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        RVSSdk.context = context;
    }

    public RVSRequestManager requestManager() {
        return requestManager;
    }

    public RVSServiceManager serviceManager() {
        return serviceManager;
    }

    public void setBaseUrl(String url) {
    	requestManager.storeBaseUrl(url);
    }
    
    public void setNetworkConnectivityChangedListener(RVSServiceManager.NetworkConnectivityChanged listener) {
        this.serviceManager.setNetworkConnectivityChangedListener(listener);
    }

	
	public boolean isNewExternalUser()
	{
	    Utils.assertCalledNotFromMainThread();
	    
	    return this.serviceManager.isNewExternalUser();
	}
	
	public boolean isEmailEmpty()
	{
		Utils.assertCalledNotFromMainThread();
	    
	    return this.serviceManager.isEmailEmpty();
	}
	
	public boolean isHandleRequired()
	{
		Utils.assertCalledNotFromMainThread();
	    
	    return this.serviceManager.isHandleRequired();
	}
	
	public RVSPromise isHandleFree(String handle){
		
        String method = String.format("userHandle/%s", handle);
        return RVSSdk.sharedRVSSdk().requestManager().get(method, new HashMap<String, String>());
	}
	
	public void retrySignIn(){
		
	    this.serviceManager.retrySignIn();
	}
	
	public RVSUser getMyUser() {
		
		return this.serviceManager.getMyUser();
	}
	
	public String getMyEmail()
	{
		Utils.assertCalledNotFromMainThread();
	    
	    return this.serviceManager.getMyEmail();
	}

    public void signInWithUserName(String userName, String password) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userName);
        Utils.assertInEmptyString(password);

        serviceManager.signInWithUserName(userName, password);
    }

    public void signInWithFacebookToken(String token) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(token);

        serviceManager.signInWithFacebookToken(token);
    }

    public void signInWithTwitterToken(String token, String secret) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(token);
        Utils.assertInEmptyString(secret);

        serviceManager.signInWithTwitterToken(token, secret);
    }
    
    public RVSPromise registerWithEmail(String email, String password, String name, String handle) {
        
    	Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(email);
        Utils.assertInEmptyString(password);
        Utils.assertInEmptyString(name);
        Utils.assertInEmptyString(handle);
        
        return serviceManager.registerWithEmail(email, password, name, handle);
    }

    public static SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(RVSSdk.RVSSdkSharedPreferencesName, Context.MODE_PRIVATE);
    }

    public String getMyUserId() {

        return serviceManager.getMyUserId();
    }


    public boolean isSignedOut() {
        return serviceManager.isSignedOut();

    }

    public Boolean isPendingSignIn() {
    	
        Utils.assertCalledNotFromMainThread();
        return serviceManager.isPendingSignIn();
    }
    
    
    public RVSPromise resetPasswordWithUserEmail(String userEmail) {

    	Utils.assertInEmptyString(userEmail);
        
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userEmail", userEmail);
        
        return requestManager.post("userForgotPassword", params);
    }
    
    
    public void cancelPendingSignIn() {
    	
        Utils.assertCalledNotFromMainThread();
        serviceManager.cancelPendingSignIn();
    }
    
    public RVSPromise updateUserProfileWithName(String name, String handle, String email) {
    	
    	Utils.assertCalledNotFromMainThread();
        
        return serviceManager.updateUserProfileWithName(name, handle, email);
    }
    
    public void signOut() {
        Utils.assertCalledNotFromMainThread();

        serviceManager.signOut();
    }

    // [+] Channels
    
    /**
     * @param channelId - id of the channel
     *                  RVSAsyncList next method return a promise with DoneCallback<RVSMediaContext>
     */
    public RVSPromise getMediaContextForChannel(String channelId) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(channelId);

        return new RVSGetMediaContext(channelId, null).getMediaContext();
    } 
    
    
    /**
    * Create media context for live program
    * Should be used when composing posts only since it uses the DVR for the program
    * @param programId program ID to use for creating the media context
    * @param channelId channel ID to use for creating the media context
    * @return a promise RVSMediaContext object
    **/
    public RVSPromise getMediaContextForProgram(String programId, String channelId) {
    	
    	 Utils.assertCalledNotFromMainThread();
    	 Utils.assertInEmptyString(programId);
    	 
    	 //Add a comment to this line
    	  return new RVSGetMediaContext(channelId,programId).getMediaContext();
    }
    
    /**
     * @return promise with DoneCallback<RVSChannelSearchResult>
     */
    @SuppressWarnings("unchecked")
    public RVSPromise channelForCallSign(String callSign) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(callSign);

        RVSGetSearchChannel getSearchChannel = new RVSGetSearchChannel();
        getSearchChannel.initWithSearchCallSign(callSign);

        final RVSPromise resultPromise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);
        RVSPromise promise = getSearchChannel.next(1);
        promise.then(new DoneCallback<List<RVSChannelSearchResult>>() {
            @Override
            public void onDone(List<RVSChannelSearchResult> result) {
                if (result.size() > 0) {
                    resultPromise.resolve(result);
                } else {
                    resultPromise.reject(null);
                }
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                resultPromise.reject(error);
            }
        });

        return resultPromise;
    }

    /**
     *  Get current program for a channel
     *  @param channelId channel ID
     *  @return promise for RVSProgram object
     */
    public RVSPromise currentProgramForChannel(String channelId) {
    	
    	  Utils.assertCalledNotFromMainThread();
    	  
    	  String method = String.format("channels/%s/currentProgram", channelId);
          return requestManager.getJsonObject(method, null, RVSProgramImp.class);
    }
    
    /**
    *  Get transcript items list for a channel
    *
    *  @param channelId channel ID
    *  @param startTime Start Time
    *  @param endTime End Time
    *
    *  @return async list object that returns an array of transcriptItems
    */
    public RVSAsyncList getTranscriptItemsForChannel(String channelId, long startTime, long endTime) {

    	Utils.assertCalledNotFromMainThread();
    	String method = String.format("channels/%s/media/transcript", channelId);

    	// sec to msec
        HashMap<String,String> postInfo = new HashMap<String, String>();
        postInfo.put("startTime", String.valueOf(startTime));
        postInfo.put("endTime", String.valueOf(endTime));
        
        return new RVSGetTranscriptItemExtendedList(method, postInfo);
    }
    
    // [-] Channels
    
    // [+] Posts

    /**
     * Create post with text and a channel clip.
     *
     * @param text         post text
     * @param mediaContext media context used for composing the post
     * @param startOffset  post clip start offset in seconds relative ot the media context
     * @param duration     post clip duration in seconds
     * @param coverImageId the thumbnail ID to use as the post cover image@return post promise object allowing to submit the post and receive the submitted post or an error if failed
     */
    public RVSPromise createPostWithText(String text, RVSMediaContext mediaContext, long startOffset,
                                         long duration, String coverImageId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(text);
        Utils.assertInEmptyString(getMyUserId());
        if (mediaContext == null) {
            throw new AssertionError("Media context must be not null");
        }

        RVSCreatePost createPost = new RVSCreatePost(text, mediaContext,
                startOffset, duration, coverImageId);

        RVSPromise promise = createPost.createPost();
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {
                // todo - update user data
//                [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
//
//                    RVSUser *user = (RVSUser*)obj;
//                    user.postCount = [NSNumber numberWithUnsignedInteger:user.postCount.unsignedIntegerValue + 1];
//                }
            }
        });

        return promise;
    }
    
    /**
     * Report a post 
     * @param postId post to report
     * @param reason report reason
     * @return promise with some object if success, object should be ignored
     */
    public RVSPromise reportPost(final String postId, String postReportType) {
        
    	Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);
        
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("reason", postReportType);
        
        String method = String.format("posts/%s/report", postId);
//        final RVSPromise promise = requestManager.post(method, params);
        final RVSPromise promise = requestManager.put(method, params);
        promise.then(new AndroidDoneCallback<String>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(String result) {

                // TODO notify obj
                logger.info("reportPost done postId " + postId);
                
                
//                RVSPost *post = (RVSPost*)obj;
//                if (reason == RVSPostReportTypeSpoiler)
//                {
//                    post.isSpoiler = [NSNumber numberWithBool:YES];
//                }
//                else if (reason == RVSPostReportTypeInappropriate)
//                {
//                    post.isInappropriate = [NSNumber numberWithBool:YES];
//                }
                
//                [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                 RVSPost *post = (RVSPost*)obj;
//                 post.repostedByCaller = [NSNumber numberWithBool:YES];
//                 post.repostCount = [NSNumber numberWithUnsignedInteger:post.repostCount.unsignedIntegerValue + 1];
            }
        });

        return promise;
    }

    /**
     * Create comment with text and a channel clip.
     *
     * @param message comment text
     * @param postId  post id of the related post
     * @return comment promise object allowing to submit the comment and receive the submitted comment or an error if failed
     */
    public RVSPromise createCommentWithMessage(String message, String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(message);
        Utils.assertInEmptyString(postId);


        RVSCreateComment createComment = new RVSCreateComment(message, postId);
        RVSPromise promise = createComment.createComment();
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {
                // todo - update user data
//                RVSPost *post = (RVSPost*)obj;
//                post.commentCount = [NSNumber numberWithUnsignedInteger:post.commentCount.unsignedIntegerValue + 1];
//
//                if (post.latestComments) {
//                    [post.latestComments insertObject:result atIndex:0];
//                } else {
//                    post.latestComments = (NSMutableArray <RVSComment, Optional> *)[NSMutableArray arrayWithObject:result];
//                }
            }
        });

        return promise;

    }

    /**
     * Get post objet for post ID
     *
     * @param postId post to get post object for
     * @return promise for an RVSPost object
     */
    @SuppressWarnings("unchecked")
    public RVSPromise postForPostId(String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s", postId);
//        return requestManager.getJsonObject(method, null, RVSPostImp.class);
        

        final RVSPromise promise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);
        RVSPromise requestPromise = requestManager().getJsonObject(method, null, RVSPostImp.class);
        requestPromise.then(new AndroidDoneCallback<RVSPost>() {
	           @Override
	           public void onDone(RVSPost post) {
	        	   if (post.isInappropriate())
	               {
	                   promise.reject(new RVSError("isInappropriate"));
	               }
	               else
	               {
	            	   promise.resolve(post);
	               }
	           };
	
	           @Override
	           public AndroidExecutionScope getExecutionScope() {
	               return AndroidExecutionScope.BACKGROUND;
	           }

       }).fail(new AndroidFailCallback<Throwable>() {

           @Override
           public AndroidExecutionScope getExecutionScope() {
               return AndroidExecutionScope.BACKGROUND;
           }

           @Override
           public void onFail(Throwable error) {

               logger.error("promise reject with error");
               promise.reject(error);
           }
       });
       
       return promise;

    }


    /**
     * Get trending posts
     *
     * @return promise with DoneCallback<List<RVSPost>>
     */
    public RVSAsyncList trendingPosts() {

        return new RVSGetPostList("posts/trending", true);
    }

    /**
     * @return promise with DoneCallback<List<RVSPost>>
     */
    public RVSAsyncList recentPosts() {

        return new RVSGetPostList("posts/recent", true);
    }

    /**
     * Get sharing URL for a post
     *
     * @param postId ID of post to share
     * @return promise a promise with DoneCallback<RVSSharingUrl>
     */
    public RVSPromise sharingUrlsForPost(String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s/sharingUrls", postId);
        return RVSSdk.sharedRVSSdk().requestManager().getJsonObject(method, null, RVSSharingUrls.class);
    }

    /**
     * Get comments list for post ID
     *
     * @param postId postId post to get comments for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSComment>>
     */
    public RVSAsyncList commentsForPost(String postId) {

        String method = String.format("posts/%s/comments", postId);
        return new RVSGetCommentList(method);
    }

    /**
     * Get users that liked a post
     *
     * @param postId id of post to get favoriting users for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSUser>>
     */
    public RVSAsyncList likingUsersForPost(String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s/likeUsers", postId);
        return new RVSGetUserList(method);
    }

    /**
     * Get users that reposted a post
     *
     * @param postId id of post to get reposting users for
     * @return async list with referring user objects
     */
    public RVSAsyncList repostingUsersForPost(String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s/repostUsers", postId);
        return new RVSGetUserList(method);
    }

    /**
     * Like a post
     *
     * @param postId post to like
     * @return promise with some object if success, object should be ignored
     */
    public RVSPromise createLikeForPost(final String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);
        Utils.assertInEmptyString(getMyUserId());

        String method = String.format("posts/%s/likeUsers/%s", postId, getMyUserId());
        RVSPromise promise = requestManager.put(method, null);
        promise.then(new AndroidDoneCallback<String>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(String result) {

                logger.info("createLikeForPost done, postId: " + postId);
                // todo
//                [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                    RVSPost *post = (RVSPost*)obj;
//                    post.likedByCaller = [NSNumber numberWithBool:YES];
//                    post.likeCount = [NSNumber numberWithUnsignedInteger:post.likeCount.unsignedIntegerValue + 1];
//                }];
            }
        });

        return promise;
    }

    /**
     * Unlike a post
     *
     * @param postId post to unlike
     * @return promise with some object if success, object should be ignored
     */
    public RVSPromise deleteLikeForPost(final String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);
        Utils.assertInEmptyString(getMyUserId());

        String method = String.format("posts/%s/likeUsers/%s", postId, getMyUserId());
        RVSPromise promise = requestManager.delete(method, null);
        promise.then(new AndroidDoneCallback<String>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(String result) {
                // todo
                logger.info("deleteLikeForPost done postId " + postId);
//                [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                    RVSPost *post = (RVSPost*)obj;
//                    post.likedByCaller = [NSNumber numberWithBool:NO];
//                    post.likeCount = [NSNumber numberWithUnsignedInteger:post.likeCount.unsignedIntegerValue - 1];
//                }];
            }
        });

        return promise;
    }

    /**
     * Repost a post
     *
     * @param postId post to repost
     * @return promise with some object if success, object should be ignored
     */
    public RVSPromise createRepostForPost(final String postId) {

        String myUserId = getMyUserId();

        Utils.assertCalledNotFromMainThread();
        ;
        Utils.assertInEmptyString(postId);
        Utils.assertInEmptyString(myUserId);

        String method = String.format("posts/%s/repostUsers/%s", postId, myUserId);
        final RVSPromise promise = requestManager.put(method, null);
        promise.then(new AndroidDoneCallback<String>() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(String result) {

                // TODO notify obj
                logger.info("createRepostForPost done postId " + postId);
//                [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                 RVSPost *post = (RVSPost*)obj;
//                 post.repostedByCaller = [NSNumber numberWithBool:YES];
//                 post.repostCount = [NSNumber numberWithUnsignedInteger:post.repostCount.unsignedIntegerValue + 1];
            }
        });

        return promise;
    }

    /**
     * Delete a repost to a post
     *
     * @param postId post to unrepost
     * @return promise with some object if success, object should be ignored
     */
    public RVSPromise deleteRepostForPost(final String postId) {

        String myUserId = getMyUserId();

        Utils.assertCalledNotFromMainThread();
        ;
        Utils.assertInEmptyString(postId);
        Utils.assertInEmptyString(myUserId);

        String method = String.format("posts/%s/repostUsers/%s", postId, myUserId);
        final RVSPromise promise = requestManager.delete(method, null);
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {

                // TODO notify obj
                logger.info("deleteRepostForPost done postId: " + postId);
//                 [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                     RVSPost *post = (RVSPost*)obj;
//                     post.repostedByCaller = [NSNumber numberWithBool:NO];
//                     post.repostCount = [NSNumber numberWithUnsignedInteger:post.repostCount.unsignedIntegerValue - 1];
//                 }];
            }
        });

        return promise;
    }

    /**
     * Delete a post
     *
     * @param postId post to delete
     * @return promise object allowing to delete the post or an error if failed
     */
    public RVSPromise deletePost(final String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s", postId);
        final RVSPromise promise = requestManager.delete(method, null);
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {

                // TODO notify obj
                logger.info("delete post done postId: " + postId);
//                [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
//                 RVSUser *user = (RVSUser*)obj;
//                 user.postCount = [NSNumber numberWithUnsignedInteger:user.postCount.unsignedIntegerValue - 1];
//
            }
        });

        return promise;
    }


    /**
     * Delete a comment
     *
     * @param commentId comment to delete
     * @param postId    parent post of comment
     * @return promise object with success callback or an error if failed
     */
    @SuppressWarnings("unchecked")
    public RVSPromise deleteComment(final String commentId, final String postId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(commentId);
        Utils.assertInEmptyString(postId);

        String method = String.format("posts/%s/comments/%s", postId, commentId);

        RVSPromise promise = requestManager.delete(method, null);
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {

                // TODO notify obj
                logger.info("delete post done postId: " + postId + ", commentId: " + commentId);
//                resultPromise.resolve(result);
//                [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                    [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
//                        RVSPost *post = (RVSPost*)obj;
//                        post.commentCount = [NSNumber numberWithUnsignedInteger:post.commentCount.unsignedIntegerValue - 1];
//                        [post.latestComments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                            if ([commentId isEqualToString:((RVSComment*)obj).commentId])
//                            {
//                                [post.latestComments removeObjectAtIndex:idx];
//                                *stop = YES;
//                            }
//                        }];
//                    }];
//                }];
            }
        });
//                .fail(new FailCallback<Throwable>() {
//            @Override
//            public void onFail(Throwable error) {
//                resultPromise.reject(error);
//            }
//        });

        return promise;
    }


    // [-] Posts

    /**
     * Sending RVSSdkClipPlaybackStartingNotification or RVSSdkClipPlaybackEndedNotification notification
     * via LocalBroadcastManager
     *
     * @param playing
     */
    public void informClipPlayback(boolean playing) {

        if (playing) {
            playingClipCount++;
        } else {
            playingClipCount--;
        }

        if (playing && playingClipCount == 1) {

            Intent intent = new Intent(RVSSdkClipPlaybackStartingNotification);
            LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);

        } else if (!playing && playingClipCount == 0) {
            Intent intent = new Intent(RVSSdkClipPlaybackEndedNotification);
            LocalBroadcastManager.getInstance(RVSSdk.getContext()).sendBroadcast(intent);
        }
    }


    // [+] Users

    /**
     * Get user object for user ID
     *
     * @param userId user id to get user object for
     * @return promise with RVSUser object
     */
    public RVSPromise userForUserId(String userId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s", userId);
        return requestManager.getJsonObject(method, null, RVSUserImp.class);
    }


    /**
     * Get user posts
     *
     * @param userId id of user to get posts for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSPost>>
     */
    public RVSAsyncList userFeedForUser(String userId) {

        Utils.assertCalledNotFromMainThread();
        ;
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/userFeed", userId);
        return new RVSGetPostList(method, true);
    }

    /**
     * Get user news feed
     *
     * @param userId id of user to get news feed for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSPost>>
     */
    public RVSAsyncList newsFeedForUser(String userId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/newsFeed", userId);
        return new RVSGetPostList(method, true);
    }


    /**
     * Get user followers
     *
     * @param userId id of user to get followers for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSPost>>
     */
    public RVSAsyncList followersOfUser(String userId) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/followers", userId);
        return new RVSGetUserList(method);
    }


    /**
     * Get users following a user
     *
     * @param userId id of user to get followed users for
     * @return async post list object
     */
    public RVSAsyncList followedByUser(String userId) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/followings", userId);
        return new RVSGetUserList(method);
    }


    /**
     * Get user liked posts
     *
     * @param userId id of user to get liked posts for
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSPost>>
     */

    public RVSAsyncList likedPostsByUser(String userId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/liked", userId);
        return new RVSGetPostList(method, true);
    }

    /**
     * Follow user
     *
     * @param userId user to follow
     * @return promise for userId
     */
    public RVSPromise followUser(String userId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);


        String method = String.format("users/%s/followings/%s", getMyUserId(), userId);
        final RVSPromise promise = requestManager.put(method, null);
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {

                // TODO notify user by class + id
                logger.info("followUser done");
//                [RVSUser notifyChangedDataObjectWithId:userId syncBlock:^(RVSSyncedDataObject *obj) {
//                RVSUser *user = (RVSUser*)obj;
//                user.isFollowedByMe = [NSNumber numberWithBool:YES];
//                user.followerCount = [NSNumber numberWithUnsignedInteger:user.followerCount.unsignedIntegerValue + 1];
//            }];
//            [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
//                RVSUser *user = (RVSUser*)obj;
//                user.followingCount = [NSNumber numberWithUnsignedInteger:user.followingCount.unsignedIntegerValue + 1];
//            }];

//                BeanDescription.
//                BasicClassIntros
            }
        });

        return promise;
    }

    /**
     * Unfollow user
     *
     * @param userId user to unfollow
     * @return promise for userId
     */
    public RVSPromise unfollowUser(String userId) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/followings/%s", getMyUserId(), userId);
        final RVSPromise promise = requestManager.delete(method, null);
        promise.then(new AndroidDoneCallback() {
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.UI;
            }

            @Override
            public void onDone(Object result) {

                // TODO notify user by class + id
                logger.info("followUser done");
//                [RVSUser notifyChangedDataObjectWithId:userId syncBlock:^(RVSSyncedDataObject *obj) {
//                RVSUser *user = (RVSUser*)obj;
//                user.isFollowedByMe = [NSNumber numberWithBool:NO];
//                user.followerCount = [NSNumber numberWithUnsignedInteger:user.followerCount.unsignedIntegerValue - 1];
//            }];
//            [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
//                RVSUser *user = (RVSUser*)obj;
//                user.followingCount = [NSNumber numberWithUnsignedInteger:user.followingCount.unsignedIntegerValue - 1];
//            }];

//                BeanDescription.
//                BasicClassIntros
            }
        });

        return promise;
    }

    /**
     * @return RVSAsyncList next method return a promise with DoneCallback<List<RVSNotification>>
     */
    public RVSAsyncList notificationsForUser(String userId) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        String method = String.format("users/%s/notifications", userId);
        return new RVSGetNotificationList(method);
    }


    /**
     * @return promise with DoneCallback<RVSFeedCountDetails>
     */
    public RVSPromise notificationsCountForUser(String userId, long startTime) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("startTime", String.valueOf(startTime));
        String method = String.format("users/%s/notifications/count", userId);

        return requestManager.getJsonObject(method, params, RVSFeedCountDetails.class);
//       return [self.requestManager getJsonObjectProperty:method parameters:parameters objectClass:[RVSFeedCountDetails class] propertyName:@"count"];

    }

    /**
     * @return promise with DoneCallback<RVSFeedCountDetails>
     */
    public RVSPromise newsFeedCountForUser(String userId, long startTime) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(userId);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("startTime", String.valueOf(startTime * 1000));
        String method = String.format("users/%s/newsFeed/count", userId);

        return requestManager.getJsonObject(method, params, RVSFeedCountDetails.class);
//        return [self.requestManager getJsonObjectProperty:method parameters:parameters objectClass:[RVSFeedCountDetails class] propertyName:@"count"];
    }

    // [-] Users end

    // [+] Search

    /**
     *  Perform a content search within a show
     *
     *  @param searchString text to search
     *  @param showId       show id
     *  @param channelId    channel id
     *
     *  @return async list with search content result NSArray with RVSContentSearchResult
     */
    public RVSAsyncList searchContent(String searchString, String showId,  String channelId) {
    	 Utils.assertCalledNotFromMainThread();
         Utils.assertInEmptyString(showId);
         Utils.assertInEmptyString(channelId);
         
	    return new RVSGetSearchContent (searchString, showId, channelId);
	}


    /**
     * @param type         - uses static variables declared in RVSSdk.ContentSearchType class
     * @param minStartTime - pass -1 if not defined
     * RVSAsyncList next method return a promise with DoneCallback<List<RVSContentSearchResult>>
     */
    public RVSAsyncList searchContent(String searchString, String type, long minStartTime) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(searchString);

        return new RVSGetSearchContent(searchString, type, minStartTime);
    }

    /**
     * RVSAsyncList next method return a promise with DoneCallback<List<RVSChannelSearchResult>>
     */
    public RVSAsyncList searchChannel(String searchString) {
        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(searchString);

        RVSGetSearchChannel searchChannel = new RVSGetSearchChannel();
        searchChannel.initWithSearchString(searchString);
        return searchChannel;
    }

    /**
     * Search user by string
     * RVSAsyncList next method return a promise with DoneCallback<List<RVSUserSearchResult>>
     */
    public RVSAsyncList searchUser(String searchString) {

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(searchString);

        return new RVSGetSearchUser(searchString);
    }


    /**
     * @return promise with DoneCallback<RVSSearchCompositeResultList>
     */
    public RVSPromise searchSuggestions(String freeText){

        Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(freeText);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("freeText", freeText);

        return requestManager.getJsonObject("search/suggestions", params, RVSSearchCompositeResultListImp.class);
    }
    // [-] Search

    @SuppressWarnings("unchecked")
	public RVSPromise reportViewWithStartOffset(final String mediaContext, long startOffset, long duration) {
		
    	Utils.assertCalledNotFromMainThread();
    	 
		long startOffsetNew = startOffset * 1000;
		long endOffset = (startOffset + duration) * 1000;
		String deviceId = serviceManager.getUniqueDeviceIdentifier();
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("mediaContext", mediaContext);
		params.put("startOffset",String.valueOf(startOffsetNew));
		params.put("endOffset", String.valueOf(endOffset));
		params.put("deviceId", deviceId);

  		//****************
  		// Do not send userId until BE will add support for this parameter!
  		//****************
  		//
//		if ([[RVSSdk sharedSdk].myUserId length] > 0)
//		{
//			[parameters setObject:[RVSSdk sharedSdk].myUserId forKey:@"userId"];
//		}
	    
	     RVSPromise promise = requestManager.putJsonObject("media/contexts/viewed", params,  MediaViewedResult.class);
	     promise.then(new DoneCallback<MediaViewedResult>() {
	
	         @Override
	         public void onDone(MediaViewedResult result) {
	
	        	logger.info("media context view allowed");
//	 	        if (result.equals("ALLOW"))
//	 	        {
//	 	            //cool
//	 	            logger.info("media context view allowed: " +  mediaContext);
//	 	        }
//	 	        else
//	 	        {
//	 	        	logger.info("media context view restricted! " + mediaContext);
//	 	            //TODO: kill player
//	 	        }
	 	        
	             // TODO notify obj
	         }
	     }).fail( new FailCallback<Throwable>() {

			@Override
			public void onFail(Throwable result) {
				logger.info("media context error " + result.getMessage());				
			}
	     });

	     return promise;		
	}
    
    
    // [+] Shows

    public RVSAsyncList trendingShows() {
        return new RVSGetShowList("shows/trending",true);
    }

    public RVSAsyncList getShows() {
        return new RVSGetShowList("shows", true);
    }

    public RVSPromise getShowForShowId(String showId, String channelId) {
        
    	Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(showId);
        Utils.assertInEmptyString(channelId);
        
        String method = String.format("channels/%s/shows/%s", channelId, showId);
        return requestManager.getJsonObject(method, null, RVSShowImp.class);
    }

    public RVSAsyncList getTrendingPostsForShow(String showId, String channelId) {
        
    	Utils.assertCalledNotFromMainThread();
        Utils.assertInEmptyString(showId);
        Utils.assertInEmptyString(channelId);
        
        String method = String.format("channels/%s/shows/%s/trending", channelId, showId);
        return new RVSGetPostList(method, true);
    }
    
    // [-] Shows

    

    // [+] Programs

    public RVSAsyncList getEpgWithStartTime(long startTimeMillis, long endTimeMillis) {
    	
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("startTime",String.valueOf(startTimeMillis));
		params.put("endTime", String.valueOf(endTimeMillis));
        
        return new RVSGetEpgSlotList("epg",  params, true);
    }
    

    /**
     *  Get current programs. no paging
     *
     *  @return async list with RVSEpgSlot objects
     */
	public RVSAsyncList getCurrentPrograms() {
	    return new RVSGetEpgSlotList("currentPrograms", null, true);
	}
    
    // [-] Programs

}
