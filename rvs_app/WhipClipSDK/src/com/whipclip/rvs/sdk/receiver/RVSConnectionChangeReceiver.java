package com.whipclip.rvs.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.whipclip.rvs.sdk.RVSSdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by iliya on 6/22/14.
 */
public class RVSConnectionChangeReceiver extends BroadcastReceiver {

    public interface NetworkChangeListener
    {
        void onNetworkConnectivityChanged();
    }

    Logger logger = LoggerFactory.getLogger(RVSConnectionChangeReceiver.class);
    private static NetworkChangeListener netListener;


    @Override
    public void onReceive(Context context, Intent intent) {

        Context sdkContext = RVSSdk.getContext();
        if(sdkContext == null)
        {
            RVSSdk.setContext(context);
        }

        if (intent == null)
        {
            return;
        }

        String action = intent.getAction();

        logger.info("onReceive: action " + action);


        if(action.equals("android.net.conn.CONNECTIVITY_CHANGE"))
        {
            logger.info(" connectivity change");

            if(netListener != null)
            {
                //Telephony to = FactoryTelephony.getTelephonyObserver();
                netListener.onNetworkConnectivityChanged();
                logger.info("network changed listener called");
            }
            else
            {
                logger.info("network changed no listener");
            }
        }
    }


    public static void setNetworkListener(NetworkChangeListener osNetworkObserver)
    {
        netListener = osNetworkObserver;
    }
}
