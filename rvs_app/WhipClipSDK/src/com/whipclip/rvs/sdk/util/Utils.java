package com.whipclip.rvs.sdk.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;

import org.slf4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by iliya on 6/11/14.
 */
public class Utils {
    public final static String TAG = "Utils";

    private static final String CONFIG_FOLDER_NAME = "whipclip";
    private static final String CONFIG_FILE_NAME = "config.properties";

    private static Properties m_applicationProperties;
    private static Properties m_externalProperties;


    /**
     * Return config property by key stored  project resource file CONFIG_FILE_NAME.
     * The properties may be overridden from file CONFIG_FILE_NAME which stored in sdcard folder CONFIG_FOLDER_NAME
     **/
    public static String getConfigParameter(String key)
    {
        String value = null;

        if (m_applicationProperties == null)
        {
            m_applicationProperties = new Properties();

            try
            {
                InputStream is = FileManager.getResourceFileStream(CONFIG_FILE_NAME);
                if (is != null)
                {
                    m_applicationProperties.load(is);
                }

                // load override properties
                String storage_state = Environment.getExternalStorageState();
                if (storage_state.contains(Environment.MEDIA_MOUNTED))
                {

                    File sdcard = Environment.getExternalStorageDirectory();
                    File propFolder = new File(sdcard, CONFIG_FOLDER_NAME);
                    if (propFolder.exists())
                    {
                        File propertiesFile = new File(propFolder, CONFIG_FILE_NAME);
                        if (propertiesFile.exists())
                        {
                            is = new FileInputStream(propertiesFile);
                            if (is != null)
                            {
                                m_externalProperties = new Properties();
                                m_externalProperties.load(is);
                            }
                        }
                    }
                }


            }
            catch (FileNotFoundException e)
            {
            }
            catch (IOException e)
            {
                Log.e(TAG, e.getMessage());
            }

        }

        if (m_externalProperties != null)
        {
            value = m_externalProperties.getProperty(key);
        }
        if (value == null)
        {
            value = m_applicationProperties.getProperty(key);
        }

        return value;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDipToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDip(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * Verify called from main thread
     * Throws Assertion Error in case it does not
     */
    public static void assertCalledNotFromMainThread() {
        // verify called from main thread
        if (Looper.getMainLooper() !=  Looper.myLooper()) {
            throw new AssertionError("Must be called from main thread");
        }

    }

    public static void assertInEmptyString(String string) {
        if (string == null || string.isEmpty()) {
            throw new AssertionError("String is empty or null");
        }

    }

    public static void assertObjectNotValid(Object object) {
        if (object == null) {
            throw new AssertionError("Object is null");
        }

    }

    public static void printHashMap(Logger logger, String title, HashMap<String, String> parameters) {
        
        StringBuffer result = new StringBuffer();
        result.append(title + ": ");
        for (String key: parameters.keySet()) {

            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(key + "='" + parameters.get(key) + "'");
        }

        logger.info(result.toString());
    }
}

