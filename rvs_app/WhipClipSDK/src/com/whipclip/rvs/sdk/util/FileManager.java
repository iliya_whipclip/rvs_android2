package com.whipclip.rvs.sdk.util;

import com.whipclip.rvs.sdk.RVSSdk;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by iliya on 6/11/14.
 */
public class FileManager {

    private static final int BUFFER = 2048;

    public static boolean fileExists(String file)
    {
        File f = new File(file);
        return f.exists();
    }

    public static InputStream openForReading(String path) throws IOException
    {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);

        return fis;
    }

    public static OutputStream openForWriting(String path, boolean append) throws IOException
    {
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file, append);

        return fos;
    }

    public static void deleteFile(String path)
    {
        File f = new File(path);
        f.delete();
    }

    public interface DownloadFileProgress
    {

        void onDownloadFileProgress(int percent);

    }

    public static void downloadFile(String sUrl, String destFilePath, DownloadFileProgress downloadProgress)
    {
        try
        {

            URL url = new URL(sUrl);

            // this will be useful so that you can show a typical 0-100%
            // progress bar
            URLConnection connection = url.openConnection();
            connection.connect();
            int fileLength = connection.getContentLength();
            long total = 0;

            FileManager.deleteFile(destFilePath);

            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(destFilePath);

            byte data[] = new byte[1024];
            int count;
            while ((count = input.read(data)) != -1)
            {
                total += count;
                // publishing the progress....
                if (downloadProgress != null)
                {
                    if (fileLength > 0)
                    {
                        downloadProgress.onDownloadFileProgress((int)(total * 100 / fileLength));
                    } else
                    {
                        downloadProgress.onDownloadFileProgress(-1);
                    }
                }
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e)
        {
        }
    }

    public static String getMD5Checksum(String path)
    {
        String md5 = "";
        try
        {
            File file = new File(path);
            FileInputStream fis = new FileInputStream(file);

            MessageDigest md = MessageDigest.getInstance("MD5");
            InputStream inputStream = new DigestInputStream(fis, md);
            // read stream to EOF as normal...
            byte[] buffer = new byte[1024];
            while (inputStream.read(buffer) > 0)
            {
            }
            md5 = new BigInteger(1, md.digest()).toString(16);
            fis.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return md5;
    }

    public static String getExternalPathForFileName(String fileName)
    {
        return RVSSdk.getContext().getExternalFilesDir(null).getAbsolutePath() + fileName;
    }

    public static boolean makeZipFileFromStrings(String path, String zipFileName, List<String> zipEntryNames, List<String> data)
    {
        String fullPath = path + zipFileName;
//        LoggerApp.e(TAG, "makeZipFileFromStrings " + fullPath);

        OutputStream dest = null;
        try
        {
            FileManager.createDirIfNotExist(path);
            dest = FileManager.openForWriting(fullPath, false);
            // dest = new FileOutputStream(zipFileName);
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
        for (int i = 0; i < zipEntryNames.size(); i++)
        {
            InputStream is = new ByteArrayInputStream(data.get(i).getBytes());
            if (addZipFile(out, zipEntryNames.get(i), is) == false)
            {
//                LoggerApp.e(TAG, "Failed to add " + zipEntryNames.get(i) + " file to zip file <" + zipFileName + ">");
                return false;
            }
        }

        try
        {
            out.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return true;
    }

    /*
     * Add input stream date into zip as zipEntryName file name
     */
    private static boolean addZipFile(ZipOutputStream out, String zipEntryName, InputStream in)
    {
        BufferedInputStream origin = new BufferedInputStream(in, BUFFER);
        ZipEntry entry = new ZipEntry(zipEntryName);
        try
        {
            out.putNextEntry(entry);
        } catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        int count;
        try
        {
            byte data[] = new byte[BUFFER];
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
                // Log.v("Write", "Adding: "+origin.read(data, 0, BUFFER));
            }
        } catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        try
        {
            origin.close();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return true;
    }

    public static void createDirIfNotExist(String path)
    {
        final File dir = new File(path);
        if (dir.exists() == false)
        {
            dir.mkdirs(); // create folders where write files
        }
    }

    public static InputStream getResourceFileStream(String fileName)
    {
        InputStream stream = null;
        if (fileName.startsWith("/"))
        {
            fileName = fileName.substring(1);
        }

        try
        {
            stream = RVSSdk.getContext().getAssets().open(fileName);
        }
        catch (Exception e)
        {

        }
        return stream;
    }

    public static boolean rename(String oldPath, String newPath, boolean overrideNewPath)
    {
        File oldFile = new File(oldPath);

        // File (or directory) with new name
        File newFile = new File(newPath);
        if(newFile.exists())
        {
            if (overrideNewPath)
            {
                deleteFile(newPath);
            }
            else
            {
//                LoggerApp.d(TAG, "rename file, old file already exist");
                return false;
            }
        }

        return oldFile.renameTo(newFile);
    }

}

