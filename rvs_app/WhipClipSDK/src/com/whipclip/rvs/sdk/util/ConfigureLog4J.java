package com.whipclip.rvs.sdk.util;

import java.io.File;

import org.apache.log4j.Level;

import android.os.Environment;
import android.util.Log;
import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Copied from https://code.google.com/p/android-logging-log4j/
 * Created by iliya on 7/2/14.
 */
public class ConfigureLog4J {
    public static void configure() {
        final LogConfigurator logConfigurator = new LogConfigurator();

//        String log4jConfPath = "/whipclip/WhipClipSDK/log4j.properties";
//        PropertyConfigurator.configure(log4jConfPath);
        
        try {
            // In order to log to a file on the external storage, the following permission needs to be placed in AndroidManifest.xml.
            // <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                logConfigurator.setFileName(Environment.getExternalStorageDirectory() + File.separator + "rvs.log");
            } else {
                logConfigurator.setFileName(FileManager.getExternalPathForFileName("rvs.log"));
            }

            logConfigurator.setRootLevel(Level.ALL);
        // Set log level of a specific logger
//        logConfigurator.setLevel("org.apache", Level.ERROR);
            logConfigurator.configure();

        } catch (Exception e) {
            Log.e("ConfigureLog4j", "failed to configure log " + e.getMessage());
        }

    }
}
