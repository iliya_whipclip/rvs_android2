package com.whipclip.rvs.sdk.util;

public class Logger
{
	static final private String TAG = "WhipClipSDK";
	
    public static void log(int level, String msg)
    {
    	if (android.util.Log.isLoggable(TAG, level))
    		android.util.Log.println(level, TAG, msg);  
    }
	
	public static void LOG_FATAL(String msg)
	{
        log(android.util.Log.ASSERT, msg);
	}
	
	public static void LOG_ERROR(String msg)
	{
		log(android.util.Log.ERROR, msg);
	}
	
	public static void LOG_WARN(String msg)
	{
		log(android.util.Log.WARN, msg);
	}
	
	public static void LOG_INFO(String msg)
	{
		log(android.util.Log.INFO, msg);
	}
	
	public static void LOG_DEBUG(String msg)
	{
		log(android.util.Log.DEBUG, msg);
	}
	
	public static void logMemory() {
		
		final int heapKb = (int) (android.os.Debug.getNativeHeapSize() / 1024);
		final int maxMemoryKb = (int) (Runtime.getRuntime().maxMemory() / 1024);
//		final int heapKb = (int) (android.os.Debug.getn() / 1024);
		
		LOG_INFO("memory: " + maxMemoryKb);
		LOG_INFO("heap  : " + heapKb);
		LOG_INFO("*****");
		
	}
	
}
