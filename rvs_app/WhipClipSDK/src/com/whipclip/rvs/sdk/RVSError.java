package com.whipclip.rvs.sdk;

/**
 * Created by iliya on 6/26/14.
 */
public class RVSError extends Error{

    private String errorCode;
    private String httpCode;
    private RVSErrorCode rvsErrorCode;

    public RVSError() {}
    public RVSError(String detailMessage) { super(detailMessage); }
    public RVSError(RVSErrorCode code, String detailMessage) { super(detailMessage); this.rvsErrorCode = code; }

    public RVSErrorCode getRVSErrorCode() { return rvsErrorCode;}

    public void setErrorCode(String errorCode) { this.errorCode = errorCode; }
    public String getErrorCode() { return errorCode; }

    public String getHttpCode() { return httpCode; }
    public void setHttpCode(String httpCode) { this.httpCode = httpCode; }

}
