package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSReferringUserList;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSReferringUserImp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iliya on 8/4/14.
 */
public class RVSGetUserList  extends RVSGetPagingListBase{

    private Logger logger = LoggerFactory.getLogger(RVSReferringUserList.class);

    private List<RVSUser> responseBuffer;

    public RVSGetUserList(String method) {
        super(method, null, RVSReferringUserList.class, "referringUsers");
    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSUser>();

        // if no posts than nothing to filter
        if (responseArray.size() > 0)
        {
            // filter out all posts with post Id that we already got beofre
            for (Object obj : responseArray)
            {
                RVSReferringUserImp referringUserImp =  (RVSReferringUserImp)obj;
                responseBuffer.add(referringUserImp.getUser());
            }
        }


        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
