package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResults;
import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResults;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSChannelSearchResultImp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by iliya on 7/26/14.
 */
public class RVSGetSearchChannel extends RVSGetPagingListBase {

    private List<RVSChannelSearchResultImp> responseBuffer;
    private final HashSet<String> channelIds;

    public RVSGetSearchChannel() {

        channelIds = new HashSet<String>();
    }

    public void initWithSearchString(String searchString) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("freeText", searchString);
        init("search/channels", params, RVSChannelSearchResults.class, "results");
    }

    public void initWithSearchCallSign(String callsign) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("callsign", callsign);
        init("search/channels", params, RVSChannelSearchResults.class, "results");
    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSChannelSearchResultImp>();

        // if no posts than nothing to filter
        if (responseArray.size() > 0)
        {
            for (Object obj : responseArray)
            {
                RVSChannelSearchResultImp result =  (RVSChannelSearchResultImp)obj;

                if (channelIds.contains(result.getChannel().getChannelId()))
                {
                    logger.info("ignoring redundant channel " + result.getChannel().getChannelId());
                    continue;
                }

                channelIds.add(result.getChannel().getChannelId());

                //add result
                responseBuffer.add(result);
            }
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;
    }

}
