package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSUserSearchResults;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserSearchResultImp;

import java.io.UnsupportedEncodingException;
import java.lang.Character.UnicodeBlock;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by iliya on 7/26/14.
 */
public class RVSGetSearchUser extends RVSGetPagingListBase {

    private List<RVSUserSearchResultImp> responseBuffer;
    private Set<String> userIds;

    public RVSGetSearchUser(String searchString) {

        userIds = new HashSet<String>();

        HashMap<String, String> params = new HashMap<String, String>();
        try {
			params.put("username",  URLEncoder.encode(searchString, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        init("search/users", params, RVSUserSearchResults.class, "results");

    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSUserSearchResultImp>();

        // if no posts than nothing to filter
        if (responseArray.size() > 0)
        {
            for (Object obj : responseArray)
            {
                RVSUserSearchResultImp result =  (RVSUserSearchResultImp)obj;

                if (userIds.contains(result.getUser().getUserId()))
                {
                    logger.info("ignoring redundant user " + result.getUser().getUserId());
                    continue;
                }

                userIds.add(result.getUser().getUserId());

                //add result
                responseBuffer.add(result);
            }
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
