package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;
import com.whipclip.rvs.sdk.dataObjects.RVSPostList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSGetPostList extends RVSGetPagingListBase{

    private Logger logger = LoggerFactory.getLogger(RVSGetPostList.class);

    private boolean shouldFilterResponse = false;
    private Set<String> postIds;
    private List<RVSPostImp> responseBuffer;

    public RVSGetPostList(String method, boolean filterRespone) {
        super(method, null, RVSPostList.class, "posts");

        if (filterRespone)
        {
            shouldFilterResponse = true;
            postIds = new HashSet<String>();

        }
    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSPostImp>();

        if (shouldFilterResponse)
        {
            // if no posts than nothing to filter
            if (responseArray.size() > 0)
            {
                // filter out all posts with post Id that we already got beofre
                for (Object obj : responseArray)
                {
                    RVSPostImp post =  (RVSPostImp)obj;

                    if (post.isInappropriate()){
                    	logger.info("ignoring inappropriate post " + post.getPostId());
                    	continue;
                    }
                    
                    if (postIds.contains(post.getPostId()))
                    {
                        logger.info("ignoring redundent post %@", post);
                        continue;
                    }

                    responseBuffer.add(post);
                    postIds.add(post.getPostId());
                }
            }
        }
        else
        {
            responseBuffer.addAll(responseArray);
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
