package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.RVSPromise.CancelCallback;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSMediaContextImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserImp;
import com.whipclip.rvs.sdk.util.Utils;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;

/**
 * Created by iliya on 7/29/14.
 */
public class RVSGetMediaContext {

    Logger logger = LoggerFactory.getLogger(RVSGetMediaContext.class);

    private final String channelId;
    private final String programId;

    RVSPromise requestPromise;

    public RVSGetMediaContext(String channelId, String programId) {
        this.channelId = channelId;
        this.programId = programId;
    }

    @SuppressWarnings("unchecked")
    public RVSPromise getMediaContext() {
        Utils.assertCalledNotFromMainThread();

        final RVSPromise promise = RVSPromise.createPromise();

        String method;
        
        if (programId != null && !programId.isEmpty()) { 
        	method = String.format("media/contexts/programs/%s?channelId=%s", this.programId, this.channelId);
        } else {
        	method = String.format("media/contexts?channelId=%s", this.channelId);
        }
        requestPromise = RVSSdk.sharedRVSSdk().requestManager().postJsonObject(method, null, RVSMediaContextImp.class);
        requestPromise.done(new DoneCallback<RVSMediaContext>() {
            @Override
            public void onDone(RVSMediaContext result) {
                promise.resolve(result);
            }
        }).fail( new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                logger.info("failed to create media context with error " + error.getMessage());
                promise.reject(error);
            }
        });

        // keep a reference to self so it won't be destyoed before the promise
//        final WeakReference<RVSPromise> weakPromise = new WeakReference<RVSPromise>(promise); ;
//        promise.fail(new FailCallback() {
//            @Override
//            public void onFail(Object result) {
//                if (weakPromise.get() != null && weakPromise.get().isCancelled())
//                {
//                    requestPromise.cancel();
//                }
//            }
//        });
        
        promise.setCancelListener(new CancelCallback() {

			@Override
			public void onCancel() {
				 requestPromise.cancel();
				 // todo fix cancel functionality of promise
				 requestPromise = null;
			}
        	
		});

        return promise;

    }
}
