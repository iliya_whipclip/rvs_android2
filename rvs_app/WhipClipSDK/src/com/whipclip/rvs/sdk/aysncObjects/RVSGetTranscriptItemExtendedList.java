package com.whipclip.rvs.sdk.aysncObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whipclip.rvs.sdk.dataObjects.RVSTranscriptItemExtendedList;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSGetTranscriptItemExtendedList extends RVSGetPagingListBase{

    private Logger logger = LoggerFactory.getLogger(RVSGetTranscriptItemExtendedList.class);

    private List<RVSPostImp> responseBuffer;

    public RVSGetTranscriptItemExtendedList(String method, HashMap<String,String> params) {
        super(method, params, RVSTranscriptItemExtendedList.class, "transcriptItems");
    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSPostImp>();

        responseBuffer.addAll(responseArray);
       
        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
