package com.whipclip.rvs.sdk.aysncObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;

import com.loopj.android.http.BinaryHttpResponseHandler;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.util.Utils;

/**
 * Created by iliya on 7/11/14.
 */
public class RVSImageFromUrlOrArray implements RVSAsyncImage {

    Logger logger = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);

//    private ArrayList<LinkedHashMap<String, String>> images;
    private List<RVSImage> images;
    private static Handler mHadler = new Handler(Looper.getMainLooper());

    public RVSImageFromUrlOrArray(List<RVSImage> images) {

        this.images = images;

        if (images.size() > 1)
        {
            Collections.sort(this.images, RVSImage.heightComparator);
        }

        logger.info("image size " + this.images.size());
    }

    public RVSImageFromUrlOrArray(String imageUrl) {

        // make it look like a single image in the array
        RVSImage image = new RVSImage();
        image.setUrl(imageUrl);

        images = new ArrayList<RVSImage>();
        images.add(image);
    }

    /**
     *
     * @param sizeDip required image size in UI points (dip)
     * @return RVSPromise with Bitmap object on DoneCallback
     */
    @Override
    public RVSPromise getImageWithSize(Point sizeDip) {

        return getImageWithSize(sizeDip, null);
    }
    
    
    public static RVSPromise loadAsyncImage(final String url, final RVSPromise promise) {
    	if (url == null) {
            promise.rejectWithDelay(new Error("Bad url"), 300);
            return promise;
        }
        
    	Looper myLopper = Looper.myLooper();
        if (myLopper != null && Looper.getMainLooper() == myLopper) { // in UI thread
        	
        	getAsync(url, promise);

        } else {
            // post to ui thread, since JsonResponseHandler must be initialized in thread with looper (used  to handle callbacks via internal handler) !
        	mHadler.post(new Runnable() {
                @Override
                public void run() {
                	getAsync(url, promise);
                }
            });
        }
        return promise;
    }
    
	private static void  getAsync(final String url, final RVSPromise promise){
		
	 	 RVSSdk.sharedRVSSdk().requestManager().get(url, new BinaryHttpResponseHandler() {
	 		
	         @Override
	         public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
	         	try {
	         		 Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
	          		 log.error("download success, url: " + url);
	          		 
	//            		Bitmap bitmap = BitmapFactory.decodeByteArray(binaryData, 0, binaryData.length);
	         		promise.resolve(binaryData);
	         	} catch (Exception e) {
	         		Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
	         		log.info(e.getMessage());
	         		promise.reject(e);
	         	}
	         }
	
	         @Override
	         public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
	
	        	 Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
	      		 log.error("download failed, url: " + url);
	        	 
	             String detailMessage = "request failed with " + statusCode + " " + error.getMessage();
	             promise.reject(new Error(detailMessage));
	         }
	     });
	
	}
	
    public static RVSPromise loadSyncImage(final String url, final RVSPromise promise) {
    	
         if (url == null) {
             promise.rejectWithDelay(new Error("Bad url"), 300);
             return promise;
         }
         
    	 RVSSdk.sharedRVSSdk().requestManager().getSync(url, new BinaryHttpResponseHandler() {

             @Override
             public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
             	try {
             		 Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
              		 log.error("download success, url: " + url);
              		 
//             		Bitmap bitmap = BitmapFactory.decodeByteArray(binaryData, 0, binaryData.length);
             		promise.resolve(binaryData);
             	} catch (Exception e) {
             		Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
             		log.info(e.getMessage());
             		promise.reject(e);
             	}
             }

             @Override
             public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {

            	 Logger log = LoggerFactory.getLogger(RVSImageFromUrlOrArray.class);
          		 log.error("download failed, url: " + url);
            	 
                 String detailMessage = "request failed with " + statusCode + " " + error.getMessage();
                 promise.reject(new Error(detailMessage));
             }
         });

         return promise;
    }
    
    /**
     * Get image URL for given size
     * @param size size required image size in UI points
     * @return string url of image
     */
    @Override
    public String getImageUrlForSize(Point sizeDp) {
    	return getImageUrlForSize(sizeDp, null);
    }

    /**
     *
     * @param sizeDp - size in dip, where x is width, y is height
     * @return closest image url to require size
     */
    @Override
    public String getImageUrlForSize(Point sizeDp,  String type) {

    	
    	List<RVSImage> images;
        if (type != null)
        {
        	List<RVSImage> filteredImages = new ArrayList<RVSImage>();
            for (RVSImage image : this.images) {
            	if (image.getType() != null && image.getType().compareToIgnoreCase(type) == 0) {
            		filteredImages.add(image);
                }
            }
            
            images = filteredImages;
        }
        else
        {
            images = this.images;
        }
        
        
        int count = images.size();

        if (count == 0)
        {
            logger.warn("no images found in image array");
            return null;
        }

        String urlString = null;

        if (count == 1)
        {
            urlString = images.get(0).getUrl();
        }
        else
        {
            float pixelHeight = Utils.convertDipToPixel(sizeDp.y, RVSSdk.getContext());

            // find an image with height bigger or equal to require size by height
            for (RVSImage image : images)
            {
                if (pixelHeight <= image.getHeight())
                {
                    urlString = image.getUrl();
                    break;
                }
            }

            if (urlString == null)
            {
                urlString = images.get(images.size() - 1).getUrl();
            }
        }

        if (urlString == null)
        {
            logger.error("did not found image URL key");
        }

        return urlString;
    }

	@Override
	public RVSPromise getImageWithSize(Point sizeDip, String type) {
		
		final RVSPromise promise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);
        String urlString =  getImageUrlForSize(sizeDip, type);

        if (urlString == null) {

            promise.rejectWithDelay(new Error("Bad url"), 300);
            return promise;
        }

//        AsyncHttpClient client = new AsyncHttpClient();
//        client.setTimeout(20 * 1000);
//        
//        try {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            trustStore.load(null, null);
//            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
//            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//            client.setSSLSocketFactory(sf);   
//        }
//        catch (Exception e) {   
//        }
        
        RVSSdk.sharedRVSSdk().requestManager().get(urlString, new BinaryHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
            	try {
            		Bitmap bitmap = BitmapFactory.decodeByteArray(binaryData, 0, binaryData.length);
            		promise.resolve(bitmap);
            	} catch (Exception e) {
            		logger.info(e.getMessage());
            		promise.reject(e);
            	}
            	
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {

                String detailMessage = "request failed with " + statusCode + " " + error.getMessage();
                promise.reject(new Error(detailMessage));
            }
        });

        return promise;
	}

	@Override
	public String getOverlayedImageUrlForSize(Point sizeDp, String channelId) {

	    int count = this.images.size();
	    
	    if (count == 0)
	    {
	    	logger.warn("no images found in image array");
	        return null;
	    }
	    
	    String imageId = "";
	    
	    if (this.images.size() == 1)
	    {
	        RVSImage image = this.images.get(0);
	        imageId = image.getId();
	    }
	    else
	    {
	    	 float pixelHeight = Utils.convertDipToPixel(sizeDp.y, RVSSdk.getContext());

            // find an image with height bigger or equal to require size by height
            for (RVSImage image : images)
            {
                if (pixelHeight <= image.getHeight())
                {
                	imageId = image.getId();
                    break;
                }
            }
	            
	        if (imageId.isEmpty())
	        {
	            RVSImage image = this.images.get(this.images.size()-1);
	            imageId = image.getId();
	        }
	    }
	    
	    if (imageId.isEmpty())
	    {
	        logger.error("did not found image URL key");
	    }
	    
	    return String.format("https://app.whipclip.com/v1/channels/%s/media/images/%s/overlay", channelId, imageId);

	}

	@Override
	public boolean isContainsType(String type) {
		if (type != null){
			
		    for (RVSImage image : this.images) {
		        if (image.getType().compareToIgnoreCase(type) == 0) {
		            return true;
		        }
		    }
		}
		
		return false;

	}

}
