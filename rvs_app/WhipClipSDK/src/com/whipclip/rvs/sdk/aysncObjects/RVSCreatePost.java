package com.whipclip.rvs.sdk.aysncObjects;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSMediaContextImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;
import com.whipclip.rvs.sdk.util.Utils;

import org.apache.http.Header;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by iliya on 7/28/14.
 */
public class RVSCreatePost {

    Logger logger = LoggerFactory.getLogger(RVSCreatePost.class);

    private final String text;
    private final RVSMediaContextImp mediaContext;
    private final long startOffset;
    private final long duration;
    private final String coverImageId;
    private RequestHandle requestHandler;

    public RVSCreatePost(String text, RVSMediaContext mediaContext, long startOffset, long duration, String coverImageId) {
        this.text = text;
        this.mediaContext = (RVSMediaContextImp)mediaContext;
        this.startOffset = startOffset;
        this.duration = duration;
        this.coverImageId = coverImageId;
    }

    public void cancel() {
        if (requestHandler != null) {
            requestHandler.cancel(true);
        }
    }

    @SuppressWarnings("unchecked")
    public RVSPromise createPost() {

        logger.info("creating post ...");

        Utils.assertCalledNotFromMainThread();

        long startOffsetNew = this.startOffset * 1000;
        long endOffset = (this.startOffset + duration) * 1000;

        HashMap<String,String> postInfo = new HashMap<String, String>();
        postInfo.put("message",text);
        postInfo.put("mediaContext",mediaContext.getContext());
        postInfo.put("startOffset",String.valueOf(startOffsetNew));
        postInfo.put("endOffset", String.valueOf(endOffset));
        postInfo.put("coverImageId",coverImageId);

        List<String> hashTags = hashTagsForPostText();
        List<String> topics = new ArrayList<String>();

        for (String topic : hashTags) {
            topics.add(convertToLegalTopic(topic));
        }

        // todo
//        if (topics.size() > 0)
//            postInfo.put("topics", topics);

        //logger.info(@"posting a post with info: %@", postInfo);

        final RVSPromise promise = RVSPromise.createPromise();
        // todo cancel request instead calling requestManager().postJosnObject
//        ResponseHandlerInterface jsonResponseHandler = getJsonResponseHandler(promise, RVSPostImp.class);
//        RequestParams params = new RequestParams(postInfo);
//        requestHandler = RVSSdk.sharedRVSSdk().requestManager().getAsyncHttpClient().post("posts", params, jsonResponseHandler);
//        final RequestHandle weakRequestHandler = requestHandler;
//        promise.setCancelListener(new RVSPromise.CancelCallback() {
//            @Override
//            public void onCancel() {
//                if (weakRequestHandler != null) {
//                    weakRequestHandler.cancel(true);
//                }
//            }
//        });

        RVSPromise promiseRequest = RVSSdk.sharedRVSSdk().requestManager().postJsonObject("posts", postInfo, RVSPostImp.class);
        promiseRequest.then(new DoneCallback<RVSPost>() {
            @Override
            public void onDone(RVSPost post) {
                promise.resolve(post);
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                promise.reject(error);
            }
        });

        return promise;
    }

    // todo
    private String convertToLegalTopic(String topic) {
        return null;
    }

    /**
     * todo
     */
    private List<String> hashTagsForPostText() {
        return new ArrayList<String>();
    }


    private <T> ResponseHandlerInterface getJsonResponseHandler(final RVSPromise promise, final Class<T> objectClass) {

        return new BaseJsonHttpResponseHandler<T>() {

            @Override
            public void onStart() {
                logger.debug("onStart");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, T response) {

                promise.resolve(response);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, T errorResponse) {

                promise.reject(throwable);
            }


            @Override
            protected T parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                ObjectMapper mapper = new ObjectMapper();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                T result = null;

                try {
                    result = mapper.readValues(new JsonFactory().createParser(rawJsonData), objectClass).next();
                } catch (JsonProcessingException e) {
                    logger.error("failed to deserialize json error");
                    promise.reject(e);
                }

                return result;
            }
        };
    }
}
