package com.whipclip.rvs.sdk.aysncObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whipclip.rvs.sdk.dataObjects.RVSEpgSlotList;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSEpgSlotImp;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSGetEpgSlotList extends RVSGetPagingListBase{

    private Logger logger = LoggerFactory.getLogger(RVSGetEpgSlotList.class);

    private boolean shouldFilterResponse = false;
    private Set<String> programIds;
    private List<RVSEpgSlotImp> responseBuffer;
    
    

    public RVSGetEpgSlotList(String method,  HashMap<String, String> params, boolean filterRespone) {
        super(method, params, RVSEpgSlotList.class, "epgSlots");

        if (filterRespone)
        {
            shouldFilterResponse = true;
            programIds = new HashSet<String>();

        }
    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSEpgSlotImp>();

        if (shouldFilterResponse)
        {
            // if no posts than nothing to filter
            if (responseArray.size() > 0)
            {
                // filter out all posts with post Id that we already got beofre
                for (Object obj : responseArray)
                {
                    RVSEpgSlotImp epg =  (RVSEpgSlotImp)obj;
                    
                    String programId = epg.getProgram().getProgramId();
                    if (programIds.contains(programId))
                    {
                        logger.info("ignoring redundent program %@", epg.getProgram().getProgramId());
                        continue;
                    }

                    responseBuffer.add(epg);
                    programIds.add(programId);
                }
            }
        }
        else
        {
            responseBuffer.addAll(responseArray);
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
