package com.whipclip.rvs.sdk.aysncObjects;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.whipclip.rvs.sdk.dataObjects.RVSShowList;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSShowImp;

public class RVSGetShowList extends RVSGetPagingListBase {

    private boolean shouldFilterResponse = false;
    private Set<String> showIds  = new HashSet<String>();
    private List<RVSShowImp> responseBuffer;

    
    public RVSGetShowList(String method, boolean filterRespone) {
        super(method, null, RVSShowList.class, "shows");
        
        if (filterRespone)
        {
            shouldFilterResponse = true;
        }
    }
    
    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSShowImp>();

        if (shouldFilterResponse)
        {
            // if no posts than nothing to filter
            if (responseArray.size() > 0)
            {
                // filter out all posts with post Id that we already got beofre
                for (Object obj : responseArray)
                {
                    RVSShowImp show =  (RVSShowImp)obj;

                    if (show == null) continue;
                    
                    responseBuffer.add(show);
                    showIds.add(show.getShowId());
                }
            }
        }
        else
        {
            responseBuffer.addAll(responseArray);
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }




}
