package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSCommentList;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iliya on 7/23/14.
 */
public class RVSGetCommentList extends RVSGetPagingListBase {

    private List<RVSCommentImp> responseBuffer;

    public RVSGetCommentList(String method) {
        super(method, null, RVSCommentList.class, "comments");

    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSCommentImp>();

        //TO DO: TBD? (otherwise just add responseArray to responseBuffer)
        List comments = new ArrayList();

        for (Object obj : responseArray)
        {
            RVSCommentImp comment =  (RVSCommentImp)obj;
            comments.add(comment);
            responseBuffer.add(comment);
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
