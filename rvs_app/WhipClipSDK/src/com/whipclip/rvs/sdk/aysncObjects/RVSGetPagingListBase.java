package com.whipclip.rvs.sdk.aysncObjects;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSPromise.CancelCallback;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSPagingContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPagingListBase;
import com.whipclip.rvs.sdk.util.Utils;

/**
 * Created by iliya on 6/23/14.
 */
public class RVSGetPagingListBase<T> implements RVSAsyncList {

    Logger logger = LoggerFactory.getLogger(RVSPagingListBase.class);

    private String method;
    private Class<T> responseClass;
    private String arrayPropertyName;
    private HashMap<String, String> params;

    protected Integer count;
    private RVSPromise promise;
    private RVSPromise requestPromise;
    private RVSPagingContext pagingContext;
    protected boolean didReachEnd;

    protected RVSGetPagingListBase() {

    }

    public RVSGetPagingListBase(String method, HashMap<String, String> params, Class<T> responseClass, String arrayPropertyName) {
        init(method,params,responseClass,arrayPropertyName);
    }

    protected void init(String method, HashMap<String, String> params, Class<T> responseClass, String arrayPropertyName) {
        this.method = method;
        this.params = params;
        this.responseClass = responseClass;
        this.arrayPropertyName = arrayPropertyName;
    }

    // perform a request to get more objects from the list, count property is not changed by given count parameter
    @SuppressWarnings("unchecked")
    protected void internalNext(int count) {
        if (requestPromise != null && requestPromise.isPending())
            throw new AssertionError("Call next(n) method only when previous next(n) call finished");
        if (didReachEnd == true)
            throw new AssertionError("did reach end");

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("pageSize", String.valueOf(count));
        if (this.pagingContext != null)
            params.put("pagingCookie", this.pagingContext.getForwardCookie());

        if (this.params != null) {
            params.putAll(this.params);
        }

        requestPromise = RVSSdk.sharedRVSSdk().requestManager().getJsonObject(this.method, params, this.responseClass);
        requestPromise.then(new AndroidDoneCallback<RVSPagingListBase>() {
            @Override
            public void onDone(RVSPagingListBase pagingList) {

                ArrayList<Object> responseArray = null;
                try {
                    Field field = pagingList.getClass().getDeclaredField(arrayPropertyName);
                    field.setAccessible(true);
                    responseArray = (ArrayList<Object>)field.get(pagingList);

                } catch (NoSuchFieldException e) {
                    RVSGetPagingListBase.this.promise.reject(new Error("no such field exception: " + arrayPropertyName ));
                    e.printStackTrace();
                    return;
                    
                } catch (IllegalAccessException e) {
                    RVSGetPagingListBase.this.promise.reject(new Error("illegal access e xception"));
                    e.printStackTrace();
                    return;
                    
                } catch (Exception e) {
                	RVSGetPagingListBase.this.promise.reject(new Error("Error during parsing: " +e.getMessage()));
                    e.printStackTrace();
                    return;
                }
                

                if (responseArray != null)
                {
//                    logger.info("got " + responseClass + ", " + pagingList);
                    pagingContext = pagingList.getPagingContext();

                    if (RVSGetPagingListBase.this.pagingContext == null || RVSGetPagingListBase.this.pagingContext.getForwardCookie() == null ||
                        RVSGetPagingListBase.this.pagingContext.getForwardCookie().length() == 0)
                    {
                        didReachEnd = true;
                    }

                    processList(responseArray);
                }
                else
                {
                	if (RVSGetPagingListBase.this.promise.isPending()) {
                		RVSGetPagingListBase.this.promise.reject(new Error("got null response array"));
                	}
                }
            };

            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.BACKGROUND;
            }

        }).fail(new AndroidFailCallback<Throwable>() {

            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.BACKGROUND;
            }

            @Override
            public void onFail(Throwable error) {

                logger.error("promise reject with error");
                promise.reject(error);
            }
        });
    }

    // called on the derived class when the list was received to allow processing by derived classes
    // derived classes should call gotResultList when got result
    public void processList(List responseArray) {
        throw new Error("processList must be overridden");
    }

    // called by the derived class when got the result list
    public  void gotResultList(List resultList) {
        promise.resolve(resultList);
    }

    /**
     * Get more object
     * The returned promise is retained by the async list. To cancel the request just release the async list
     *
     * @param count desired for number of posts to receive, actual received number can be different
     * @return promise for NSArray
     */
    @Override
    public RVSPromise next(int count) {

        Utils.assertCalledNotFromMainThread();

        if (promise != null && promise.isPending()) {
            throw new AssertionError("Call next(n) method only when previous next(n) call finished");
        }


        this.promise = RVSPromise.createPromise(RVSPromise.RVSPromiseExecutionScope.UI);

        this.count = count;
        internalNext(count);

//        final WeakReference<RVSPromise> weakPromise = new WeakReference<RVSPromise>(promise); ;
//        promise.fail(new FailCallback() {
//            @Override
//            public void onFail(Object result) {
//                if (weakPromise.get() != null && weakPromise.get().isCancelled())
//                {
//                    requestPromise.cancel();
//                }
//            }
//        });
        
        promise.setCancelListener(new CancelCallback() {

			@Override
			public void onCancel() {
				 requestPromise.cancel();
				 // todo fix cancel functionality of promise
				 requestPromise = null;
			}
        	
		});

        return promise;
    }

    /**
     * Flag if list reached its end
     */
    @Override
    public boolean getDidReachEnd() {
        return didReachEnd;
    }

    @Override
    public void setDidReachEnd(boolean reachEnd) {

    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
