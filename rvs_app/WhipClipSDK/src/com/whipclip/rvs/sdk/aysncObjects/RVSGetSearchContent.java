package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResults;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSContentSearchResultImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSPostImp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by iliya on 7/26/14.
 */
public class RVSGetSearchContent extends RVSGetPagingListBase {

    private List<RVSContentSearchResultImp> responseBuffer;
    private Set<String> postIds;
    private Set<String> contectIds;

    public RVSGetSearchContent(String searchString, String type, long minStartTime) {

        postIds = new HashSet<String>();
        contectIds = new HashSet<String>();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("freeText", searchString);
        if (type != null) {
            params.put("type", type);
        }
        if (minStartTime != -1) {
            params.put("minStartTime", String.valueOf(minStartTime));
        }

        init("search/content", params, RVSContentSearchResults.class, "results");

    }

    public RVSGetSearchContent(String searchString, String showId, String channelId) {

    	  postIds = new HashSet<String>();
          contectIds = new HashSet<String>();
          
    	  HashMap<String, String> params = new HashMap<String, String>();
    	  params.put("freeText", searchString);
    	  
    	  String method = String.format("search/channels/%s/shows/%s/content", channelId, showId);
    	  
    	  init(method, params, RVSContentSearchResults.class, "results");
	}

	public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSContentSearchResultImp>();

        // if no posts than nothing to filter
        if (responseArray.size() > 0)
        {

            for (Object obj : responseArray)
            {
                RVSContentSearchResultImp result =  (RVSContentSearchResultImp)obj;

                if (result == null) continue;
                
                if (result.getPost() != null)
                {
                	 if (result.getPost().isInappropriate()){
                     	logger.info("ignoring inappropriate post " + result.getPost().getPostId());
                     	continue;
                     }
                	 

                    if (postIds.contains(result.getPost().getPostId()))
                    {
                        logger.info("ignoring redundent post " + result.getPost().getPostId());
                        continue;
                    }

                    postIds.add(result.getPost().getPostId());
                }
                else if (result.getProgramExcerpt() != null)
                {
                    if (contectIds.contains(result.getProgramExcerpt().getProgram().getContentId()))
                    {
                        logger.info("ignoring redundent program " + result.getProgramExcerpt().getProgram().getProgramId());
                        continue;
                    }

                    contectIds.add(result.getProgramExcerpt().getProgram().getContentId());

                }

                //add result
                responseBuffer.add(result);
            }
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd)
        {
            internalNext(count -  responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;
    }
}
