package com.whipclip.rvs.sdk.aysncObjects;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.loopj.android.http.RequestHandle;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;
import com.whipclip.rvs.sdk.util.Utils;

/**
 * Created by iliya on 8/7/14.
 */
public class RVSCreateComment {

    private Logger logger = LoggerFactory.getLogger(RVSCreateComment.class);
    
    private String postId;
    private String message;
    private RequestHandle requestHandler;


    public RVSCreateComment(String message, String postId) {
        Utils.assertInEmptyString(postId);
        Utils.assertInEmptyString(message);

//        try {
//        	this.message = URLEncoder.encode(message, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
            
            this.message = message;
//        }
        
        this.postId = postId;
    }

    public void cancel() {
        if (requestHandler != null) {
            requestHandler.cancel(true);
        }
    }

    public RVSPromise createComment() {

        Utils.assertCalledNotFromMainThread();
        
        HashMap<String,String> postInfo = new HashMap<String, String>();
        postInfo.put("message", message);

        logger.info("posting a post with comment message: " + message);

          /**********/
        // todo change getjson to RVSJsonHttpResponseHandler in RVSCreatePost
        final RVSPromise promise = RVSPromise.createPromise();
//        ResponseHandlerInterface jsonResponseHandler = new RVSJsonHttpResponseHandler(promise, RVSCommentImp.class);
        // todo cancel request
//        RequestParams params = new RequestParams(postInfo);
//        requestHandler = RVSSdk.sharedRVSSdk().requestManager().getAsyncHttpClient().post(method, params, jsonResponseHandler);
//        final RequestHandle weakRequestHandler = requestHandler;
//        promise.setCancelListener(new RVSPromise.CancelCallback() {
//            @Override
//            public void onCancel() {
//                if (weakRequestHandler != null) {
//                    weakRequestHandler.cancel(true);
//                }
//            }
//        });

        //POST /posts/{postId}/comments
        String method = String.format("posts/%s/comments", postId);
        RVSPromise promiseRequest = RVSSdk.sharedRVSSdk().requestManager().postJsonObject(method, postInfo, RVSCommentImp.class);
        promiseRequest.then(new DoneCallback<RVSComment>() {

            @Override
            public void onDone(RVSComment comment) {
                promise.resolve(comment);
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
                promise.reject(error);
            }
        });

        return promise;
    }
}
