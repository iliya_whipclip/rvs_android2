package com.whipclip.rvs.sdk.aysncObjects;

import com.whipclip.rvs.sdk.dataObjects.RVSNotificationList;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSNotificationImp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iliya on 8/8/14.
 */
public class RVSGetNotificationList extends RVSGetPagingListBase {

    private List<RVSNotificationImp> responseBuffer;

    public RVSGetNotificationList(String method) {

        super(method, null, RVSNotificationList.class, "notifications");

    }

    public void processList(List responseArray) {

        if (responseBuffer == null)
            responseBuffer = new ArrayList<RVSNotificationImp>();

        //TO DO: TBD? (otherwise just add responseArray to responseBuffer)
        List<RVSNotificationImp> comments = new ArrayList<RVSNotificationImp>();

        for (Object obj : responseArray) {
            RVSNotificationImp comment = (RVSNotificationImp) obj;
            comments.add(comment);
            responseBuffer.add(comment);
        }

        //if response buffer don't contain enough items, perform another request so caller won't think its end of list
        if (responseBuffer.size() < this.count && !this.didReachEnd) {
            internalNext(count - responseBuffer.size());
            return;
        }

        gotResultList(responseBuffer);
        responseBuffer = null;

    }
}
