package com.whipclip.rvs.sdk.aysncObjects;

import java.util.HashMap;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.RVSPromise.RVSPromiseExecutionScope;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.util.Utils;

public class RVSUpdateUserProfile {
	
	private Logger logger = LoggerFactory.getLogger(RVSUpdateUserProfile.class);
	 
	private String mName;
	private String mHandle;
	private String mEmail;
	
	private RVSPromise updateProfilePromise;

	public RVSUpdateUserProfile(String name, String handle, String email) {
		
		this.mName = name;
		this.mHandle = handle;
		this.mEmail = email;
	}

	@SuppressWarnings("unchecked")
	public RVSPromise updateProfile() {
		
		Utils.assertCalledNotFromMainThread();
		
	    final RVSPromise promise = RVSPromise.createPromise(RVSPromiseExecutionScope.UI);
	    HashMap<String,String> parameters = new HashMap<String,String>();
	    
	    String myUserId = RVSSdk.sharedRVSSdk().getMyUserId();
	    
	    
	    if (mName != null && mName.length() > 0){
	        parameters.put("name",mName);
	    }
	    
	    if (mEmail != null && mEmail.length() > 0){
	    	 parameters.put("email", mEmail);	   
	    }
	    
	    if (mHandle != null && mHandle.length() > 0){
	    	 parameters.put("handle", mHandle);	   
	    }
	    
	    String method = String.format("users/%s/profile", myUserId);
	    this.updateProfilePromise = RVSSdk.sharedRVSSdk().requestManager().post(method, parameters);
		this.updateProfilePromise.then(new DoneCallback<Object>() {
            @Override
            public void onDone(Object result) {
            	if (!promise.isCancelled()) { 
            		promise.resolve(result);
            	}
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable error) {
            	
            	if (!promise.isCancelled()) {
            		logger.error("failed to create post with error " + error.getMessage());
            		promise.reject(error);
            	}
            }
        });		
	    
	    return promise;
	}

}
