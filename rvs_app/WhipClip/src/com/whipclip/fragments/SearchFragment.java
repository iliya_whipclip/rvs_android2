package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSChannelSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchChannelSuggestionResult;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchContentSuggestionResult;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchMatch;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchUserSuggestionResult;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUserSearchResult;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSSearchCompositeResultListImp;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSSearchMatchImp;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.SearchAdapter;
import com.whipclip.ui.adapters.SearchResultsAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.adapters.items.searchitems.SearchItemHeader;
import com.whipclip.ui.adapters.items.searchitems.SearchItemVideo;
import com.whipclip.ui.adapters.items.searchitems.SearchItemWeb;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsChannel;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsMultiply;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsMultiplyItem;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsScrollUsers;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsSingle;

public class SearchFragment extends BasePostsFragment<List<Object>> implements OnQueryTextListener, OnFocusChangeListener
{
	public final static int MAX_MATCHES_IN_RESULT = 6;

	private MenuItem									_searchMenuItem			= null;
	private SearchView									_searchView				= null;

	private boolean										_loadingResults			= false;
	private boolean										_refreshSearchList		= false;
	private RVSAsyncList								_channelAsyncList;
	private RVSAsyncList								_contentAsyncList;
	private RVSAsyncList								_userAsyncList;
	private RVSAsyncList								_recentAsyncList;
	private RVSAsyncList								_trendingAsyncList;
	private RVSPromise<List<Object>, Throwable, Integer>	_channelPromise;
	private RVSPromise<List<Object>, Throwable, Integer>	_contentPromise;
	private RVSPromise<List<Object>, Throwable, Integer>	_userPromise;
	private RVSPromise<List<RVSPost>, Throwable, Integer>	_recentPromise;
	private RVSPromise<List<RVSPost>, Throwable, Integer>	_trendingPromise;
	private RVSPromise									_suggestionPromise;
	private TextView									_title;
	private String										_searchQuery;
	private String										_searchedUser;
	private LinearLayout								_emptyView;

	private int											SEARCH_REQUESTS			= 15;
	private boolean										_suggestionsVisible;
	private boolean										_dontCollapse;
	private boolean										_isFromSearchText		= false;
	//	private boolean										_fetchingSuggestions	= false;
	//prevents the user from clicking multiply times on the same line, since it'll send multiply requests to the server, and ends up getting multiply (and the same) channels.
	private boolean										_isCurrentlySearching	= false;
	//requesting each search for 3 requests : channels, users, clips.We're waiting for all to come, and then reloading all at once.
	private int											_finishCounter			= 0;

	//relevant to the _searchResult - only on the 1st entrance to SearchFragment we'd like to open the keyboard at once.
	private boolean										_firstTime				= true;
	//This is a patch for the searchView : when we're in other screen (comments, userprofile etc) and we're
	//coming back to SearchFragment, we don't want the search field to reset, as well as not show the keyboard.
	//we'll arrive to onQueryTextSubmit and then to onQueryTextChange(String text) - but we won't make a search 
	//on the sdk, since this is just for the _searchView.
	private boolean										_searchResults			= true;
	
	
	
	enum SearchDownloadType {
		CHANNEL,
		USER,
		CONTECT,
	};
	
	private Queue<SearchDownloadType> searchDownloadQueue = new LinkedList<SearchDownloadType>();

	@Override
	public void onRefresh()
	{
		super.onRefresh();
		_isCurrentlySearching = false;
		if (!_suggestionsVisible && !_loadingResults)
		{
			search(_searchQuery);
		}
	}

	public void showSearchResults(String arg0)
	{
		search(arg0);
	}

	@SuppressWarnings("unchecked")
	private void search(String searchQuery)
	{
		_loadingResults = true;
		_swipeLayout.setRefreshing(true);
		_list = new ArrayList<PostUIItem>();
		_postsAdapter = null;
		_searchedUser = searchQuery;
		_finishCounter = 0;
		
		
		_channelAsyncList = AppManager.getInstance().getSDK().searchChannel(searchQuery);
		_userAsyncList = AppManager.getInstance().getSDK().searchUser(searchQuery);
		_contentAsyncList = AppManager.getInstance().getSDK().searchContent(searchQuery, RVSSdk.ContentSearchType.POST_OR_PROGRAM, 1);

		// no need to sync, sync it called from UI thread
		searchDownloadQueue.clear();
		searchDownloadQueue.add(SearchDownloadType.CHANNEL);
		searchDownloadQueue.add(SearchDownloadType.USER);
		searchDownloadQueue.add(SearchDownloadType.CONTECT);
		
		doSearchDownloadStep();
		
//		_channelPromise = _channelAsyncList.next(10);
//		_channelPromise.then(this, this);
//		
//		_userPromise = _userAsyncList.next(10);
//		_userPromise.then(this, this);
//		
//		_contentPromise = _contentAsyncList.next(10);
//		_contentPromise.then(this, this);
	}

	private void doSearchDownloadStep()
	{
		SearchDownloadType searchType = searchDownloadQueue.poll();
		if (searchType != null) {
			switch (searchType)
			{
				case CHANNEL:
					_channelPromise = _channelAsyncList.next(10);
					_channelPromise.then(this, this);
					break;
				
				case USER:
					_userPromise = _userAsyncList.next(10);
					_userPromise.then(this, this);
					break;
					
				case CONTECT:
					_contentPromise = _contentAsyncList.next(10);
					_contentPromise.then(this, this);
					break;

				default:
					break;
			}
		}
	}

	@Override
	public void initViews(View fragmentView)
	{
		getPosts(SEARCH_REQUESTS);
		_emptyView = (LinearLayout) fragmentView.findViewById(R.id.empty_view);

		_swipeLayout.setEnabled(false);
		_title = (TextView) fragmentView.findViewById(R.id.txt_search);
	}

	@Override
	public void initListeners()
	{

	}

	@Override
	protected void loadMorePostResults(int totalItemCount)
	{
		//		if (_channelAsyncList != null && !_channelAsyncList.getDidReachEnd() && !_channelPromise.isPending())
		//		{
		//			_channelPromise = _channelAsyncList.next(5);
		//			_channelPromise.then(this, this);
		//		}
		//		if (_userAsyncList != null && !_userAsyncList.getDidReachEnd() && !_userPromise.isPending())
		//		{
		//			_userPromise = _userAsyncList.next(5);
		//			_userPromise.then(this, this);
		//		}
		if (_contentAsyncList != null && !_contentAsyncList.getDidReachEnd() && !_contentPromise.isPending() && !_suggestionsVisible)
		{
			try
			{
				_progressBar.setVisibility(View.VISIBLE);
				if (!_contentAsyncList.getDidReachEnd())
				{
					_contentPromise = _contentAsyncList.next(5);
					_contentPromise.then(this, this);
				}
			}
			catch (Throwable e)
			{
				_progressBar.setVisibility(View.GONE);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy()
	{
		stopPendingOperations();
		getActionBar().setDisplayHomeAsUpEnabled(true);
//		((MainActivity) getActivity()).showNavigation();
		AppManager.getInstance().clearRealTimeScrollUsersInformation();
		super.onDestroy();
	}

	private void stopPendingOperations()
	{
		if (_channelPromise != null)
		{
			_channelPromise.cancel();
		}
		if (_contentPromise != null)
		{
			_contentPromise.cancel();
		}
		if (_userPromise != null)
		{
			_userPromise.cancel();
		}
		if (_recentPromise != null)
		{
			_recentPromise.cancel();
		}
		if (_trendingPromise != null)
		{
			_trendingPromise.cancel();
		
		}
		if (_suggestionPromise != null)
		{
			_suggestionPromise.cancel();
		}
	}

	@Override
	public void onResume()
	{
		boolean destroying = AppManager.getInstance().getRefreshPosts();
		super.onResume();

		if (destroying)
		{
			return;
		}
		_dontCollapse = false;
		setHasOptionsMenu(true);
		_refreshSearchList = false;
		AppManager.getInstance().sendToGA(getString(R.string.search));
		getActivity().invalidateOptionsMenu();

		if (_postsAdapter != null)
		{ //when returning from profile fragment (if the user wants to see the profile of other user in the userss search results)
			//we have to update the adapter, since the user might had changed the follow.
			_postsAdapter.notifyDataSetChanged();
		}

		if (_firstTime == false)
		{
			if (_searchView != null) 
			{
				_searchResults = false;
//				_searchView.setQuery(_searchedUser, false);
//				_searchView.clearFocus();
			}
		}
		else
		{
			_firstTime = false;
		}

		_handler.postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				if (_searchView != null)
				{
					_searchView.setVisibility(View.INVISIBLE);
					_searchView.setVisibility(View.VISIBLE);
				}
			}
		}, 10);
	}

	/**
	 * Action bar settings
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		menu.clear();
		inflater.inflate(R.menu.feeds, menu);
//		((MainActivity) getActivity()).hideNavigation();

		ActionBar actionBar = getActionBar();
		setActionBarSearchSettings(menu);
		//removing the whipclip logo
		actionBar.setTitle(null);
		actionBar.setLogo(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		actionBar.setDisplayHomeAsUpEnabled(false);
//		((MainActivity) getActivity()).hideNavigation();

		_searchMenuItem.expandActionView();

		//		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPause()
	{
		_dontCollapse = true;
		_refreshSearchList = false;
		if (_searchMenuItem != null) _searchMenuItem.collapseActionView();
		((MainActivity) getActivity()).dismissKeyboard();
		super.onPause();
	}

	private void setActionBarSearchSettings(final Menu menu)
	{
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
		_searchMenuItem = menu.findItem(R.id.action_search);
		_searchView = (SearchView) _searchMenuItem.getActionView();
		_searchView.setQueryHint(Html.fromHtml("<small>" + getResources().getString(R.string.search_hint) + "</small>"));
		int textViewID = _searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		AutoCompleteTextView searchTextView = (AutoCompleteTextView) _searchView.findViewById(textViewID);
		searchTextView.setTextColor(Color.BLACK);
		searchTextView.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		try
		{
			java.lang.reflect.Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
			mCursorDrawableRes.setAccessible(true);
			mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
		}
		catch (Exception e)
		{
		}

		if (searchManager != null && _searchView != null)
		{
			_searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
			_searchView.setOnQueryTextFocusChangeListener(this);
			_searchView.setOnQueryTextListener(this);
		}

		_searchMenuItem.setOnActionExpandListener(new OnActionExpandListener()
		{

			@Override
			public boolean onMenuItemActionExpand(MenuItem item)
			{
				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem arg0)
			{
				if (!_dontCollapse)
				{
					getActivity().onBackPressed();
				}
				return true;
			}
		});

	}

	public void setSearchText(String text, boolean search)
	{
		_isFromSearchText = true;
		if (_isCurrentlySearching == false)
		{
			_isCurrentlySearching = true;
			_list.clear();
			if (_postsAdapter != null) {
				_postsAdapter.notifyDataSetChanged();
			}
			_searchView.setQuery(text, search);
		}
	}

	@Override
	public boolean onQueryTextSubmit(String arg0)
	{
		if (AppManager.getInstance().hasInternetConnection() == false)
		{
			getMainActivity().noNetworkDialog();
		}
		_searchQuery = arg0;
		if (_searchResults)
		{
			stopPendingOperations();
			showSearchResults(arg0);

			//hides and then un hides search tab to make sure keyboard disappears when query is submitted
			if (_searchView != null)
			{
				_searchView.setVisibility(View.INVISIBLE);
				_searchView.setVisibility(View.VISIBLE);
			}
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onQueryTextChange(String text)
	{
		if ("".equals(text) && _refreshSearchList)
		{ //init search like the 1st time this fragment is opened.
			_listView.setEmptyView(null);
			if (_emptyView != null)
			{
				_emptyView.setVisibility(View.GONE);
			}
			_list.clear();
			if (_postsAdapter != null)
			{
				_postsAdapter.clear();
				_postsAdapter.clearRealtimePost();
				_postsAdapter.notifyDataSetChanged();
			}
			stopPendingOperations();
			getPosts(SEARCH_REQUESTS);
		}
		else if (_isFromSearchText)
		{
			_isFromSearchText = false;
			if (_postsAdapter != null)
			{
				_postsAdapter.notifyDataSetChanged();
			}
		}
		else if ("".equals(text) == false && _searchResults)
		{
			try
			{
				stopPendingOperations();
				_suggestionPromise = AppManager.getInstance().getSDK().searchSuggestions(text);
				_suggestionPromise.then(new DoneCallback<RVSSearchCompositeResultListImp>()
				{

					@Override
					public void onDone(final RVSSearchCompositeResultListImp result)
					{
						Log.e(getClass().getName(), "onDone suggestionPromise"
								+ (result != null ? result.getChannelResults() + " " + result.getUserResults() + " " + result.getContentResults()
										: " result = null"));
						try
						{
							getActivity().runOnUiThread(new Runnable()
							{
								@Override
								public void run()
								{
									_list.clear();
									if (_postsAdapter != null)
									{
										_postsAdapter.notifyDataSetChanged();
									}
									if (result != null && isVisible())
									{

										List<RVSSearchChannelSuggestionResult> channelsList = result.getChannelResults();
										if (channelsList != null)
										{
											for (int i = 0; i < channelsList.size(); i++)
											{
												addItemToList(channelsList.get(i).getChannelName());
											}
										}
										List<RVSSearchUserSuggestionResult> userList = result.getUserResults();
										if (userList != null)
										{
											for (int i = 0; i < userList.size(); i++)
											{
												addItemToList(userList.get(i).getUserName());
											}
										}
										List<RVSSearchContentSuggestionResult> contentsList = result.getContentResults();
										if (contentsList != null)
										{
											for (int i = 0; i < contentsList.size(); i++)
											{
												addItemToList(contentsList.get(i).getSearchText());
											}
										}
										if (_list != null && _list.isEmpty() == false)
										{
											refreshAdapterSuggestions(true);
										}
									}

								}

								private void addItemToList(String name)
								{
									for (int i = 0; i < _list.size(); i++)
									{
										PostUIItem item = _list.get(i);
										if (item instanceof SearchItemWeb)
										{
											if (name != null && name.equalsIgnoreCase(((SearchItemWeb) item).getName()))
											{
												return;
											}
										}
									}
									_list.add(new SearchItemWeb(name));
								}
							});
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}

					}

				}, new FailCallback<Object>()
				{

					@Override
					public void onFail(Object result)
					{
						Log.e(getClass().getName(), "onDone suggestionPromise FAIL");
						getActivity().runOnUiThread(new Runnable()
						{

							@Override
							public void run()
							{
								if (_postsAdapter != null)
								{
									_postsAdapter.notifyDataSetChanged();
								}
								_list.clear();
								getMainActivity().noNetworkDialog();
							}
						});
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			_searchResults = true;
		}

		_refreshSearchList = true;

		return false;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		if (_videoContainer.getVisibility() == View.VISIBLE)
		{
			stopVideo();
		}
	}

	@Override
	protected void getPosts(int number)
	{
		_handler.removeCallbacksAndMessages(this);
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		_list.clear();

		_swipeLayout.setRefreshing(true);
		getRecents();

		getTrending();
	}

	@SuppressWarnings("unchecked")
	private void getTrending()
	{
		_trendingAsyncList = AppManager.getInstance().getSDK().trendingPosts();
		_trendingPromise = _trendingAsyncList.next(10);
		_trendingPromise.then(new DoneCallback<List<RVSPost>>()
		{
			@Override
			public void onDone(List<RVSPost> result)
			{
				Log.e(getClass().getName(), "onDone getTrending - result = " + result);
				HashMap<String, Boolean> addedMap = new HashMap<String, Boolean>();
				if (result != null && isVisible() && !_trendingPromise.isCancelled())
				{
					for (RVSPost post : result)
					{
						String text = post.getText();
						if (text.contains("#"))
						{
							int hashTagIndex = text.indexOf("#");
							int indexOfBlackSpace = text.indexOf(" ", hashTagIndex) == -1 ? text.length() : text.indexOf(" ", hashTagIndex);
							String hashTag = text.subSequence(hashTagIndex, indexOfBlackSpace).toString();
							_list.add(new SearchItemWeb(hashTag));
						}
						else if (post.getProgram() != null && !addedMap.containsKey(post.getProgram().getSpecialShowName()))
						{
							_list.add(new SearchItemWeb(post.getProgram().getSpecialShowName()));
							addedMap.put(post.getProgram().getSpecialShowName(), true);
						}
						else if (post.getChannel() != null && !addedMap.containsKey(post.getChannel().getName()))
						{
							_list.add(new SearchItemWeb(post.getChannel().getName()));
							addedMap.put(post.getChannel().getName(), true);
						}
					}
					refreshAdapterSuggestions(false);
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void getRecents()
	{
		_recentAsyncList = AppManager.getInstance().getSDK().recentPosts();
		_recentPromise = _recentAsyncList.next(10);
		_recentPromise.then(new DoneCallback<List<RVSPost>>()
		{
			@Override
			public void onDone(List<RVSPost> result)
			{
				Log.e(getClass().getName(), "onDone getRecents - result = " + result);
				if (result != null && isVisible() && !_recentPromise.isCancelled())
				{
					if (_list == null)
					{
						_list = new ArrayList<PostUIItem>();
					}
					HashMap<String, Integer> showMap = new HashMap<String, Integer>();

					for (RVSPost post : result)
					{
						String name = "";
						RVSProgram program = post.getProgram();
						if (program != null)
						{
							name = program.getSpecialShowName();
						}
						RVSChannel channel = post.getChannel();
						if (channel != null)
						{
							name = channel.getName();
						}
						if (showMap.containsKey(name))
						{
							showMap.put(name, showMap.get(name) + 1);
						}
						else
						{
							showMap.put(name, 1);
						}
					}
					ValueComparator bvc = new ValueComparator(showMap);

//					HashMap<String, Pair<RVSChannel, RVSShow>> frequencyMap = new HashMap<String, Pair<RVSChannel, RVSShow>>();
					
					TreeMap<String, Integer> sortedFrequencyMap = new TreeMap<String, Integer>(bvc);
					sortedFrequencyMap.putAll(showMap);

					int i = 0;
					for (String sortedShow : sortedFrequencyMap.keySet())
					{
						if (i++ == 3)
						{
							break;
						}
						for (RVSPost post : result)
						{
							RVSProgram program = post.getProgram();
							String syn = "";
							if (program != null)
							{
								syn = program.getSynopsis();
							}
							if (program != null && sortedShow.equals(post.getProgram().getSpecialShowName()))
							{
//								frequencyMap.put(sortedShow, new Pair<RVSChannel, RVSShow>(post.getChannel(), post.getProgram().getShow()));
								_list.add(i - 1, new SearchItemVideo(getActivity(), post.getChannel(), syn));// sortedShow, post.getChannel().getName(), post.getChannel().getLogo()
								//										.getImageUrlForSize(new Point(20, 20))));
								break;
							}
							RVSChannel channel = post.getChannel();
							if (channel != null && sortedShow.equals(channel.getName()))
							{
//								frequencyMap.put(sortedShow, new Pair<RVSChannel, RVSShow>(post.getChannel(), null));
								_list.add(i - 1, new SearchItemVideo(getActivity(), post.getChannel(), syn));// sortedShow, post.getChannel().getName(), post.getChannel().getLogo()
								//										.getImageUrlForSize(new Point(20, 20))));
								break;
							}
						}
					}
					_title.setVisibility(View.VISIBLE);
					refreshAdapterSuggestions(false);
				}
			}
		});
	}

	@Override
	public int getLayoutResourceID()
	{
		return R.layout.fragment_search;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDone(List<Object> result)
	{
		doSearchDownloadStep();
		
		_handler.removeCallbacksAndMessages(this);
		_isCurrentlySearching = false;
		if (result != null && isVisible())
		{
			_title.setVisibility(View.GONE);
			Log.e(getClass().getSimpleName(), "OnDone: " + result);
			if (_finishCounter == 0 && _loadingResults == true)
			{
				_list.clear();
			}
			_finishCounter++;
			if (result != null && !result.isEmpty() && result.get(0) instanceof RVSUserSearchResult && !_userPromise.isCancelled())
			{
				handleUserSearchResult((List<RVSUserSearchResult>) (List<?>) result);
			}
			else if (result != null && !result.isEmpty() && result.get(0) instanceof RVSChannelSearchResult && !_channelPromise.isCancelled())
			{
				handleChannelSearchResult((List<RVSChannelSearchResult>) (List<?>) result);
			}
			else if (result != null && !result.isEmpty() && result.get(0) instanceof RVSContentSearchResult && !_contentPromise.isCancelled())
			{
				handleContentSearchResult((List<RVSContentSearchResult>) (List<?>) result);
			}
			if ((_userPromise != null && _userPromise.isCancelled()) || 
					(_channelPromise != null && _channelPromise.isCancelled()) || 
					(_contentPromise != null && _contentPromise.isCancelled()))
			{
				_swipeLayout.setRefreshing(false);
				_loadingResults = false;
			}
			else if (_finishCounter == 3)
			{
				refreshAdapterResults();
				_finishCounter = 0;
			}
		}
	}
	
	private void setEmptyView()
	{
		_handler.postAtTime(new Runnable()
		{
			@Override
			public void run()
			{
				if (isVisible())
				{
					_loadingResults = false;
					TextView emptyTitle = (TextView) _emptyView.findViewById(R.id.empty_view_text);
					emptyTitle.setText(String.format(getString(R.string.no_results), "'" + _searchQuery + "'"));
					_listView.setEmptyView(_emptyView);
				}
			}
		}, this, SystemClock.uptimeMillis() + 1000);
	}

	/**
	 * @param searchSuggestionResults - true if this is a search suggestion result (coming from _suggestionPromise meaning onQueryTextChange)
	 * 								  - else if this is a getTrending() and/or getRecents() results. 
	 */
	private void refreshAdapterSuggestions(boolean searchSuggestionResults)
	{
		if (!isVisible())
		{
			return;
		}
		_suggestionsVisible = true;
		_swipeLayout.setEnabled(false);
		_swipeLayout.setRefreshing(false);
		_progressBar.setVisibility(View.GONE);

		_isCurrentlySearching = false;
		//Setting the header (from _suggestionPromise - "Search Suggestions").
		if (searchSuggestionResults)
		{
			if (_list != null && _list.isEmpty() == false)
			{
				_list.add(0, new SearchItemHeader(getActivity(), getResources().getString(R.string.search_suggestions)));
			}
		}
		else
		{
			updateListWithHeaders();
		}

		if (_postsAdapter == null || !(_postsAdapter instanceof SearchAdapter))
		{
			_postsAdapter = null;
			Log.i(getClass().getName(), "refreshAdapterSuggestions if");
			_postsAdapter = new SearchAdapter(getActivity(), _list, this);
			_listView.setAdapter(_postsAdapter);
		}
		else
		{
			Log.i(getClass().getName(), "refreshAdapterSuggestions else");
			if (_list != null && !_list.isEmpty()) {
				_postsAdapter.insertElementsToRealtimePost(_list);
				_postsAdapter.notifyDataSetChanged();
			}
		}
	}

	private void updateListWithHeaders()
	{
		if (_list != null && _list.isEmpty() == false)
		{
			if (_list.get(0) instanceof SearchItemHeader == false)
			{
				if (_list.get(0) instanceof SearchItemVideo)
				{
					_list.add(0, new SearchItemHeader(getActivity(), getResources().getString(R.string.trending_live_channels)));
				}
				else
				{
					_list.add(0, new SearchItemHeader(getActivity(), getResources().getString(R.string.trending_searches)));
				}
			}
			else
			{
				int i = 1;
				while (i < _list.size() && _list.get(i) instanceof SearchItemVideo)
				{
					i++;
				}
				if (_list.size() >= i)
				{
					_list.add(i, new SearchItemHeader(getActivity(), getResources().getString(R.string.trending_searches)));
				}
			}
		}
	}

	private void refreshAdapterResults()
	{
		_swipeLayout.setEnabled(true);
		setEmptyView();

		if (!isVisible())
		{
			return;
		}
		_suggestionsVisible = false;
		_swipeLayout.setRefreshing(false);
		_progressBar.setVisibility(View.GONE);

		if (_postsAdapter == null || !(_postsAdapter instanceof SearchResultsAdapter))
		{
			_postsAdapter = new SearchResultsAdapter(getActivity(), _list, this);
			_listView.setAdapter(_postsAdapter);
		}
		else
		{
			_postsAdapter.insertElementsToRealtimePost(_list);
			_postsAdapter.notifyDataSetChanged();
		}
	}

	private void handleContentSearchResult(List<RVSContentSearchResult> list)
	{
		_loadingMore = false;
		_progressBar.setVisibility(View.GONE);
		contentConvertToUIItems(list);
	}

	private void handleChannelSearchResult(List<RVSChannelSearchResult> list)
	{
		channelConvertToUIItems(list);
	}

	private void handleUserSearchResult(List<RVSUserSearchResult> list)
	{
		userConvertToUIItems(list);
	}

	@Override
	public void onFail(Throwable arg0)
	{
		doSearchDownloadStep();
		
		_finishCounter++;
		getMainActivity().noNetworkDialog();
	}

	private ArrayList<PostUIItem> contentConvertToUIItems(List<RVSContentSearchResult> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		for (RVSContentSearchResult post : data)
		{
			handleContentResults(post);
		}
		return _list;
	}

	private ArrayList<PostUIItem> userConvertToUIItems(List<RVSUserSearchResult> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}

		ArrayList<RVSUser> users = new ArrayList<RVSUser>();
		for (RVSUserSearchResult post : data)
		{
			users.add(post.getUser());
		}
		//selecting only Consts.MAX_SEARCH_RESULTS_USERS items for the adapter to display. 
		//new ArrayList<RVSUser>(_users.subList(0, Consts.MAX_SEARCH_RESULTS_USERS - 1))
		_list.add(0, new SearchResultsScrollUsers(getActivity(), users, _searchedUser));
		return _list;
	}

	private ArrayList<PostUIItem> channelConvertToUIItems(List<RVSChannelSearchResult> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		for (RVSChannelSearchResult post : data)
		{
			String syn = "";
			try
			{
				syn = post.getChannel().getCurrentProgram().getSynopsis();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			_list.add(0, new SearchResultsChannel(getActivity(), post.getChannel(), syn));
		}
		return _list;
	}

	public static List<RVSSearchMatch> filteredSearchMatchesFrom(List<RVSSearchMatch> searchMatches, int limit){

		
		String[] knownSearchFieldNames = {
				"transcript",
				"showName",
				"episodeName",
				"episodeSynopsis",
				"showSynopsis",
				"showCast.character",
				"showCast.actor",
				"episodeCast.character",
				"episodeCast.actor",
				"channelName",
				"hotSpot"
				};
				
		if (searchMatches == null || searchMatches.size() == 0) {
	        return searchMatches;
		}
	    
		boolean includeHotSpots = false;
	    String fieldName;
	    List<RVSSearchMatch> filteredSearchMatches = new  ArrayList<RVSSearchMatch>();
	    
	    
	    for (int i = 0; i < knownSearchFieldNames.length; i++)
	    {
	         fieldName = knownSearchFieldNames[i];
	         if (fieldName.equals("transcript")) {
	        	 includeHotSpots = true;
	         }
	         
	         for (Iterator<RVSSearchMatch> it = searchMatches.iterator(); it.hasNext();) {
	        	 RVSSearchMatch  obj = (RVSSearchMatch) it.next();
	        	 String name = obj.getFieldName();
	        	 
	        	 if (name != null && name.equals(fieldName)) {
	        		 
	        		 filteredSearchMatches.add(obj);
	        	 
	        	 } else if (includeHotSpots && name.equals("hotspot")) {
	        		 
	        		 filteredSearchMatches.add(obj);

	        	 }
	        	 
	         }
	        
	        if (filteredSearchMatches.size() > 0)
	            break;
	    }
	    
	    if (filteredSearchMatches.size() > limit)
	    {
	        //cool, continue.
	    	 return filteredSearchMatches.subList(0,  limit);
	    }
//	    else //containing unknown field(s)
//	    {

//	        //fallback
//	    	Collections.sort(searchMatches, new Comparator<RVSSearchMatch>() {
//	            @Override
//	            public int compare(RVSSearchMatch  search1, RVSSearchMatch  search2)
//	            {
//
//	                return  search1.getFieldName().compareTo(search2.getFieldName());
//	            }
//	        });
//	    	
//	    	filteredSearchMatches = searchMatches;
//	    }
//	    
//	    if (filteredSearchMatches.size() > limit) {
//	        filteredSearchMatches = filteredSearchMatches.subList(0,  limit);
//	    }
	    
	    return filteredSearchMatches;
	}
	
	private void handleContentResults(RVSContentSearchResult searchResult)
	{
		List<RVSSearchMatch> searchMatches = searchResult.getSearchMatches();
		if (searchMatches != null && searchMatches.size() > 0 && searchResult.getPost() != null)
		{
			_list.add(generateFullPostItem(searchResult.getPost(), filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT).get(0)));
		}
		else
		{
			RVSProgramExcerpt program = searchResult.getProgramExcerpt();
			if (program.getMediaContext() == null)
			{//multiply	with exception bellow.
				PostUIItem item = generateFakeSingleFromMultiply(searchMatches, searchResult);
				if (item != null)
				{
					_list.add(item);
				}
				else
				{ //generateMultiplyItem might return singleItem, if while iterating searchMatches we're 
					//finding only 1 item.
					if (searchMatches  !=null) {
						_list.add(generateMultiplyItem(program, searchResult, filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT)));
					}
				}
			}
			else
			{//single

				PostUIItem item = generateSingleItem(program, searchResult, filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT));
				if (item != null)
				{
					_list.add(item);
				}

			}
		}
	}

	/******************************************************************************************************************************************
	 * Aux functions for handleContentResults
	 * PostUIItem generateFullPostItem(RVSPost rvsPost, RVSSearchMatch rvsSearchMatch)
	 * PostUIItem generateSingleItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	 * PostUIItem generateFakeSingleFromMultiply(List<RVSSearchMatch> searchMatches, RVSContentSearchResult post)
	 * PostUIItem generateMultiplyItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	 ******************************************************************************************************************************************/
	private PostUIItem generateFullPostItem(RVSPost rvsPost, RVSSearchMatch rvsSearchMatch)
	{
		String searchTitle = "";
		if (rvsSearchMatch != null)
		{
			searchTitle = searchResultsTitleFormat(rvsSearchMatch.getFieldName(), rvsSearchMatch.getValueFragment());
		}
		FullPostView item = new FullPostView(getActivity(), rvsPost, true);
		item.setSearchQuery(_searchQuery);
		item.setSearchTitle(searchTitle);
		return item;
	}

	private PostUIItem generateSingleItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	{
		if (searchMatches.size() == 0) return null;

		if (program.getProgram() != null)
		{ //Getting the type from the 2nd item, if exists. 
			//			if (searchMatches.size() == 2)
			//			{
			//				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), searchResultsTitleFormat(searchMatches.get(1).getFieldName(),
			//						program.getProgram().getShow().getSynopsis()), null, this, _searchQuery, program.getMediaContext());
			//			}
			//			else
			{ //There's no type for this item.
				//				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), program.getProgram().getShow().getSynopsis(), null, this,
				//						_searchQuery, program.getMediaContext());
				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), searchResultsTitleFormat(searchMatches.get(0).getFieldName(),
						searchMatches.get(0).getValueFragment()), null,  _searchQuery, program.getMediaContext());
			}
		}
		else
		{
			RVSSearchMatch match = searchMatches.get(0);
			return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), searchResultsTitleFormat(match.getFieldName(),
					match.getValueFragment()), null,  _searchQuery, program.getMediaContext());
		}
	}

	private PostUIItem generateFakeSingleFromMultiply(List<RVSSearchMatch> searchMatches, RVSContentSearchResult post)
	{
		if (searchMatches.size() == 0) return null;

		//This is basically a multiply item, but it's converted to single item.
		RVSSearchMatch match = searchMatches.get(0);
		if (searchMatches.size() == 2 && searchMatches.get(1).getCoverImage() == null)
		{ //this is a multiply item, BUT, with only 1 item, so we're converting it to single.
			return new SearchResultsSingle(getActivity(), post.getProgramExcerpt(), searchResultsTitleFormat(match.getFieldName(), match.getValueFragment()),
					match.getCoverImage(),  _searchQuery, match.getMediaContext());
		}
		else if (match.getCoverImage() == null)
		{ //simple single element - choosing the 1st item from the search matches.
			return new SearchResultsSingle(getActivity(), post.getProgramExcerpt(), searchResultsTitleFormat(match.getFieldName(), match.getValueFragment()),
					null,  _searchQuery, match.getMediaContext());
		}

		return null;
	}

	private PostUIItem generateMultiplyItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	{
		ArrayList<SearchResultsMultiplyItem> items = new ArrayList<SearchResultsMultiplyItem>();
		for (RVSSearchMatch searchItem : searchMatches)
		{
			if (searchItem.getCoverImage() != null)
			{
				items.add(new SearchResultsMultiplyItem(searchItem));
			}
			if (items.size() == Consts.SEARCH_RESULTS_MULTIPLY_LIMIT)
			{ //maximum results per multiply item.
				break;
			}
		}

		if (items.size() == 1)
		{ //convert multiply item to single item.
			return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), searchResultsTitleFormat(searchMatches.get(0).getFieldName(),
					searchMatches.get(0).getValueFragment()), items.get(0).getImage(), _searchQuery, searchMatches.get(0).getMediaContext());
		}
		else
		{
			SearchResultsMultiply result = new SearchResultsMultiply(getActivity(), items, program);
			result.setSearchQuery(_searchQuery);
			return result;
		}
	}

	/*****
	 * 
	 * enum
	 *
	 */
	private class TitleFormat
	{
		private static final String	POST_MESSAGE		= "postMessage";
		private static final String	TRANSCRIPT			= "transcript";
		private static final String	SHOW_NAME			= "showName";
		private static final String	EPISODE_SYNOPSIS	= "episodeSynopsis";
		private static final String	SHOW_SYNOPSIS		= "showSynopsis";
		private static final String	SHOW_CHARACTER		= "showCast.character";
		private static final String	SHOW_ACTOR			= "showCast.actor";
		private static final String	EPISODE_CHARACTER	= "episodeCast.character";
		private static final String	EPISODE_ACTOR		= "episodeCast.actor";
		private static final String	CHANNEL_NAME		= "channelName";
		private static final String	EPISODE_NAME		= "episodeName";
		private static final String	SHOW_CAST			= "showCast";

	}

	public static String searchResultsTitleFormat(String type, String title)
	{
		Log.i(SearchFragment.class.getName(), "search result : type = " + type + " title = " + title);
		String prefix = "";
		try
		{
			if (TitleFormat.SHOW_NAME.equals(type) || TitleFormat.SHOW_CAST.equals(type))
			{
				prefix = "Show: ";
			}
			else if (TitleFormat.EPISODE_SYNOPSIS.equals(type) || TitleFormat.SHOW_SYNOPSIS.equals(type))
			{
				prefix = "Synopsis: ";
			}
			else if (TitleFormat.SHOW_CHARACTER.equals(type) || TitleFormat.EPISODE_CHARACTER.equals(type))
			{
				prefix = "Character: ";
			}
			else if (TitleFormat.SHOW_ACTOR.equals(type) || TitleFormat.EPISODE_ACTOR.equals(type))
			{
				prefix = "Actors: ";
			}
			else if (TitleFormat.EPISODE_NAME.equals(type))
			{
				prefix = "Episode: ";
			}
			else if (TitleFormat.CHANNEL_NAME.equals(type))
			{
				prefix = "Channel: ";
			}
			else
			{
				prefix = "";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (title != null) {
			return prefix + title.toLowerCase();
		}
		
		return prefix;
	}

	@Override
	protected void convertToUIItems(List<Object> data)
	{
	}

	class ValueComparator implements Comparator<String>
	{

		Map<String, Integer>	base;

		public ValueComparator(Map<String, Integer> base)
		{
			this.base = base;
		}

		// Note: this comparator imposes orderings that are inconsistent with equals.    
		public int compare(String a, String b)
		{
			if (base.get(a) >= base.get(b))
			{
				return -1;
			}
			else
			{
				return 1;
			} // returning 0 would merge keys
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void followUser(String userId)
	{
		RVSPromise followPromise = AppManager.getInstance().getSDK().followUser(userId);
		followPromise.then(new DoneCallback()
		{
			@Override
			public void onDone(Object result)
			{
				Log.i("follow User: OK", "Horray!");
			}
		}, new FailCallback()
		{
			@Override
			public void onFail(Object result)
			{
				Log.e("follow User: NOT OK", ":(");
				getMainActivity().noNetworkDialog();
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unFollowUser(String userId)
	{
		RVSPromise followPromise = AppManager.getInstance().getSDK().unfollowUser(userId);
		followPromise.then(new DoneCallback()
		{
			@Override
			public void onDone(Object result)
			{
				Log.i("UNfollow User: OK", "Horray!");
			}
		}, new FailCallback()
		{
			@Override
			public void onFail(Object result)
			{
				Log.e("UNfollow User: NOT OK", ":(");
				getMainActivity().noNetworkDialog();
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			PendingOperation op = getPendingOperation();
			if (op != null)
			{
				executePendingOperation(op);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void executePendingOperation(PendingOperation op)
	{
		switch (op.getType())
		{
			case COMPOSE_CHANNEL:
			{
				openChannelCompose((String) op.getData1(), (String) op.getData2());
				break;
			}
			case COMPOSE_VIDEO:
			{
				openVideoCompose((RVSMediaContext) op.getData1(), (String) op.getData2());
				break;
			}
			case LIKE:
			{
				if ((Boolean) op.getData2())
				{
					RVSPromise promiss = AppManager.getInstance().getSDK().createLikeForPost((String) op.getData1());
					createPromissHandlers(op, promiss, "create like");
				}
				else
				{
					RVSPromise promiss = AppManager.getInstance().getSDK().deleteLikeForPost((String) op.getData1());
					createPromissHandlers(op, promiss, "delete like");
				}

				break;
			}
			case WHIP:
			{
				if ((Boolean) op.getData2())
				{
					RVSPromise promiss = AppManager.getInstance().getSDK().createRepostForPost((String) op.getData1());
					createPromissHandlers(op, promiss, "create repost(whip)");
				}
				else
				{
					RVSPromise promiss = AppManager.getInstance().getSDK().deleteRepostForPost((String) op.getData1());
					createPromissHandlers(op, promiss, "delete repost(whip)");
				}
				refreshAfterSuccessLogin(op);
				break;
			}
			case COMMENT:
			{
				openComments((String) op.getData1(), (Integer) op.getData2());
				break;
			}
			default:
				break;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createPromissHandlers(final PendingOperation op, RVSPromise promiss, final String type)
	{
		promiss.then(new DoneCallback<Object>()
		{

			@Override
			public void onDone(Object result)
			{
				Log.i(getClass().getName(), "postId " + (String) op.getData1() + " " + type + " successfully.");
				refreshAfterSuccessLogin(op);
			}
		}, new FailCallback<Object>()
		{

			@Override
			public void onFail(Object result)
			{
				Log.e(getClass().getName(), "postId " + (String) op.getData1() + " " + type + " failed.");
			}
		});
	}

	private void refreshAfterSuccessLogin(PendingOperation op)
	{
		// TODO Iliya check 
		_postsAdapter.notifyDataSetChanged();
//		for (int i = 0; i < _list.size(); i++)
//		{ //TODO - This is wrong... find a better way !! maybe call onRefresh() instead. 
//			PostUIItem item = _list.get(i);
//			if (item instanceof FullPostView && ((FullPostView) item).setSelected(op.getType(), (String) op.getData1(), (Boolean) op.getData2()))
//			{
//				break;
//			}
//		}

		onRefresh();
	}

	public void openChannelCompose(String channelID, String syn)
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.BUNDLE_CHANNEL_ID_COMPOSE, channelID);
		bundle.putString(Consts.BUNDLE_CHANNEL_SYN, syn);
		if (getMainActivity().isLoggedIn(this, true))
		{
			((BaseActivity) getActivity()).replaceFragmentContent(ComposeFragment.class, bundle);
		}
		else
		{
			addPendingOperation(new PendingOperation(OperationType.COMPOSE_CHANNEL, channelID, null));
		}
	}

	private void openVideoCompose(RVSMediaContext searchItem, String title)
	{
		AppManager.getInstance().setCurrentMediaContext(searchItem, -1);
		Bundle bundle = new Bundle();
		bundle.putString(Consts.BUNDLE_TEXT_COMPOSE, title);
		((BaseActivity) getActivity()).replaceFragmentContent(ComposeFragment.class, bundle);
	}

	private void openComments(String postId, int numberOfComments)
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.COMMENTS_POST_ID, postId);
		bundle.putInt(Consts.TAB_POSITION, getCurrentTabPosition());
		bundle.putInt(Consts.COMMENTS_NUM_ITEMS, numberOfComments);
//		getMainActivity().replaceFragmentContent(CommentFragment.class, bundle);
//		getMainActivity().replaceTabFragmentContent(TabFragmentAdapter.TAB_POSITION_POPULAR, CommentFragment.class, bundle);
		replaceTabFragmentContent(CommentFragment.class, bundle);

	}

}
