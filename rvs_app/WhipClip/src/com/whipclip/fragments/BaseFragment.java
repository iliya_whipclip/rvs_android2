package com.whipclip.fragments;

import java.util.LinkedList;
import java.util.Queue;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.items.PendingOperation;

public abstract class BaseFragment<T> extends Fragment implements DoneCallback<T>, FailCallback<Throwable>, ProgressCallback<Integer>
{
	protected T								_data;
	protected RVSPromise<T, Throwable, Integer>	_promise;
	protected Handler						_handler	= new Handler();
	protected Queue<PendingOperation>		_pendingOperations;
	protected int							mCurrentTabPosition = -1;
	protected boolean						mIsTabWasSelected = false;


	

	public abstract void initViews(View FfragmentView);

	public abstract void initListeners();

	public void setCurrentTabPosition(int position) {
		this.mCurrentTabPosition = position;
	}

	public int getCurrentTabPosition() {
		return this.mCurrentTabPosition;
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.i(getClass().getSimpleName(), "onCreate");
		_pendingOperations = new LinkedList<PendingOperation>();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {

	    if (isVisibleToUser) { 
			if (getActivity() instanceof MainActivity) {
				getMainActivity().setCurrentFragment(this);
			}
	    }
	    
	    super.setUserVisibleHint(isVisibleToUser);

	}
	
	
	@Override
	public void onResume()
	{
		Log.i(getClass().getSimpleName(), "onResume");
		if (getActivity() instanceof MainActivity) {
			getMainActivity().setCurrentFragment(this);
		}
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.i(getClass().getSimpleName(), "onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		Activity activity = getActivity();
		if (activity != null && activity instanceof MainActivity)
		{
			getMainActivity().refreshSideMenu();
		}
	}

	@Override
	public void onDestroy()
	{
		Log.i(getClass().getSimpleName(), "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onPause()
	{
		Log.i(getClass().getSimpleName(), "onPause");
		super.onPause();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
//		if (getFragmentManager().getBackStackEntryCount() > 1)
		if ( ((MainActivity)getActivity()).getTabBackStackEntryCount() > 0)
		{
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setDisplayShowHomeEnabled(true);
//			getMainActivity().hideNavigation();
			getMainActivity().setTitle();
			getMainActivity().updateActionTitle();
		}
		else
		{
			((MainActivity) getActivity()).restoreActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	public void openNewFragment(Bundle bundle)
	{
		//Doing nothing in the BaseFragment, if needed, implement this at the inherit fragment.
		//This will be called when there's a click on the adapter's items, in order to open a new fragment.
	}

	public ActionBar getActionBar()
	{
		if (getActivity() != null)
		{
			return ((MainActivity) getActivity()).getActionBar();
		}
		return null;
	}

	public boolean onBackPressed()
	{
		return false;
	}

	public MainActivity getMainActivity()
	{
		return (MainActivity) getActivity();
	}

	protected void dismissKeyboard()
	{
		View vFocus = getActivity().getCurrentFocus();
		if (vFocus == null)
		{
			return;
		}
		InputMethodManager imm = (InputMethodManager) WhipClipApplication._context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(vFocus.getApplicationWindowToken(), 0);
	}

	protected void showKeyboard(EditText editText)
	{
		InputMethodManager imm = (InputMethodManager) WhipClipApplication._context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
	}

	protected void setFonts(TextView... views)
	{
		Typeface face = AppManager.getInstance().getTypeFaceRegular();
		for (TextView textView : views)
		{
			textView.setTypeface(face);
		}
	}

	public synchronized void showProgressDialog()
	{
		((BaseActivity) getActivity()).showProgressDialog();
	}

	public synchronized void showProgressDialog(String title, String message)
	{
		((BaseActivity) getActivity()).showProgressDialog(title, message);
	}

	public synchronized void dismissProgressDialog()
	{
		try
		{
			((BaseActivity) getActivity()).dismissProgressDialog();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onProgress(Integer arg0)
	{
		Log.i(getClass().getSimpleName(), "onProgress: " + arg0);
	}

	public void addPendingOperation(PendingOperation pendingOperation)
	{
		_pendingOperations.add(pendingOperation);
	}

	public PendingOperation getPendingOperation()
	{
		if (_pendingOperations != null)
		{
			return _pendingOperations.poll();
		}
		return null;
	}

	protected Bitmap getImage(final String targetPath)
	{
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		BitmapFactory.decodeFile(targetPath, options);

		options.inSampleSize = calculateInSampleSize(options, UiUtils.getDipMargin(50));

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bmp = BitmapFactory.decodeFile(targetPath, options);
		return bmp;
	}

	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth)
	{
		final int targetSize = reqWidth;//70;
		int width_tmp = options.outWidth, height_tmp = options.outHeight;
		int scaleFactor = 1;
		while (true)
		{
			if (width_tmp / 2 < targetSize && height_tmp / 2 < targetSize)
			{
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scaleFactor *= 2;
		}

		if (scaleFactor >= 2)
		{
			scaleFactor /= 2;
		}
		return scaleFactor;
	}

	public void onTabReselected()
	{
		
	}
	
	
	public void onTabSelected(){
		
	}


	public void onTabDeselected(){
		
	}

	
	public String getActionBarTitle()
	{
		return "";
	}
	
	// supporting adding child fragments
//	public FragmentViewPagerTab(Fragment baseFragment, int position)
//	{
//		this.mBaseFragment = baseFragment;
//		if (mBaseFragment instanceof BaseFragment) {
//			((BaseFragment)mBaseFragment).setCurrentTabPosition(position);
//		}
//	}


//	@Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//        View root = inflater.inflate(R.layout.fragment_tab_empty, container, false);
//        mChildFragmentManager = getChildFragmentManager();
//        
//        if (mBaseFragment != null) {
//        	addChildFragment(mBaseFragment);
//        }
//        
//        return root;
//    }
//	
	
	public void addChildFragment(Fragment newFragment) {
		
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.replace(R.id.frame_container, newFragment);
		transaction.addToBackStack(null).commit();
    }


//	public void onTabReselected()
//	{
//		if (mBaseFragment instanceof BaseFragment) {
//			((BaseFragment)mBaseFragment).onTabReselected();
//		}
//	}
	
//	public void onTabSelected()
//	{
//		if (mBaseFragment instanceof BaseFragment) {
//			((BaseFragment)mBaseFragment).onTabSelected();
//		}
//	}
	
//	public Fragment getBaseFragment() {
//		return mBaseFragment;
//	}	

}
