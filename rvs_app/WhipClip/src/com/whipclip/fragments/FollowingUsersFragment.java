package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.adapters.SearchedUsersAdapter;

public class FollowingUsersFragment extends BaseUsersFragment<List<RVSUser>>
{

	@Override
	protected void loadUsers()
	{
		if (_userId != null && !"".equals(_userId))
		{
			_swipeLayout.setRefreshing(true);
			_userAsyncList = AppManager.getInstance().getSDK().followedByUser(_userId);
			if (!_userAsyncList.getDidReachEnd())
			{
				_userPromise = _userAsyncList.next(10);
				_userPromise.then(this, this);
			}
		}
	}

	@Override
	protected void setTitleOnActionBar()
	{
		ActionBar ab = getActionBar();
		ab.setTitle(getString(R.string.following));
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	getMainActivity().onSectionAttached(26);
			AppManager.getInstance().sendToGA(getString(R.string.following));
	    }
	}

	@Override
	public void onResume()
	{
		super.onResume();
//		getMainActivity().onSectionAttached(26);
//		AppManager.getInstance().sendToGA(getString(R.string.following));
	}

	@Override
	public void onDone(List<RVSUser> result)
	{
		if (result != null)
		{
			_swipeLayout.setEnabled(true);

			if (!isVisible())
			{
				return;
			}
			_swipeLayout.setRefreshing(false);

			_listAdapter = new SearchedUsersAdapter(getActivity(), (ArrayList<RVSUser>) result, this, this);
			_usersList.setAdapter(_listAdapter);
		}
		setEmptyView(getString(R.string.no_following));
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}
	
	@Override
	public String getActionBarTitle() {
		return getResources().getString(R.string.following);
	}

}
