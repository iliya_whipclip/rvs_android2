package com.whipclip.fragments;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.ui.adapters.TabFragmentAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class FeedFragment extends PostsFragment
{
	@SuppressWarnings("unchecked")
	protected void getPosts(int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
				//				showProgressDialog();
			}
			if (_dataAsync == null)
			{
				if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID) == null
						|| "".equals(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID)))
				{
					return;
				}
				_dataAsync = AppManager.getInstance().getSDK().newsFeedForUser(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID));
			}
			if (_promise != null && _promise.isPending())
			{
				return;
			}
			if (!_dataAsync.getDidReachEnd())
			{
				_promise = _dataAsync.next(number);
				if (_promise != null)
				{
					_promise.then(this, this);
				}
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		
//		if (((MainActivity) getActivity()).getCurrentTabFragment() instanceof FeedFragment) {
		if (((MainActivity) getActivity()).isMyTabOpened(TabFragmentAdapter.TAB_POSITION_HOME)) {
			
			((MainActivity) getActivity()).onSectionAttached(1);
			AppManager.getInstance().sendToGA(getString(R.string.feed));
			AppManager.getInstance().setRefreshPosts(false);
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(1);
	}

	@Override
	public void onDone(List<RVSPost> arg0)
	{
		super.onDone(arg0);
		TextView empty = (TextView) _fragmentView.findViewById(R.id.empty_view);
		empty.setText(R.string.follow_other_whipclip);
		_listView.setEmptyView(empty);

	}
}