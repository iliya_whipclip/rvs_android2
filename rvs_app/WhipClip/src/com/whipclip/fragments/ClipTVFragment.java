package com.whipclip.fragments;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.CustomSwipeRefreshLayout;
import android.support.v4.widget.CustomSwipeRefreshLayout.OffsetLisetener;
import android.support.v4.widget.CustomSwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AnalogClock;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.WCNotificationManager;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSEpgSlot;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.ClipTvListAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.items.searchitems.ClipTvUIItem;
import com.whipclip.ui.adapters.items.searchitems.ClipTvUIItem.ClipTvViewHolder;
import com.whipclip.ui.widgets.ProgramTimestampLayout;

public class ClipTVFragment extends BaseFragment<Object> implements OnClickListener, IDrawableCache, OffsetLisetener, OnRefreshListener {

//	private static final int STATE_LOADING_NONE 	= -1;
//	private static final int STATE_LOADING_CURRENT 	= 0;
//	private static final int STATE_LOADING_EPG 		= 1;
	
	protected ProgressBar				mProgressBar;

	protected RVSAsyncList				mEpgDataAsyncList;
	protected RVSAsyncList				mCurrentDataAsyncList;
	protected ClipTvListAdapter			mAdapter;
	protected ArrayList<ClipTvUIItem>	mUiItemList;
	
	protected boolean					mLoadingMore	= false;

	protected ListView					mListView;
	private CustomSwipeRefreshLayout	mSwipeLayout;
	
	private View						mFragmentView;

	private ProgramTimestampLayout	mAnalogClockLayout;

	private AnalogClock	mAnalogClock;
	private int	mFirstVisibleItem = -1;
	private int	mVisibleItemCount = -1;

	private final int	GET_TYPE_CURRENT = 0;
	private final int	GET_TYPE_EPG = 1;
	
	private int mGetType = GET_TYPE_CURRENT;

	private FragmentManager	childFragmentManager;
	
	
//	private int mLoadingState = STATE_LOADING_NONE;
	
	
	public ClipTVFragment() {
		super();
		setCurrentTabPosition(TabFragmentAdapter.TAB_POSITION_CLIP_TV);
		
	}
	
    public ClipTVFragment(int tabPosition){
		
		super();
		setCurrentTabPosition(tabPosition);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		if (mFragmentView == null) {
			mFragmentView = inflater.inflate(R.layout.fragment_clip_tv, container, false);
	
	        mListView = (ListView) mFragmentView.findViewById(R.id.shows_list);
//	        mListView. setScrollingCacheEnabled( false );
	        
	    	mSwipeLayout = (CustomSwipeRefreshLayout) mFragmentView.findViewById(R.id.swipe_container);
			mSwipeLayout.setOffsetListener(this);
			mSwipeLayout.setOnRefreshListener(this);
//			mSwipeLayout.setColorScheme(android.R.color.holo_orange_light, android.R.color.holo_red_light, android.R.color.holo_green_light,
//					android.R.color.holo_blue_bright);
			
			mAnalogClockLayout = (ProgramTimestampLayout)mFragmentView.findViewById(R.id.analog_clock_layout);
			mAnalogClockLayout.initLayout();
			

			mSwipeLayout.setColorScheme(R.color.gray_cover, R.color.gray_text, R.color.orange,  R.color.notify_bg);

			mListView.setOnScrollListener(new OnScrollListener() {

				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {}
	
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
					if (totalItemCount != 0 && mLoadingMore == false && firstVisibleItem + visibleItemCount == totalItemCount) {
						mLoadingMore = true;
						loadMorePostResults(5);
					}
					
					if(mFirstVisibleItem  != firstVisibleItem || mVisibleItemCount != visibleItemCount) {
						mVisibleItemCount = visibleItemCount;
						mFirstVisibleItem = firstVisibleItem;
						updateAnalogClock();
//						updateAnalogClockInList(visibleItemCount);
					}
					
					
					updadeStickyHeader();
				}
			});
			
			mAnalogClockLayout.setVisibility(View.GONE);
			
	//		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	//			@Override
	//		    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {
	//				TvShowUIItem item = (TvShowUIItem) adapter.getItemAtPosition(position);
	//				openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
	//		    // TODO Auto-generated method stub
	////		    TextView v=(TextView) view.findViewById(R.id.txtLstItem);
	////		    Toast.makeText(getApplicationContext(), "selected Item Name is "+v.getText(), Toast.LENGTH_LONG).show();
	//		    }
	//		});
			
			
	        mProgressBar = (ProgressBar) mFragmentView.findViewById(R.id.shows_progressbar);
	        
			mUiItemList = new ArrayList<ClipTvUIItem>();
			mAdapter = new ClipTvListAdapter(getActivity(), mUiItemList, this);
			mListView.setAdapter(mAdapter);
			
//			mDataAsyncList = AppManager.getInstance().getSDK().trendingShows();
			
			refreshAllData();
//			long startTime = System.currentTimeMillis();
//			long endTime = startTime + (60 * 60 * 24 * 6) * 1000; //6 days in millisecs
//			
//			    
//			mDataAsyncList = AppManager.getInstance().getSDK().getEpgWithStartTime(startTime, endTime);
			
	//        onFilterButtonClick(true);
	
	        
	//        mAdapter = new TabTvShowsFragmentAdapter(getChildFragmentManager());
	//
	//		mPager = (ViewPager)root.findViewById(R.id.pager_tv_shows);
	//	    mPager.setAdapter(mAdapter);
	//	    mPager.setOffscreenPageLimit(5); //  predefine number of "cached" pages
	//	
	//	    mIndicator = (TabPageIndicator)root.findViewById(R.id.indicator_tv_shows);
	//	    mIndicator.setTabIconLocation (TabPageIndicator.LOCATION_UP);
	//	    mIndicator.setTypeFace(AppManager.getInstance().getTypeFaceLight());
	//	    mIndicator.setViewPager(mPager);
			
		} else if (mFragmentView.getParent() != null) {
			((ViewGroup) mFragmentView.getParent()).removeView(mFragmentView);
		}
		
		
		String intentUri = getMainActivity().getLaunchIntetUri();
		if (intentUri != null ) {
			try
			{
				Intent intent = Intent.parseUri(intentUri, 0);
				if (intent.getAction().equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY)) {
					getMainActivity().setLaunchIntetUri(null);
					onTabSelected();
				}
			}
			catch (URISyntaxException e)
			{
				e.printStackTrace();
			}
		}
        
        return mFragmentView;
    }

	protected void updadeStickyHeader()
	{
		if (mAnalogClockLayout == null) return;
		
		int height = mAnalogClockLayout.getHeight();

		if (mListView.getChildCount() > 1) {
			View childAt = mListView.getChildAt(1);
			if (childAt != null) { 
				Object tag = childAt.getTag();
		//		ClipTvUIItem clipTvUIItem = mUiItemList.get(mFirstVisibleItem);
				if (tag != null)// && tag instanceof ClipTvViewHold) { 
					
//					Log.i("TEST", "tag has header visible: " + ((ClipTvUIItem)tag).isTimestampVisible());;
					if (((ClipTvViewHolder)tag).mAnalogClockLayout.getVisibility() == View.VISIBLE) {
						float y = childAt.getY();
						Log.i("TEST", "second y postion: " + y);
						if (y < height) {
							mAnalogClockLayout.setY(y - height);
						}
						
				}
				
		//			mAnalogClockLayout.setVisibility(View.VISIBLE);
					
				
		//			mAnalogClockLayout.setStartTime(clipTvUIItem.getEpgSlot().getProgram().getStart());
			} 
		}
	}

	@Override
	public void initViews(View FfragmentView)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
			((MainActivity) getActivity()).onSectionAttached(18);
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
//		((MainActivity) getActivity()).onSectionAttached(18);
		// todo workaround, check when after notification, pressing back, no shows
//		onTabSelected();
		
		

		
//		showPages(12);
//		if (!mIsTabWasSelected) {
//			
//			mIsTabWasSelected = true;
//			onFilterButtonClick(true);
//		}
//			getShows(12);
			// TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
			
//		if (((MainActivity) getActivity()).isMyTabOpened(TabFragmentAdapter.TAB_POSITION_TV_SHOWS)) {
//		if (((MainActivity) getActivity()).getCurrentTabFragment() instanceof TVShowsTabFragment) {
//			getShows(12);
			// TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
	}
	
	public void onTabReselected()
	{
		super.onTabReselected();
		
//		getMainActivity().popBackStack(getChildFragmentManager());
		
		if (mListView != null && mAdapter.getCount() > 0) {
//			getMainActivity().popBackStack(getChildFragmentManager());
			mListView.smoothScrollToPosition(0);	
		} else {
			onTabSelected();
		}
	}
	
	@Override
	public void onTabSelected() {
		super.onTabSelected();
		
		// TODO add refresh timeout
//		if (!mIsTabWasSelected) {
			
			mIsTabWasSelected = true;
//			getMainActivity().useNavigation(true);

//			getMainActivity().popBackStack(getChildFragmentManager());
			refreshAllData();
			
		

//			TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
	}

	@Override
	public void onTabDeselected() {
		super.onTabDeselected();
		
		if (_promise != null){
			_promise.cancel();
			_promise = null;
		}
		
//			AppManager.getInstance().sendToGA(getString(R.string.));
	}

	
	
	protected void getCurrentProgram(int number)
	{
		mGetType = GET_TYPE_CURRENT;
		
		Log.i(getClass().getSimpleName(), "getEpgSlots");

		if (!mLoadingMore && !mSwipeLayout.isRefreshing())
		{
			mSwipeLayout.setRefreshing(true);
		}
		
		if (_promise != null && _promise.isPending()){
			return;
		}
		
		try{
			_promise = mCurrentDataAsyncList.next(number);
		}
		catch (Throwable e) {
			_promise = null;
			mSwipeLayout.setRefreshing(true);
			e.printStackTrace();
		}
		
		if (_promise != null){
			_promise.then(this, this);
		}
	}
	
	protected void getEpgSlots(int number)
	{
		mGetType = GET_TYPE_EPG ;
		Log.i(getClass().getSimpleName(), "getEpgSlots");

		if (!mLoadingMore && !mSwipeLayout.isRefreshing())
		{
			mSwipeLayout.setRefreshing(true);
		}
		
		if (_promise != null && _promise.isPending()){
			return;
		}
		
		try{
			_promise = mEpgDataAsyncList.next(number);
		}
		catch (Throwable e) {
			_promise = null;
			mSwipeLayout.setRefreshing(false);
			e.printStackTrace();
		}
		
		if (_promise != null){
			_promise.then(this, this);
		}
			
	}

	
//	@SuppressWarnings("unchecked")
//	private DoneCallback<List<RVSShow>> getOnShowLoadedDoneCallback()
//	{
//		return new AndroidDoneCallback<List<RVSShow>>(){
//			
//			@Override
//			public void onDone(final List<RVSShow> result)
//			{
//				
//			}
//
//			@Override
//			public AndroidExecutionScope getExecutionScope()
//			{
//				return AndroidExecutionScope.UI;
//			}
//		};
//		
//	}
	
	
	@Override
	public void onPause()
	{
//		if (mImagesCache != null){
//			mImagesCache.clear();
//			mImagesCache = null;
//		}
		
		super.onPause();
	}

	

	@Override
	public void onDestroy()
	{
//		if (mImagesCache != null){
//			mImagesCache.clear();
//			mImagesCache = null;
//		}
		
		if (_promise != null){
			_promise.cancel();
			_promise = null;
		}
		
		mProgressBar = null;

		mEpgDataAsyncList = null;
		mCurrentDataAsyncList = null;
		mAdapter = null;
		mUiItemList = null;
		mListView.setAdapter(null);
		mListView = null;
		mSwipeLayout = null;

		mFragmentView = null;

		mAnalogClockLayout = null;

		mAnalogClock = null;
		childFragmentManager = null;
			
		if (mSwipeLayout != null) {
			mSwipeLayout.setOffsetListener(null);
			mSwipeLayout.setOnRefreshListener(null);
		}

		
		super.onDestroy();
	}

//	private FailCallback<Throwable> getFailCallback() {
//		return new FailCallback<Throwable>()
//		{
//
//			@Override
//			public void onFail(Throwable result)
//			{
//				_swipeLayout.setRefreshing(false);
//				
//				_progressBar.setVisibility(View.GONE);
//				_loadingMore = false;
//				dismissProgressDialog();
////				Log.w(getClass().getSimpleName(), "onFail: " + arg0);
//				if (mTrendingTvShowUiItemsList != null)
//				{
//					mTrendingTvShowUiItemsList.clear();
//				}
//				
//				try {
//					getMainActivity().noNetworkDialog();
//				} catch (Exception e) {
//					e.printStackTrace();
//
//				}
//
//				
//			}
//		};
//	}
	
	@Override
	public void onFail(Throwable arg0)
	{
		getMainActivity().runOnUiThread(new Runnable()
		{
			
			@Override
			public void run()
			{
				mSwipeLayout.setRefreshing(false);
				
				mProgressBar.setVisibility(View.GONE);
				mLoadingMore = false;
				dismissProgressDialog();
		//		Log.w(getClass().getSimpleName(), "onFail: " + arg0);
				 
				if (mGetType == GET_TYPE_CURRENT && !mEpgDataAsyncList.getDidReachEnd()) {
					mProgressBar.setVisibility(View.VISIBLE);
					getEpgSlots(12);
				} else {
		
		//			if (mUiItemList != null) {
		//				mUiItemList.clear();
		//				mAdapter.notifyDataSetChanged();
		//			}
					
					setEmptyView(getResources().getString(R.string.no_programs_title_error), getResources().getString(R.string.no_programs_body_error));
				}
		
				try {
					getMainActivity().noNetworkDialog();
				} catch (Exception e) {
					e.printStackTrace();
				}
	
			}
			
		});
		

	}
	
	protected void loadMorePostResults(int totalItemCount)
	{
		if (mGetType == GET_TYPE_CURRENT && !mCurrentDataAsyncList.getDidReachEnd())
		{
			mProgressBar.setVisibility(View.VISIBLE);
			getCurrentProgram(totalItemCount);
		}else if (!mEpgDataAsyncList.getDidReachEnd())
		{
			mProgressBar.setVisibility(View.VISIBLE);
			getEpgSlots(totalItemCount);
		}
	}

	@Override
	public void onDone(Object result)
	{
		mSwipeLayout.setRefreshing(false);
		mProgressBar.setVisibility(View.GONE);
		
		List<RVSEpgSlot> epgList = (List<RVSEpgSlot>)result;
		if (epgList == null || epgList.size() == 0) {
			
			if (mGetType == GET_TYPE_CURRENT) {
				loadMorePostResults(5);
			}
			else {
				_handler.post(new Runnable()
				{
					@Override
					public void run()
					{
						setEmptyView(getResources().getString(R.string.no_programs_title), getResources().getString(R.string.no_programs_body));
					}
				});
			}
			return;
		}
		
		if (!isVisible()){
			return;
		}
		   
		dismissProgressDialog();
		Log.i(getClass().getSimpleName(), "onDone: " + epgList.size());
		
		if (result != null){
			
			convertToUIItems(epgList);
			updateAnalogClock();
			updateAnalogClockInList();
//			updateAnalogClockInList(visibleChildCount);

			mAdapter.notifyDataSetChanged();
		}
		mLoadingMore = false;
	}
	
	private void updateAnalogClock()
	{
		if (mUiItemList != null && mFirstVisibleItem < mUiItemList.size()) {
			mAnalogClockLayout.setVisibility(View.VISIBLE);
			
			ClipTvUIItem clipTvUIItem = mUiItemList.get(mFirstVisibleItem);
			mAnalogClockLayout.setStartTime(clipTvUIItem.getEpgSlot().getProgram().getStart());
			
			// part of sticky implementation, update sticker header when first item changed
			mAnalogClockLayout.setY(0);
		} else {
			mAnalogClockLayout.setVisibility(View.GONE);
		}
	}
	
	private void updateAnalogClockInList()
	{
		if (mUiItemList != null && mUiItemList.size() > 0) {
			
			ClipTvUIItem item = mUiItemList.get(0);
			item.showTimestamp(true);
			long start = item.getEpgSlot().getProgram().getStart();
			
			for (int index = 1; index < mUiItemList.size(); index++ ) {
//				mAnalogClockLayout.setVisibility(View.VISIBLE);
				item = mUiItemList.get(index);
				long itemStart = item.getEpgSlot().getProgram().getStart();
				if (itemStart >  start) {
					start = itemStart;
					item.showTimestamp(true);
				} else {
					item.showTimestamp(false);
				}
			}
		}
	}
	
	
//	private void updateAnalogClockInList(int visibleItemCount)
//	{
//		
//		if (mUiItemList != null && mFirstVisibleItem < mUiItemList.size()) {
//			
//			ClipTvUIItem clipTvUIItem = mUiItemList.get(mFirstVisibleItem);
//			long start = clipTvUIItem.getEpgSlot().getProgram().getStart();
////			clipTvUIItem.showTimestamp(true);
//			
//			// in case list still not visible
//			if (visibleItemCount == 0) {
//				visibleItemCount =  mUiItemList.size();
//			}
//			
//			ClipTvUIItem item = null;
//			int index = (mFirstVisibleItem == -1) ? 0 : mFirstVisibleItem;
//			index++;
//			for (; index < mFirstVisibleItem + visibleItemCount+1 && index < mUiItemList.size(); index++ ) {
////				mAnalogClockLayout.setVisibility(View.VISIBLE);
//				item = mUiItemList.get(index);
//				long itemStart = item.getEpgSlot().getProgram().getStart();
//				if (itemStart >  start) {
//					start = itemStart;
//					item.showTimestamp(true);
//				} else {
//					item.showTimestamp(false);
//				}
//			}
//			
//			mAdapter.notifyDataSetChanged();
////			
////			ClipTvUIItem clipTvUIItem = mUiItemList.get(mFirstVisibleItem);
////			mAnalogClockLayout.setStartTime(clipTvUIItem.getEpgSlot().getProgram().getStart());
//		}
//	}
	
	protected void convertToUIItems(List<RVSEpgSlot> data)
	{
		if (mUiItemList == null) {
			mUiItemList = new ArrayList<ClipTvUIItem>();
			mAdapter = new ClipTvListAdapter(getActivity(), mUiItemList, this);
		}
				 
		int size = mUiItemList.size();
		int index = 0;
		for (RVSEpgSlot epgSlot : data) {
			
			mUiItemList.add(new ClipTvUIItem(getMainActivity(), epgSlot, this, size+index));
			index++;
		}
	}
	
	@Override
	public void addToCache(String name, Drawable draw)
	{
		if (isAdded())
		{
			AppManager.getInstance().getImagesCache().addDrawableToMemoryCache(name, draw);
		}
	}

	@Override
	public Drawable getFromCache(String name)
	{
		return AppManager.getInstance().getImagesCache().getDrawableFromMemCache(name);
	}

	
	// event from list - load more data
	@Override
	public void onRefresh()
	{
		refreshAllData();
	}
	
	private void refreshAllData() {
		
		mGetType = GET_TYPE_CURRENT;
		
		if (mUiItemList != null) {
			mUiItemList.clear();
		}
		
		if (_promise != null) {
			_promise.cancel();
			_promise = null;
		}
		
		mAnalogClockLayout.setVisibility(View.GONE);
		
		mCurrentDataAsyncList = AppManager.getInstance().getSDK().getCurrentPrograms();
		
		long startTime = System.currentTimeMillis();
		long endTime = startTime + (60 * 60 * 24 * 6) * 1000; //6 days in millisecs
		mEpgDataAsyncList = AppManager.getInstance().getSDK().getEpgWithStartTime(startTime, endTime);

		getCurrentProgram(10);
		
		
//		getEpgSlots(12);
	}

	@Override
	public void onOffset(int offset)
	{
		
	}
	
	@Override
	public String getActionBarTitle()
	{
		return getResources().getString(R.string.tab_title_tv_shows);
	}

	@Override
	public void onClick(View v)
	{
		
	}
	
	protected void setEmptyView(String string, String string2)
	{
		if (isVisible() && mFragmentView != null && mListView != null)
		{
			LinearLayout layout = (LinearLayout) mFragmentView.findViewById(R.id.empty_view);
			
			TextView text = (TextView) mFragmentView.findViewById(R.id.empty_view_text1);
			text.setText(string);
			text.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			TextView text2 = (TextView) mFragmentView.findViewById(R.id.empty_view_text2);
			text2.setText(string2);
			text2.setTypeface(AppManager.getInstance().getTypeFaceLight());

			mListView.setEmptyView(layout);
		}
	}
}
