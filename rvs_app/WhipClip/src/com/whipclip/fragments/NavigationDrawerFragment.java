package com.whipclip.fragments;

import java.io.File;
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.UserDetails;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.DrawerMenuAdapter;
import com.whipclip.ui.adapters.items.DrawerMenuItem;
import com.whipclip.ui.views.AsyncImageView;
import com.whipclip.ui.widgets.SearchEditText;
import com.whipclip.ui.widgets.SearchEditText.OnSearchEditTextListener;

public class NavigationDrawerFragment extends Fragment implements View.OnClickListener, OnSearchEditTextListener
{
	public enum NavigationType
	{
		FEEDBACK, TERM_OF_SERVICE, PRIVACY_POLICY, LOGIN, DEVELOPMENT, SETTINGS;
	}

	private static final String			STATE_SELECTED_POSITION	= "selected_navigation_drawer_position";

	private NavigationDrawerCallbacks	_mainActivity;
//	private boolean mIsDebug 			= true;
	private boolean mIsDebug 			= false;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle		_drawerToggle;

	private DrawerLayout				_drawerLayout;
	private ListView					_drawerListView;
	private DrawerMenuAdapter			_listAdapter;
	private View						_fragmentContainerView;
	private View						_view;

	private int							_selectedPosition		= 0;
	protected boolean					_fromSavedInstanceState;

	private LinearLayout 				mProfileLayout;
	private TextView					mSingInOutText;
	private TextView					_userNameText;
	private TextView					_handleText;
	private AsyncImageView				_profileImage;

	private int							_notifications			= 0;

	private SearchEditText	mSearchEditBox;

	private LinearLayout	mUserTextLayout;

	private TextView	mUserText;

	private TextView	mHandlerText;
	
	public NavigationDrawerFragment()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null)
		{
			_selectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			_fromSavedInstanceState = true;
		}

		// Select either the default item (0) or the last selected item.
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				if (isAdded())
				{
					selectItem(_selectedPosition, false);
				}
			}
		}, 200);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = (View) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
		_drawerListView = (ListView) _view.findViewById(R.id.menu_list);

		_drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
//				onNavigationDrawerItemSelected
				_mainActivity.onNavigationDrawerItemSelected(position);
				selectItem(position, true);
			}
		});
		
		ArrayList<DrawerMenuItem> values = new ArrayList<DrawerMenuItem>();
		for (NavigationType navigationType : NavigationType.values())
		{
			if (!mIsDebug && (navigationType == NavigationType.DEVELOPMENT ||  navigationType == NavigationType.SETTINGS)) continue;
			
			DrawerMenuItem drawerMenuItem = getDrawerMenuItem(navigationType);
			if (drawerMenuItem != null)
			{
				values.add(drawerMenuItem);
			}
		}

		_listAdapter = new DrawerMenuAdapter(WhipClipApplication._context, R.layout.menu_list_item, values);
		_drawerListView.setAdapter(_listAdapter);
		setItemChecked(_selectedPosition, true);

		initViewItems();

		return _view;
	}

	private DrawerMenuItem getDrawerMenuItem(NavigationType navigationType)
	{
		switch (navigationType)
		{
//			case TRENDING:
//				return new DrawerMenuItem(R.drawable.icon_trending, getResources().getString(R.string.trending));
//			case FEED:
//				return new DrawerMenuItem(R.drawable.icon_feed, getResources().getString(R.string.feed));
//			case RECENT:
//				return new DrawerMenuItem(R.drawable.icon_recent, getResources().getString(R.string.recent));
//			case LIKED:
//				return new DrawerMenuItem(R.drawable.icon_liked, getResources().getString(R.string.liked));
//			case NOTIFICATIONS:
//				return new DrawerMenuItem(R.drawable.icon_notifications, getResources().getString(R.string.notifications));
			case FEEDBACK:
				return new DrawerMenuItem(R.drawable.feedback_white, getResources().getString(R.string.feedback));
			case DEVELOPMENT:
				return new DrawerMenuItem(R.drawable.icon_settings, getResources().getString(R.string.development));
			case SETTINGS:
				return new DrawerMenuItem(R.drawable.icon_settings, getResources().getString(R.string.settings));
				
			case TERM_OF_SERVICE:
				return new DrawerMenuItem(R.drawable.tos_white, getResources().getString(R.string.term_of_service));
			
			case PRIVACY_POLICY:
				return new DrawerMenuItem(R.drawable.privacy_white, getResources().getString(R.string.privacy_policy));
				
			case LOGIN:
				if (AppManager.getInstance().isLoggedIn()) {
					return new DrawerMenuItem(R.drawable.log_out, getResources().getString(R.string.sign_out));
//					return new DrawerMenuItem(R.drawable.log_out, AppManager.getInstance().isLoggedIn() ? getResources().getString(R.string.sign_out) : getResources().getString(R.string.sign_in));
				} else {
					return null;
				}
		}

		return null;
	}

	public void initViewItems()
	{
		_listAdapter.notifyDataSetChanged();
		
		mSearchEditBox = (SearchEditText) _view.findViewById(R.id.search_layout); 
		mSearchEditBox.setBackground(getResources().getColor(R.color.navigation_bg));
		mSearchEditBox.setHinText(R.string.find_people);
		mSearchEditBox.initLayout((MainActivity) getActivity(), this);
		
		mProfileLayout = (LinearLayout) _view.findViewById(R.id.menu_profile_layout);
		_profileImage = (AsyncImageView) _view.findViewById(R.id.menu_profile_image);
		mSingInOutText = (TextView) _view.findViewById(R.id.menu_sing_in_out_text);
		mSingInOutText.setText(getResources().getString(R.string.sign_in));
		
		mUserTextLayout = (LinearLayout) _view.findViewById(R.id.menu_user_text_layout);
		mUserText = (TextView) _view.findViewById(R.id.menu_user_text);
		mHandlerText = (TextView) _view.findViewById(R.id.menu_handle_text);
		mUserText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		mHandlerText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		
//		_profileText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		mSingInOutText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_profileImage.setImageResource(R.drawable.default_avatar);

		if (AppManager.getInstance().isLoggedIn())
		{

			final String targetPath = WhipClipApplication._context.getCacheDir() + "/images/"
					+ AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).hashCode() + ".png";
			if (new File(targetPath).exists())
			{
				_profileImage.setImageBitmap(UiUtils.getRoundedCornerBitmap(getImage(targetPath)));
			}
			else
			{
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
//						RVSUser myUser1 = RVSSdk.sharedRVSSdk().getMyUser();
//						String path = myUser1.getPofileImage().getImageUrlForSize(new Point(100,100));
						String path = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USER_IMAGE);
						if (isAdded())
						{
							if (path != null && !path.isEmpty())
							{
								AppManager.getInstance().getFilesManager().downloadFile(path, targetPath);

								try
								{
									getActivity().runOnUiThread(new Runnable()
									{
										@Override
										public void run()
										{
											if (isAdded())
											{
												_profileImage.setImageBitmap(UiUtils.getRoundedCornerBitmap(getImage(targetPath)));
											}
										}
									});
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
							}
						}
					}
				}).start();
			}

			UserDetails userDetails = AppManager.getInstance().getUserDetails();
			if (userDetails != null) {
				mUserText.setText(userDetails.getUserName());
				mHandlerText.setText("@" + userDetails.getHandle());

//			RVSUser myUser = RVSSdk.sharedRVSSdk().getMyUser();
//			if (myUser != null) {
//				mUserText.setText(myUser.getName());
//				mHandlerText.setText("@" + myUser.getHandle());
//			}
			
				mSingInOutText.setVisibility(View.GONE);
				mUserTextLayout.setVisibility(View.VISIBLE);
			}
		}

		else
		{
			mSingInOutText.setVisibility(View.VISIBLE);
			mUserTextLayout.setVisibility(View.GONE);
			
			mSingInOutText.setText(getString(R.string.sign_in));
		}

//		_profileImage.setOnClickListener(this);
//		_profileText.setOnClickListener(this);
		mProfileLayout.setOnClickListener(this);
	}

	public void resetProfileInformationUI()
	{
		_listAdapter.notifyDataSetChanged();
		
		if (isAdded()) {
			
			mSingInOutText.setVisibility(View.VISIBLE);
			mUserTextLayout.setVisibility(View.GONE);
			
			mSingInOutText.setText(getString(R.string.sign_in));
			_profileImage.setImageResource(R.drawable.default_avatar);
			
			removeSignOutMenu();
		}
	}

	private void removeSignOutMenu()
	{
		boolean found = false;
		
		ArrayList<DrawerMenuItem> values = _listAdapter.getValues();
		for (DrawerMenuItem item : values) {
			if (item.getImageResource() == R.drawable.log_out) {
				values.remove(item);
				_listAdapter.notifyDataSetChanged();
				break;
			}
		}
	}

	public boolean isDrawerOpen()
	{
		return _drawerLayout != null && _drawerLayout.isDrawerOpen(_fragmentContainerView);
	}

	public DrawerLayout getDrawerLayout()
	{
		return _drawerLayout;
	}

	public ActionBarDrawerToggle getDrawerTOggle()
	{
		return _drawerToggle;
	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout)
	{
		_fragmentContainerView = getActivity().findViewById(fragmentId);
		_drawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer
		// opens
		_drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		_drawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
		_drawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.navigation_drawer_open, /* "open drawer" description for accessibility*/
		R.string.navigation_drawer_close /* "close drawer" description for accessibility*/)
		{
			@Override
			public void onDrawerClosed(View drawerView)
			{
				super.onDrawerClosed(drawerView);
				if (!isAdded())
				{
					return;
				}

				getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView)
			{
				super.onDrawerOpened(drawerView);
				if (!isAdded())
				{
					return;
				}

				getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}
		};

		// Defer code dependent on restoration of previous instance state.
		_drawerLayout.post(new Runnable()
		{
			@Override
			public void run()
			{
				_drawerToggle.syncState();
			}
		});

		_drawerLayout.setDrawerListener(_drawerToggle);
	}

	public void selectItem(final int position, boolean fromItemClick)
	{
		if (_drawerListView != null)
		{
			setItemChecked(position, true);
		}
		if (_drawerLayout != null)
		{
			_drawerLayout.closeDrawer(_fragmentContainerView);
		}
	}
	
	public void closeDrawer() {
		_drawerLayout.closeDrawer(_fragmentContainerView);
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		try
		{
			_mainActivity = (NavigationDrawerCallbacks) activity;
		}
		catch (ClassCastException e)
		{
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		_mainActivity = null;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		_drawerToggle = null;
		_drawerLayout = null;
		_drawerListView.setAdapter(null);
		_drawerListView = null;
		_listAdapter = null;
		_fragmentContainerView = null;
		_view = null;
		mProfileLayout = null;
		mSingInOutText = null;
		_userNameText = null;
		_handleText = null;
		_profileImage = null;
		mSearchEditBox = null;
		mUserTextLayout = null;
		mUserText = null;
		mHandlerText = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, _selectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		_drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
//		if (_drawerLayout != null && isDrawerOpen())
//		{
//			inflater.inflate(R.menu.global, menu);
//			showGlobalContextActionBar();
//		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (_drawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}

		if (item.getItemId() == R.id.action_example)
		{
			Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to
	 * show the global app 'context', rather than just what's in the current
	 * screen.
	 */
	private void showGlobalContextActionBar()
	{
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//		actionBar.setTitle(R.string.app_name);
	}

	private ActionBar getActionBar()
	{
		return getActivity().getActionBar();
	}

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	
	 */
	public static interface NavigationDrawerCallbacks
	{
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int navigationPosition);
		void onNavigationLoggedIn(int currentNavigationPosition);
		
	}
	
	@Override
	public void onClick(View v)
	{
//		if (v.equals(_profileText) || v.equals(_profileImage))
		if (v.equals(mProfileLayout))
		{
			if (!((MainActivity) getActivity()).isLoggedIn(null, false)) {
			
//			if (!AppManager.getInstance().isLoggedIn())
//			{
//				AppManager.getInstance().showLogin(getActivity(), MainActivity.REQUEST_CODE_LOGIN);
				closeDrawer();
			}
			else
			{
				if (((MainActivity) getActivity()).getCurrentFragment() instanceof UserProfileFragment)
				{
					_drawerLayout.closeDrawer(_fragmentContainerView);
					return;
				}
				setItemChecked(_selectedPosition, false);
				_selectedPosition = -12;
				_drawerLayout.closeDrawer(_fragmentContainerView);
				String userID = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);
				((MainActivity) getActivity()).openUserProfile(userID, true);
//				((MainActivity) getActivity()).openMyUserTab();
//				((MainActivity) getActivity()).openUserProfileActivity(TabFragmentAdapter.TAB_POSITION_PROFILE, userID, true);
			}
		}
	}


//	public void selectItemInNavigation(int position)
//	{
//		selectItem(position, false);
//	}

	private void setItemChecked(int selectedPosition, boolean value)
	{
		_drawerListView.setItemChecked(selectedPosition, false);
//		_drawerListView.setItemChecked(selectedPosition, value);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			onLoggedIn(true);
		}
	}

	public void onLoggedIn(boolean switchTabOnLoggedInCancel)
	{
		boolean found = false;
		
		ArrayList<DrawerMenuItem> values = _listAdapter.getValues();
		for (DrawerMenuItem item : values) {
			if (item.getImageResource() == R.drawable.log_out) {
				found = true;
				break;
			}
		}
		
		if (!found) {
			DrawerMenuItem drawerMenuItem = getDrawerMenuItem(NavigationType.LOGIN);
			if (drawerMenuItem != null) {
				values.add(drawerMenuItem);
			}
			_listAdapter.notifyDataSetChanged();
		}
		
		
		initViewItems();
		if (switchTabOnLoggedInCancel) {
			_mainActivity.onNavigationLoggedIn(_selectedPosition);
		}
		
		
	}

	public void setNotifications(int notifications)
	{
		_notifications = notifications;
		View v = _drawerListView.getChildAt(4);
		if (v == null) return;

		TextView notificationsText = (TextView) v.findViewById(R.id.menu_item_notifications);
		notificationsText.setTypeface(AppManager.getInstance().getTypeFaceMedium());

		if (notifications <= 0)
		{
			notificationsText.setVisibility(View.GONE);
			notificationsText.setText("");
		}
		else
		{
			notificationsText.setVisibility(View.VISIBLE);
			notificationsText.setText(notifications + "");
		}
	}

	public int getNotifications()
	{
		return _notifications;
	}

	protected Bitmap getImage(final String targetPath)
	{
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		BitmapFactory.decodeFile(targetPath, options);

		options.inSampleSize = calculateInSampleSize(options, UiUtils.getDipMargin(45));

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bmp = BitmapFactory.decodeFile(targetPath, options);
		return bmp;
	}

	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth)
	{
		final int targetSize = reqWidth;//70;
		int width_tmp = options.outWidth, height_tmp = options.outHeight;
		int scaleFactor = 1;
		while (true)
		{
			if (width_tmp / 2 < targetSize && height_tmp / 2 < targetSize)
			{
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scaleFactor *= 2;
		}

		if (scaleFactor >= 2)
		{
			scaleFactor /= 2;
		}
		return scaleFactor;
	}

	@Override
	public void onClearClick()
	{
		
	}

	@Override
	public void onSearchTextEntered(String searchString)
	{
		
		mSearchEditBox.clear();
		
		if (searchString.compareToIgnoreCase("dev settings") == 0 && !mIsDebug) {
			
			ArrayList<DrawerMenuItem> values = _listAdapter.getValues();
			values.add(getDrawerMenuItem(NavigationType.DEVELOPMENT));
			values.add(getDrawerMenuItem(NavigationType.SETTINGS));
			_listAdapter.notifyDataSetChanged();
			return;
		}
		
		Bundle bundle = new Bundle();
		bundle.putString(Consts.BUNDLE_USER_NAME, searchString);
		
		((MainActivity) getActivity()).replaceFragmentContent(SearchedUsersFragment.class,bundle);
//		((MainActivity) getActivity()).replaceFragmentContent(SearchedUsersFragment.class, bundle);
		
//		((MainActivity) getActivity()).replaceFragmentContent(SearchedUsersFragment.class, bundle);
	}
}
