package com.whipclip.fragments;

import java.util.ArrayList;

import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.CustomSwipeRefreshLayout;
import android.support.v4.widget.CustomSwipeRefreshLayout.OffsetLisetener;
import android.support.v4.widget.CustomSwipeRefreshLayout.OnRefreshListener;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView.FailReason;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView.RVSClipViewStateChangedCallback;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView.State;
import com.whipclip.rvs.sdk.dataObjects.RVSEndCard;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.views.AsyncImageView;

/**
 * A placeholder fragment containing a simple view.
 */
public abstract class BasePostsFragment<T> extends BaseFragment<T> implements OnRefreshListener, RVSClipViewStateChangedCallback,
		OnSeekBarChangeListener, OffsetLisetener, OnClickListener
{
	
	private Logger logger = LoggerFactory.getLogger(BasePostsFragment.class);
	
	
	protected ListView					_listView;
	protected BasePostsListAdapter		_postsAdapter;
//	protected DrawableCache				_photosCache;
	protected CustomSwipeRefreshLayout	_swipeLayout;
	protected ProgressBar				_progressBar;
	protected boolean					_loadingMore	= false;
	protected View						_fragmentView;
	protected ArrayList<PostUIItem>		_list;
	protected RVSAsyncList				_dataAsync;

	protected RVSClipView				_videoView;
	protected RelativeLayout			_videoContainer;
	protected View						_videoReference;
	protected ProgressBar				_videoSpinner;
	protected SeekBar					_seekBar;
	protected SeekBar					_seekBarFull;
	protected Button					_playPauseSeekFull;
	protected TextView					_videoTitle;
	protected Button					_videoPlayPauseButton;
	protected Button					_videoPlayPauseButtonFull;

	protected RelativeLayout			_videoFullScreenBottom;
	protected RelativeLayout			_videoFullScreenTop;
	protected AsyncImageView			_videoFullScreenLogo;
	protected TextView					_videoFullScreenName;

	protected int						_currentTime	= 0;
	private long						_duration		= 0;

	private ProgressBar					_videoSpinnerFull;

	private RelativeLayout				_endCardLayout;
	private TextView					_endCardReplayText;
	private LinearLayout				_endCardReplayButton;
	private AsyncImageView				_endCardAsynImage;

	

	protected abstract void getPosts(int number);

	protected abstract void convertToUIItems(T data);

	public abstract void onDone(T arg0);

	public abstract void onFail(Throwable arg0);

	public abstract int getLayoutResourceID();

	@Override
	public abstract void initViews(View FfragmentView);

	@Override
	public abstract void initListeners();

	@Override
	public void onRefresh()
	{
		if (_videoView != null && _rel != null)
		{
			stopVideo();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if (_fragmentView == null)
		{
			_list = new ArrayList<PostUIItem>();
			_fragmentView = inflater.inflate(getLayoutResourceID(), container, false);
			_listView = (ListView) _fragmentView.findViewById(R.id.posts_list);

			setScrollListener();

			_swipeLayout = (CustomSwipeRefreshLayout) _fragmentView.findViewById(R.id.swipe_container);
			_swipeLayout.setOffsetListener(this);
			_swipeLayout.setOnRefreshListener(this);
			
			_swipeLayout.setColorScheme(R.color.gray_cover, R.color.gray_text, R.color.orange,  R.color.notify_bg);

//			
//			_swipeLayout.setColorScheme(android.R.color.holo_orange_light, android.R.color.holo_red_light, android.R.color.holo_green_light,
//					android.R.color.holo_blue_bright);

			_progressBar = (ProgressBar) _fragmentView.findViewById(R.id.posts_progressbar);
			_seekBar = (SeekBar) _fragmentView.findViewById(R.id.video_seekbar);
			_videoTitle = (TextView) _fragmentView.findViewById(R.id.full_screen_video_title);
			_videoPlayPauseButton = (Button) _fragmentView.findViewById(R.id.full_screen_video_button);

			initViews(_fragmentView);

			_videoContainer = (RelativeLayout) _fragmentView.findViewById(R.id.full_screen_video_container);
			_videoSpinner = (ProgressBar) _fragmentView.findViewById(R.id.video_progress_bar);
			_videoSpinnerFull = (ProgressBar) _fragmentView.findViewById(R.id.full_screen_video_progress_bar);
			_videoFullScreenBottom = (RelativeLayout) _fragmentView.findViewById(R.id.video_seekbar_fullscreen_bottom);
			_videoFullScreenTop = (RelativeLayout) _fragmentView.findViewById(R.id.video_seekbar_fullscreen_top);
			_videoFullScreenLogo = (AsyncImageView) _fragmentView.findViewById(R.id.full_screen_net_logo);
			_videoFullScreenName = (TextView) _fragmentView.findViewById(R.id.full_screen_title);
			_videoPlayPauseButtonFull = (Button) _fragmentView.findViewById(R.id.full_screen_video_button_full);

			_seekBarFull = (SeekBar) _fragmentView.findViewById(R.id.video_seekbar_fullscreen_full);
			_playPauseSeekFull = (Button) _fragmentView.findViewById(R.id.play_pause_seek_full_screen);

			_videoView = new RVSClipView(getActivity());
			_videoContainer.addView(_videoView);

			_endCardLayout = (RelativeLayout) _fragmentView.findViewById(R.id.video_end_card);
			_endCardReplayText = (TextView) _fragmentView.findViewById(R.id.video_replay_text);
			_endCardReplayButton = (LinearLayout) _fragmentView.findViewById(R.id.video_replay_button);
			_endCardAsynImage = (AsyncImageView) _fragmentView.findViewById(R.id.end_card_image);

			_seekBar.setOnSeekBarChangeListener(this);
			_seekBarFull.setOnSeekBarChangeListener(this);
			_videoContainer.setOnClickListener(this);
			_endCardReplayButton.setOnClickListener(this);
			_videoContainer.setOnTouchListener(new OnTouchListener()
			{
				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
					{
						event.setLocation(event.getX(), event.getY() + _videoContainer.getY());
						_listView.dispatchTouchEvent(event);
						return true;
					}
					return false;
				}
			});

			initListeners();
		}
		else if (_fragmentView.getParent() != null)
		{
			((ViewGroup) _fragmentView.getParent()).removeView(_fragmentView);
		}
		setHasOptionsMenu(true);
		return _fragmentView;
	}
	
	protected void onListScrolled(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
		
	}

	protected void setScrollListener()
	{
		_listView.setOnScrollListener(new OnScrollListener()
		{
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState)
			{
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount)
			{
				onListScrolled(view, firstVisibleItem, visibleItemCount, totalItemCount);
				
				if (totalItemCount != 0 && _loadingMore == false && firstVisibleItem + visibleItemCount == totalItemCount)
				{
					_loadingMore = true;
					loadMorePostResults(totalItemCount);
				}

				if (_videoView == null || _videoView.getTag() == null)
				{
					return;
				}

				int position = (Integer) _videoView.getTag();
				//				Log.i("hadas", "_videoContainer.getY() = " + _videoContainer.getY() + " _videoContainer.getHeight() = " + _videoContainer.getHeight());
				float yPosition = _videoContainer.getY();
				if (/*(position < firstVisibleItem) ||*/
				(position >= firstVisibleItem + visibleItemCount && getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) // Yuval - added configuration check
						|| ((yPosition < 0 && Math.abs(_videoContainer.getY()) >= _videoContainer.getHeight()) || (yPosition >= 0 && yPosition
								+ getActionBar().getHeight() >= WhipClipApplication._screenHeight))) //the video is out of bounds in the last ||
				{
					//					Log.i("hadas", "iifififififfffff");
					stopVideo();
					_endCardLayout.setVisibility(View.GONE);
					_videoView.setVisibility(View.GONE);
					_seekBar.setVisibility(View.GONE);
					_videoContainer.setVisibility(View.GONE);
				}
				
				_listView.getTop();
				int top = ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).getTop();
				logger.info("top on scroll: " + top);
				if (_rel != null && _videoView != null && getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				{
					int postDescHeight = 0;
					if (((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.post_description) != null)
					{
						postDescHeight = ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.post_description).getHeight();
						if (((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.refer_layout).getVisibility() == View.VISIBLE) {
							postDescHeight += ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.refer_layout).getHeight();
						}
					}
					_videoContainer.setY(getListItemTop(_rel) + postDescHeight);
				}
			}

		});
	}
	
	/**
	 * video_place_holder element in video_display.xml
	 * @return 
	 */
	private int getListItemTop(RelativeLayout videoPlaceHolder) {
		
		ViewGroup parent = (ViewGroup)videoPlaceHolder.getParent();
		while (parent != null) {
			if (parent.getId() == R.id.post_list_item_layout || parent.getId() == R.id.search_results_layout) {
				
				int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_top_bottom);
				return parent.getTop() + offset;
			}
			parent = (ViewGroup)parent.getParent();
		}
		
		return 0;
	}

	protected void loadMorePostResults(int totalItemCount)
	{
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		
		if (activity instanceof MainActivity) {
			((MainActivity) activity).onSectionAttached(0);
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	if (_videoView != null && _videoView.getState() == State.PAUSED && _backgroundImage != null && _backgroundImage.getVisibility() != View.VISIBLE)
			{
				showHideFeature(_backgroundImage, false);
			}

			if (AppManager.getInstance().getRefreshPosts())
			{
				if (getActivity() instanceof MainActivity && getMainActivity().getCurrentFragment() instanceof SearchFragment)
				{
					getFragmentManager().popBackStackImmediate(); //popping search fragment
					return;
				}
				AppManager.getInstance().setRefreshPosts(false);
				getMainActivity().setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//				getMainActivity().navigateToPosition(NavigationType.FEED.ordinal());//setting current fragment to news feeds
				getMainActivity().setActivityTitle(getActivity().getString(R.string.feed));
				onRefresh();
			}	
	    }
	}

	
	@Override
	public void onResume()
	{
		super.onResume();
	}

	private int	_savePausedTime	= -1;

	@Override
	public void onPause()
	{
		pauseVideoPlayback();
		super.onPause();
	}
	
	
	public void onTabDeselected(){
		
		super.onTabDeselected();
		
//		pauseVideoPlayback();
		
		destroyVideoView();
		
	}

	@Override
	public void onDestroy()
	{
		_seekBar.setOnSeekBarChangeListener(null);
		_seekBarFull.setOnSeekBarChangeListener(null);
		_videoContainer.setOnClickListener(null);
		_endCardReplayButton.setOnClickListener(null);

		if (_listView != null) {
			_listView.setAdapter(null);
			_listView = null;
		}
		
		if (_list != null) {
			_list.clear();
			_list = null;
		}
		
		_fragmentView  = null;
		_backgroundImage = null;
		_netLogo = null;
		_endCardAsynImage = null;
		_endCardLayout = null;
		_endCardReplayButton = null;
		_videoFullScreenLogo = null;
		_videoFullScreenTop = null;
		
		destroyVideoView();
		super.onDestroy();
	}

	public void destroyVideoView() {
		if (_videoView != null)
		{
			try
			{
				stopVideo();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		
	}

	@Override
	public void openNewFragment(Bundle bundle)
	{
		if (AppManager.getInstance().isLoggedIn() == false)
		{
			AppManager.getInstance().showLogin(getActivity(), MainActivity.REQUEST_CODE_NONE);
			return;
		}

		if (bundle != null && (bundle.getString(Consts.COMMENTS_POST_ID, null) != null))
		{
			stopVideo();
			((MainActivity) getActivity()).replaceFragmentContent(CommentFragment.class, bundle);
					
		}
	}

	protected void setEmptyView(String string)
	{
		if (isVisible() && _fragmentView != null && _listView != null)
		{
			TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
			text.setText(string);
			_listView.setEmptyView(text);
		}
	}

	private AsyncImageView	_backgroundImage;
	private AsyncImageView	_netLogo;
	private TextView		_programTitle;
	private RelativeLayout	_rel;
	private boolean			_tracking;
	private RVSMediaContext	_mediaContex;



	private RVSPost	mCurrentPost;


	private Runnable	mStartRunnable;


	

	/**
	 * 
	 * @param mediaContext
	 * @param reference
	 * @param title
	 * @param remoteUriResource
	 * @param position
	 * @param networkLogo
	 * @param refreshPB
	 * @param endCard
	 * @return true if the post played from the beginning, otherwise false
	 */
	public boolean handleVideoClick(RVSMediaContext mediaContext, final View reference, String title, String remoteUriResource, int position, String networkLogo,
			ProgressBar refreshPB, RVSEndCard endCard, RVSPost post)
	{ 
		mCurrentPost = post;
		
		boolean isFirstTimePlayback = false;
		//Only in portrait mode, when in landscape mode, we'll reach onClick method. 		
		if (_swipeLayout.isRefreshing())
		{ //not setting anything on refreshing mode.
			//This prevents the bug when there's a swipe, and in the middle the user presses on a video. (bug 
			//The outcome will be onScroll won't have the context for the parent top/height, and the _videoContainer won't move right.
			//Hence, we're only showing play sign, since after refresh we're loading the _videoView anyhow.
			//Used when the user presses play, and the list is refreshing.
			//We don't want to set the _videoContainer, since once the refreshing is done, we're not playing a video.
			refreshPB.setVisibility(View.VISIBLE);
		}
		else
		{
			if (_videoView != null && _videoView.isPlaying() && reference.equals(_videoReference))
			{
				_videoView.pause();
				showPlayPauseButton(_videoPlayPauseButton, R.drawable.pause, false, false);
			}
			else if (_videoView != null && _videoView.getState() == State.PAUSED && reference.equals(_videoReference))
			{
				if (_savePausedTime != -1)
				{
					playVideo();
//					_videoView.play();
					_videoView.beginScrubbing();
					_videoView.scrubToValue(_savePausedTime, 0, _seekBar.getMax());
					_videoView.endScrubbing();
					showHideFeature(_backgroundImage, true);
					_savePausedTime = -1;
				}
				else
				{
					playVideo();
//					_videoView.play();
				}
//				showPlayPauseButton(_videoPlayPauseButton, R.drawable.play, true, false);
			}
			else
			{
				if (!reference.equals(_videoReference) || (_videoView != null && _videoView.isPlaying() == false && reference.equals(_videoReference)))
				{
					if (isEndCardVisible(mediaContext)) return false;

					stopVideo();
					_videoReference = reference;

					if (mediaContext != null)
					{
						handleNewImageClick(mediaContext, reference, title, remoteUriResource, position, networkLogo, endCard);
//						_videoView.play();
						playVideo();
						isFirstTimePlayback = true;
					}
					else
					{
						showErrorDialog(null);
						return false;
					}
				}
				else
				{
					playVideo();
				}

//				showPlayPauseButton(_videoPlayPauseButton, R.drawable.play, true, false);
			}
		}
		
		return isFirstTimePlayback;
	}
	
	private void playVideo() {
		
		showPlayPauseButton(_videoPlayPauseButton, R.drawable.play, true, false);
		_videoView.play();
//		_handler.postDelayed(new Runnable()
//		{
//
//			@Override
//			public void run()
//			{
////				_videoView.play();
//			}
//		}, 400);
	}

	private boolean handleNewImageClick(RVSMediaContext mediaContext, final View reference, String title, String remoteUriResource, int position,
			String networkLogo, RVSEndCard endCard)
	{
		if (isEndCardVisible(mediaContext)) return false;

		_mediaContex = mediaContext;

		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);

		_videoView.setLayoutParams(new RelativeLayout.LayoutParams(reference.getWidth(), /*reference.getHeight()*/(int) (reference.getWidth() * 0.5625D)));
		_videoView.setTag(position);

		ViewParent continerParent = (ViewParent) _videoContainer.getParent();
		_rel = (RelativeLayout) reference.getTag(); 
		
//		getResources().getResourceEntryName(_rel.getId())
//		getResources().getResourceEntryName( ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).getId())
		 
		if (continerParent == null)
		{
			((ViewGroup) getView()).addView(_videoContainer);
		} 
//		else if (((ViewGroup) getView()) != continerParent) {
//			((RelativeLayout) _videoContainer.getParent()).removeView(_videoContainer);
//			
//			((ViewGroup) getView()).addView(_videoContainer);
//		}
		
		_videoContainer.getLayoutParams().height = ((View) _rel.getParent()).getHeight();
		int postDescHeight = 0;
		if (((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.post_description) != null)
		{
			postDescHeight = ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.post_description).getHeight();
			if (((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.refer_layout).getVisibility() == View.VISIBLE) {
				postDescHeight += ((ViewGroup) ((ViewGroup) _rel.getParent()).getParent()).findViewById(R.id.refer_layout).getHeight();
			}
		}
		
		_videoContainer.setY(getListItemTop(_rel) + postDescHeight);
		updateVideoContainerPosition(Configuration.ORIENTATION_PORTRAIT);
		
		
		
		
//    	RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//		params.leftMargin = offset; 
//		params.rightMargin = offset;
//		_videoContainer.setLayoutParams(params);
		
//		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//		params.leftMargin = offset; 
//		params.topMargin = offset;
//		_videoContainer.setLayoutParams(params);
		
		

		_videoContainer.bringToFront();
		//rel.addView(_videoContainer);
		//rel.bringToFront();

		_videoView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in)); //prevents screen from "flickering".
		_videoView.setVisibility(View.VISIBLE);
		_videoView.setStateChangedListener(this);

		//		Log.w("hadas", "resetting video media context : " + "duration : " + mediaContext.getDuration() + " start :" + mediaContext.getStart());
		_videoView.setClipWithMediaContext(mediaContext, 0, mediaContext.getDuration());
		_seekBar.bringToFront();
//		_seekBar.setVisibility(View.VISIBLE);
		_seekBar.setMax((int) mediaContext.getDuration() * 100);
		_seekBarFull.setMax((int) mediaContext.getDuration() * 100);

		_videoPlayPauseButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				reference.performClick();
			}
		});

		_videoTitle.setText(title);
		_videoFullScreenName.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_videoFullScreenName.setText(title);

		fixLayout(reference, remoteUriResource, networkLogo, title);

		/* unit test *
		RVSEndCardImp endCard1 = new RVSEndCardImp();
		endCard1.setTuneInInformation("Yahoooo!!!!");
		endCard1.setLinkDescription("Click here");
		endCard1.setLinkUrl("http://www.google.com");
		
		List<RVSImage> images = new ArrayList<RVSImage>();
		RVSImage image = new RVSImage();
		image.setUrl("http://www.foreignpolicy.com/files/fp_uploaded_images/130425_PutinCat1.jpg");
		images.add(image);
			
		endCard1.setImages(images);
		endCard = endCard1;
		/**/

		((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).width = reference.getWidth();
		((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).height = reference.getHeight();
		_endCardReplayText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		//		_endCardLayout.bringToFront();
		//		_endCardLayout.setOnClickListener(new OnClickListener()
		//		{
		//			
		//			@Override
		//			public void onClick(View v)
		//			{
		//				Log.i("a", "aa");
		//				
		//			}
		//		});

		_videoContainer.setVisibility(View.VISIBLE);

		String endCardText = ""; // endCard.getImage();
		if (endCard != null && endCard.getTuneInInformation() != null && endCard.getTuneInInformation().length() > 0)
		{
			endCardText = endCard.getTuneInInformation();
		}
		else
		//fallback
		{
			// todo change text according to channel/program
			endCardText = getResources().getString(R.string.watch) + " " + title;
			//			    todo
			//			    if ([program.show.name length] > 0)
			//			    {
			//			        NSString *format = NSLocalizedString(@"end card text format", nil);
			//			        text = [NSString stringWithFormat:format, program.show.name, channel.name];
			//			    }
			//			    else
			//			    {
			//			        NSString *format = NSLocalizedString(@"end card no program text format", nil);
			//			        text = [NSString stringWithFormat:format, channel.name];
			//			    }
			//			    
		}

		if (endCard != null && endCard.getLinkDescription() != null && endCard.getLinkDescription().length() > 0)
		{
			Spanned linkTxt = Html.fromHtml(endCardText + "<br /><a href=\"" + endCard.getLinkUrl() + "\">" + endCard.getLinkDescription() + "</a>");
			_endCardReplayText.setText(linkTxt);
			_endCardReplayText.setMovementMethod(LinkMovementMethod.getInstance());
			_endCardReplayText.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					if (mCurrentPost != null) {
						AnalyticsManager.sharedManager().trackEndCardLinkClickWithPost(mCurrentPost);
					}
				}
			});
		}
		else
		{
			_endCardReplayText.setOnClickListener(null);
			_endCardReplayText.setText(endCardText);
		}
		
		if (endCard != null && endCard.getImage() != null) {
			
			int heightDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenHeight / 3,  getActivity());
			int widthDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenWidth, getActivity());
			String imageUrl = endCard.getImage().getImageUrlForSize(new Point(widthDp , heightDp));

			_endCardAsynImage.setSampleSize(WhipClipApplication._screenWidth / 3);
			_endCardAsynImage.setRemoteURI(imageUrl);
		}

		return true;
	}

	private void updateVideoContainerPosition(int orientation)
	{
	
		if (orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)_videoContainer.getLayoutParams();
			layoutParams.leftMargin = 0;
			layoutParams.rightMargin = 0;
			_videoContainer.setLayoutParams(layoutParams);
			
//			int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);
//			_videoContainer.setX(0);
//			_videoContainer.getLayoutParams().width = _rel.getLayoutParams().width;
			
		} else {
			
			getView().postDelayed(new Runnable()
			{

				@Override
				public void run()
				{
					int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);

					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)_videoContainer.getLayoutParams();
					layoutParams.leftMargin = offset;
					layoutParams.rightMargin = offset;
					_videoContainer.setLayoutParams(layoutParams);
					
				}
				
				
			},100);
			
			
//			_videoContainer.setX(offset);
//			_videoContainer.setX(offset);
			
			
//			if (_rel != null) {
//				_videoContainer.getLayoutParams().width = _rel.getLayoutParams().width;
//				
//				LayoutParams layoutParams = _seekBar.getLayoutParams();
//				layoutParams.width = _rel.getLayoutParams().width;
//				_seekBar.setLayoutParams(layoutParams);
				
//				_seekBar
//				_seekBar.getLayoutParams().width = _seekBar.getLayoutParams().width - 1;
//				_seekBar.requestLayout();
				
//				_endCardLayout.getLayoutParams().width = _rel.getLayoutParams().width;
//			}
			
		}
	}

	private boolean isEndCardVisible(RVSMediaContext mediaContext)
	{
		if (_mediaContex != null && _mediaContex == mediaContext && _endCardLayout.getVisibility() == View.VISIBLE)
		{
			return true;
		}

		return false;
	}

	@Override
	public void onDidProgressWithCurrentDuration(RVSClipView clipView, int currentTime, long duration)
	{
//		Log.i("AAA", "current time: " + currentTime);
		//		Log.e("hadas", "onDidProgressWithCurrentDuration + " + _videoContainer.getY());
		if (_seekBar != null)
		{
			_seekBar.setProgress(currentTime / 10);
		}
		if (_seekBarFull != null && !_tracking)
		{
			_seekBarFull.setProgress(currentTime / 10);
		}
		if (currentTime > 0)
		{
			if (_currentTime != currentTime) { // hack, implement logic for _videoSpinner visible/gone
				_videoSpinner.setVisibility(View.GONE);
			}
			
			_currentTime = currentTime;
		}
		_duration = duration;
		
	}

	@Override
	public void onClipViewStateDidChange(RVSClipView clipView)
	{
		//		Log.e("hadas", "onClipViewStateDidChange + " + _videoContainer.getY());
		if (clipView == null)
		{
			return;
		}

		if (clipView.getState() == State.PLAYING)
		{
			if (_videoSpinner.getVisibility() == View.VISIBLE)
			{
				_videoSpinner.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_fast));
				_videoSpinner.setVisibility(View.GONE);
				//				Log.e("hadas", "_videoSpinner gone");
			}
			if (_endCardLayout != null && _endCardLayout.getVisibility() == View.VISIBLE)
			{
				_endCardLayout.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
				_endCardLayout.setVisibility(View.GONE);
			}
		}
		else if (_videoView != null && _videoView.getState() == State.PLAYBACK_ENDED)
		{
			//			Log.e("hadas", "onClipViewStateDidChange - end of video.");
			if (_endCardAsynImage.getRemoteURI() != null && !_endCardAsynImage.getRemoteURI().isEmpty()) {
				_endCardAsynImage.loadImage();
			}

			_endCardLayout.setVisibility(View.VISIBLE);
			_endCardLayout.bringToFront();
			
			if (mCurrentPost != null) {
				AnalyticsManager.sharedManager().trackEndCardViewEventWithPost(mCurrentPost);
			}
			// todo 
//				else if () {
//				AnalyticsManager.sharedManager().trackEndCardViewEventWithPost()
//			}

		}
		else if (clipView.getState() == State.FAILED)
		{
			stopVideo();
			RVSPromise<FailReason, Throwable, FailReason> reasonPromise = clipView.getFailReason();
			reasonPromise.then(new AndroidDoneCallback<RVSClipView.FailReason>()
			{
				@Override
				public void onDone(FailReason failReason)
				{
					String failText;
					boolean canRetry = false;
					switch (failReason)
					{
						case INTERNAL_ERROR:
							failText = getResources().getString(R.string.playback_error_internal);
							canRetry = true;
							break;

						case REASON_RESTRICTED:
							failText = getResources().getString(R.string.playback_error_restricted);
							break;

						case SUPPERESSED_CHANNEL:
							failText = getResources().getString(R.string.playback_error_suppressed_channel);
							break;

						case SUPPERESED_PART_OF_SHOW:
							failText = getResources().getString(R.string.playback_error_suppressed_part_of_show);
							break;

						case SUPPERESSED_ENTIRE_SHOW:
							failText = getResources().getString(R.string.playback_error_suppressed_entire_show);
							break;

						case UNKNOWN:
							failText = getResources().getString(R.string.playback_error_other);
							break;

						default:
							failText = failReason.name();
					}

					showErrorDialog(failText);
				}

				@Override
				public AndroidExecutionScope getExecutionScope()
				{
					return AndroidExecutionScope.UI;
				}

			}).fail(new AndroidFailCallback<Throwable>()
			{
				@Override
				public void onFail(Throwable error)
				{
					showErrorDialog(error.getMessage());
				}

				@Override
				public AndroidExecutionScope getExecutionScope()
				{
					return AndroidExecutionScope.UI;
				}
			});

		}
	}

	@Override
	public void onBufferingStateChanged(RVSClipView clipView, final boolean isBuffering)
	{
		//		Log.e("hadas", "isBuffering=" + isBuffering);
		//	final boolean isBuff = isBuffering;

		_handler.post(new Runnable()
		{
			@Override
			public void run()
			{
				if (isVisible() == true)
				{
					if (isBuffering == false) //playing
					{
						bufferringFalse();
					}
					else
					{
						bufferringTrue();
					}
				}
			}
		});
	}

	private void bufferringFalse()
	{
		
		_handler.removeCallbacks(mStartRunnable);
		
		Log.e("hadas", "bufferringFalse");
		if (_backgroundImage != null && _backgroundImage.getVisibility() == View.VISIBLE && _videoView != null && _videoView.isPlaying())
		{
			_backgroundImage.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_fast)); //prevents screen from "flickering".
			_backgroundImage.setVisibility(View.GONE);
			//			Log.e("hadas", "_backgroundImage gone");
		}

		if (_videoReference != null && _videoView.getLayoutParams().width == _videoReference.getWidth())
		{
			_seekBar.bringToFront();
			_seekBar.setVisibility(View.VISIBLE);
		}

		if (_videoSpinner.getVisibility() == View.VISIBLE)
		{
			_videoSpinner.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_fast));
			_videoSpinner.setVisibility(View.GONE);
		}

		if (_videoSpinnerFull.getVisibility() == View.VISIBLE)
		{
			_videoSpinnerFull.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_fast));
			_videoSpinnerFull.setVisibility(View.GONE);
		}
	}

	private void bufferringTrue()
	{
		
		if (mStartRunnable == null) {
			mStartRunnable = new Runnable()
			{
	
				@Override
				public void run()
				{
					((ViewGroup) _videoContainer.getParent()).bringToFront();
					_videoView.setVisibility(View.VISIBLE);
	
					//		if (_backgroundImage != null && _backgroundImage.getVisibility() != View.VISIBLE)
					//		{
					//			_backgroundImage.setVisibility(View.VISIBLE);
					//			Log.e("hadas", "_backgroundImage VISIBLE");
					//		}
	
					if (_videoView.getState() != State.PAUSED) //prevents the case where user presses play then immediately pause. 
					{
						_videoSpinner.bringToFront();
						_videoSpinner.setVisibility(View.VISIBLE);
					}
					else
					{
						stopVideo();
					}
				}
			};
		}
		
		_handler.postDelayed(mStartRunnable, 400);
		
		
		
//		((ViewGroup) _videoContainer.getParent()).bringToFront();
//		_videoView.setVisibility(View.VISIBLE);
//
//		//		if (_backgroundImage != null && _backgroundImage.getVisibility() != View.VISIBLE)
//		//		{
//		//			_backgroundImage.setVisibility(View.VISIBLE);
//		//			Log.e("hadas", "_backgroundImage VISIBLE");
//		//		}
//
//		if (_videoView.getState() != State.PAUSED) //prevents the case where user presses play then immediately pause. 
//		{
//			_videoSpinner.bringToFront();
//			_videoSpinner.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			stopVideo();
//		}

	}

	private void showErrorDialog(String message)
	{
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
					case DialogInterface.BUTTON_POSITIVE:
						dialog.dismiss();
						break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		String dialogMessage = message;
		if (dialogMessage == null)
		{
			dialogMessage = getResources().getString(R.string.content_not_available);
		}
		builder.setMessage(dialogMessage).setPositiveButton("Ok", dialogClickListener);
		//		if (title != null)
		//			builder.setTitle(getResources().getString(R.string.sorry));
		builder.show();
	}

	private void showPlayPauseButton(final Button button, int id, boolean play, boolean fullScreen)
	{
		if (id != -1)
		{
			button.setBackgroundResource(id);
		}

//		button.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
//		button.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.play_btn));
		Animation loadAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.play_btn);
		loadAnimation.setAnimationListener(new AnimationListener()
		{
			
			@Override
			public void onAnimationStart(Animation animation) {}
			
			@Override
			public void onAnimationRepeat(Animation animation) {}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				button.setVisibility(View.GONE);
			}
		});
		button.startAnimation(loadAnimation);
		button.setVisibility(View.VISIBLE);
		button.bringToFront();

//		_handler.postDelayed(new Runnable()
//		{
//
//			@Override
//			public void run()
//			{
////				button.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
////				button.setVisibility(View.INVISIBLE);
//			}
//		}, 400);

		if (fullScreen == false)
		{
			showHideFeature(_netLogo, play);
			showHideFeature(_programTitle, play);
		}
	}

	private void showHideFeature(View v, boolean play)
	{
		if (v == null)
		{
			return;
		}

		if (play)
		{
			v.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
			v.setVisibility(View.GONE);
		}
		else
		{
			v.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
			v.setVisibility(View.VISIBLE);
			v.bringToFront();
		}
	}

	private void fixLayout(View reference, String remoteUriResource, String networkLogo, String title)
	{
		int[] loc = new int[2];
		reference.getLocationOnScreen(loc);

		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) _videoSpinner.getLayoutParams();
		layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		_videoSpinner.setLayoutParams(layoutParams);
		_videoSpinner.requestLayout();

		//	((RelativeLayout.LayoutParams) _seekBar.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM, _videoView.getId());
		//	_seekBar.requestLayout();

		layoutParams = (RelativeLayout.LayoutParams) _videoPlayPauseButton.getLayoutParams();
		layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		_videoPlayPauseButton.setLayoutParams(layoutParams);
		_videoPlayPauseButton.requestLayout();

		_backgroundImage = new AsyncImageView(getActivity(), null);
		_backgroundImage.setSampleSize(WhipClipApplication._screenWidth / 4);
		_backgroundImage.setRemoteURI(remoteUriResource);
		_backgroundImage.loadImage();
		//		Log.i("hadas", "reference.getWidth() = " + reference.getWidth() + "   reference.getHeight() = " + reference.getHeight());
		_backgroundImage.setLayoutParams(new RelativeLayout.LayoutParams(reference.getWidth(), (int) (reference.getWidth() * 0.5625D))); 
		((RelativeLayout.LayoutParams) _backgroundImage.getLayoutParams()).topMargin = ((RelativeLayout.LayoutParams) reference.getLayoutParams()).topMargin;
		_backgroundImage.requestLayout();

		if (networkLogo != null)
		{
			_netLogo = new AsyncImageView(getActivity(), null);
			_netLogo.setRemoteURI(networkLogo);
			_netLogo.setSampleSize(UiUtils.getDipMargin(20));
			_netLogo.loadImage();
			_netLogo.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			((RelativeLayout.LayoutParams) _netLogo.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			((RelativeLayout.LayoutParams) _netLogo.getLayoutParams()).rightMargin = AppManager.getInstance().convertDpToPixel(10, getActivity());
			((RelativeLayout.LayoutParams) _netLogo.getLayoutParams()).topMargin = AppManager.getInstance().convertDpToPixel(10, getActivity());
			_netLogo.requestLayout();

			_videoFullScreenLogo.setRemoteURI(networkLogo);
			_videoFullScreenLogo.setSampleSize(UiUtils.getDipMargin(20));
			_videoFullScreenLogo.loadImage();

			_videoContainer.addView(_netLogo);
		}

		_programTitle = new TextView(getActivity(), null);
		_programTitle.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		((RelativeLayout.LayoutParams) _programTitle.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		int padding = AppManager.getInstance().convertDpToPixel(10, getActivity());
		_programTitle.setPadding(padding, padding, padding, padding);
		_programTitle.setShadowLayer(1, -1, -1, getResources().getColor(R.color.black_semi_transparent));
		_programTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_programTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		_programTitle.setTextColor(getResources().getColor(android.R.color.white));
		_programTitle.setText(title);
		_programTitle.setMaxLines(1);
		_programTitle.requestLayout();

		_videoContainer.addView(_programTitle);
		_videoContainer.addView(_backgroundImage);

	}

//	protected void addRemoveSpinner(final boolean isBuffering)
//	{
//		if (_videoSpinner == null)
//		{
//			return;
//		}
//
//		_handler.post(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				if (isVisible() == true)
//				{
//					if (isBuffering == true)
//					{
//						_videoSpinner.bringToFront();
//						_videoSpinner.setVisibility(View.VISIBLE);
//					}
//					else
//					{
//						_videoSpinner.setVisibility(View.GONE);
//						_videoView.setVisibility(View.VISIBLE);
//					}
//				}
//			}
//		});
//	}

	public void stopVideo()
	{
		if (getActivity() != null) {
			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		if (_videoView != null)
		{
			_tracking = false;
			if (_videoSpinner != null) {
				_videoSpinner.setVisibility(View.GONE);
			}
			
			if (_seekBar != null) {
				_seekBar.setVisibility(View.GONE);
			}
			
			_videoView.setStateChangedListener(null);
			//			Log.i("hadas", "stopping video");
			_videoView.stop();

			if (_videoContainer != null) _videoContainer.setVisibility(View.GONE);

			_videoView.setTag(null);

			if (_netLogo != null)
			{
				if (_videoContainer != null) {
					_videoContainer.removeView(_netLogo);
				}
				_netLogo = null;
			}

			if (_backgroundImage != null)
			{
				if (_videoContainer != null) {
					_videoContainer.removeView(_backgroundImage);
				}
				_backgroundImage = null;
			}

			if (_programTitle != null)
			{
				if (_videoContainer != null) {
					_videoContainer.removeView(_programTitle);
				}
				_programTitle = null;
			}
			_videoReference = null;
			if (_endCardLayout != null) {
				_endCardLayout.setVisibility(View.GONE);
			}
			if (_postsAdapter != null) {
				_postsAdapter.notifyDataSetChanged();
			}
		}
	}

	/****************************************
	 * 				Seekbar methods			*
	 ****************************************/
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean arg2)
	{
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
		_tracking = true;
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
		_tracking = false;
		if (_videoView != null /*&& _videoView.isPlaying()*/)
		{
			_videoView.beginScrubbing();
			_videoView.scrubToValue(seekBar.getProgress(), 0, _seekBar.getMax());
			_videoView.endScrubbing();
		}
	}

	@Override
	public void onOffset(int offset)
	{
		if (offset != 0)
		{
			if (_videoContainer != null)
			{
				_videoContainer.setY(_videoContainer.getY() + offset);
			}
		}

	}

	private boolean	_wasPlaying	= false;
	private int		_oldVidContHeight;
	private int		_oldVidContWidth;
	private int		_oldVidViewHeight;
	private int		_oldVidViewWidth;
	private float	_oldVidContY;

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			getMainActivity().setFullScreen();
			getMainActivity().setPagerSwipeEnabled(false);
			
			_listView.setVisibility(View.INVISIBLE);
			//resetValues();
			_oldVidContHeight = _videoContainer.getHeight();
			_oldVidContWidth = _videoContainer.getWidth();
			_oldVidViewHeight = _videoView.getHeight();
			_oldVidViewWidth = _videoView.getWidth();
			_oldVidContY = _videoContainer.getY();
//			((MarginLayoutParams)_videoContainer.getLayoutParams()).leftMargin = 0;
//			((MarginLayoutParams)_videoContainer.getLayoutParams()).rightMargin = 0;

//			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//			params.leftMargin = 0; 
//			params.topMargin = 0;
//			_videoContainer.setLayoutParams(params);
			
			
			_videoContainer.getLayoutParams().width = WhipClipApplication._screenHeight + getActionBar().getHeight();
			_videoContainer.getLayoutParams().height = WhipClipApplication._screenWidth;
			_videoView.getLayoutParams().width = WhipClipApplication._screenHeight + getActionBar().getHeight();
			_videoView.getLayoutParams().height = WhipClipApplication._screenWidth;
			_videoContainer.setX(0); 
			_videoContainer.setY(0);
			

			
			
			
			_videoContainer.bringToFront();
			
		
//	    	int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);
//	    	RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//			params.leftMargin = offset; 
//			params.rightMargin = offset;
//			_videoContainer.setLayoutParams(params);

//			int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);
////			_videoContainer.setX(offset);
//			
//			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//			params.leftMargin = offset; 
//			params.topMargin = offset;
//			_videoContainer.setLayoutParams(params);

			((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).width = WhipClipApplication._screenHeight + getActionBar().getHeight();
			((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).height = WhipClipApplication._screenWidth;

			if (_videoView.getState() == State.PAUSED)
			{
				showHideFeature(_programTitle, true);
				showHideFeature(_netLogo, true);
			}
			showHideFeature(_seekBar, true);

			setClickListeners();
		}
		else
		{
			
			getMainActivity().setPagerSwipeEnabled(true);
			getMainActivity().switchOffFullScreen();
			if (isVisible())
			{
				_listView.setVisibility(View.VISIBLE);
				updateVideoContainerPosition(newConfig.orientation);
				_videoContainer.getLayoutParams().height = _oldVidContHeight;
				_videoContainer.getLayoutParams().width = _oldVidContWidth;
				_videoView.getLayoutParams().height = _oldVidViewHeight;
				_videoView.getLayoutParams().width = _oldVidViewWidth;
				_videoContainer.setY(_oldVidContY);
				
				
//				_videoContainer.postDelayed(new Runnable()
//				{
//				    @Override
//				    public void run()
//				    {	
//				    	int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);
//				    	RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//						params.leftMargin = offset; 
//						params.topMargin = offset;
//						_videoContainer.setLayoutParams(params);
//
//				    }
//				}, 50);
//				
//				int offset = (int) getResources().getDimensionPixelSize(R.dimen.post_list_item_margin_left_right);
////				_videoContainer.setX(offset);
//				
//				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _videoContainer.getLayoutParams();
//				params.leftMargin = offset; 
//				params.topMargin = offset;
//				_videoContainer.setLayoutParams(params);
				
				

				((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).width = _oldVidViewWidth;
				((RelativeLayout.LayoutParams) _endCardLayout.getLayoutParams()).height = _oldVidViewHeight;
				_videoFullScreenBottom.setVisibility(View.GONE);
				_videoFullScreenTop.setVisibility(View.GONE);

				if (_videoView.getState() == State.PAUSED)
				{
					showHideFeature(_programTitle, false);
					showHideFeature(_netLogo, false);
				} else if (_videoView.getState() == State.PLAYING)
				{
					showHideFeature(_seekBar, false);
				}
				
				//				_seekBar.bringToFront();

				//				if (_videoView.isPlaying())
				//				{
				//					_videoView.pause();
				//					_wasPlaying = true;
				//				}
				//				_videoSpinnerFull.setVisibility(View.GONE);
				//				_videoFullScreenBottom.setVisibility(View.GONE);
				//				_videoFullScreenTop.setVisibility(View.GONE);
				//				final int[] loc = new int[2];
				//				_videoReference.getLocationOnScreen(loc);
				//
				//				_controlsContainerFullScreen.setOnClickListener(null);
				//				_controlsContainerFullScreen.removeView(_videoView);
				//
				//				_handler.postDelayed(new Runnable()
				//				{
				//					@Override
				//					public void run()
				//					{
				//						_videoContainer.addView(_videoView);
				//
				//						_videoView.setVisibility(View.INVISIBLE);
				//						((RelativeLayout.LayoutParams) _videoView.getLayoutParams()).topMargin = ((RelativeLayout.LayoutParams) _videoReference
				//								.getLayoutParams()).topMargin;
				//						_videoView.getLayoutParams().width = _videoReference.getWidth();
				//						_videoView.getLayoutParams().height = _videoReference.getHeight();
				//						_videoView.requestLayout();
				//						_videoView.bringToFront();
				//						_videoView.setVisibility(View.VISIBLE);
				//
				//						_controlsContainerFullScreen.setVisibility(View.GONE);
				//
				//						if (_wasPlaying)
				//						{
				//							_videoView.play();
				//							_videoView.beginScrubbing();
				//							_videoView.scrubToValue(_currentTime / 1000, 0, _duration);
				//							_videoView.endScrubbing();
				//							_wasPlaying = false;
				//
				//							_videoSpinner.bringToFront();
				//							_videoSpinner.setVisibility(View.VISIBLE);
				//						}
				//
				//						_videoTitle.setVisibility(View.GONE);
				//
				//						_seekBar.setVisibility(View.VISIBLE);
				//						((RelativeLayout.LayoutParams) _seekBar.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				//						_seekBar.requestLayout();
				//						_seekBar.bringToFront();
				//
				//						((RelativeLayout.LayoutParams) _videoPlayPauseButton.getLayoutParams()).topMargin = (int) (loc[1]
				//								- getMainActivity().getActionBarHeight() + (_videoReference.getHeight() / 2.0) - UiUtils.getDipMargin(10));
				//						((RelativeLayout.LayoutParams) _videoPlayPauseButton.getLayoutParams()).leftMargin = (int) (loc[0] + (_videoReference.getWidth() / 2.0) - UiUtils
				//								.getDipMargin(10));
				//						_videoPlayPauseButton.requestLayout();
				//
				//					}
				//				}, 500);
			}

			//			getMainActivity().switchOffFullScreen();
			//			_handler.postDelayed(new Runnable()
			//			{
			//				@Override
			//				public void run()
			//				{
			//					if (isVisible())
			//					{
			//						int[] loc = new int[2];
			//						_videoReference.getLocationOnScreen(loc);
			//
			//						((RelativeLayout.LayoutParams) _videoView.getLayoutParams()).topMargin = loc[1] - getMainActivity().getActionBarHeight();
			//						_videoView.getLayoutParams().width = _videoReference.getWidth();
			//						_videoView.getLayoutParams().height = _videoReference.getHeight();
			//						_videoView.requestLayout();
			//						_controlsContainerFullScreen.setVisibility(View.GONE);
			//						_videoTitle.setVisibility(View.GONE);
			//						_seekBar.setVisibility(View.VISIBLE);
			//
			//						((RelativeLayout.LayoutParams) _videoPlayPauseButton.getLayoutParams()).topMargin = (int) (loc[1]
			//								- getMainActivity().getActionBarHeight() + (_videoReference.getHeight() / 2.0) - UiUtils.getDipMargin(10));
			//						((RelativeLayout.LayoutParams) _videoPlayPauseButton.getLayoutParams()).leftMargin = (int) (loc[0] + (_videoReference.getWidth() / 2.0) - UiUtils
			//								.getDipMargin(10));
			//						_videoPlayPauseButton.requestLayout();
			//					}
			//				}
			//			}, 200);

		}
	}

//	private void resetValues()
//	{
//
//		_videoView.setLayoutParams(new RelativeLayout.LayoutParams(WhipClipApplication._screenHeight + getActionBar().getHeight(),
//				WhipClipApplication._screenWidth));
//		_videoView.requestLayout();
//		_videoView.bringToFront();
//		_videoView.setVisibility(View.INVISIBLE);
//		_videoView.setVisibility(View.VISIBLE);
//
//		_endCardLayout.setVisibility(View.GONE);
//		_seekBar.setVisibility(View.GONE);
//
//		((RelativeLayout.LayoutParams) _videoPlayPauseButtonFull.getLayoutParams()).topMargin = (int) (WhipClipApplication._screenWidth / 2.0);
//		((RelativeLayout.LayoutParams) _videoPlayPauseButtonFull.getLayoutParams()).leftMargin = (int) (WhipClipApplication._screenHeight / 2.0);
//		_videoPlayPauseButtonFull.setVisibility(View.INVISIBLE);
//		_videoPlayPauseButtonFull.setVisibility(View.VISIBLE);
//		_videoPlayPauseButtonFull.requestLayout();
//		_videoPlayPauseButtonFull.bringToFront();
//
//		_videoPlayPauseButtonFull.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
//		_videoPlayPauseButtonFull.setVisibility(View.GONE);
//
//		if (_videoView.isPlaying())
//		{
//			_videoView.beginScrubbing();
//			_videoView.scrubToValue(_currentTime / 1000, 0, _duration);
//			_videoView.endScrubbing();
//
//			_videoView.setVisibility(View.VISIBLE);
//
//			((RelativeLayout.LayoutParams) _videoSpinnerFull.getLayoutParams()).topMargin = (int) (WhipClipApplication._screenWidth / 2.0);
//			((RelativeLayout.LayoutParams) _videoSpinnerFull.getLayoutParams()).leftMargin = (int) (WhipClipApplication._screenHeight / 2.0);
//			_videoSpinnerFull.bringToFront();
//			_videoSpinnerFull.setVisibility(View.VISIBLE);
//		}
//	}

	private void setClickListeners()
	{
		_playPauseSeekFull.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (_videoView.isPlaying())
				{
					showPlayPauseButton(_videoPlayPauseButtonFull, R.drawable.pause, false, true);
					_playPauseSeekFull.setBackgroundResource(R.drawable.play);
					_videoView.pause();
					showPannel(true);
				}
				else if (_videoView.getState() == State.PAUSED || _videoView.getState() == State.PLAYBACK_ENDED)
				{
//					showPlayPauseButton(_videoPlayPauseButtonFull, R.drawable.play, false, true);
					_playPauseSeekFull.setBackgroundResource(R.drawable.pause);
					playVideo();
//					_videoView.play();
					showPannel(false);
				}
			}
		});
	}

	private void showPannel(boolean showConstantly)
	{
		if (_videoFullScreenBottom.getVisibility() != View.VISIBLE)
		{
			_videoFullScreenBottom.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
			_videoFullScreenBottom.setVisibility(View.VISIBLE);
			_videoFullScreenBottom.requestLayout();
			_videoFullScreenBottom.bringToFront();
			_videoFullScreenTop.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
			_videoFullScreenTop.setVisibility(View.VISIBLE);
			_videoFullScreenTop.requestLayout();
			_videoFullScreenTop.bringToFront();
		}

		if (showConstantly == false)
		{
			_playPauseSeekFull.setBackgroundResource(R.drawable.pause);
			_handler.postDelayed(new Runnable()
			{

				@Override
				public void run()
				{
					if (_videoFullScreenBottom.getVisibility() == View.VISIBLE && !_tracking)
					{
						_videoFullScreenBottom.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
						_videoFullScreenBottom.setVisibility(View.GONE);
						_videoFullScreenTop.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
						_videoFullScreenTop.setVisibility(View.GONE);
					}
				}
			}, 3000); //the pannel hides after x seconds. This is NOT a glitch ! ;) 
		}
		else
		{
			_playPauseSeekFull.setBackgroundResource(R.drawable.play);
		}

	}

	@Override
	public void onClick(View v)
	{
		if (v == _videoContainer)
		{
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				if (isEndCardVisible(_mediaContex)) return;
				
				handleVideoClipOrientationLandscape();
			}
			else
			{
				_videoReference.performClick();
			}
		}
		else if (v == _endCardReplayButton)
		{
			_endCardLayout.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
			_endCardLayout.setVisibility(View.GONE);

			_videoView.play();
			
			if (mCurrentPost != null) {
				AnalyticsManager.sharedManager().trackClipPlaybackEventWithPost(mCurrentPost, true, mCurrentTabPosition);
			} else {
				
			}
			
		}
	}

	private void handleVideoClipOrientationLandscape()
	{
		if (_videoView != null && _videoView.isPlaying())
		{
			_videoView.pause();
			showPannel(true);
			showPlayPauseButton(_videoPlayPauseButton, R.drawable.pause, false, true);
		}
		else
		{
			playVideo();
//			_videoView.play();
			showPannel(false);
//			showPlayPauseButton(_videoPlayPauseButton, R.drawable.play, true, true);
		}
	}

	public RVSClipView  getVideoView()
	{
		return _videoView;
	}
	
	public long getCurrentPlaybackTime() {
		return _currentTime;
	}
	
	@Override
	public void onTabReselected()
	{
		super.onTabReselected();
		
		if (_listView != null) {
			_listView.smoothScrollToPosition(0);
		}
	}
	
	public void pauseVideoPlayback() {
		
		if (_videoView != null && _videoView.isPlaying() == true)
		{
			if (_seekBar.getVisibility() == View.VISIBLE)
			{
				_savePausedTime = _seekBar.getProgress();
			}
			else
			{
				_savePausedTime = _seekBarFull.getProgress();
			}
			_videoView.pause();
		}
	}
	
	public void openUserProfileAsChild(String userId, final boolean cameFromSideMenu) {
		
		boolean success = ((MainActivity) getActivity()).openUserProfile(userId, cameFromSideMenu);
		
		if (success) {
			stopVideo();
		}
		
	}
	
	public void replaceTabFragmentContent(Class<? extends BaseFragment> fragmentClass, Bundle bundle) {
		
		((MainActivity) getActivity()).replaceFragmentContent(fragmentClass, bundle);
		stopVideo();
		
	}
	
//	public void replaceTabFragmentContent(Class<? extends BaseFragment> fragmentClass, Bundle bundle) {
//		
//		((MainActivity) getActivity()).replaceTabFragmentContent(getCurrentTabPosition(), fragmentClass, bundle);
//		stopVideo();
//		
//	}
}