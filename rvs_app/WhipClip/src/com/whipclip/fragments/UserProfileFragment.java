package com.whipclip.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.PostsListAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.views.AsyncImageView;

/**
 * A placeholder fragment containing a simple view.
 */
public class UserProfileFragment extends BasePostsFragment<List<RVSPost>> implements OnClickListener
{
	private RVSUser			_user;
	private View			_userProfileLayout;
	private RelativeLayout	_followersLayout;
	private RelativeLayout	_followingLayout;
	private RelativeLayout	_likesLayout;
	private RelativeLayout	_followLayout;
	private boolean			_isMe;
	private boolean			_isFollowing;
 
	@SuppressWarnings("unchecked")
	protected void getPosts(int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
			}
			if (_dataAsync == null && _user != null)
			{
				_dataAsync = AppManager.getInstance().getSDK().userFeedForUser(_user.getUserId());//AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID));// trendingPosts();
			}
			if (_promise != null && _promise.isPending())
			{
				return;
			}
			try
			{
				if (_dataAsync.getDidReachEnd() == false)
				{
					_promise = _dataAsync.next(number);
				}
			}
			catch (Throwable e)
			{
				_swipeLayout.setRefreshing(false);
				e.printStackTrace();
				return;
			}
			if (_promise != null)
			{
				_promise.then(this, this);
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	protected void loadMorePostResults(int loadMorePostResults)
	{
		if (_dataAsync != null && !_dataAsync.getDidReachEnd())
		{
			_progressBar.setVisibility(View.VISIBLE);
			getPosts(10);
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		if (activity instanceof MainActivity) {
			((MainActivity) activity).onSectionAttached(_isMe ? 14 : 12);
		}
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
		_list = null;

		_dataAsync = null;
		_postsAdapter = null;
		getPosts(12);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	getActivity().invalidateOptionsMenu();
			((MainActivity) getActivity()).onSectionAttached(_isMe ? 14 : 12);
			AppManager.getInstance().sendToGA(getString(R.string.profile));

			if (_list != null && !_list.isEmpty() && !_loadingMore)
			{
				RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
				if (relatedComments != null && relatedComments.getRelatedPostId() != null)
				{
					_postsAdapter.updateElementInRealtimePost();
				}
				_postsAdapter.updateCommentSelected();
				_postsAdapter.notifyDataSetChanged();
			}

			if (_isMe && AppManager.getInstance().getUserUpdatedInfo() != -1)
			{
				((TextView) _userProfileLayout.findViewById(R.id.following_number)).setText(AppManager.getInstance().getUserUpdatedInfo() + "");
				AppManager.getInstance().resetUserUpdatedInfo();
			}
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
//		getActivity().invalidateOptionsMenu();
//		((MainActivity) getActivity()).onSectionAttached(_isMe ? 14 : 12);
//		AppManager.getInstance().sendToGA(getString(R.string.profile));
//
//		if (_list != null && !_list.isEmpty() && !_loadingMore)
//		{
//			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
//			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
//			{
//				_postsAdapter.updateElementInRealtimePost();
//			}
//			_postsAdapter.updateCommentSelected();
//			_postsAdapter.notifyDataSetChanged();
//		}
//
//		if (_isMe && AppManager.getInstance().getUserUpdatedInfo() != -1)
//		{
//			((TextView) _userProfileLayout.findViewById(R.id.following_number)).setText(AppManager.getInstance().getUserUpdatedInfo() + "");
//			AppManager.getInstance().resetUserUpdatedInfo();
//		}
	}

	@Override
	public void initViews(View FfragmentView)
	{
		_swipeLayout.setRefreshing(true);

		_userProfileLayout = LayoutInflater.from(getActivity()).inflate(R.layout.user_profile_list_header, null);

		_followersLayout = (RelativeLayout) _userProfileLayout.findViewById(R.id.followers_layout);
		_followingLayout = (RelativeLayout) _userProfileLayout.findViewById(R.id.following_layout);
		_followLayout = (RelativeLayout) _userProfileLayout.findViewById(R.id.follow_layout);
		_likesLayout = (RelativeLayout) _userProfileLayout.findViewById(R.id.likes_layout);

		//When opening a profile fragment, we might not have the user details yet.
		//if callProfileFragmentBefore = true, we have the user information onCreate, so we're calling
		//initViewsAfterLoadingResults. when we open a profile of mine, or in other cases, we might not have this info yet.
		if (AppManager.getInstance().getCallProfileFragmentBefore())
		{
			initViewsAfterLoadingResults();
			AppManager.getInstance().setCallProfileFragmentBefore(false);
		}
	}

	public void initViewsAfterLoadingResults()
	{
		if (_swipeLayout != null) {
			_swipeLayout.setRefreshing(false);
		}
		
		_user = AppManager.getInstance().getSDKUser();
		if (_user == null) return;
		
		_isMe = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(_user.getUserId());
		if (getMainActivity() != null) {
			getMainActivity().updateActionTitle();
		// TODO workaround, should be null
		}
		
//		MainActivity activity = (MainActivity) getActivity();
//		if (activity != null) {
//			if (_isMe == false)
//			{
//				activity.setActivityTitle(getActivity().getString(R.string.profile));
//				activity.setTitle();
//			}
//			else
//			{
//				((MainActivity) getActivity()).setActivityTitle(getActivity().getString(R.string.my_profile));
//				activity.setTitle();
//			}
//		}
		initHeaderLayout();
		getPosts(12);
	}
	
	public void initWithMyUser()
	{
		_swipeLayout.setRefreshing(false);
		String myUserId = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);

		RVSUser user = AppManager.getInstance().getSDKUser();
		
		// TODO workaround, should be null
		if (_user != null &&  myUserId.equals(_user.getUserId()))
			return;
		
		
		// Always get updated user profile
		RVSPromise<RVSUser, Error, Integer> prom = AppManager.getInstance().getSDK().userForUserId(myUserId);
		prom.then(new DoneCallback<RVSUser>() {
			
			@Override
			public void onDone(final RVSUser result)
			{
			
				getActivity().runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						if (result != null)
						{
							_user = result;
							((MainActivity) getActivity()).setActivityTitle(getActivity().getString(R.string.my_profile));
							getMainActivity().setTitle();
							initHeaderLayout();
							getPosts(12);
						}
					}					
				});
			}
		});
		
	}


	private void initHeaderLayout()
	{
		((TextView) _userProfileLayout.findViewById(R.id.user_name)).setText(_user.getName());
		((TextView) _userProfileLayout.findViewById(R.id.user_handle)).setText(_user.getDisplayHandle());
		try
		{
			((TextView) _userProfileLayout.findViewById(R.id.followers_number)).setText(_user.getFollowerCount() + "");
			((TextView) _userProfileLayout.findViewById(R.id.following_number)).setText(_user.getFollowingCount() + "");
			((TextView) _userProfileLayout.findViewById(R.id.likes_number)).setText(_user.getLikeCount() + "");
			
			
			int totalPosts = _user.getPostCount()  + _user.getRepostCount();
			if (totalPosts < 2) {
				((TextView) _userProfileLayout.findViewById(R.id.whip_count)).setText(totalPosts + " " + getString(R.string.clip));
			} else {
				((TextView) _userProfileLayout.findViewById(R.id.whip_count)).setText(totalPosts + " " + getString(R.string.clips));
			}
			((TextView) _userProfileLayout.findViewById(R.id.followers_number)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.following_number)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.likes_number)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.whip_count)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.followers_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.following_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.likes_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.user_name)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView) _userProfileLayout.findViewById(R.id.user_handle)).setTypeface(AppManager.getInstance().getTypeFaceLight());
			((TextView) _userProfileLayout.findViewById(R.id.follow_title)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		//		((TextView) _userProfileLayout.findViewById(R.id.mentions_number)).setText(_user.getRepostCount() + "");

		AsyncImageView profileImage = ((AsyncImageView) _userProfileLayout.findViewById(R.id.user_image));

		doAvatar(profileImage);

		_isFollowing = _user.isFollowedByMe();

		if (_isMe)
		{
			_userProfileLayout.findViewById(R.id.row_1).setVisibility(View.GONE);
		}
		else if (_isFollowing)
		{
			((TextView) _userProfileLayout.findViewById(R.id.follow_title)).setText(getString(R.string.following));
			((ImageView) _userProfileLayout.findViewById(R.id.follow_image)).setImageResource(R.drawable.followed_user);
		}

		_postsAdapter = new PostsListAdapter(getActivity(), _list, this);
		_listView.addHeaderView(_userProfileLayout);
		_listView.setAdapter(_postsAdapter);
	}

	private void doAvatar(final AsyncImageView profileImage)
	{
		final String targetPath = WhipClipApplication._context.getCacheDir() + "/images/" + _user.getUserId().hashCode() + ".png";
		if (new File(targetPath).exists())
		{
			Bitmap bmp = getImage(targetPath);
			profileImage.setImageBitmap(UiUtils.getRoundedCornerBitmap(bmp));
			return;
		}
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				String path = _user.getPofileImage().getImageUrlForSize(new Point(40, 40));
				if (isVisible())
				{
					if (path != null)
					{
						AppManager.getInstance().getFilesManager().downloadFile(path, targetPath);

						try
						{
							getActivity().runOnUiThread(new Runnable()
							{
								@Override
								public void run()
								{
									if (isVisible())
									{
										Bitmap bmp = getImage(targetPath);

										profileImage.setImageBitmap(UiUtils.getRoundedCornerBitmap(bmp));
									}
								}

							});
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								profileImage.setDefualImageRes(R.drawable.default_profile);
								profileImage.setSampleSize(UiUtils.getDipMargin(40));
								profileImage.setRoundedCorner(true);
								profileImage.loadImage();
							}
						});
					}
				}
			}
		}).start();
	}

	@Override
	public void initListeners()
	{
		_followersLayout.setOnClickListener(this);
		_followingLayout.setOnClickListener(this);
		_followLayout.setOnClickListener(this);
		_likesLayout.setOnClickListener(this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.feeds, menu);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home: 
            getMainActivity().onBackPressed();
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    } 

	@Override
	public void onDone(List<RVSPost> arg0)
	{
		Log.e(getClass().getName(), "onDone UserProfileFragment " + arg0);
		_swipeLayout.setRefreshing(false);
		if (!isVisible())
		{
			return;
		}
		_progressBar.setVisibility(View.GONE);
		dismissProgressDialog();
		Log.i(getClass().getSimpleName(), "onDone: " + arg0.size());
		if (arg0 != null)
		{
			convertToUIItems(arg0);

			if (_postsAdapter == null)
			{
				_postsAdapter = new PostsListAdapter(UserProfileFragment.this.getActivity(), _list, this);
				_listView.setAdapter(_postsAdapter);
			}
			else
			{
				_postsAdapter.insertElementsToRealtimePost(_list);
				_postsAdapter.notifyDataSetChanged();
			}
		}
		_loadingMore = false;
	}

	@Override
	public void onDestroy()
	{
		if (_promise != null)
		{
			_promise.cancel();
		}
		super.onDestroy();
	}

	@Override
	public void onFail(Throwable arg0)
	{
		if (_swipeLayout != null) {
			_swipeLayout.setRefreshing(false);
		}
		
		_progressBar.setVisibility(View.GONE);
		_loadingMore = false;
		dismissProgressDialog();
		Log.w(getClass().getSimpleName(), "onFail: " + arg0);
		if (_list != null)
		{
			_list.clear();
		}

		getMainActivity().noNetworkDialog();
	}

	@Override
	public int getLayoutResourceID()
	{
		return R.layout.posts_fragment_layout;
	}

	@Override
	protected void convertToUIItems(List<RVSPost> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		for (RVSPost post : data)
		{
			_list.add(new FullPostView(getActivity(), post, false));
		}
	}

	@Override
	public void onClick(View v)
	{
		if (v.equals(_followLayout))
		{
			followUser();
		}
		else if (v.equals(_followingLayout))
		{
			openFollowingList();
		}
		else if (v.equals(_followersLayout))
		{
			openFollowersList();
		}
		else if (v.equals(_likesLayout))
		{
			openLikedList();
		}
		else if (v.equals(_videoContainer))
		{
			super.onClick(v);
		}
	}

	private void openFollowersList()
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.EXTRA_USER_ID, _user.getUserId());
//		((MainActivity) getActivity()).replaceFragmentContent(FollowersUsersFragment.class, bundle);
//		getMainActivity().replaceTabFragmentContent(mCurrentTabPosition, FollowersUsersFragment.class, bundle);
		replaceTabFragmentContent(FollowersUsersFragment.class, bundle);


	}

	private void openLikedList()
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.EXTRA_USER_ID, _user.getUserId());
//		((MainActivity) getActivity()).replaceFragmentContent(LikedPostsFragment.class, bundle);
//		getMainActivity().replaceTabFragmentContent(mCurrentTabPosition, LikedPostsFragment.class, bundle);
		replaceTabFragmentContent(LikedPostsFragment.class, bundle);

	}

	private void openFollowingList()
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.EXTRA_USER_ID, _user.getUserId());
//		((MainActivity) getActivity()).replaceFragmentContent(FollowingUsersFragment.class, bundle);
//		getMainActivity().replaceTabFragmentContent(mCurrentTabPosition, FollowingUsersFragment.class, bundle);
		replaceTabFragmentContent(FollowingUsersFragment.class, bundle);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void followUser()
	{
		if (getMainActivity().isLoggedIn(this, false) == false)
		{
			getMainActivity().getCurrentFragment().addPendingOperation(new PendingOperation(OperationType.FOLLOW, _user.getUserId(), null));
		}
		else if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(_user.getUserId()))
		{ //can't follow yourself.
			_userProfileLayout.findViewById(R.id.row_1).setVisibility(View.GONE);
			return;
		}
		else if (_isFollowing)
		{
			RVSPromise followPromise = AppManager.getInstance().getSDK().unfollowUser(_user.getUserId());
			followPromise.then(new DoneCallback()
			{
				@Override
				public void onDone(Object result)
				{
					Log.i("UNfollow User: OK", "Horray!");

					if (isVisible() && getActivity() != null)
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								if (isVisible())
								{
									try
									{
										//_isFollowing = false; // amir *** disabled moved to if statement below ****
										((TextView) _userProfileLayout.findViewById(R.id.follow_title)).setText(getString(R.string.follow));
										((ImageView) _userProfileLayout.findViewById(R.id.follow_image)).setImageResource(R.drawable.add_follow_user);

										TextView followersNumber = (TextView) _userProfileLayout.findViewById(R.id.followers_number);
										int followersCount;
										followersCount = Integer.parseInt((followersNumber).getText().toString());
										if (followersCount > 0)
										{
											if (_isFollowing == true)
											{
												followersCount--;
												(followersNumber).setText(followersCount + "");
												_isFollowing = false;
											}

										}
									}
									catch (NumberFormatException e)
									{
										e.printStackTrace();
									}

									//									TextView tv = ((TextView) _userProfileLayout.findViewById(R.id.followers_number));
									//									String current = (String) tv.getText();
									//									tv.setText(Integer.parseInt(current) - 1 + "");
								}
							}
						});
					}
				}
			}, new FailCallback()
			{
				@Override
				public void onFail(Object result)
				{
					Log.e("UNfollow User: NOT OK", ":(");
					if (isVisible() && getActivity() != null)
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								if (isVisible())
								{
									try
									{
										_isFollowing = true;
										((TextView) _userProfileLayout.findViewById(R.id.follow_title)).setText(getString(R.string.following));
										((ImageView) _userProfileLayout.findViewById(R.id.follow_image)).setImageResource(R.drawable.followed_user);
									}
									catch (Exception e)
									{
										e.printStackTrace();
									}
								}
							}
						});
						getMainActivity().noNetworkDialog();
					}
				}
			});
		}
		else
		{
			RVSPromise followPromise = AppManager.getInstance().getSDK().followUser(_user.getUserId());
			followPromise.then(new DoneCallback()
			{
				@Override
				public void onDone(Object result)
				{
					Log.i("follow User: OK", "Horray!");
					//_isFollowing = true; // amir *** disabled - moved to if statement below ****
					if (isVisible())
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								if (isVisible())
								{
									TextView t = ((TextView) _userProfileLayout.findViewById(R.id.follow_title));
									t.setText(getString(R.string.following));

									((ImageView) _userProfileLayout.findViewById(R.id.follow_image)).setImageResource(R.drawable.followed_user);

									TextView followersNumber = (TextView) _userProfileLayout.findViewById(R.id.followers_number);
									int followersCount;
									try
									{
										if (_isFollowing == false)
										{
											followersCount = Integer.parseInt((followersNumber).getText().toString());
											followersCount++;
											(followersNumber).setText(followersCount + "");
											_isFollowing = true;
										}

									}
									catch (NumberFormatException e)
									{
										e.printStackTrace();
									}

									//									TextView tv = ((TextView) _userProfileLayout.findViewById(R.id.followers_number));
									//									String current = (String) tv.getText();
									//									tv.setText(Integer.parseInt(current) + 1 + "");
								}
							}
						});
					}
				}
			}, new FailCallback()
			{
				@Override
				public void onFail(Object result)
				{
					_isFollowing = false;
					Log.e("follow User: NOT OK", ":(");
					if (isVisible())
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								if (isVisible())
								{
									Toast.makeText(getActivity(), "Failed to follow user", Toast.LENGTH_SHORT).show();
									((TextView) _userProfileLayout.findViewById(R.id.follow_title)).setText(getString(R.string.follow));
									((ImageView) _userProfileLayout.findViewById(R.id.follow_image)).setImageResource(R.drawable.add_follow_user);
								}
							}
						});
					}

					getMainActivity().noNetworkDialog();
				}
			});
		}
	}

	public String getUserId()
	{
		return _user != null ? _user.getUserId() : "";
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			PendingOperation op = getPendingOperation();
			if (op != null)
			{
				executePendingOperation(op);
			}
		}
	}

	private void executePendingOperation(PendingOperation op)
	{
		switch (op.getType())
		{
			case NOTIFICATIONS:
			{
				getMainActivity().replaceFragmentContent(NotificationsFragment.class, null);
				break;
			}
			case FOLLOW:
			{
				followUser();
				break;
			}
			default:
				break;
		}
	}

	public void updateFollowingCount(int addOrRemove)
	{
		if (_isMe && _userProfileLayout != null && _userProfileLayout.findViewById(R.id.following_number) != null)
		{
			final TextView followingNumber = (TextView) _userProfileLayout.findViewById(R.id.following_number);
			final int followingCount = Integer.parseInt((followingNumber).getText().toString());
			try
			{
				final int newFollowingCount = followingCount + addOrRemove;

				if (getActivity() != null)
				{
					getActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							if (followingNumber != null)
							{
								followingNumber.setText(newFollowingCount + "");
							}
						}
					});
				}
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public String getActionBarTitle() {
		
		if (_isMe)
		{
			return getString(R.string.my_profile);
		} else if (_user != null) {
			return _user.getName();
		}
		
		return getString(R.string.profile);
	}
}