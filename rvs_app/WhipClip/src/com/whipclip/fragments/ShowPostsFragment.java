package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSContentSearchResult;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchMatch;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.ui.adapters.PostsListAdapter;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsMultiply;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsMultiplyItem;
import com.whipclip.ui.adapters.items.searchitems.SearchResultsSingle;
import com.whipclip.ui.widgets.ShowUserInteractionLayout;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShowPostsFragment extends PostsFragment
{
	public static final String SHOW_ID 	= "show_id";
	public static final String CHANNEL_ID 	= "channel_id";
//	private TextView	mSearchTextView;
	private ShowUserInteractionLayout	mUserInteractionLayout;
	private String	mSearchString;
	protected RVSPromise mSearchPromise;

	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		
		View view = super.onCreateView(inflater, container, savedInstanceState);

		if (_fragmentView != null)
		{
			TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
			text.setVisibility(View.GONE);
			
			if (mUserInteractionLayout == null) {
				mUserInteractionLayout = (ShowUserInteractionLayout)inflater.inflate(R.layout.show_user_interact_item, null);

//				unittest
//				_listView.addHeaderView(mUserInteractionLayout);
				
				mUserInteractionLayout.initLayout(getMainActivity(), new ShowUserInteractionLayout.OnSearchListener()
				{
					
					@Override
					public void onSearchStringSubmitted(String text)
					{
						getMainActivity().dismissKeyboard();
						String searchString = text;
						reload(searchString);	
					}
					
					@Override
					public void onClearButtonClick()
					{
						if (_promise != null && _promise.isPending()) {
							_promise.cancel();
							_promise = null;
						}
//							
						reload(null);
					}
				});
			} else {
				_listView.removeHeaderView(mUserInteractionLayout);
			}
			
//			TODO ILIYA
//			mSearchTextView = ((TextView)_fragmentView.findViewById(R.id.search_text));
//			mSearchTextView.setTypeface(AppManager.getInstance().getTypeFaceRegular());
//			((ImageButton)_fragmentView.findViewById(R.id.clear_icon_btn)).setOnClickListener(new View.OnClickListener()
//			{
//				
//				@Override
//				public void onClick(View v)
//				{
//					if (_promise != null && _promise.isPending()) {
//						_promise.cancel();
//						_promise = null;
//					}
//						
//					mSearchTextView.setText("");
//					reload(null);
//				}
//			});

//			TODO ILIYA
//			mSearchTextView.setOnEditorActionListener(new OnEditorActionListener()
//			{
//				
//				@Override
//				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
//				{
//					if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
//                            || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                        //Do your action
//						getMainActivity().dismissKeyboard();
//						
//						String searchString = v.getText().toString();
//						
//						reload(searchString);
//						
//                    }
//					return false;
//				}
//			});
			
//			 TODO ILIYA
//			mSearchTextView.setOnTouchListener(new View.OnTouchListener()
//			{
//				
//				@Override
//				public boolean onTouch(View v, MotionEvent event)
//				{
//					v.setFocusable(true);
//			        v.setFocusableInTouchMode(true);
//					return false;
//				}
//			});
			  
		}
		
		
//		
//		view.postDelayed(new Runnable()
//		{
//			
//			@Override
//			public void run()
//			{
//				mSearchTextView.clearFocus(); 
//				_fragmentView.findViewById(R.id.show_fragment_layout).requestFocus();
//				getMainActivity().dismissKeyboard();
//			}
//		}, 200);
//		

		return view;
	}
	
	protected void setEmptyView(String string)
	{
	}
	
	protected void onListScrolled(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
		
	}

	private void reload(String searchString)
	{
		if (_list != null ) {
			_list.clear();
		}
	
		if (_videoView != null)
		{
			stopVideo();
		}
		
		mSearchString = searchString;
		_dataAsync = null;
		
		getPosts(12);		
	}

	
	@SuppressWarnings("unchecked")
	protected void getPosts(final int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
				//				showProgressDialog();
			}
			if (_dataAsync == null)
			{
//				if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID) == null
//						|| "".equals(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID)))
//				{
//					return;
//				}
				
				
				final String showId = getArguments().getString(SHOW_ID, null);
				final String channelId = getArguments().getString(CHANNEL_ID, null);
				
				if (mSearchString == null || mSearchString.isEmpty()) {
					_dataAsync = AppManager.getInstance().getSDK().getTrendingPostsForShow(showId, channelId);
				} else {
					_dataAsync = AppManager.getInstance().getSDK().searchContent(mSearchString, showId, channelId);
				}
				
				RVSShow showObj = AppManager.getInstance().getShow();
				if (showObj != null && showObj.getShowId().equals(showId) && showObj.getChannel().equals(channelId)) {
				
					initStep2(showObj, number);
					
				} else {
					RVSPromise showForShowId = RVSSdk.sharedRVSSdk().getShowForShowId(showId, channelId);
					showForShowId.then(new AndroidDoneCallback<RVSShow>(){

						@Override
						public void onDone(RVSShow showObj)
						{
							initStep2(showObj, number);
							AppManager.getInstance().setShow(showObj);
							MainActivity activity = (MainActivity) getActivity();
							if (activity != null) {
								activity.updateActionTitle();
							}
						}

						@Override
						public AndroidExecutionScope getExecutionScope()
						{
							// TODO Auto-generated method stub
							return AndroidExecutionScope.UI;
						}
						
					});
					
				}
			        
					
	//				if (showObj.getNextProgram() != null) {
						_listView.removeHeaderView(mUserInteractionLayout);
						_listView.addHeaderView(mUserInteractionLayout);
	//				}
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	private void initStep2(RVSShow showObj, int number)
	{
		mUserInteractionLayout.setShow(showObj);
		
		int episodesCount = showObj.getEpisodesCount();
        if (episodesCount < 2)
        {
        	mUserInteractionLayout.setTitle(WhipClipApplication._context.getResources().getString(R.string.show_detail_posts_header_last_episode));
        }
        else
        {
        	String str = WhipClipApplication._context.getResources().getString(R.string.show_detail_posts_header_episodes_format);
            mUserInteractionLayout.setTitle(String.format(str, "" + episodesCount));
        }   
        
        
        if (!_dataAsync.getDidReachEnd())
		{
			if (mSearchString != null && !mSearchString.isEmpty()) {
				
				if (mSearchPromise != null && mSearchPromise.isPending())
				{
					return;
				}
				
				mSearchPromise = _dataAsync.next(number);
				if (mSearchPromise != null) {
					mSearchPromise.then(new DoneCallback<List<RVSContentSearchResult>>()
					{
						
						@Override
						public void onDone(List<RVSContentSearchResult> result)
						{
							onSearchDone(result);
						}
					} , this);
				}

			} else {
			
				if (_promise != null && _promise.isPending())
				{
					return;
				}
				
				_promise = _dataAsync.next(number);
				if (_promise != null)
				{
					_promise.then(this, this);
				}
			}
		}
		
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		((MainActivity) getActivity()).onSectionAttached(18);
		
//		if (((MainActivity) getActivity()).getCurrentTabFragment() instanceof FeedFragment) {
//		if (((MainActivity) getActivity()).isMyTabOpened(TabFragmentAdapter.TAB_POSITION_HOME)) {
//			
//			((MainActivity) getActivity()).onSectionAttached(1);
//			AppManager.getInstance().sendToGA(getString(R.string.feed));
//			AppManager.getInstance().setRefreshPosts(false);
//		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(1);
	}

	@Override
	protected void convertToUIItems(List<RVSPost> data)
	{
		// workaround, todo move search to different view//flow  
		boolean isSearchResult = false;
		if (data != null && data.size() > 0) {
			Object object = data.get(0);
			if (object instanceof RVSContentSearchResult) {
				isSearchResult = true;
			}
		}
		
		if (mSearchString != null && !mSearchString.isEmpty() && isSearchResult) {
			contentConvertToUIItems((List<RVSContentSearchResult>)((List<?>)data));	
			mSearchString = null;
		} else {
			super.convertToUIItems(data);
		}
	}
	
	@Override
	public void onDone(List<RVSPost> arg0)
	{
		super.onDone(arg0);
		
		if (_fragmentView != null) {
			if (arg0.size() == 0) {
				
				TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
				text.setText(R.string.trending_shows_no_data);
				text.setVisibility(View.VISIBLE);
			}
			else {
				TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
				text.setVisibility(View.GONE);
			}
	//		TextView empty = (TextView) _fragmentView.findViewById(R.id.empty_view);
	//		empty.setText(R.string.trending_shows_no_data);
	//		_listView.setEmptyView(empty);
		}

	}
	
	public String getActionBarTitle()
	{
		RVSShow show = AppManager.getInstance().getShow();
		if (show != null) {
			return show.getName();
		}
		
		return "";
	}
	
	@Override
	public int getLayoutResourceID()
	{
//		return R.layout.posts_fragment_layout;
		return R.layout.shows_fragment_layout;
	}
	
	
	////dsd
	
	
	private ArrayList<PostUIItem> contentConvertToUIItems(List<RVSContentSearchResult> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		for (RVSContentSearchResult post : data)
		{
			handleContentResults(post);
		}
		return _list;
	}
	
	// ****** Search result handle *********
	private void handleContentResults(RVSContentSearchResult searchResult)
	{
		List<RVSSearchMatch> searchMatches = searchResult.getSearchMatches();
		if (searchMatches != null && searchMatches.size() > 0 && searchResult.getPost() != null)
		{
			List<RVSSearchMatch> filteredSearchMatchesFrom = SearchFragment.filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT);
			if (filteredSearchMatchesFrom.size() > 0) {
				_list.add(generateFullPostItem(searchResult.getPost(), filteredSearchMatchesFrom.get(0)));
			} else {
				_list.add(generateFullPostItem(searchResult.getPost(), searchMatches.get(0)));
			}
		}
		else
		{
			RVSProgramExcerpt program = searchResult.getProgramExcerpt();
			if (program.getMediaContext() == null)
			{//multiply	with exception bellow.
				PostUIItem item = generateFakeSingleFromMultiply(searchMatches, searchResult);
				if (item != null)
				{
					_list.add(item);
				}
				else
				{ //generateMultiplyItem might return singleItem, if while iterating searchMatches we're 
					//finding only 1 item.
					if (searchMatches  !=null) {
						_list.add(generateMultiplyItem(program, searchResult, SearchFragment.filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT)));
					}
				}
			}
			else
			{//single
				PostUIItem item = generateSingleItem(program, searchResult, SearchFragment.filteredSearchMatchesFrom(searchMatches, SearchFragment.MAX_MATCHES_IN_RESULT));
				if (item != null)
				{
					_list.add(item);
				}
			}
		}
	}
	
	/******************************************************************************************************************************************
	 * Aux functions for handleContentResults
	 * PostUIItem generateFullPostItem(RVSPost rvsPost, RVSSearchMatch rvsSearchMatch)
	 * PostUIItem generateSingleItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	 * PostUIItem generateFakeSingleFromMultiply(List<RVSSearchMatch> searchMatches, RVSContentSearchResult post)
	 * PostUIItem generateMultiplyItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	 ******************************************************************************************************************************************/
	private PostUIItem generateFullPostItem(RVSPost rvsPost, RVSSearchMatch rvsSearchMatch)
	{
		String searchTitle = "";
		if (rvsSearchMatch != null)
		{
			searchTitle = SearchFragment.searchResultsTitleFormat(rvsSearchMatch.getFieldName(), rvsSearchMatch.getValueFragment());
		}
		FullPostView item = new FullPostView(getActivity(), rvsPost, true);
		item.setSearchQuery(mSearchString);
		item.setSearchTitle(searchTitle);
		return item;
	}

	private PostUIItem generateSingleItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	{
		if (searchMatches.size() == 0) return null;

		if (program.getProgram() != null)
		{ //Getting the type from the 2nd item, if exists. 
			//			if (searchMatches.size() == 2)
			//			{
			//				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), searchResultsTitleFormat(searchMatches.get(1).getFieldName(),
			//						program.getProgram().getShow().getSynopsis()), null, this, _searchQuery, program.getMediaContext());
			//			}
			//			else
			{ //There's no type for this item.
				//				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), program.getProgram().getShow().getSynopsis(), null, this,
				//						_searchQuery, program.getMediaContext());
				return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), SearchFragment.searchResultsTitleFormat(searchMatches.get(0).getFieldName(),
						searchMatches.get(0).getValueFragment()), null, mSearchString, program.getMediaContext());
			}
		}
		else
		{
			RVSSearchMatch match = searchMatches.get(0);
			return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), SearchFragment.searchResultsTitleFormat(match.getFieldName(),
					match.getValueFragment()), null, mSearchString, program.getMediaContext());
		}
	}

	private PostUIItem generateFakeSingleFromMultiply(List<RVSSearchMatch> searchMatches, RVSContentSearchResult post)
	{
		if (searchMatches.size() == 0) return null;

		//This is basically a multiply item, but it's converted to single item.
		RVSSearchMatch match = searchMatches.get(0);
		if (searchMatches.size() ==2 && searchMatches.get(1).getCoverImage() == null)
		{ //this is a multiply item, BUT, with only 1 item, so we're converting it to single.
			return new SearchResultsSingle(getActivity(), post.getProgramExcerpt(), SearchFragment.searchResultsTitleFormat(match.getFieldName(), match.getValueFragment()),
					match.getCoverImage(), mSearchString, match.getMediaContext());
		}
		else if (match.getCoverImage() == null)
		{ //simple single element - choosing the 1st item from the search matches.
			return new SearchResultsSingle(getActivity(), post.getProgramExcerpt(), SearchFragment.searchResultsTitleFormat(match.getFieldName(), match.getValueFragment()),
					null, mSearchString, match.getMediaContext());
		}

		return null;
	}

	private PostUIItem generateMultiplyItem(RVSProgramExcerpt program, RVSContentSearchResult searchResult, List<RVSSearchMatch> searchMatches)
	{
		ArrayList<SearchResultsMultiplyItem> items = new ArrayList<SearchResultsMultiplyItem>();
		for (RVSSearchMatch searchItem : searchMatches)
		{
			if (searchItem.getCoverImage() != null)
			{
				items.add(new SearchResultsMultiplyItem(searchItem));
			}
			if (items.size() == Consts.SEARCH_RESULTS_MULTIPLY_LIMIT)
			{ //maximum results per multiply item.
				break;
			}
		}

		if (items.size() == 1)
		{ //convert multiply item to single item.
			return new SearchResultsSingle(getActivity(), searchResult.getProgramExcerpt(), SearchFragment.searchResultsTitleFormat(searchMatches.get(0).getFieldName(),
					searchMatches.get(0).getValueFragment()), items.get(0).getImage(), mSearchString, searchMatches.get(0).getMediaContext());
		}
		else
		{
			SearchResultsMultiply result = new SearchResultsMultiply(getActivity(), items, program);
			result.setSearchQuery(mSearchString);
			return result;
		}
	}
	
	
	public void onSearchDone(List<RVSContentSearchResult> arg0)
	{
		if (!isVisible() || _fragmentView == null)
		{
			return;
		}

		if (arg0.size() == 0) {
			
			TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
			text.setText(R.string.trending_shows_no_data);
			text.setVisibility(View.VISIBLE);
		}
		else {
			TextView text = (TextView) _fragmentView.findViewById(R.id.empty_view);
			text.setVisibility(View.GONE);
		}

		_swipeLayout.setRefreshing(false);
		
		_progressBar.setVisibility(View.GONE);
		dismissProgressDialog();
		Log.i(getClass().getSimpleName(), "onDone: " + arg0.size());
		if (arg0 != null)
		{
			contentConvertToUIItems(arg0);
			
			if (_postsAdapter == null)
			{
				_postsAdapter = new PostsListAdapter(ShowPostsFragment.this.getActivity(), _list, this);
				_listView.setAdapter(_postsAdapter);
			}
			else
			{
				_postsAdapter.insertElementsToRealtimePost(_list);
				_postsAdapter.notifyDataSetChanged();
			}
		}
		_loadingMore = false;
	}
	
	
	
	//

}
