package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;
import com.whipclip.ui.adapters.CommentsAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.adapters.items.searchitems.PostInformation;
import com.whipclip.ui.views.SwipeDismissListViewTouchListener;
import com.whipclip.ui.views.SwipeDismissListViewTouchListener.DismissCallbacks;

public class CommentFragment extends BaseFragment<List<RVSCommentImp>> implements TextWatcher, OnClickListener, OnItemLongClickListener
{
	private View						_view;
	private ListView					_commentsListView;
	private CommentsAdapter				_listAdapter;

	private EditText					_commentText;
	private TextView					_charactersCounter;
	private Button						_commentPost;
	private TextView					_loadMoreCommentsText;
	private View						_lineSeperator;
	private ProgressBar					_progressBar;
	private ImageButton					_deleteButton;
	private ImageButton					_atButton;

	private int							_maxCharacters			= 0;

	private String						_postId;
	private ArrayList<RVSComment>		_commentsList;

	private RVSAsyncList				_commentsAsyncList;
	@SuppressWarnings("rawtypes")
	private RVSPromise					_commentsPromise;
	private int							_commentNumItems		= 0;
	private int							_counterSaveTotalItems	= 0;
	private int							_counterRemovedComments	= 0;
	private int							_counterAddComments		= 0;
	private boolean						_commentsWereChanged	= false;

	//Variables to interact with delete/add comment.
	private float[]						_saveLastPosition		= { 0, 0 };
	private String						_commentInformation;
	private int							_commentPositionToDelete;
	private Animation					_disapearAnimation;
	private int	_tabPositon = -1;
	public static boolean				ANIMATE_ADD_NEW_ITEM	= false;	//animating new item on getView's adapter.

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = inflater.inflate(R.layout.fragment_comment, container, false);

		((MainActivity) getActivity()).setActivityTitle(getActivity().getString(R.string.comments));

		initViews(_view);
		initListeners();
		initDeleteVars();
		getComments();

		_deleteButton.setVisibility(View.VISIBLE);
		_deleteButton.setVisibility(View.INVISIBLE);
		_atButton.setVisibility(View.VISIBLE);
		_atButton.setVisibility(View.INVISIBLE);

		setHasOptionsMenu(true);

		return _view;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	AppManager.getInstance().sendToGA(getString(R.string.comments));
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
//		AppManager.getInstance().sendToGA(getString(R.string.comments));
	}

	@Override
	public void onDestroy()
	{
		getActionBar().setDisplayHomeAsUpEnabled(true);
//		((MainActivity) getActivity()).showNavigation();
		dismissKeyboard();
		if (_commentsWereChanged)
		{ //only if there's a change in the items(more/less items), we'd like to update the prior fragment.
			AppManager.getInstance().setRelatedComments(_counterSaveTotalItems - _counterRemovedComments + _counterAddComments, _postId, _commentsList);
		}
		super.onDestroy();
	}

	@Override
	public void initViews(View v)
	{
		_commentsListView = (ListView) _view.findViewById(R.id.comments_list);
		_commentText = (EditText) _view.findViewById(R.id.comments_text);

		_commentText.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		_charactersCounter = (TextView) _view.findViewById(R.id.comments_characters_counter);
		_commentPost = (Button) _view.findViewById(R.id.comment_post);
		_loadMoreCommentsText = (TextView) _view.findViewById(R.id.comments_load_more);
		_lineSeperator = _view.findViewById(R.id.comments_separator);
		_progressBar = (ProgressBar) _view.findViewById(R.id.comment_progressbar);
		_deleteButton = (ImageButton) _view.findViewById(R.id.comment_delete);
		_atButton = (ImageButton) _view.findViewById(R.id.comment_at);

		_maxCharacters = getResources().getInteger(R.integer.comment_max_length);
		_charactersCounter.setTypeface(AppManager.getInstance().getTypeFaceLight());
		_commentPost.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_loadMoreCommentsText.setTypeface(AppManager.getInstance().getTypeFaceLight());

		_commentsList = new ArrayList<RVSComment>();
		_disapearAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.disappear);

		Bundle bundle = getArguments();
		if (bundle != null)
		{
			_postId = bundle.getString(Consts.COMMENTS_POST_ID, null);
			_tabPositon  = bundle.getInt(Consts.COMMENTS_POST_ID, -1);
			_commentNumItems = bundle.getInt(Consts.COMMENTS_NUM_ITEMS, 0);
			
			_counterSaveTotalItems = _commentNumItems;
			Log.e(getClass().getName(), "comments size = " + _commentNumItems);
		}

		final SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(_commentsListView, new DismissCallbacks()
		{
			public void onDismiss(ListView listView, int[] reverseSortedPositions)
			{
				for (int position : reverseSortedPositions)
				{
					Log.e("ASDFASDF", "" + position);
					_commentInformation = _commentsList.get(position).getCommentId();
					_commentPositionToDelete = position;
					handleDeleteComment();
					_listAdapter.remove(_listAdapter.getItem(position));
				}
				_listAdapter.notifyDataSetChanged();
			}

			@Override
			public boolean canDismiss(int position)
			{
				return AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(_commentsList.get(position).getAuthor().getUserId());
			}
		});
		_commentsListView.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN)
				{
					initDeleteVars();
				}
				return touchListener.onTouch(v, event);
			}
		});
		_commentsListView.setOnScrollListener(touchListener.makeScrollListener());
	}

	@Override
	public void initListeners()
	{
		_commentText.addTextChangedListener(this);
		_commentPost.setOnClickListener(this);
		_loadMoreCommentsText.setOnClickListener(this);
		_commentsListView.setOnItemLongClickListener(this);
		//		_commentsListView.setOnTouchListener(this);
		_deleteButton.setOnClickListener(this);
		_atButton.setOnClickListener(this);
		_disapearAnimation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				
				RVSComment rvsComment = _commentsList.get(_commentPositionToDelete);
				
				_commentsWereChanged = true;
				_commentsList.remove(_commentPositionToDelete);
				_counterRemovedComments++;
				_listAdapter.notifyDataSetChanged();
				initDeleteVars();
			}
		});
	}

	private void initDeleteVars()
	{
		_deleteButton.setVisibility(View.GONE);
		_atButton.setVisibility(View.GONE);

		_saveLastPosition[0] = 0;
		_saveLastPosition[1] = 0;
		_commentInformation = null;
		_commentPositionToDelete = -1;
	}

	@SuppressWarnings({ "unchecked" })
	private void getComments()
	{
		_progressBar.setVisibility(View.VISIBLE);
		_commentsAsyncList = AppManager.getInstance().getSDK().commentsForPost(_postId);
		if (!_commentsAsyncList.getDidReachEnd())
		{
			_commentsPromise = _commentsAsyncList.next(Consts.MAX_DISPLAY_COMMENTS);
			_commentsPromise.then(this);
		}
	}

	/**
	 * Call this when there's more than 5 comments.
	 * We're only showing 5 comments at a time, if a user wants to see more, we'll reload more.
	 */
	private void setLoadMore()
	{
		if (_commentsList.size() < _commentNumItems)
		{
			_loadMoreCommentsText.setVisibility(View.VISIBLE);
			_lineSeperator.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _commentsListView.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
			_commentsListView.setLayoutParams(params);
			Log.i(getClass().getSimpleName(), "setLoadMore - there are more comments to load");
		}
		else
		{
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _commentsListView.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			_commentsListView.setLayoutParams(params);
			Log.i(getClass().getSimpleName(), "setLoadMore - there are NO more comments to load");
		}
	}

	@Override
	public void onDone(List<RVSCommentImp> result)
	{
		Log.e(getClass().getSimpleName(), "OnDone comment: " + result);

		if (result != null)
		{
			Collections.reverse(_commentsList);
			for (int i = 0; i < result.size(); i++)
			{
				if (isCommentAlreadyExists(result.get(i).getCommentId()) == false)
				{
					_commentsList.add(result.get(i));
				}
			}
			Collections.reverse(_commentsList);
		}
		if (isVisible())
		{
			setLoadMore();
			_listAdapter = new CommentsAdapter(getActivity(), R.layout.comment_item, _commentsList, CommentFragment.this);
			_commentsListView.setAdapter(_listAdapter);
			_handler.postAtTime(new Runnable()
			{
				@Override
				public void run()
				{
					if (isVisible())
					{
						Log.i("hadas", "dasdfads   " + _commentsListView.getHeight());
						if (_commentsListView.getHeight() > WhipClipApplication._screenHeight / 2)
						{ //Only openning the keyboard if it's not opened yet. 
							((MainActivity) getActivity()).showKeyboard();
						}
					}
				}
			}, this, SystemClock.uptimeMillis() + 150);
			_progressBar.setVisibility(View.GONE);
		}
	}

	/**
	 * This is needed in the situations where we've erased some comments, and then we're
	 * loading more comments. We have to check if its already existed, since we don't want
	 * it to display multiply times. 
	 * @return
	 */
	private boolean isCommentAlreadyExists(String commentId)
	{
		for (RVSComment item : _commentsList)
		{
			if (item.getCommentId().equals(commentId))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onProgress(Integer progress)
	{
	}

	@Override
	public void onPause()
	{
		_handler.removeCallbacksAndMessages(this);
		((MainActivity) getActivity()).dismissKeyboard();
		super.onPause();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.feeds, menu);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home: 
            getMainActivity().onBackPressed();
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    } 

	/**
	 * EditText methods
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		String enterCheck = s.toString().replace("\n", "").trim();
		if ("".equals(enterCheck))
		{
			_commentPost.setEnabled(false);
			_commentPost.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
			_charactersCounter.setText(_maxCharacters + "");
			return;
		}

		int counter = _maxCharacters - s.length();
		_charactersCounter.setText(Integer.toString(counter));
		if (counter <= -1)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.dark_red));
			_commentPost.setEnabled(false);
			_commentPost.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
		}
		else if (counter >= 0 && counter < _maxCharacters)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.gray_text_comment));
			_commentPost.setEnabled(true);
			_commentPost.setTextColor(getResources().getColor(R.color.orange));
		}
		else if (counter >= _maxCharacters)
		{
			_commentPost.setEnabled(false);
			_commentPost.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
		}
	}

	@Override
	public void afterTextChanged(Editable s)
	{
		String subString = s.toString();
		if (subString.length() == 0)
		{
			return;
		}
		subString = subString.substring(subString.length() - 1);
		if (subString.equals("\n"))
		{
			subString = s.toString().substring(0, s.toString().length() - 1);
			_commentText.setText(subString);
			_commentText.setSelection(_commentText.getText().length());
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
	}

	/**
	 * _commentPost click (aka "post")
	 */
	@Override
	public void onClick(View v)
	{
		if (v.equals(_commentPost))
		{
			handlePostClick();
		}
		else if (v.equals(_loadMoreCommentsText))
		{
			handleMoreCommentsClick();
		}
		else if (v.equals(_deleteButton))
		{
			handleDeleteComment();
		}
		else if (v.equals(_atButton))
		{
			handleAtComment();
		}
	}

	private void handleAtComment()
	{
		_atButton.setVisibility(View.GONE);
		if (_commentInformation != null)
		{
			_commentText.append(_commentInformation + " ");
			_commentText.setSelection(_commentText.getText().length());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void handleDeleteComment()
	{
		_deleteButton.setVisibility(View.GONE);
		if (_commentInformation != null)
		{
			RVSPromise promiss = AppManager.getInstance().getSDK().deleteComment(_commentInformation, _postId);
			promiss.then(new DoneCallback<Object>()
			{

				@Override
				public void onDone(Object result)
				{
					Log.e(getClass().getName(), "successfully erased comment");
					_commentNumItems--;
					
					
					AppManager app = AppManager.getInstance();
					
					PostInformation openedCommentsViewPost = app.getOpenedCommentsViewPost();
					openedCommentsViewPost.removeCommentFromLatestsComments(_commentInformation);
//					String myUserId = rvsComment.getAuthor().getUserId();
					String myUserId = AppManager.getInstance().getSDK().getMyUserId();
					boolean myCommnetExist = false;
					for (RVSComment c : _commentsList) {
						if (c.getAuthor().getUserId().equals(myUserId))
							myCommnetExist = true;
							break;
					}
					
					openedCommentsViewPost.setCommentedByCaller(myCommnetExist);
					
				}
			}, new FailCallback<Object>()
			{

				@Override
				public void onFail(Object result)
				{
					if (getMainActivity() != null)
					{
						getMainActivity().noNetworkDialog();
						initDeleteVars();
					}
				}
			});
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void handlePostClick()
	{
		if (getMainActivity().isLoggedIn(this, false))
		{
			createComment();
		}
		else
		{
			addPendingOperation(new PendingOperation(OperationType.COMMENT, null, null));
		}

	}

	@SuppressWarnings("unchecked")
	private void createComment()
	{
		
		_progressBar.setVisibility(View.VISIBLE);
		String commentTxt = _commentText.getText().toString().replaceAll("\\s+", " ");
		
		// TODO
		final RVSPost openedCommentPost = AppManager.getInstance().getOpenedCommentPost();
		
		RVSPromise promiss = AppManager.getInstance().getSDK().createCommentWithMessage(commentTxt, _postId);
		promiss.then(new DoneCallback<RVSComment>()
		{

			@Override
			public void onDone(final RVSComment result)
			{
				if (result != null && isVisible())
				{
					if (openedCommentPost != null) {
						AnalyticsManager.sharedManager().trackPostCommentWithPost(openedCommentPost, result, _tabPositon);
					}
					
					getActivity().runOnUiThread(new Runnable()
					{

						@Override
						public void run()
						{
							_commentNumItems++;
							_counterAddComments++;
							_commentsList.add(result);
							if (_listAdapter != null)
							{
								_listAdapter.notifyDataSetChanged();
							}
							_commentsWereChanged = true;
							ANIMATE_ADD_NEW_ITEM = true;
							setLoadMore();
							_progressBar.setVisibility(View.GONE);
							
							AppManager app = AppManager.getInstance();
							app.setLastPostCommented(_postId);
							
							PostInformation openedCommentsViewPost = app.getOpenedCommentsViewPost();
							openedCommentsViewPost.addCommentToLatestsComments(result);
							openedCommentsViewPost.setCommentedByCaller(true);
							
							getMainActivity().onBackPressed();
							
						}
					});
				}
			}
		});

		//restoring the edittext and post button to the start status.
		_commentText.setText("");
		_commentText.setHint(R.string.write_a_comment);
		_commentText.requestFocus();

		_commentPost.setEnabled(false);
		_commentPost.setTextColor(getResources().getColor(R.color.orange_semi_transparent));

	}

	@SuppressWarnings("unchecked")
	private void handleMoreCommentsClick()
	{
		if (_commentsList != null)
		{
			Log.e(getClass().getName(), "Loading more comments");
			_progressBar.setVisibility(View.VISIBLE);
			if (_commentsPromise != null && _commentsPromise.isPending())
			{
				return;
			}
			
			if (!_commentsAsyncList.getDidReachEnd())
			{
				_commentsPromise = _commentsAsyncList.next(Consts.MAX_DISPLAY_COMMENTS);
				_commentsPromise.then(this);
			}
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3)
	{
		if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(_commentsList.get(position).getAuthor().getUserId()))
		{
			return true;
		}
		else
		{
			_commentInformation = _commentsList.get(position).getAuthor().getName();
			_commentPositionToDelete = position;
			if (true)//(_saveLastPosition[0] != 0)
			{
				_atButton.setX((WhipClipApplication._screenWidth / 2) - (_atButton.getWidth() / 2));
				int[] arr = new int[2];
				arg1.getLocationOnScreen(arr);
				_atButton.setY(arr[1] - arg1.getHeight());
				_atButton.setVisibility(View.VISIBLE);
			}
		}

		Log.i(getClass().getName(), "going to erase comment - " + _commentsList.get(position).getMessage() + " position : " + _saveLastPosition[0] + " "
				+ _saveLastPosition[1]);

		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			PendingOperation op = getPendingOperation();
			if (op != null)
			{
				if (op.getType() == OperationType.COMMENT)
				{
					createComment();
				}
			}
		}
	}
	
	@Override
	public String getActionBarTitle() {
		
		return getActivity().getString(R.string.comments);
	}
}
