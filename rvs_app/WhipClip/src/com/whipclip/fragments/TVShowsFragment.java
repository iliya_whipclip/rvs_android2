package com.whipclip.fragments;

import com.whipclip.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;


public class TVShowsFragment extends Fragment implements OnClickListener {

    public static final String POSITION_KEY = "FragmentPositionKey";
    private int position;

    public TVShowsFragment(int i)
	{
    	 position = i;
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_tv_shows, container, false);
        TextView textview = (TextView) root.findViewById(R.id.textViewPosition);
        textview.setText(Integer.toString(position));
        textview.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(v.getContext(), "Clicked Position: " + position, Toast.LENGTH_LONG).show();
    }
}
