package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.SignUpActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView.RVSClipViewStateChangedCallback;
import com.whipclip.rvs.sdk.dataObjects.RVSClipView.State;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnail;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnailList;
import com.whipclip.rvs.sdk.dataObjects.RVSTranscriptItem;
import com.whipclip.ui.DialogUtils;
import com.whipclip.ui.views.AsyncImageView;
import com.whipclip.ui.views.TimelineView;
import com.whipclip.ui.views.TimelineView.MoveMode;

public class ComposeFragment extends BaseFragment<Object> implements OnSeekBarChangeListener, TextWatcher
{
	private static final int		kMaxCharacters				= 140;

	private View					_view;

	//video image & video.
	private AsyncImageView			_videoImage;
	private RVSClipView				_videoClip;
	private Button					_playPause;
	private ProgressBar				_videoSpinner;

	private AsyncImageView			_videoPreviewImage;

	private SeekBar					_seekbar;
	private TextView				_markingText;
	private TextView				_videoCaptionText;
	
	private EditText				_caption;
	private EditText				_textViewPrompt;

	private int						MARKING_TEXT_WIDTH			= 0;

//	private ComposeAdapter			_composeAdapter;

	private RVSMediaContext			_saveLastResult;
	private List<RVSThumbnail>		_mediaContextThumbnais;
	private long					_startTime					= 0;								//Representing the start and end time of the video/images.
	private long					_endTime					= 0;
//	private int						_framesGap;

	private RVSMediaContext			_mediaContext;
	private Toast					_shareToast;
	private boolean					_videoUnavailible			= false;

	private View					_toastLayout;
	private int						_currentTime				= 0;
	private boolean					_resetMediaVideo			= false;

	private RVSAsyncList			_transcriptsList;
	private RVSPromise				_transcriptPromise;

	private List<RVSTranscriptItem>	_transcripts;
	private String					_defaultText;

	private boolean					_textWasEditedByUser;

	private long					_selectionStartOffsetSec	= 0;

	private TimelineView 					_timelineView;
	private TextView					_charactersCounter;
	private int							_maxCharacters			= 0;

	private boolean	mIsLive;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = inflater.inflate(R.layout.fragment_compose, container, false);

		setHasOptionsMenu(true);
		getActivity().invalidateOptionsMenu();

		_timelineView = new TimelineView(getActivity(), this, _handler);
		
		_maxCharacters = getResources().getInteger(R.integer.comment_max_length);
		_charactersCounter = (TextView) _view.findViewById(R.id.comments_characters_counter);
		_charactersCounter.setTypeface(AppManager.getInstance().getTypeFaceLight());
		

		initViews(_view);
		initListeners();
		initMediaContext();
		
		if (mIsLive) {
			_videoCaptionText.setText(R.string.create_clip_live);
		} else {
			_videoCaptionText.setText(R.string.create_clip_vod);
		}
	
		((MainActivity) getActivity()).useNavigation(false);
		
		return _view;
	}
	
	/**
	 * EditText methods
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		String enterCheck = s.toString().replace("\n", "").trim();
		if ("".equals(enterCheck))
		{
//			_caption.setEnabled(false);
//			_caption.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
			_charactersCounter.setText(_maxCharacters + "");
			return;
		}

		int counter = _maxCharacters - s.length();
		_charactersCounter.setText(Integer.toString(counter));
		if (counter <= -1)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.dark_red));
//			_caption.setEnabled(false);
//			_caption.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
		}
		else if (counter >= 0 && counter < _maxCharacters)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.gray_text_comment));
//			_caption.setEnabled(true);
//			_caption.setTextColor(getResources().getColor(R.color.orange));
		}
		else if (counter >= _maxCharacters)
		{
//			_caption.setEnabled(false);
//			_caption.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
		}
	}


	@SuppressWarnings("unchecked")
	private void initMediaContext()
	{
		try
		{
			_mediaContext = AppManager.getInstance().getCurrentMediaContext();
			mIsLive = false;
			
			if (_mediaContext != null)
			{
				if (getArguments() != null)
				{
					mIsLive = getArguments().getBoolean(Consts.BUNDLE_PROGRAML_IS_LIVE, false);
					String caption = getArguments().getString(Consts.BUNDLE_TEXT_COMPOSE, null);
					if (caption != null)
					{
						setCaptionText(caption);
					}
				}
				_videoSpinner.setVisibility(View.VISIBLE);
				handleOnDone(_mediaContext);
			}
			else if (getArguments() != null)
			{
				String caption = getArguments().getString(Consts.BUNDLE_CHANNEL_SYN, null);
				String channelId = getArguments().getString(Consts.BUNDLE_CHANNEL_ID_COMPOSE, null);
				String programId = getArguments().getString(Consts.BUNDLE_PROGRAML_ID_COMPOSE, null);
				mIsLive = getArguments().getBoolean(Consts.BUNDLE_PROGRAML_IS_LIVE, false);
				
				if (channelId != null)
				{
					_videoSpinner.setVisibility(View.VISIBLE);
					loadChannel(channelId, programId);

				}
				else if (caption != null)
				{
					setCaptionText(caption);
				}

			}
		}
		catch (AssertionError e)
		{
			e.printStackTrace();
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

	}

	private void setCaptionText(String caption)
	{
		if (!isAdded()) return ;
		
		_caption.setText(caption);
		
		int counter = _maxCharacters - caption.length();
		_charactersCounter.setText(Integer.toString(counter));
	}

	@SuppressWarnings("unchecked")
	private void loadChannel(final String channelId, String programId)
	{
		AppManager.getInstance().setMediaContextStartOffsetSec(-1);
		RVSPromise mediaContextForChannel = null;
		
		if (programId != null && !programId.isEmpty()) {
			mediaContextForChannel = AppManager.getInstance().getSDK().getMediaContextForProgram(programId, channelId);	
		} else {
			mediaContextForChannel = AppManager.getInstance().getSDK().getMediaContextForChannel(channelId);	
		}
			
		mediaContextForChannel.then(new AndroidDoneCallback<RVSMediaContext>()
		{
			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}

			@Override
			public void onDone(final RVSMediaContext result)
			{
				_mediaContext = result;
				handleOnDone(result);

				if (result == null) {
					showErrorDialog();
					
				} else if (_defaultText != null)
				{
					updateTextFromDefaultText();
				}
				else
				{
					getAllTranscripts(channelId);
				}
			}

		}, new FailCallback<Object>()
		{

			@Override
			public void onFail(Object result)
			{
				Log.e(getClass().getName(), "on fail 1!");
				getMainActivity().noNetworkDialog();

				String caption = getArguments().getString(Consts.BUNDLE_CHANNEL_SYN, null);
				if (caption != null)
				{
					if (_defaultText != null)
					{
						setCaptionText(_defaultText);
					}
				}
			}
		});

	}

	private void getAllTranscripts(final String channelId)
	{
		_transcripts = new ArrayList<RVSTranscriptItem>();
		_transcriptsList = RVSSdk.sharedRVSSdk().getTranscriptItemsForChannel(channelId, _mediaContext.getStart(), _mediaContext.getEnd());
		getNextTranscripts();
	}

	@SuppressWarnings("unchecked")
	private void getNextTranscripts()
	{
				
		if (_transcriptPromise != null && _transcriptPromise.isPending()) {
			_transcriptPromise.cancel();
		}
	    _transcriptPromise = _transcriptsList.next(100);
	    _transcriptPromise.then(new DoneCallback<List<RVSTranscriptItem>>() {

			@Override
			public void onDone(List<RVSTranscriptItem> result)
			{
				_transcripts.addAll(result);
				if (_transcriptsList.getDidReachEnd()) {
					Log.i(getClass().getName(),String.format("done! found %s transcripts", _transcripts.size()));
			        updateTextFromTranscriptBySelection();
				} else {
					getNextTranscripts();
				}
				
			}
		}).fail(new FailCallback<Throwable>() {

			@Override
			public void onFail(Throwable result)
			{
				updateTextFromTranscriptBySelection();
			}
			
		});
	}

	public void updateTextFromTranscriptBySelection()
	{
		if (_mediaContext != null) {
			updateTextFromTranscriptWithStartTime(_mediaContext.getStart() / 1000 + _selectionStartOffsetSec, _mediaContext.getStart() / 1000
				+ _selectionStartOffsetSec + _saveMarkEndTime);
		}
	}

	private void updateTextFromTranscriptWithStartTime(long startTime, long endTime)
	{

			    if (_transcripts == null || _transcripts.size() == 0) {
			        return;
			    }
		
				Log.i(getClass().getName(), String.format("updateTextFromTranscript start: %d, end: %d", startTime, endTime));
		
			    StringBuilder text = new StringBuilder(); 
			    
			    for (RVSTranscriptItem transcript : _transcripts) {
			        
			    	if ((transcript.getStartTime() / 1000) >= startTime) {//
		//	    		1411770706
		//	    		1411770660
			    		long transcriptEndTime = transcript.getEndTime()  / 1000;
		//	    		Log.i(getClass().getName(), String.format("transcript/clip end time: %d - %d", transcriptEndTime, endTime));
			            if (transcriptEndTime  <= endTime) {
			                if (text.length() == 0 || text.length() + transcript.getText().length() <= kMaxCharacters) //only add if first or can add fully
			                {
			                    if (text.length() > 0)
			                    {
			                        text.append(" ");
			                    }
		//	                    
			                    text .append(transcript.getText());
			                }
			            }
			            else
			            {
			                break;
			            }
			        }
			    }
		//	    1411768185
			    String transcriptText = text.toString();
			    if (text.length() > kMaxCharacters) //crop if needed (should only happen with first text)
			    {
			    	transcriptText = transcriptText.substring(0, kMaxCharacters);
			    }
			    
			    setCaptionText(transcriptText);
		//	    [self textViewDidChange:self.textView];
	}

	private void updateTextFromDefaultText()
	{

		_textWasEditedByUser = true;

		if (_defaultText.length() > kMaxCharacters) //crop if needed (should only happen with first text)
		{
			setCaptionText(_defaultText.substring(0, kMaxCharacters));
		}
		else
		{
			setCaptionText(_defaultText);
		}

		textViewDidChange();
	}

	private void textViewDidChange()
	{

		if (_caption.getText().length() == 0)
		{
			_textViewPrompt.setText(R.string.compose_add_comment);
		}
		else
		{
			if (_defaultText != null || _textWasEditedByUser)
			{
				_textViewPrompt.setText(R.string.compose_edit_comment);
			}
			else
			{
				_textViewPrompt.setText(R.string.compose_edit_caption);
			}
		}

		updateCharCount();
	}

	// todo
	private void updateCharCount()
	{
		//	    NSString *cleanText = [self cleanText];
		//	    NSInteger charsLeft = kMaxCharacters - cleanText.length;
		//	    self.textAccView.charsCountLabel.text = [NSString stringWithFormat:@"%li", (long)charsLeft];
		//	    
		//	    if (cleanText.length == 0)
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor lightGrayColor];
		//	    }
		//	    else if (cleanText.length <= kMaxCharacters)
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor darkGrayColor];
		//	    }
		//	    else
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor redColor];
		//	    }
	}

	@SuppressWarnings({ "unused", "unchecked" })
	private void handleOnDone(RVSMediaContext mediaContext)
	{
		long mediaContextStartOffset   = AppManager.getInstance().getMediaContextStartOffsetSec(); 
		if (mediaContextStartOffset != -1) {
			_saveMarkStartTime = _startTime + mediaContextStartOffset * 1000; //for 120 !!!!
			_saveMarkStartTime -= 5000;
			if (_saveMarkStartTime < 0) {
				_saveMarkStartTime = 0;
			}
		} else {
			_saveMarkStartTime = _startTime;// + (_mediaContextDurationInSec - _maxClipDurationInSec) * 1000; //for 120 !!!!
		}
		
		_saveLastResult = mediaContext;
		if (mediaContext == null)
		{
			showErrorDialog();
			return;
		}

		_startTime = mediaContext.getStart();
		_endTime = mediaContext.getEnd();
		_mediaContextDurationInSec = (int)mediaContext.getDuration(); // in seconds
		_maxClipDurationInSec = (int)mediaContext.getMaxPostTime() / 1000;
		
		if (_maxClipDurationInSec > _mediaContextDurationInSec) _maxClipDurationInSec = _mediaContextDurationInSec;
		 
		if (_mediaContext.getStatus().equals(RVSMediaContext.STATUS_OK)) {
			
			//_videoClip.setClipWithMediaContext(_mediaContext, 0, _mediaContext.getDuration());
			_videoClip.setClipWithMediaContext(_mediaContext, _mediaContext.getDuration() / 2, _mediaContext.getDuration() / 2);
//			AppManager.getInstance().setMediaContextStartOffsetSec(_mediaContext.getDuration() / 2);
			_videoClip.setVisibility(View.VISIBLE);
			_videoClip.getLayoutParams().width = _videoImage.getWidth();
			_videoClip.getLayoutParams().height = _videoImage.getHeight();
			_videoClip.requestLayout();
	
			_timelineView.setMaxClipDurationInSec(_maxClipDurationInSec);
			long secPerFrame = _maxClipDurationInSec / TimelineView.NUM_OF_FRAMES_TIMELINE;
			if (secPerFrame < 1) secPerFrame = 1;
			int leftFrames = (int) Math.ceil((_mediaContextDurationInSec - _maxClipDurationInSec) / secPerFrame);
			int totalNumOfFrames = TimelineView.NUM_OF_FRAMES_TIMELINE + leftFrames;
			_timelineView.setNumOfFamesTotal(totalNumOfFrames);
			calculateTimeValues();

			Log.i(getClass().getName(), "total duration (seconds) = " + mediaContext.getDuration() + "start = " + _startTime + "  end = " + _endTime);
			RVSPromise rvsPromise = mediaContext.thumbnailsWithStartOffset(0, mediaContext.getDuration());
			rvsPromise.then(new AndroidDoneCallback<RVSThumbnailList>()
			{
				@Override
				public void onDone(RVSThumbnailList thumbnailList)
				{
					if (thumbnailList.getImages() == null || thumbnailList.getImages().size() == 0)
					{
						showErrorDialog();
					}
					else
					{
						_mediaContextThumbnais = thumbnailList.getImages();
						_timelineView.initFramesGap(_mediaContextDurationInSec);
						_timelineView.initThumbnailImages(thumbnailList);
						loadImages();
						initSeekBar();
//						_timelineView.initImageListScroll();
						
						// auto play
						onVideoImageClick();
						
						setNextMenuEnabled(true);
					}
				}
	
				@Override
				public AndroidExecutionScope getExecutionScope()
				{
					return AndroidExecutionScope.UI;
				}
			});
			
			
		} else {
			Dialog dialog = DialogUtils.createSignleButtonAlertDialog(getActivity(), getString(R.string.compose_unavailable_title),
				getString(R.string.compose_unavailable_message), getString(R.string.ok), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						getActivity().onBackPressed();
					}
				});
			dialog.setCanceledOnTouchOutside(true);
			dialog.setCancelable(false);
			dialog.show();
			
		}
			
	}

	private void showErrorDialog()
	{
		final AlertDialog dialog = DialogUtils.buildShortMessageDialog(getResources().getString(R.string.content_not_available),
				getResources().getString(R.string.sorry), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						getActivity().onBackPressed();
					}
				}, new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						getActivity().onBackPressed();
					}
				}, getActivity());
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
		dialog.show();
		_videoUnavailible = true;
	}

	public void loadVideoPreviewImage(int position)
	{
		try
		{
			_videoClip.setVisibility(View.INVISIBLE);
			_videoPreviewImage.setUseDefaultImage(false);
			_videoPreviewImage.setSampleSize(WhipClipApplication._screenWidth / 4);
			_videoPreviewImage.setLayoutParams(new RelativeLayout.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenWidth * 9 / 16));
			String imagePath = _mediaContextThumbnais.get(position).getAsyncImage().getImageUrlForSize(Consts.IMAGE_POINT);
			_videoPreviewImage.setRemoteURI(imagePath);
			_videoPreviewImage.loadImage();
			_lastClickedThumbnailsLocation = 0;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	

	private void loadImages()
	{
		if (isVisible())
		{
			_timelineView.loadImages();
		}
	}

	@Override
	public void initViews(View FfragmentView)
	{
		_videoPreviewImage = (AsyncImageView) _view.findViewById(R.id.post_image);
		_videoPreviewImage.setSampleSize(WhipClipApplication._screenWidth / 4);

		_timelineView.initViews(_view);
	
		_videoSpinner = (ProgressBar) _view.findViewById(R.id.video_progress_bar);

		_seekbar = (SeekBar) _view.findViewById(R.id.compose_seekbar);
	
		
		
		_videoCaptionText = (TextView) _view.findViewById(R.id.video_caption_text);
		_videoCaptionText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		
		_markingText = (TextView) _view.findViewById(R.id.compose_frame_text);
		_markingText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_markingText.setVisibility(View.INVISIBLE);
		_playPause = (Button) _view.findViewById(R.id.play_pause_button);
		_caption = (EditText) _view.findViewById(R.id.compose_caption);
		_caption.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_caption.addTextChangedListener(this);

		// DIsable "Enter" (new line) presses
		_caption.setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (keyCode == KeyEvent.KEYCODE_ENTER)
				{
					return true;
				}
				return false;
			}
		});

		_view.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
		{
			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom)
			{
				MARKING_TEXT_WIDTH = _markingText.getWidth();
				
			}
		});
		
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inTargetDensity = DisplayMetrics.DENSITY_DEFAULT;
		Bitmap bmp = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_drawer, o);

//		RelativeLayout layout = (RelativeLayout) _view.findViewById(R.id.video_parent);
//		layout.getLayoutParams().height = AppManager.getInstance().convertDpToPixel(202, getActivity());

		_videoImage = (AsyncImageView) _view.findViewById(R.id.post_image);
		_videoClip = (RVSClipView) _view.findViewById(R.id.post_video);
		setVideoClickListeners();

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(MainActivity.LAYOUT_INFLATER_SERVICE);
		_toastLayout = inflater.inflate(R.layout.whiped_toast, (ViewGroup) _view.findViewById(R.id.whipped_layout));
		TextView text = (TextView) _toastLayout.findViewById(R.id.whipped_toast_text);
		text.setTypeface(AppManager.getInstance().getTypeFaceLight());
		_shareToast = new Toast(getActivity());
		_shareToast.setGravity(Gravity.TOP, 0, getActionBar().getHeight());
		_shareToast.setDuration(Toast.LENGTH_SHORT);
		_shareToast.setView(_toastLayout);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try
		{
			if (activity != null) {
				TextView tv = (TextView) activity.findViewById(R.id.action_share);
				if (tv != null) {
					tv.setTypeface(AppManager.getInstance().getTypeFaceRegular());
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public String getActionBarTitle() {
//		return getString(R.string.cancel);
		return getString(R.string.create_clip);
	}


	@Override
	public void initListeners()
	{
		_seekbar.setOnSeekBarChangeListener(this);
	}


	private boolean	pause	= false;

	private void setVideoClickListeners()
	{
		_videoImage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onVideoImageClick();
			}
		});
	}
	
	public void onVideoImageClick() {
		
		_videoClip.setVisibility(View.VISIBLE);
		if (!_resetMediaVideo)
		{
			playOrPauseVideo();
		}
		else if (_mediaContext != null)
		{
			resetMediaContextOnVideo();
		}
	}

	private void resetMediaContextOnVideo()
	{
		_resetMediaVideo = false;
		//		_videoClip.stop();
		_videoClip.setStateChangedListener(new RVSClipViewStateChangedCallback()
		{
			@Override
			public void onDidProgressWithCurrentDuration(RVSClipView clipView, int currentTime, long duration)
			{
				handleDidProgressWithCurrentDuration(clipView, currentTime, duration);
			}

			@Override
			public void onClipViewStateDidChange(RVSClipView clipView)
			{
				Log.e("hadas", "onClipViewStateDidChange");
				if (_videoClip != null && _videoClip.getState() == State.PLAYBACK_ENDED)
				{
					Log.e("hadas", "onClipViewStateDidChange - end of video.");
					_seekbar.setProgress(_seekbar.getMax());
				}
			}

			@Override
			public void onBufferingStateChanged(RVSClipView clipView, boolean isBuffering)
			{
				Log.e("hadas", "is buff = " + isBuffering);
				if (isVisible() == false)
				{
					return;
				}
				showHideSpinner(isBuffering);
			}
		});

		_videoClip.play();
		showPlayPauseButton(R.drawable.play);
	}

	private int			_saveLastProgressPausePlay	= -1;

	private MenuItem	_nextMenu;

	/**
	 * That means - when we're only playing, pausing, scrolling to value.
	 * Once there's a change in the borders (size or position) _resetMediaVideo = true, and we won't enter here. 
	 * !_resetMediaVideo
	 */
	private void playOrPauseVideo()
	{
		if (_videoClip.getState() == State.PLAYING)
		{
			_videoClip.pause(); //don't enable this, then "scrubbing" won't work. 
			_timelineView.setWhiteLineVisible(View.GONE);
			
			showPlayPauseButton(R.drawable.pause);
			pause = true;
		}
		else if (pause || _videoClip.getState() == State.PLAYBACK_ENDED)
		{
			_videoClip.play();
			if (_saveLastProgressPausePlay > 0)
			{ //don't remove this, unless there's an option for scrubbing if the video isn't playing. wasPlayingBeforeScrubbing var changes this.. 
				scrubToValue();
			}
			_saveLastProgressPausePlay = -1;
			_timelineView.setWhiteLineVisible(View.VISIBLE);
			showPlayPauseButton(R.drawable.play);
			pause = false;
		}
	}

	/**
	 * 
	 * @param clipView
	 * @param currentTime - in millisec
	 * @param duration - in seconds
	 */
	private void handleDidProgressWithCurrentDuration(RVSClipView clipView, int currentTime, long duration)
	{
		long progress = 0;
		if (_seekbar.getMax() > 100) {
			progress = (currentTime * _seekbar.getMax()) / duration / 1000;
		} else {
			long playbackPercent = currentTime/10 / duration;
			progress = (_seekbar.getMax()/100 )* playbackPercent;
		}
		
//		int progress = currentTime / 10;//_adjustSize ? currentTime / 1000 : currentTime / 1000 - _saveStartOffset;
//		Log.i("AAAAAA", "handleDidProgressWithCurrentDuration progress = " + progress);
//		Log.i("hadas", "handleDidProgressWithCurrentDuration = " + _saveStartOffset + "_seekSize = " + _seekSize + " currentTime = " + currentTime + " progress = " + progress);
		if (progress >= 0)
		{
			_seekbar.setProgress(/*test +*/(int)progress);
		}
		if (progress >= 0 && progress >= _seekbar.getMax())
		{
			_handler.post(new Runnable()
			{
				@Override
				public void run()
				{
					_timelineView.setWhiteLineVisible(View.GONE);
				}
			});
		}
		if (currentTime != 0 && currentTime != _currentTime)
		{
			showHideSpinner(false);
		}
		if (currentTime > 0)
		{
			_currentTime = currentTime;
		}
	}

	private void showHideSpinner(final boolean buffering)
	{
		_handler.post(new Runnable()
		{
			@Override
			public void run()
			{
				if (buffering)
				{
					_videoSpinner.bringToFront();
					_videoSpinner.setVisibility(View.VISIBLE);
				}
				else if (!buffering && _videoSpinner.getVisibility() == View.VISIBLE)
				{
					_videoSpinner.setVisibility(View.GONE);
				}
			}
		});
	}

	private void scrubToValue()
	{
		if (_videoClip != null && _videoClip.isPlaying())
		{
			_videoClip.beginScrubbing();
			if (_saveLastProgressPausePlay != -1)
			{
				Log.e("hadas", "scrub to value : " + _saveLastProgressPausePlay);
				_videoClip.scrubToValue(_saveLastProgressPausePlay, 0, _seekbar.getMax());
			}
			else
			{
				Log.e("hadas", "scrub to default value : " + _saveLastProgressPausePlay);
				_videoClip.scrubToValue(_seekbar.getProgress(), 0, _seekbar.getMax());
			}
			_videoClip.endScrubbing();
		}
	}
	
//	private int getMediaContextSeekPosition() {
//		if (_mediaContext == null) return 0;
//		
//		int progress = _seekbar.getProgress();
//		int seekPercent = (progress*100)/_seekbar.getMax();
//				
//		_mediaContext.getDuration();
//		
//		return 0;
//	}

	private void showPlayPauseButton(int id)
	{
		if (id != -1)
		{
			_playPause.setBackgroundResource(id);
		}

		if (_playPause != null && ComposeFragment.this.getActivity() != null) {
			_playPause.setAnimation(AnimationUtils.loadAnimation( ComposeFragment.this.getActivity(), R.anim.fade_in));
			_playPause.setVisibility(View.VISIBLE);
			_playPause.bringToFront();
		}

		_handler.postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				if (_playPause != null && ComposeFragment.this.getActivity() != null) { 
					_playPause.setAnimation(AnimationUtils.loadAnimation(ComposeFragment.this.getActivity(), R.anim.fade_out));
					_playPause.setVisibility(View.INVISIBLE);
				}
			}
		}, 400);

	}

	@Override
	public void onPause()
	{
		
		_timelineView.clearPhotoCache();
		if (_videoClip != null && _videoClip.isPlaying() == true)
		{
			_videoClip.pause();
		}
		super.onPause();
	}

	@Override
	public void onDestroy()
	{
		AppManager.getInstance().setComposeMediaContext(null);
		
		_timelineView.clearPhotoCache();
		
		dismissKeyboard();

		AppManager.getInstance().setCurrentMediaContext(null, -1);

		if (_videoClip != null)
		{
			try
			{
				_videoClip.setStateChangedListener(null);
				_videoClip.stop();
				_videoClip.release();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		super.onDestroy();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		((MainActivity) getActivity()).setActionBarTitle(getActivity().getString(R.string.cancel));
//		((MainActivity) getActivity()).onSectionAttached(16);
		AppManager.getInstance().sendToGA(getString(R.string.compose));
	}

	@Override
	public boolean onBackPressed()
	{
		((MainActivity) getActivity()).useNavigation(true);
		AppManager.getInstance().setComposeMediaContext(null);
		  getMainActivity().onBackPressedFromCompose();
          return true;
        		  
//		if (_videoUnavailible)
//		{
//			getFragmentManager().popBackStack();
//		}
//		else
//		{
//			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
//			{
//				@Override
//				public void onClick(DialogInterface dialog, int which)
//				{
//					switch (which)
//					{
//						case DialogInterface.BUTTON_POSITIVE:
//							dialog.dismiss();
//							getFragmentManager().popBackStack();
//							break;
//					}
//				}
//			};
//
//			getFragmentManager().popBackStack();
//
//			//			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//			//			builder.setMessage(getResources().getString(R.string.your_lost)).setTitle(getResources().getString(R.string.are_you_sure))
//			//					.setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
//		}

//		return true;
	}

	/****************************************
	 * 			ActionBar methods			*
	 ****************************************/
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.compose_next, menu);
//		inflater.inflate(R.menu.compose, menu);
		_nextMenu = menu.getItem(0);
		setNextMenuEnabled(false);
		
//		AndroidBug5497Workaround.assistActivity(this);
//		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIEDRESIZE);
	}

	private void setNextMenuEnabled(boolean enable)
	{
		//		if needed, remove <item name="android:actionMenuTextColor">@color/orange</item> in styles file first.
		//		Then, there's a problem that the 2nd time this is called, the text color is white for some reason.
				if (_nextMenu != null)
				{
					_nextMenu.setEnabled(enable);
					SpannableString s = new SpannableString(_nextMenu.getTitle());
					s.setSpan(new ForegroundColorSpan(enable ? Color.WHITE : getResources().getColor(R.color.btn_text_gray)), 0, s.length(), 0);
					_nextMenu.setTitle(s);
				}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		 switch (item.getItemId()) {
//			 case android.R.id.home: 
//				 ((MainActivity) getActivity()).useNavigation(true);
//				 getMainActivity().onBackPressedFromCompose();
//				 return true;
				
			 case R.id.action_next:
				 openComposeCommentFragment();
		         return true;
		         
			 case R.id.action_share:
				 share();
		         return true;

			 default:
		            return super.onOptionsItemSelected(item);
		 }
	}

	private void openComposeCommentFragment()
	{
		RVSThumbnail selectedThumbnail = _mediaContextThumbnais.get(getStartThumbnails());
		
		
//		mThumbnailStartOffest = ,
//				mDuration = ;
//				mThumbnailId = selectedThumbnail.getThumbnailId()
		AppManager.getInstance().setComposeMediaContext(_saveLastResult);
				
				
		Bundle bundle = new Bundle();
		bundle.putLong("thumbnail_offset",  selectedThumbnail.getStartOffset() / 1000);
		bundle.putLong("duration",  getDuration(selectedThumbnail.getStartOffset()) / 1000);
		bundle.putString("thumbnail_id", selectedThumbnail.getThumbnailId());
		
//		if (type == SignUpFragment.SING_UP_UPDATE_USERNAME) {
//			bundle.putString("userName", mUserDetails.getUserName()); 
//		}
		getMainActivity().replaceFragmentContent(ComposeCommentFragment.class, false, R.anim.slide_in_right, R.anim.slide_out_right,
					R.anim.slide_in_right_exit, R.anim.slide_out_right_exit, bundle);
		
	}

	@SuppressWarnings("unchecked")
	private void share()
	{
		try
		{
			if (isCapationEmpty() || isCaptionTooLong())
			{
				return;
			}

			((ImageView) _toastLayout.findViewById(R.id.whipped_toast_image)).setVisibility(View.GONE);
			((TextView) _toastLayout.findViewById(R.id.whipped_toast_text)).setText(getResources().getString(R.string.whipping));
			_shareToast.show();

			RVSThumbnail selectedThumbnail = _mediaContextThumbnais.get(getStartThumbnails());
			long duration = getDuration(selectedThumbnail.getStartOffset());
			Log.i(getClass().getName(), "Share : createPostWithText - " + "selectedThumbnail.getStartOffset() / 1000 = " + selectedThumbnail.getStartOffset()
					/ 1000 + " duration : " + duration + "  selectedThumbnail.getThumbnailId() = " + selectedThumbnail.getThumbnailId());

			RVSPromise postWithTextPromise = AppManager
					.getInstance()
					.getSDK()
					.createPostWithText(_caption.getText() != null ? _caption.getText().toString() : "", _saveLastResult,
							selectedThumbnail.getStartOffset() / 1000, duration / 1000, selectedThumbnail.getThumbnailId());
			postWithTextPromise.then(new DoneCallback<RVSPost>()
			{

				@Override
				public void onDone(final RVSPost postItem)
				{
					getActivity().runOnUiThread(new Runnable()
					{

						@Override
						public void run()
						{
							((ImageView) _toastLayout.findViewById(R.id.whipped_toast_image)).setVisibility(View.VISIBLE);
							((TextView) _toastLayout.findViewById(R.id.whipped_toast_text)).setText(getResources().getString(R.string.whipped_ex));
						}
					});
					_shareToast.show();
					AppManager.getInstance().setRefreshPosts(true);
					getActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							getActivity().onBackPressed();
							getMainActivity().openShareMoreOptionsDialog(true, postItem);
						}
					});
				}
			}, new FailCallback<Object>()
			{
				@Override
				public void onFail(Object result)
				{
					Toast.makeText(getActivity(), "There was an error sharing this clip", Toast.LENGTH_LONG).show();
					AppManager.getInstance().setRefreshPosts(false);
//					getActivity().getFragmentManager().popBackStack();
					
					getActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							getActivity().onBackPressed();
						}
					});
					
				}
			});

		}
		catch (AssertionError e)
		{
			e.printStackTrace();
		}
		catch (Exception e1)
		{
			Toast.makeText(getActivity(), "Share is not ready yet, please try again", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		}

	}

	private boolean isCaptionTooLong()
	{
		String captionText = _caption.getText().toString();
		if (captionText != null && captionText.length() <= kMaxCharacters)
		{
			return false;
		}

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
					case DialogInterface.BUTTON_POSITIVE:
						dialog.dismiss();
						break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.caption_too_long)).setTitle(getResources().getString(R.string.sorry))
				.setPositiveButton("Ok", dialogClickListener).show();

		return true;
	}

	private boolean isCapationEmpty()
	{
		String captionText = _caption.getText().toString();
		if (captionText != null && "".equals(captionText.trim()) == false)
		{
			return false;
		}

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
					case DialogInterface.BUTTON_POSITIVE:
						dialog.dismiss();
						break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.give_caption_first)).setTitle(getResources().getString(R.string.sorry))
				.setPositiveButton("Ok", dialogClickListener).show();

		return true;
	}

	/****************************************
	 * 				Seekbar methods			*
	 ****************************************/
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		if (_videoClip.isPlaying() == false)
		{
			return;
		}
		_timelineView.onSeekBarChanged(seekBar);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
		if (_mediaContextThumbnais == null || _mediaContextThumbnais.size() == 0)
		{
			return;
		}

		if (_videoClip.isPlaying())
		{
			scrubToValue();
		}
		else
		{
			int[] firstVisibleItem = _timelineView.getFirstVisiblePositionWidth();
			int startOffset = firstVisibleItem[0] * _secondsPerFrame * 1000; //adding the frames not visible (firstVisible starts at 0, so if we're in the 1st frame, this will be 0, which is ok.
			startOffset += (firstVisibleItem[1] / _pxPerSecond) * 1000; //adding the frame on the left that's not fully hidden with all its width. 
			int location = getProgressThumbnailsInt(startOffset, seekBar.getProgress());
			loadVideoPreviewImage(location);
			_saveLastProgressPausePlay = _seekbar.getProgress();
		}
	}

	//	private int getOffsetSeconds(long time)
	//	{
	//		try
	//		{
	//			String start1 = _thumbnailsImagesFull.get(0).getThumbnailId();
	//			long start = Long.parseLong(start1);
	//			for (int i = 0; i < _thumbnailsImagesFull.size() - 1; i++)
	//			{
	//				if (start + _thumbnailsImagesFull.get(i).getStartOffset() <= time && start + _thumbnailsImagesFull.get(i + 1).getStartOffset() >= time)
	//				{
	//					if (start + _thumbnailsImagesFull.get(i + 1).getStartOffset() == time)
	//					{
	//						return (int) (_thumbnailsImagesFull.get(i + 1).getStartOffset() / 1000);
	//					}
	//					else
	//					{
	//						return (int) (_thumbnailsImagesFull.get(i).getStartOffset() / 1000);
	//					}
	//				}
	//			}
	//			int size = _thumbnailsImagesFull.size();
	//			start1 = _thumbnailsImagesFull.get(size - 1).getThumbnailId();
	//			start = Long.parseLong(start1);
	//			if (Math.abs(start - time) <= 3000)
	//			{
	//				return (int) (_thumbnailsImagesFull.get(size - 1).getStartOffset() / 1000);
	//			}
	//		}
	//		catch (Exception e)
	//		{
	//			e.printStackTrace();
	//		}
	//
	//		return (int) (_thumbnailsImagesFull.get(0).getStartOffset() / 1000);
	//	}
	//
	//	private long getProgressThumbnailsLong(long offset, int seekBarProgress)
	//	{
	//		int[] vals = getThumbnails(offset, seekBarProgress);
	//		int location = vals[0] + (int) (((double) seekBarProgress / (double) _seekbar.getMax()) * (double) vals[1]);
	//		if (location >= _thumbnailsImagesFull.size())
	//		{
	//			location = _thumbnailsImagesFull.size() - 1;
	//		}
	//		return _thumbnailsImagesFull.get(location).getStartOffset();
	//		//		return saveStartPosition + (int) (((double) seekBarProgress / (double) MAX_PROGRESS) * (double) sizeLeft);
	//	}
	//
	//	private long getScrubValue(long offset, int seekbarProgress)
	//	{
	//		RVSThumbnail selectedThumbnail = _thumbnailsImagesFull.get(getStartThumbnails());
	//		long duration = getDuration(selectedThumbnail.getStartOffset());
	//		Log.i(getClass().getName(), "Share : createPostWithText - " + "selectedThumbnail.getStartOffset() / 1000 = " + selectedThumbnail.getStartOffset()
	//				/ 1000 + " duration : " + duration + "  selectedThumbnail.getThumbnailId() = " + selectedThumbnail.getThumbnailId());
	//		
	//		double seekbarPrecentage = (double) seekbarProgress / (double) _seekbar.getMax();
	//		int addDurationToStart = (int) (duration * seekbarPrecentage);
	//		
	//		return duration + addDurationToStart;
	//	}

	private int getProgressThumbnailsInt(long offset, int seekBarProgress)
	{
		int[] vals = getThumbnails(offset, seekBarProgress);
		return vals[0] + (int) (((double) seekBarProgress / (double) _seekbar.getMax()) * (double) vals[1]);
	}

	private int[] getThumbnails(long offset, int seekBarProgress)
	{
		int saveStartPosition = -1;
		for (int i = 0; i < _mediaContextThumbnais.size() - 1; i++)
		{
			if (_mediaContextThumbnais.get(i).getStartOffset() <= offset && _mediaContextThumbnais.get(i + 1).getStartOffset() >= offset)
			{
				if (_mediaContextThumbnais.get(i + 1).getStartOffset() == offset)
				{
					saveStartPosition = i + 1;
					break;
				}
				else
				{
					saveStartPosition = i;
					break;
				}
			}
		}

		if (offset == 0)
		{
			saveStartPosition = 0;
		}
		else
		{
			saveStartPosition = saveStartPosition == -1 ? _mediaContextThumbnais.size() - 1 : saveStartPosition;
		}

		int sizeLeft = _mediaContextThumbnais.size() - saveStartPosition;
		if (sizeLeft >= _mediaContextThumbnais.size() - 1)
		{
			sizeLeft = _mediaContextThumbnais.size() / 2;
		}
		int[] returnVal = { saveStartPosition, sizeLeft };
		return returnVal;
	}

	public void resetSeekBar()
	{
		_seekbar.setProgress(0);
		_saveLastProgressPausePlay = -1;
		//		scrubToValue();
	}

	/****************************************
	 * 				LayoutMove methods		*
	 ****************************************/


//	private static int	MAX_LEFT_RIGHT					= 100;				//The minimum size of the frame, change this once there's a clear view of the time.



	private int			_lastClickedThumbnailsLocation	= 0;				//We would like to save the last frame the user has clicked in order to present it in the video screen(onTouch of HorizontalListView).

	public void updateLastThumbnailLocation() {
		
//		mComposeFragment.updateMarkingText(_moveMode,(int) event.getX(), event.getX() + "");
//		setLastClickedThumbnailsLocation(_timelineView.getScribLeftX());
		_timelineView.actionMove();
	}

	public void setLastClickedThumbnailsLocation(float xCoordinate)
	{
		if (_mediaContextThumbnais == null || _mediaContextThumbnais.size() == 0)
		{
			Log.e("hadas", "333333333333333333333");
			_lastClickedThumbnailsLocation = 0;
		}
		
		if (_pxPerSecond == 0) _pxPerSecond = 1;
		
		int[] firstVisibleItem = _timelineView.getFirstVisiblePositionWidth();
		int offset = firstVisibleItem[0] * _secondsPerFrame * 1000; //adding the frames not visible (firstVisible starts at 0, so if we're in the 1st frame, this will be 0, which is ok.
		offset += (firstVisibleItem[1] / _pxPerSecond) * 1000; //adding the frame on the left that's not fully hidden with all its width. 

		int saveStartPosition = -1;
		for (int i = 0; i < _mediaContextThumbnais.size() - 1; i++)
		{
			if (_mediaContextThumbnais.get(i).getStartOffset() <= offset && _mediaContextThumbnais.get(i + 1).getStartOffset() >= offset)
			{
				if (_mediaContextThumbnais.get(i + 1).getStartOffset() == offset)
				{
					saveStartPosition = i + 1;
					break;
				}
				else
				{
					saveStartPosition = i;
					break;
				}
			}
		}

		saveStartPosition = saveStartPosition == -1 ? _mediaContextThumbnais.size() - 1 : saveStartPosition;
		int sizeLeft = _mediaContextThumbnais.size() - saveStartPosition;
		int seekBarProgress = (int) ((xCoordinate / (float) WhipClipApplication._screenWidth) * 100);

		_lastClickedThumbnailsLocation = saveStartPosition + (int) (((double) seekBarProgress / (double) _seekbar.getMax()) * (double) sizeLeft);
		if (_lastClickedThumbnailsLocation >= _mediaContextThumbnais.size())
		{
			_lastClickedThumbnailsLocation = _mediaContextThumbnais.size() - 1;
		}

		//		updateVideoClipMediaContext();

	}

	public void updateVideoClipMediaContext()
	{
		Log.i(getClass().getName(),
				String.format("updataMediaContext startOffset %d, saveEndOffset %d, end %d", _saveStartOffset, _saveEndOffset, _saveEndOffset
						- _saveStartOffset));
		_videoClip.setClipWithMediaContext(_mediaContext, _saveStartOffset, _saveEndOffset - _saveStartOffset);
	}

	public int getLastClickedThumbnailsLocation()
	{
		return _lastClickedThumbnailsLocation;
	}
	
	public void setLastClickedThumbnailsLocation(int lastClickedThumbnailsLocation)
	{
		_lastClickedThumbnailsLocation = lastClickedThumbnailsLocation;
	}

	/**
	 * Calculating each time the frame is moving the corresponding time.
	 * For left border, we're calculating how many frames not visible to us = firstVisibleItem[0], then multiplying it with the number of seconds per frame.
	 * Later on, we'll take the element that's partly visible, and see how much is NOT visible from it's left = firstVisibleItem[1], then dividing it with the 
	 * pixels per seconds variable. 
	 * At last, we'll combine those 2, and take the _layoutLeftMargin - MARGINS_LIMIT as the number of pixels there is, since the 
	 * start of the time begins as MARGINS_LIMIT and NOT at 0. 
	 * 
	 * *** Same calculations applies for the right border, but instead of getting the first visible item, we'll get the last visible item
	 * (meaning - the element that's on the far right).
	 * So, we're calculating here the number of seconds from the right, we're not able to see, and subtracting it from the main _endTime.
	 */
	public void updateMarkingText(MoveMode moveMode, int position, String value)
	{
		if (_mediaContextThumbnais == null || _mediaContextThumbnais.size() == 0)
		{
			Log.e("hadas", "22222222222222222222222222222222222");
			return;
		}
		if (value.equals(""))
		{
			_markingText.setText(null);
			_markingText.setVisibility(View.INVISIBLE);
		}
		else
		{
			_markingText.setVisibility(View.VISIBLE);
			if (moveMode == MoveMode.RIGHT_BOX_DRAG)
			{
				int t = WhipClipApplication._screenWidth - _timelineView.getLayoutRightMargin() - MARKING_TEXT_WIDTH / 2;
				if (t + MARKING_TEXT_WIDTH > WhipClipApplication._screenWidth)
				{
					_markingText.setX(Math.min(WhipClipApplication._screenWidth, position - MARKING_TEXT_WIDTH / 2));
				}
				else
				{
					_markingText.setX(t);
				}
				//				_markingText.setX(Math.min(WhipClipApplication._screenWidth, ));
				//				_markingText.setX(position - MARKING_TEXT_WIDTH / 2);
			}
			else
			{
				_markingText.setX(Math.max(0, _timelineView.getLayoutLeftMargin() - MARKING_TEXT_WIDTH / 2));
			}
			//			_markingText.setX(position - (_moveMode == MoveMode.RIGHT_BOX_DRAG ? MARKING_TEXT_WIDTH / 2 : 0));
		}

		int[] lastVisibleItem = _timelineView.getLastVisiblePositionWidth();
		int endOffset = (_timelineView.getComposeAdapter().getCount() - 1 - lastVisibleItem[0]) * _secondsPerFrame * 1000;
		endOffset += (lastVisibleItem[1] / _pxPerSecond) * 1000;
		long secondsRight = _endTime - endOffset - ((_timelineView.getLayoutRightMargin() - _timelineView.getMarginLimit()) / _pxPerSecond) * 1000;
		_saveMarkEndTime = secondsRight;
		if (moveMode == MoveMode.RIGHT_BOX_DRAG)
		{
			_markingText.setText(DateUtils.getLocalizedTime(secondsRight));
		}

		int[] firstVisibleItem = _timelineView.getFirstVisiblePositionWidth();
		int startOffset = firstVisibleItem[0] * _secondsPerFrame * 1000; //adding the frames not visible (firstVisible starts at 0, so if we're in the 1st frame, this will be 0, which is ok.
		startOffset += (firstVisibleItem[1] / _pxPerSecond) * 1000; //adding the frame on the left that's not fully hidden with all its width. 
		long secondsLeft = _startTime + startOffset + ((_timelineView.getLayoutLeftMargin() - _timelineView.getMarginLimit()) / _pxPerSecond) * 1000;
		_saveMarkStartTime = secondsLeft;
		if (moveMode == MoveMode.LEFT_BOX_DRAG)
		{
			_markingText.setText(DateUtils.getLocalizedTime(secondsLeft));
		}

	}

	public void updateVideoImage(MoveMode mode)
	{
		if (MoveMode.LEFT_BOX_DRAG.equals(mode))
		{
			if (_mediaContextThumbnais != null) {
				loadVideoPreviewImage(getStartThumbnails());
				long offset = _saveMarkStartTime - _startTime;
			}
			updateTextFromTranscriptBySelection();
//			_videoClip.play()
//			play

		}
		else if (MoveMode.RIGHT_BOX_DRAG.equals(mode))
		{
			loadVideoPreviewImage(getEndThumbnails());
		}
	}

	private int 	_maxClipDurationInSec 	= 0;
	private int		_mediaContextDurationInSec		= 0;	//keep this seconds !! NOT milliseconds
	private int		_pxPerSecond		= 0;	//meaning : _pxPerSecond pixel = 1 second.
	private int		_numberOfFrames		= 0;	//total number of frames ->
	private int		_secondsPerFrame	= 0;	//meaning : _secondsPerFrame seconds = 1 frame.
	private long	_saveMarkStartTime	= 0;
	private long	_saveMarkEndTime	= 0;
	private int		_seekSize			= 0;
	private int		_saveStartOffset	= 0;
	private int		_saveEndOffset		= 0;

	private void calculateTimeValues()
	{
		//TODO 120 !!
//		_totalSeconds = (int) (_endTime - _startTime) / 1000;
		Log.i(getClass().getName(), "start time : " + DateUtils.getLocalizedTime(_startTime) + " end time : " + DateUtils.getLocalizedTime(_endTime));
		_pxPerSecond = _timelineView.getPxPerSeconds();
		_numberOfFrames = _timelineView.getNumOfFramesTotal();
		
		_secondsPerFrame = _mediaContextDurationInSec / _numberOfFrames;
		
		//User can't cut a video shorter than Consts.MINUMUM_CUT_VIDEO_SECONDS = 5 seconds.
		_timelineView.setMaxLeftRight(_pxPerSecond * Consts.MINUMUM_CUT_VIDEO_SECONDS);
		
		Log.i("hadas", " MARGINS_LIMIT = " + _timelineView.getMarginLimit());
		_saveMarkEndTime = _endTime;
		
//		long mediaContextStartOffset   = AppManager.getInstance().getMediaContextStartOffsetSec(); // millisec to sec
//		if (mediaContextStartOffset != -1) {
//			_saveMarkStartTime = _startTime + mediaContextStartOffset * 1000; //for 120 !!!!
//		} else {
//			_saveMarkStartTime = _startTime + (_mediaContextDurationInSec - _maxClipDurationInSec) * 1000; //for 120 !!!!
//		}
		
		
		
//		_saveMarkStartTime = _startTime + _mediaContextDurationInSec / 2 * 1000; //for 120 !!!!
	}

	public void initSeekBar()
	{
		int start = getStartThumbnails();
		int end = getEndThumbnails();
		_saveStartOffset = (int) _mediaContextThumbnais.get(start).getStartOffset() / 1000;
		_saveEndOffset = (int) _mediaContextThumbnais.get(end).getStartOffset() / 1000;
//		_seekSize = _saveEndOffset - _saveStartOffset;
//		_seekbar.setMax(_seekSize * 100);
		Log.e("hadas", "max on seek : " + _seekSize + /*" start = " + start + " end = " + end + */" _thumbnailsImagesFull.get(start).getStartOffset() = "
				+ _mediaContextThumbnais.get(start).getStartOffset() + "  _thumbnailsImagesFull.get(end).getStartOffset() = "
				+ _mediaContextThumbnais.get(end).getStartOffset());

		_resetMediaVideo = true;
		_videoClip.setStateChangedListener(null);
		_videoClip.stop();
		_videoSpinner.setVisibility(View.GONE);
		
	}

	/**
	 * Calculating the start according to the user's selection. 
	 * We have to iterate the whole array since we can't relay that the items will be organized as 0, 2000, 4000, ... 118,000 etc. 
	 * It can be [0,2000,4000, ... 118000] or [1000, 3000,4000,6000...] etc....
	 * @return int representing the start index thumbnail item to be sent to createPostWithText
	 */
	public int getStartThumbnails()
	{
		long offset = _saveMarkStartTime - _startTime;
		_selectionStartOffsetSec = offset / 1000;
		if (_mediaContextThumbnais != null) {
			for (int i = 0; i < _mediaContextThumbnais.size() - 1; i++)
			{
				if (_mediaContextThumbnais.get(i).getStartOffset() <= offset && _mediaContextThumbnais.get(i + 1).getStartOffset() >= offset)
				{
					if (_mediaContextThumbnais.get(i + 1).getStartOffset() == offset)
					{
						return i + 1;
					}
					else
					{
						return i;
					}
				}
			}
		}

		//		return _thumbnailsImagesFull.size() - 1;
		return 0;
	}

	/**
	 * Calculating the end according to the user's selection. 
	 * We have to iterate the whole array since we can't relay that the items will be organized as 0, 2000, 4000, ... 118,000 etc. 
	 * It can be [0,2000,4000, ... 118000] or [1000, 3000,4000,6000...] etc....
	 * @return int representing the end index thumbnail item to be sent to createPostWithText
	 */
	private int getEndThumbnails()
	{
		long offset = _mediaContextThumbnais.get(_mediaContextThumbnais.size() - 1).getStartOffset() - _endTime + _saveMarkEndTime;

		for (int i = _mediaContextThumbnais.size() - 1; i > 0; i--)
		{
			if (_mediaContextThumbnais.get(i).getStartOffset() >= offset && _mediaContextThumbnais.get(i - 1).getStartOffset() <= offset)
			{
				return i;
			}
		}
		Log.e(getClass().getName(), "We shouldn't reach here !!!!!!!!!!!!");
		return 0;
	}

	/**
	 * Calculating the duration of the clip marked by the user. 
	 * First, we're calculating the offset from the right - aka the offset to the end of the clip.
	 * Then, we're going over the thumbnailImages to find the exact offset.
	 * Again, as getSelectedThumbnails function, we can't relay on the items to be organized with fixed offset difference. 
	 * It can be [0,2000,4000, ... 118000] or [1000, 3000,4000,6000...] etc....
	 * @param startOffset - the users's start marking (as offset). this is basically getSelectedThumbnails().getStartOffset().
	 * @return the duration of the clip selected by the user.
	 */
	private long getDuration(long startOffset)
	{
		return _mediaContextThumbnais.get(getEndThumbnails()).getStartOffset() - startOffset;
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onDone(Object result)
	{
		Log.e(getClass().getSimpleName(), "on done");
	}

	public void setVideoSpinnerVisible(int visibility)
	{
		_videoSpinner.setVisibility(visibility);
	}

	public void videoClipPause()
	{
		_videoClip.pause();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable s)
	{
		// TODO Auto-generated method stub
		
	}

}
