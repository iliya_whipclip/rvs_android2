package com.whipclip.fragments;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;

/**
 * A placeholder fragment containing a simple view.
 */
public class LikedPostsFragment extends PostsFragment
{
	private String	_userId;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (getArguments() != null)
		{
			_userId = getArguments().getString(Consts.EXTRA_USER_ID);
		}
	}

	@SuppressWarnings("unchecked")
	protected void getPosts(int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
				//				showProgressDialog();
			}
			if (_dataAsync == null)
			{
				_dataAsync = AppManager.getInstance().getSDK().likedPostsByUser(_userId);
			}
			if (_promise != null && _promise.isPending())
			{
				return;
			}
			try
			{
				_promise = _dataAsync.next(number);
			}
			catch (Throwable e)
			{
				_promise = null;
				_swipeLayout.setRefreshing(false);
				e.printStackTrace();
			}
			if (_promise != null)
			{
				_promise.then(this, this);
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onDone(List<RVSPost> arg0)
	{
		super.onDone(arg0);
		if (isVisible())
		{
			_handler.post(new Runnable()
			{
				@Override
				public void run()
				{
					setEmptyView(getString(R.string.no_likes));
				}
			});
		}
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
//		int num = _list == null ? 12 : _list.size();
//		_list = null;
//		_dataAsync = null;
//		_postsAdapter = null;
//		getPosts(num);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		((MainActivity) getActivity()).onSectionAttached(18);
		AppManager.getInstance().sendToGA(getString(R.string.likes));
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(18);
	}

	
	@Override
	public String getActionBarTitle() {
		
		return getString(R.string.likes);
	}
}