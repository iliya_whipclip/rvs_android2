package com.whipclip.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends BaseFragment<Object>
{
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String	ARG_SECTION_NUMBER	= "section_number";

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static PlaceholderFragment newInstance(int sectionNumber)
	{
		PlaceholderFragment fragment = new PlaceholderFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public PlaceholderFragment()
	{
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		return rootView;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
//		((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
	}

	@Override
	public void initViews(View FfragmentView)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDone(Object result)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onProgress(Integer progress)
	{
		// TODO Auto-generated method stub

	}
}