package com.whipclip.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;

public class TosFragment extends BaseFragment<Object>
{
	View	_view;
	private WebView	webView;

	public TosFragment()
	{
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		((MainActivity) getActivity()).useNavigation(false);
		_view = inflater.inflate(R.layout.fragment_privacy, container, false);
		webView = (WebView) _view.findViewById(R.id.webView);
		String url = getResources().getString(R.string.term_of_service_url);
		webView.loadUrl(url);

		return _view;
	}

	
	public void onDone(Object result)
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public boolean onBackPressed()
	{
		((MainActivity) getActivity()).useNavigation(true);
		  getMainActivity().onBackPressedFromCompose();
          return true;
          
	}
	
//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
//	{
//		super.onCreateOptionsMenu(menu, inflater);
////		menu.clear();
////		inflater.inflate(R.menu.compose, menu);
////		_share = menu.getItem(0);
////		setShareEnabled(false);
//	}
//	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item)
//	{
//		 switch (item.getItemId()) {
//			 case android.R.id.home: 
//				 ((MainActivity) getActivity()).useNavigation(true);
//				 getMainActivity().onBackPressedFromCompose();
//				 return true;
//
//			 default:
//		            return super.onOptionsItemSelected(item);
//		 }
//	}

	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	
			((MainActivity) getActivity()).setActionBarTitle(getActivity().getString(R.string.term_of_service));
			((MainActivity) getActivity()).useNavigation(false);
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
//		
//		((MainActivity) getActivity()).setActionBarTitle(getActivity().getString(R.string.term_of_service));
//		((MainActivity) getActivity()).useNavigation(false);
	}

	@Override
	public void initViews(View FfragmentView)
	{
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
	}
	
	public String getActionBarTitle()
	{
		return getResources().getString(R.string.term_of_service);
	}


}
