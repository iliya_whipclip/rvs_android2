package com.whipclip.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.ui.adapters.PostsListAdapter;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;

public class SinglePostFragment extends BasePostsFragment<RVSPost>
{
	private TextView	_postNotFound;

	private String		_postId;

	@Override
	public void onResume()
	{
		super.onResume();
		((MainActivity) getActivity()).setActivityTitle(getActivity().getString(R.string.whip));
		getPosts(1);
	}

	@Override
	public String getActionBarTitle() {
		
		return getActivity().getString(R.string.whip);
	}
	
	@Override
	public void onDone(final RVSPost result)
	{
		_swipeLayout.setRefreshing(false);
		if (result != null)
		{
			try
			{
				getActivity().runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						convertToUIItems(result);
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home: 
            getMainActivity().onBackPressed();
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    } 
	
	@Override
	protected void convertToUIItems(RVSPost data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		_list.clear();
		FullPostView fullPostView = new FullPostView(getActivity(), data, false);
		fullPostView.setRemoveViewBottom(true);
		_list.add(fullPostView);
		_postsAdapter = new PostsListAdapter(SinglePostFragment.this.getActivity(), _list, this);
		_listView.setAdapter(_postsAdapter);
	}

	@Override
	public void onFail(Throwable result)
	{
		Log.e(getClass().getName(), "An error was caught from the SDK" + result != null ? result.getMessage() : "");
		_swipeLayout.setRefreshing(false);
		_postNotFound.setVisibility(View.VISIBLE);
		_listView.setVisibility(View.GONE);

		getMainActivity().noNetworkDialog();
	}

	@Override
	public void initViews(View FfragmentView)
	{
		_postNotFound = (TextView) _fragmentView.findViewById(R.id.single_post_not_found);
		_postNotFound.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_postNotFound.setVisibility(View.GONE);
	}

	@Override
	public void initListeners()
	{
	}

	@Override
	protected void getPosts(int number)
	{
		if (_postId != null)
		{
			RVSPromise promiss = AppManager.getInstance().getSDK().postForPostId(_postId);
			promiss.then(this);
		}
		else
		{
			Bundle bundle = getArguments();
			if (bundle != null)
			{
				_postId = bundle.getString(Consts.BUNDLE_LATESTS_POST_ID, null);
				if (_postId == null)
				{
					_postNotFound.setVisibility(View.VISIBLE);
					_listView.setVisibility(View.GONE);
				}
				else
				{
					_postNotFound.setVisibility(View.GONE);
					_swipeLayout.setRefreshing(true);
					RVSPromise promiss = AppManager.getInstance().getSDK().postForPostId(_postId);
					promiss.then(this);
				}
			}
		}
	}

	@Override
	public int getLayoutResourceID()
	{
		return R.layout.fragment_single_post;
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
		_list = null;
		_postsAdapter = null;
		getPosts(1);
	}

}
