package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.ui.adapters.PostsListAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostsFragment extends BasePostsFragment<List<RVSPost>>
{

	@SuppressWarnings("unchecked")
	protected void getPosts(int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && _swipeLayout != null && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
			}
			if (_dataAsync == null)
			{
				_dataAsync = AppManager.getInstance().getSDK().trendingPosts();
			}
			if (_promise != null && _promise.isPending())
			{
				return;
			}
			try
			{
				if (!_dataAsync.getDidReachEnd()) {
					_promise = _dataAsync.next(number);
				}
			}
			catch (Throwable e)
			{
				_promise = null;
				_swipeLayout.setRefreshing(false);
				e.printStackTrace();
			}
			if (_promise != null)
			{
				_promise.then(this, this);
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	protected void loadMorePostResults(int totalItemCount)
	{
		if (!_dataAsync.getDidReachEnd())
		{
			_progressBar.setVisibility(View.VISIBLE);
			getPosts(5);
		}
	}

	@Override
	public void onRefresh()
	{
		super.onRefresh();
//		int num = _list == null ? 12 : _list.size();
		_list = null;
		_dataAsync = null;
		_postsAdapter = null;
		getPosts(12);
	}

	@Override
	public void onResume()
	{ 
		super.onResume();
		
//		if (((MainActivity) getActivity()).isMyTabOpened(TabFragmentAdapter.TAB_POSITION_POPULAR)) {
		
			((MainActivity) getActivity()).onSectionAttached(0);
			getPosts(12);
			AppManager.getInstance().sendToGA(getString(R.string.trending));
//		}
	}
	
	@Override
	public String getActionBarTitle() {
		return getString(R.string.tab_title_popular);
	}

	@Override
	public void initViews(View FfragmentView)
	{
		
	}

	@Override
	public void initListeners()
	{

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		//		((MainActivity) getActivity()).restoreActionBar();
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.feeds, menu);
	}

	@Override
	public void onDone(List<RVSPost> arg0)
	{
		_swipeLayout.setRefreshing(false);
		if (!isVisible())
		{
			return;
		}
		_progressBar.setVisibility(View.GONE);
		dismissProgressDialog();
		Log.i(getClass().getSimpleName(), "onDone: " + arg0.size());
		if (arg0 != null)
		{
			convertToUIItems(arg0);

			if (_postsAdapter == null)
			{
				_postsAdapter = new PostsListAdapter(PostsFragment.this.getActivity(), _list, this);
				_listView.setAdapter(_postsAdapter);
			}
			else
			{
				_postsAdapter.insertElementsToRealtimePost(_list);
				_postsAdapter.notifyDataSetChanged();
			}
		}
		_loadingMore = false;
	}

	@Override
	public void onDestroy()
	{
		if (_promise != null)
		{
			_promise.cancel();
		}
		super.onDestroy();
	}

	@Override
	public void onFail(Throwable arg0)
	{
		_swipeLayout.setRefreshing(false);
		_progressBar.setVisibility(View.GONE);
		_loadingMore = false;
		dismissProgressDialog();
		Log.w(getClass().getSimpleName(), "onFail: " + arg0);
//		if (_list != null)
//		{
//			_list.clear();
//		}
		
		try {
			getMainActivity().noNetworkDialog();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Override
	public int getLayoutResourceID()
	{
		return R.layout.posts_fragment_layout;
	}

	@Override
	protected void convertToUIItems(List<RVSPost> data)
	{
		if (_list == null)
		{
			_list = new ArrayList<PostUIItem>();
		}
		for (RVSPost post : data)
		{			
			_list.add(new FullPostView(getActivity(), post, false));
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			PendingOperation op = getPendingOperation();
			if (op != null)
			{
				executePendingOperation(op);
			}
		}
	}

	private void executePendingOperation(PendingOperation op)
	{
		switch (op.getType())
		{
			case LIKE:
			{
				if ((Boolean) op.getData2())
				{
					AppManager.getInstance().getSDK().createLikeForPost((String) op.getData1());
				}
				else
				{
					AppManager.getInstance().getSDK().deleteLikeForPost((String) op.getData1());
				}
				refreshAfterSuccessLogin(op);
				break;
			}
			case WHIP:
			{
				if ((Boolean) op.getData2())
				{
					AppManager.getInstance().getSDK().createRepostForPost((String) op.getData1());
				}
				else
				{
					AppManager.getInstance().getSDK().deleteRepostForPost((String) op.getData1());
				}
				refreshAfterSuccessLogin(op);
				break;
			}
			case COMMENT:
			{
				openComments((String) op.getData1(), (Integer) op.getData2());
				break;
			}
			case NOTIFICATIONS:
			{
				getMainActivity().replaceFragmentContent(NotificationsFragment.class, null);
				break;
			}
			default:
				break;
		}
	}

	private void openComments(String postId, int numberOfComments)
	{
		Bundle bundle = new Bundle();
		bundle.putString(Consts.COMMENTS_POST_ID, postId);
		bundle.putInt(Consts.TAB_POSITION, getCurrentTabPosition());
		bundle.putInt(Consts.COMMENTS_NUM_ITEMS, numberOfComments);
//		getMainActivity().replaceFragmentContent(CommentFragment.class, bundle);
//		getMainActivity().replaceTabFragmentContent(TabFragmentAdapter.TAB_POSITION_POPULAR, CommentFragment.class, bundle);
		replaceTabFragmentContent(CommentFragment.class, bundle);

	}

	private void refreshAfterSuccessLogin(PendingOperation op)
	{
		// TODO Iliya check
		_postsAdapter.notifyDataSetChanged();

//		for (int i = 0; i < _list.size(); i++)
//		{ //TODO - This is wrong... find a better way !! maybe call onRefresh() instead. 
//			PostUIItem item = _list.get(i);
//			if (item instanceof FullPostView && ((FullPostView) item).setSelected(op.getType(), (String) op.getData1(), (Boolean) op.getData2()))
//			{
//				break;
//			}
//		}

		onRefresh();
	}
	
}