package com.whipclip.fragments;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.CustomSwipeRefreshLayout;
import android.support.v4.widget.CustomSwipeRefreshLayout.OffsetLisetener;
import android.support.v4.widget.CustomSwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.WCNotificationManager;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.ClipTvListAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.items.searchitems.ClipTvUIItem;

public class TVShowsTabFragment extends BaseFragment<Object> implements OnClickListener, IDrawableCache, OffsetLisetener, OnRefreshListener {

	private static final int VIEW_TYPE_NONE 	= -1;
	private static final int VIEW_TYPE_POPULAR 	= 0;
	private static final int VIEW_TYPE_A_Z 		= 1;
	
	protected ProgressBar			mProgressBar;

	private Button	mPopularBtn;
	private Button	mAzBtn;
	
	protected RVSAsyncList				mTrendingDataAsync;
	protected RVSAsyncList				mShowsDataAsync;

	protected ClipTvListAdapter			mTrendingAdapter;
	protected ClipTvListAdapter			mShowsAdapter;
	
	protected ArrayList<ClipTvUIItem>	mTrendingClipTvUIItemsList;
	protected ArrayList<ClipTvUIItem>	mShowsUiItemsList;
	
	protected boolean					mLoadingMore	= false;

	protected ListView					mListView;
	private CustomSwipeRefreshLayout	mSwipeLayout;
	
	private int							mViewType = VIEW_TYPE_NONE;
	private View						mFragmentView;
	
	 public TVShowsTabFragment(){
			
			super();
			setCurrentTabPosition(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
	 }
	
    public TVShowsTabFragment(int tabPosition){
		
		super();
		setCurrentTabPosition(tabPosition);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		if (mFragmentView == null) {
			mFragmentView = inflater.inflate(R.layout.fragment_tab_tv_shows, container, false);
	
//			if (mImagesCache == null){
//				mImagesCache = new DrawableCache();
//			}
	        mListView = (ListView) mFragmentView.findViewById(R.id.shows_list);
	        
	    	mSwipeLayout = (CustomSwipeRefreshLayout) mFragmentView.findViewById(R.id.swipe_container);
	    	if (mSwipeLayout != null) {
	    		mSwipeLayout.setOffsetListener(this);
	    		mSwipeLayout.setOnRefreshListener(this);
	    		mSwipeLayout.setColorScheme(android.R.color.holo_orange_light, android.R.color.holo_red_light, android.R.color.holo_green_light,
	    				android.R.color.holo_blue_bright);
	    	}
			
			mListView.setOnScrollListener(new OnScrollListener() {
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {}
	
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
					if (totalItemCount != 0 && mLoadingMore == false && firstVisibleItem + visibleItemCount == totalItemCount) {
						mLoadingMore = true;
						loadMorePostResults(totalItemCount);
					}
	
				}
	
			});
			
			
			
			
	//		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	//			@Override
	//		    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {
	//				ClipTvUIItem item = (ClipTvUIItem) adapter.getItemAtPosition(position);
	//				openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
	//		    // TODO Auto-generated method stub
	////		    TextView v=(TextView) view.findViewById(R.id.txtLstItem);
	////		    Toast.makeText(getApplicationContext(), "selected Item Name is "+v.getText(), Toast.LENGTH_LONG).show();
	//		    }
	//		});
			
			
	        mProgressBar = (ProgressBar) mFragmentView.findViewById(R.id.shows_progressbar);
	        
	        mPopularBtn = (Button)mFragmentView.findViewById(R.id.left_btn);
	        mAzBtn = (Button)mFragmentView.findViewById(R.id.right_btn);
	        
	        
	        mAzBtn.setOnClickListener(this);
	        mPopularBtn.setOnClickListener(this);
	
	        mPopularBtn.setTypeface(AppManager.getInstance().getTypeFaceMedium());
	        mAzBtn.setTypeface(AppManager.getInstance().getTypeFaceMedium());
	        
	        
			mTrendingClipTvUIItemsList = new ArrayList<ClipTvUIItem>();
			mShowsUiItemsList = new ArrayList<ClipTvUIItem>();
			
//			BaseFragment b =  (BaseFragment) this;
			mTrendingAdapter = new ClipTvListAdapter(getActivity(), mTrendingClipTvUIItemsList, this);
//			mTrendingAdapter = new ClipTvListAdapter((Context)getActivity() , mTrendingClipTvUIItemsList, (BaseFragment)this);
			mShowsAdapter = new ClipTvListAdapter(getActivity(), mShowsUiItemsList,(BaseFragment) this);
	
			mTrendingDataAsync = AppManager.getInstance().getSDK().trendingShows();
			mShowsDataAsync = AppManager.getInstance().getSDK().getShows();
	        
	//        onFilterButtonClick(true);
	
	        
	//        mAdapter = new TabTvShowsFragmentAdapter(getChildFragmentManager());
	//
	//		mPager = (ViewPager)root.findViewById(R.id.pager_tv_shows);
	//	    mPager.setAdapter(mAdapter);
	//	    mPager.setOffscreenPageLimit(5); //  predefine number of "cached" pages
	//	
	//	    mIndicator = (TabPageIndicator)root.findViewById(R.id.indicator_tv_shows);
	//	    mIndicator.setTabIconLocation (TabPageIndicator.LOCATION_UP);
	//	    mIndicator.setTypeFace(AppManager.getInstance().getTypeFaceLight());
	//	    mIndicator.setViewPager(mPager);
			
		} else if (mFragmentView.getParent() != null) {
			((ViewGroup) mFragmentView.getParent()).removeView(mFragmentView);
		}
		
//		validateLaunchIntent(false);
		
		onTabSelected();
        
        return mFragmentView;
    }

//	private boolean validateLaunchIntent(boolean calledFromTabSelected)
//	{
//		String intentUri = getMainActivity().getLaunchIntetUri();
//		if (intentUri != null ) {
//			try
//			{
//				Intent intent = Intent.parseUri(intentUri, 0);
//				if (intent.getAction().equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY)) {
//					
//					getMainActivity().showShowDetailsPageByLaunchIntent();
//					return true;
////					if (!calledFromTabSelected) {
////						onTabSelected();
////					}
//				}
//			}
//			catch (URISyntaxException e)
//			{
//				e.printStackTrace();
//			}
//		}
//		
//		return false;
//		
//	}

	@Override
	public void initViews(View FfragmentView)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v)
	{
		if (mPopularBtn.equals(v)) {
			onFilterButtonClick(true);
		} else if (mAzBtn.equals(v)) {
			onFilterButtonClick(false);
		}
	}
	
	private void onFilterButtonClick(boolean isPopular) {
		
		int viewType;
		if (isPopular) {
			viewType = VIEW_TYPE_POPULAR;
		} else {
			viewType = VIEW_TYPE_A_Z;
		}
		
		
		if (viewType == mViewType) {

			ArrayList<ClipTvUIItem> uiItems =  isPopular ? mTrendingClipTvUIItemsList : mShowsUiItemsList;
			if (uiItems.size() == 0) {
				getShows(12);
			}
			return;
		}
		
		mViewType = viewType;
		
		mPopularBtn.setSelected(isPopular);
		mPopularBtn.setTextColor(isPopular ? getResources().getColor(R.color.btn_text_orange) :  getResources().getColor(R.color.btn_text_gray));
//		mPopularBtn.setTextColor(isPopular ? getResources().getColor(R.color.orange) : Color.GRAY);
//        mPopularBtn.setEnabled(!isPopular);
        
        
        mAzBtn.setSelected(!isPopular);
        
        mAzBtn.setTextColor(!isPopular ?  getResources().getColor(R.color.btn_text_orange) : getResources().getColor(R.color.btn_text_gray));
//        mAzBtn.setTextColor(!isPopular ? getResources().getColor(R.color.orange) : Color.GRAY);
//        mAzBtn.setEnabled(isPopular);
        
//        updateList();
        
//        sort(isPopular);
        

        if (_promise != null && _promise.isPending()){
			_promise.cancel();
			_promise = null;
		}
		mLoadingMore = false;
		
		ClipTvListAdapter adapter =  isPopular ? mTrendingAdapter : mShowsAdapter;
		mListView.setAdapter(adapter);
		
		ArrayList<ClipTvUIItem> uiItems =  isPopular ? mTrendingClipTvUIItemsList : mShowsUiItemsList;
		if (uiItems.size() == 0) {
			getShows(12);
		}
//		else
//		{
//			adapter.notifyDataSetChanged();
//		}
	}
	
//	@Override
//	public void setUserVisibleHint(boolean isVisibleToUser) {
//	    super.setUserVisibleHint(isVisibleToUser);
//	    if (isVisibleToUser) {
//	    	((MainActivity) getActivity()).onSectionAttached(18);
//			// todo workaround, check when after notification, pressing back, no shows
//	    	if (isAdded() && mTrendingAdapter.getCount() == 0) {
//	    		onTabSelected();
//	    	}
//	    }
//	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		((MainActivity) getActivity()).onSectionAttached(18);
//		// todo workaround, check when after notification, pressing back, no shows
//		onTabSelected();
//		
//		validateLaunchIntent();
//		
//		showPages(12);
//		if (!mIsTabWasSelected) {
//			
//			mIsTabWasSelected = true;
//			onFilterButtonClick(true);
//		}
//			getShows(12);
			// TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
			
//		if (((MainActivity) getActivity()).isMyTabOpened(TabFragmentAdapter.TAB_POSITION_TV_SHOWS)) {
//		if (((MainActivity) getActivity()).getCurrentTabFragment() instanceof TVShowsTabFragment) {
//			getShows(12);
			// TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
	}
	

	@Override
	public void onTabReselected() {
		super.onTabReselected();
		
		boolean isPopular = (mViewType == VIEW_TYPE_POPULAR);
		ClipTvListAdapter adapter =  isPopular ? mTrendingAdapter : mShowsAdapter;
		
		if (mListView != null && adapter.getCount() > 0) {
//			getMainActivity().popBackStack(getChildFragmentManager());
			mListView.smoothScrollToPosition(0);	
		} else {
			onTabSelected();
		}
		
	}
	
	@Override
	public void onTabSelected() {
		super.onTabSelected();
		
		// TODO add refresh timeout
//		if (!mIsTabWasSelected) {
			
			mIsTabWasSelected = true;
			onFilterButtonClick(true);
			
//			if (!validateLaunchIntent(true)) { 
//				getMainActivity().popBackStack(getChildFragmentManager());
//			}

//			getShows(12);
			// TODO
//			AppManager.getInstance().sendToGA(getString(R.string.));

//		}
	}
	
	protected void getShows(int number)
	{
		Log.i(getClass().getSimpleName(), "getShows");
		boolean isPopular = (mViewType == VIEW_TYPE_POPULAR);
		ArrayList<ClipTvUIItem> uiItems = isPopular ? mTrendingClipTvUIItemsList : mShowsUiItemsList;
		if (uiItems.isEmpty() || mLoadingMore)
		{
			if (!mLoadingMore && !mSwipeLayout.isRefreshing())
			{
				mSwipeLayout.setRefreshing(true);
			}
			
			if (_promise != null && _promise.isPending()){
				return;
			}
			
			try{
				if (mViewType == VIEW_TYPE_POPULAR) {
					_promise = mTrendingDataAsync.next(number);
				} else {
					_promise = mShowsDataAsync.next(number);
				}
			}
			catch (Throwable e) {
				_promise = null;
				mSwipeLayout.setRefreshing(false);
				e.printStackTrace();
			}
			
			
			if (_promise != null){
				_promise.then(this, this);
			}
		}
		else
		{
			if (mViewType == VIEW_TYPE_POPULAR) {
				mTrendingAdapter.notifyDataSetChanged();
			} else {
				mShowsAdapter.notifyDataSetChanged();
			}
			
		}
	}

	
//	@SuppressWarnings("unchecked")
//	private DoneCallback<List<RVSShow>> getOnShowLoadedDoneCallback()
//	{
//		return new AndroidDoneCallback<List<RVSShow>>(){
//			
//			@Override
//			public void onDone(final List<RVSShow> result)
//			{
//				
//			}
//
//			@Override
//			public AndroidExecutionScope getExecutionScope()
//			{
//				return AndroidExecutionScope.UI;
//			}
//		};
//		
//	}
	
	
	@Override
	public void onPause()
	{
//		if (mImagesCache != null){
//			mImagesCache.clear();
//		}
		
		super.onPause();
	}

	

	@Override
	public void onDestroy()
	{
//		if (mImagesCache != null){
//			mImagesCache.clear();
//			mImagesCache = null;
//		}
		
		if (_promise != null){
			_promise.cancel();
		}
		
		
		mTrendingDataAsync = null;
		mShowsDataAsync = null;
		mTrendingAdapter = null;
		mShowsAdapter = null;
		mTrendingClipTvUIItemsList = null;
		mShowsUiItemsList = null;
		mListView = null;
		mSwipeLayout = null;
		mFragmentView = null;
				
		super.onDestroy();
	}
	
//	private FailCallback<Throwable> getFailCallback() {
//		return new FailCallback<Throwable>()
//		{
//
//			@Override
//			public void onFail(Throwable result)
//			{
//				_swipeLayout.setRefreshing(false);
//				
//				_progressBar.setVisibility(View.GONE);
//				_loadingMore = false;
//				dismissProgressDialog();
////				Log.w(getClass().getSimpleName(), "onFail: " + arg0);
//				if (mTrendingClipTvUIItemsList != null)
//				{
//					mTrendingClipTvUIItemsList.clear();
//				}
//				
//				try {
//					getMainActivity().noNetworkDialog();
//				} catch (Exception e) {
//					e.printStackTrace();
//
//				}
//
//				
//			}
//		};
//	}
	
	@Override
	public void onFail(Throwable arg0)
	{
		mSwipeLayout.setRefreshing(false);
		
		mProgressBar.setVisibility(View.GONE);
		mLoadingMore = false;
		dismissProgressDialog();
//		Log.w(getClass().getSimpleName(), "onFail: " + arg0);
		if (mViewType == VIEW_TYPE_POPULAR && mTrendingClipTvUIItemsList != null) {
				mTrendingClipTvUIItemsList.clear();
				mTrendingAdapter.notifyDataSetChanged();
				
		} else if (mViewType == VIEW_TYPE_A_Z && mShowsUiItemsList != null) {
			mShowsUiItemsList.clear();
			mShowsAdapter.notifyDataSetChanged();
		}
			
		try {
			getMainActivity().noNetworkDialog();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	protected void loadMorePostResults(int totalItemCount)
	{
		boolean isPopular = (mViewType == VIEW_TYPE_POPULAR);
		RVSAsyncList rsyncList =  isPopular ? mTrendingDataAsync : mShowsDataAsync;
		if (!rsyncList.getDidReachEnd())
		{
			mProgressBar.setVisibility(View.VISIBLE);
			getShows(5);
		}
	}

	@Override
	public void onDone(Object result)
	{
		mSwipeLayout.setRefreshing(false);
		
		if (!isVisible()){
			return;
		}
		List<RVSShow> showList = (List<RVSShow>)result;
		
		mProgressBar.setVisibility(View.GONE);
		dismissProgressDialog();
		Log.i(getClass().getSimpleName(), "onDone: " + showList.size());
		
		if (result != null){
			
			convertToUIItems(showList);
			boolean isPopular = (mViewType == VIEW_TYPE_POPULAR);
			ClipTvListAdapter adapter =  isPopular ? mTrendingAdapter : mShowsAdapter;
//				_showAdapter.insertElementsToRealtimePost(_list);
			adapter.notifyDataSetChanged();
		}
		mLoadingMore = false;
	}
	
	protected void convertToUIItems(List<RVSShow> data)
	{
		 ArrayList<ClipTvUIItem> list = (mViewType == VIEW_TYPE_POPULAR) ? mTrendingClipTvUIItemsList : mShowsUiItemsList;
				 
		int index = 0;
		for (RVSShow show : data) {
			
			list.add(new ClipTvUIItem(getMainActivity(), show, this, index));
			index++;
		}
		
		if (mViewType == VIEW_TYPE_POPULAR) {
			mTrendingClipTvUIItemsList = list;
		} else {
			mShowsUiItemsList = list;
		}
	}
	
	@Override
	public void addToCache(String name, Drawable draw)
	{
		if (isAdded())
		{
			AppManager.getInstance().getImagesCache().addDrawableToMemoryCache(name, draw);
		}
	}

	@Override
	public Drawable getFromCache(String name)
	{
		return AppManager.getInstance().getImagesCache().getDrawableFromMemCache(name);
	}

	
	@Override
	public void onRefresh()
	{
		ArrayList<ClipTvUIItem> list = (mViewType == VIEW_TYPE_POPULAR) ? mTrendingClipTvUIItemsList : mShowsUiItemsList;
		list.clear();
		
		if (mViewType == VIEW_TYPE_POPULAR) {
			mTrendingClipTvUIItemsList = list;
			mTrendingAdapter.notifyDataSetChanged();
		} else {
			mShowsUiItemsList = list;
			mShowsAdapter.notifyDataSetChanged();
		}
		mTrendingDataAsync = AppManager.getInstance().getSDK().trendingShows();
		mShowsDataAsync = AppManager.getInstance().getSDK().getShows();

//		mTrendingAdapter = new ClipTvListAdapter(getActivity(), mTrendingClipTvUIItemsList);
//		mShowsAdapter = new ClipTvListAdapter(getActivity(), mShowsUiItemsList);

//		mTrendingClipTvUIItemsList = null;
//		mTrendingDataAsync = null;
//		mTrendingAdapter = null;
		getShows(12);
	}

	@Override
	public void onOffset(int offset)
	{
		
	}
	
	@Override
	public String getActionBarTitle()
	{
		return getResources().getString(R.string.tab_title_tv_shows);
	}

	
	
//	private void sort(final boolean isPopular) {
//		
//		if (_showAdapter == null) return;
//		
//		_showAdapter.sort(new Comparator<ClipTvUIItem>() {
//
//			@Override
//			public int compare(ClipTvUIItem lhs, ClipTvUIItem rhs)
//			{
//				if (!isPopular)
//					return lhs.getShowName().compareTo(rhs.getShowName());
//				
//				return rhs.getIndex() - lhs.getIndex() ;
//			}
//		});
//	}
//	
	
	
//	public void openShowsFragment(String showId, String channelId)
//	{
//		Bundle bundle = new Bundle();
//		bundle.putString(ShowsFragment.SHOW_ID, showId);
//		bundle.putString(ShowsFragment.CHANNEL_ID, channelId);
//		getMainActivity().replaceTabFragmentContent(TabFragmentAdapter.TAB_POSITION_TV_SHOWS, ShowsFragment.class, bundle);
//	}
}
