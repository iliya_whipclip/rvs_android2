package com.whipclip.fragments;

import android.app.Activity;
import android.util.Log;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.RelatedComments;

/**
 * A placeholder fragment containing a simple view.
 */
public class RecentFragment extends PostsFragment
{
	@SuppressWarnings("unchecked")
	protected void getPosts(int number)
	{
		Log.i(getClass().getSimpleName(), "getPosts");
		if (_list == null || _list.isEmpty() || _loadingMore)
		{
			if (!_loadingMore && !_swipeLayout.isRefreshing())
			{
				_swipeLayout.setRefreshing(true);
				//				showProgressDialog();
			}
			if (_dataAsync == null)
			{
				_dataAsync = AppManager.getInstance().getSDK().recentPosts();
			}
			if (_promise != null && _promise.isPending())
			{
				return;
			}

			if (_dataAsync.getDidReachEnd() == false)
			{
				_promise = _dataAsync.next(number);
				if (_promise != null)
				{
					_promise.then(this, this);
				}
			}
		}
		else
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null && relatedComments.getRelatedPostId() != null)
			{
				_postsAdapter.updateElementInRealtimePost();
			}
			_postsAdapter.updateCommentSelected();
			_postsAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		((MainActivity) getActivity()).onSectionAttached(2);
		AppManager.getInstance().sendToGA(getString(R.string.recent));
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(2);
	}

}