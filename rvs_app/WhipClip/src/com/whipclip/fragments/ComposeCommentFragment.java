package com.whipclip.fragments;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnail;

public class ComposeCommentFragment extends BaseFragment<Object> implements TextWatcher
{
	private static final int		kMaxCharacters				= 140;

	private View					_view;
	private EditText				_caption;
	private EditText				_textViewPrompt;

	private int						MARKING_TEXT_WIDTH			= 0;
	private Toast					_shareToast;
	private View					_toastLayout;
	private String					_defaultText;

	private TextView					_charactersCounter;
	private int							_maxCharacters			= 0;

	private boolean	mIsLive;

	private long	mThumbnailStartOffest;
	private long	mDuration;
	private String	mThumbnailId;

	private MenuItem	_shareMenu;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = inflater.inflate(R.layout.fragment_compose_add_comment, container, false);

		setHasOptionsMenu(true);
		getActivity().invalidateOptionsMenu();

		Bundle arguments = getArguments();
		mThumbnailStartOffest = arguments.getLong("thumbnail_offset");
		mDuration = arguments.getLong("duration");
		mThumbnailId = arguments.getString("thumbnail_id");
		
		_maxCharacters = getResources().getInteger(R.integer.comment_max_length);
		_charactersCounter = (TextView) _view.findViewById(R.id.comments_characters_counter);
		_charactersCounter.setTypeface(AppManager.getInstance().getTypeFaceLight());
		

		initViews(_view);
		initListeners();
		
		return _view;
	}
	
	/**
	 * EditText methods
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		String enterCheck = s.toString().replace("\n", "").trim();
		if ("".equals(enterCheck))
		{
			_charactersCounter.setText(_maxCharacters + "");
			return;
		}

		int counter = _maxCharacters - s.length();
		_charactersCounter.setText(Integer.toString(counter));
		if (counter <= -1)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.dark_red));
		}
		else if (counter >= 0 && counter < _maxCharacters)
		{
			_charactersCounter.setTextColor(getResources().getColor(R.color.gray_text_comment));
		}
		else if (counter >= _maxCharacters)
		{
//			_caption.setEnabled(false);
//			_caption.setTextColor(getResources().getColor(R.color.orange_semi_transparent));
		}
	}

	private void setCaptionText(String caption)
	{
		_caption.setText(caption);
		
		int counter = _maxCharacters - caption.length();
		_charactersCounter.setText(Integer.toString(counter));
	}



	private void textViewDidChange()
	{

		if (_caption.getText().length() == 0)
		{
			_textViewPrompt.setText(R.string.compose_add_comment);
		}
		else
		{
			if (_defaultText != null)
			{
				_textViewPrompt.setText(R.string.compose_edit_comment);
			}
			else
			{
				_textViewPrompt.setText(R.string.compose_edit_caption);
			}
		}

		updateCharCount();
	}

	// todo
	private void updateCharCount()
	{
		//	    NSString *cleanText = [self cleanText];
		//	    NSInteger charsLeft = kMaxCharacters - cleanText.length;
		//	    self.textAccView.charsCountLabel.text = [NSString stringWithFormat:@"%li", (long)charsLeft];
		//	    
		//	    if (cleanText.length == 0)
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor lightGrayColor];
		//	    }
		//	    else if (cleanText.length <= kMaxCharacters)
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor darkGrayColor];
		//	    }
		//	    else
		//	    {
		//	        self.textAccView.charsCountLabel.textColor = [UIColor redColor];
		//	    }
	}
	

	@Override
	public void initViews(View FfragmentView)
	{
		
		
		_caption = (EditText) _view.findViewById(R.id.compose_caption);
		_caption.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_caption.addTextChangedListener(this);

		// DIsable "Enter" (new line) presses
		_caption.setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (keyCode == KeyEvent.KEYCODE_ENTER)
				{
					return true;
				}
				return false;
			}
		});

		
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(MainActivity.LAYOUT_INFLATER_SERVICE);
		_toastLayout = inflater.inflate(R.layout.whiped_toast, (ViewGroup) _view.findViewById(R.id.whipped_layout));
		TextView text = (TextView) _toastLayout.findViewById(R.id.whipped_toast_text);
		text.setTypeface(AppManager.getInstance().getTypeFaceLight());
		_shareToast = new Toast(getActivity());
		_shareToast.setGravity(Gravity.TOP, 0, getActionBar().getHeight());
		_shareToast.setDuration(Toast.LENGTH_SHORT);
		_shareToast.setView(_toastLayout);
		
		
		 ((TextView) _view.findViewById(R.id.clear)).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v)
				{
					_caption.setText("");
				}
		});
		 

		 ((TextView) _view.findViewById(R.id.tag)).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v)
				{
					String comment = _caption.getText().toString();
					if (!comment.isEmpty() && comment.charAt(comment.length() -1) == ' ') {
						comment += " #"; 
					} else {
						comment += "#";
					}
					_caption.setText(comment);
				}
		});
		 
		final FragmentActivity activity = getActivity();
		 _caption.setOnFocusChangeListener(new OnFocusChangeListener() {
	            @Override
	            public void onFocusChange(View v, boolean hasFocus) {
	            	_caption.post(new Runnable() {
	                    @Override
	                    public void run() {
	                        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
	                        imm.showSoftInput(_caption, InputMethodManager.SHOW_IMPLICIT);
	                    }
	                });
	            }
	        });
		 _caption.requestFocus();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try
		{
			if (activity != null) {
				TextView tv = (TextView) activity.findViewById(R.id.action_share);
				if (tv != null) {
					tv.setTypeface(AppManager.getInstance().getTypeFaceRegular());
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String getActionBarTitle() {
//		return getString(R.string.cancel);
		return getString(R.string.add_comment);
	}


	@Override
	public void onResume()
	{
		super.onResume();
//		((MainActivity) getActivity()).setActionBarTitle(getActivity().getString(R.string.cancel));
		((MainActivity) getActivity()).useNavigation(false);
//		((MainActivity) getActivity()).onSectionAttached(16);
//		AppManager.getInstance().sendToGA(getString(R.string.compose));
	}

	@Override
	public boolean onBackPressed()
	{
		((MainActivity) getActivity()).useNavigation(true);
		getActivity().getSupportFragmentManager().popBackStack();
		return true;
        		  
	}

	/****************************************
	 * 			ActionBar methods			*
	 ****************************************/
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.compose, menu);
		_shareMenu = menu.getItem(0);
		setShareEnabled(true);
		
//		AndroidBug5497Workaround.assistActivity(this);
//		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIEDRESIZE);
	}

	private void setShareEnabled(boolean enable)
	{
		//		if needed, remove <item name="android:actionMenuTextColor">@color/orange</item> in styles file first.
		//		Then, there's a problem that the 2nd time this is called, the text color is white for some reason.
			if (_shareMenu != null)
			{
				_shareMenu.setEnabled(enable);
//					SpannableString s = new SpannableString("Test" /*_share.getTitle()*/);
//					s.setSpan(new ForegroundColorSpan(enable ? getResources().getColor(R.color.orange) : getResources().getColor(R.color.gray_text)), 0, s.length(), 0);
//					_share.setTitle(s);
			}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		 switch (item.getItemId()) {
//			 case android.R.id.home: 
//				 ((MainActivity) getActivity()).useNavigation(true);
//				 getMainActivity().onBackPressedFromCompose();
//				 return true;
			 case R.id.action_share:
				 share();
		         return true;

			 default:
		            return super.onOptionsItemSelected(item);
		 }
	}


	@SuppressWarnings("unchecked")
	private void share()
	{
		try
		{
			if (isCapationEmpty() || isCaptionTooLong())
			{
				return;
			}
			
			setShareEnabled(false);

			((ImageView) _toastLayout.findViewById(R.id.whipped_toast_image)).setVisibility(View.GONE);
			((TextView) _toastLayout.findViewById(R.id.whipped_toast_text)).setText(getResources().getString(R.string.whipping));
			_shareToast.show();

		
			RVSMediaContext saveLastResult = AppManager.getInstance().getComposeMediaContext();
					
//			RVSThumbnail selectedThumbnail = null; // TODO = _mediaContextThumbnais.get(getStartThumbnails());
			 // TODO getDuration(selectedThumbnail.getStartOffset());
//			long duration = 0;
			
			Log.i(getClass().getName(), "Share : createPostWithText - " + "selectedThumbnail.getStartOffset() / 1000 = " + mThumbnailStartOffest
					 + " duration : " + mDuration + "  selectedThumbnail.getThumbnailId() = " + mThumbnailId);

			
			
			RVSPromise postWithTextPromise = AppManager
					.getInstance()
					.getSDK()
					.createPostWithText(_caption.getText() != null ? _caption.getText().toString() : "", saveLastResult,
							mThumbnailStartOffest, mDuration , mThumbnailId);
			postWithTextPromise.then(new DoneCallback<RVSPost>()
			{

				@Override
				public void onDone(final RVSPost postItem)
				{
					getActivity().runOnUiThread(new Runnable()
					{

						@Override
						public void run()
						{
							((ImageView) _toastLayout.findViewById(R.id.whipped_toast_image)).setVisibility(View.VISIBLE);
							((TextView) _toastLayout.findViewById(R.id.whipped_toast_text)).setText(getResources().getString(R.string.whipped_ex));
						}
					});
//					_shareToast.show();
					AppManager.getInstance().setRefreshPosts(true);
					getActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							Handler handler = new Handler();
							handler.postDelayed(new Runnable()
							{
								
								@Override
								public void run()
								{
									AppManager.getInstance().setCurrentMediaContext(null, 0);
									
//									// Get the back stack fragment id.
//									int backStackCount = getActivity().getSupportFragmentManager().getBackStackEntryCount();
//									for (int i = 0; i < backStackCount; i++) {
//
//									    int backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(i).getId();
//									    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//									}
//								    
								    
									getActivity().getSupportFragmentManager().popBackStack(null,  FragmentManager.POP_BACK_STACK_INCLUSIVE); // close
									getMainActivity().onBackPressedFromCompose();
//									getActivity().onBackPressed(); // one to come back to compose screen
									getMainActivity().openShareMoreOptionsDialog(true, postItem);

									
								}
							}, 100);
//							AppManager.getInstance().setCurrentMediaContext(null, 0);
//							getActivity().getSupportFragmentManager().popBackStack(); // close
//							getMainActivity().onBackPressedFromCompose();
////							getActivity().onBackPressed(); // one to come back to compose screen
//							getMainActivity().openShareMoreOptionsDialog(true, postItem);
						}
					});
				}
			}, new FailCallback<Object>()
			{
				@Override
				public void onFail(Object result)
				{
					Toast.makeText(getActivity(), "There was an error sharing this clip", Toast.LENGTH_LONG).show();
					AppManager.getInstance().setRefreshPosts(false);
//					getActivity().getFragmentManager().popBackStack();
					
					getActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							getActivity().onBackPressed();
						}
					});
					
				}
			});

		}
		catch (AssertionError e)
		{
			e.printStackTrace();
		}
		catch (Exception e1)
		{
			Toast.makeText(getActivity(), "Share is not ready yet, please try again", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		}

	}

	private boolean isCaptionTooLong()
	{
		String captionText = _caption.getText().toString();
		if (captionText != null && captionText.length() <= kMaxCharacters)
		{
			return false;
		}

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
					case DialogInterface.BUTTON_POSITIVE:
						dialog.dismiss();
						break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.caption_too_long)).setTitle(getResources().getString(R.string.sorry))
				.setPositiveButton("Ok", dialogClickListener).show();

		return true;
	}

	private boolean isCapationEmpty()
	{
		String captionText = _caption.getText().toString();
		if (captionText != null && "".equals(captionText.trim()) == false)
		{
			return false;
		}

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
					case DialogInterface.BUTTON_POSITIVE:
						dialog.dismiss();
						break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.give_caption_first)).setTitle(getResources().getString(R.string.sorry))
				.setPositiveButton("Ok", dialogClickListener).show();

		return true;
	}

	/****************************************
	 * 				Seekbar methods			*
	 ****************************************/

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onDone(Object result)
	{
		Log.e(getClass().getSimpleName(), "on done");
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable s)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}

}
