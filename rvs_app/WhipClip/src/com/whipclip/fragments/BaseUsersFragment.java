package com.whipclip.fragments;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

//import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.ui.DrawableCache;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.SearchedUsersAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;

public abstract class BaseUsersFragment<T> extends BaseFragment<T> implements IDrawableCache, OnRefreshListener
{
	protected View							_view;
	protected ListView						_usersList;
	protected SearchedUsersAdapter			_listAdapter;
	protected SwipeRefreshLayout			_swipeLayout;

	protected String						_searchedUser;
	protected String						_userId;
	protected String						_postId;		//for "liking users" + "reposting users" screen when user presses a like or whip counter on FullPostView 

	protected RVSPromise<T, Throwable, Integer>	_userPromise;
	protected RVSAsyncList					_userAsyncList;

	protected abstract void loadUsers();

	protected abstract void setTitleOnActionBar();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = inflater.inflate(R.layout.fragment_searched_users, container, false);

		if (getArguments() != null)
		{
			_searchedUser = getArguments().getString(Consts.BUNDLE_USER_NAME, null);
			_postId = getArguments().getString(Consts.BUNDLE_POST_ID, null);
			_userId = getArguments().getString(Consts.EXTRA_USER_ID, "");
		}

		_swipeLayout = (SwipeRefreshLayout) _view.findViewById(R.id.swipe_container);
		_swipeLayout.setOnRefreshListener(this);
//		_swipeLayout.setColorScheme(android.R.color.holo_orange_light, android.R.color.holo_red_light, android.R.color.holo_green_light,
//				android.R.color.holo_blue_bright);
		
		_swipeLayout.setColorScheme(R.color.gray_cover, R.color.gray_text, R.color.orange,  R.color.notify_bg);


		initViews(_view);
		initListeners();

		loadUsers();
		setHasOptionsMenu(true);
//		((MainActivity) getActivity()).hideNavigation();

		return _view;
	}

//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
//	{
//		menu.clear();
//		getActionBar().setDisplayHomeAsUpEnabled(false);
//		setTitleOnActionBar();
//
//		super.onCreateOptionsMenu(menu, inflater);
//	}

	@Override
	public void onPause()
	{
//		if (_photosCache != null)
//		{
//			_photosCache.clear();
//		}
		super.onPause();
	}

	@Override
	public void onDestroy()
	{
		if (_userPromise != null)
		{
			_userPromise.cancel();
		}
//		if (_photosCache != null)
//		{
//			_photosCache.clear();
//			_photosCache = null;
//		}
		getActionBar().show();
		super.onDestroy();
	}

	@Override
	public void addToCache(String name, Drawable draw)
	{
		if (isAdded())
		{
			AppManager.getInstance().getImagesCache().addDrawableToMemoryCache(name, draw);
		}
	}

	@Override
	public Drawable getFromCache(String name)
	{
		return AppManager.getInstance().getImagesCache().getDrawableFromMemCache(name);
	}

	@Override
	public void initViews(View FfragmentView)
	{
		_usersList = (ListView) FfragmentView.findViewById(R.id.users_list);
	}

	@Override
	public void initListeners()
	{
		_usersList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				((MainActivity) getActivity()).openUserProfile(_listAdapter.getItem(position).getUserId(), false);
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			PendingOperation op = getPendingOperation();
			if (op != null)
			{
				executePendingOperation(op);
			}
		}
	}

	@Override
	public void onRefresh()
	{
		_handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				_swipeLayout.setRefreshing(false);
			}
		}, 2000);
	}

	private void executePendingOperation(PendingOperation op)
	{
		switch (op.getType())
		{
			case FOLLOW:
			{
				followUser((String) op.getData1());
				break;
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void followUser(String userId)
	{
		final FragmentManager fm = getFragmentManager();
		RVSPromise followPromise = AppManager.getInstance().getSDK().followUser(userId);
		followPromise.then(new DoneCallback()
		{
			@Override
			public void onDone(Object result)
			{
				Log.i("follow User: OK", "Horray!");
				updateFollowersCountIfNeeded(1, fm);
			}
		}, new FailCallback()
		{
			@Override
			public void onFail(Object result)
			{
				Log.e("follow User: NOT OK", ":(");
				getMainActivity().noNetworkDialog();
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unFollowUser(String userId)
	{
		final FragmentManager fm = getFragmentManager();
		RVSPromise followPromise = AppManager.getInstance().getSDK().unfollowUser(userId);
		followPromise.then(new DoneCallback()
		{
			@Override
			public void onDone(Object result)
			{
				Log.i("UNfollow User: OK", "Horray!");
				updateFollowersCountIfNeeded(-1, fm);
			}
		}, new FailCallback()
		{
			@Override
			public void onFail(Object result)
			{
				Log.e("UNfollow User: NOT OK", ":(");
				getMainActivity().noNetworkDialog();
			}
		});
	}

	private void updateFollowersCountIfNeeded(int addOrRemove, FragmentManager fm)
	{
		try
		{
			String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2 < 0 ? fm.getBackStackEntryCount() - 1 : fm.getBackStackEntryCount() - 2)
					.getName();
			if (UserProfileFragment.class.getSimpleName().equals(tag))
			{
				((UserProfileFragment) fm.findFragmentByTag(tag)).updateFollowingCount(addOrRemove);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	protected void setEmptyView(String string)
	{
		if (isVisible() && _view != null && _usersList != null)
		{
			TextView text = (TextView) _view.findViewById(R.id.empty_view);
			text.setText(string);
			_usersList.setEmptyView(text);
		}
	}

}
