package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.CustomSwipeRefreshLayout;
import android.support.v4.widget.CustomSwipeRefreshLayout.OffsetLisetener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncList;
import com.whipclip.rvs.sdk.dataObjects.RVSFeedCountDetails;
import com.whipclip.rvs.sdk.dataObjects.RVSNotification;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.NotificationsAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;

public class NotificationsFragment extends BaseFragment<List<RVSNotification>> implements CustomSwipeRefreshLayout.OnRefreshListener, OffsetLisetener
{
	//Views
	private View						_view;
	private ListView					_notificationsListView;
	private NotificationsAdapter		_listAdapter;
	private CustomSwipeRefreshLayout	_swipeLayout;
	private ProgressBar					_progressBar;

	private ArrayList<RVSNotification>	_notificationsList;
	private RVSAsyncList				_notificationsAsyncList;
	private RVSPromise					_notificationsPromiss;

	private int							_totalNotifications			= 0;
	private int							NUM_OF_NOTIFICATIONS_TO_GET	= 10;
	protected boolean					_loadingMore				= false;

	public NotificationsFragment(){
			
		super();
		setCurrentTabPosition(TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS);
	}
	
 
	public NotificationsFragment(int tabPositionNotifications)
	{
		super();
		
		setCurrentTabPosition(tabPositionNotifications);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if (_view != null && _notificationsListView != null && _listAdapter != null)
		{
			//view is already set, no need to do anything. 
			//this happens when we're pressing notifications -> search -> go back to notifications.F 
		}
		else
		{
			_view = inflater.inflate(R.layout.fragment_notifications, container, false);

			initViews(_view);
			initListeners();

//			if (_photosCache == null)
//			{
//				_photosCache = new DrawableCache();
//			}

			loadNotifications();

			setHasOptionsMenu(true);
		}
		
		if (_view.getParent() != null) {
			((ViewGroup) _view.getParent()).removeView(_view);
		}
		
		return _view;
	}

	@Override
	public void initViews(View FfragmentView)
	{
		_progressBar = (ProgressBar) _view.findViewById(R.id.notifications_progressbar);

		_swipeLayout = (CustomSwipeRefreshLayout) _view.findViewById(R.id.notifications_swipe_container);
		_swipeLayout.setOnRefreshListener(this);
		_swipeLayout.setOffsetListener(this);

		
//		_swipeLayout.setColorScheme(android.R.color.holo_orange_light, android.R.color.holo_red_light, android.R.color.holo_green_light,
//				android.R.color.holo_blue_bright);
		
		_swipeLayout.setColorScheme(R.color.gray_cover, R.color.gray_text, R.color.orange,  R.color.notify_bg);


		_notificationsListView = (ListView) _view.findViewById(R.id.notifications_list);

	}

	@Override
	public void initListeners()
	{
		_notificationsListView.setOnScrollListener(new OnScrollListener()
		{
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState)
			{
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount)
			{
				if (_notificationsListView != null && _notificationsListView.getChildCount() > 0)
				{
					boolean reachedLastChild = (_notificationsListView.getLastVisiblePosition() == _notificationsListView.getAdapter().getCount() - 1 && _notificationsListView
							.getChildAt(_notificationsListView.getChildCount() - 1).getBottom() <= _notificationsListView.getHeight());
					if (totalItemCount != 0 && _loadingMore == false && _notificationsList != null && _totalNotifications > _notificationsList.size()
							&& reachedLastChild)
					{
						_loadingMore = true;
						loadNotificationsStep();
					}
				}
			}

		});
	}

	@SuppressWarnings("unchecked")
	private void loadNotifications()
	{
		if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID) == null || "".equals(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID)))
		{
			return;
		}
		_swipeLayout.setRefreshing(true);

		RVSPromise promiss = AppManager.getInstance().getSDK().notificationsCountForUser(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID), 0);
		promiss.then(new AndroidDoneCallback<RVSFeedCountDetails>()
		{

			@Override
			public void onDone(final RVSFeedCountDetails result)
			{
				if (result != null)
				{

					if (_notificationsList != null && _notificationsList.isEmpty() == false) {
						_notificationsList.clear();
					}

					_totalNotifications = result.getCount();
					Log.i(getClass().getName(), "total number of items = " + _totalNotifications);
					_notificationsAsyncList = AppManager.getInstance().getSDK().notificationsForUser(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID));
					if (_swipeLayout != null) {
						_swipeLayout.setRefreshing(true);
					}
					loadNotificationsStep();
				}
			}

			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}
		});
	}

//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
//	{
//		super.onCreateOptionsMenu(menu, inflater);
//		menu.clear();
//		inflater.inflate(R.menu.feeds, menu);
//	}

	@Override
	public void onDone(List<RVSNotification> result)
	{
		if (_progressBar != null) {
			_progressBar.setVisibility(View.GONE);
		}
		
		Log.e(getClass().getSimpleName(), "OnDone comment: " + result);
		if (result != null)
		{
			if (_notificationsList == null)
			{
				_notificationsList = new ArrayList<RVSNotification>();
			}

			//adding items to the end.
			for (int i = 0; i < result.size(); i++)
			{
				_notificationsList.add(result.get(i));
			}
			_swipeLayout.setEnabled(true);

//			if (!isVisible())
//			{
//				return;
//			}

			_swipeLayout.setRefreshing(false);
			if (_listAdapter == null)
			{
				_listAdapter = new NotificationsAdapter(NotificationsFragment.this.getActivity(), R.layout.notification_item, _notificationsList);
				_notificationsListView.setAdapter(_listAdapter);
			}
			else
			{
				_listAdapter.notifyDataSetChanged();
			}

			if (result.size() > 0)
			{
				getMainActivity().setLastFetchNotifications(((RVSNotification) result.get(0)).getTimestamp() + 1);
			}
			else
			{
				getMainActivity().setLastFetchNotifications(-1);
				if (isVisible())
				{
					_handler.post(new Runnable()
					{
						@Override
						public void run()
						{
							setEmptyView(getString(R.string.no_notifications), getString(R.string.no_notifications2));
						}
					});
				}
			}
			getMainActivity().resetNotifications();
		}
		_loadingMore = false;
		AppManager.getInstance().savePrefrences(Consts.PREF_NOTIFICATION_COUNT, "" + 0);
	}

	@Override
	public void onFail(Throwable result)
	{
		_swipeLayout.setRefreshing(false);
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onPause()
	{
//		if (_photosCache != null)
//		{
//			_photosCache.clear();
//		}
		super.onPause();
	}

	@Override
	public void onDestroy()
	{
//		if (_photosCache != null)
//		{
//			_photosCache.clear();
//			_photosCache = null;
//		}
		
		_view = null;
		_notificationsListView.setAdapter(null);
		_notificationsListView = null;
		if (_listAdapter != null) {
			_listAdapter.clear();
			_listAdapter = null;
		}
		
		_swipeLayout = null;
		_progressBar = null;

		if (_notificationsList != null) {
			_notificationsList.clear();
			_notificationsList = null;
		}
		
		_notificationsAsyncList = null;
		_notificationsPromiss = null;
		
		super.onDestroy();
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
			_progressBar.setVisibility(View.GONE);
			AppManager.getInstance().sendToGA(getString(R.string.notifications));
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
//		_progressBar.setVisibility(View.GONE);
//		AppManager.getInstance().sendToGA(getString(R.string.notifications));
	}

	@Override
	public String getActionBarTitle() {
		
		return getResources().getString(R.string.notifications);
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(1);
	}
	
	@Override
	public void onRefresh()
	{
		if (_swipeLayout != null)
		{
			_swipeLayout.setRefreshing(true);
			_handler.postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					_swipeLayout.setRefreshing(false);
				}
			}, 2000);
		}

		if (_notificationsList != null)
		{
			_notificationsList = null;
			_listAdapter = null;
			_notificationsPromiss = null;
			_notificationsAsyncList = null;
			_totalNotifications = 0;
			loadNotifications();
			
//			getMainActivity().resetNotifications();
		} else {
			loadNotifications();
		}
	}

	private void loadNotificationsStep()
	{
		try
		{
			if (!_notificationsAsyncList.getDidReachEnd())
			{
				Log.e(getClass().getName(), "Loading more notifications");

				if (_notificationsList != null && _notificationsList.isEmpty() == false)
				{
					_progressBar.setVisibility(View.VISIBLE);
				}
				
				getMainActivity().setLastFetchNotifications(-1);
				_notificationsPromiss = _notificationsAsyncList.next(NUM_OF_NOTIFICATIONS_TO_GET);
				_notificationsPromiss.then(this);
			} else {
				_progressBar.setVisibility(View.GONE);
			}
			
		}
		catch (AssertionError e)
		{
			e.printStackTrace();
			_loadingMore = false;
			_swipeLayout.setRefreshing(false);
			_progressBar.setVisibility(View.GONE);
		}
	}
	
	protected void setEmptyView(String string, String string2)
	{
		if (isVisible() && _view != null)
		{
			LinearLayout layout = (LinearLayout) _view.findViewById(R.id.empty_view);
			
			TextView text = (TextView) _view.findViewById(R.id.empty_view_text1);
			text.setText(string);
			text.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			TextView text2 = (TextView) _view.findViewById(R.id.empty_view_text2);
			text2.setText(string2);
			text2.setTypeface(AppManager.getInstance().getTypeFaceLight());

			_notificationsListView.setEmptyView(layout);
		}
	}
	
	@Override
	public void onTabSelected() {
		super.onTabSelected();
		
		loadNotifications();
	}
	
	@Override
	public void onTabReselected() {
		super.onTabReselected();
		
		loadNotifications();
	}

	@Override
	public void onOffset(int offset)
	{
		// TODO Auto-generated method stub
		
	}

}
