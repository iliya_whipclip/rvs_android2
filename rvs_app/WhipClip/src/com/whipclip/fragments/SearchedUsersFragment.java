package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSUserSearchResult;
import com.whipclip.ui.adapters.SearchedUsersAdapter;

public class SearchedUsersFragment extends BaseUsersFragment<List<RVSUserSearchResult>>
{

	@Override
	protected void loadUsers()
	{
		if (_searchedUser != null)
		{
			_swipeLayout.setRefreshing(true);
			_userAsyncList = AppManager.getInstance().getSDK().searchUser(_searchedUser);
			if (!_userAsyncList.getDidReachEnd())
			{
				_userPromise = _userAsyncList.next(10);
				_userPromise.then(this, this);
			}
		}
	}

	@Override
	protected void setTitleOnActionBar()
	{
		ActionBar ab = getActionBar();
		ab.setTitle(_searchedUser);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (isVisibleToUser) {
	    	getMainActivity().setActivityTitle(_searchedUser);
			AppManager.getInstance().sendToGA("Searched users");
	    }
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
//		getMainActivity().setActivityTitle(_searchedUser);
//		AppManager.getInstance().sendToGA("Searched users");
	}

	
//	@Override
//	public boolean onBackPressed()
//	{
//		((MainActivity) getActivity()).useNavigation(true);
//		return super.onBackPressed();
//	}
	
	@Override
	public String getActionBarTitle() {
		
		return _searchedUser;
	}
	
	@Override
	public void onDone(List<RVSUserSearchResult> result)
	{
		if (result != null)
		{
			ArrayList<RVSUser> users = new ArrayList<RVSUser>();
			for (RVSUserSearchResult post : result)
			{
				users.add(post.getUser());
			}

			_swipeLayout.setEnabled(true);

			if (!isVisible())
			{
				return;
			}
			_swipeLayout.setRefreshing(false);

			_listAdapter = new SearchedUsersAdapter(getActivity(), users, this, this);
			_usersList.setAdapter(_listAdapter);
		}
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}
	
//	@Override
//	public void initListeners()
//	{
//		_usersList.setOnItemClickListener(new OnItemClickListener()
//		{
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//			{
////				((MainActivity) getActivity()).openUserProfileAsChild(mCurrentTabPosition, _listAdapter.getItem(position).getUserId(), false);
//				
//				((MainActivity) getActivity()).openUserProfileActivity(-1, _listAdapter.getItem(position).getUserId(), false);
//			}
//		});
//	}

}
