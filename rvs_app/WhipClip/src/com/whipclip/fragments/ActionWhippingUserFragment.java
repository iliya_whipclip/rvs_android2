package com.whipclip.fragments;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSUserImp;
import com.whipclip.ui.adapters.SearchedUsersAdapter;

public class ActionWhippingUserFragment extends BaseUsersFragment<List<RVSUserImp>>
{

	@Override
	protected void loadUsers()
	{
		if (_postId != null)
		{
			_swipeLayout.setRefreshing(true);
			_userAsyncList = AppManager.getInstance().getSDK().repostingUsersForPost(_postId);
			if (!_userAsyncList.getDidReachEnd())
			{
				_userPromise = _userAsyncList.next(10);
				_userPromise.then(this, this);
			}
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getMainActivity().setActivityTitle(getString(R.string.whipping_users));
	}

	@Override
	public String getActionBarTitle() {
		
		return getActivity().getString(R.string.whipping_users);
	}
	
	@Override
	public void onDone(List<RVSUserImp> result)
	{
		Log.e(getClass().getName(), "onDone ActionWhippingUserFragment result = ");
		if (result != null)
		{
			ArrayList<RVSUser> users = new ArrayList<RVSUser>();
			for (RVSUserImp post : result)
			{
				users.add(post);
			}

			_swipeLayout.setEnabled(true);

			if (!isVisible())
			{
				return;
			}
			_swipeLayout.setRefreshing(false);

			_listAdapter = new SearchedUsersAdapter(getActivity(), users, this, this);
			_usersList.setAdapter(_listAdapter);
		}
	}

	@Override
	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	protected void setTitleOnActionBar()
	{
		// TODO Auto-generated method stub

	}

}
