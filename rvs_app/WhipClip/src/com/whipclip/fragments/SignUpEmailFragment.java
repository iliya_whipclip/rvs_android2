package com.whipclip.fragments;

import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.activities.SignInForgotPasswod;
import com.whipclip.activities.SignUpActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.SignUpBroadcastReceiver;
import com.whipclip.logic.SignUpBroadcastReceiver.SingUpStatusChanged;
import com.whipclip.logic.UserDetails;
import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.DialogUtils;


public class SignUpEmailFragment extends BaseFragment<Object> implements OnClickListener, SingUpStatusChanged
{
	private Logger logger = LoggerFactory.getLogger(SignUpEmailFragment.class);
	
    public static final int SING_UP_UPDATE_USERNAME = 1;
    public static final int SING_UP_BY_EMAIL = 2;
    public static final int SIGN_IN_EMAIL = 3;
	
	private View		mFragmentView;
	private TextView	mTermOfServeTextView;
	private Button		mBackButton;
	
	private String	mHandleName = "";

	private TextView 	mLoginSingupTitle;
	private EditText	mFullNameEditText;
	private EditText	mEmailEditText;
	private EditText 	mHandleEditText;
	private EditText	mPasswordEditText;
//	private EditText	mPasswordConfirmEditText;
	
	private TextView 	mHandleHelp;
	private TextView 	mPasswordHelp;
	

	private String	mEmail;
	private int mType = SING_UP_BY_EMAIL;

	SignUpBroadcastReceiver mSingUpBroadcastReceiver;

	private BaseActivity	mActivity;

	private RVSPromise	mUpdateOrRegisterPromise;

	private TextView	mForgotPassword;

//	private ProgressBar	mLoginProgressBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
	{
		Bundle arguments = getArguments();
		if (arguments != null)
		{
			mType = arguments.getInt("type");
		} else {
			mType = SING_UP_BY_EMAIL;
		}
		
		if (mFragmentView == null)
		{
			if (mType == SIGN_IN_EMAIL) {
				
				mFragmentView = inflater.inflate(R.layout.sign_in_email,  container, false);
				initSignInView(mFragmentView);
				
			} else {
				
				mFragmentView = inflater.inflate(R.layout.sign_up_email, container, false);
			
				mTermOfServeTextView = (TextView) mFragmentView.findViewById(R.id.term_of_service);
				mTermOfServeTextView.setMovementMethod(LinkMovementMethod.getInstance());
				mTermOfServeTextView.setText(Html.fromHtml(getString(R.string.term_of_service_login)));
				initViews(mFragmentView);
			}
			
//			mBackButton = (Button) mFragmentView.findViewById(R.id.login_button_back);
//			mBackButton.setOnClickListener(this);
//			mLoginProgressBar  = (ProgressBar)mFragmentView .findViewById(R.id.login_progress_bar);
		} else {
			((ViewGroup) mFragmentView.getParent()).removeView(mFragmentView);
		}
		
//		mLoginProgressBar.setVisibility(View.GONE);
			
			
		initViewByType();
			
		if (mType == SIGN_IN_EMAIL) {
			if (mSingUpBroadcastReceiver == null) {
				mSingUpBroadcastReceiver = new SignUpBroadcastReceiver(this);
				mSingUpBroadcastReceiver.registerReceiver();
			}
		}
		
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setLogo(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		if (mType == SIGN_IN_EMAIL) {
			actionBar.setTitle(R.string.login_singin_title);
		} else {
			actionBar.setTitle(R.string.login_sign_up);
		}
		
		actionBar.show();
		
		return mFragmentView;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		mActivity = (BaseActivity)activity;
	}

	private void initViewByType()
	{
		if (mType == SING_UP_UPDATE_USERNAME) {
			
			RVSSdk sdk = RVSSdk.sharedRVSSdk();
			RVSUser user = sdk.getMyUser();
			mEmail = RVSSdk.sharedRVSSdk().getMyEmail();
			boolean toShowEmail = (mEmail == null || mEmail.isEmpty());
			boolean toShowHandle = sdk.isNewExternalUser() || sdk.isHandleRequired();
			
			
			mFragmentView.findViewById(R.id.login_password_edit).setVisibility(View.GONE);
//			mFragmentView.findViewById(R.id.login_password_confirm_edit).setVisibility(View.GONE);
			
			// handle 
			mFragmentView.findViewById(R.id.login_handle_edit).setVisibility(toShowHandle ? View.VISIBLE : View.GONE);
			if (toShowHandle) {
				
				//can I use one offered by server
				String handleByServer = sdk.getMyUser().getHandle();
		        if (!sdk.isHandleRequired() && handleByServer != null && !handleByServer.isEmpty())
		        {
		        	mHandleName = sdk.getMyUser().getHandle();
		        	mHandleEditText.setText(mHandleName);
		        	mHandleEditText.setTextColor(getResources().getColor(R.color.btn_text_gray));
		        }
			}
			
			if (user.getName() != null && !user.getName().isEmpty()) {
				((EditText)mFragmentView.findViewById(R.id.login_full_name).findViewById(R.id.login_btn_edit_text)).setText(user.getName());
			}
			
			mFragmentView.findViewById(R.id.login_email_edit).setVisibility(toShowEmail ?View.VISIBLE : View.GONE);
			if (toShowEmail) {
				((EditText)mFragmentView.findViewById(R.id.login_email_edit).findViewById(R.id.login_btn_edit_text)).setText(mEmail);
			}
		} else if (mType == SIGN_IN_EMAIL) {
			
			((Button)mFragmentView.findViewById(R.id.login_sign_up_button)).setText(R.string.login_singin_title);
			
//			mFragmentView.findViewById(R.id.login_full_name).setVisibility(View.GONE);
//			mFragmentView.findViewById(R.id.login_handle_edit).setVisibility(View.GONE);
			
		}
	}
	
	@Override
	public void onDone(Object result)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(Throwable result)
	{
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	public void initSignInView(View fragmentView)
	{
		Button loginSingupBtn = (Button)fragmentView.findViewById(R.id.login_sign_up_button);
		loginSingupBtn.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		
		View view  = fragmentView.findViewById(R.id.login_email_edit);
		mEmailEditText = ((EditText)view.findViewById(R.id.login_btn_edit_text));
		mEmailEditText.setHint(R.string.login_email_hint);
		mEmailEditText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		
		dismissProgressDialog();
		
		view = fragmentView.findViewById(R.id.login_password_edit);
		mPasswordEditText = (EditText)view.findViewById(R.id.login_btn_edit_text);
		mPasswordEditText.setHint(R.string.login_password_sing_in_hint);
		mPasswordHelp = (TextView)view.findViewById(R.id.login_btn_text_help);
		mPasswordHelp.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		mPasswordHelp.setText(R.string.sign_up_password_help);

		mPasswordEditText.addTextChangedListener(new TextWatcher(){
	        
			public void afterTextChanged(Editable s) {
				if (s.toString().length() == 0) {
					mPasswordHelp.setVisibility(View.GONE);
				}  else if (s.toString().length() > 3) {
	        		mPasswordHelp.setVisibility(View.GONE);
	        		mPasswordEditText.setTextColor(getResources().getColor(R.color.btn_text_gray));
	        	}
	        	else {
	        		mPasswordHelp.setVisibility(View.VISIBLE);
	        		mPasswordEditText.setTextColor(Color.GRAY);
	        	}
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
	        public void onTextChanged(CharSequence s, int start, int before, int count){ logger.info("onTextChanged"); }
	    }); 

		mPasswordEditText.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus) {
					mPasswordEditText.setTransformationMethod(null);	
				}
				else {
					mPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance()); // put **** instead code
				}
			}
		});
		
		
		Button signUpBtn = (Button)mFragmentView.findViewById(R.id.login_sign_up_button);
		signUpBtn.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				int errorStringId = validateData();
				if (errorStringId != -1)  {
					showErrorDialog(getString(errorStringId));
					return;
				}
				
				if (mType == SIGN_IN_EMAIL) {
					
					showProgressDialog();
//					mLoginProgressBar.setVisibility(View.VISIBLE);
					RVSSdk.sharedRVSSdk().signInWithUserName(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString());
					
				} else {
					updateOrRegister();
//					showConfirmDialog(); 
				}
			}
		});
		
		
		mForgotPassword = (TextView)fragmentView.findViewById(R.id.forgot_password);
		mForgotPassword.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		mForgotPassword.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				((SignUpActivity) mActivity).replaceFragmentContent(SignInForgotPasswod.class, false, R.anim.slide_in_right, R.anim.slide_out_right,
						R.anim.slide_in_right_exit, R.anim.slide_out_right_exit, null);
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public void initViews(View fragmentView)
	{
		Button loginSingupBtn = (Button)fragmentView.findViewById(R.id.login_sign_up_button);
		loginSingupBtn.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		
		View view = fragmentView.findViewById(R.id.login_full_name);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setText(R.string.login_full_name);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setTypeface(AppManager.getInstance().getTypeFaceMedium());
		mFullNameEditText = ((EditText)view.findViewById(R.id.login_btn_edit_text));
//		fragmentView.findViewById(R.id.login_full_name).setBackgroundResource(R.drawable.bg_top_circle);
//		mFullNameEditText.setBackgroundResource(R.drawable.bg_top_circle);
		mFullNameEditText.setHint(R.string.login_full_name_hint);
		
		view = fragmentView.findViewById(R.id.login_email_edit);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setText(R.string.login_email);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setTypeface(AppManager.getInstance().getTypeFaceMedium());
		mEmailEditText = ((EditText)view.findViewById(R.id.login_btn_edit_text));
		mEmailEditText.setHint(R.string.login_email_hint);

		view = fragmentView.findViewById(R.id.login_handle_edit);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setText(R.string.login_username);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setTypeface(AppManager.getInstance().getTypeFaceMedium());
		
		mHandleHelp = (TextView)view.findViewById(R.id.login_btn_text_help);
		mHandleHelp.setText(R.string.sign_up_handle_help);
		mHandleHelp.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		mHandleEditText = (EditText)view.findViewById(R.id.login_btn_edit_text);
		mHandleEditText.setHint(R.string.login_username_hint);
		final EditText usernameEdit = mHandleEditText;
		final TextView tv = (TextView)view.findViewById(R.id.login_btn_text_username_prefix);
		tv.setVisibility(view.VISIBLE);
		tv.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
	           public void onLayoutChange(View v, int left, int top, int right, int bottom, 
	                                      int oldLeft, int oldTop, int oldRight, int oldBottom) {
	        	   usernameEdit.setPadding(tv.getLeft() + tv.getWidth(),0,0,0);
	           }
	                    
		});
		 
		dismissProgressDialog();
//		mLoginProgressBar.setVisibility(View.GONE);
		
		mHandleEditText.addTextChangedListener(new TextWatcher(){
	        
			public void afterTextChanged(Editable s) {
	        	logger.info("afterTextChanged");
	        	if (s.toString().compareTo(mHandleName) != 0) {
	        		mHandleName = s.toString();
	        		
//		        	 if ([textField.text isEqualToString:self.myUser.handle])
//		     	    {
//		     	        //same, no change
//		     	        textField.textColor = [UIColor blackColor];
//		     	    }
//		     	    else
//		     	    {
	        		if (!mHandleName.isEmpty()) {
		     	       	RVSSdk.sharedRVSSdk().isHandleFree(mHandleName).then(new AndroidDoneCallback()
						{

							@Override
							public void onDone(Object result)
							{
//								usernameEdit.setTextColor(Color.BLACK);
								usernameEdit.setTextColor(getResources().getColor(R.color.btn_text_gray));
								mHandleHelp.setVisibility(View.GONE);
							}

							@Override
							public AndroidExecutionScope getExecutionScope()
							{
								return AndroidExecutionScope.UI;
							}
		     	       		
						}).fail(new AndroidFailCallback()
						{

							@Override
							public void onFail(Object result)
							{
								usernameEdit.setTextColor(Color.GRAY);
								mHandleHelp.setVisibility(View.VISIBLE);
							}

							@Override
							public AndroidExecutionScope getExecutionScope()
							{
								return AndroidExecutionScope.UI;
							}
						});
	        		
					} else {
						usernameEdit.setTextColor(getResources().getColor(R.color.btn_text_gray));
						mHandleHelp.setVisibility(View.GONE);
					}
	        	}
	        	
	        	

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
	        public void onTextChanged(CharSequence s, int start, int before, int count){ 	        	logger.info("onTextChanged"); }
	    }); 
		
		
		view = fragmentView.findViewById(R.id.login_password_edit);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setText(R.string.login_password);
		((TextView)view.findViewById(R.id.login_btn_text_view)).setTypeface(AppManager.getInstance().getTypeFaceMedium());
		mPasswordEditText = (EditText)view.findViewById(R.id.login_btn_edit_text);
		mPasswordEditText.setHint(R.string.login_password_hint);
		mPasswordHelp = (TextView)view.findViewById(R.id.login_btn_text_help);
		mPasswordHelp.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		mPasswordHelp.setText(R.string.sign_up_password_help);

		mPasswordEditText.addTextChangedListener(new TextWatcher(){
	        
			public void afterTextChanged(Editable s) {
				if (s.toString().length() == 0) {
					mPasswordHelp.setVisibility(View.GONE);
				}  else if (s.toString().length() > 3) {
	        		mPasswordHelp.setVisibility(View.GONE);
	        		mPasswordEditText.setTextColor(getResources().getColor(R.color.btn_text_gray));
	        	}
	        	else {
	        		mPasswordHelp.setVisibility(View.VISIBLE);
	        		mPasswordEditText.setTextColor(Color.GRAY);
	        	}
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
	        public void onTextChanged(CharSequence s, int start, int before, int count){ logger.info("onTextChanged"); }
	    }); 

		mPasswordEditText.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus) {
					mPasswordEditText.setTransformationMethod(null);	
				}
				else {
					mPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance()); // put **** instead code
				}
			}
		});
		
		
		Button signUpBtn = (Button)mFragmentView.findViewById(R.id.login_sign_up_button);
		signUpBtn.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				int errorStringId = validateData();
				if (errorStringId != -1)  {
					showErrorDialog(getString(errorStringId));
					return;
				}
				
				if (mType == SIGN_IN_EMAIL) {
					
					showProgressDialog();
//					mLoginProgressBar.setVisibility(View.VISIBLE);
					RVSSdk.sharedRVSSdk().signInWithUserName(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString());
					
				} else {
					updateOrRegister();
//					showConfirmDialog(); 
				}
			}
		});
	}
	
	private int validateData()
	{
		int errorId = -1;

		    
	    if (mType != SIGN_IN_EMAIL && mFullNameEditText.getText().length() == 0)
	    {
	    	errorId = R.string.sign_up_error_message_invalid_name; //NSLocalizedString(@"sign up error message invalid name" , nil);
	    }
	    else if ((mEmail == null || mEmail.isEmpty()) && !android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailEditText.getText()).matches())
	    {
	    	errorId = R.string.sign_up_error_message_invalid_email; //NSLocalizedString(@"sign up error message invalid email", nil);
	    }
	    else if (mType != SIGN_IN_EMAIL && (mType == SING_UP_BY_EMAIL || RVSSdk.sharedRVSSdk().isHandleRequired()) && mHandleEditText.getText().length() == 0)
	    {
	    	errorId = R.string.sign_up_error_message_invalid_handle; //NSLocalizedString(@"sign up error message invalid handle" , nil);
	    } else if (mType == SING_UP_BY_EMAIL || mType == SIGN_IN_EMAIL) {
	    	if (mPasswordEditText.length() < 4) {
	    		errorId = R.string.sign_up_error_message_password_lenght;
	    	} 
//	    	else if (mPasswordEditText.getText().toString().compareTo(mPasswordConfirmEditText.getText().toString()) != 0) {
//	    		errorId = R.string.sign_up_error_message_password_confirm;
//	    	}
	    }
	    
	    return errorId;
	}

	@Override
	public void initListeners()
	{
	}
	
	@Override
	public boolean onBackPressed()
	{
		dismissKeyboard();
		return super.onBackPressed();
	}
	
	public void onDestroy() {
		super.onDestroy();

		ActionBar actionBar = getActivity().getActionBar();
		actionBar.hide();
		dismissKeyboard();
		
	}

	@Override
	public void onClick(View v)
	{
		if (v.equals(mBackButton)) {
			dismissKeyboard();
			SignUpActivity activity = (SignUpActivity)getActivity();  	
			activity.getSupportFragmentManager().popBackStack();
		}
	}
	
	private void showErrorDialog(String message) {
		
		final Dialog dialog = DialogUtils.createSignleButtonAlertDialog(
				getActivity(),
				getString(R.string.sign_up_error_title),
				message,
				getString(R.string.ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						if (AppManager.getInstance().hasInternetConnection() == false)
						{
							startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS), MainActivity.REQUEST_CODE_WIRELES_SETTINGS);
						}
					}
				});
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private void showConfirmDialog()
	{
		RVSSdk sdk = RVSSdk.sharedRVSSdk();
		//probably need details formating
	    String message =  String.format(getString(R.string.confirm_details_name_format), mFullNameEditText.getText().toString());
	    
	    if (mType == SING_UP_BY_EMAIL || sdk.isEmailEmpty() || !sdk.isPendingSignIn())
	    {
	    	String emailDetails =  String.format(getString(R.string.confirm_details_email_format), mEmailEditText.getText().toString()); 
	        message = String.format("%s\n%s", message, emailDetails);
	    }
	    
	    if (mType == SING_UP_BY_EMAIL || sdk.isHandleRequired() || !sdk.isPendingSignIn())
	    {
	    	String handleDetails =  String.format(getString(R.string.confirm_details_handle_format), mHandleEditText.getText().toString()); 
	        message = String.format("%s\n%s", message, handleDetails);
	    }
	    
		final Dialog dialog = DialogUtils.createTwoButtonsAlertDialog(getActivity(),
				getString(R.string.confirm_details_title),
				message, 
				getString(R.string.yes),
				getString(R.string.no),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						updateOrRegister();
					}
				});
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	@SuppressWarnings("unchecked")
	public void updateOrRegister() {
		
	    if (RVSSdk.sharedRVSSdk().isPendingSignIn())
	    {
	        mUpdateOrRegisterPromise = RVSSdk.sharedRVSSdk().updateUserProfileWithName(mFullNameEditText.getText().toString(),
	        		mHandleEditText.getText().toString(), mEmailEditText.getText().toString());
	        		
	    }
	    else
	    {
	        mUpdateOrRegisterPromise = RVSSdk.sharedRVSSdk().registerWithEmail(mEmailEditText.getText().toString(), 
	        		mPasswordEditText.getText().toString(), 
	        		mFullNameEditText.getText().toString(), 
	        		mHandleEditText.getText().toString());
	    }

	    mUpdateOrRegisterPromise.then(new AndroidDoneCallback()
		{

			@Override
			public void onDone(Object result)
			{
				onSignUpSuccess();
			}

			
			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}
		}).fail(new AndroidFailCallback<Throwable>(){

			@Override
			public void onFail(Throwable error)
			{
				if (isAdded()) {
				     String message = getString(R.string.sign_up_error_message);
				     
				     if (error instanceof RVSError) {
				            String errorCode = ((RVSError)error).getErrorCode();
				            if (errorCode != null && errorCode.compareToIgnoreCase("USER_HANDLE_EXIST") == 0){
				                message = getString(R.string.sign_up_error_message_taken_handle);
				            } else if (errorCode.compareToIgnoreCase("USER_EMAIL_EXIST") == 0){
				                message = getString(R.string.sign_up_error_message_taken_email);
				            }
				     }
				     showErrorDialog(message);
				}
			}

			@Override
			public AndroidExecutionScope getExecutionScope()
			{
				return AndroidExecutionScope.UI;
			}
			
		});
	}

	@Override
	public void onSignedUpStatusChanged(final boolean isSignedIn, final String errorMessage)
	{
		dismissProgressDialog();
		
		_handler.post(new Runnable()
		{
			
			@Override
			public void run()
			{
				if (isSignedIn) {
					
					LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(mSingUpBroadcastReceiver);

					RVSSdk sdk = AppManager.getInstance().getSDK();
					String myUserId = sdk.getMyUserId();
					AppManager.getInstance().savePrefrences(Consts.PREF_SDK_USERID, myUserId);
					try {
						onSignUpSuccess();
					} catch (Exception e) {
						logger.error("onSignUpSuccess " + e.getMessage());
						e.printStackTrace();
					}
				
				} else {
					
					if(isAdded()){ 
						String message = getString(R.string.sign_up_error_message);
						if (errorMessage != null) {
							
							if (errorMessage.equals("UNKNOWN_EMAIL_ADDRESS") || errorMessage.equals("INVALID_USER_CREDENTIALS") ||  errorMessage.equals("MISSING_USER_AUTH_INFO")) {
								message = getString(R.string.sign_in_email_bad_credentials_message);
							}
						} 
					    
						showErrorDialog(message);
					}
				}				
			}
		});
	}
	
	
	private void onSignUpSuccess()
	{
		
		mActivity.dismissProgressDialog();
		
		RVSUser myUser = RVSSdk.sharedRVSSdk().getMyUser();
		UserDetails details = new UserDetails(myUser.getName(), myUser.getUserId(),RVSSdk.sharedRVSSdk().getMyEmail(), "", UserDetails.USER_TYPE_FACEBOOK);
		details.setHandle(myUser.getHandle());
		AppManager.getInstance().getFilesManager().save(Consts.USER_DETAILS, details);

		if (mType == SING_UP_BY_EMAIL) {
			AppManager.getInstance().savePrefrences(Consts.PREF_SDK_USERID, myUser.getUserId());
		}
		
		mActivity.setResult(Consts.LOGIN_OK);
		mActivity.finish();
		
	}

	@Override
	public void onSignedOut(String error)
	{
		dismissProgressDialog();
	}
}
