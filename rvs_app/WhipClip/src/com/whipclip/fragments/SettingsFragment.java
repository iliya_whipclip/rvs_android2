package com.whipclip.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.ui.adapters.TabFragmentAdapter;

public class SettingsFragment extends BaseFragment<Object>
{
	View	_view;

	public SettingsFragment()
	{
		super();
	}
	
	public SettingsFragment(int tabPosition)
	{
		super();
		
		setCurrentTabPosition(tabPosition);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_view = inflater.inflate(R.layout.fragment_settings, container, false);

		initViews(_view);

		getMainActivity().onSectionAttached(7);

		return _view;
	}

	public void onDone(Object result)
	{
		// TODO Auto-generated method stub

	}

	public void onFail(Throwable result)
	{
		getMainActivity().noNetworkDialog();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		AppManager.getInstance().sendToGA(getString(R.string.search));
	}

	@Override
	public void initViews(View FfragmentView)
	{
		TextView version = ((TextView) _view.findViewById(R.id.settings_about_version));
		version.setText(WhipClipApplication.getVersionNumber());
		version.setTypeface(AppManager.getInstance().getTypeFaceLight());

//		final Button signButton = (Button) _view.findViewById(R.id.settings_sign_out);
//		if (!AppManager.getInstance().isLoggedIn())
//		{
//			signButton.setText(getResources().getString(R.string.sign_in));
//		}
//		else
//		{
//			signButton.setText(getResources().getString(R.string.sign_out));
//		}
//
//		signButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());
//		signButton.setOnClickListener(new OnClickListener()
//		{
//
//			public void onClick(View arg0)
//			{
//				if (!AppManager.getInstance().isLoggedIn())
//				{
//					Intent intent = new Intent(getActivity(), LoginActivity.class);
//					startActivityForResult(intent, 1);
//					//					_drawerLayout.closeDrawer(_fragmentContainerView);
//				}
//				else
//				{
//					AppManager.getInstance().deleteUserDetails();
//					getMainActivity().resetProfileInformationUI();
//					getMainActivity().navigateToPosition(NavigationType.TRENDING.ordinal());
//				}
//			}
//		});
//
//		Button rateUsButton = (Button) _view.findViewById(R.id.settings_rate_us);
//		rateUsButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());
//		rateUsButton.setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View arg0)
//			{
//				String appPackageName = getActivity().getPackageName();
//				try
//				{
//					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.market_http) + appPackageName)));
//				}
//				catch (android.content.ActivityNotFoundException anfe)
//				{
//					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.market_google_play) + appPackageName)));
//				}
//
//			}
//		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Consts.LOGIN_OK)
		{
			getMainActivity().setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//			getMainActivity().navigateToPosition(NavigationType.TRENDING.ordinal());
			//			initViewItems();
			//			if (_selectedPosition == 1)
			//			{
			//				changeFragment(1);
			//			}
			//			else if (_selectedPosition == 3)
			//			{
			//				changeFragment(3);
			//			}
		}
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub

	}

}
