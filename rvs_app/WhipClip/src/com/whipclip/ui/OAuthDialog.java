package com.whipclip.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.whipclip.WhipClipApplication;

public class OAuthDialog extends Dialog
{

	static final FrameLayout.LayoutParams	FILL	= new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

	private String							mUrl;
	private TwDialogListener				mListener;
	private ProgressDialog					mSpinner;
	private WebView							mWebView;
	private LinearLayout					mContent;
	private String							_callbackURL;

	private static final String				TAG		= "OAuthDialog";

	public OAuthDialog(Context context, String url, TwDialogListener listener)
	{
		super(context);

		mUrl = url;
		mListener = listener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSpinner = new ProgressDialog(getContext());

		mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mSpinner.setMessage("Loading...");

		mContent = new LinearLayout(getContext());
		mContent.setLayoutParams(FILL);

		mContent.setOrientation(LinearLayout.VERTICAL);

		setUpTitle();
		setUpWebView();

		addContentView(mContent, new FrameLayout.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenHeight));
	}

	public void setCallbackUrl(String url)
	{
		_callbackURL = url;
	}

	private void setUpTitle()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	private void setUpWebView()
	{
		mWebView = new WebView(getContext());

		mWebView.setWebViewClient(new OAuthWebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setLayoutParams(FILL);
		mWebView.loadUrl(mUrl);
		mContent.addView(mWebView);
	}

	private class OAuthWebViewClient extends WebViewClient
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			Log.d(TAG, "Redirecting URL " + url);

			if (url.startsWith(_callbackURL))
			{
				mListener.onComplete(url);

				OAuthDialog.this.dismiss();

				return true;
			}
			else if (url.startsWith("authorize"))
			{
				return false;
			}

			return false;
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
		{
			Log.d(TAG, "Page error: " + description);

			super.onReceivedError(view, errorCode, description, failingUrl);

			mListener.onError(description);

			OAuthDialog.this.dismiss();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			Log.d(TAG, "Loading URL: " + url);
			super.onPageStarted(view, url, favicon);
			if (getContext() instanceof Activity) {
				if(!((Activity) getContext()).isFinishing())
				{
					mSpinner.show();
				}
			}
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{
			super.onPageFinished(view, url);
			String title = mWebView.getTitle();
			if (title != null && title.length() > 0)
			{
				//				mTitle.setText(title);
			}
			
			if (mSpinner.isShowing()) {
				mSpinner.dismiss();       
			}
		}
	}

	public interface TwDialogListener
	{
		public void onComplete(String value);

		public void onError(String value);
	}
}
