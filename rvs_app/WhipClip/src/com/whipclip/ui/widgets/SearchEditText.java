package com.whipclip.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;

public class SearchEditText extends RelativeLayout
{
	public interface OnSearchEditTextListener {
		public void onClearClick();
		public void onSearchTextEntered(String searchString);
	}
	
	private MainActivity	mMainActivity;
	private OnSearchEditTextListener mListener;
	
	
	public SearchEditText(Context context)
	{
		super(context);
	}
	
	public SearchEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}


	public SearchEditText(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public void setBackground(int color) {
		findViewById(R.id.search_layout).setBackgroundColor(color);
	}
	
	
	public void initLayout(MainActivity mainActivity, OnSearchEditTextListener listener)
	{
		mMainActivity = mainActivity;
		mListener = listener; 
		
		final TextView searchTextView = ((TextView)findViewById(R.id.search_text));
		searchTextView.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		((ImageButton)findViewById(R.id.clear_icon_btn)).setOnClickListener(new View.OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				searchTextView.setText("");
				mListener.onClearClick();
			}
		});
		
		searchTextView.setOnEditorActionListener(new OnEditorActionListener()
		{
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
	                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
	                //Do your action
					mMainActivity.dismissKeyboard();
					
					String searchString = v.getText().toString();
					mListener.onSearchTextEntered(searchString);
					
	            }
				return false;
			}
		});
		
		searchTextView.setOnTouchListener(new View.OnTouchListener()
		{
			
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				v.setFocusable(true);
		        v.setFocusableInTouchMode(true);
				return false;
			}
		});
	}

	public void setHinText(int resid)
	{
		((EditText)findViewById(R.id.search_text)).setHint(resid);
		
	}

	public void clear()
	{
		 ((TextView)findViewById(R.id.search_text)).setText("");
		
	}

}
