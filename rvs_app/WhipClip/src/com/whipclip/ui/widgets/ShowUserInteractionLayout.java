package com.whipclip.ui.widgets;

import java.util.Date;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.WCNotificationManager;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;

public class ShowUserInteractionLayout extends RelativeLayout
{
	public interface OnSearchListener {

		void onClearButtonClick();

		void onSearchStringSubmitted(String text);
		
	}
	
	public enum State
	{
		NONE, CLIP_LIVE, NOTIFY_ME, WILL_NOTIFY, NO_CHANNEL
	}

	private State			mState = State.NONE;
	private LinearLayout	mBtnLayout;
	private ImageView		mBtnImage;
	private ImageView 		mBtnImageBg;
	private TextView		mBtnText;
	private TextView		mWillNotifyText;
	private RelativeLayout	mInteractionLayout;
	private TextView		mShowNameText;
	private TextView		mShowSubNameText;
	private TextView		mTrendingTitleText;
	private MainActivity	mMainActivity;
	private SeekBar			seekbar;

	private RVSShow			mShow;
	private long			mStartTime;
	
	private TextView	mSearchTextView;
	private OnSearchListener	mOnSearchListener;


	public ShowUserInteractionLayout(Context context)
	{
		super(context);

	}

	public ShowUserInteractionLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ShowUserInteractionLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public void initLayout(MainActivity mainActivity, OnSearchListener listener)
	{
		mMainActivity = mainActivity;
		mOnSearchListener = listener;
		
		mInteractionLayout = (RelativeLayout) findViewById(R.id.interaction_layout);
		mBtnLayout = (LinearLayout) findViewById(R.id.btn_layout);
		mBtnImage = (ImageView) findViewById(R.id.btn_image);
		mBtnImageBg = (ImageView) findViewById(R.id.btn_image_bg);
		mBtnText = (TextView) findViewById(R.id.btn_text);
		mWillNotifyText = (TextView) findViewById(R.id.will_notify_text);

		mShowNameText = (TextView) findViewById(R.id.show_name_text);
		mShowSubNameText = (TextView) findViewById(R.id.show_subname_text);
		
		mTrendingTitleText = (TextView) findViewById(R.id.trending_title);
		
		seekbar = (SeekBar)findViewById(R.id.seekbar);
		
		
		Typeface typeFace = AppManager.getInstance().getTypeFaceMedium();
		mShowNameText.setTypeface(typeFace);
		mShowSubNameText.setTypeface(typeFace);
		mBtnText.setTypeface(typeFace);
		mWillNotifyText.setTypeface(typeFace);
		mTrendingTitleText.setTypeface(typeFace);
		setTitle(null);
		
		mBtnLayout.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				switch (mState)
				{
					case CLIP_LIVE:
						showComposeView();
						break;
						
					case NOTIFY_ME:
						scheduleNotifation();
						break;

					default:
						break;
				}
			}
		});
		
		
//		mWillNotifyText.setOnClickListener(new OnClickListener()
//		{
//			
//			@Override
//			public void onClick(View v)
//			{
//				mState = State.NONE;
//				WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
//				notificationManager.cancelLocalNotificationWithProgram(mShow);
//				setShow(mShow);
//			}
//		});
		
		initSearch(mainActivity);
	}
	
	private void initSearch(MainActivity mainActivity)
	{
		mSearchTextView = ((TextView)findViewById(R.id.search_text));
		mSearchTextView.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		((Button)findViewById(R.id.clear_icon_btn)).setVisibility(View.GONE);
		((Button)findViewById(R.id.clear_icon_btn)).setOnClickListener(new View.OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				((Button)findViewById(R.id.clear_icon_btn)).setVisibility(View.GONE);

				mOnSearchListener.onClearButtonClick();
				mSearchTextView.setText("");
//				mInteractionLayout.setVisibility(View.VISIBLE);
//				mWillNotifyText.setVisibility(View.VISIBLE);
				mTrendingTitleText.setVisibility(View.VISIBLE);
				updateLayout();
				updateTexts();
			}
		});

		mSearchTextView.setOnEditorActionListener(new OnEditorActionListener()
		{
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
					
					mOnSearchListener.onSearchStringSubmitted(v.getText().toString());
					((Button)findViewById(R.id.clear_icon_btn)).setVisibility(View.VISIBLE);

//					mInteractionLayout.setVisibility(View.GONE);
//					mWillNotifyText.setVisibility(View.GONE);
					mTrendingTitleText.setVisibility(View.GONE);
//					
                }
				return false;
			}
		});
		
		mSearchTextView.setOnTouchListener(new View.OnTouchListener()
		{
			
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				v.setFocusable(true);
		        v.setFocusableInTouchMode(true);
				return false;
			}
		});
	}

	private void showComposeView() {
		
		Bundle bundle = new Bundle();
		bundle.putString(Consts.BUNDLE_CHANNEL_ID_COMPOSE, mShow.getChannel().getChannelId());
		if (mMainActivity.isLoggedIn(null, false)) {
			
			mMainActivity.replaceFragmentContent(ComposeFragment.class, bundle);
		}
		
		
	}
	
	
	private void scheduleNotifation() {
		
	
		 WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
		 notificationManager.scheduleLocalNotificationWithProgram(mShow.getNextProgram(), mShow.getShowId(), mShow.getName(), mShow.getChannel(), WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY);
		 
		// todo: store notification
		setState(State.WILL_NOTIFY);
		
	}
	

	public void setShow(RVSShow show)
	{
		mShow = show;
		
		 WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
		 String startTime = mShow.getNextProgram() != null ? String.valueOf(mShow.getNextProgram().getStart()) : "";
		String waitingForNotify = AppManager.getInstance().getPrefrences(notificationManager.getShowNotificationTag(mShow.getShowId(), show.getChannel().getChannelId(), startTime));
		 
		if (mShow.getChannel().getChannelId() == null || mShow.getChannel().getChannelId().isEmpty()) {
			Toast.makeText(getContext(), "Channel is null!!!!", Toast.LENGTH_LONG);
			return;
		}

		
		initState(show, waitingForNotify.equals("1"));
		
		// unit test
//		setState(State.NOTIFY_ME);
	}

	private void updateTexts()
	{
		mShowNameText.setText(mShow.getName());
		RVSChannel channel;
		
		switch (mState)
		{
			case CLIP_LIVE:
				updateLiveText();
//				channel = mShow.getChannel();
//				if (channel != null){
//					
//					mShowSubNameText.setText(channel.getName());
//				} else {
//					mShowSubNameText.setText("LIVE " + mShow.getName());
//				}
				break;

			case NOTIFY_ME:
				String channelName = mShow.getChannel() == null ? "" : mShow.getChannel().getName();
				String notifyMe = String.format(getResources().getString(R.string.show_notify_me_msg), getShowTime(mShow), getShowDate(mShow), channelName);
				mShowSubNameText.setText(notifyMe);
				break;

			case WILL_NOTIFY:
				String notifyText = String.format(getResources().getString(R.string.show_we_will_notify_you), mShow.getName(), getShowDate(mShow), getShowTime(mShow));
				mWillNotifyText.setText(notifyText);
				break;

			default:
				break;
		}
		
	}

	private void updateLiveText()
	{
		
		RVSProgram program = mShow.getNextProgram();
		String text, temp = null;
		if (program.getEpisodeMetadata() != null) {
			if (program.getEpisodeMetadata().getEpisodeNumber() > 0 &&
					program.getEpisodeMetadata().getSeasonNumber() > 0){
				
				temp = getResources().getString(R.string.next_program_live_episode_number_format);
	            text = String.format(temp, program.getEpisodeMetadata().getSeasonNumber(), 
	            		program.getEpisodeMetadata().getEpisodeNumber());
	            
			} else if (program.getEpisodeMetadata().getName() != null && 
					!program.getEpisodeMetadata().getName().isEmpty()) {
	            text = program.getEpisodeMetadata().getName();
			} else {
				text = program.getName();
			}
		} else {
        	text = program.getName();
        }

		mShowNameText.setText(R.string.live_now);
		mShowSubNameText.setText(text);
		
	}

	private String getShowTime(RVSShow show)
	{
		if (show.getNextProgram() == null) return "";
		
		long startTime = show.getNextProgram().getStart();
		return DateUtils.getPostTimeStamp(startTime, false);
	}

	private String getShowDate(RVSShow show)
	{
		if (show.getNextProgram() == null) return "";
		
		String day = "";
		long startTime = show.getNextProgram().getStart();
		if (DateUtils.isToday(startTime)) {
			day = getResources().getString(R.string.time_format_today);
					
		} else {
			day = DateUtils.getDay(new Date(startTime));	
		}
		return day;
	}

	private void initState(RVSShow show, boolean waitingForNotify)
	{
		RVSProgram nextProgram = show.getNextProgram();
		if (nextProgram != null) {
			
			long now = System.currentTimeMillis(); 			

			mStartTime = nextProgram.getStart(); 			 
			long end = nextProgram.getEnd(); 				
			if (mStartTime <= now && end > now) { // live

				//	    	seekbar.setVisibility(View.VISIBLE);

				long nowOffset = now - mStartTime;
				long duration = end - mStartTime;

				int progress = (int) (nowOffset / duration);

				setState(State.CLIP_LIVE);
				//	        seekbar.setProgress(progress);
			}else {
				if (waitingForNotify) {
					setState(State.WILL_NOTIFY);
				} else {
					setState(State.NOTIFY_ME);
				}
			}
		} else {
			setState(State.NO_CHANNEL);
		}
	}

	public void setState(State state)
	{

		if (mState == state) return;

		mState = state;
		updateLayout();
		updateTexts();
	}

	private void updateLayout()
	{
		switch (mState) {
			
			case CLIP_LIVE:
				
				mBtnImage.setImageResource(R.drawable.clip_focused);
				mBtnImageBg.setBackground(getResources().getDrawable(R.drawable.bg_circle_clip_tv_btn));

//				mBtnLayout.setBackgroundResource(R.color.orange);
				mBtnText.setText(R.string.show_clip_live);

				mInteractionLayout.setVisibility(View.VISIBLE);
				mWillNotifyText.setVisibility(View.GONE);
				break;

			case NOTIFY_ME:
				
				RVSProgram nextProgram = mShow.getNextProgram();
				
				mBtnImage.setImageResource(R.drawable.ic_notify_me);
				mBtnImageBg.setBackground(getResources().getDrawable(R.drawable.bg_circle_show_details_notify_btn));

//				mBtnLayout.setBackgroundResource(R.color.notify_bg);
				mBtnText.setText(R.string.show_notify_me);

				mInteractionLayout.setVisibility(View.VISIBLE);
				mWillNotifyText.setVisibility(View.GONE);
				break;

			case WILL_NOTIFY:
				mInteractionLayout.setVisibility(View.GONE);
				mWillNotifyText.setVisibility(View.VISIBLE);
				break;
				
			case NO_CHANNEL:
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)findViewById(R.id.search_layout).getLayoutParams();
				params.setMargins(0, 0, 0, 0);
				findViewById(R.id.search_layout).setLayoutParams(params);
				
				mInteractionLayout.setVisibility(View.GONE);
				mWillNotifyText.setVisibility(View.GONE);
				break;

			default:
				break;
		}
		
		updateSeekBar();

	}
	
	public void setTitle(String title) {
		
		if (title == null || title.isEmpty()) {
			mTrendingTitleText.setVisibility(View.GONE);
		} else {
			mTrendingTitleText.setVisibility(View.VISIBLE);
			mTrendingTitleText.setText(title);
		}
		
	}
	
	
	private void updateSeekBar()
	{
		
		RVSProgram nextProgram = mShow.getNextProgram();
		
		if (nextProgram != null) {
			
			long now = System.currentTimeMillis();
			long start = nextProgram.getStart();
			long end = nextProgram.getEnd();
		    if (start  <= now &&  end > now) { // live
	
		    	seekbar.setVisibility(View.VISIBLE);
		        
		        long nowOffset = now - start;
		        long duration =  end - start;
		        
		        int progress = (int)(nowOffset * 100 / duration);
		        seekbar.setProgress(progress);
		    }
		    else {
		    	seekbar.setVisibility(View.GONE);
			}
		}
		else {
	    	seekbar.setVisibility(View.GONE);
		}
	}


	//	if(!isInEditMode()){
	//		   // Your custom code that is not letting the Visual Editor draw properly
	//		   // i.e. thread spawning or other things in the constructor
	//		}
	//

}
