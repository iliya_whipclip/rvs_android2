package com.whipclip.ui.widgets;

import java.util.Date;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AnalogClock;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.utils.DateUtils;

// analog clock http://code.tutsplus.com/tutorials/build-a-custom-clock-widget-clock-design--mobile-11737
public class ProgramTimestampLayout extends RelativeLayout
{

	private TextView		mTimeText;
	private TextView		mAmPmText;
	private TextView 		mWeekDayText;
//	private MainActivity	mMainActivity;
	private WAnalogClock	mAnalogClock;
	
//	private long			mStartTime;
	
	public ProgramTimestampLayout(Context context)
	{
		super(context);

	}

	public ProgramTimestampLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ProgramTimestampLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	
	public void initLayout()
	{
//		mMainActivity = mainActivity;
		
//		mBtnLayout = (LinearLayout) findViewById(R.id.btn_layout);

//		mScheduleTextLayout = (LinearLayout) findViewById(R.id.schedule_text_layout);
		mAnalogClock = (WAnalogClock) findViewById(R.id.analog_clock);
		mTimeText = (TextView) findViewById(R.id.time_text);
		mAmPmText = (TextView) findViewById(R.id.am_pm_text);
		mWeekDayText = (TextView) findViewById(R.id.week_day_text);
		
		Typeface typeFace = AppManager.getInstance().getTypeFaceMedium();
		mTimeText.setTypeface(typeFace);
		mAmPmText.setTypeface(typeFace);
		mWeekDayText.setTypeface(typeFace);
		
		
	}
	

	public void setStartTime(long startTime)
	{
//		mEpgSlot = epgSlot;
				
		
//		initState(mEpgSlot);
		
		updateTexts(startTime);
		
		
//		
		// unit test
//		setState(State.CLIP_LIVE);
//		setState(State.NOTIFY_ME);
	}

	private void updateTexts(long startTime)
	{
//		RVSProgram program = mEpgSlot.getProgram();
//		program.getStart()
				
		String programTime = DateUtils.getPostTimeStamp(startTime, false);
		
		
		String[] fullTime = programTime.split(" ");
		if (fullTime.length > 1) {
			mTimeText.setText(fullTime[0]);
			mAmPmText.setText(fullTime[1]);
		} else {
			mTimeText.setText(programTime);
		}
		mWeekDayText.setText(getProgramDate(startTime));
		
		String[] time = fullTime[0].split(":");
		if (time.length > 1) {
			mAnalogClock.setTime(Integer.valueOf(time[0]).intValue(), Integer.valueOf(time[1]).intValue(), 0);
		} 
//			else {
//			mAnalogClock.setTime(Integer.valueOf(time[0]).intValue(), Integer.valueOf(time[1]).intValue(), 0);
//		}
		
	}

	private String getProgramDate(long startTime)
	{
		
		String day = "";
		if (DateUtils.isToday(startTime)) {
			day = getResources().getString(R.string.time_format_today);
					
		} else {
			day = DateUtils.getDay(new Date(startTime));	
		}
		return day;
	}
}
