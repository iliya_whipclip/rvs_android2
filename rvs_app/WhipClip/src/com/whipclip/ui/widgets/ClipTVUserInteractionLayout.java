package com.whipclip.ui.widgets;

import java.util.Date;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.WCNotificationManager;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSEpgSlot;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.ui.animation.ResizeAnimation;
import com.whipclip.ui.animation.ResizeWidthAnimation;

public class ClipTVUserInteractionLayout extends RelativeLayout
{
	public enum State
	{
		NONE, 
//		CLIP_LIVE_TIME, 
		CLIP_LIVE, 
//		NOTIFY_ME_TIME, 
		NOTIFY_ME, 
		WILL_NOTIFY,
	}

	private State			mState = State.NONE;
//	private LinearLayout	mBtnLayout;
	private ImageView		mBtnImage;
	private TextView		mBtnText;
	private TextView		mWillNotifyText;
	private ImageView		mBtnImageBg;
	
//	private LinearLayout 	mScheduleTextLayout;
//	private TextView		mTimeText;
//	private TextView		mAmPmText;
//	private TextView 		mWeekDayText;
	private MainActivity	mMainActivity;
	
	private long			mStartTime;
	ResizeAnimation anim;
	
	private ImageView	mAnimatedImage;
	private float	mAnimatedImageY;
	private float	mAnimatedImageX;
	private float	mAnimatedImageW;
	private float	mAnimatedImageH;
	private RelativeLayout	mWillNotifyBg;
	private boolean	mWaitingForNotify;
	private RelativeLayout	mBtnLayout;
	private boolean mEnableNotifyMe = true;
	
	private Runnable mHideWillNotifyRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			animateWillNotify(false);
			
		}
	};
	
	private RVSProgram	mProgram;
	private RVSChannel	mChannel;
	private RVSShow	mShow;

	public ClipTVUserInteractionLayout(Context context)
	{
		super(context);

	}

	public ClipTVUserInteractionLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ClipTVUserInteractionLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	private void stopButtonAnim() {
//		mAnimatedImage.setVisibility(View.GONE);
	}
	
	private void startButtonAnim() {
		
//		mAnimatedImage.setVisibility(View.VISIBLE);
		
		if (mAnimatedImageX == 0 ) {
			mAnimatedImageX = mAnimatedImage.getX();
			mAnimatedImageY = mAnimatedImage.getY();
			mAnimatedImageW = mAnimatedImage.getWidth();
			mAnimatedImageH = mAnimatedImage.getHeight();
		}
		
		mAnimatedImage.setX(mAnimatedImageX);
		mAnimatedImage.setY(mAnimatedImageY);
		mAnimatedImage.requestLayout();
		
		getHandler().postDelayed(new Runnable()
		{
			
			@Override
			public void run()
			{
				anim = new ResizeAnimation(mAnimatedImage, mAnimatedImageX, mAnimatedImageY,  mAnimatedImageW, mAnimatedImageH,
						mAnimatedImageW + 50, 
						mAnimatedImageH + 50
							);
				anim.setDuration(1000);
					
					
				  AnimationListener animListener = new Animation.AnimationListener() {

			            @Override
			            public void onAnimationStart(Animation animation) {
			                // TODO Auto-generated method stub
			
			            }
			
			            @Override
			            public void onAnimationRepeat(Animation animation) {
			                // TODO Auto-generated method stub
			
			            }
			
			            @Override
			            public void onAnimationEnd(Animation animation) {
			            	mAnimatedImage.setVisibility(View.GONE);
			            }
				  };
				  
				  anim.setAnimationListener(animListener);
				  mAnimatedImage.startAnimation(anim);
				
			}
		}, 100);
	        
		
	}
	
	public void initLayout(MainActivity mainActivity)
	{
		mMainActivity = mainActivity;
		
//		mBtnLayout = (LinearLayout) findViewById(R.id.btn_layout);
		mBtnImage = (ImageView) findViewById(R.id.btn_image);
		mBtnText = (TextView) findViewById(R.id.btn_text);
		
		mWillNotifyBg = (RelativeLayout)findViewById(R.id.will_notify_text_layout);
		mWillNotifyBg.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//		mWillNotifyBg.setDrawingCacheEnabled(false);
		
		setDrawingCacheEnabled(false);
		
		mWillNotifyText = (TextView)mWillNotifyBg.findViewById(R.id.will_notify_text);
		mWillNotifyText.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//		mWillNotifyText.setDrawingCacheEnabled(false);

		
		mBtnLayout = (RelativeLayout) findViewById(R.id.btn_image_layout);
		
		mBtnImageBg = (ImageView) findViewById(R.id.btn_image_bg);
		mAnimatedImage = (ImageView) findViewById(R.id.btn_image_anim_bg);

//		mScheduleTextLayout = (LinearLayout) findViewById(R.id.schedule_text_layout);
//		mTimeText = (TextView) findViewById(R.id.time_text);
//		mAmPmText = (TextView) findViewById(R.id.am_pm_text);
//		mWeekDayText = (TextView) findViewById(R.id.week_day_text);
		
		Typeface typeFace = AppManager.getInstance().getTypeFaceMedium();
//		mTimeText.setTypeface(typeFace);
//		mAmPmText.setTypeface(typeFace);
//		mWeekDayText.setTypeface(typeFace);
		mBtnText.setTypeface(typeFace);
		mWillNotifyText.setTypeface(typeFace);
		
//		 Handler handler = new Handler(Looper.getMainLooper());
//         handler.postDelayed(new Runnable()
//		{
//			
//			@Override
//			public void run()
//			{
//				updateBackgroundSize();
//			}
//		}, 1000);
		
		
		mBtnImageBg.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				switch (mState)
				{
//					case CLIP_LIVE_TIME:
//						animateBg();
//						break;
					case CLIP_LIVE:
						showComposeView();
//						startButtonAnim();
						break;
						
					case NOTIFY_ME:
						if (!mWaitingForNotify) {
							scheduleNotifation();
						}
						setState(State.WILL_NOTIFY);
						break;

					default:
						break;
				}
			}
		});
		
		
		
		
//		mWillNotifyText.setOnClickListener(new OnClickListener()
//		{
//			
//			@Override
//			public void onClick(View v)
//			{
//				mState = State.NONE;
//				WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
//				notificationManager.cancelLocalNotificationWithProgram(mShow);
//				setShow(mShow);
//			}
//		});

		
		
	}
	
	private void showComposeView() {
		
		Bundle bundle = new Bundle();
		bundle.putString(Consts.BUNDLE_CHANNEL_ID_COMPOSE, mChannel.getChannelId());
		bundle.putString(Consts.BUNDLE_PROGRAML_ID_COMPOSE, mProgram.getProgramId());
		bundle.putBoolean(Consts.BUNDLE_PROGRAML_IS_LIVE, true);
		if (mMainActivity.isLoggedIn(null, false)) {
			
			mMainActivity.replaceFragmentContent(ComposeFragment.class, bundle);
		}
		
		
	}
	
	
	
	private void scheduleNotifation() {
		
	
		WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
		String showName = mProgram.getSpecialShowName() != null ? mProgram.getSpecialShowName() : mProgram.getName();
		notificationManager.scheduleLocalNotificationWithProgram(mProgram, mProgram.getShowId(), showName , mChannel, WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY);
		 
		// todo: store notification
		mWaitingForNotify = true;
		setState(State.WILL_NOTIFY);
		
	}
	
	public void setShow(RVSShow show)
	{
		if (show == null) {
			setState(State.NONE);
			return;
		}
		
		setEnableNotifyMe(false);
		
		mChannel = show.getChannel();
		mProgram = show.getNextProgram();
		mShow = show;
		
		
		initState();
	}

		
	public void setEpgSlot(RVSEpgSlot epgSlot)
	{
		if (epgSlot == null) return;
		
		
		mChannel = epgSlot.getChannel();
		mProgram = epgSlot.getProgram();
		mShow = epgSlot.getShow();
		
		initState();
//		
		// unit test
//		setState(State.CLIP_LIVE);
//		setState(State.NOTIFY_ME);
	}

	private void updateTexts(RVSProgram program)
	{
		switch (mState)
		{
			case NONE:
//				mAmPmText.setText("");
//				mTimeText.setText("");
//				mWeekDayText.setText("");
				mBtnText.setTag("");
				break;
			case CLIP_LIVE:
			case NOTIFY_ME:
//				if (mProgram.getProgramId().equals("rovi_24268782_1415671200")) 
//				{
//					long start = mProgram.getStart();
//				}
				
				String programTime = getProgramTime(program);
				String[] time = programTime.split(" ");
//				if (time.length > 1) {
//					mTimeText.setText(time[0]);
//					mAmPmText.setText(time[1]);
//				} else {
//					mTimeText.setText(programTime);
//				}
//				mWeekDayText.setText(getProgramDate(program));
				// NO BREAK, CONTINUSE
				mBtnText.setText( (mState == State.CLIP_LIVE) ? R.string.show_clip_live : R.string.show_notify_me);
				break;

				
			case WILL_NOTIFY:
				String notifyText = String.format(getResources().getString(R.string.show_we_will_notify_you), mProgram.getSpecialShowName(), getProgramDate(program), getProgramTime(program));
				mWillNotifyText.setText(notifyText);
				break;

			default:
				break;
		}
		
	}

	private String getProgramTime(RVSProgram program)
	{
		long startTime = program.getStart();
		return DateUtils.getPostTimeStamp(startTime, false);
	}

	private String getProgramDate(RVSProgram program)
	{
		
		String day = "";
		long startTime = program.getStart();
		if (DateUtils.isToday(startTime)) {
			day = getResources().getString(R.string.time_format_today);
					
		} else {
			day = DateUtils.getDay(new Date(startTime));	
		}
		return day;
	}

	private void initState()
	{
		mWaitingForNotify = false;
//		if (mEpgSlot == null) return;
				
		WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
		
		String channelId = mChannel.getChannelId();
				
		if (mShow != null && mProgram != null) {
			String startTime = String.valueOf(mProgram.getStart());

			String showId = mShow.getShowId();
			String showNotificationTag = notificationManager.getShowNotificationTag(showId, channelId, startTime);
			String waitingForNotify = AppManager.getInstance().getPrefrences(showNotificationTag);
			mWaitingForNotify =  waitingForNotify.equals("1");
		} 
		
		if (mProgram != null) {
			
			long now = System.currentTimeMillis(); 			

			mStartTime = mProgram.getStart(); 			 
			long end = mProgram.getEnd(); 				
			if (mStartTime <= now && end > now) { // live

				//	    	seekbar.setVisibility(View.VISIBLE);

				long nowOffset = now - mStartTime;
				long duration = end - mStartTime;

				int progress = (int) (nowOffset / duration);

				setState(State.CLIP_LIVE);
				//	        seekbar.setProgress(progress);
			}else {
//				if (waitingForNotify) {
//					setState(State.WILL_NOTIFY);
//				} else {
					setState(State.NOTIFY_ME);
//				}
			}
		} else {
			setState(State.NONE);
		}
	}

	private void setState(State state)
	{
		mState = state;
		
		if (mState == State.NOTIFY_ME && mEnableNotifyMe == false) {
			mState = State.NONE;
		}
		
		updateLayout();
		updateTexts(mProgram);
	}

	private void updateLayout()
	{
		stopButtonAnim();
		
		Handler handler = getHandler();
		if (handler != null) {
			handler.removeCallbacks(mHideWillNotifyRunnable);
		}
		mWillNotifyBg.clearAnimation();
		
		findViewById(R.id.btn_checked).setVisibility(View.GONE);
		
		switch (mState) {
			
			
			case NONE:
//				mScheduleTextLayout.setVisibility(View.GONE);
				mBtnLayout.setVisibility(View.GONE);
				mWillNotifyText.setVisibility(View.GONE);
				break;
				
			case CLIP_LIVE:
				mBtnLayout.setVisibility(View.VISIBLE);
//				mInteractionBgLayout.setBackgroundResource(R.drawable.clip_tv_epg_indicator_live_bg);
				mBtnImage.setImageResource(R.drawable.clip_focused);
				mBtnImageBg.setBackground(getResources().getDrawable(R.drawable.bg_circle_clip_tv_btn));
//				mBtnLayout.setVisibility(View.VISIBLE);
//				mScheduleTextLayout.setVisibility(View.VISIBLE);
				
				mWillNotifyBg.setVisibility(View.GONE);
				
//				mTimeText.setTextColor(Color.WHITE);
//				mAmPmText.setTextColor(Color.WHITE);
//				mWeekDayText.setTextColor(Color.WHITE);
//				mBtnText.setTextColor(Color.WHITE);
				break;

			case NOTIFY_ME:
				mBtnLayout.setVisibility(View.VISIBLE);
//				mInteractionBgLayout.setBackgroundResource(R.drawable.clip_tv_epg_indicator_notify_bg);
				findViewById(R.id.btn_checked).setVisibility(mWaitingForNotify ? View.VISIBLE : View.GONE);
					
					
				mBtnImage.setImageResource(R.drawable.ic_notify_me);
//				mBtnLayout.setVisibility(View.VISIBLE);
//				mScheduleTextLayout.setVisibility(View.VISIBLE);
				mWillNotifyBg.setVisibility(View.GONE);
				
				mBtnImageBg.setBackground(getResources().getDrawable(R.drawable.bg_circle_clip_tv_notify_btn));
//				mTimeText.setTextColor(color);
//				mAmPmText.setTextColor(color);
//				mWeekDayText.setTextColor(color);
//				mBtnText.setTextColor(color);
				break;

			case WILL_NOTIFY:
				mBtnLayout.setVisibility(View.GONE);
				
				animateWillNotify(true);
//				mScheduleTextLayout.setVisibility(View.GONE);
//				mBtnLayout.setVisibility(View.GONE);
				mWillNotifyBg.setVisibility(View.VISIBLE);
				handler.postDelayed(mHideWillNotifyRunnable , 4000);
				break;

			default:
				break;
		}

	}
	
//	public void updateBackgroundSize() {
//		
//		int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.clip_tv_thumbnail_width);
//		int offset = getResources().getDimensionPixelSize(R.dimen.clip_tv_interaction_x_offset);
//		mInteractionBgLayout.getLayoutParams().width  =  WhipClipApplication._screenWidth - dimensionPixelSize + offset;
//	}
	
	

	private void animateWillNotify(final boolean toShow)
	{
		ResizeWidthAnimation anim = null;
		
		if (toShow) {
			int width = WhipClipApplication._screenWidth ;//mWillNotifyBg.getWidth();
			int startWidth = mBtnImage.getLayoutParams().width*2;
			mWillNotifyBg.getLayoutParams().width = mBtnImage.getLayoutParams().width;
			mWillNotifyBg.requestLayout();
			
			anim = new ResizeWidthAnimation(mWillNotifyBg, startWidth, width);
		} else {
			anim = new ResizeWidthAnimation(mWillNotifyBg, WhipClipApplication._screenWidth, mBtnImage.getLayoutParams().width*2);
		}
			
		final ResizeWidthAnimation finalAnim = anim;
		finalAnim.setDuration(300);
		
	    AnimationListener animation1Listener = new Animation.AnimationListener() {

			            @Override
			            public void onAnimationStart(Animation animation) {
			                // TODO Auto-generated method stub
			
			            }
			
			            @Override
			            public void onAnimationRepeat(Animation animation) {
			                // TODO Auto-generated method stub
			
			            }
			
			            @Override
			            public void onAnimationEnd(Animation animation) {
			            	if (!toShow) {
			            		setState(State.NOTIFY_ME);
			            	}
			            }
			};

	    anim.setAnimationListener(animation1Listener);
	    mWillNotifyBg.startAnimation(anim);
	}
	
	public void setEnableNotifyMe(boolean enable) {
		mEnableNotifyMe = enable;
	}
}
