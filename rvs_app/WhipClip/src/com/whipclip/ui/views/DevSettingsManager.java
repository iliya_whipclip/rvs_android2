package com.whipclip.ui.views;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.managers.RVSRequestManager;
import com.whipclip.rvs.sdk.util.FileManager;
import com.whipclip.ui.adapters.DevSettingsAdapter;

public class DevSettingsManager
{
	private Dialog	developmentDialog;
	private int	mPosition;
	
	public DevSettingsManager(final MainActivity context) {
		
		if (developmentDialog == null) {
			developmentDialog = new Dialog(context);
		
//			developmentDialog.setCancelable(false);
//			developmentDialog.setCanceledOnTouchOutside(false);
			developmentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			developmentDialog.setContentView(R.layout.dialog_layout);
			ListView dialogList = (ListView) developmentDialog.findViewById(R.id.dialog_list);
	
			final Properties envProperties = loadProperties("env.properties");				
			// 0 - production, 1 - integration (default), defined in env.properties file
			final int prevEnvPosition  = (int)AppManager.getInstance().getPrefrences(Consts.PREF_DEV_SETTING_ENVIROMENT, 0);
			List<String> envNamesList = new ArrayList<String>();
			final List<String> envUrlList = new ArrayList<String>();
			
			Enumeration elements = envProperties.propertyNames();
			while (elements.hasMoreElements()) {
				String key = (String) elements.nextElement();
				envNamesList.add(key);
				envUrlList.add(envProperties.getProperty(key));
		    }
			final String[] envStr = envNamesList.toArray(new String[envNamesList.size()]);
			
			ArrayList<List<String>> items = new ArrayList<List<String>>();
			List<String> list = new ArrayList<String>();
			list.add("Backend Server");
			list.add(envNamesList.get(prevEnvPosition));
			items.add(list);
			
			
			// add ip settings
			String overrideIp = context.getResources().getString(R.string.set_ip_override);
			list = new ArrayList<String>();
			list.add(overrideIp);
			items.add(list); 
			
			
			String cancelStr = context.getResources().getString(R.string.dialog_cancel);
			list = new ArrayList<String>();
			list.add(cancelStr);
			items.add(list); 
			
			DevSettingsAdapter _adapter = new DevSettingsAdapter(context, items);

			dialogList.setAdapter(_adapter);
			dialogList.setOnItemClickListener(new OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					switch (position)
					{
						case 0:
							showDevEnvDialog();
							break;
							
						case 1:
							showOverrideIpDialog();
							break;
							
					}
					if (parent.getChildCount() == (position+1)) { // CANCEL
						developmentDialog.dismiss();
					}
				}
				
				int position = -1;
						
				private void showDevEnvDialog()
				{			
					AlertDialog dialog = new AlertDialog.Builder(context)
//					AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.DialogCustom))
					.setTitle("Select environment")//getString(R.string.currency_choose))
					.setCancelable(false)
					.setSingleChoiceItems(envStr, prevEnvPosition, new DialogInterface.OnClickListener() {
					  
						public void onClick(DialogInterface dialog, int which) {
					    	position = which;
					    }
					})
					.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int id) {
					    	  if (position != -1 && position != prevEnvPosition) {
					    		  AppManager.getInstance().savePrefrences(Consts.PREF_DEV_SETTING_ENVIROMENT, position);
					    		  
					    		  AppManager.getInstance().deleteUserDetails();
								  RVSSdk.sharedRVSSdk().signOut();
								  
					    		  RVSSdk.sharedRVSSdk().setBaseUrl((String)envUrlList.get(position));
	
								 

					    		  // System.exit() does not kill your app if you have more than one
					    		  // activity on the stack. What actually happens is that the process is
					    		  // killed and immediately restarted with one fewer activity on the
					    		  // stack.
					    		  System.exit(0);
					    		  
					    	  }
					    	  
					    }
					})
					.setNegativeButton(android.R.string.cancel, null)
					.create();
					dialog.show();
					
				}
			});
		}
		
		
	}
	
	
	private void showOverrideIpDialog()
	{		
		
		
		final Properties envProperties = loadProperties("settings_ip.properties");				
		// 0 - production, 1 - integration (default), defined in env.properties file
		final int prevPosition  = 0; //(int)AppManager.getInstance().getPrefrences(Consts.PREF_DEV_SETTING_ENVIROMENT, 0);
		List<String> namesList = new ArrayList<String>();
		final List<String> valuesList = new ArrayList<String>();
		
		Enumeration elements = envProperties.propertyNames();
		while (elements.hasMoreElements()) {
			String key = (String) elements.nextElement();
			namesList.add(key);
			valuesList.add(envProperties.getProperty(key));
	    }
		final String[] namesArray = namesList.toArray(new String[namesList.size()]);
		
		
		mPosition = -1;
		
		Context context = developmentDialog.getContext();
		AlertDialog dialog = new AlertDialog.Builder(context)
//		AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.DialogCustom))
		.setTitle("Select IP")//getString(R.string.currency_choose))
//		.setCancelable(false)
		.setSingleChoiceItems(namesArray, -1, new DialogInterface.OnClickListener() {
//		  
			public void onClick(DialogInterface dialog, int which) {
				mPosition = which;
		    }
		})
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {
		    	  if (mPosition != -1) {
		    		  
		    		  RVSSdk.sharedRVSSdk().requestManager().setOverrideIp((String)valuesList.get(mPosition));
		    		  
		    		  // System.exit() does not kill your app if you have more than one
		    		  // activity on the stack. What actually happens is that the process is
		    		  // killed and immediately restarted with one fewer activity on the
		    		  // stack.
		    		  System.exit(0);
		    		  
		    	  }
		    	  
		    }
		})
		.setNegativeButton(android.R.string.cancel, null)
		.create();
		
		dialog.show();
		
	}

	private Properties loadProperties(String propertiesName)
	{
		Properties properties = new Properties();
		try
		{
			InputStream is = FileManager.getResourceFileStream(propertiesName);
    		if (is != null)
            {
    			properties.load(is);
            }
		}
		catch (IOException e)
		{
			e.printStackTrace();
        }
		return properties;
	}
	

	public void show()
	{
		if (!developmentDialog.isShowing()) {
			developmentDialog.show();
		}
	}

}
