package com.whipclip.ui.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonSwipeableViewPager extends ViewPager
{
	private boolean mSwipeEnabled = true;

	public NonSwipeableViewPager(Context context)
	{
		super(context);
	}

	public NonSwipeableViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0)
	{
		if (this.mSwipeEnabled)
		{
			return super.onInterceptTouchEvent(arg0);
		}

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (this.mSwipeEnabled)
		{
			return super.onTouchEvent(event);
		}
		return false;
	}

	public void setSwipeEnabled(boolean enabled)
	{
		this.mSwipeEnabled = enabled;
	}
}
