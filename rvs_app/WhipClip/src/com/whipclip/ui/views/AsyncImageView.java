package com.whipclip.ui.views;

import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.WeakReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.whipclip.logic.AppManager;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.remoteimageview.HTTPQueue;
import com.whipclip.ui.remoteimageview.ImageHTTPThread;

public class AsyncImageView extends ImageView
{
	private Logger	logger = LoggerFactory.getLogger(AsyncImageView.class);
	
	protected final int		BITMAP_ARRIVED		= 123;
	protected final int		LOAD_DRAWBALE		= 321;
	private String			_local;
	private String			_remote;
	private String			_fallbackUrl;
	private ImageHTTPThread	_thread				= null;
	private ProgressBar		_progressBar		= null;
	private int				_defaultImageRes	= -1;
	private boolean			_isRoundedCorner	= false;
	private boolean			_useOriginalSize	= false;
	private IDrawableCache	_cache;
	private int				_sampleSize;
	private boolean			_isBlurred			= false;
	private boolean			mUseTag 			= false;

	private boolean			mUseDefaultImage 	= true;
	
	public AsyncImageView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public AsyncImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public void setLocalURI(String local)
	{
		_local = local;
	}

	public void setIDrawableCache(IDrawableCache cache)
	{
		_cache = cache;
	}

	public void setDefualImageRes(int defaultImageRes)
	{
		_defaultImageRes = defaultImageRes;
	}
	
	public void setUseDefaultImage(boolean usability) {
		mUseDefaultImage = usability;
	}

	public boolean isRoundedCorner()
	{
		return _isRoundedCorner;
	}

	public void setRoundedCorner(boolean _isRoundedCorner)
	{
		this._isRoundedCorner = _isRoundedCorner;
	}

	public void setRemoteURI(String uri, String fallbackUri)
	{
//		uri = "http://cps-static.rovicorp.com/2/Open/E!/E%20News/2014/Gallery/_derived_jpg_q90_235x358_m0/NUP_163138_0003.jpg?partner=boazrcwqp9y4hybh2nv5sawa4thmu";
//        Log.i(getClass().getName(), "download image needed " + uri);

		_remote = uri;
		_fallbackUrl = fallbackUri;
		
//		if (uri != null && uri.startsWith("http"))
//		{
////			if (uri.startsWith("http:")) 
////				uri = uri.replace("http:", "https:");
//				
//			_remote = uri;
//			
//			_fallbackUrl = fallbackUri;
//		}
	}
	
	public void setRemoteURI(String uri)
	{
//        Log.i(getClass().getName(), "download image needed " + uri);
//		uri = "http://cps-static.rovicorp.com/2/Open/E!/E%20News/2014/Gallery/_derived_jpg_q90_235x358_m0/NUP_163138_0003.jpg?partner=boazrcwqp9y4hybh2nv5sawa4thmu";
		
		setRemoteURI(uri, null);
	}

	public String getRemoteURI()
	{
		return _remote;
	}

	public void loadImage()
	{
		logger.info("loadImage remote: " + _remote);
		if (_remote != null)
		{
			_local = getContext().getCacheDir() + "/images/" + _remote.hashCode() + ".png";

			if (_cache != null)
			{
				
				final Drawable cachedDrawable = AppManager.getInstance().getFromCache(_cache, _local);
//				final Drawable cachedDrawable = _cache.getFromCache(_local);
				if (cachedDrawable != null)
				{
					logger.info("loadImage from cache remote: " + _remote + ", local " + _local);
//					setDrawableUsingTag(cachedDrawable, getRemoteTag());
					setImageDrawable(cachedDrawable);
					invalidate();
					return;
				}
			}
			if (AppManager.getInstance().isFileExist(_local)) {
				String tag = null;
				setFromLocalFile(_local, getRemoteTag());
				return;
			}
//			File local = new File(_local); 
//			if (local.exists() && local.length() > 0)
//			{
//				String tag = null;
//				
//				setFromLocalFile(_local, getRemoteTag());
//				local = null;
//
//				return;
//			}

			showDefaultImage();
			invalidate();

			if (_progressBar != null)
			{
				_progressBar.setVisibility(VISIBLE);
			}
			File local = new File(_local); 
			local.getParentFile().mkdirs();
			queue();
			
		} else {
			showDefaultImage();
			invalidate();
		}
		
	}

	private String getRemoteTag()
	{
		if (getTag() instanceof String) return (String)getTag();

		return null;
	}

	public void showDefaultImage()
	{
		if (!mUseDefaultImage) return; 
		
		logger.info("showDefaultImage remote: " + _remote + ", default: " + _defaultImageRes);
		if (_defaultImageRes != -1)
		{
//			
			Drawable def = getResources().getDrawable(_defaultImageRes);
			if (_isRoundedCorner) {
				Bitmap bmp = BitmapFactory.decodeResource(getResources(), _defaultImageRes);
				def = new BitmapDrawable(getResources(), UiUtils.getRoundedCornerBitmap(bmp));
			}
			
			setImageDrawable(null); // test
			setImageDrawable(def);
		}
		else
		{
			setImageDrawable(null);
		}
	}

	/**
	 * Loads the image from the url first, then, if nothing is there loads the default image.
	 * This is done in order to prevent flicker image on scrolling the list.
	 */
	public void loadImageFirstUrl()
	{
		if (_remote != null)
		{
			_local = getContext().getCacheDir() + "/images/" + _remote.hashCode() + ".png";

			if (_cache != null)
			{
				final Drawable cachedDrawable = AppManager.getInstance().getFromCache(_cache, _local);
//				final Drawable cachedDrawable = _cache.getFromCache(_local);
				if (cachedDrawable != null)
				{
					logger.info("laod from cached for url: " + _remote);
//					setDrawableUsingTag(cachedDrawable, getRemoteTag());
					setImageDrawable(cachedDrawable);
					postInvalidate();
					return;
				}
			}
			if (AppManager.getInstance().isFileExist(_local)) {
				String tag = null;
				setFromLocalFile(_local, getRemoteTag());
				return;
			}
//			
//			File local = new File(_local);
//			if (local.exists() && local.length() > 0)
//			{
//				setFromLocalFile(_local, getRemoteTag());
//				local = null;
//
//				return;
//			}

			if (_progressBar != null)
			{
				_progressBar.setVisibility(VISIBLE);
			}
			File local = new File(_local);
			local.getParentFile().mkdirs();
			queue();
		}

		else if (_defaultImageRes != -1)
		{
			showDefaultImage();
		}
	}

	@Override
	public void finalize()
	{
		if (_thread != null)
		{
			HTTPQueue queue = HTTPQueue.getInstance();
			queue.dequeue(_thread);
		}
	}

	private void queue()
	{
		if (_thread == null)
		{
			_thread = new ImageHTTPThread(_remote, _fallbackUrl, _local, _handler);
			HTTPQueue queue = HTTPQueue.getInstance();
			queue.enqueue(_thread, HTTPQueue.PRIORITY_HIGH);
		}

	}

	private void setFromLocalFile(final String local, final String tag)
	{
		
		logger.info("setfromLocalFile: remote: " + _remote + ", local: " + local);
		
		if (_progressBar != null)
		{
			_progressBar.setVisibility(GONE);
		}
		_thread = null;

		// since decode may make time, set defualt image first
		showDefaultImage();
		invalidate();
		
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				try
				{

					logger.info("setfromLocalFile saveBitmap: " + local);
					final BitmapDrawable	drawable = decodeFromLocalFileToDrawable(local);
				
					logger.info("setfromLocalFile saveBitmap done: " + local);
					if (drawable != null)
					{
						if (_cache != null && drawable.getIntrinsicHeight() > 0 && drawable.getIntrinsicWidth() > 0)
						{
							logger.info("setfromLocalFile add to cache: " + local);
							AppManager.getInstance().addToCache(_cache, local, drawable);
//							_cache.addToCache(local, drawable);
						}
						
						((Activity) getContext()).runOnUiThread(new Runnable()
						{
							
							@Override
							public void run()
							{
								logger.info("setfromLocalFile run on ui thread: " + local);
								if (drawable.getIntrinsicHeight() > 0 && drawable.getIntrinsicWidth() > 0)
								{
									
									logger.info("setfromLocalFile run on ui thread, drawable good : " + local);

									
									if (_isBlurred) {
										blur(drawable.getBitmap());
									}
									
	//								Log.i(getClass().getName(), "loadImage set cached 2 from local for url: " + _remote);
//									setImageDrawable(drawable);
									setDrawableUsingTag(drawable, tag);
									invalidate();
								}
								else {
									logger.info("setfromLocalFile run on ui thread, drawable bad (set default) : " + local);
									showDefaultImage();
									invalidate();
								}
							}
						});
					}
					else {
						((Activity) getContext()).runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								showDefaultImage();
								postInvalidate();
							}
						});
					}
				}
				catch (Exception ex)
				{
					Log.e(getClass().getName(), "Loacl path = " + local + ", " + ex.getMessage(), ex);
					
					showDefaultImage();
					postInvalidate();
				}
			}

			private BitmapDrawable decodeFromLocalFileToDrawable(final String local)
			{
				BitmapDrawable drawable = null;
				// First decode with inJustDecodeBounds=true to check dimensions
				final BitmapFactory.Options options = new BitmapFactory.Options();
				if (!_useOriginalSize)
				{
					options.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(local, options);

					// Calculate inSampleSize
					options.inSampleSize = calculateInSampleSize(options, _sampleSize);

					// Decode bitmap with inSampleSize set
					options.inJustDecodeBounds = false;
					
					Log.d(getClass().getName(), String.format("save bitmap inSampleSize %d, outWidth %d, outHeight %d, _sampleSize %d",
							_sampleSize,options.outWidth, options.outHeight, _sampleSize));
				}
				
				try {
//					Bitmap bmp = BitmapFactory.decodeFile(_local, options);
					Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(local), null, options);
					if (_isRoundedCorner)
					{
						drawable = new BitmapDrawable(getResources(), UiUtils.getRoundedCornerBitmap(bmp));
					}
					else
					{
						drawable = new BitmapDrawable(getResources(), bmp);
					}
					
				} catch (OutOfMemoryError ex) {
		            ex.printStackTrace();
		            
		            Log.e(getClass().getName(), "OutOfMemoryError Loacl path = " + local + ", " + ex.getMessage(), ex);
		            // System.out.println("bitmap creating success");
		            System.gc();
		            
		            
		            try {
			            options.inSampleSize *= 2;
			            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(local), null, options);
						if (_isRoundedCorner)
						{
							drawable = new BitmapDrawable(getResources(), UiUtils.getRoundedCornerBitmap(bmp));
						}
						else
						{
							drawable = new BitmapDrawable(getResources(), bmp);
						}
		            }catch (OutOfMemoryError e) {
		            	 Log.e(getClass().getName(), "OutOfMemoryError 2 Loacl path = " + local + ", " + ex.getMessage(), ex);
		            }catch (Exception e) {
						Log.e(getClass().getName(), "Local 2 path = " + local + ", " + ex.getMessage(), ex);

		            }
		            
		        }catch (Exception ex)
				{
					Log.e(getClass().getName(), "Local 2 path = " + local + ", " + ex.getMessage(), ex);
				}
				
				return drawable;

			}
		}).start();
	}

	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth)
	{
		if (Thread.interrupted())
		{
			return 8;
		}
		final int targetSize = reqWidth;//70;
		int width_tmp = options.outWidth, height_tmp = options.outHeight;
		int scaleFactor = 1;
		while (true)
		{
			if (width_tmp / 2 < targetSize && height_tmp / 2 < targetSize)
			{
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scaleFactor *= 2;
		}

		if (scaleFactor >= 2)
		{
			scaleFactor /= 2;
		}
		return scaleFactor;
	}

	/*private void animateView()
	{
		AlphaAnimation anim = new AlphaAnimation(0.4F, 1F);
		anim.setDuration(200);
		startAnimation(anim);
	}*/

	public void setProgressBar(ProgressBar progressBar)
	{
		_progressBar = progressBar;
	}

	public ProgressBar getProgressBar()
	{
		return _progressBar;
	}

	public boolean isUseOriginalSize()
	{
		return _useOriginalSize;
	}

	public void setUseOriginalSize(boolean _useOriginalSize)
	{
		this._useOriginalSize = _useOriginalSize;
	}

//	@Override
//	public void setImageURI(Uri uri)
//	{
//		File file = new File(uri.getPath());
//		if (file.exists() && file.length() > 0)
//		{
//			_local = uri.getPath();
//			setFromLocalFile(_local, getRemoteTag());
//		}
//		else
//		{
//			super.setImageURI(uri);
//		}
//	}

	public void setSampleSize(int size)
	{
		_sampleSize = size;
	}

	
	static class MyInnerHandler extends Handler{
        WeakReference<AsyncImageView> mReference;
        
        public MyInnerHandler(AsyncImageView image) {
        	mReference = new WeakReference<AsyncImageView>(image);
        }
        
    	@Override
		public void handleMessage(Message msg)
		{
    		AsyncImageView image = mReference.get();
    		 if (mReference.get() == null)
    	            return;
    		 
    		image.logger.debug("imagelabel handler message - remote image stored to file, remote:  " + image._remote);
			if (image._remote == null || msg == null) return;
			
			String target = msg.getData().getString("url");
			String local = image.getContext().getCacheDir() + "/images/";
			local += image._remote.hashCode() + ".png";
			
//			if (msg.what == BITMAP_ARRIVED)
//			{
//				// animateView();
//				setImageBitmap((Bitmap) msg.obj);
//			}
//			else if (msg.what == LOAD_DRAWBALE)
//			{
//
//			}
//			else
//			{
				// animateView();
			image.setFromLocalFile(local, target);
//			}
		}
        
        
	};
	
	private MyInnerHandler	_handler	= new MyInnerHandler(this);
	
	public void setBlurred(boolean isBlurred) {
		_isBlurred = isBlurred;
	}
	
	private void blur(Bitmap bitmapOriginal) {
		//define this only once if blurring multiple times
		RenderScript rs = RenderScript.create(getContext());
		
		//this will blur the bitmapOriginal with a radius of 8 and save it in bitmapOriginal
		final Allocation input = Allocation.createFromBitmap(rs, bitmapOriginal); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
		final Allocation output = Allocation.createTyped(rs, input.getType());
		final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
		script.setRadius(8f);
		script.setInput(input);
		script.forEach(output);
		output.copyTo(bitmapOriginal);
	}				
	
	public void usedTag(boolean useTag) {
		this.mUseTag  = useTag;
	}
	
	public void setDrawableUsingTag(Drawable drawable, String requiredTag) {
		
		Object tag = getTag();
		
		// if this does not match, then the view has probably been reused
		if (mUseTag && requiredTag != null) {
			logger.info("setDrawableUsingTag tag " + tag + ", _remote" + _remote);
			if (tag != null && tag instanceof String && tag.equals(requiredTag)) {
				setImageDrawable(drawable);
				invalidate();
			}
		} 
		else {
			setImageDrawable(drawable);
			invalidate();
		}
	}
//	
//	@Override
//	protected void onDetachedFromWindow() {
//		
//		super.onDetachedFromWindow();
//		_handler.removeCallbacksAndMessages(null);
//	}

}