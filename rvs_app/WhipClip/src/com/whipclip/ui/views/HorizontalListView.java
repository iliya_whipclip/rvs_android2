package com.whipclip.ui.views;

import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Scroller;

import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.ui.views.TimelineView.MoveMode;

public class HorizontalListView extends AdapterView<ListAdapter>
{
	protected ListAdapter	_adapter;
	private int				_leftViewIndex		= -1;
	private int				_rightViewIndex		= 0;
	protected int			_currentX;
	protected int			_nextX;
	private int				_maxX				= Integer.MAX_VALUE;
	private int				_displayOffset		= 0;
	protected Scroller		_scroller;
	private GestureDetector	_gesture;
	private Queue<View>		_removedViewQueue	= new LinkedList<View>();
	private boolean			_dataChanged		= false;

	private Context			_context;

	public HorizontalListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		_context = context;
		initView();
	}

	private synchronized void initView()
	{
		_leftViewIndex = -1;
		_rightViewIndex = 0;
		_displayOffset = 0;
		_currentX = 0;
		_nextX = 0;
		_maxX = Integer.MAX_VALUE;
		_scroller = new Scroller(getContext());
		_gesture = new GestureDetector(getContext(), mOnGesture);
	}

	@Override
	public ListAdapter getAdapter()
	{
		return _adapter;
	}

	@Override
	public View getSelectedView()
	{
		//TODO: implement
		return null;
	}

	@Override
	public void setAdapter(ListAdapter adapter)
	{
		if (_adapter != null)
		{
			_adapter.unregisterDataSetObserver(mDataObserver);
		}
		_adapter = adapter;
		_adapter.registerDataSetObserver(mDataObserver);
		reset();

		this.setOnTouchListener(new OnTouchListener()
		{

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				ComposeFragment fragment = ((ComposeFragment) ((MainActivity) _context).getCurrentFragment());
				try
				{
					fragment.updateMarkingText(MoveMode.NONE, 0, "");
				}
				catch (Exception e)
				{
					Log.w(getClass().getName(), "An exception since the user tried to move the list, but it's not initialized yet." + e.getMessage());
				}
				switch (event.getAction())
				{
					case MotionEvent.ACTION_DOWN:
					{
						((ComposeFragment) ((MainActivity) _context).getCurrentFragment()).resetSeekBar();
						return false;
					}
					case MotionEvent.ACTION_UP:
					{
						fragment.updateLastThumbnailLocation();
//						fragment.loadVideoPreviewImage(fragment.getLastClickedThumbnailsLocation());
						fragment.initSeekBar();
						fragment.updateVideoClipMediaContext();
						return false;
					}
					default:
						return false;
				}
			}
		});
	}

	private synchronized void reset()
	{
		initView();
		removeAllViewsInLayout();
		requestLayout();
	}

	@Override
	public void setSelection(final int position)
	{
		//TODO: implement
	}

	private void addAndMeasureChild(final View child, int viewPos)
	{
		LayoutParams params = child.getLayoutParams();
		if (params == null)
		{
			params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}

		addViewInLayout(child, viewPos, params, true);
		child.measure(MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.AT_MOST));
	}

	@Override
	protected synchronized void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		super.onLayout(changed, left, top, right, bottom);

		if (_adapter == null)
		{
			return;
		}

		if (_dataChanged)
		{
			int oldCurrentX = _currentX;
			initView();
			removeAllViewsInLayout();
			_nextX = oldCurrentX;
			_dataChanged = false;
		}

		if (_scroller.computeScrollOffset())
		{
			int scrollx = _scroller.getCurrX();
			_nextX = scrollx;
		}

		if (_nextX <= 0)
		{
			_nextX = 0;
			_scroller.forceFinished(true);
		}
		if (_nextX >= _maxX)
		{
			_nextX = _maxX;
			_scroller.forceFinished(true);
		}

		int dx = _currentX - _nextX;

		removeNonVisibleItems(dx);
		fillList(dx);
		positionItems(dx);

		_currentX = _nextX;

		if (!_scroller.isFinished())
		{
			post(new Runnable()
			{
				@Override
				public void run()
				{
					requestLayout();
				}
			});

		}
	}

	private void fillList(final int dx)
	{
		int edge = 0;
		View child = getChildAt(getChildCount() - 1);
		if (child != null)
		{
			edge = child.getRight();
		}
		fillListRight(edge, dx);

		edge = 0;
		child = getChildAt(0);
		if (child != null)
		{
			edge = child.getLeft();
		}
		fillListLeft(edge, dx);

	}

	private void fillListRight(int rightEdge, final int dx)
	{
		while (rightEdge + dx < getWidth() && _rightViewIndex < _adapter.getCount())
		{

			View child = _adapter.getView(_rightViewIndex, _removedViewQueue.poll(), this);
			addAndMeasureChild(child, -1);
			rightEdge += child.getMeasuredWidth();

			if (_rightViewIndex == _adapter.getCount() - 1)
			{
				_maxX = _currentX + rightEdge - getWidth();
			}

			if (_maxX < 0)
			{
				_maxX = 0;
			}
			_rightViewIndex++;
		}

	}

	private void fillListLeft(int leftEdge, final int dx)
	{
		while (leftEdge + dx > 0 && _leftViewIndex >= 0)
		{
			View child = _adapter.getView(_leftViewIndex, _removedViewQueue.poll(), this);
			addAndMeasureChild(child, 0);
			leftEdge -= child.getMeasuredWidth();
			_leftViewIndex--;
			_displayOffset -= child.getMeasuredWidth();
		}
	}

	private void removeNonVisibleItems(final int dx)
	{
		View child = getChildAt(0);
		while (child != null && child.getRight() + dx <= 0)
		{
			_displayOffset += child.getMeasuredWidth();
			_removedViewQueue.offer(child);
			removeViewInLayout(child);
			_leftViewIndex++;
			child = getChildAt(0);

		}

		child = getChildAt(getChildCount() - 1);
		while (child != null && child.getLeft() + dx >= getWidth())
		{
			_removedViewQueue.offer(child);
			removeViewInLayout(child);
			_rightViewIndex--;
			child = getChildAt(getChildCount() - 1);
		}
	}

	private void positionItems(final int dx)
	{
		if (getChildCount() > 0)
		{
			_displayOffset += dx;
			int left = _displayOffset;
			for (int i = 0; i < getChildCount(); i++)
			{
				View child = getChildAt(i);
				int childWidth = child.getMeasuredWidth();
				child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
				left += childWidth + child.getPaddingRight();
			}
		}
	}

	@Override
	public int getFirstVisiblePosition()
	{
		try
		{
			if (getChildAt(0).getX() + getChildAt(0).getWidth() < 0)
			{
				return getChildAt(1).getId();
			}
			else
			{
				return getChildAt(0).getId();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public int[] getFirstVisiblePositionWidth()
	{
		int[] first = { 0, 0 };
		if (getChildAt(0).getX() + getChildAt(0).getWidth() < 0)
		{
			//			Log.i("hadas", "getFirstVisiblePositionWidth getChildAt(1).getId() = " + getChildAt(1).getId());
			first[0] = getChildAt(1).getId();
			first[1] = getChildAt(1).getLeft() * -1;
		}
		else
		{
			//			Log.i("hadas", "getFirstVisiblePositionWidth getChildAt(0).getId() = " + getChildAt(0).getId());
			first[0] = getChildAt(0).getId();
			first[1] = Math.abs(getChildAt(0).getLeft());
		}

		//		Log.i("hadas", "getChildAt(0).getRight() = " + getChildAt(0).getRight() + " getChildAt(0).getLeft()" + getChildAt(0).getLeft()
		//				+ "  getChildAt(1).getRight()" + getChildAt(1).getRight() + "  getChildAt(1).getLeft()" + getChildAt(1).getLeft());
		return first;
	}

	public int[] getLastVisiblePositionWidth()
	{
		int[] first = { 0, 0 };
		int lastItem = getLastVisiblePosition();
		View child = getChildAt(lastItem);
		if (child.getX() + child.getWidth() > 0)
		{
			//			Log.i("hadas", "getLastVisiblePositionWidth getChildAt(lastItem).getId() = " + child.getId());
			first[0] = child.getId();
			first[1] = Math.abs(WhipClipApplication._screenWidth - child.getRight());
		}

		//		Log.i("hadas", "lastItem = " + lastItem + " child.getRight() = " + child.getRight() + " child.getLeft()" + child.getLeft());

		return first;
	}

	public synchronized void scrollTo(int x)
	{
		_scroller.startScroll(_nextX, 0, x - _nextX, 0);
		requestLayout();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev)
	{
		if (isEnabled())
		{
			boolean handled = super.dispatchTouchEvent(ev);
			handled |= _gesture.onTouchEvent(ev);
			return handled;
		}
		return super.dispatchTouchEvent(ev);
	}

	protected boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		synchronized (HorizontalListView.this)
		{
			_scroller.fling(_nextX, 0, (int) -velocityX, 0, 0, _maxX, 0, 0);
		}
		requestLayout();

		return true;
	}

	public int getDisplayOffset()
	{
		return _displayOffset;
	}

	@Override
	public Object getItemAtPosition(int position)
	{
		return _adapter.getItem(position);
	}

	private DataSetObserver		mDataObserver	= new DataSetObserver()
												{

													@Override
													public void onChanged()
													{
														synchronized (HorizontalListView.this)
														{
															_dataChanged = true;
														}
														invalidate();
														requestLayout();
													}

													@Override
													public void onInvalidated()
													{
														reset();
														invalidate();
														requestLayout();
													}

												};

	private OnGestureListener	mOnGesture		= new GestureDetector.SimpleOnGestureListener()
												{

													@Override
													public boolean onDown(MotionEvent e)
													{
														//Toast.makeText(getContext(), "click on image", Toast.LENGTH_SHORT).show();
														_scroller.forceFinished(true);
														return true;
													}

													@Override
													public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
													{
														return HorizontalListView.this.onFling(e1, e2, velocityX, velocityY);
													}

													@Override
													public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
													{

														synchronized (HorizontalListView.this)
														{
															_nextX += (int) distanceX;
														}
														requestLayout();

														return true;
													}

													//												private boolean isEventWithinView(MotionEvent e, View child)
													//												{
													//													Rect viewRect = new Rect();
													//													int[] childPosition = new int[2];
													//													child.getLocationOnScreen(childPosition);
													//													int left = childPosition[0];
													//													int right = left + child.getWidth();
													//													int top = childPosition[1];
													//													int bottom = top + child.getHeight();
													//													viewRect.set(left, top, right, bottom);
													//													return viewRect.contains((int) e.getRawX(), (int) e.getRawY());
													//												}
												};

}
