package com.whipclip.ui.views;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnail;
import com.whipclip.rvs.sdk.dataObjects.RVSThumbnailList;
import com.whipclip.ui.DrawableCache;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.TimelineAdapter;

public class TimelineView implements OnTouchListener, IDrawableCache
{
	private class MoveData{
		RelativeLayout.LayoutParams layoutParams;
		float eventX;
		float eventRawX;
		int eventLeftY;
	}
	
	private long					_mediaContextStartOffset = -1;
	
//	public static final int			NUM_OF_FRAMES_TOTAL		= 10;
	public static final int			NUM_OF_FRAMES_TIMELINE	= 10;
	
	private static int				MARGINS_LIMIT		= 0;				// Representing how "far" we can scroll on the orange frame(don't want the user to reach the 0 margins on the left/right.
	private static int				MARGIN_OFFSET		= 70;				// Offset for handling down and move.
	public static int				LISTVIEW_HEIGHT		= 180;				// default value, will be changed on viewTreeObserver...
	
	
	private ArrayList<String>		_imagesUrl					= new ArrayList<String>();
	
	private Activity				mActivity;

	private HorizontalListView		_imagesList;
	private RelativeLayout			_videoFrame;
	private RelativeLayout			_videoFrameLeft;
	private RelativeLayout			_videoFrameRight;
	private View					_whiteLineProgress;
	
	private int			_layoutLeftMargin				= 0;
	private int			_layoutRightMargin				= 0;
	
	private ArrayList<RVSThumbnail>	_thumbnailsImages			= new ArrayList<RVSThumbnail>();
	private TimelineAdapter			_timelineAdapter;
	private int						_framesGap;
//	private int						_frameGap;
	private DrawableCache			_photosCache;
	ComposeFragment 				mComposeFragment;

	protected Handler				_handler;
	private PointF					_startRect	= new PointF();
	
	private MoveData aActionMoveData = new MoveData();

	public enum MoveMode
	{
		NONE, LEFT_BOX_DRAG, RIGHT_BOX_DRAG, MIDDLE_DRAG
	}

	private MoveMode	_moveMode			= MoveMode.NONE;
	private boolean		_isFirstClick;

	private int	mMaxClipDurationInSec		= 0;

	private Logger logger = LoggerFactory.getLogger(TimelineView.class);

	private int	mMaxLeftRight;

	private int	mNumOfFramesTotal;

	private int	_mediaContextDurationInSec;

//	private float	_videoFrameLeftX = 0;

	
	
	public TimelineView(Activity activity, ComposeFragment composeFragment, Handler handler){
		mComposeFragment = composeFragment;
		mActivity = activity;
		_handler = handler;
		
		initCache();
	}

	public void initViews(View _view)
	{
		_videoFrame = (RelativeLayout) _view.findViewById(R.id.compose_video_frame);
		_videoFrameLeft = (RelativeLayout) _view.findViewById(R.id.compose_video_frame_left);
		_videoFrameRight = (RelativeLayout) _view.findViewById(R.id.compose_video_frame_right);
		
		_whiteLineProgress = _view.findViewById(R.id.compose_progress_line); 
		
		_imagesList = (HorizontalListView) _view.findViewById(R.id.list_compose_images);
		_imagesList.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()
		{

			@Override
			public void onGlobalLayout()
			{
				_layoutRightMargin = (int) ((RelativeLayout.LayoutParams) _videoFrame.getLayoutParams()).rightMargin;
				_layoutLeftMargin = (int) ((RelativeLayout.LayoutParams) _videoFrame.getLayoutParams()).leftMargin;
				logger.info("aaaa scrollTo 1");
				initImageListScroll();
				MARGINS_LIMIT = _videoFrame.getLeft();
				LISTVIEW_HEIGHT = _imagesList.getHeight() - _imagesList.getPaddingBottom() - _imagesList.getPaddingTop();
				try
				{
					((TextView) getActivity().findViewById(R.id.action_share)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				_imagesList.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});

		_videoFrame.setOnTouchListener(this);
	}

	public void loadImages()
	{
		int firstVisibleFrameIndex  = (mNumOfFramesTotal - NUM_OF_FRAMES_TIMELINE);
		
		for (int i = 0; i < _thumbnailsImages.size(); i++)
		{
//			if (i < firstVisibleFrameIndex || i > (firstVisibleFrameIndex + NUM_OF_FRAMES_TIMELINE))
			if (i < NUM_OF_FRAMES_TIMELINE)
			{
				_imagesUrl.add("");
			}
			else
			{
				String imageUrl = _thumbnailsImages.get(i).getAsyncImage().getImageUrlForSize(new Point(20, 20));
				_imagesUrl.add(imageUrl);
			}
		}
		
		getActivity().runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				if (_timelineAdapter == null)
				{
					_timelineAdapter = new TimelineAdapter(getActivity(), _imagesUrl, TimelineView.this);
					_imagesList.setAdapter(_timelineAdapter);
//					initImageListScroll();
					
					if (_imagesList != null) {
						_imagesList.scrollTo(3*WhipClipApplication._screenWidth);
					}

					// Iliya, change to bottom
//					mComposeFragment.loadVideoPreviewImage(_framesGap * NUM_OF_FRAMES_TIMELINE/2);
					mComposeFragment.loadVideoPreviewImage(0);

					_imagesList.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()
					{
						@Override
						public void onGlobalLayout()
						{
//							_imagesList.scrollTo(WhipClipApplication._screenWidth);
							logger.info("aaaa scrollTo 2 ");
							initImageListScroll();
							mComposeFragment.setVideoSpinnerVisible(View.GONE);
							_imagesList.getViewTreeObserver().removeGlobalOnLayoutListener(this);
						}
					});

					adjustAdapterImages();

				}
				else
				{
					_timelineAdapter.notifyDataSetChanged();
				}
			}

		});
		
	}
	
	private void adjust(int seconds)
	{
		_handler.postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				for (int i = NUM_OF_FRAMES_TIMELINE; i >= 0; i--)
				{
					adjustAdapterImage(i);
				}
				_timelineAdapter.notifyDataSetChanged();
			}
		}, seconds);
	}

	private void adjustAdapterImages()
	{ //Really UGLY !! Go over this later.. The issue is that the AsyncImageView is loading 10 images at a short time, and something caused it to crash with java.io.filenotfoundexception open failed enoent
		//Here we're just making sure all the images gets loaded. 
		adjust(1000);
		adjust(3000);

	}

	private void adjustAdapterImage(int position)
	{
		if (position >= _timelineAdapter.getCount()) return;
		
		String imagePath = _timelineAdapter.getItem(position);

		if ("".equals(imagePath))
		{
			String imageUrl = _thumbnailsImages.get(position).getAsyncImage().getImageUrlForSize(new Point(20, 20));
			_imagesUrl.set(position, imageUrl);
			_timelineAdapter.updateItems(position, imageUrl);
			_timelineAdapter.notifyDataSetChanged();
		}
	}

	private Activity getActivity()
	{
		return mActivity;
	}

	/********************************************************************************************************
	 * onTouch Method : we're only using this to the videoFrame. 											*
	 * ACTION_DOWN : checking to see if the user pressed close to the frame borders (left/right).			*
	 * 				 if so, we'll initial the variables and set the move mode according to the location 	*
	 * 					of the down event(left/right).														*
	 * 				 if not, we'll call the HorizontalListView onTouch event, in order for the images 		*
	 * 					layout to move.																	    *
	 * ACTION_MOVE : checking if the user has moved the left or right border.							 	*
	 * 				 The cases are pretty similar. If new margins is bigger than MARGINS_LIMIT AND we 		*
	 * 				 didn't reach the size limit, we're updating the layout margins.						*
	 ********************************************************************************************************/
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (v.equals(_videoFrame) == false || (_thumbnailsImages == null) || _thumbnailsImages.size() == 0)
		{
			return false;
		}

		mComposeFragment.resetSeekBar();
		_whiteLineProgress.setVisibility(View.GONE);
		((RelativeLayout.LayoutParams) _whiteLineProgress.getLayoutParams()).leftMargin = _layoutLeftMargin + UiUtils.getDipMargin(10);

		try
		{
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();

			switch (event.getAction() & MotionEvent.ACTION_MASK)
			{
				case MotionEvent.ACTION_DOWN:
					_layoutRightMargin = (int) layoutParams.rightMargin;
					_layoutLeftMargin = (int) layoutParams.leftMargin;
//					logger.info("*********");
//					logger.info("aaaa changing 2 _layoutLeftMargin " + _layoutLeftMargin);
					
//					logger.info("aaaa down eventX " + event.getX());
//					logger.info("aaaa _layoutLeftMargin + MARGIN_OFFSET " + (_layoutLeftMargin + MARGIN_OFFSET));
//					logger.info("aaaa WhipClipApplication._screenWidth: " + WhipClipApplication._screenWidth);
//					logger.info("aaaa _layoutRightMargin: " + _layoutRightMargin);
//					logger.info("aaaa 1: " + (event.getX() + _layoutLeftMargin + MARGIN_OFFSET));
//					logger.info("aaaa 2: " + (WhipClipApplication._screenWidth - _layoutRightMargin));
					
//					if (event.getX() <= _layoutLeftMargin + MARGIN_OFFSET
					if (event.getX() <=  MARGIN_OFFSET
							|| (event.getX() + _layoutLeftMargin   >= (WhipClipApplication._screenWidth - _layoutRightMargin - MARGIN_OFFSET)))
					{
						//if the user has pressed close enough to the borders, we'll start and move the borders. otherwise, we'll move the HorizontalListView.
//						_moveMode = (event.getX() < 80) ? MoveMode.LEFT_BOX_DRAG : MoveMode.RIGHT_BOX_DRAG;
						_moveMode = (event.getX() < MARGIN_OFFSET) ? MoveMode.LEFT_BOX_DRAG : MoveMode.RIGHT_BOX_DRAG;
						 
						mComposeFragment.updateMarkingText(_moveMode,(int) event.getX(), event.getX() + "");
						
						_isFirstClick = !_isFirstClick;
						
						if (_moveMode == MoveMode.LEFT_BOX_DRAG)
						{
//							_videoFrameLeftX   = event.getX();
							_startRect.set(event.getX(), event.getY());
						}
					}
					else
					{
						_moveMode = MoveMode.MIDDLE_DRAG;
						mComposeFragment.setLastClickedThumbnailsLocation(event.getX());
						return v.onTouchEvent(event);
					}
					break;

				case MotionEvent.ACTION_UP:
					mComposeFragment.updateMarkingText(_moveMode, 0, "");
					mComposeFragment.updateVideoImage(_moveMode);
					mComposeFragment.setLastClickedThumbnailsLocation(0);
					_moveMode = MoveMode.NONE;
					mComposeFragment.initSeekBar();
					mComposeFragment.updateVideoClipMediaContext();
					mComposeFragment.onVideoImageClick();
					break;

				case MotionEvent.ACTION_MOVE:
					actionMove(event.getX(), event.getRawX(),  v.getLeft(), layoutParams);
					//					updateVideoClipMediaContext();
					break;
			}

			v.setLayoutParams(layoutParams);
			v.invalidate();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return true;

	}

	public void actionMove()
	{
		_moveMode = MoveMode.LEFT_BOX_DRAG;
		actionMove(aActionMoveData.eventX, aActionMoveData.eventRawX, aActionMoveData.eventLeftY, aActionMoveData.layoutParams);
	}
	
	public void actionMove(float eventX, float eventRawX, int eventLeftY, RelativeLayout.LayoutParams layoutParams)
	{
		aActionMoveData.eventX = eventX;
		aActionMoveData.eventRawX = eventRawX;
		aActionMoveData.eventLeftY = eventLeftY;
		aActionMoveData.layoutParams = layoutParams;
		
		
		mComposeFragment.videoClipPause();
		_isFirstClick = false;
		int maxLeftRight = mMaxLeftRight;
		
		if (layoutParams == null) {
			// do nothing
		} else if (_moveMode == MoveMode.LEFT_BOX_DRAG)
		{
			int newLeftMargin = (int) eventX /*- (int) _startRect.x*/+ eventLeftY; 
			if ((newLeftMargin >= MARGINS_LIMIT) && WhipClipApplication._screenWidth - maxLeftRight - _layoutRightMargin >= eventRawX)
			{ //setting limit for moving the border right (borders can't overlap). Plus, preventing the user from moving the marker to 0 margins.
				layoutParams.leftMargin = newLeftMargin;
				mComposeFragment.updateMarkingText(_moveMode, (int) layoutParams.leftMargin, layoutParams.leftMargin + " v.getLeft() = " + eventLeftY);

				RelativeLayout.LayoutParams layoutParamsLeft = (RelativeLayout.LayoutParams) _videoFrameLeft.getLayoutParams();
				layoutParamsLeft.width = (int) layoutParams.leftMargin + 10;

				_layoutLeftMargin = layoutParams.leftMargin;
//				logger.info("aaaa changing _layoutLeftMargin " + _layoutLeftMargin);
			}
		}
		else
		{
			int newRightMargin = WhipClipApplication._screenWidth - (int) eventX - eventLeftY;
			if ((newRightMargin >= MARGINS_LIMIT) && (_layoutLeftMargin + maxLeftRight <= eventRawX))
			{ //if we did reach the limit, but, we're wishing to increase the border size, it's ok. 
				layoutParams.rightMargin = newRightMargin;
				mComposeFragment.updateMarkingText(_moveMode, WhipClipApplication._screenWidth - (int) layoutParams.rightMargin - MARGIN_OFFSET,
						WhipClipApplication._screenWidth - eventX + "");

				RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) _videoFrameRight.getLayoutParams();
				layoutParamsRight.width = layoutParams.rightMargin + 10;

				_layoutRightMargin = layoutParams.rightMargin;
			}

		}
		mComposeFragment.updateVideoImage(_moveMode);

		
	}

	public void setWhiteLineVisible(int visibility){
		
		_whiteLineProgress.setVisibility(visibility);
		if (visibility == View.GONE) {
			((RelativeLayout.LayoutParams) _whiteLineProgress.getLayoutParams()).leftMargin = _layoutLeftMargin + UiUtils.getDipMargin(10);
		}
	}

	public void onSeekBarChanged(SeekBar seekBar) {
		_whiteLineProgress.setVisibility(View.VISIBLE);
		_whiteLineProgress.bringToFront();
		int totalSizePX = WhipClipApplication._screenWidth - _layoutRightMargin - _layoutLeftMargin - UiUtils.getDipMargin(15);
		double progressPrecentage = ((double) seekBar.getProgress() / (double) seekBar.getMax());
		int shouldMove = (int) (totalSizePX * (double) progressPrecentage);
		((RelativeLayout.LayoutParams) _whiteLineProgress.getLayoutParams()).leftMargin = _layoutLeftMargin + shouldMove + UiUtils.getDipMargin(10);
		_whiteLineProgress.requestLayout();		
	}

	public int[] getFirstVisiblePositionWidth() {
		 return _imagesList.getFirstVisiblePositionWidth();
	}
	
	public int[] getLastVisiblePositionWidth() {
		return _imagesList.getLastVisiblePositionWidth();
	}
	
	public int getLayoutRightMargin() {
		return _layoutRightMargin;
	}

	public int getLayoutLeftMargin() {
		return _layoutLeftMargin;
	}

	public int getMarginLimit() {
		return MARGINS_LIMIT;
	}

	public TimelineAdapter getComposeAdapter()
	{
		return _timelineAdapter;
	}


	@Override
	public void addToCache(String name, Drawable draw)
	{
//		if (isAdded())
//		{
			_photosCache.addDrawableToMemoryCache(name, draw);
//		}
	}

	@Override
	public Drawable getFromCache(String name)
	{
		return _photosCache.getDrawableFromMemCache(name);
	}
	
	private void initCache()
	{
		if (_photosCache == null)
		{
			_photosCache = new DrawableCache();
		}
	}

	public void clearPhotoCache()
	{
		if (_photosCache != null)
		{
			_photosCache.clear();
//			_photosCache = null;
		}		
	}

	public void setMaxClipDurationInSec(int maxClipDurationInSec)
	{
		mMaxClipDurationInSec = maxClipDurationInSec;
	}

	public void initThumbnailImages(RVSThumbnailList result)
	{
		//for now, assuming testMediaContext[0].getDuration() == 120 seconds. We're taking 10 samples right now. 
		for (int i = 0; i < getNumOfFramesTotal(); i++)
		{
			int size = result.getImages().size();
			int position = i * _framesGap;
			logger.info("position / size " + position + " / " + size);
			RVSThumbnail thumbnail = result.getImages().get(position < size ? position : size - 1);
			if (thumbnail != null) {
//				String imageUrl = thumbnail.getAsyncImage().getImageUrlForSize(new Point(20, 20));
//				logger.info("bbb " + i + ", url: " + imageUrl);
				_thumbnailsImages.add(thumbnail);
			}
		}
	}

	public void initFramesGap(int mediaContextDurationInSec)
	{
		_mediaContextDurationInSec = mediaContextDurationInSec;
		_framesGap = (int) Math.ceil(((float)mediaContextDurationInSec / (float)getNumOfFramesTotal())/ 2) ;
		
	}

	public void setMaxLeftRight(int maxLeftRight)
	{
		mMaxLeftRight = maxLeftRight;
		
	}

	public int getPxPerSeconds()
	{
//		Before 120 / 2 = 60 sec
//		return WhipClipApplication._screenWidth / (mediaContextDurationInSec / 2); //TODO !!!!!! /2 is just for 120 seconds !!!;
		if (mMaxClipDurationInSec == 0) return 0;
		
		return WhipClipApplication._screenWidth / mMaxClipDurationInSec;
	}
	
	public void setNumOfFamesTotal(int num) {
		mNumOfFramesTotal = num;
	}

	public int getNumOfFramesTotal()
	{
//		return NUM_OF_FRAMES_TOTAL;
		return mNumOfFramesTotal;
	}
	
	public float getScribLeftX() {
		return _startRect.x;
	}

	public void initImageListScroll()
	{
		_mediaContextStartOffset   = AppManager.getInstance().getMediaContextStartOffsetSec();
		if (_mediaContextStartOffset != -1) {
			int scrollTo = (int)_mediaContextStartOffset*getPxPerSeconds();
			logger.info("scroll to: " + scrollTo);
			_imagesList.scrollTo(scrollTo);
			
//			_moveMode = MoveMode.LEFT_BOX_DRAG;
//			 mComposeFragment.updateVideoImage(_moveMode);
			mComposeFragment.updateTextFromTranscriptBySelection();
		} else {
//			if (_imagesList.getAdapter() != null) {
//				_imagesList.scrollTo(WhipClipApplication._screenWidth);
//				_moveMode = MoveMode.LEFT_BOX_DRAG;
//				 mComposeFragment.updateVideoImage(_moveMode);
//			}
		
//		int scrollTo = (int)_mediaContextStartOffset*getPxPerSeconds();
		
//			if (_imagesList.getAdapter() != null) { 
//	//			logger.info("aaa firstVisibleFrameIndex: " + firstVisibleFrameIndex + ", size: " + _imagesList.getAdapter().getCount());
//				
//	//			int pxPerSecond = getPxPerSeconds();
//	//			int secondsPerFrame = _mediaContextDurationInSec / getNumOfFramesTotal();
//				
//				
//				_imagesList.postDelayed(new Runnable() {
//			        @Override
//			        public void run() {
//			        	int firstVisibleFrameIndex  = (mNumOfFramesTotal - NUM_OF_FRAMES_TIMELINE);
//			    		if (firstVisibleFrameIndex < 0) firstVisibleFrameIndex = 0;
//			    		
//						int pxPerFrame = WhipClipApplication._screenWidth / NUM_OF_FRAMES_TIMELINE;
//	
//						
//			        	int scrollTo = mNumOfFramesTotal * pxPerFrame;//(pxPerSecond * secondsPerFrame);
//						_imagesList.scrollTo(scrollTo);		        
//						}
//			    }, 500);
//			}
		}
//			int scrollTo = firstVisibleFrameIndex * pxPerFrame;//(pxPerSecond * secondsPerFrame);
//			_imagesList.scrollTo(scrollTo);

//			firstVisibleFrameIndex * getp
//		if (_imagesList != null) {
//			_imagesList.scrollTo(3*WhipClipApplication._screenWidth);
//		}
		
//			_imagesList.setSelection(firstVisibleFrameIndex);
//		}

	}
}
