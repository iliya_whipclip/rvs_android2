package com.whipclip.ui.animation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {


	private View mView;
    private float mToHeight;
    private float mFromHeight;

    private float mToWidth;
    private float mFromWidth;
	private float mFromX;
	private float mFromY;

    public ResizeAnimation(View v, float fromX, float fromY,  float fromWidth, float fromHeight, float toWidth, float toHeight) {
        mToHeight = toHeight;
        mToWidth = toWidth;
        mFromHeight = fromHeight;
        mFromWidth = fromWidth;
        mFromX = fromX;
        mFromY = fromY;
        mView = v;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        float height =
                (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
        float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
        
        
        mView.getLayoutParams().height = (int) height;
        mView.getLayoutParams().width = (int) width;
        mView.setX(mFromX -  ((mToWidth - mFromWidth) * interpolatedTime) /2 );
        mView.setY(mFromY - ((mToHeight - mFromHeight) * interpolatedTime) / 2);
        mView.requestLayout();
    }
    
    @Override
    public boolean willChangeBounds()
    {
        return true;
    }
}
