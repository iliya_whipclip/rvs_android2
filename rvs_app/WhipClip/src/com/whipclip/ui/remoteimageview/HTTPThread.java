package com.whipclip.ui.remoteimageview;

import java.lang.ref.SoftReference;

import android.os.Handler;
import android.os.Message;

abstract class HTTPThread extends Thread
{
	public static final int			STATUS_PENDING	= 0;
	public static final int			STATUS_RUNNING	= 1;
	public static final int			STATUS_FINISHED	= 2;

	private SoftReference<Handler>	_loadQueueHandler;

	protected int					_Status			= STATUS_PENDING;

	@Override
	public void start()
	{
		if (getStatus() == STATUS_PENDING)
		{
			synchronized (this)
			{
				_Status = STATUS_RUNNING;
			}
			super.start();
		}
	}

	public void setLoadQueueHandler(Handler handler)
	{
		_loadQueueHandler = new SoftReference<Handler>(handler);
	}

	public int getStatus()
	{
		synchronized (this)
		{
			return _Status;
		}
	}

	public void finish()
	{
		synchronized (this)
		{
			_Status = STATUS_FINISHED;
		}
		if (_loadQueueHandler != null)
		{
			Message msg = new Message();
			msg.obj = this;
			msg.what = _Status;
			_loadQueueHandler.get().sendMessage(msg);
		}
	}

	abstract public void onDequeue();
}
