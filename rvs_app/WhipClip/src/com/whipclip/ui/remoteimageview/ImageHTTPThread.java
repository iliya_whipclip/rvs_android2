package com.whipclip.ui.remoteimageview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.jdeferred.DoneCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.aysncObjects.RVSImageFromUrlOrArray;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class ImageHTTPThread extends HTTPThread
{
	private boolean					_error		= false;
	private Exception				_exception	= null;
	private String					_url;
	private String 					_fallbackUrl;
	private String					_local;
	private SoftReference<Handler>	_remoteImageHandler;
	private IOnFinishDownload		_listener;
	
	Logger logger = LoggerFactory.getLogger(ImageHTTPThread.class);

	public ImageHTTPThread(String url, String fallbackUrl, String local, Handler handler)
	{
		logger.info("ImageHTTPThread remote: "+ url + ", local: " + local);
		
		_url = url;
		_local = local;
		_fallbackUrl = fallbackUrl;
		_remoteImageHandler = new SoftReference<Handler>(handler);
	}

	public void setOnFinishedListener(IOnFinishDownload listener)
	{
		_listener = listener;
	}

	public Handler getRemoteImageHandler()
	{
		if (_remoteImageHandler != null)
		{
			return _remoteImageHandler.get();
		}
		return null;
	}

	public void setURL(String remoteURL, String localPath)
	{
		_url = remoteURL;
		_local = localPath;
	}

	public void setLocalPath(String localPath)
	{
		_local = localPath;
	}

	public String getLocalUrl()
	{
		return _local;
	}

	public String getUrl(){
		return _url;
	}
	
	@Override
	public void run()
	{
		try
		{
			if (_url != null && _url.startsWith("http"))
			{
				String urlBase = _url.substring(0, _url.lastIndexOf('/'));
				String urlPath = _url.substring(_url.lastIndexOf('/') + 1, _url.length());
				urlPath = URLEncoder.encode(urlPath, "UTF-8");
				urlPath = urlPath.replace("%3f", "?").replace("%3d", "=").replace("%3F", "?").replace("%3D", "="); // GIVI - was - urlPath = urlPath.toLowerCase().replace("%3f", "?").replace("%3d", "=");
				_url = urlBase + "/" + urlPath;

				int index = _url.indexOf('+');
				while (index != -1)
				{
					String firstStr = _url.substring(0, index);
					String lastString = _url.substring(index + 1, _url.length());
					_url = firstStr + "%20" + lastString;
					index = _url.indexOf('+');
				}

				final File file = new File(_local);
				if (file.exists() && file.length() > 0)
				{
					if (_listener != null)
					{
						_listener.finished(_local, true);
					}
					finish();
					return;
				}

				file.getParentFile().mkdirs();
				file.createNewFile();

				URL request = null;
				InputStream is = null;
				FileOutputStream fos = null;

				//Catch timout, Io etc  exception 
				try
				{
					request = new URL(_url);

					HttpURLConnection conn = (HttpURLConnection) request.openConnection();
					conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
					conn.addRequestProperty("User-Agent", "Chrome");
					conn.addRequestProperty("Referer", "google.com");

					conn.setReadTimeout(20000);
					conn.setConnectTimeout(20000);

					boolean redirect = false;

					int status = conn.getResponseCode();
					if (status != HttpURLConnection.HTTP_OK)
					{
						if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM
								|| status == HttpURLConnection.HTTP_SEE_OTHER)
						{
							redirect = true;
						}
					}

					if (redirect)
					{

						String newUrl = conn.getHeaderField("Location");

						String cookies = conn.getHeaderField("Set-Cookie");

						conn = (HttpURLConnection) new URL(newUrl).openConnection();
						conn.setRequestProperty("Cookie", cookies);
						conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
						conn.addRequestProperty("User-Agent", "Chrome");
						conn.addRequestProperty("Referer", "google.com");

					}

					is = (InputStream) conn.getInputStream();

					String suffix = "_temp";
					fos = new FileOutputStream(_local + suffix);
					byte[] buffer = new byte[4096];
					int l;
					while ((l = is.read(buffer)) > 0)
					{
						fos.write(buffer, 0, l);
					}
					
					try {
						fos.flush();
						fos.close();
						is.close();
					} catch (Exception e) {
						
					}
					
					AppManager.getInstance().renameFile(_local + suffix, _local);
				}
				catch (Exception e)
				{
					Log.e(getClass().getName(), "failed download file: " + _url);
					
					Log.e(getClass().getName(), e.getMessage(), e);
					
					if (_fallbackUrl != null && _fallbackUrl.length() > 0) {
						RVSPromise fallbackImage = RVSPromise.createPromise();
						fallbackImage.then(new DoneCallback<byte[]>() {

							@Override
							public void onDone(byte[] result)
							{
//								saveImageToFile(file, result);
								AppManager.getInstance().saveImageToFile(file, result, _url, _local, _listener);
							}
						});
						fallbackImage = RVSImageFromUrlOrArray.loadSyncImage(_fallbackUrl, fallbackImage);
					}

				}
				finally
				{
					
					if (_listener != null)
					{
						_listener.finished(_local, true);
					}
				}
			}
			else
			{
				if (_listener != null)
				{
					_listener.finished(_local, true);
				}
				else
				{
					//					SmartLog.d("432", "ImageHTTPThread - _listener is null");
				}
			}
		}
		catch (Exception e)
		{
			Log.e(getClass().getName(), e.getMessage(), e);
			e.printStackTrace();

			if (_listener != null)
			{
				_listener.finished(_local, false);
			}
			else
			{
				//				SmartLog.d("432", "ImageHTTPThread - _listener is null");
			}
		}

		finish();
	}

	public boolean hasError()
	{
		return _error;
	}

	public Exception getException()
	{
		return _exception;
	}

	@Override
	public void onDequeue()
	{
		Handler handler = getRemoteImageHandler();
		if (handler != null)
		{
			String local = getLocalUrl();
			Message msg = new Message();
			msg.obj = local;
			msg.what = _Status;
			
			Bundle bundle = new Bundle();
			bundle.putString("url", getUrl());
			msg.setData(bundle);
			
			handler.sendMessage(msg);
		}
	}

	public interface IOnFinishDownload
	{
		public void finished(String filePath, boolean isSuccess);
	}
	
//	private void saveImageToFile(File file, byte[] result)
//	{
//		InputStream is = null;
//		FileOutputStream fos = null;
//		try {
//			try {
//				Log.i(getClass().getName(), "download image finished, url " + _url + ", stored: " + _local);
//				file.getParentFile().mkdirs();
//				file.createNewFile();
//				
//				fos = new FileOutputStream(_local);
//				fos.write(result, 0, result.length);
//				fos.flush();
//				
//				if (_listener != null)
//				{
//					_listener.finished(_local, true);
//				}
//			}
//			catch (Exception e)
//			{
//				Log.e(getClass().getName(), e.getMessage(), e);
//				file.delete();
//				
//			} finally {
//				
//				
//				fos.flush();
//				fos.close();
//			}
//		}
//		catch (Exception e)
//		{
//			Log.e(getClass().getName(), e.getMessage(), e);
//			e.printStackTrace();
//			file.delete();
//		}
//		
//	}

}
