package com.whipclip.ui.remoteimageview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class HTTPEngine
{
//	public static InputStream getInputStream(String urlStr)
//	{
//		InputStream data = null;
//		URLConnection conn = null;
//		try
//		{
//			java.net.URL url = new java.net.URL(urlStr);
//			conn = url.openConnection();
//			data = conn.getInputStream();
//			// String dataStr = convertStreamToString(data);
//			//MyLogger.d("GET RESPONSE", dataStr);
//			return data;
//		}
//		catch (IOException e)
//		{
//			Log.e("HTTPEngine.getInputStream", "url " + urlStr + ", " + e.getMessage(), e);
//		}
//		finally
//		{
//			if (conn != null)
//			{
//				conn = null;
//			}
//		}
//		return null;
//	}

//	public static String getStringFromUrl(String urlStr)
//	{
//		InputStream is = getInputStream(urlStr);
//		if (is == null)
//		{
//			return null;
//		}
//		return convertStreamToString(is);
//	}

	private static String convertStreamToString(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try
		{
			while ((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
		}
		catch (Exception e)
		{
			Log.e("HTTPEngine.getInputStream", "2. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				is.close();
			}
			catch (Exception e)
			{
				Log.e("HTTPEngine.getInputStream", "3. " + e.getMessage(), e);
			}
		}
		return sb.toString();
	}

	public static Bitmap getImageByUrl(String url)
	{
		URL newUrl;
		try
		{
			newUrl = new URL(url);
			InputStream is = (InputStream) newUrl.getContent();
			Bitmap bitmap = BitmapFactory.decodeStream(is);
			return bitmap;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

}
