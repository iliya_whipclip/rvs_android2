package com.whipclip.ui;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.SparseIntArray;
import android.util.TypedValue;

import com.whipclip.WhipClipApplication;

public class UiUtils
{
	private static SparseIntArray	_values	= new SparseIntArray();

	/**
	 * Get devices Dip from pixel (margin use)
	 */
	public static int getDipMargin(int pixVal)
	{
		initValues();

		Integer num = _values.get(pixVal);

		if ((num != null) && (num != 0))
		{
			return num;
		}
		int newnum = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixVal, WhipClipApplication._context.getResources().getDisplayMetrics());
		_values.put(pixVal, newnum);
		return newnum;
	}

	private static void initValues()
	{
		if (_values == null)
		{
			_values = new SparseIntArray();
		}
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap)
	{
		try
		{
			if (bitmap != null)
			{
				if (bitmap.getWidth() >= bitmap.getHeight())
				{

					bitmap = Bitmap.createBitmap(bitmap, bitmap.getWidth() / 2 - bitmap.getHeight() / 2, 0, bitmap.getHeight(), bitmap.getHeight());

				}
				else
				{
					bitmap = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2 - bitmap.getWidth() / 2, bitmap.getWidth(), bitmap.getWidth());
				}
				// 2. create rounded corners:
				Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);// ARGB_8888);
				Canvas canvas = new Canvas(output);

				final int color = 0xff424242;
				final Paint paint = new Paint();
				final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
				final RectF rectF = new RectF(rect);

				paint.setAntiAlias(true);
				canvas.drawARGB(0, 0, 0, 0);
				paint.setColor(color);
				canvas.drawRoundRect(rectF, bitmap.getWidth() / 2, bitmap.getHeight() / 2, paint);

				paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
				canvas.drawBitmap(bitmap, rect, rect, paint);

				return output;
			}
		}
		catch (Exception ex)
		{
			//Do nothing
		}
		return bitmap;
	}

	public static boolean isImageFileType(String fileName)
	{
		return fileName.toLowerCase().contains("image") || fileName.toLowerCase().contains("png") || fileName.toLowerCase().contains("jpg")
				|| fileName.toLowerCase().contains("jpeg") || fileName.toLowerCase().contains("gif");
	}

}