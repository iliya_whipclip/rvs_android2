package com.whipclip.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;

public class DevSettingsAdapter extends ArrayAdapter<List<String>>
{
	private Context			_context;
	ArrayList<List<String>>	_data;
	private ViewHolder		_holder;

	public DevSettingsAdapter(Context context, ArrayList<List<String>> data)
	{
		super(context, R.layout.dialog_item, data); //R.layout.list_item_2		
		_context = context;
		_data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final List<String> data = _data.get(position);

		if (data.size() == 1)
		{

			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(R.layout.dialog_item, parent, false);

			_holder = new ViewHolder();
			_holder.text1 = (TextView) convertView.findViewById(R.id.dialog_item);

			convertView.setTag(_holder);

			_holder.text1.setText(data.get(0));
			if (_context.getResources().getString(R.string.dialog_cancel).equals(_holder.text1.getText()))
			{
				_holder.text1.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			}
			else
			{
				_holder.text1.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			}
			_holder.text1.setText(data.get(0));

		}
		else if (data.size() == 2)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(R.layout.list_item_2, parent, false);
			
			_holder = new ViewHolder();
			_holder.text1 = (TextView) convertView.findViewById(R.id.text1);
			_holder.text2 = (TextView) convertView.findViewById(R.id.text2);
			
			convertView.setTag(_holder);
			
			_holder.text1.setText(data.get(0));
			_holder.text2.setText(data.get(1));
			
			_holder.text1.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			_holder.text2.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		}

		return convertView;
	}

	private static class ViewHolder
	{
		TextView	text1;
		TextView	text2;
	}
}
