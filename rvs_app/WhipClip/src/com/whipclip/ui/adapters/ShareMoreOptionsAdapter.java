package com.whipclip.ui.adapters;

import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.FacebookDialog;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;

public class ShareMoreOptionsAdapter extends ArrayAdapter<String>
{
	private Context			_context;
	List<String>			_data;
	private RVSPost			mPost;
	private boolean			mIsFbChecked		= false;
	private boolean			mIsTwitterChecked	= false;
	private MainActivity	mMainActivity;

	public ShareMoreOptionsAdapter(Context context, RVSPost post, List<String> data, MainActivity mainActivity)
	{
		super(context, R.layout.dialog_item, data); //R.layout.list_item_2		
		_context = context;
		mMainActivity = mainActivity;
		_data = data;
		mPost = post;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView tv = null;
		final String data = _data.get(position);

		switch (position)
		{
			case 0:
				LayoutInflater inf = LayoutInflater.from(_context);
				convertView = inf.inflate(R.layout.share_dialog_item, parent, false);
				convertView.setEnabled(false);
				tv = (TextView) convertView.findViewById(R.id.dialog_item);

				tv.setText(data);
				tv.setTypeface(AppManager.getInstance().getTypeFaceRegular());
				tv.setTextColor(_context.getResources().getColor(R.color.gray_text));
				break;
			case 1: // facebook
			case 2: // twitter
				convertView = LayoutInflater.from(_context).inflate(R.layout.share_list_item_2, parent, false);

				final ImageView image = (ImageView) convertView.findViewById(R.id.image);
				tv = (TextView) convertView.findViewById(R.id.text);
				image.setVisibility(View.GONE);

				tv.setText(data);
				tv.setTypeface(AppManager.getInstance().getTypeFaceRegular());

				//				if (position == 1) { // facebook 		
				//					_holder.image.setImageResource(mIsFbChecked ? R.drawable.plus_selected : R.drawable.plus_non_selected);
				//				}
				//				
				//				if (position == 2) { // facebook 		
				//					_holder.image.setImageResource(mIsTwitterChecked ? R.drawable.plus_selected : R.drawable.plus_non_selected);
				//				}
				final int pos = position;
//				convertView.setOnClickListener(new View.OnClickListener()
//				{
//
//					@Override
//					public void onClick(View v)
//					{
//						shareOn(pos-1);
//						
////						if (pos == 1)
////						{ // facebook
////							mIsFbChecked = !mIsFbChecked;
////							image.setImageResource(mIsFbChecked ? R.drawable.plus_selected : R.drawable.plus_non_selected);
////						}
////
////						{ // facebook
////							mIsTwitterChecked = !mIsTwitterChecked;
////							image.setImageResource(mIsTwitterChecked ? R.drawable.plus_selected : R.drawable.plus_non_selected);
////						}
//
//					}
//				});

				break;
			case 3: // DONE
				convertView = LayoutInflater.from(_context).inflate(R.layout.dialog_item, parent, false);

				tv = (TextView) convertView.findViewById(R.id.dialog_item);
//				tv.setText(R.string.dialog_done);
				tv.setText(R.string.dialog_cancel);
				tv.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			default:
				break;
		}

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled()
	{
		return false;
	}

	@Override
	public boolean isEnabled(int position)
	{
		return position != 0;
	}

	
	private void useFallBackWebDialog(final RVSSharingUrls result)
	{
		Bundle params = new Bundle();
		params.putString(Consts.LINK, result.getUrl() + "");

		mMainActivity.shareWeb(params);

	}

}
