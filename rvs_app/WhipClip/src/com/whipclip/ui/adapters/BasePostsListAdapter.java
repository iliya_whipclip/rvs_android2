package com.whipclip.ui.adapters;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.RelatedComments;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;
import com.whipclip.ui.adapters.items.searchitems.FullPostView;
import com.whipclip.ui.adapters.items.searchitems.PostInformation;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.adapters.items.searchitems.SearchItemWeb.IHandleSearchClick;

public abstract class BasePostsListAdapter extends ArrayAdapter<PostUIItem> implements IHandleSearchClick
{
	protected ArrayList<PostUIItem>				_items;
	protected LayoutInflater					_inflater;
	protected BasePostsFragment					_fragment;

	private Dictionary<Integer, Integer>		_listViewItemHeights	= new Hashtable<Integer, Integer>();

	//We're changing the like in real time, so we can't relay on _postItem.getLikedByCaller() or postItem.getLikeCount() anymore.
	//For now, this is in use of PostListAdapter & SearchResultsAdapter.
	private HashMap<String, PostInformation>	_realTimePostInformation;

	public enum RowTypes
	{
		FULL_POST_VIEW, SEARCH_RESULTS_SINGLE, SEARCH_RESULTS_MULTIPLY, SEARCH_RESULTS_CHANNEL, SEARCH_RESULTS_SCROLL_USERS, SEARCH_WEB, SEARCH_VIDEO, SEARCH_HEADER
		//SEARCH_HEADER - for the headers apear above SEARCH_WEB & SEARCH_VIDEO
	}

	public BasePostsListAdapter(Context context, ArrayList<PostUIItem> items, BasePostsFragment fragment)
	{
		super(context, 0, items);
		_items = items;
		_inflater = LayoutInflater.from(context);
		_fragment = fragment;

		_realTimePostInformation = new HashMap<String, PostInformation>();

		insertElementsToRealtimePost(items);
	}

	@Override
	public int getViewTypeCount()
	{
		return RowTypes.values().length;
	}

	@Override
	public int getItemViewType(int position)
	{
		return _items.get(position).getViewType();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final View retView = _items.get(position).getView(position, _inflater, convertView, this);
		return retView;
	}

	public Dictionary<Integer, Integer> getItemsHeights()
	{
		return _listViewItemHeights;
	}

	@Override
	public void onSearchSuggetionClicked(String searchText, boolean search)
	{
	}

	@Override
	public void onSearchChannelClicked(String channelID, String synopsis)
	{

	}

	public MainActivity getMainActivity()
	{
		return (MainActivity) _fragment.getActivity();
	}

	public BasePostsFragment getFragment()
	{
		return _fragment;
	}

	/**
	 * _realTimePostInformation aux functions
	 */
	public void insertElementsToRealtimePost(ArrayList<PostUIItem> items)
	{
		for (PostUIItem item : items)
		{
			if (item instanceof FullPostView)
			{
				RVSPost currentItem = ((FullPostView) item).getPostItem();
				_realTimePostInformation.put(currentItem.getPostId(), new PostInformation(currentItem));
			}
		}
	}

	public void updateElementInRealtimePost()
	{
		Log.i(getClass().getName(), "updating elements to realtime post");
		try
		{
			RelatedComments relatedComments = AppManager.getInstance().getRelatedComments();
			if (relatedComments != null)
			{
				String postId = relatedComments.getRelatedPostId();
				ArrayList<RVSComment> comments = relatedComments.getRelatedComments();
				if (postId != null && comments != null)
				{
					List<RVSComment> latestsComments = new ArrayList<RVSComment>();
					int commentsSize = comments.size();
					if (commentsSize == 1)
					{
						latestsComments.add(comments.get(0));
					}
					else if (commentsSize > 1)
					{
						latestsComments.add(comments.get(commentsSize - 2));
						latestsComments.add(comments.get(commentsSize - 1));
					} //added 2 latest comments at the most.

					PostInformation information = _realTimePostInformation.get(postId);
					Log.i(getClass().getName(), "removing postId = " + postId + " from the hashmap." + "before, there were " + information.getCommentCount()
							+ " comments.");
					_realTimePostInformation.remove(postId);
					information.setLatestsComments(latestsComments);
					information.setCommentsCount(relatedComments.getRelatedTotalComments());
					_realTimePostInformation.put(postId, information);
					Log.i(getClass().getName(), "Insering postId = " + postId + " to the hashmap." + "after, there is " + information.getCommentCount()
							+ " comments.");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{ //We'd like to make sure this is reseted so that there wouldn't be any mismatches to other comments.
			//DO NOT remove this finally, since we want to reset the values, no matter what.
			AppManager.getInstance().setRelatedComments(0, null, null); //reseting the recent comment values.
		}
	}

	public void updateCommentSelected()
	{
		String postCommented = AppManager.getInstance().getLastPostCommented();
		if (postCommented != null && _realTimePostInformation.containsKey(postCommented))
		{
			_realTimePostInformation.get(postCommented).setCommmented(true);
		}
		AppManager.getInstance().setLastPostCommented(null);
	}

	public void clearRealtimePost()
	{
		_realTimePostInformation.clear();
	}

	public HashMap<String, PostInformation> getRealTimePostInformation()
	{
		return _realTimePostInformation;
	}

	/**
	 * 
	 * @param postId - the post to update from the list
	 * @param gap this will be +1 - for when the user adds a like to the post or -1 for when the user dismisses his like on the post.
	 */
	public void updateLike(String postId, int gap)
	{
		PostInformation info = _realTimePostInformation.get(postId);
		if (info != null)
		{
			int like = info.getLikeCount();
			info.setLikeCount(like + gap);
			int likedByCaller = info.getLikedByCaller();
			info.setLikedByCaller(likedByCaller + gap);
			//				Log.e("4444444444444hadas", "update item " + postId + " like to " + info.getLikeCount() + " " + " and setLikedByCaller = " + info.getLikedByCaller());
		}
	}

	/**
	 * 
	 * @param postId - the post to update from the list
	 * @param gap this will be +1 - for when the user whips the post or -1 for when the "un-whips" the post.
	 */
	public void updateRepost(String postId, int gap)
	{
		PostInformation info = _realTimePostInformation.get(postId);
		if (info != null)
		{
			int repost = info.getRepostCount();
			info.setRepostCount(repost + gap);
			repost = info.getRepostedByCaller();
			info.setRepostedByCaller(repost + gap);
		}
	}

	/**
	 * 
	 * @param postId
	 * @param commentsCount
	 * @param latestsComments
	 */
	public void updateComments(String postId, int commentsCount, List<RVSComment> latestsComments)
	{
		PostInformation info = _realTimePostInformation.get(postId);
		if (info != null)
		{
			info.setCommentsCount(commentsCount);
			info.setLatestsComments(latestsComments);
		}
	}

	public void shareImage(String path)
	{
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, _fragment.getActivity().getString(R.string.share));
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, path);

		_fragment.getActivity().startActivity(shareIntent);
	}

}
