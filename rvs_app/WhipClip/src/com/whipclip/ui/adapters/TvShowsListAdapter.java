package com.whipclip.ui.adapters;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.ui.adapters.items.searchitems.PostInformation;
import com.whipclip.ui.adapters.items.searchitems.TvShowUIItem;

public class TvShowsListAdapter extends ArrayAdapter<TvShowUIItem>
{
	protected ArrayList<TvShowUIItem>			_items;
	protected LayoutInflater					_inflater;
	protected BaseFragment						_fragment;

	private Dictionary<Integer, Integer>		_listViewItemHeights	= new Hashtable<Integer, Integer>();

	public TvShowsListAdapter(Context context, ArrayList<TvShowUIItem> items, BaseFragment fragment)
	{
		super(context, 0, items);
		_items = items;
		_inflater = LayoutInflater.from(context);
		_fragment = fragment;

	}

//	@Override
//	public int getViewTypeCount()
//	{
//		return RowTypes.values().length;
//	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final View retView = _items.get(position).getView(position, _inflater, convertView, this);
		return retView;
	}

	public Dictionary<Integer, Integer> getItemsHeights()
	{
		return _listViewItemHeights;
	}


	public MainActivity getMainActivity()
	{
		return (MainActivity) _fragment.getActivity();
	}
//
//	public BasePostsFragment getFragment()
//	{
//		return _fragment;
//	}


	
}
