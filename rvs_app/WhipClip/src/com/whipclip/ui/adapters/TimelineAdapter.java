package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.views.AsyncImageView;
import com.whipclip.ui.views.TimelineView;

public class TimelineAdapter extends ArrayAdapter<String>
{
	private Context				_context;
	private ArrayList<String>	_items;
	private IDrawableCache		_cache;

	public TimelineAdapter(Context context, ArrayList<String> items, IDrawableCache cache)
	{
		super(context, R.layout.compose_image_item, items);
		_context = context;
		_items = items;
		_cache = cache;
	}

	private ViewHolder	_holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(R.layout.compose_image_item, parent, false);
			_holder = new ViewHolder();
			_holder._image = (AsyncImageView) convertView.findViewById(R.id.compose_image);
			_holder._image.setIDrawableCache(_cache);
			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		convertView.setId(position);

		_holder._image.setLayoutParams(new LinearLayout.LayoutParams(WhipClipApplication._screenWidth / TimelineView.NUM_OF_FRAMES_TIMELINE,
				TimelineView.LISTVIEW_HEIGHT));
		_holder._image.setDefualImageRes(R.drawable.compose_image);
		_holder._image.setSampleSize(WhipClipApplication._screenWidth / TimelineView.NUM_OF_FRAMES_TIMELINE);
		_holder._image.setRemoteURI(_items.get(position));
		_holder._image.loadImage();

//		Log.w("hadas", "Position = " + position + "width = " + _holder._image.getLayoutParams().width + " height = " + _holder._image.getLayoutParams().height);

		return convertView;
	}

	public void updateItems(int position, String url)
	{
		if (url == null || "".equals(url))
		{
			return;
		}

		if ("".equals(_items.get(position)) == false)
		{
			_items.set(position, url);
			Log.i("hadas", "update item " + position + " with url : " + url);
		}
	}

	static class ViewHolder
	{
		AsyncImageView	_image;
	}

	@Override
	public int getCount()
	{
		return _items.size();
	}
	
	public static int getPxPerFrame() {
		return WhipClipApplication._screenWidth / TimelineView.NUM_OF_FRAMES_TIMELINE;
	}

}
