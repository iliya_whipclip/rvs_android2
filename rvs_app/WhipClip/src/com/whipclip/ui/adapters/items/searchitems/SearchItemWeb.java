package com.whipclip.ui.adapters.items.searchitems;

import java.io.ObjectInputStream.GetField;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;

public class SearchItemWeb implements PostUIItem
{
	private final String	_name;

	public SearchItemWeb(String name)
	{
		super();
		_name = name;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_WEB.ordinal() - 5;
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, final BasePostsListAdapter ad)
	{
		if (convertView == null)
		{
			convertView = (View) inflater.inflate(R.layout.search_item_web, null);
		}

		TextView textView = (TextView) convertView.findViewById(R.id.search_item_web);
		textView.setText(_name);

		textView.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		convertView.setTag(_name);

		convertView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				ad.onSearchSuggetionClicked(_name, true);
			}
		});

		return convertView;
	}

	public String getName()
	{
		return _name;
	}

	public interface IHandleSearchClick
	{
		public void onSearchSuggetionClicked(String searchText, boolean search);

		public void onSearchChannelClicked(String channelID, String synopsis);
	}

}
