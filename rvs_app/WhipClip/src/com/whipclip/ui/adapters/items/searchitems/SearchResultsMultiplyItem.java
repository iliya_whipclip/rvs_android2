package com.whipclip.ui.adapters.items.searchitems;

import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSSearchMatch;

public class SearchResultsMultiplyItem
{
	private String			_title;
	private String			_subTitle;		//time
	private String			_imageUrl;
	private RVSAsyncImage	_image;
	private RVSMediaContext	_mediaContext;

	public SearchResultsMultiplyItem(RVSSearchMatch seachItem)
	{
		_image = seachItem.getCoverImage();
		_imageUrl = _image.getImageUrlForSize(Consts.IMAGE_POINT);
		_title = seachItem.getValueFragment().toLowerCase();
		_subTitle = DateUtils.getStyledTimeFull(seachItem.getReferenceTime(), true, true);
		_mediaContext = seachItem.getMediaContext();
	}

	public String getTitle()
	{
		return _title;
	}

	public String getSubTitle()
	{
		return _subTitle;
	}

	public String getImageUrl()
	{
		return _imageUrl; //basically this is : _image.getImageUrlForSize(Consts.IMAGE_POINT).
	}

	public RVSAsyncImage getImage()
	{
		return _image;
	}

	public RVSMediaContext getMediaContext()
	{
		return _mediaContext;
	}
}
