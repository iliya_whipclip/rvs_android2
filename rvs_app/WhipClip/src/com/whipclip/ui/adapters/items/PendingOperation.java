package com.whipclip.ui.adapters.items;

public class PendingOperation
{
	private OperationType	_type;
	private Object			_data1;
	private Object			_data2;
	private Object			_data3; //Latest comment # 1
	private Object			_data4; //Latest comment # 2

	public enum OperationType
	{
		COMPOSE_CHANNEL, COMPOSE_VIDEO, WHIP, LIKE, FEED, COMMENT, FOLLOW, NOTIFICATIONS;
	}

	public PendingOperation(OperationType type, Object data1, Object data2)
	{
		_type = type;
		_data1 = data1;
		_data2 = data2;
	}

	public PendingOperation(OperationType type, Object data1, Object data2, Object data3, Object data4)
	{
		_type = type;
		_data1 = data1;
		_data2 = data2;
		_data3 = data3;
		_data4 = data4;
	}

	public OperationType getType()
	{
		return _type;
	}

	public void setType(OperationType type)
	{
		this._type = type;
	}

	public Object getData1()
	{
		return _data1;
	}

	public void setData1(Object data1)
	{
		this._data1 = data1;
	}

	public Object getData2()
	{
		return _data2;
	}

	public void setData2(Object data2)
	{
		this._data2 = data2;
	}

	public Object getData3()
	{
		return _data3;
	}

	public void setData3(Object data3)
	{
		this._data3 = data3;
	}

	public Object getData4()
	{
		return _data4;
	}

	public void setData4(Object data4)
	{
		this._data4 = data4;
	}

}
