package com.whipclip.ui.adapters.items.searchitems;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;

public class SearchResultsSingle implements PostUIItem
{
	private Context					_context;

	//This will be null if the item is received as a single.
	//In case we've received a multiply item, BUT, "converted" it to single than the cover image will be this, since we need to present the searchResult.getCoverImage and not the program.getCoverImage().
	private final RVSAsyncImage		_coverImage;
	private String					_title;

	private RVSProgramExcerpt		_program;
	ViewHolder						_holder;
	private String					_searchQuery;
	private BasePostsListAdapter	_adapter;
	private RVSMediaContext			_mediaContext	= null;
	private int	imageHeight;

	public SearchResultsSingle(Context context, RVSProgramExcerpt program, String title, RVSAsyncImage coverImage, String searchQuery,
			RVSMediaContext mediaContext)
	{ //constructor for multiply item that is now converted to single item.
		super();
		_context = context;
		_program = program;
		_title = title;
		_coverImage = coverImage;
		_searchQuery = searchQuery;
		_mediaContext = mediaContext;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_RESULTS_SINGLE.ordinal();
	}

	@Override
	public View getView(final int position, LayoutInflater inflater, View convertView, BasePostsListAdapter adapter)
	{
		_adapter = adapter;
		RVSProgramExcerpt program = _program;
		if (convertView == null || !(convertView.getTag() instanceof ViewHolder))
		{
			convertView = (View) inflater.inflate(R.layout.search_results_single, null);

			_holder = new ViewHolder();
			_holder.title = (TextView) convertView.findViewById(R.id.search_result_title);
			_holder.subTitle = (TextView) convertView.findViewById(R.id.search_result_subtitle);
			_holder.image = (AsyncImageView) convertView.findViewById(R.id.post_image);
			_holder.image.setTag(convertView.findViewById(R.id.video_place_holder));
			_holder.netLogo = (AsyncImageView) convertView.findViewById(R.id.network_image);
			_holder.postShowText = (TextView) convertView.findViewById(R.id.post_video_text);
			_holder.clipLayout = (RelativeLayout) convertView.findViewById(R.id.whip_container);
			_holder.refreshProgressBar = (ProgressBar) convertView.findViewById(R.id.refresh_progress_bar);
			_holder.postError = (TextView) convertView.findViewById(R.id.post_error);

			_holder.title.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			_holder.subTitle.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.postShowText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			((TextView) convertView.findViewById(R.id.clip_live)).setTypeface(AppManager.getInstance().getTypeFaceRegular());

			IDrawableCache cache = AppManager.getInstance();

			_holder.image.setIDrawableCache(cache);
			_holder.netLogo.setIDrawableCache(cache);

			convertView.setTag(_holder);
			
			_holder.image.setScaleType(ScaleType.FIT_CENTER);

			
			_holder.image.setLayoutParams(new RelativeLayout.LayoutParams(WhipClipApplication._screenWidth, 
					(int) (WhipClipApplication._screenWidth * 0.5625D)));

			
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		if (_searchQuery != null) {
			String[] searchQueries = _searchQuery.split(" ");
			String title = _title;
			for (String word : searchQueries)
			{
				if (title != null && title.toLowerCase().contains(word.toLowerCase())
						&& (searchQueries.length == 1 || (searchQueries.length > 1 && word.length() > 2)))
				{ //if _searchQuery is a sentence, we won't mark 2 letter words.
					title = title.replaceAll("(?i)" + word, "<font color='black'><b>" + word + "</b></font>");
				}
				_holder.title.setText(Html.fromHtml(title));
			}
			//just making sure that _searchQuery will be marked fully, if existed.
			if ((searchQueries.length > 1) && (title != null && title.toLowerCase().contains(_searchQuery.toLowerCase())))
			{
				title = title.replaceAll("(?i)" + _searchQuery, "<font color='black'><b>" + _searchQuery + "</b></font>");
				_holder.title.setText(Html.fromHtml(title));
			}
		} else {
			
			_holder.title.setText("");
		}

		long startTime = program.getProgram().getStart();
		_holder.subTitle.setText(DateUtils.getStyledTimeFull(startTime, true, true));
		_holder.postShowText.setText(program.getProgram().getSpecialShowName());

		//setting image fields
		_holder.image.setDefualImageRes(R.drawable.video_logo);
		_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
		RVSAsyncImage image = program.getCoverImage();
		String fallbackUrl = (program.getProgram() != null && program.getProgram() != null && program.getProgram().getImage() != null) ? 
				program.getProgram().getImage().getImageUrlForSize(Consts.IMAGE_POINT) : null;

		if (_coverImage != null || image != null)
		{
			String imageUrl = (_coverImage == null) ? image.getImageUrlForSize(Consts.IMAGE_POINT) : _coverImage.getImageUrlForSize(Consts.IMAGE_POINT);
			_holder.image.setRemoteURI(imageUrl, fallbackUrl);
		}
		else
		{
			_holder.image.setRemoteURI(fallbackUrl, null);
		}
		_holder.image.loadImage();

		if (_program != null && _program.getChannel() != null)
		{
			_holder.netLogo.setRemoteURI(_program.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)));
			_holder.netLogo.setSampleSize(UiUtils.getDipMargin(20));
			_holder.netLogo.loadImage();
		}

		if (_program.getMediaContext() != null && (!RVSMediaContext.STATUS_OK.equals(_program.getMediaContext().getStatus())))
		{
			_holder.postError.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.postError.setVisibility(View.VISIBLE);
		}
		else
		{
			_holder.postError.setVisibility(View.GONE);
		}

		_holder.clipLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false))
				{
					Bundle bundle = new Bundle();
					bundle.putString(Consts.BUNDLE_TEXT_COMPOSE, _holder.title.getText().toString());
					AppManager.getInstance().setCurrentMediaContext(_mediaContext, -1);
					((BaseActivity) _context).replaceFragmentContent(ComposeFragment.class, bundle);
				}
				else
				{
					_adapter.getFragment().addPendingOperation(
							new PendingOperation(OperationType.COMPOSE_VIDEO, _mediaContext, _holder.title.getText().toString()));
				}
			}
		});

		_holder.image.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String channel = _program.getChannel() != null ? _program.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)) : null;

				if (_holder.postError.getVisibility() != View.VISIBLE)
				{
					if (_program.getProgram() != null)
					{
						_adapter.getFragment().handleVideoClick(_mediaContext, v, _program.getProgram().getSpecialShowName(), _holder.image.getRemoteURI(),
								position, channel, _holder.refreshProgressBar, null, null);
						
					}
					else if (_program.getChannel() != null)
					{
						_adapter.getFragment().handleVideoClick(_mediaContext, v, _program.getChannel().getName(), _holder.image.getRemoteURI(), position,
								channel, _holder.refreshProgressBar, null, null);
					}
					
					AnalyticsManager.sharedManager().trackClipPlaybackEventWithProgram(_program.getProgram(), _program.getChannel(), _mediaContext, AnalyticsManager.RVSAppAnalyticsManagerSourceTypeProgramAired, false, _adapter.getFragment().getCurrentTabPosition());

				}
			}
		});

		return convertView;
	}

	public String getSearchQuery()
	{
		return _searchQuery;
	}

	public void setSearchQuery(String searchQuery)
	{
		this._searchQuery = searchQuery;
	}

	private static class ViewHolder
	{
		TextView		title;
		TextView		subTitle;
		AsyncImageView	image;
		AsyncImageView	netLogo;
		TextView		postShowText;

		RelativeLayout	clipLayout;
		ProgressBar		refreshProgressBar;
		TextView		postError;
	}
}
