package com.whipclip.ui.adapters.items;

public class DrawerMenuItem
{
	private int		_imageResource;
	private String	_text;

	public DrawerMenuItem()
	{
		super();
	}

	public DrawerMenuItem(int imageId, String text)
	{
		super();
		_imageResource = imageId;
		_text = text;
	}

	public int getImageResource()
	{
		return _imageResource;
	}

	public String getText()
	{
		return _text;
	}

}
