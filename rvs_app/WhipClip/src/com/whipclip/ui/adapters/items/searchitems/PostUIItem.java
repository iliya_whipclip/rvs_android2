package com.whipclip.ui.adapters.items.searchitems;

import android.view.LayoutInflater;
import android.view.View;

import com.whipclip.ui.adapters.BasePostsListAdapter;

public interface PostUIItem
{
	public int getViewType();

	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter postsListAdapter);
}
