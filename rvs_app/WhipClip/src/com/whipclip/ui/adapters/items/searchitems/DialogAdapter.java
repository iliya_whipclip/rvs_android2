package com.whipclip.ui.adapters.items.searchitems;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;

public class DialogAdapter extends ArrayAdapter<String>
{
	private Context				_context;
	private ArrayList<String>	_items;

	public DialogAdapter(Context context, ArrayList<String> items)
	{
		super(context, R.layout.dialog_layout, items);
		_context = context;
		_items = items;
	}

	private ViewHolder	_holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(R.layout.dialog_item, parent, false);
			_holder = new ViewHolder();
			_holder._itemText = (TextView) convertView.findViewById(R.id.dialog_item);
			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		convertView.setId(position);

		_holder._itemText.setText(_items.get(position));
		if (_context.getResources().getString(R.string.dialog_cancel).equals(_holder._itemText.getText()))
		{
			_holder._itemText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		}
		else
		{
			_holder._itemText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		}
		return convertView;
	}

	static class ViewHolder
	{
		TextView	_itemText;
	}

}
