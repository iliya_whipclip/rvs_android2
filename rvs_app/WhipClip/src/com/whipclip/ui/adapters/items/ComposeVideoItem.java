package com.whipclip.ui.adapters.items;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;
import com.whipclip.ui.views.AsyncImageView;

public class ComposeVideoItem implements PostUIItem
{
	private ViewHolder				_holder;
	private BasePostsListAdapter	_adapter;
	private LayoutInflater			_li;
	private IDrawableCache			_cache;
	private Context					_context;

	private RVSPost					_postItem;

	public ComposeVideoItem(Context context, RVSPost postItem, IDrawableCache cache)
	{
		_postItem = postItem;
		_li = LayoutInflater.from(context);
		_cache = cache;
		_context = context;
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter postsListAdapter)
	{
		_adapter = postsListAdapter;
		RVSPost post = _postItem;
		if (convertView == null)
		{
			convertView = _li.inflate(R.layout.compose_image_item, null);
			_holder = new ViewHolder();
			_holder.image = (AsyncImageView) convertView.findViewById(R.id.compose_image);

			_holder.image.setIDrawableCache(_cache);
			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		_holder.image.setDefualImageRes(R.drawable.video_logo);
		_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
		_holder.image.setRemoteURI(post.getCoverImage().getImageUrlForSize(
				new Point(WhipClipApplication._screenWidth / 4, WhipClipApplication._screenWidth / 4)));//urls[position % urls.length]);//
		_holder.image.loadImage();

		return convertView;
	}

	@Override
	public int getViewType()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	private static class ViewHolder
	{
		AsyncImageView	image;
	}

}
