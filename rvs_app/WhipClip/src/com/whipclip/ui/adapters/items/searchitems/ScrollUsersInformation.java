package com.whipclip.ui.adapters.items.searchitems;

import com.whipclip.rvs.sdk.dataObjects.RVSUser;

public class ScrollUsersInformation
{
	private boolean	_followedByMe;
	private int		_followerCount;

	public ScrollUsersInformation(RVSUser user)
	{
		super();
		_followedByMe = user.isFollowedByMe();
		_followerCount = user.getFollowerCount();
	}

	public boolean isFollowedByMe()
	{
		return _followedByMe;
	}

	public void setFollowedByMe(boolean followedByCaller)
	{
		_followedByMe = followedByCaller;
	}

	public int getFollowerCount()
	{
		return _followerCount;
	}

	public void setFollowedCount(int followedCount)
	{
		if (followedCount < 0)
		{
			_followerCount = 0; //shouldn't happen, just a precaution.
		}
		else
		{
			_followerCount = followedCount;
		}
	}

}
