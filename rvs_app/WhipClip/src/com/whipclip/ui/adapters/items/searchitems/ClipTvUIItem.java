package com.whipclip.ui.adapters.items.searchitems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSEpgSlot;
import com.whipclip.rvs.sdk.dataObjects.RVSImage;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.ClipTvListAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.views.AsyncImageView;
import com.whipclip.ui.widgets.ClipTVUserInteractionLayout;
import com.whipclip.ui.widgets.ProgramTimestampLayout;

public class ClipTvUIItem
{


	private ClipTvListAdapter	mAdapter;
	private RVSEpgSlot			mEpgSlot;
	private IDrawableCache		mCache;
	private MainActivity		mContext;
	private int 				mIndex;
	private Logger				logger  = LoggerFactory.getLogger(ClipTvUIItem.class);
	private boolean 			mShowTimestamp = false;
	private RVSShow	mShow;
	
	public class ClipTvViewHolder
	{
		AsyncImageView	image;
		SeekBar			seekbar;
		ClipTVUserInteractionLayout	clipTvUserInteraction;
		public ProgramTimestampLayout	mAnalogClockLayout;
		
	}
	
	// used in tv shows tab
	public ClipTvUIItem(MainActivity context, RVSShow  show, IDrawableCache cache, int index)
	{
		mShow = show;
		mEpgSlot = null;
		mCache = cache;
		mContext = context;
		mIndex = index;
	}
	
	
	// used in Clip Tv tab
	public ClipTvUIItem(MainActivity context, RVSEpgSlot epgSlot, IDrawableCache cache, int index)
	{
		mShow = null;
		mEpgSlot = epgSlot;
		mCache = cache;
		mContext = context;
		mIndex = index;
	}
	
	public RVSEpgSlot getEpgSlot() {
		return mEpgSlot;
	}
	
	public View getView(int position, LayoutInflater inflater, View convertView, ClipTvListAdapter adapter) {
	
			mAdapter = adapter;
			final RVSEpgSlot epgSlot = mEpgSlot;
			ClipTvViewHolder mHolder = null;

			if (convertView == null || !(convertView.getTag() instanceof ClipTvViewHolder))
			{
				convertView = inflater.inflate(R.layout.clip_tv_list_item_layout, null);
				mHolder = new ClipTvViewHolder();
				
				mHolder.image = (AsyncImageView) convertView.findViewById(R.id.show_image_wrapper).findViewById(R.id.show_image);
//				mHolder.image = (AsyncImageView) convertView.findViewById(R.id.show_image);
				mHolder.seekbar = (SeekBar)convertView.findViewById(R.id.seekbar);
//				mHolder.seekbar.setBackgroundColor(Color.WHITE);
//				mHolder.showTitle = (TextView) convertView.findViewById(R.id.show_title);
				
				mHolder.clipTvUserInteraction = (ClipTVUserInteractionLayout)convertView.findViewById(R.id.clip_tv_user_interact_item);
				mHolder.clipTvUserInteraction.initLayout(mContext);
				
				
//				mHolder.channelTitle = (TextView) convertView.findViewById(R.id.channel_title);
				
				
//				mHolder.showTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
//				mHolder.channelTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
				
				mHolder.image.setIDrawableCache(mCache);
				mHolder.image.usedTag(true);
				
				mHolder.mAnalogClockLayout = (ProgramTimestampLayout)convertView.findViewById(R.id.analog_clock_layout);
				mHolder.mAnalogClockLayout.initLayout();

				mHolder.image.setLayoutParams(new RelativeLayout.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenWidth * 9 / 16));
				mHolder.image.requestLayout();
				
				convertView.setTag(mHolder);
				
			} else {
				mHolder = (ClipTvViewHolder) convertView.getTag();
			}
			
			if (mShow != null) {
				mHolder.clipTvUserInteraction.setShow(mShow);
				
			} else {
				mHolder.clipTvUserInteraction.setEpgSlot(mEpgSlot);
			}
			
			
			if (epgSlot == null && mShow == null) return convertView; 
			
			
			
			
//			RelativeLayout l = (RelativeLayout) convertView.findViewById(R.id.show_image_wrapper);
//			l.setLayoutParams(new AbsListView.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenWidth * 9 / 16));
//			l.setLayoutParams(new LinearLayout.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenWidth * 9 / 16));
			
//			mHolder.image.setLayoutParams(new RelativeLayout.LayoutParams(WhipClipApplication._screenWidth, WhipClipApplication._screenWidth * 9 / 16));
//			mHolder.image.requestLayout();
			
			if (mShowTimestamp) {
				mHolder.mAnalogClockLayout.setVisibility(View.VISIBLE );
				mHolder.mAnalogClockLayout.setStartTime(mEpgSlot.getProgram().getStart());
			} else {
				mHolder.mAnalogClockLayout.setVisibility( View. GONE);
			}
			
			showIsLiveIndicator(mHolder.seekbar);
			
			try{
	//			mHolder.image.setImageBitmap(null);
	//			mHolder.image.setDefualImageRes(R.drawable.video_logo);
				
				
				mHolder.image.setDefualImageRes(R.drawable.video_logo);
				
				RVSAsyncImage image = null;
				if (mShow != null) {
					image = mShow.getImage();
				} else {
					if (epgSlot.getShow() != null) {
						image = epgSlot.getShow().getImage();
					} else {
						image = epgSlot.getProgram().getImage();
					}
				}
				
				if (image != null) {
					
					int heightDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenHeight / 3, mContext);
					int widthDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenWidth, mContext);
					
					String imageUrl = "";
					if (image.isContainsType(RVSImage.RVSImageTypeShowImage)) {
						imageUrl = image.getImageUrlForSize(new Point(widthDp , heightDp), RVSImage.RVSImageTypeShowImage);
			        }
			        else if (image.isContainsType(RVSImage.RVSImageTypeDefaultShowImage)) {
						imageUrl = image.getImageUrlForSize(new Point(widthDp , heightDp), RVSImage.RVSImageTypeDefaultShowImage);
			        }
			        else 
			        {
			        	imageUrl = image.getImageUrlForSize(new Point(widthDp , heightDp));
			        }
					

//
//					logger.info("imagelabel ");
//					logger.info("imagelabel getview index " + position + " imageUrl :" + imageUrl );
//					String _local = mContext.getCacheDir() + "/images/" + imageUrl.hashCode() + ".png";
//					logger.info("imagelabel getview index " + position + " local :" + _local );
					
					
					
					mHolder.image.setSampleSize(WhipClipApplication._screenWidth / 3);
					mHolder.image.setTag(imageUrl);
					mHolder.image.setRemoteURI(imageUrl);
					
				} else {
					mHolder.image.setRemoteURI(null);
					mHolder.image.setTag(null);
				}
				
				
				mHolder.image.loadImage();
				

				
			}catch (Exception e) {
				logger.error("set tag for position: " + position + " crash ");
			}
			
			
			if (mShow != null) {
				convertView.setOnClickListener(new View.OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
	//					TvShowUIItem item = (TvShowUIItem) adapter.getItemAtPosition(position);
	//					
	//					openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
	//				
						MainActivity mainActivity = mAdapter.getMainActivity();
						
						AppManager.getInstance().setShow(mShow);
						mainActivity.showShowDetailsPage( mShow.getShowId(), mShow.getChannel().getChannelId());
					}
				});
			} else {
				final RVSShow show = mEpgSlot.getShow();
				if (show != null) {
					convertView.setOnClickListener(new View.OnClickListener()
					{
						
						@Override
						public void onClick(View v)
						{
		//					TvShowUIItem item = (TvShowUIItem) adapter.getItemAtPosition(position);
		//					
		//					openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
		//				
							MainActivity mainActivity = mAdapter.getMainActivity();
							
							AppManager.getInstance().setShow(show);
							mainActivity.showShowDetailsPage(show.getShowId(), mEpgSlot.getChannel().getChannelId());
						}
					});
				} else {
				
					convertView.setOnClickListener(null);
				}
			}
				
			
//			if (show != null && show.getImage() != null) {
////				_holder.image.setDefualImageRes(R.drawable.ic_launcher);
//				String imageUrlForSize = show.getImage().getImageUrlForSize(new Point(40, 40));
//				if (imageUrlForSize == null) {
//					Log.e(TvShowUIItem.class.getName(), "imagw null");
//				}
//				
////				_holder.image.setDefualImageRes(R.drawable.video_logo);
//				_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
//				_holder.image.setRemoteURI(show.getImage().getImageUrlForSize(
//						new Point(WhipClipApplication._screenWidth / 3, WhipClipApplication._screenWidth / 3)));
//				_holder.image.loadImage();
//				
//				
//			} 
//			else {
//				Log.e(TvShowUIItem.class.getName(), "imagw null");
//			}
			
//			convertView.setOnClickListener(new View.OnClickListener()
//			{
//				
//				@Override
//				public void onClick(View v)
//				{
////					TvShowUIItem item = (TvShowUIItem) adapter.getItemAtPosition(position);
////					
////					openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
////				
//					
//				}
//			});
//				
			
//			convertView.addOnLayoutChangeListener(new OnLayoutChangeListener()
//			{
//				
//				@Override
//				public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom)
//				{
//					// TODO Auto-generated method stub
//					
//				}
//			})(new ViewTreeObserver.OnGlobalLayoutListener() {
//				@Override
//				public void onGlobalLayout() {
//				   //Do it here
//				   LinearLayout layoutGet=(LinearLayout) findViewById(R.id.GameField1);
//				   LayoutParams layParamsGet= layoutGet.getLayoutParams();
//				   int width=layParamsGet.width;
//				}
//			});
//			
			
			
			return convertView;
	}
	
	public void showTimestamp(boolean visible)
	{
//		if (visible ) {
			mShowTimestamp = visible;
//			mHolder.mAnalogClockLayout.setVisibility(View.VISIBLE);
			
//		} else {
//			mHolder.mAnalogClockLayout.setVisibility(View.GONE);
//		}
			
	}
	
	public boolean isTimestampVisible() {
		return mShowTimestamp;
	}
	
	private void showIsLiveIndicator(SeekBar seekbar)
	{
		seekbar.setVisibility(View.GONE);
		
		long now = System.currentTimeMillis();
		
		RVSProgram nextProgram = null;
		
		if (mShow != null) {
			nextProgram = mShow.getNextProgram();
		} else {
			nextProgram = mEpgSlot.getProgram();
		}
		
		if (nextProgram != null) {
			long start = nextProgram.getStart();
			long end = nextProgram.getEnd();
		    if (start  <= now &&  end > now) { // live
	
		    	seekbar.setVisibility(View.VISIBLE);
		        
		        long nowOffset = now - start;
		        long duration =  end - start;
		        
		        int progress = (int)(nowOffset * 100 / duration);
		        seekbar.setProgress(progress);
		    }
		}
	}


//	public String getShowName() {
//		if (mHolder == null) return "";
//		
//		return mHolder.showTitle.getText().toString();
//	}
	
	public int getIndex() {
		return mIndex;
	}
	
}
