package com.whipclip.ui.adapters.items.searchitems;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;

public class SearchItemVideo implements PostUIItem
{
	private Context					_context;
	private RVSChannel				_channel;
	private BasePostsListAdapter	_adapter;
	private String					_synopsis;

	public SearchItemVideo(Activity activity, RVSChannel channel, String syn)
	{
		super();
		_context = activity;
		_channel = channel;
		_synopsis = syn;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_VIDEO.ordinal() - 5;
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter ad)
	{
		_adapter = ad;
		if (convertView == null)
		{
			convertView = (View) inflater.inflate(R.layout.search_item_video, null);
		}

		TextView textViewSubTitle = (TextView) convertView.findViewById(R.id.search_item_video_subtitle);
		if (_channel.getCurrentProgram() != null && _channel.getCurrentProgram() != null)
		{
			textViewSubTitle.setText(_channel.getCurrentProgram().getSpecialShowName());
		}
		else
		{
			textViewSubTitle.setText(_channel.getName());
		}
		textViewSubTitle.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		AsyncImageView channelIcon = (AsyncImageView) convertView.findViewById(R.id.channel_image);
		if (_channel.getLogo() != null)
		{
			channelIcon.setRemoteURI(_channel.getLogo().getImageUrlForSize(new Point(20, 20)));
			channelIcon.setSampleSize(UiUtils.getDipMargin(30));
			channelIcon.setRoundedCorner(true);
			channelIcon.loadImage();
		}
		convertView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_adapter == null) return;
				
				if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false))
				{
					_adapter.onSearchChannelClicked(_channel.getChannelId(), _synopsis);
				}
				else
				{
					_adapter.getFragment().addPendingOperation(new PendingOperation(OperationType.COMPOSE_CHANNEL, _channel.getChannelId(), _synopsis));
				}
			}
		});

		TextView text = (TextView) convertView.findViewById(R.id.search_clip_live);
		text.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		return convertView;
	}
}
