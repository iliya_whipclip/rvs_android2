package com.whipclip.ui.adapters.items.searchitems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;

public class SearchItemHeader implements PostUIItem
{
	private Context	_context;
	private String	_title;

	public SearchItemHeader(Context context, String title)
	{
		super();
		_context = context;
		_title = title;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_HEADER.ordinal() - 5;
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter postsListAdapter)
	{
		convertView = (View) inflater.inflate(R.layout.search_item_header, null);

		TextView textView = (TextView) convertView.findViewById(R.id.search_item_header);
		textView.setText(_title);

		textView.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		convertView.setTag(_title);

		return convertView;
	}

}
