package com.whipclip.ui.adapters.items.searchitems;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSProgramExcerpt;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.NuxPagerAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;
import com.whipclip.ui.views.NonSwipeableViewPager;
import com.whipclip.ui.widgets.WCCustomSwipeToRefresh;

public class SearchResultsMultiply implements PostUIItem
{
	private Logger logger 	= LoggerFactory.getLogger(SearchResultsMultiply.class);
	
	private Context									_context;
	private ViewHolder								_holder;
	private ArrayList<SearchResultsMultiplyItem>	_multiplyItems;
	private String									_searchQuery;
	private RVSProgramExcerpt						_program;
	private BasePostsListAdapter					_adapter;

	public SearchResultsMultiply(Context context, ArrayList<SearchResultsMultiplyItem> items, RVSProgramExcerpt program)
	{
		super();
		_context = context;
		_multiplyItems = items;
		_program = program;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_RESULTS_MULTIPLY.ordinal();
	}

	@Override
	public View getView(final int position, LayoutInflater inflater, View convertView, BasePostsListAdapter ad)
	{
		_adapter = ad;
		if (convertView == null || !(convertView.getTag() instanceof ViewHolder))
		{
			convertView = (View) inflater.inflate(R.layout.search_results_multiply, null);

			_holder = new ViewHolder();
			_holder.image = (AsyncImageView) convertView.findViewById(R.id.post_image);
			_holder.image.setTag(convertView.findViewById(R.id.video_place_holder));
			_holder.netLogo = (AsyncImageView) convertView.findViewById(R.id.network_image);
			_holder.postShowText = (TextView) convertView.findViewById(R.id.post_video_text);

			_holder.viewPager = (NonSwipeableViewPager) convertView.findViewById(R.id.search_results_view_pager);
			
			_holder.indicator1 = (ImageView) convertView.findViewById(R.id.search_result_indicator1);
			_holder.indicator2 = (ImageView) convertView.findViewById(R.id.search_result_indicator2);
			_holder.indicator3 = (ImageView) convertView.findViewById(R.id.search_result_indicator3);
			_holder.indicator4 = (ImageView) convertView.findViewById(R.id.search_result_indicator4);
			_holder.indicator5 = (ImageView) convertView.findViewById(R.id.search_result_indicator5);
			_holder.indicator6 = (ImageView) convertView.findViewById(R.id.search_result_indicator6);
			
//			((TextView) convertView.findViewById(R.id.clip_live)).setTypeface(AppManager.getInstance().getTypeFaceRegular());

			_holder.clipLayout = (RelativeLayout) convertView.findViewById(R.id.whip_container);
			_holder.refreshProgressBar = (ProgressBar) convertView.findViewById(R.id.refresh_progress_bar);
			_holder.postError = (TextView) convertView.findViewById(R.id.post_error);

			_holder.postShowText.setTypeface(AppManager.getInstance().getTypeFaceMedium());

			IDrawableCache cache = AppManager.getInstance();
			_holder.image.setIDrawableCache(cache);
			_holder.netLogo.setIDrawableCache(cache);

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		if (_program.getProgram() != null)
		{
			_holder.postShowText.setText(_program.getProgram().getSpecialShowName());
		}
		else if (_program.getChannel() != null)
		{
			_holder.postShowText.setText(_program.getChannel().getName());
		}
		else
		{
			_holder.postShowText.setText("");
		}

		_holder.viewPager.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1)
			{
				return arg0.onTouchEvent(arg1);
			}
		});

		
		ArrayList<SearchResultsMultiplyItem> arrayList = _holder.multiplyItems != null ? _holder.multiplyItems.get() : null;
		if (arrayList == null || arrayList != _multiplyItems) {
			List<View> pages = new ArrayList<View>();
	
			final ArrayList<ImageView> pageIndicators = new ArrayList<ImageView>();
			pageIndicators.add(_holder.indicator1);
			pageIndicators.add(_holder.indicator2);
			pageIndicators.add(_holder.indicator3);
			pageIndicators.add(_holder.indicator4);
			pageIndicators.add(_holder.indicator5);
			pageIndicators.add(_holder.indicator6);
	
			resetPageIndicator(pageIndicators);
			for (int i = 0; i < _multiplyItems.size(); i++)
			{
				addItemToViewPager(pages, _multiplyItems.get(i));
				pageIndicators.get(i).setVisibility(View.VISIBLE);
			}
			for (int i = _multiplyItems.size(); i < pageIndicators.size(); i++)
			{
				pageIndicators.get(i).setVisibility(View.INVISIBLE);
			}
	
			NuxPagerAdapter pagerAdapter = new NuxPagerAdapter(pages);
	
			_holder.viewPager.setAdapter(pagerAdapter);
			_holder.viewPager.setCurrentItem(0);
			_holder.multiplyItems = new WeakReference<ArrayList<SearchResultsMultiplyItem>>(_multiplyItems);
					
			_holder.viewPager.setOnPageChangeListener(new OnPageChangeListener()
			{
				@Override
				public void onPageSelected(int idx)
				{
					_holder.pageCurrentPage = idx;
					setPageIndicator(pageIndicators, idx);
				}
	
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2)
				{
				}
	
				@Override
				public void onPageScrollStateChanged(int position)
				{
				}
			});
		}
		else {
			
			_holder.viewPager.setCurrentItem(_holder.pageCurrentPage);
//			_holder.viewPager.getAdapter().notifyDataSetChanged();
		}
			
//			_holder.viewPager.setOnTouchListener(new View.OnTouchListener() {
//			    @Override
//			    public boolean onTouch(View v, MotionEvent event) {
//			    	ListView view = (ListView)((RelativeLayout)((RelativeLayout)v.getParent()).getParent()).getParent();
//			    	NonSwipeableViewPager parent = (NonSwipeableViewPager)view.getParent();
//			    	parent.setEnabled(false);
//			        switch (event.getAction()) {
//			            case MotionEvent.ACTION_UP:
//			            	parent.setEnabled(true);
//			                break;
//			        }
//			        return false;
//			    }
//			});

		_holder.image.setDefualImageRes(R.drawable.video_logo);
		_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
		try
		{
			if (_holder.viewPager.getCurrentItem() < _multiplyItems.size()) {
				_holder.image.setRemoteURI(_multiplyItems.get(_holder.viewPager.getCurrentItem()).getImageUrl());
			} else {
				logger.error("_holder.viewPager.getCurrentItem(): " + _holder.viewPager.getCurrentItem() + " _multiplyItems.size(): " + _multiplyItems.size());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		_holder.image.loadImage();

		if (_program != null && _program.getChannel() != null)
		{
			_holder.netLogo.setRemoteURI(_program.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)));
			_holder.netLogo.setSampleSize(UiUtils.getDipMargin(20));
			_holder.netLogo.loadImage();
		}

		if (_program.getMediaContext() != null && (!RVSMediaContext.STATUS_OK.equals(_program.getMediaContext().getStatus())))
		{
			_holder.postError.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.postError.setVisibility(View.VISIBLE);
		}
		else
		{
			_holder.postError.setVisibility(View.GONE);
		}

		_holder.clipLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				SearchResultsMultiplyItem item = _multiplyItems.get(_holder.viewPager.getCurrentItem());
				long currentTime = 0; 

				BasePostsFragment fragment = _adapter.getFragment();
				if (fragment.getVideoView() != null) 
					currentTime = fragment.getCurrentPlaybackTime();
				
				AppManager.getInstance().setCurrentMediaContext(item.getMediaContext(),currentTime);
				if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false))
				{
					Bundle bundle = new Bundle();
					bundle.putString(Consts.BUNDLE_TEXT_COMPOSE, item.getTitle());
					((BaseActivity) _context).replaceFragmentContent(ComposeFragment.class, bundle);
				}
				else
				{
					_adapter.getFragment().addPendingOperation(new PendingOperation(OperationType.COMPOSE_VIDEO, item.getMediaContext(), item.getTitle()));
				}
			}
		});

		_holder.image.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				RVSMediaContext mediaContext = _multiplyItems.get(_holder.viewPager.getCurrentItem()).getMediaContext();
				if (mediaContext != null)
				{
					String channel = _program.getChannel() != null ? _program.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)) : null;

					if (_holder.postError.getVisibility() != View.VISIBLE)
					{
						boolean isFirstTimePlayback = false;
						if (_program.getProgram() != null)
						{
							isFirstTimePlayback = _adapter.getFragment().handleVideoClick(mediaContext, v, _program.getProgram().getSpecialShowName(), _holder.image.getRemoteURI(),
									position, channel, _holder.refreshProgressBar, null, null);
						}
						else if (_program.getChannel() != null)
						{
							isFirstTimePlayback = _adapter.getFragment().handleVideoClick(mediaContext, v, _program.getChannel().getName(), _holder.image.getRemoteURI(), position,
									channel, _holder.refreshProgressBar, null, null);
						}
						
						if (isFirstTimePlayback) {
							AnalyticsManager.sharedManager().trackClipPlaybackEventWithProgram(_program.getProgram(), _program.getChannel(), mediaContext, AnalyticsManager.RVSAppAnalyticsManagerSourceTypeProgramAired, false, _adapter.getFragment().getCurrentTabPosition());
						}
					}
				}
			}
		});

		return convertView;
	}

	private void setPageIndicator(ArrayList<ImageView> pageIndicators, final int position)
	{
		_adapter.getFragment().stopVideo();
		if (position >= Consts.SEARCH_RESULTS_MULTIPLY_LIMIT)
		{
			Log.e(_context.getApplicationContext().getPackageName(), "Watch out - there are more than " + Consts.SEARCH_RESULTS_MULTIPLY_LIMIT
					+ " items in the search results. The results are fixed to " + Consts.SEARCH_RESULTS_MULTIPLY_LIMIT + " at the most.");
			return;
		}
		pageIndicators.get(position).setImageResource(R.drawable.p_orange);
		if (pageIndicators.size() > position + 1)
		{
			pageIndicators.get(position + 1).setImageResource(R.drawable.p_gray);
		}
		if (position > 0)
		{
			pageIndicators.get(position - 1).setImageResource(R.drawable.p_gray);
		}

		_holder.image.setDefualImageRes(-1);//R.drawable.video_logo);
		_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
		_holder.image.setRemoteURI(_multiplyItems.get(_holder.viewPager.getCurrentItem()).getImageUrl());
		_holder.image.loadImage();

	}

	private void resetPageIndicator(ArrayList<ImageView> pageIndicators)
	{
		pageIndicators.get(0).setImageResource(R.drawable.p_orange);
		for (int i = 1; i < pageIndicators.size(); i++)
		{
			pageIndicators.get(i).setImageResource(R.drawable.p_gray);
		}
	}

	private void addItemToViewPager(List<View> container, SearchResultsMultiplyItem item)
	{
		LayoutInflater inflater = LayoutInflater.from(_context);
		View page = inflater.inflate(R.layout.search_results_text_layout, null);

		TextView txtTitle = (TextView) page.findViewById(R.id.search_result_title);
		String title = item.getTitle();

		String[] searchQueries = _searchQuery.split(" ");
		for (String word : searchQueries)
		{
			if (title != null && title.toLowerCase().contains(word.toLowerCase())
					&& (searchQueries.length == 1 || (searchQueries.length > 1 && word.length() > 2)))
			{ //if _searchQuery is a sentence, we won't mark 2 letter words.
				title = title.replaceAll("(?i)" + word, "<font color='black'><b>" + word + "</b></font>");
			}
			txtTitle.setText(Html.fromHtml(title));
		}
		//just making sure that _searchQuery will be marked fully, if existed.
		if ((searchQueries.length > 1) && (title != null && title.toLowerCase().contains(_searchQuery.toLowerCase())))
		{
			title = title.replaceAll("(?i)" + _searchQuery, "<font color='black'><b>" + _searchQuery + "</b></font>");
			txtTitle.setText(Html.fromHtml(title));
		}

		TextView txtSubTitle = (TextView) page.findViewById(R.id.search_result_subtitle);
		txtSubTitle.setText(item.getSubTitle());

		txtTitle.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		txtSubTitle.setTypeface(AppManager.getInstance().getTypeFaceLight());

		container.add(page);
	}

	public String getSearchQuery()
	{
		return _searchQuery;
	}

	public void setSearchQuery(String searchQuery)
	{
		this._searchQuery = searchQuery;
	}

	private static class ViewHolder
	{
		protected int	pageCurrentPage;
		AsyncImageView	image;
		AsyncImageView	netLogo;
		TextView		postShowText;

		NonSwipeableViewPager	viewPager;
		ImageView		indicator1;
		ImageView		indicator2;
		ImageView		indicator3;
		ImageView		indicator4;
		ImageView		indicator5;
		ImageView		indicator6;

		RelativeLayout	clipLayout;
		ProgressBar		refreshProgressBar;
		TextView		postError;
		
		WeakReference<ArrayList<SearchResultsMultiplyItem>>	multiplyItems;

	}
}
