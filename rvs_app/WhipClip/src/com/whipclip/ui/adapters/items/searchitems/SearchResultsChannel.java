package com.whipclip.ui.adapters.items.searchitems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;

public class SearchResultsChannel implements PostUIItem
{
//	private IDrawableCache			_cache;
	private Context					_context;

	private RVSChannel				_channel;
	ViewHolder						_holder;
	private BasePostsListAdapter	_adapter;
	private String					_synopsis;

	public SearchResultsChannel(Context context, RVSChannel channel, String syn)
	{
		super();
		_context = context;
		_channel = channel;
		_synopsis = syn;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_RESULTS_CHANNEL.ordinal();
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter adapter)
	{
		_adapter = adapter;
		final RVSChannel channel = _channel;

		if (convertView == null || !(convertView.getTag() instanceof ViewHolder))
		{
			convertView = (View) inflater.inflate(R.layout.search_results_channel, null);

			_holder = new ViewHolder();
			_holder.title = (TextView) convertView.findViewById(R.id.search_result_title);
			_holder.subTitle = (TextView) convertView.findViewById(R.id.search_result_subtitle);
			_holder.image = (AsyncImageView) convertView.findViewById(R.id.channel_image);
			_holder.whipLayout = (LinearLayout) convertView.findViewById(R.id.whip_layout);
			
			((TextView) convertView.findViewById(R.id.clip_live)).setTypeface(AppManager.getInstance().getTypeFaceRegular());

			IDrawableCache cache = AppManager.getInstance();
			_holder.image.setIDrawableCache(cache);
			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		//setting search fields
		RVSProgram program = channel.getCurrentProgram();
		_holder.title.setText(channel.getName());
		if (program != null)
		{
			_holder.subTitle.setText(program.getSpecialShowName());
		}
		else
		{
			_holder.subTitle.setText(channel.getName());
		}

		_holder.title.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_holder.subTitle.setTypeface(AppManager.getInstance().getTypeFaceLight());
		_holder.subTitle.setTextColor(_context.getResources().getColor(R.color.gray_text));

		//setting image fields
		if (channel != null && channel.getLogo() != null)
		{
			RVSAsyncImage image = channel.getLogo();
			_holder.image.setDefualImageRes(R.drawable.logo);
			_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
			_holder.image.setRemoteURI(image.getImageUrlForSize(Consts.IMAGE_POINT));
			_holder.image.loadImage();
		}
		_holder.whipLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false))
				{
					_adapter.onSearchChannelClicked(_channel.getChannelId(), _synopsis);
				}
				else
				{
					_adapter.getFragment().addPendingOperation(new PendingOperation(OperationType.COMPOSE_CHANNEL, _channel.getChannelId(), _synopsis));
				}
			}
		});

		return convertView;
	}

	private static class ViewHolder
	{
		TextView		title;
		TextView		subTitle;
		AsyncImageView	image;

		LinearLayout	whipLayout;
	}

}
