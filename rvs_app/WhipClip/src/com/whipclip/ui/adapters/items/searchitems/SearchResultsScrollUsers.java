package com.whipclip.ui.adapters.items.searchitems;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.SearchFragment;
import com.whipclip.fragments.SearchedUsersFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.NuxPagerAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;

public class SearchResultsScrollUsers implements PostUIItem
{
	private Context					_context;
	private ViewHolder				_holder;
	private ArrayList<RVSUser>		_users;
	private String					_searchedUser;
	private BasePostsListAdapter	_postsListAdapter;

	private List<View>				_pages;
	private NuxPagerAdapter		_pagerAdapter;

	//saving the current viewPager's location, in order to retrieve it once we're coming back from profile or from view all click.
	private int						_profileSave	= -1;

	private String					_whips;

	public SearchResultsScrollUsers(Context context, ArrayList<RVSUser> users, String searchedUser)
	{
		super();
		_context = context;
		_users = users;
		_searchedUser = searchedUser;

		AppManager.getInstance().insertElementsToRealtimeScrollUsers(_users);

		_whips = _context.getResources().getString(R.string.whips);
	}

	@Override
	public int getViewType()
	{
		return RowTypes.SEARCH_RESULTS_SCROLL_USERS.ordinal();
	}

	@Override
	public View getView(int position, LayoutInflater inflater, View convertView, BasePostsListAdapter postsListAdapter)
	{
		_postsListAdapter = postsListAdapter;
		if (convertView == null || !(convertView.getTag() instanceof ViewHolder))
		{
			convertView = (View) inflater.inflate(R.layout.search_results_scroll_users, null);
			_holder = new ViewHolder();

			_holder.viewPager = (ViewPager) convertView.findViewById(R.id.search_results_view_pager);
			_holder.indicator1 = (ImageView) convertView.findViewById(R.id.search_result_indicator1);
			_holder.indicator2 = (ImageView) convertView.findViewById(R.id.search_result_indicator2);
			_holder.indicator3 = (ImageView) convertView.findViewById(R.id.search_result_indicator3);
			_holder.indicator4 = (ImageView) convertView.findViewById(R.id.search_result_indicator4);
			_holder.indicator5 = (ImageView) convertView.findViewById(R.id.search_result_indicator5);
			_holder.indicator6 = (ImageView) convertView.findViewById(R.id.search_result_indicator6);
			_holder.viewAll = (TextView) convertView.findViewById(R.id.search_result_view_all);

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		final ArrayList<ImageView> pageIndicators = new ArrayList<ImageView>();
		pageIndicators.add(_holder.indicator1);
		pageIndicators.add(_holder.indicator2);
		pageIndicators.add(_holder.indicator3);
		pageIndicators.add(_holder.indicator4);
		pageIndicators.add(_holder.indicator5);
		pageIndicators.add(_holder.indicator6);

		_holder.viewPager.setOnTouchListener(new OnTouchListener()
		{

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1)
			{
				return arg0.onTouchEvent(arg1);
			}
		});

		if (_profileSave == -1)
		{
			_pages = new ArrayList<View>();

			resetPageIndicator(pageIndicators);

			for (int i = 0; i < _users.size() && i < Consts.MAX_SEARCH_RESULTS_USERS; i++)
			{
				addItemToViewPager(_pages, _users.get(i));
				pageIndicators.get(i).setVisibility(View.VISIBLE);
			}
			for (int i = _users.size(); i < pageIndicators.size(); i++)
			{
				pageIndicators.get(i).setVisibility(View.INVISIBLE);
			}

			_pagerAdapter = new NuxPagerAdapter(_pages);
			_holder.viewPager.setAdapter(_pagerAdapter);
			_holder.viewPager.setCurrentItem(_profileSave);

			for (int i = 0; i < _users.size() && i < Consts.MAX_SEARCH_RESULTS_USERS; i++)
			{
				pageIndicators.get(i).setVisibility(View.VISIBLE);
			}
			for (int i = _users.size(); i < pageIndicators.size(); i++)
			{
				pageIndicators.get(i).setVisibility(View.INVISIBLE);
			}

			if (_users.size() == 1)
			{
				convertView.findViewById(R.id.indicators).setVisibility(View.GONE);
				convertView.findViewById(R.id.search_separator).setVisibility(View.GONE);
			}

			_holder.viewPager.setCurrentItem(0);
		}
		else
		{
			setPageIndicator(pageIndicators, _profileSave);
		}

		updateItemInViewPager();

		_holder.viewPager.setOnPageChangeListener(new OnPageChangeListener()
		{
			@Override
			public void onPageSelected(int idx)
			{
				setPageIndicator(pageIndicators, idx);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{
			}

			@Override
			public void onPageScrollStateChanged(int position)
			{
			}
		});

		if (_users != null && _users.size() > Consts.MAX_SEARCH_RESULTS_USERS)
		{
			_holder.viewAll.setVisibility(View.VISIBLE);
			_holder.viewAll.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Bundle bundle = new Bundle();
					bundle.putString(Consts.BUNDLE_USER_NAME, _searchedUser);
					_profileSave = _holder.viewPager.getCurrentItem();
					((MainActivity) _context).replaceFragmentContent(SearchedUsersFragment.class, bundle);
				}
			});
		}
		else
		{
			_holder.viewAll.setVisibility(View.GONE);
		}

		return convertView;
	}

	private void setPageIndicator(ArrayList<ImageView> pageIndicators, final int position)
	{
		if (position >= Consts.SEARCH_RESULTS_MULTIPLY_LIMIT)
		{
			Log.e(_context.getApplicationContext().getPackageName(), "Watch out - there are more than " + Consts.SEARCH_RESULTS_MULTIPLY_LIMIT
					+ " items in the search results. The results are fixed to " + Consts.SEARCH_RESULTS_MULTIPLY_LIMIT + " at the most.");
			return;
		}
		pageIndicators.get(position).setImageResource(R.drawable.p_orange);
		if (pageIndicators.size() > position + 1)
		{
			pageIndicators.get(position + 1).setImageResource(R.drawable.p_gray);
		}
		if (position > 0)
		{
			pageIndicators.get(position - 1).setImageResource(R.drawable.p_gray);
		}
	}

	private void resetPageIndicator(ArrayList<ImageView> pageIndicators)
	{
		pageIndicators.get(0).setImageResource(R.drawable.p_orange);
		for (int i = 1; i < pageIndicators.size(); i++)
		{
			pageIndicators.get(i).setImageResource(R.drawable.p_gray);
		}
	}

	/**
	 * This function is to refresh the items in the viewpager, if there were any change in the SearchUsersAdapter.
	 * The user may follow/unfollow other user on the list, and then increase/decrees the counter and the follow image on the right.
	 * This IS NOT being refreshed, so we must do so by saving this data.
	 */
	private void updateItemInViewPager()
	{
		for (int i = 0; i < _pages.size(); i++)
		{
			View currentPage = _pages.get(i);
			final RVSUser currentUser = _users.get(i);
			TextView txtSubTitle = (TextView) currentPage.findViewById(R.id.user_info);
			txtSubTitle.setText(generateUserInfo(currentUser));

			ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(currentUser.getUserId());
			boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : currentUser.isFollowedByMe();
			final ImageView actionSign = (ImageView) (ImageView) currentPage.findViewById(R.id.user_action_sign);
			if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(currentUser.getUserId()))
			{
				actionSign.setVisibility(View.GONE);
			}
			else
			{
				actionSign.setVisibility(View.VISIBLE);
				actionSign.setImageResource(isFollowedByMe ? R.drawable.followed_user : R.drawable.add_follow_user);
			}

			actionSign.setOnClickListener(new OnClickListener()
			{
				@SuppressWarnings("rawtypes")
				@Override
				public void onClick(View v)
				{
					if (((MainActivity) _context).isLoggedIn(((MainActivity) _context).getCurrentFragment(), true))
					{
						ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(currentUser.getUserId());
						boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : currentUser.isFollowedByMe();
						if (isFollowedByMe)
						{
							((SearchFragment) ((MainActivity) _context).getCurrentFragment()).unFollowUser(currentUser.getUserId());
							actionSign.setImageResource(R.drawable.add_follow_user);
							AppManager.getInstance().updateFollowersInfo(currentUser.getUserId(), currentUser.getFollowerCount() - 1, false);
						}
						else
						{
							((SearchFragment) ((MainActivity) _context).getCurrentFragment()).followUser(currentUser.getUserId());
							actionSign.setImageResource(R.drawable.followed_user);
							AppManager.getInstance().updateFollowersInfo(currentUser.getUserId(), currentUser.getFollowerCount() + 1, true);
						}
						v.invalidate();
					}
					else
					{
						((MainActivity) _context).getCurrentFragment().addPendingOperation(
								new PendingOperation(OperationType.FOLLOW, currentUser.getUserId(), null));
					}

				}
			});

			currentPage.invalidate();
		}
	}

	private void addItemToViewPager(List<View> container, final RVSUser user)
	{
		LayoutInflater inflater = LayoutInflater.from(_context);
		View page = inflater.inflate(R.layout.search_results_user_item, null);

		TextView txtTitle = (TextView) page.findViewById(R.id.user_name);
		txtTitle.setText(user.getName());
		TextView txtSubTitle = (TextView) page.findViewById(R.id.user_info);
		txtSubTitle.setText(generateUserInfo(user));

		txtTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		txtSubTitle.setTypeface(AppManager.getInstance().getTypeFaceLight());

		AsyncImageView image = (AsyncImageView) page.findViewById(R.id.avatar_image);
		IDrawableCache cache = AppManager.getInstance();
		image.setIDrawableCache(cache);
		image.setDefualImageRes(R.drawable.default_profile);
		image.setSampleSize(UiUtils.getDipMargin(40));
		image.setRoundedCorner(true);
		image.setRemoteURI(user.getPofileImage().getImageUrlForSize(Consts.IMAGE_POINT));
		image.loadImage();

		page.findViewById(R.id.layout_user_item).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				_profileSave = _holder.viewPager.getCurrentItem();
//				((MainActivity) _context).openUserProfileActivity(-1, _users.get(_profileSave).getUserId(), false);
				((MainActivity) _context).openUserProfile(_users.get(_profileSave).getUserId(), false);

			}
		});

		final ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(user.getUserId());
		final boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : user.isFollowedByMe();
		final ImageView actionSign = (ImageView) (ImageView) page.findViewById(R.id.user_action_sign);

		if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(user.getUserId()))
		{
			actionSign.setVisibility(View.GONE);
		}
		else
		{
			actionSign.setVisibility(View.VISIBLE);
			actionSign.setImageResource(isFollowedByMe ? R.drawable.followed_user : R.drawable.add_follow_user);
		}

		actionSign.setOnClickListener(new OnClickListener()
		{
			@SuppressWarnings("rawtypes")
			@Override
			public void onClick(View v)
			{
				if (((MainActivity) _context).isLoggedIn(((MainActivity) _context).getCurrentFragment(), true))
				{
					ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(user.getUserId());
					boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : user.isFollowedByMe();
					if (isFollowedByMe)
					{
						((SearchFragment) ((MainActivity) _context).getCurrentFragment()).unFollowUser(user.getUserId());
						actionSign.setImageResource(R.drawable.add_follow_user);
						AppManager.getInstance().updateFollowersInfo(user.getUserId(), user.getFollowerCount() - 1, false);
					}
					else
					{
						((SearchFragment) ((MainActivity) _context).getCurrentFragment()).followUser(user.getUserId());
						actionSign.setImageResource(R.drawable.followed_user);
						AppManager.getInstance().updateFollowersInfo(user.getUserId(), user.getFollowerCount() + 1, true);
					}
					v.invalidate();
				}
				else
				{
					((MainActivity) _context).getCurrentFragment().addPendingOperation(new PendingOperation(OperationType.FOLLOW, user.getUserId(), null));
				}

			}
		});

		container.add(page);
	}

	private String generateUserInfo(RVSUser user)
	{
		String userInfo = "";
		ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(user.getUserId());

		int followers = info != null ? info.getFollowerCount() : user.getFollowerCount();
		if (followers > 0)
		{
			if (followers == 1)
			{
				userInfo += followers + " follower \u2022";
			}
			else
			{
				userInfo += followers + " followers \u2022";
			}
		}
		int following = user.getFollowingCount();
		if (following > 0)
		{
			userInfo += following + " following \u2022";
		}
		int whips = user.getPostCount();
		if (whips > 0)
		{
			if (whips == 1)
			{
				userInfo += whips + " whip";
			}
			else
			{
				userInfo += whips + " whips";
			}
		}

		if (userInfo.length() > 0 && userInfo.charAt(userInfo.length() - 1) == '\u2022')
		{
			return userInfo.substring(0, userInfo.length() - 1);
		}
		if ("".equals(userInfo))
		{
			return (0 + " " + _whips);
		}
		return userInfo;
	}

	private static class ViewHolder
	{
		ViewPager	viewPager;

		ImageView	indicator1;
		ImageView	indicator2;
		ImageView	indicator3;
		ImageView	indicator4;
		ImageView	indicator5;
		ImageView	indicator6;

		TextView	viewAll;
	}

}
