package com.whipclip.ui.adapters.items.searchitems;

import java.util.ArrayList;
import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.FacebookDialog;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.ActionLikingUserFragment;
import com.whipclip.fragments.ActionWhippingUserFragment;
import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.fragments.CommentFragment;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSEndCard;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.DrawableCache;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.BasePostsListAdapter;
import com.whipclip.ui.adapters.BasePostsListAdapter.RowTypes;
import com.whipclip.ui.adapters.ShareMoreOptionsAdapter;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.views.AsyncImageView;

public class FullPostView implements PostUIItem
{
	
	private final int   DSIPLAY_MORE_COMMENTS_AFTER = 2;
	
	private final int	COUNT_LIKE 		= 0;
	private final int	COUNT_COMMENT 	= 1;
	private final int	COUNT_SHARE 	= 2;
	
	Context					_context;
	private String			_searchQuery;
	private String			_searchTitle;
	private RVSPost			_postItem;
	private boolean			_isSearch;

	public FullPostView(Context context, RVSPost postItem, boolean isSearch)
	{
		_isSearch = isSearch;
		_postItem = postItem;
		_context = context;
	}

	@Override
	public int getViewType()
	{
		return RowTypes.FULL_POST_VIEW.ordinal();
	}

	public RVSPost getPostItem()
	{
		return _postItem;
	}

//	private ViewHolder				_holder;
	private BasePostsListAdapter	_adapter;
	private boolean	mRemoveViewBottom = false;
	

	@Override
	public View getView(final int position, LayoutInflater inflater, View convertView, BasePostsListAdapter adapter)
	{
		_adapter = adapter;
		final RVSPost post = _postItem;
		ViewHolder _holder = null; 

		if (convertView == null || !(convertView.getTag() instanceof ViewHolder))
		{
			convertView = inflater.inflate(R.layout.posts_list_item_layout, null);
			_holder = new ViewHolder();
			_holder.image = (AsyncImageView) convertView.findViewById(R.id.post_image);

			_holder.image.setTag(convertView.findViewById(R.id.video_place_holder));
			
			_holder.referView = (RelativeLayout)convertView.findViewById(R.id.refer_layout);
			_holder.referUserName = (TextView) convertView.findViewById(R.id.refer_user_name1);
			_holder.referUserText = (TextView) convertView.findViewById(R.id.refer_user_text1);
			

			_holder.avatar = (AsyncImageView) convertView.findViewById(R.id.avatar_image);
			_holder.netLogo = (AsyncImageView) convertView.findViewById(R.id.network_image);
			
//			_holder.comment = (LinearLayout) convertView.findViewById(R.id.comment_layout);
			
			
			_holder.likeBtn = (LinearLayout) convertView.findViewById(R.id.action_like_button);
			_holder.commentBtn = (LinearLayout) convertView.findViewById(R.id.action_comment_button);
			_holder.shareBtn	 = (LinearLayout)convertView.findViewById(R.id.action_share_button);
			_holder.moreBtn= (LinearLayout) convertView.findViewById(R.id.action_more_button);
			
			
			_holder.spoilerView = (RelativeLayout) convertView.findViewById(R.id.video_spoiler);
			TextView spoilerTextView = (TextView) convertView.findViewById(R.id.spoiler_alert_tv);
			spoilerTextView.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			
			_holder.titleText = (TextView) convertView.findViewById(R.id.post_title);
			_holder.handleText = (TextView) convertView.findViewById(R.id.post_handle);
			_holder.postText = (TextView) convertView.findViewById(R.id.post_text);
			_holder.postTime = (TextView) convertView.findViewById(R.id.post_time);
			_holder.postShowText = (TextView) convertView.findViewById(R.id.post_video_text);
			
			_holder.countLayout = (LinearLayout) convertView.findViewById(R.id.count_layout);
			_holder.commentLayout  = (RelativeLayout) convertView.findViewById(R.id.comment_layout);
			
			_holder.likeCount = (TextView) convertView.findViewById(R.id.like_count);
			_holder.whipCount = (TextView) convertView.findViewById(R.id.whip_count);
			_holder.commentCount = (TextView) convertView.findViewById(R.id.comment_count);
			
			_holder.dotOne = (TextView) convertView.findViewById(R.id.dot_1);
			
			
			_holder.commentUserName1 = (TextView) convertView.findViewById(R.id.comment_user_name1);
			_holder.commentUserHandle1 = (TextView) convertView.findViewById(R.id.comment_user_handle1);
			_holder.commentUserText1 = (TextView) convertView.findViewById(R.id.comment_user_text1);
			_holder.commentUserName2 = (TextView) convertView.findViewById(R.id.comment_user_name2);
			_holder.commentUserHandle2 = (TextView) convertView.findViewById(R.id.comment_user_handle2);
			_holder.commentUserText2 = (TextView) convertView.findViewById(R.id.comment_user_text2);

			// Search Views			
			_holder.searchResultText = (TextView) convertView.findViewById(R.id.post_search_text);
			_holder.searchSeparator = convertView.findViewById(R.id.search_separator);
//			_holder.bottomSeparator = convertView.findViewById(R.id.post_list_bottom_separator);
//			_holder.commentsSeparator = convertView.findViewById(R.id.separator);

			_holder.refreshProgressBar = (ProgressBar) convertView.findViewById(R.id.refresh_progress_bar);
			_holder.postError = (TextView) convertView.findViewById(R.id.post_error);

			IDrawableCache cache = AppManager.getInstance();
			_holder.image.setIDrawableCache(cache);
			_holder.avatar.setIDrawableCache(cache);
			_holder.netLogo.setIDrawableCache(cache);

			_holder.titleText.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.handleText.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.postText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			_holder.postTime.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.postShowText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			_holder.referUserName.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.referUserText.setTypeface(AppManager.getInstance().getTypeFaceRegular());

			_holder.likeCount.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.whipCount.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.commentCount.setTypeface(AppManager.getInstance().getTypeFaceMedium());

			_holder.commentUserName1.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.commentUserHandle1.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.commentUserText1.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			_holder.commentUserName2.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.commentUserHandle2.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.commentUserText2.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			
			
			((TextView)_holder.likeBtn.findViewById(R.id.post_action_button_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView)_holder.commentBtn.findViewById(R.id.post_action_button_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
			((TextView)_holder.shareBtn.findViewById(R.id.post_action_button_text)).setTypeface(AppManager.getInstance().getTypeFaceRegular());

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		try
		{
			Log.i(((MainActivity) _context).getActionBar().getTitle().toString(), "Post - '" + post.getText() + "'" + "    PostId = " + post.getPostId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		_holder.titleText.setText(post.getAuthor().getName());
		_holder.handleText.setText(post.getAuthor().getDisplayHandle());
		_holder.postText.setText(post.getText());
		_holder.postTime.setText(DateUtils.getStyledTimeFull(post.getCreationTime(), false, false));
		RVSChannel channel = post.getChannel();
		if (channel != null)
		{
			_holder.postShowText.setText(channel.getName());
		}

		RVSProgram program = post.getProgram();
		if (program != null)
		{
			String specialShowName = program.getSpecialShowName();
			if (specialShowName != null && !specialShowName.isEmpty()) {
				_holder.postShowText.setText(specialShowName);
			} else {
				_holder.postShowText.setText(program.getName());
			}
		}
		
		setDetailsForPost(_holder, post.getPostId());

		PostInformation currentPostInfo = _adapter != null ? _adapter.getRealTimePostInformation().get(post.getPostId()) : null;
		if (currentPostInfo != null)
		{
			updateSelectedMode(_holder, currentPostInfo.getLikedByCaller(), currentPostInfo.getRepostedByCaller(), currentPostInfo.isCommentedByCaller());
		}
		else
		{
			updateSelectedMode(_holder, post.getLikedByCaller(), post.getRepostedByCaller(), post.isCommentedByCaller());
		}

		if (_postItem.getMediaContext() != null && (!RVSMediaContext.STATUS_OK.equals(_postItem.getMediaContext().getStatus())))
		{
			_holder.postError.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.postError.setVisibility(View.VISIBLE);
		}
		else
		{
			_holder.postError.setVisibility(View.GONE);
		}

		final ViewHolder holder = _holder;
		_holder.likeBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_adapter != null)
				{
					if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false) && !"".equals(_postItem.getPostId()) && _postItem.getPostId() != null)
					{
						try
						{
							if (holder.likeBtn.isSelected())
							{
								AppManager.getInstance().getSDK().deleteLikeForPost(_postItem.getPostId());
								holder.likeBtn.setSelected(false);
								_adapter.updateLike(post.getPostId(), -1);
								setDetailsForPost(holder, post.getPostId());
							}
							else
							{
								AppManager.getInstance().getSDK().createLikeForPost(_postItem.getPostId());
								holder.likeBtn.setSelected(true);
								_adapter.updateLike(post.getPostId(), 1);
								setDetailsForPost(holder, post.getPostId());
								
								AnalyticsManager.sharedManager().trackPostLikeEventWithPost(_postItem, _adapter.getFragment().getCurrentTabPosition());
							}
						}
						catch (Throwable e)
						{
							Toast.makeText(_context, "Internal error occurred", 1).show();
							e.printStackTrace();
						}
					}
					else
					{ //in the 2nd variable, we'll pass true if we need to create a like, and false if we need to delete the like.					
						_adapter.getFragment().addPendingOperation(
								new PendingOperation(OperationType.LIKE, _postItem.getPostId(), holder.likeBtn.isSelected() == false));
					}
				}
			}
		});

//		whip_layout
		
		
		if (isMyUserId(_postItem.getAuthor().getUserId())) {
			
			_holder.shareBtn.setSelected(true);
		}
			
		_holder.shareBtn.setOnClickListener(new OnClickListener()
		{
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void onClick(View v)
			{
				if (_adapter != null)
				{
					if (_adapter.getMainActivity().isLoggedIn(_adapter.getFragment(), false) && !"".equals(_postItem.getPostId()) && _postItem.getPostId() != null)
					{
						try
						{
							if (holder.shareBtn.isSelected())
							{
								_adapter.getFragment().getMainActivity().openShareMoreOptionsDialog(false,_postItem);
								// delete
								
//									AppManager.getInstance().getSDK().deleteRepostForPost(_postItem.getPostId());
//									_holder.shareBtn.setSelected(false);
//									_adapter.updateRepost(post.getPostId(), -1);
//									setDetailsForPost(post.getPostId());
							}
							else
							{
								openShareDialog(holder, post);
							}
						}
						catch (Throwable e)
						{
							Toast.makeText(_context, "Internal error occurred", 1).show();
							e.printStackTrace();
						}
					}
					else
					{ //in the 2nd variable, we'll pass true if we need to create a like, and false if we need to delete the like.					
						_adapter.getFragment().addPendingOperation(
								new PendingOperation(OperationType.WHIP, _postItem.getPostId(), holder.shareBtn.isSelected() == false));
					}
				}
			}

			
		});

		OnClickListener commentClickListener = new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				PostInformation currentPostInfo = _adapter != null ? _adapter.getRealTimePostInformation().get(post.getPostId()) : null;
				if (_adapter != null)
				{
					Bundle bundle = new Bundle();
					bundle.putString(Consts.COMMENTS_POST_ID, _postItem.getPostId());
					bundle.putInt(Consts.TAB_POSITION, _adapter.getFragment().getCurrentTabPosition());
					bundle.putInt(Consts.COMMENTS_NUM_ITEMS, currentPostInfo != null ? currentPostInfo.getCommentCount() : _postItem.getCommentCount());
					
					
					AppManager.getInstance().setOpenedCommentPost(_postItem);
					AppManager.getInstance().setOpenedCommentsViewForPost(currentPostInfo);
					((BasePostsFragment)_adapter.getFragment()).replaceTabFragmentContent(CommentFragment.class, bundle);
				}
			}
		};
		
		_holder.commentCount.setOnClickListener(commentClickListener);
		_holder.commentBtn.setOnClickListener(commentClickListener);

		_holder.moreBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				openMoreDialog(holder, post);
			}
		});

		_holder.spoilerView.setVisibility(View.GONE);
		if (post.getCoverImage() != null)
		{
			if (post.isSpoiler()) {
				_holder.image.setBlurred(true);
				_holder.spoilerView.setVisibility(View.VISIBLE);
			} else {
				_holder.image.setBlurred(false);
				_holder.spoilerView.setVisibility(View.GONE);
			}
			
			int heightDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenHeight / 4, _context);
			int widthDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenWidth / 4, _context);
			
			_holder.image.setDefualImageRes(R.drawable.video_logo);
			_holder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
			_holder.image.setRemoteURI(post.getCoverImage().getImageUrlForSize(new Point(widthDp, heightDp)));

			_holder.image.setTag(convertView.findViewById(R.id.video_place_holder));
			_holder.image.loadImage();
		}
		if (post.getAuthor() != null && post.getAuthor().getPofileImage() != null)
		{
			_holder.avatar.setRoundedCorner(true);
			_holder.avatar.setDefualImageRes(R.drawable.default_profile);
			if (post.getAuthor() != null && post.getAuthor().getPofileImage().getImageUrlForSize(new Point(40, 40)) != null)
			{
				
				_holder.avatar.setRemoteURI(post.getAuthor().getPofileImage().getImageUrlForSize(new Point(40, 40)));
				_holder.avatar.setSampleSize(UiUtils.getDipMargin(40));
				_holder.avatar.loadImage();
			}
			else
			{
				_holder.avatar.showDefaultImage();
//				_holder.avatar.setDrawable(drawable)ImageResource(R.drawable.default_profile);
			}
		}
		if (post.getChannel() != null && post.getChannel().getWatermarkLogo() != null)
		{
			_holder.netLogo.setRemoteURI(post.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)));
			_holder.netLogo.setSampleSize(UiUtils.getDipMargin(20));
			_holder.netLogo.loadImage();
		}
		if (_isSearch)// Search
		{
			String title = post.getText();//_searchTitle;
			String[] searchQueries = _searchQuery.split(" ");
			for (String word : searchQueries)
			{
				if (title != null && title.toLowerCase().contains(word.toLowerCase())
						&& (searchQueries.length == 1 || (searchQueries.length > 1 && word.length() > 2)))
				{ //if _searchQuery is a sentence, we won't mark 2 letter words.
					title = title.replaceAll("(?i)" + word, "<font color='black'><b>" + word + "</b></font>");
				}
				_holder.postText.setText(Html.fromHtml(title));
			}
			//just making sure that _searchQuery will be marked fully, if existed.
			if ((searchQueries.length > 1) && (title != null && title.toLowerCase().contains(_searchQuery.toLowerCase())))
			{
				title = title.replaceAll("(?i)" + _searchQuery, "<font color='black'><b>" + _searchQuery + "</b></font>");
				_holder.postText.setText(Html.fromHtml(title));
			}
		}
		else
		{
			_holder.postText.setText(post.getText());
		}

		_holder.image.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					RVSEndCard endCard = null;
					String name = "";
					if (post.getProgram() != null && post.getProgram().getSpecialShowName() != null)
					{
						name = post.getProgram().getSpecialShowName();
					}
					else if (post.getChannel() != null)
					{
						name = post.getChannel().getName();
					}
					
					if (post.getProgram() != null) {
						endCard = post.getProgram().getEndCard();
					}

					if (_postItem.getMediaContext() != null && (RVSMediaContext.STATUS_OK.equals(_postItem.getMediaContext().getStatus())))
					{
						boolean firstTimePlayback = _adapter.getFragment().handleVideoClick(_postItem.getMediaContext(), v, name, holder.image.getRemoteURI(), position,
								post.getChannel() != null ? post.getChannel().getWatermarkLogo().getImageUrlForSize(new Point(20, 20)) : null,
										holder.refreshProgressBar, endCard, _postItem);
						
						if (firstTimePlayback) {
							AnalyticsManager.sharedManager().trackClipPlaybackEventWithPost(_postItem, false, _adapter.getFragment().getCurrentTabPosition());
						}
					}
				}
			}
		});

		_holder.avatar.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					((BasePostsFragment)_adapter.getFragment()).openUserProfileAsChild(post.getAuthor().getUserId(), false);

				}
			}
		});

		_holder.titleText.setOnClickListener(new OnClickListener()
		{
			

			@Override
			public void onClick(View v)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					((BasePostsFragment)_adapter.getFragment()).openUserProfileAsChild(post.getAuthor().getUserId(), false);

//					String userId = post.getAuthor().getUserId();
//					FragmentManager fm = _adapter.getFragment().getChildFragmentManager();
//					
//					
//					// update the main content by replacing fragments
//					BaseFragment newFrag = new UserProfileFragment();
//					
//					FragmentTransaction transaction = fm.beginTransaction();
//					
//					transaction.addToBackStack(null);
//					transaction.replace(R.id.tab_frame_layout, newFrag, UserProfileFragment.class.getSimpleName());
//					transaction.commit();
//					
//				//	transaction.addToBackStack(UserProfileFragment.class.getSimpleName()).commitAllowingStateLoss();
				}
			}
		});

		_holder.referUserName.setOnClickListener(new OnClickListener()
		{

			public void onClick(View arg0)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					PostInformation postInfo = _adapter.getRealTimePostInformation().get(post.getPostId());
					if (postInfo != null) 
					{
						String userId = null;
						if (postInfo.getRepostingUser() != null) {
							userId =  postInfo.getRepostingUser().getUser().getUserId();
						} else if (postInfo.getCommentingUser() != null) {
							userId =  postInfo.getCommentingUser().getUser().getUserId();
						}
						
						if (userId != null) {
							
							int currentTabPosition = _adapter.getFragment().getCurrentTabPosition();
//							((MainActivity) _adapter.getFragment().getActivity()).openUserProfileActivity(currentTabPosition, userId, false);
							((MainActivity) _adapter.getFragment().getActivity()).openUserProfile(userId, false);

						}
					}
				}
			}
		});
		
		_holder.commentUserName1.setOnClickListener(new OnClickListener()
		{

			public void onClick(View arg0)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					PostInformation postInfo = _adapter.getRealTimePostInformation().get(post.getPostId());
					if (postInfo != null && postInfo.getLatestsComments() != null && postInfo.getLatestsComments().size() > 0)
					{
						RVSComment comment = postInfo.getLatestsComments().get(0);
						int currentTabPosition = _adapter.getFragment().getCurrentTabPosition();
//						((MainActivity) _adapter.getFragment().getActivity()).openUserProfileActivity(currentTabPosition, comment.getAuthor().getUserId(), false);
						((BasePostsFragment) _adapter.getFragment()).openUserProfileAsChild(comment.getAuthor().getUserId(), false);
						
						//						AppManager.getInstance().setLastCommenter(comment.getAuthor());
						//						((BaseActivity) _context).replaceFragmentContent(UserProfileFragment.class, null);
						//						AppManager.getInstance().setCallProfileFragmentBefore(true);
					}
				}
			}
		});

		_holder.commentUserName2.setOnClickListener(new OnClickListener()
		{

			public void onClick(View arg0)
			{
				if (_adapter != null && _adapter.getFragment().isVisible())
				{
					PostInformation postInfo = _adapter.getRealTimePostInformation().get(post.getPostId());
					if (postInfo != null && postInfo.getLatestsComments() != null && postInfo.getLatestsComments().size() > 1)
					{
						RVSComment comment = postInfo.getLatestsComments().get(1);
						int currentTabPosition = _adapter.getFragment().getCurrentTabPosition();
//						((MainActivity) _adapter.getFragment().getActivity()).openUserProfileActivity(currentTabPosition, comment.getAuthor().getUserId(), false);
						((BasePostsFragment) _adapter.getFragment()).openUserProfileAsChild(comment.getAuthor().getUserId(), false);

						//						AppManager.getInstance().setLastCommenter(comment.getAuthor());
						//						((BaseActivity) _context).replaceFragmentContent(UserProfileFragment.class, null);
						//						AppManager.getInstance().setCallProfileFragmentBefore(true);
					}
				}
			}
		});
		
		removeViewBottom(_holder);

		return convertView;
	}

	   
	private boolean isMyUserId(String userId)
	{
		String myUseId = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);
		boolean signedOut = RVSSdk.sharedRVSSdk().isSignedOut();
		
		return (!signedOut && userId.equals(myUseId));
	}

	private void updateSelectedMode(ViewHolder _holder, int likedByCaller, int repostedByCaller, boolean commentedByCaller)
	{
		_holder.likeBtn.setSelected(likedByCaller == 1);
		_holder.shareBtn.setSelected(repostedByCaller == 1);
		_holder.commentBtn.setSelected(commentedByCaller);
	}

	private void setDetailsForPost(ViewHolder _holder, String postId)
	{
		PostInformation postInfo = _adapter != null ? _adapter.getRealTimePostInformation().get(postId) : null;
		//		if (postId.equals("24d12ea55e5e4c87b755ed00f469b28d"))
		//		{
		//			Log.e("hadas", "333333333post " + postId + " like = " + postInfo.getLikeCount() + " " + " setLikedByCaller = " + postInfo.getLikedByCaller());
		//		}
		if (postInfo == null)
		{
			setCount(_holder, COUNT_LIKE , _postItem.getLikeCount(), postId, _postItem.getLikedByCaller() == 1, _holder.likeCount);
			setCount(_holder, COUNT_SHARE, _postItem.getRepostCount(), postId, _postItem.getRepostedByCaller() == 1, _holder.whipCount);
			setCount(_holder, COUNT_COMMENT, _postItem.getCommentCount(), postId, false, _holder.commentCount);
		}
		else
		{
			setCount(_holder, COUNT_LIKE, postInfo.getLikeCount(), postId, postInfo.getLikedByCaller() == 1, _holder.likeCount);
			setCount(_holder, COUNT_SHARE, postInfo.getRepostCount(), postId, postInfo.getRepostedByCaller() == 1, _holder.whipCount);
			setCount(_holder, COUNT_COMMENT, postInfo.getCommentCount(), postId, false, _holder.commentCount);
		}
		
		updateCountDotsLayout(_holder, postInfo);
		
		_holder.referView.setVisibility(View.GONE);
		_holder.commentUserName1.setVisibility(View.GONE);
		_holder.commentUserHandle1.setVisibility(View.GONE);
		_holder.commentUserText1.setVisibility(View.GONE);
		_holder.commentUserName2.setVisibility(View.GONE);
		_holder.commentUserHandle2.setVisibility(View.GONE);
		_holder.commentUserText2.setVisibility(View.GONE);
		_holder.commentUserName1.setText(null);
		_holder.commentUserText1.setText(null);
		_holder.commentUserName2.setText(null);
		_holder.commentUserText2.setText(null);
		
		RVSReferringUser referUser = null;
		String referStr = null;
		if (_postItem.getRepostingUser() != null && _postItem.getRepostingUser().getUser() != null)
		{
		    referUser = _postItem.getRepostingUser();
		    referStr = _context.getResources().getString(R.string.post_refer_repost);
		}
	    else if (_postItem.getCommentingUser() != null && _postItem.getCommentingUser().getUser() != null)
	    {
	        referUser = _postItem.getCommentingUser();
	        referStr = _context.getResources().getString(R.string.post_refer_comment);
	    }
		    
		if (referUser != null) {
			_holder.referView.setVisibility(View.VISIBLE);
			_holder.referUserName.setText(referUser.getUser().getName());
			_holder.referUserText.setText(referStr);
		}
		
		List<RVSComment> latestsComments = postInfo == null ? _postItem.getLatestComments() : postInfo.getLatestsComments();
		if (latestsComments != null && latestsComments.size() > 0)
		{ //max 2 comments to display.
			RVSComment comment = latestsComments.get(0);
			if (comment != null)
			{
				_holder.commentLayout.setVisibility(View.VISIBLE);
				
				_holder.commentUserName1.setVisibility(View.VISIBLE);
				_holder.commentUserHandle1.setVisibility(View.VISIBLE);
				_holder.commentUserText1.setVisibility(View.VISIBLE);
				_holder.commentUserName1.setText(comment.getAuthor().getName());
				_holder.commentUserHandle1.setText("@" + comment.getAuthor().getHandle());
				_holder.commentUserText1.setText(comment.getMessage());
				if (latestsComments.size() > 1)
				{
					comment = latestsComments.get(1);
					if (comment != null)
					{
						_holder.commentUserName2.setVisibility(View.VISIBLE);
						_holder.commentUserHandle2.setVisibility(View.VISIBLE);
						_holder.commentUserText2.setVisibility(View.VISIBLE);
						_holder.commentUserName2.setText(comment.getAuthor().getName());
						_holder.commentUserHandle2.setText("@" + comment.getAuthor().getHandle());
						_holder.commentUserText2.setText(comment.getMessage());
					}
				}
//				_holder.commentsSeparator.setVisibility(View.VISIBLE);
			}
			else
			{
//				_holder.commentsSeparator.setVisibility(View.GONE);
				_holder.commentLayout.setVisibility(View.GONE);
			}
		}
		else
		{
//			_holder.commentsSeparator.setVisibility(View.GONE);
			_holder.commentLayout.setVisibility(View.GONE);

		}

	}

	private void updateCountDotsLayout(ViewHolder _holder, PostInformation postInfo)
	{
		int numOfCountToDisplay = 0;
		if (postInfo == null)
		{
			numOfCountToDisplay += _postItem.getLikeCount() > 0 ? 1 : 0;
			numOfCountToDisplay += _postItem.getRepostCount() > 0 ? 1 : 0;
		}
		else
		{
			numOfCountToDisplay += postInfo.getLikeCount() > 0 ? 1 : 0;
			numOfCountToDisplay += postInfo.getRepostCount() > 0 ? 1 : 0;
		}
		
		_holder.dotOne.setVisibility(numOfCountToDisplay > 1 ? View.VISIBLE : View.GONE);
		
		_holder.countLayout.setVisibility((numOfCountToDisplay == 0) ? View.GONE : View.VISIBLE);
		
	}

	private void setCount(ViewHolder _holder, int count_type, int count, final String postId, boolean isActionByUser, TextView viewToUpdate)
	{
		if (count_type == COUNT_COMMENT) {
			count = (count - DSIPLAY_MORE_COMMENTS_AFTER) > 0 ? (count - DSIPLAY_MORE_COMMENTS_AFTER) : 0;
		}
		
		String text = "";
		if (count > 0) {
			text = count + " ";
		}
		
		
		if (count == 0){
			viewToUpdate.setVisibility(View.GONE);
		}else if (count > 1) {
			
			switch (count_type) {
				case COUNT_LIKE: 
					text += _context.getResources().getString(R.string.post_bar_likes);
					break;
				case COUNT_COMMENT:
					text += _context.getResources().getString(R.string.post_bar_comments);
					break;
				case COUNT_SHARE:
					text += _context.getResources().getString(R.string.post_bar_shares);
					break;
			}
			viewToUpdate.setVisibility(View.VISIBLE);
		}
		else
		{
			switch (count_type) {
				case COUNT_LIKE: 
					text += _context.getResources().getString(R.string.post_bar_like);
					break;
				case COUNT_COMMENT:
					text += _context.getResources().getString(R.string.post_bar_comment);
					break;
				case COUNT_SHARE:
					text += _context.getResources().getString(R.string.post_bar_share);
					break;
			}
			
			viewToUpdate.setVisibility(View.VISIBLE);
		}
		
		
		
		viewToUpdate.setText(text);
		
		if (count_type == COUNT_SHARE && isMyUserId(postId)) 
		{
			viewToUpdate.setTextColor(_context.getResources().getColor(R.color.orange));

		} else if (count == 0) {
			viewToUpdate.setTextColor(_context.getResources().getColor(R.color.dark_gray_menu));
			switch (count_type) {
				case COUNT_LIKE:
				case COUNT_SHARE: 
					viewToUpdate.setOnClickListener(null);
					break;
			}

		} else {
			viewToUpdate.setTextColor(_context.getResources().getColor(R.color.orange));
			viewToUpdate.setEnabled(true);

			switch (count_type) {
				case COUNT_LIKE: 
					_holder.likeCount.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View arg0)
						{
							Bundle bundle = new Bundle();
							bundle.putString(Consts.BUNDLE_POST_ID, _postItem.getPostId());
//							((MainActivity) _context).replaceTabFragmentContent(TabFragmentAdapter.TAB_POSITION_POPULAR, ActionLikingUserFragment.class, bundle);
							((BasePostsFragment)_adapter.getFragment()).replaceTabFragmentContent(ActionLikingUserFragment.class, bundle);
							
						}
					});
					break;

				case COUNT_SHARE:
					_holder.whipCount.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View arg0)
						{
							Bundle bundle = new Bundle();
							bundle.putString(Consts.BUNDLE_POST_ID, _postItem.getPostId());
//							((MainActivity) _context).replaceTabFragmentContent(TabFragmentAdapter.TAB_POSITION_POPULAR, ActionWhippingUserFragment.class, bundle);
							((BasePostsFragment)_adapter.getFragment()).replaceTabFragmentContent(ActionWhippingUserFragment.class, bundle);
						}
					});
					break;
			}
		}
	}

	public static class ViewHolder
	{
		TextView		titleText;
		TextView		handleText;
		TextView		postText;
		TextView		postTime;
		TextView		postShowText;
		TextView		referUserName;
		TextView		referUserText;
		AsyncImageView	image;
		AsyncImageView	avatar;
		AsyncImageView	netLogo;
		RelativeLayout 	referView;
		RelativeLayout 	spoilerView;

		LinearLayout	countLayout;
		RelativeLayout	commentLayout;
		TextView		likeCount;
		TextView		whipCount;
		TextView		commentCount;
		
		TextView		dotOne;

		LinearLayout	comment;
		TextView		commentUserName1;
		TextView		commentUserHandle1;
		TextView		commentUserText1;
		TextView		commentUserName2;
		TextView		commentUserHandle2;
		TextView		commentUserText2;
		
		LinearLayout	likeBtn;
		LinearLayout	commentBtn;
		LinearLayout	shareBtn;
		LinearLayout	moreBtn;

		TextView		searchResultText;
		View			searchSeparator;
		View			bottomSeparator;
//		View			commentsSeparator;

		ProgressBar		refreshProgressBar;
		TextView		postError;
	}
	
	public String getSearchQuery()
	{
		return _searchQuery;
	}

	public void setSearchQuery(String searchQuery)
	{
		this._searchQuery = searchQuery;
	}

	public String getSearchTitle()
	{
		return _searchTitle;
	}

	public void setSearchTitle(String title)
	{
		this._searchTitle = title;
	}

	public boolean setSelected(ViewHolder _holder, OperationType type, String postId, boolean selected)
	{
		if (_adapter != null)
		{
			if (_postItem.getPostId().equals(postId))
			{
				if (OperationType.LIKE.equals(type))
				{
					_holder.likeBtn.setSelected(selected);
					_adapter.updateLike(postId, selected ? 1 : -1);
				}
				else if (OperationType.WHIP.equals(type))
				{
					_holder.likeBtn.setSelected(selected);
					_adapter.updateRepost(postId, selected ? 1 : -1);
				}
				else if (OperationType.COMMENT.equals(type))
				{
					_adapter.updateComments(postId, 549, new ArrayList<RVSComment>());
				}
				setDetailsForPost(_holder, postId);
				return true;
			}
		}
		return false;
	}

	private void removeViewBottom(ViewHolder _holder)
	{
		if (_holder != null && _holder.bottomSeparator != null && mRemoveViewBottom)
		{
			_holder.bottomSeparator.setVisibility(View.GONE);
		}
	}

	
	
	private void openShareDialog(final ViewHolder _holder, final RVSPost post)
	{
		final ArrayList<String> items = new ArrayList<String>();
		items.add(_context.getString(R.string.post_bar_share_as_is));
		items.add(_context.getString(R.string.post_bar_share_edit_clip));
		items.add(_context.getString(R.string.dialog_cancel));
		final DialogAdapter adapter = new DialogAdapter(_context, items);
		
		final Dialog dialog = new Dialog(_context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_layout);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		ListView dialogList = (ListView) dialog.findViewById(R.id.dialog_list);
		dialogList.setAdapter(adapter);
		
		final long playbackCurrentTime = _adapter.getFragment().getCurrentPlaybackTime();
		dialogList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
			{
				dialog.dismiss();
				
				switch (position) {
					case 0: // as is
						AppManager.getInstance().getSDK().createRepostForPost(_postItem.getPostId());
						AnalyticsManager.sharedManager().trackPostShareWithPost(_postItem, 
								AnalyticsManager.RVSAppAnalyticsManagerShareTypeWhipclip);
						_holder.shareBtn.setSelected(true);
						_adapter.updateRepost(post.getPostId(), 1);
						setDetailsForPost(_holder, post.getPostId());
						
						_adapter.getFragment().getMainActivity().openShareMoreOptionsDialog(true,_postItem);
						break;
					
					case 1: // edit clip
						_adapter.getFragment().destroyVideoView();
						
						AppManager.getInstance().setCurrentMediaContext((RVSMediaContext) post.getMediaContext(), playbackCurrentTime);
						Bundle bundle = new Bundle();
						bundle.putString(Consts.BUNDLE_TEXT_COMPOSE, post.getText());
						((BaseActivity) _context).replaceFragmentContent(ComposeFragment.class, bundle);
						break;
				}
			}
		});
		dialog.show();
	}


//	@SuppressWarnings("rawtypes")
//	private void executePendingOperation(PendingOperation op)
//	{
//		switch (op.getType())
//		{
//
//			case COMPOSE_VIDEO:
//				
//			default:
//				break;
//		}
//	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void openMoreDialog(final ViewHolder _holder, final RVSPost post)
	{

		final Dialog dialog = new Dialog(_context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_layout);
		ListView dialogList = (ListView) dialog.findViewById(R.id.dialog_list);

		final ArrayList<String> items = new ArrayList<String>();
//		items.add(_context.getString(R.string.dialog_share_fb));
//		items.add(_context.getString(R.string.dialog_share_tw));
		items.add(_context.getString(R.string.dialog_pin_it));
		items.add(_context.getString(R.string.dialog_post_tumblr));
		items.add(_context.getString(R.string.dialog_email_link));
		items.add(_context.getString(R.string.dialog_sms_link));
		
		boolean isLoggedIn = AppManager.getInstance().isLoggedIn();
		
		final String spoilerText = _context.getString(R.string.dialog_mark_spoiler);
		if (isLoggedIn && !post.isSpoiler() ) {
			items.add(spoilerText);
		}
		
		final String inappropriateText = _context.getString(R.string.dialog_mark_inappropriate);
		if (isLoggedIn && !post.isInappropriate() ) {
			items.add(inappropriateText);
		}
		
		int repostedByCaller = _postItem.getRepostedByCaller();
		final String deleteText = _context.getString(R.string.dialog_delete_post);
		final String unshareText = _context.getString(R.string.dialog_unshare_clip);
		final boolean isMyPost = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(post.getAuthor().getUserId());
		if (isMyPost)
		{
			items.add(deleteText);
		}
		else if (repostedByCaller == 1)
		{
			items.add(unshareText);
		}
		
		items.add(_context.getString(R.string.dialog_cancel));

		final DialogAdapter adapter = new DialogAdapter(_context, items);
		dialogList.setAdapter(adapter);

		dialogList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
			{
				dialog.dismiss();
				if ((items.size() == (position + 1) && !AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(post.getAuthor().getUserId())) || position >= items.size())
				{
					return;
				}

				String itemText = items.get(position);
				if (itemText.equals(spoilerText)) {
					AppManager.getInstance().getSDK().reportPost(post.getPostId(), RVSSdk.PostReportType.SPOILER);
				} else if (itemText.equals(inappropriateText)) {
					AppManager.getInstance().getSDK().reportPost(post.getPostId(), RVSSdk.PostReportType.INAPPROPRIATE);
				} else if (itemText.equals(unshareText)) {
					
					AppManager.getInstance().getSDK().deleteRepostForPost(_postItem.getPostId());
					_holder.shareBtn.setSelected(false);
					_adapter.updateRepost(post.getPostId(), -1);
					setDetailsForPost(_holder, post.getPostId());
				
				} else if (itemText.equals(deleteText)) {
					
					deleteCurrentPost();
					
				} else {
				
					final MainActivity mainActivity = _adapter.getFragment().getMainActivity();
					if (_adapter != null)
					{
						mainActivity.showProgressDialog();
					}
					
					RVSPromise sharePromise = AppManager.getInstance().getSDK().sharingUrlsForPost(post.getPostId());
					sharePromise.then(new DoneCallback<RVSSharingUrls>()
					{
						@Override
						public void onDone(final RVSSharingUrls result)
						{
							if (_adapter != null && _adapter.getFragment().isVisible())
							{
								_adapter.getFragment().getMainActivity().dismissProgressDialog();
								if (result != null && result.getUrl() != null)
								{
									//						_adapter.shareImage(result.getUrl());
									((Activity) _context).runOnUiThread(new Runnable()
									{
										public void run()
										{
											switch (position)
											{
//												case 0:
//												{
//	
//													try
//													{
//														if (FacebookDialog.canPresentShareDialog((Activity) _context,
//																FacebookDialog.ShareDialogFeature.SHARE_DIALOG))
//														{
////															FacebookDialog shareDialog1 = new FacebookDialog.ShareDialogBuilder((Activity) _context);
////															shareDialog1.g
////															FacebookLogic.shareOnFacebook(result, _postItem, false);
//															FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity) _context).setLink(
//																	result.getUrl() + "").build();
//															((MainActivity) _context).showFacebookDialog(shareDialog.present());
//														}
//														else
//														{
//															mainActivity.useFallBackWebDialog(result);
//														}
//	
//													}
//													catch (Exception e)
//													{
//														e.printStackTrace();
//													}
//													break;
//												}
//												case 1:
//												{
//													AppManager.getInstance().getTwitterLogic().postTwit(post.getText(), result.getUrl(), null);
//													break;
//												}
												case 0:
												{
													((MainActivity) _context).pinIt("" + result.getUrl(), result.getCoverWithOverlayUrl());
													break;
												}
												case 1:
												{
													((MainActivity) _context).shareTumblr("" + result.getUrl(), _postItem.getText(),
															result.getCoverWithOverlayUrl() + "");
													break;
												}
												case 2:
												{
													shareByEmail("" + result.getUrl(), result.getCoverWithOverlayUrl());
													AnalyticsManager.sharedManager().trackPostShareWithPost(_postItem, 
															AnalyticsManager.RVSAppAnalyticsManagerShareTypeEmail);

													break;
												}
												case 3:
												{
													shareBySMS("" + result.getUrl());
													AnalyticsManager.sharedManager().trackPostShareWithPost(_postItem, 
															AnalyticsManager.RVSAppAnalyticsManagerShareTypeSMS);

													break;
												}
											}
										}
									});
	
								}
								else
								{
									WhipClipApplication.getCurrentActivity().runOnUiThread(new Runnable()
									{
										@Override
										public void run()
										{
											Toast.makeText(WhipClipApplication.getCurrentActivity(), _context.getString(R.string.error_compose), Toast.LENGTH_SHORT)
													.show();
										}
									});
								}
							}
						}
					}, new FailCallback<Object>()
					{
						@Override
						public void onFail(Object result)
						{
							if (_adapter != null && _adapter.getFragment().isVisible())
							{
								_adapter.getFragment().getMainActivity().dismissProgressDialog();
							}
						}
					});
				}

			}
		});
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		dialog.show();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void deleteCurrentPost()
	{
		if (!AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(_postItem.getAuthor().getUserId()))
		{
			return;
		}
		RVSPromise deletePost = AppManager.getInstance().getSDK().deletePost(_postItem.getPostId());
		deletePost.then(new DoneCallback<Object>()
		{
			@Override
			public void onDone(Object result)
			{
				((Activity) _context).runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						_adapter.getFragment().onRefresh();
					}
				});
				Log.i(getClass().getName(), "Deleted post " + _postItem.getPostId() + " successfully");
			}
		}, new FailCallback<Error>()
		{
			@Override
			public void onFail(Error result)
			{
				((Activity) _context).runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						Toast.makeText(WhipClipApplication.getCurrentActivity(), _context.getString(R.string.error_compose), Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
	}

	private void shareByEmail(final String url, final String thumbnailUrl)
	{
		_adapter.getFragment().getMainActivity().showProgressDialog();
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				String emailSubject = "";
				if (_postItem.getProgram() != null && _postItem.getProgram().getSpecialShowName() != null) {
					emailSubject = String.format(_context.getString(R.string.email_subject),  _postItem.getProgram().getSpecialShowName());
				}  else if (_postItem.getProgram() != null && _postItem.getProgram().getName() != null) {
					emailSubject = String.format(_context.getString(R.string.email_subject),  _postItem.getProgram().getName());
				} else {
					emailSubject = _context.getString(R.string.email_subject_no_show_name);
				}
				
				AppManager.getInstance().getFilesManager().downloadFile(thumbnailUrl, "/sdcard/img.png");
				Intent intent = new Intent(Intent.ACTION_SENDTO);
				intent.setType("text/html");
				String uriText = "mailto:" + Uri.encode("") + "?subject=" + Uri.encode(emailSubject) + "&body="
						+ Uri.encode(
								String.format(_context.getString(R.string.email_body), _postItem.getAuthor().getName(), _postItem.getText(), url));
				Uri uri = Uri.parse(uriText);
				intent.setData(uri);
				intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/img.png"));
				
				
//										intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml("<img src=\"" + "file://sdcard/img.png" + "\" height=\"42\" width=\"42\" />"));
//										intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml("<img src=\"" + thumbnailUrl + "\" height=\"42\" width=\"42\" />"));
				
//				intent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
//				intent.putExtra(Intent.EXTRA_TEXT, 
//						 Uri.encode(
//									String.format(_context.getString(R.string.email_body), _postItem.getAuthor().getName(), _postItem.getText(), url)));

				
				
				final Intent mailer = Intent.createChooser(intent, "Send Email");
				((Activity) _context).runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						_adapter.getFragment().getMainActivity().dismissProgressDialog();
						_context.startActivity(mailer);
					}
				});
			}
		}).start();
	}

	private void shareBySMS(String string)
	{
		Uri uri = Uri.parse("smsto:");
		Intent it = new Intent(Intent.ACTION_SENDTO, uri);
		it.putExtra("sms_body", string);
		_context.startActivity(it);
	}
	//	private void sendEmail()
	//	{
	//		Intent intent = new Intent(Intent.ACTION_SEND);
	//		intent.setType("message/rfc822");
	//		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { getResources().getString(R.string.feedback_recipient) });
	//		intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_subject));
	//		intent.putExtra(Intent.EXTRA_TEXT, "Device: " + android.os.Build.MODEL + "(" + android.os.Build.DEVICE + ")" + "\nSDK: "
	//				+ android.os.Build.VERSION.SDK_INT + "\nApp: " + WhipClipApplication._appName + "\nVersion: " + WhipClipApplication.getVersionNumber());
	//		Intent mailer = Intent.createChooser(intent, "Send Email");
	//		startActivity(mailer);
	//		_navigationDrawerFragment.selectItemInNavigation(0);
	//	}

	public void setRemoveViewBottom(boolean b) {
		mRemoveViewBottom = b;
	}
	
	
	
	

	

	

}
