package com.whipclip.ui.adapters.items.searchitems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSAsyncImage;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.TvShowsListAdapter;
import com.whipclip.ui.views.AsyncImageView;

public class TvShowUIItem
{
//	public int getViewType();

	private TvShowsListAdapter	mAdapter;
	private RVSShow				mShow;
	private IDrawableCache		mCache;
	private Context				mContext;
	private TvShowViewHolder	mHolder;
	private int 				mIndex;
	private Logger	logger 		= LoggerFactory.getLogger(TvShowUIItem.class);
	
	public static class TvShowViewHolder
	{
		AsyncImageView	image;
		TextView		showTitle;
		TextView		channelTitle;
		SeekBar			seekbar;
		
	}
	
	
	public TvShowUIItem(Context context, RVSShow show, IDrawableCache cache, int index)
	{
		mShow = show;
		mCache = cache;
		mContext = context;
		mIndex = index;
	}
	
	public RVSShow getShow() {
		return mShow;
	}
	
	public View getView(int position, LayoutInflater inflater, View convertView, TvShowsListAdapter adapter) {
	
			mAdapter = adapter;

			if (convertView == null || !(convertView.getTag() instanceof TvShowViewHolder))
			{
				convertView = inflater.inflate(R.layout.tv_show_list_item_layout, null);
				mHolder = new TvShowViewHolder();
				mHolder.image = (AsyncImageView) convertView.findViewById(R.id.show_image);
				mHolder.seekbar = (SeekBar)convertView.findViewById(R.id.seekbar);
				mHolder.showTitle = (TextView) convertView.findViewById(R.id.show_title);
				mHolder.channelTitle = (TextView) convertView.findViewById(R.id.channel_title);
				
				mHolder.showTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
				mHolder.channelTitle.setTypeface(AppManager.getInstance().getTypeFaceMedium());
				
				mHolder.image.setIDrawableCache(mCache);
				mHolder.image.usedTag(true);
				
				convertView.setTag(mHolder);
				
			} else {
				mHolder = (TvShowViewHolder) convertView.getTag();
			}
			
			
			mHolder.showTitle.setText(mShow.getName());
			RVSChannel channel = mShow.getChannel();
			if (channel != null) {
				mHolder.channelTitle.setText(channel.getName());
			}
			
			showIsLiveIndicator(mHolder.seekbar);
			
			logger .info("position " + position);
			try{
				
	//			mHolder.image.setImageBitmap(null);
				mHolder.image.setDefualImageRes(R.drawable.video_logo);
				
				RVSAsyncImage image = mShow.getImage();
				
				if (image != null) {
					int heightDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenHeight / 7, mContext);
					int widthDp = (int)Utils.convertPixelsToDip(WhipClipApplication._screenWidth / 3, mContext);
					String imageUrl = mShow.getImage().getImageUrlForSize(new Point(widthDp, heightDp));
					logger .info("position " + position + ", show " + mShow.getName() +" , url: " + imageUrl);
					mHolder.image.setSampleSize(WhipClipApplication._screenWidth / 4);
					mHolder.image.setTag(imageUrl);
					mHolder.image.setRemoteURI(imageUrl);
				} else {
					mHolder.image.setRemoteURI(null);
					mHolder.image.setTag(null);
				}
				
				mHolder.image.loadImage();
				
			}catch (Exception e) {
				logger .info("position " + position + ", crash");
			}
			
			convertView.setOnClickListener(new View.OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
//					TvShowUIItem item = (TvShowUIItem) adapter.getItemAtPosition(position);
//					
//					openShowsFragment(item.getShow().getShowId(), item.getShow().getChannel().getChannelId());
//				
					MainActivity mainActivity = mAdapter.getMainActivity();
					
					AppManager.getInstance().setShow(mShow);
					mainActivity.showShowDetailsPage(mShow.getShowId(), mShow.getChannel().getChannelId());
				}
			});
//				
			
			return convertView;
	}
	
	private void showIsLiveIndicator(SeekBar seekbar)
	{
		seekbar.setVisibility(View.GONE);
		
		long now = System.currentTimeMillis();
		RVSProgram nextProgram = mShow.getNextProgram();
		if (nextProgram != null) {
			long start = mShow.getNextProgram().getStart();
			long end = mShow.getNextProgram().getEnd();
		    if (start  <= now &&  end > now) { // live
	
		    	seekbar.setVisibility(View.VISIBLE);
		        
		        long nowOffset = now - start;
		        long duration =  end - start;
		        
		        int progress = (int)(nowOffset * 100 / duration);
		        seekbar.setProgress(progress);
		    }
		}
	}


	public String getShowName() {
		if (mHolder == null) return "";
		
		return mHolder.showTitle.getText().toString();
	}
	
	public int getIndex() {
		return mIndex;
	}
	
}
