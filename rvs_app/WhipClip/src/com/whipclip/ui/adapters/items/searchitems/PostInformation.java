package com.whipclip.ui.adapters.items.searchitems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSReferringUser;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;

/**
 * 
 * @author hadas
 * Saving likes and whips, since we're updating on real time those values,
 * SO, if we'll try to get this values again, postItem.getLikeCount() will
 * NOT give us an updated count.
 */
public class PostInformation
{
	private int					_likedByCaller;
	private int					_likeCount;
	private boolean				_commentedByCaller;
	private int					_repostedByCaller;	//getRepostedByCaller
	private int					_repostCount;		//getRepostCount
	private int					_commentsCount;
	private List<RVSComment>	_latestsComments;
	private boolean				_isCommented;
	private RVSReferringUser  	_commentingUser;
	private RVSReferringUser 	_repostingUser;
	

	public PostInformation(RVSPost post)
	{
		super();
		_likedByCaller = post.getLikedByCaller();
		_likeCount = post.getLikeCount();
		_repostedByCaller = post.getRepostedByCaller();
		_commentedByCaller = post.isCommentedByCaller();
		_repostCount = post.getRepostCount();
		_commentsCount = post.getCommentCount();
		_latestsComments = post.getLatestComments();
		if (_latestsComments != null)
		{
			Collections.reverse(_latestsComments);
		}
		_isCommented = false;
		_commentingUser = post.getCommentingUser();
		_repostingUser = post.getRepostingUser();
	}

	public int getLikedByCaller()
	{
		return _likedByCaller;
	}

	public void setLikedByCaller(int likedByCaller)
	{
		_likedByCaller = likedByCaller;
	}

	public int getLikeCount()
	{
		return _likeCount;
	}

	public void setLikeCount(int likeCount)
	{
		_likeCount = likeCount;
	}

	public int getRepostedByCaller()
	{
		return _repostedByCaller;
	}

	public void setRepostedByCaller(int repostedByCaller)
	{
		_repostedByCaller = repostedByCaller;
	}
	
	public boolean isCommentedByCaller() {
		return _commentedByCaller;
	}
	
	public void setCommentedByCaller(boolean commentedByCalled) {
		_commentedByCaller = commentedByCalled;
	}


	public int getRepostCount()
	{
		return _repostCount;
	}

	public void setRepostCount(int repostCount)
	{
		_repostCount = repostCount;
	}

	public int getCommentCount()
	{
		return _commentsCount;
	}

	public void setCommentsCount(int commentsCounts)
	{
		_commentsCount = commentsCounts;
	}

	public List<RVSComment> getLatestsComments()
	{
		return _latestsComments;
	}

	public void setLatestsComments(List<RVSComment> latestsComments)
	{
		_latestsComments = latestsComments;
	}
	
	public void addCommentToLatestsComments(RVSComment comment)
	{
		if (_latestsComments == null) _latestsComments = new ArrayList<RVSComment>();
	
		_latestsComments.add(comment);
	}
	
	public void removeCommentFromLatestsComments(String commentId)
	{
		if (_latestsComments == null) return;
		
		for (RVSComment comment : _latestsComments) {
			if (comment.getCommentId().equals(commentId)) {
				_latestsComments.remove(comment);
				break;
			}
		}
		
	}


	public void setCommmented(boolean isCommented)
	{
		_isCommented = isCommented;
	}

	public boolean isCommmented()
	{
		return _isCommented;
	}

	 public RVSReferringUser getCommentingUser() 
	 {
		 return _commentingUser;
	 }

	 public RVSReferringUser getRepostingUser() 
	 {
		 return _repostingUser;
	 }
}
