package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.fragments.CommentFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.views.AsyncImageView;

public class CommentsAdapter extends ArrayAdapter<RVSComment>
{
	private Context						_context;
	private int							_layoutId;
	private ArrayList<RVSComment>		_info;
	protected BaseFragment				_fragment;
	
	public CommentsAdapter(Context context, int layoutId, ArrayList<RVSComment> info, BaseFragment fragment)
	{
		super(context, layoutId, info);
		_context = context;
		_info = info;
		_layoutId = layoutId;
		_fragment = fragment;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		CommentInfoHolder holder = null;

		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(_layoutId, parent, false);
			holder = new CommentInfoHolder();
			holder._avatarIcon = (AsyncImageView) convertView.findViewById(R.id.comment_avatar);
			holder._username = (TextView) convertView.findViewById(R.id.comment_username);
			holder._handle = (TextView) convertView.findViewById(R.id.comment_handle);
			holder._content = (TextView) convertView.findViewById(R.id.comment_content);
			holder._date = (TextView) convertView.findViewById(R.id.comment_date);
			convertView.setTag(holder);
		}
		else
		{
			holder = (CommentInfoHolder) convertView.getTag();
		}

		final RVSComment info = _info.get(position);

		holder._avatarIcon.setDefualImageRes(R.drawable.default_profile);
		holder._avatarIcon.setRemoteURI(info.getAuthor().getPofileImage().getImageUrlForSize(Consts.IMAGE_POINT));
		holder._avatarIcon.setSampleSize(UiUtils.getDipMargin(45));
		holder._avatarIcon.setRoundedCorner(true);
		holder._avatarIcon.loadImageFirstUrl();

		holder._username.setText(info.getAuthor().getName());
		holder._handle.setText(info.getAuthor().getDisplayHandle());
		holder._content.setText(info.getMessage());
		holder._date.setText(DateUtils.getStyledTimeFullAgoFormat(info.getCreationTime(), false, true));// + " " + _context.getResources().getString(R.string.time_ago));

		holder._username.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		holder._content.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		holder._date.setTypeface(AppManager.getInstance().getTypeFaceLight());
		holder._handle.setTypeface(AppManager.getInstance().getTypeFaceLight());

		if (CommentFragment.ANIMATE_ADD_NEW_ITEM)
		{
			Animation anim = AnimationUtils.loadAnimation(_context, R.anim.appear);
			convertView.startAnimation(anim);
			CommentFragment.ANIMATE_ADD_NEW_ITEM = false;
		}
		
		
		holder._avatarIcon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_fragment != null && _fragment.isVisible())
				{
					_fragment.getMainActivity().openUserProfile(info.getAuthor().getUserId(), false);
				}
			}
		});

		holder._username.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (_fragment != null && _fragment.isVisible())
				{
					_fragment.getMainActivity().openUserProfile(info.getAuthor().getUserId(), false);
				}
			}
		});

		return convertView;
	}

	static class CommentInfoHolder
	{
		AsyncImageView	_avatarIcon;
		TextView		_username;
		TextView		_handle;
		TextView		_content;
		TextView		_date;
	}
}
