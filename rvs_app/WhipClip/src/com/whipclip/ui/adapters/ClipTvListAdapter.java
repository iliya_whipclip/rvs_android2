package com.whipclip.ui.adapters;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.ui.adapters.items.searchitems.ClipTvUIItem;

public class ClipTvListAdapter extends ArrayAdapter<ClipTvUIItem>
{
	protected ArrayList<ClipTvUIItem>			_items;
	protected LayoutInflater					_inflater;
	protected BaseFragment						_fragment;

	private Dictionary<Integer, Integer>		_listViewItemHeights	= new Hashtable<Integer, Integer>();

	public ClipTvListAdapter(Context context, ArrayList<ClipTvUIItem> items, BaseFragment fragment)
	{
		super(context, 0, items);
		_items = items;
		_inflater = LayoutInflater.from(context);
		_fragment = fragment;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		if (_items != null && position < _items.size()) {
			final View retView = _items.get(position).getView(position, _inflater, convertView, this);
			return retView;
		}
		
		return convertView;
	}

	public Dictionary<Integer, Integer> getItemsHeights()
	{
		return _listViewItemHeights;
	}


	public MainActivity getMainActivity()
	{
		return (MainActivity) _fragment.getActivity();
	}
}
