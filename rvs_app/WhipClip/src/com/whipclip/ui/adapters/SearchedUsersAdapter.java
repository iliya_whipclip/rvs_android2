package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.BaseUsersFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.adapters.items.PendingOperation;
import com.whipclip.ui.adapters.items.PendingOperation.OperationType;
import com.whipclip.ui.adapters.items.searchitems.ScrollUsersInformation;
import com.whipclip.ui.views.AsyncImageView;

public class SearchedUsersAdapter extends ArrayAdapter<RVSUser>
{
	private Context				_context;
	private ArrayList<RVSUser>	_users;
	private ViewHolder			_holder;
	private IDrawableCache		_cache;
	private String				_followers;
	private String				_following;
	private String				_whips;
	private String				_whip;
	private String				_follower;
	private BaseUsersFragment	_fargment;

	public SearchedUsersAdapter(Context context, ArrayList<RVSUser> users, IDrawableCache cache, BaseUsersFragment fargment)
	{
		super(context, R.layout.user_details_list_item, users);
		_fargment = fargment;
		_context = context;
		_users = users;
		_cache = cache;

		_followers = _context.getString(R.string.followers);
		_follower = _context.getString(R.string.follower);
		_following = _context.getString(R.string.following);
		_whips = _context.getString(R.string.whips);
		_whip = _context.getString(R.string.whip);

		//Since before calling this adapter, we're calling for a server's new data, this is a change to update the _realTimeScrollUsersInformation.
		AppManager.getInstance().insertElementsToRealtimeScrollUsers(_users);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final RVSUser user = _users.get(position);

		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(R.layout.user_details_list_item, parent, false);
			_holder = new ViewHolder();

			_holder.avatar = (AsyncImageView) convertView.findViewById(R.id.avatar_image);
			_holder.userName = (TextView) convertView.findViewById(R.id.user_name);
			_holder.followersCount = (TextView) convertView.findViewById(R.id.followers_count);
			_holder.followingCount = (TextView) convertView.findViewById(R.id.following_count);
			_holder.whipsCount = (TextView) convertView.findViewById(R.id.whips_count);
			_holder.bullet1 = (TextView) convertView.findViewById(R.id.bullet1);
			_holder.bullet2 = (TextView) convertView.findViewById(R.id.bullet2);
			_holder.userActionSign = (ImageView) convertView.findViewById(R.id.user_action_sign);

			_holder.avatar.setIDrawableCache(_cache);
			_holder.userName.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			_holder.followersCount.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.followingCount.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.whipsCount.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.bullet1.setTypeface(AppManager.getInstance().getTypeFaceLight());
			_holder.bullet2.setTypeface(AppManager.getInstance().getTypeFaceLight());

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (ViewHolder) convertView.getTag();
		}

		String imageResource = _users.get(position).getPofileImage().getImageUrlForSize(Consts.IMAGE_POINT);
		_holder.avatar.setDefualImageRes(R.drawable.default_profile);
		_holder.avatar.setSampleSize(UiUtils.getDipMargin(40));
		_holder.avatar.setRoundedCorner(true);
		_holder.avatar.setRemoteURI(imageResource);
		_holder.avatar.loadImage();

		_holder.userName.setText(user.getName());

		String followersText = "";
		if (user.getFollowerCount() > 1)
		{
			_holder.followersCount.setVisibility(View.VISIBLE);
			followersText = user.getFollowerCount() + " " + _followers;
		}
		else if (user.getFollowerCount() == 1)
		{
			_holder.followersCount.setVisibility(View.VISIBLE);
			followersText = user.getFollowerCount() + " " + _follower;
		}
		else
		{
			_holder.followersCount.setVisibility(View.GONE);
		}

		_holder.followersCount.setText(followersText);

		String followingText = "";
		if (user.getFollowingCount() > 1)
		{
			if (_holder.followersCount.getVisibility() != View.GONE)
			{
				_holder.bullet1.setVisibility(View.VISIBLE);
			}
			else
			{
				_holder.bullet1.setVisibility(View.GONE);
			}
			_holder.followingCount.setVisibility(View.VISIBLE);
			followingText = user.getFollowingCount() + " " + _following;
		}
		else if (user.getFollowingCount() == 1)
		{
			if (_holder.followersCount.getVisibility() != View.GONE)
			{
				_holder.bullet1.setVisibility(View.VISIBLE);
			}
			else
			{
				_holder.bullet1.setVisibility(View.GONE);
			}
			_holder.followingCount.setVisibility(View.VISIBLE);
			followingText = user.getFollowingCount() + " " + _following;
		}
		else
		{
			_holder.bullet1.setVisibility(View.GONE);
			_holder.followingCount.setVisibility(View.GONE);
		}

		String whipsText = "";
		if (user.getRepostCount() > 1)
		{
			if (_holder.followingCount.getVisibility() != View.GONE || _holder.followersCount.getVisibility() != View.GONE)
			{
				_holder.bullet2.setVisibility(View.VISIBLE);
			}
			else
			{
				_holder.bullet2.setVisibility(View.GONE);
			}
			_holder.whipsCount.setVisibility(View.VISIBLE);
			whipsText = user.getPostCount() + " " + _whips;
		}
		else if (user.getPostCount() == 1)
		{
			if (_holder.followingCount.getVisibility() != View.GONE)
			{
				_holder.bullet2.setVisibility(View.VISIBLE);
			}
			else
			{
				_holder.bullet2.setVisibility(View.GONE);
			}
			_holder.whipsCount.setVisibility(View.VISIBLE);
			whipsText = user.getFollowingCount() + " " + _whip;
		}

		else
		{
			_holder.bullet2.setVisibility(View.GONE);
		}

		_holder.whipsCount.setText(whipsText);
		_holder.followingCount.setText(followingText);
		_holder.followersCount.setText(followersText);

		if ("".equals(whipsText) && "".equals(followingText) && "".equals(followersText))
		{
			_holder.whipsCount.setText(0 + " " + _whips);
		}

		final ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(user.getUserId());
		final boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : user.isFollowedByMe();
		if (AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID).equals(user.getUserId()))
		{
			_holder.userActionSign.setVisibility(View.GONE);
		}
		else
		{
			_holder.userActionSign.setVisibility(View.VISIBLE);
			_holder.userActionSign.setImageResource(isFollowedByMe ? R.drawable.followed_user : R.drawable.add_follow_user);
		}

		final ImageView actionSign = _holder.userActionSign;
		actionSign.setOnClickListener(new OnClickListener()
		{
			@SuppressWarnings("rawtypes")
			@Override
			public void onClick(View v)
			{
				if (((MainActivity) _context).isLoggedIn(((MainActivity) _context).getCurrentFragment(), true))
				{
					//This fixes the situation when the user presses follow/unfollow. 
					//We can't relay on the in accurate user.isFollowedBy anymore, so we're relaying on the icon itself. 
					ScrollUsersInformation info = AppManager.getInstance().getRealTimeScrollUsersInformation().get(user.getUserId());
					boolean isFollowedByMe = (info != null) ? info.isFollowedByMe() : user.isFollowedByMe();
					if (isFollowedByMe)
					{
						((BaseUsersFragment) ((MainActivity) _context).getCurrentFragment()).unFollowUser(user.getUserId());
						actionSign.setImageResource(R.drawable.add_follow_user);
						AppManager.getInstance().updateFollowersInfo(user.getUserId(), user.getFollowerCount() - 1, false);
					}
					else
					{
						((BaseUsersFragment) ((MainActivity) _context).getCurrentFragment()).followUser(user.getUserId());
						actionSign.setImageResource(R.drawable.followed_user);
						AppManager.getInstance().updateFollowersInfo(user.getUserId(), user.getFollowerCount() + 1, true);
					}
					v.invalidate();
				}
				else
				{
					((MainActivity) _context).getCurrentFragment().addPendingOperation(new PendingOperation(OperationType.FOLLOW, user.getUserId(), null));
				}

			}
		});

		return convertView;
	}

	private static class ViewHolder
	{
		AsyncImageView	avatar;
		TextView		userName;
		ImageView		userActionSign;

		TextView		followersCount;
		TextView		bullet1;
		TextView		followingCount;
		TextView		bullet2;
		TextView		whipsCount;
	}

	public MainActivity getMainActivity()
	{
		return (MainActivity) _fargment.getActivity();
	}

	public BaseUsersFragment getFragment()
	{
		return _fargment;
	}

}
