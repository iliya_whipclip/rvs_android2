package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;

import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.fragments.SearchFragment;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;

public class SearchResultsAdapter extends BasePostsListAdapter
{
	public SearchResultsAdapter(Context context, ArrayList<PostUIItem> items, BasePostsFragment fragment)
	{
		super(context, items, fragment);
	}

	@Override
	public int getViewTypeCount()
	{
		return 5;
	}

	@Override
	public void onSearchChannelClicked(String channelID, String syn)
	{
		getFragment().openChannelCompose(channelID, syn);
	}
	
	@Override
	public SearchFragment getFragment()
	{
		return (SearchFragment) _fragment;
	}

}