package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.ui.adapters.items.DrawerMenuItem;

public class DrawerMenuAdapter extends ArrayAdapter<DrawerMenuItem>
{
	private Context						_context;
	private int							_layoutId;
	private ArrayList<DrawerMenuItem>	_info;

	public DrawerMenuAdapter(Context context, int layoutId, ArrayList<DrawerMenuItem> info)
	{
		super(context, layoutId, info);
		_context = context;
		_info = info;
		_layoutId = layoutId;
	}

	MenuInfoHolder	_holder	= null;

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(_layoutId, parent, false);
			_holder = new MenuInfoHolder();
			_holder._icon = (ImageView) convertView.findViewById(R.id.menu_item_image);
			_holder._text = (TextView) convertView.findViewById(R.id.menu_item_text);
//			_holder._notifications = (TextView) convertView.findViewById(R.id.menu_item_notifications);

			_holder._text.setTypeface(AppManager.getInstance().getTypeFaceRegular());
//			_holder._notifications.setTypeface(AppManager.getInstance().getTypeFaceRegular());

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (MenuInfoHolder) convertView.getTag();
		}

		DrawerMenuItem info = _info.get(position);
		_holder._icon.setImageResource(info.getImageResource());
		
		String signOutStr = _context.getResources().getString(R.string.sign_out);
		String signInStr = _context.getResources().getString(R.string.sign_in);
		
		if (info.getText().equals(signOutStr) || info.getText().equals(signInStr)) {
			_holder._text.setText(AppManager.getInstance().isLoggedIn() ? signOutStr : signInStr);
		} else {
			_holder._text.setText(info.getText());
		}

		return convertView;
	}

	public ArrayList<DrawerMenuItem>  getValues() {
		return _info;
	}
	
//	public void setNotifications(int notifications)
//	{
//		if (notifications <= 0)
//		{
//			_holder._notifications.setVisibility(View.GONE);
//			_holder._notifications.setText("");
//		}
//		else
//		{
//			_holder._notifications.setVisibility(View.VISIBLE);
//			_holder._notifications.setText(notifications + "");
//		}
//	}

	static class MenuInfoHolder
	{
		ImageView	_icon;
		TextView	_text;
//		TextView	_notifications;
	}
}
