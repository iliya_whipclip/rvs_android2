package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;

import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;

public class PostsListAdapter extends BasePostsListAdapter
{

	public PostsListAdapter(Context context, ArrayList<PostUIItem> items, BasePostsFragment fragment)
	{
		super(context, items, fragment);
	}

	@Override
	public int getViewTypeCount()
	{
		return 1;//RowResultType.values().length;
	}

}
