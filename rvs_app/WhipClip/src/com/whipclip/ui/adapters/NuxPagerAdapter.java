package com.whipclip.ui.adapters;

import java.util.List;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

public class NuxPagerAdapter extends PagerAdapter
{
	List<View>	pages	= null;

	public NuxPagerAdapter(List<View> pages)
	{
		this.pages = pages;
	}

	@Override
	public Object instantiateItem(View collection, int position)
	{
		View v = this.pages.get(position);
		((ViewPager) collection).addView(v, 0);
		return v;
	}

	@Override
	public void destroyItem(View collection, int position, Object view)
	{
		((ViewPager) collection).removeView((View) view);
	}

	@Override
	public int getCount()
	{
		return this.pages.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view.equals(object);
	}
}
