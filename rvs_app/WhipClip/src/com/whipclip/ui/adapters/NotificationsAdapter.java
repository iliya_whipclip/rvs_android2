package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.fragments.SinglePostFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.utils.DateUtils;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.dataObjects.RVSNotification;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.UiUtils;
import com.whipclip.ui.views.AsyncImageView;

public class NotificationsAdapter extends ArrayAdapter<RVSNotification>
{
	private Context							_context;
	private int								_layoutId;
	private ArrayList<RVSNotification>		_info;
	private IDrawableCache					_cache;

	private NotificationInfo				_holder;

	public NotificationsAdapter(Context context, int layoutId, ArrayList<RVSNotification> info)
	{
		super(context, layoutId, info);
		_context = context;
		_info = info;
		_layoutId = layoutId;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			LayoutInflater inf = LayoutInflater.from(_context);
			convertView = inf.inflate(_layoutId, parent, false);
			_holder = new NotificationInfo();
			_holder._avatarIcon = (AsyncImageView) convertView.findViewById(R.id.notification_avatar);
			_holder._user = (TextView) convertView.findViewById(R.id.notification_user);
			_holder._handle = (TextView) convertView.findViewById(R.id.comment_handle);
			_holder._content = (TextView) convertView.findViewById(R.id.notification_content);
//			_holder._subContent = (TextView) convertView.findViewById(R.id.notification_sub_content);
//			_holder._date = (TextView) convertView.findViewById(R.id.notification_date);

			_holder._avatarIcon.setIDrawableCache(_cache);

			convertView.setTag(_holder);
		}
		else
		{
			_holder = (NotificationInfo) convertView.getTag();
		}

		if (position >= _info.size()) {
			return convertView; 
		}
		
		final RVSNotification info = _info.get(position);

		RVSUser currentUser = null;
		String currentPostId = null;
		if (info.getCommentEvent() != null && info.getCommentEvent().getComment() != null && info.getCommentEvent().getComment().getAuthor() != null)
		{
			currentUser = info.getCommentEvent().getComment().getAuthor();
			currentPostId = info.getCommentEvent().getPost().getPostId();
			generateItem(currentUser, info.getTimestamp(), R.string.commented_on_your_whip, info.getCommentEvent().getComment().getMessage());
		}
		else if (info.getFollowEvent() != null && info.getFollowEvent().getUser() != null)
		{
			currentUser = info.getFollowEvent().getUser();
			generateItem(currentUser, info.getTimestamp(), R.string.follwed_you, null);
		}
		else if (info.getLikeEvent() != null && info.getLikeEvent().getUser() != null)
		{
			currentUser = info.getLikeEvent().getUser();
			currentPostId = info.getLikeEvent().getPost().getPostId();
			generateItem(currentUser, info.getTimestamp(), R.string.liked_your_whip, null);
		}
		else if (info.getRepostEvent() != null && info.getRepostEvent().getUser() != null)
		{
			currentUser = info.getRepostEvent().getUser();
			currentPostId = info.getRepostEvent().getPost().getPostId();
			generateItem(currentUser, info.getTimestamp(), R.string.whipped_your_whip, null);
		}

		final RVSUser user = currentUser;
		final String postId = currentPostId;
		_holder._avatarIcon.setOnClickListener(new OnClickListener()
		{
			//avatar and user name opens the user's profile.
			public void onClick(View arg0)
			{
				if (user != null)
				{
					((MainActivity) _context).openUserProfile(user.getUserId(), false);
				}
			}
		});

		_holder._user.setOnClickListener(new OnClickListener()
		{
			//avatar and user name opens the user's profile.
			public void onClick(View arg0)
			{
				if (user != null)
				{
//					((MainActivity) _context).openUserProfileActivity(-1, user.getUserId(), false);
					((MainActivity) _context).openUserProfile(user.getUserId(), false);
					
				}
			}
		});

		convertView.setOnClickListener(new OnClickListener()
		{

			public void onClick(View v)
			{ //Everything that's not avatar or user name, opens the post.
				if (postId == null)
				{ //In case we didn't find the postId OR, in NOT an error, this is a follow event, hence - no postId.
					
//					((MainActivity) _context).openUserProfileActivity(-1, user.getUserId(), false);
					((MainActivity) _context).openUserProfile(user.getUserId(), false);
				}
				else
				{
					Bundle bundle = new Bundle();
					bundle.putString(Consts.BUNDLE_LATESTS_POST_ID, postId);
//					((BaseActivity) _context).replaceFragmentContent(SinglePostFragment.class, bundle);
					((MainActivity) _context).replaceFragmentContent(SinglePostFragment.class, bundle);
				}
			}
		});

		return convertView;
	}

	private void generateItem(RVSUser user, long timeStamp, int contentTextId, String subContextText)
	{
		if (user.getPofileImage() != null && user.getPofileImage().getImageUrlForSize(new Point(40, 40)) != null)
		{
			_holder._avatarIcon.setDefualImageRes(R.drawable.default_profile);
			_holder._avatarIcon.setRemoteURI(user.getPofileImage().getImageUrlForSize(new Point(40, 40)));
			_holder._avatarIcon.setSampleSize(UiUtils.getDipMargin(40));
		}
		else
		{
			_holder._avatarIcon.setDefualImageRes(R.drawable.default_profile);
			_holder._avatarIcon.setSampleSize(UiUtils.getDipMargin(40));
		}
		_holder._avatarIcon.setRoundedCorner(true);
		_holder._avatarIcon.loadImage();

		_holder._user.setText(user.getName());
		_holder._handle.setText(user.getDisplayHandle());
		
	
//		_holder._date.setText(DateUtils.getStyledTimeFull(timeStamp, true, false));

		_holder._user.setTypeface(AppManager.getInstance().getTypeFaceMedium());
		_holder._handle.setTypeface(AppManager.getInstance().getTypeFaceLight());
		_holder._content.setTypeface(AppManager.getInstance().getTypeFaceRegular());
//		_holder._date.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		String contentStr = _context.getResources().getString(contentTextId);
		if (subContextText != null)
		{
			contentStr += subContextText;
//			_holder._subContent.setVisibility(View.VISIBLE);
//			_holder._subContent.setText(Html.fromHtml("\"" + "<font color='black'><b>" + subContextText + "</b></font>" + "\""));
//			_holder._subContent.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		}
//		else
//		{
//			_holder._subContent.setVisibility(View.GONE);
//		}
		
		String time = DateUtils.getStyledTimeFull(timeStamp, true, false);
		contentStr += " " + time;
		
		_holder._content.setText(contentStr );
	}

	private static class NotificationInfo
	{
		AsyncImageView	_avatarIcon;
		TextView		_user;
		TextView		_handle;
		TextView		_content;
//		TextView		_subContent;
//		TextView		_date;
	}
}
