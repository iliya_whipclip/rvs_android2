package com.whipclip.ui.adapters;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viewpagerindicator.IconPagerAdapter;
import com.whipclip.R;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.fragments.ClipTVFragment;
import com.whipclip.fragments.FeedFragment;
import com.whipclip.fragments.NotificationsFragment;
import com.whipclip.fragments.PlaceholderFragment;
import com.whipclip.fragments.PostsFragment;
import com.whipclip.fragments.TVShowsTabFragment;

public class TabFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter{

	private Logger logger = LoggerFactory.getLogger(TabFragmentAdapter.class);

	public static final int TAB_POSITION_HOME = 0;
	public static final int TAB_POSITION_POPULAR = 1;
	public static final int TAB_POSITION_CLIP_TV = 2;
	public static final int TAB_POSITION_TV_SHOWS = 3;
	public static final int TAB_POSITION_NOTIFICATIONS = 4;
	public static final int TAB_POSITION_TOTAL = TAB_POSITION_NOTIFICATIONS + 1;

	
	private HashMap<Integer, Fragment> mFragmentsHash = new HashMap<Integer, Fragment>();
	 
	 private static final int[] ICONS = new int[] {
         R.drawable.tab_home_selector,
         R.drawable.tab_trending_selector,
         R.drawable.tab_clip_tv_selector,
         R.drawable.tab_tv_shows_selector,
         R.drawable.tab_notify_selector,
	 };
	 
	private boolean	mShowUserProfile;

    public TabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }
    
    @Override
    public int getIconResId(int index) {
        return ICONS[index];
    }

      @Override
    public Fragment getItem(int position) 
    {
//    	  logger.debug("getItem position: " + position);
    	  BaseFragment newFrag = null;
  		switch (position)
  		{
  			case TabFragmentAdapter.TAB_POSITION_HOME:
  				newFrag = new FeedFragment();
  				newFrag.setCurrentTabPosition(position);
  				break;

  			case TabFragmentAdapter.TAB_POSITION_POPULAR:
  				newFrag = new PostsFragment();
  				newFrag.setCurrentTabPosition(TabFragmentAdapter.TAB_POSITION_POPULAR);
  				break;
  				
  			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
  				newFrag = new ClipTVFragment(TabFragmentAdapter.TAB_POSITION_CLIP_TV);
  				break;

  			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
  				newFrag = new TVShowsTabFragment(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
  				break;

  			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
  				newFrag = new NotificationsFragment(TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS);
  				break;

  			default:
  				newFrag = new PlaceholderFragment();
  		}
  		
		mFragmentsHash.put(position, newFrag);
  		return newFrag;
  		
//  	fallback
//    	Fragment fragment = EmptyFragment.newInstance(getPageTitle(position).toString());
//    	return fragment;
    }
      
      @Override
    public int getCount() {
        return TAB_POSITION_TOTAL;
    }
    
//      @Override
//    public CharSequence getPageTitle(int position){
//    	return CONTENT[position % CONTENT.length];
//    }

	public void addChildFragment(int position, Fragment newFragment) {
		
		((BaseFragment)newFragment).setCurrentTabPosition(position);
		// currently only popular supporting child fragments
		switch (position)
		{
			case TabFragmentAdapter.TAB_POSITION_HOME:
			case TabFragmentAdapter.TAB_POSITION_POPULAR:
			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
//			case TabFragmentAdapter.TAB_POSITION_PROFILE:
			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
			
				BaseFragment fragment = (BaseFragment) mFragmentsHash.get(position);
				fragment.addChildFragment(newFragment);
				break;
		}
	}
}



