package com.whipclip.ui.adapters;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.whipclip.R;
import com.whipclip.fragments.TVShowsFragment;

public class TabTvShowsFragmentAdapter extends FragmentPagerAdapter{

	private Logger logger = LoggerFactory.getLogger(TabTvShowsFragmentAdapter.class);

	public static final int TAB_POSITION_HOME = 0;
	public static final int TAB_POSITION_POPULAR = 1;
	
	 private static final String[] CONTENT = new String[] { "Popular", "A-Z"};
	 
	 private HashMap<Integer, Fragment> mFragmentsHash = new HashMap<Integer, Fragment>();
	 
	 private FragmentManager fragMan;
	 
	 private static final int[] ICONS = new int[] {
         R.drawable.perm_group_calendar,
         R.drawable.perm_group_camera,
	 };
	 
    public TabTvShowsFragmentAdapter(FragmentManager fm) {
        super(fm);
        
        fragMan = fm;
    }
    
      @Override
    public Fragment getItem(int position) 
    {
//    	  logger.debug("getItem position: " + position);
  		Fragment newFrag = null;
  		switch (position)
  		{
  			case TabTvShowsFragmentAdapter.TAB_POSITION_HOME:
  				newFrag = new TVShowsFragment(1);
  				break;

  			case TabTvShowsFragmentAdapter.TAB_POSITION_POPULAR:
  				newFrag = new TVShowsFragment(2);
  				break;
  		}
  		
		mFragmentsHash.put(position, newFrag);
  		return newFrag;
  		
    }
      
      @Override
    public int getCount() {
        return CONTENT.length;
    }
    
      @Override
    public CharSequence getPageTitle(int position){
    	return CONTENT[position % CONTENT.length];
    }
}



