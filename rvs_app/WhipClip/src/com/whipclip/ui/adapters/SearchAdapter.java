package com.whipclip.ui.adapters;

import java.util.ArrayList;

import android.content.Context;

import com.whipclip.fragments.BasePostsFragment;
import com.whipclip.fragments.SearchFragment;
import com.whipclip.ui.adapters.items.searchitems.PostUIItem;

public class SearchAdapter extends BasePostsListAdapter
{

	public SearchAdapter(Context context, ArrayList<PostUIItem> items, BasePostsFragment fragment)
	{
		super(context, items, fragment);
	}

	@Override
	public int getViewTypeCount()
	{
		return 3;

	}

	@Override
	public void onSearchSuggetionClicked(String searchText, boolean search)
	{
		getFragment().setSearchText(searchText, search);
	}

	@Override
	public void onSearchChannelClicked(String channelID, String syn)
	{
		getFragment().openChannelCompose(channelID, syn);
	}

	@Override
	public SearchFragment getFragment()
	{
		return (SearchFragment) _fragment;
	}

}