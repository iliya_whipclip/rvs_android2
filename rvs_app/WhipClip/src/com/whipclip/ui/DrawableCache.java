package com.whipclip.ui;

import java.text.DecimalFormat;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Debug;
import android.support.v4.util.LruCache;
import android.util.Log;

// import android.util.LruCache;

public class DrawableCache
{
//	final int							maxMemoryKb			= (int) (android.os.Debug.getNativeHeapSize() / 1024);	//(int) (Runtime.getRuntime().maxMemory() / 1024);
//	final int							cacheSizeKB		= (int) ((float) maxMemoryKb * 1.2F);					// / 2 ;//((maxMemory * 2D) / 3D);

	final int							maxMemoryKb			= (int) (Runtime.getRuntime().maxMemory() / 1024);
//	final int							cacheSizeKB			= (int) ((float) maxMemoryKb / 8);	// use 1/7th of the availabme memory for this memory cache
	final int							cacheSizeKB			= (int) ((float) maxMemoryKb / 4);	// use 1/4th of the availabme memory for this memory cache
	
	private LruCache<String, Drawable>	_drawMemoryCache	= new LruCache<String, Drawable>(cacheSizeKB)
															{
																@Override
																protected int sizeOf(String key, Drawable draw)
																{
																	if (((BitmapDrawable) draw).getBitmap() == null)
																	{
																		return 0;
																	}
																	Bitmap bitmap = ((BitmapDrawable) draw).getBitmap();
																	int bytes = (bitmap.getRowBytes() * bitmap.getHeight()) / 1024;
//																	Log.e("LRU CACHE", "key: " + key + ", bytes: " + bytes);
																	return bytes;
//																	return ((BitmapDrawable) draw).getBitmap().getByteCount() / 1024;
																}
															};

	public void addDrawableToMemoryCache(String key, Drawable draw)
	{
		if (getDrawableFromMemCache(key) == null)
		{
			_drawMemoryCache.put(key, draw);
		}
	}
	
	public static void logHeap() {
        Double allocated = new Double(Debug.getNativeHeapAllocatedSize())/new Double((1048576));
        Double available = new Double(Debug.getNativeHeapSize())/1048576.0;
        Double free = new Double(Debug.getNativeHeapFreeSize())/1048576.0;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        Log.d("tag", "debug. =================================");
        Log.d("tag", "debug.heap native: allocated " + df.format(allocated) + "MB of " + df.format(available) + "MB (" + df.format(free) + "MB free)");
        Log.d("tag", "debug.memory: allocated: " + df.format(new Double(Runtime.getRuntime().totalMemory()/1048576)) + "MB of " + df.format(new Double(Runtime.getRuntime().maxMemory()/1048576))+ "MB (" + df.format(new Double(Runtime.getRuntime().freeMemory()/1048576)) +"MB free)");
    }

	public Drawable getDrawableFromMemCache(String key)
	{
		return _drawMemoryCache.get(key);
	}

	public void clear()
	{
		_drawMemoryCache.evictAll();
	}

	public void removeDrawableFromMemCache(String key)
	{
		_drawMemoryCache.remove(key);
	}

	public interface IDrawableCache
	{
		public void addToCache(String name, Drawable draw);

		public Drawable getFromCache(String name);
	}
}
