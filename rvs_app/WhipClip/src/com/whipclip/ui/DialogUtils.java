package com.whipclip.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.BitmapFactory.Options;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout.LayoutParams;

import com.whipclip.R;

public class DialogUtils
{
	public static AlertDialog buildMessageDialog(String title, String message, Context context)
	{
		return new AlertDialog.Builder(context).setTitle(title).setMessage(message).create();
	}

	public static AlertDialog buildShortMessageDialog(String message, Context context)
	{
		return new AlertDialog.Builder(context).setMessage(message).setPositiveButton(R.string.ok, new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		}).create();
	}

	public static AlertDialog buildShortMessageDialog(String message, OnClickListener listener, OnClickListener cancelListener, Context context)
	{
		return new AlertDialog.Builder(context).setMessage(message).setPositiveButton(R.string.ok, listener).setNegativeButton(R.string.cancel, cancelListener)
				.create();
	}

	public static AlertDialog buildShortMessageDialog(String message, String title, Context context)
	{
		AlertDialog dialog = buildShortMessageDialog(message, context);
		dialog.setTitle(title);
		return dialog;
	}

	public static AlertDialog buildShortMessageDialog(String message, String title, OnClickListener listener, OnClickListener cancelListener, Context context)
	{
		AlertDialog dialog = buildShortMessageDialog(message, listener, cancelListener, context);
		dialog.setTitle(title);
		return dialog;
	}

	public static AlertDialog buildNoConnectionDialog(OnClickListener listener, OnClickListener cancelListener, Context context)
	{
		return new AlertDialog.Builder(context).setMessage(R.string.service_not_online_message).setTitle(R.string.connection_problem)
				.setPositiveButton(R.string.try_again, listener).setNegativeButton(R.string.cancel, cancelListener).create();
	}

	public static ProgressDialog buildProgressDialog(String message, Context context)
	{
		ProgressDialog pd = new ProgressDialog(context);
		pd.setMessage(message);
		return pd;
	}

	public static Dialog createEditTextDialog(Context context, String dialogTitle, String editTextText, String positiveButtonText, String negativeButtonText,
			final IEditTextDialogListener listener)
	{
		// create edit text
		final EditText editText = new EditText(context);
		LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		editText.setLayoutParams(params);
		editText.setSingleLine();
		editText.setText(editTextText);
		InputFilter[] filterArray = new InputFilter[1];
		filterArray[0] = new InputFilter.LengthFilter(30);
		editText.setFilters(filterArray);
		editText.setSelection(editText.getText().length());

		// create relative layout - for padding
		RelativeLayout layout = new RelativeLayout(context);
		layout.addView(editText);
		layout.setPadding(10, 10, 10, 20);

		// create dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(layout);
		builder.setTitle(dialogTitle).setCancelable(true).setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.cancel();
			}
		}).setPositiveButton(positiveButtonText, new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (listener != null)
				{
					String input = editText.getText().toString();
					listener.onTextViewDialogConfirmClick(input);
				}
			}
		});

		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}

	public static Dialog createSignleButtonAlertDialog(Context context, String dialogTitle, String dialogMessage, String buttonText,
			OnClickListener buttonClickListener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if ((dialogTitle != null) && !dialogTitle.equals(""))
		{
			builder.setTitle(dialogTitle);
		}
		if ((dialogMessage != null) && !dialogMessage.equals(""))
		{
			builder.setMessage(dialogMessage);
		}
		builder.setPositiveButton(buttonText, buttonClickListener);
		return builder.create();
	}

	public static Dialog createTwoButtonsAlertDialog(Context context, String dialogTitle, String dialogMessage, String positiveButtonText,
			String negativeButtonText, OnClickListener positiveButtonClickListener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if ((dialogTitle != null) && !dialogTitle.equals(""))
		{
			builder.setTitle(dialogTitle);
		}
		if ((dialogMessage != null) && !dialogMessage.equals(""))
		{
			builder.setMessage(dialogMessage);
		}

		builder.setCancelable(true).setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.cancel();
			}
		}).setPositiveButton(positiveButtonText, positiveButtonClickListener);

		return builder.create();
	}

	public static Dialog createTwoButtonsAlertDialog(Context context, String dialogTitle, String dialogMessage, String positiveButtonText,
			String negativeButtonText, OnClickListener positiveButtonClickListener, OnClickListener negativeButtonClickListener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if ((dialogTitle != null) && !dialogTitle.equals(""))
		{
			builder.setTitle(dialogTitle);
		}
		if ((dialogMessage != null) && !dialogMessage.equals(""))
		{
			builder.setMessage(dialogMessage);
		}

		builder.setCancelable(true).setNegativeButton(negativeButtonText, negativeButtonClickListener)
				.setPositiveButton(positiveButtonText, positiveButtonClickListener);

		return builder.create();
	}

	public interface OnSettingChanged
	{
		public void onSettingOptionChanged(Options option, String newValue);
	}

	public interface OnJoinedSession
	{
		public void onJoined(String joindId, int retVal);
	}

	public interface OnButtonPressed
	{
		public void onPress();

		public void onContextualPress(View view);
	}

	public interface ISendFeddbackListener
	{
		public void onSendFeddbackPressed(String input);
	}

	public interface ISignatureDialogListener
	{
		public void onSignatureChanged(String accountEmail, String signature, boolean showMoltoSignature);
	}

	public interface IEditTextDialogListener
	{
		public void onTextViewDialogConfirmClick(String input);
	}

}
