package com.whipclip.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.viewpagerindicator.CirclePageIndicator;
import com.whipclip.R;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.ui.adapters.NuxPagerAdapter;

public class NuxFragment extends BaseFragment<Object> implements View.OnClickListener, StatusCallback
{

	public interface OnFacebookStatusCallback {
		public void onCall(Session session, SessionState state, Exception exception);
	}
	
	private OnFacebookStatusCallback mOnFacebookListener;
	
	private final String		TAG_NAME	= getClass().getSimpleName();
	CirclePageIndicator 		mIndicator;
//	private ProgressBar			_loginProgressBar;
	private View				mFragmentView;
	private Activity			mMainActivity;
	
	private UiLifecycleHelper	mUiLifeCycleHlpr;

	private boolean	mIsFirstTime;

	private SignUpView	mSingUpPage;

	private ViewPager	mViewPager;

	public NuxFragment(boolean isFirstTime)
	{
		mIsFirstTime  = isFirstTime;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if (mFragmentView == null)
		{
			mFragmentView = inflater.inflate(R.layout.nux, container, false);
		} else if (mFragmentView.getParent() != null) {
			((ViewGroup) mFragmentView.getParent()).removeView(mFragmentView);
		}
		
		// workaround - reset facebook login state
		AppManager.getInstance().deleteUserDetails();
		
		showMainLogin(savedInstanceState);

		return mFragmentView;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		mMainActivity = activity;
	}

	private void showMainLogin(Bundle savedInstanceState)
	{

		//		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		//		setContentView(R.layout.activity_login);

		initViews();
		initFacebook(savedInstanceState);
		initListeners();
		initViewPager();

	}

	public void initViews()
	{
//		_loginPageBackground = (RelativeLayout) _fragmentView.findViewById(R.id.login_page_background);

	}

	private void initViewPager()
	{
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		
		
		List<View> pages = new ArrayList<View>();


		if (mIsFirstTime) {
			View page = inflater.inflate(R.layout.nux_page, null);
	
			TextView txtTitle = (TextView) page.findViewById(R.id.login_page_header);
			txtTitle.setText(R.string.login_header1);
			TextView txtSubTitle = (TextView) page.findViewById(R.id.login_page_subtitle);
			txtSubTitle.setText(R.string.login_subtitle1);
			txtTitle.setTypeface( AppManager.getInstance().getTypeFaceRegular());
			txtSubTitle.setTypeface(AppManager.getInstance().getTypeFaceLight());
			pages.add(page);
	
			page = inflater.inflate(R.layout.nux_page, null);
			TextView txtTitle1 = (TextView) page.findViewById(R.id.login_page_header);
			txtTitle1.setText(R.string.login_header2);
			ImageView image = (ImageView) page.findViewById(R.id.image);
			image.setImageResource(R.drawable.nux_trending);
			TextView txtSubTitle1 = (TextView) page.findViewById(R.id.login_page_subtitle);
			txtSubTitle1.setText(R.string.login_subtitle2);
			txtTitle1.setTypeface(AppManager.getInstance().getTypeFaceRegular());
			txtSubTitle1.setTypeface(AppManager.getInstance().getTypeFaceLight());
			pages.add(page);
			
		}

		mSingUpPage = (SignUpView)inflater.inflate(R.layout.sign_up_page, null);
		mSingUpPage.init(mSingUpPage, getActivity(), mIsFirstTime);
		mOnFacebookListener = mSingUpPage;

		TextView txtTitle2 = (TextView) mSingUpPage.findViewById(R.id.login_page_header);
		txtTitle2.setText(R.string.login_header3);
		TextView txtSubTitle2 = (TextView) mSingUpPage.findViewById(R.id.login_page_subtitle);
		txtSubTitle2.setText(R.string.login_subtitle3);
		txtTitle2.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		txtSubTitle2.setTypeface(AppManager.getInstance().getTypeFaceLight());
		pages.add(mSingUpPage);
		
		NuxPagerAdapter pagerAdapter = new NuxPagerAdapter(pages);
		mViewPager = (ViewPager) mFragmentView.findViewById(R.id.view_pager);
		final ViewPager viewPager = mViewPager;
		viewPager.setAdapter(pagerAdapter);
		viewPager.setCurrentItem(0);

		
	    mIndicator = (CirclePageIndicator)mFragmentView.findViewById(R.id.indicator);
	    mIndicator.setViewPager(viewPager);
	    mIndicator.setVisibility(mIsFirstTime ? View.VISIBLE : View.GONE);
	    
	    mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{
			
			@Override
			public void onPageSelected(int position)
			{
				if (position == 2) { // sing up
					 AppManager.getInstance().setFirstTime(false); 
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
	}


//	public boolean onBackPressed() {
//		
//		if (!mIsFirstTime) {
//			mMainActivity.finish();
//		} else if (mViewPager.getCurrentItem() == 2) { // sing up page displayed
//			mMainActivity.finish();
//		}
//		return true;
//		
//	}

	@Override
	public void onResume()
	{
		Log.e(getClass().getSimpleName(), "onresume");
		super.onResume();

		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed()))
		{
			onSessionStateChange(session, session.getState(), null);
		}

		mUiLifeCycleHlpr.onResume();
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception)
	{
		if (state.isOpened())
		{
			Log.d(TAG_NAME, "Logged in...");
		}
		else if (state.isClosed())
		{
			Log.d(TAG_NAME, "Logged out...");
		}
		else
		{
			Log.d(TAG_NAME, "Unknown state: " + state);
		}
	}

	@Override
	public void onActivityResult(int arg0, int arg1, Intent arg2)
	{
		super.onActivityResult(arg0, arg1, arg2);
		mUiLifeCycleHlpr.onActivityResult(arg0, arg1, arg2);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		mUiLifeCycleHlpr.onSaveInstanceState(outState);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		mUiLifeCycleHlpr.onPause();
	}

	@Override
	public void onDestroy()
	{
		mUiLifeCycleHlpr.onDestroy();
		super.onDestroy();
		
		if (mSingUpPage != null) {
			mSingUpPage.onDestroy();
		}
		
		mMainActivity = null;
	}

//	//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  //
//	//  ~~~~~~~~~~~~~~~~~~~~~~~  Facebook Methods  ~~~~~~~~~~~~~~~~~~~~~~~~  //
//	//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  //

	protected void initFacebook(Bundle savedInstanceState)
	{
		if (mUiLifeCycleHlpr == null) {
			mUiLifeCycleHlpr = new UiLifecycleHelper(getActivity(), NuxFragment.this);
			mUiLifeCycleHlpr.onCreate(savedInstanceState);
		}
	

		if (mMainActivity instanceof SignUpActivity)
		{
			((SignUpActivity) mMainActivity).setUiLifeCycleHlpr(mUiLifeCycleHlpr);
		}
	}

	@Override
	public void call(Session session, SessionState state, Exception exception)
	{
		if (mOnFacebookListener != null) {
			mOnFacebookListener.onCall(session, state, exception);
		}
	}


	@Override
	public void onDone(Object result)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onFail(Throwable result)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void initViews(View FfragmentView)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}
}
