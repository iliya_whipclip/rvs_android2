package com.whipclip.activities;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.pinterest.pinit.PinItButton;
import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.types.Photo;
import com.tumblr.jumblr.types.PhotoPost;
import com.viewpagerindicator.TabPageIndicator;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.fragments.ComposeFragment;
import com.whipclip.fragments.FeedFragment;
import com.whipclip.fragments.NavigationDrawerFragment;
import com.whipclip.fragments.NavigationDrawerFragment.NavigationType;
import com.whipclip.fragments.NotificationsFragment;
import com.whipclip.fragments.PlaceholderFragment;
import com.whipclip.fragments.PostsFragment;
import com.whipclip.fragments.PrivacyFragment;
import com.whipclip.fragments.SearchFragment;
import com.whipclip.fragments.SettingsFragment;
import com.whipclip.fragments.ShowPostsFragment;
import com.whipclip.fragments.TVShowsTabFragment;
import com.whipclip.fragments.TosFragment;
import com.whipclip.fragments.UserProfileFragment;
import com.whipclip.logic.AnalyticsManager;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.SignUpBroadcastReceiver;
import com.whipclip.logic.SignUpBroadcastReceiver.SingUpStatusChanged;
import com.whipclip.logic.UserDetails;
import com.whipclip.logic.UserDetails.IOnDetailsArrived;
import com.whipclip.logic.WCNotificationManager;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSFeedCountDetails;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.ui.DialogUtils;
import com.whipclip.ui.TypefaceSpan;
import com.whipclip.ui.adapters.ShareMoreOptionsAdapter;
import com.whipclip.ui.adapters.TabFragmentAdapter;
import com.whipclip.ui.adapters.items.searchitems.DialogAdapter;
import com.whipclip.ui.views.DevSettingsManager;
import com.whipclip.ui.views.NonSwipeableViewPager;
//import android.app.FragmentManager;


public class MainActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, StatusCallback//, OnQueryTextListener, OnFocusChangeListener
		, IOnDetailsArrived, SingUpStatusChanged
{
	
	Logger logger = LoggerFactory.getLogger(MainActivity.class);

	public static final int REQUEST_CODE_NONE 				= -1; 
	public static final int REQUEST_CODE_LOGIN 				= 1; 
	public static final int REQUEST_CODE_WIRELES_SETTINGS 	= 556;
	
	private NavigationDrawerFragment	_navigationDrawerFragment;

	private CharSequence				_activityTitle;
	private boolean						_doingUser;

	private UiLifecycleHelper			_uiLifeCycleHlpr;

	AlertDialog							_dialog				= null;

	private Timer						_notificationTimer;
	private TimerTask					_notificationTimerTask;

//	private PendingIntent				_pendingNotificationIntent;
//	private AlarmManager				_notificationAlarmManager;

	private DevSettingsManager			devSetting;

	private Bundle						_pendingShare;
	private SignUpBroadcastReceiver 	mSingUpBroadcastReceiver = new SignUpBroadcastReceiver(this);

	
	NonSwipeableViewPager mPager;
	TabPageIndicator mIndicator;
    private int mPagerCurrentIndex 	= -1;
    private int mPagerPrevIndex 	= -1;
	private TabFragmentAdapter	mAdapter;

	private String	mLaunchIntetUri;

	private int	_selectedPosition;

	private boolean	mUseNavigation = true;

	private boolean	mSwitchTabOnLoggedInCancel = true;

	private Handler mHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
//		overridePendingTransition(R.anim.anim_alpha2, R.anim.anim_alpha);

		mSingUpBroadcastReceiver.registerReceiver();
		AnalyticsManager.createManager(this);

		
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_layout);

		_uiLifeCycleHlpr = new UiLifecycleHelper(this, this);
		_uiLifeCycleHlpr.onCreate(savedInstanceState);

		
		_navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
		_activityTitle = getTitle();

		// Set up the drawer.
		_navigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

		initNotificationAlarmManager();
		devSetting = new DevSettingsManager(this);
		
		
		mAdapter = new TabFragmentAdapter(getSupportFragmentManager());

		mPager = (NonSwipeableViewPager)findViewById(R.id.pager);
	    mPager.setAdapter(mAdapter);
	    mPager.setOffscreenPageLimit(5); //  predefine number of "cached" pages
	
	    mIndicator = (TabPageIndicator)findViewById(R.id.indicator);
	    mIndicator.setTabIconLocation (TabPageIndicator.LOCATION_UP);
	    mIndicator.setTypeFace(AppManager.getInstance().getTypeFaceLight());
	    mIndicator.setViewPager(mPager);
	    mIndicator.setOnTabReselectedListener(new TabPageIndicator.OnTabReselectedListener() {
			
			@Override
			public void onTabReselected(int position)
			{
//                Toast.makeText(MainActivity.this, "Clicked to page " + position, Toast.LENGTH_SHORT).show();
        		switch (position)
        		{
        			case TabFragmentAdapter.TAB_POSITION_HOME:
        			case TabFragmentAdapter.TAB_POSITION_POPULAR:
        			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
//        			case TabFragmentAdapter.TAB_POSITION_PROFILE:
        			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
        			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
        				popBackTabStack(position);
        				Fragment mainTabFragment = getMainTabFragment(position);
        				if (mainTabFragment instanceof BaseFragment) {
        					((BaseFragment)mainTabFragment).onTabReselected();
        				}
        				break;
        		}
        				
				
			}
		});

	    
	  //We set this on the indicator, NOT the pager
        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(MainActivity.this, "Changed to page " + position, Toast.LENGTH_SHORT).show();
                
				Fragment mainTabFragment = getMainTabFragment(mPagerCurrentIndex);
				if (mainTabFragment instanceof BaseFragment) {
					((BaseFragment)mainTabFragment).onTabDeselected();
				}

				// each time the tab was switched clear the cache 
				AppManager.getInstance().clearImagesCache();
				
                mPagerPrevIndex 	= mPagerCurrentIndex;
                mPagerCurrentIndex 	= position;
                
                dismissKeyboard();
                
//                updateActionBarLogoNotifications();
              //If we've selected the same draw, we won't open a new instance.
        		switch (position)
        		{
        			case TabFragmentAdapter.TAB_POSITION_HOME:
        				mSwitchTabOnLoggedInCancel = true;
        				if (!AppManager.getInstance().isLoggedIn()) {
        					AppManager.getInstance().showLogin(MainActivity.this, 1);
        				}
        				else {
        					
        				}
        				break;
        				
//        			case TabFragmentAdapter.TAB_TYPE_POPULAR:
//        				return  new PostsFragment();	
        				
//        			case TabFragmentAdapter.TAB_TYPE_CLIP_TV:
//        				break;
//        			case TabFragmentAdapter.TAB_TYPE_TV_SHOWS:
//        				break;
        			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
//        			case TabFragmentAdapter.TAB_POSITION_PROFILE:
        				mSwitchTabOnLoggedInCancel = true;
        				if (!AppManager.getInstance().isLoggedIn()) {
        					AppManager.getInstance().showLogin(MainActivity.this, 1);
        				}
//        				else {
////        				Fragment mainTabFragment = getMainTabFragment(TabFragmentAdapter.TAB_POSITION_PROFILE);
//        					UserProfileFragment userFragment = (UserProfileFragment)((FragmentViewPagerTab)mainTabFragment).getBaseFragment();
//        					if (userFragment != null) {
//        						userFragment.initWithMyUser();
//        					}
//        				}
        				break;
        				
        		}
        		
        		switch (position)
        		{
        			case TabFragmentAdapter.TAB_POSITION_HOME:
        			case TabFragmentAdapter.TAB_POSITION_POPULAR:
        			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
//        			case TabFragmentAdapter.TAB_POSITION_PROFILE:
        			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:	
        			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
        				popBackTabStack(position);
        				mainTabFragment = getMainTabFragment(position);
        				if (mainTabFragment instanceof BaseFragment) {
        					((BaseFragment)mainTabFragment).onTabSelected();
        				}
        				break;
        		}
        		
        		updateActionTitleOnTabChanged(position);

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        
    	setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//    	onIntentReceive(getIntent());
    	
    	// Unittest
    	
//    	 "9242879", "us_own", 1418450400000
    
    	// postpone till tabs will be initialized
    	mHandler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				onIntentReceive(getIntent());
			}
		}, 100);

        // first time show login page
        if (AppManager.getInstance().isFirstTime()) 
		{
        	AppManager.getInstance().showLogin(MainActivity.this, REQUEST_CODE_LOGIN, true);
		}
        
        
        restoreActionBar();
        
//        startMonitringMem();
	}
	
	private void startMonitringMem()
	{
		mHandler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				com.whipclip.rvs.sdk.util.Logger.logMemory();
				startMonitringMem();
			}
		}, 1000);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		onIntentReceive(intent);
	}
	
	
	private boolean onIntentReceive(Intent intent) {
		  
        if (intent != null && intent.getAction() != null) {
        	
        	setLaunchIntetUri(intent.toUri(0));
        	
        	String action = intent.getAction();
        	logger.info("start by intent action: " + action);
        	if (action.equals(WCNotificationManager.ACTION_USER_NOTIFICATIONS_SYNC)) {
        		
				setLaunchIntetUri(null);
				
//				Bundle bundle = new Bundle();
//				bundle.putString(ShowPostsFragment.SHOW_ID, showId);
//				bundle.putString(ShowPostsFragment.CHANNEL_ID, channelId);
//				replaceFragmentContent(ShowPostsFragment.class, bundle);
				
				if (AppManager.getInstance().isLoggedIn()) {
					setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS);
				}
				
        	} if (action.equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY) || 
        			action.equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY)) {
        		
        		logger.info("open tv shows tab");
        		showShowDetailsPageByLaunchIntent();
        		return true;
//        		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
//	        		mIndicator.setCurrentItem(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
//	        		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
//
//        	}
//        	else if (action.equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY)) {
//        		
//        		logger.info("open clip tv tab");
////        		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_CLIP_TV);
////	        		mIndicator.setCurrentItem(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);
//	        		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_TV_SHOWS);

        	}
//        	else {
//            	setLaunchIntetUri(null);
//            	setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//        	}
        	
        	
        } 
//        else {
//        	setLaunchIntetUri(null);
//        	setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//        }
        
        return false;
	}
	
	public void setCurrentPageItem(int position) {
		mPager.setCurrentItem(position);
	}
	
	public String getLaunchIntetUri() {
		return mLaunchIntetUri;
	}
	
	private void updateActionTitleOnTabChanged(int position)
	{
		
		
		// We retrieve the fragment manager of the activity
		boolean foundTitle = false;
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			foundTitle = setActionBarTitleForFragmentManager(getSupportFragmentManager());
		}
		
		if (!foundTitle) {
			FragmentManager frgmtManager = getSupportFragmentManager();
		    Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());
		    if (fragment != null) {
			    FragmentManager childFragmentManager = fragment.getChildFragmentManager(); //childFragmentManager.getBackStackEntryCount()
			    foundTitle = setActionBarTitleForFragmentManager(childFragmentManager);
		    }
		}
		
		if (foundTitle) {
			return ;
		}
		
//		    FragmentManager frgmtManager = getSupportFragmentManager();
//		    Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());
//		    if (fragment != null) {
//			    FragmentManager childFragmentManager = fragment.getChildFragmentManager(); //childFragmentManager.getBackStackEntryCount()
//			    List<Fragment> fragments = childFragmentManager.getFragments();
//			    // find child title
//			    if (fragments != null && fragments.size() > 0) {
//			    	// if i == 0 - main tab fragment
//				    for (int i = fragments.size() - 1; i >=0 ; i--) {
//					    	Fragment childFragment = fragments.get(i);
//					    	if (childFragment == null) continue;
//					    	
//						    if (childFragment instanceof BaseFragment) { 
//						    	_activityTitle = ((BaseFragment) childFragment).getActionBarTitle();
//								setTitle();
//								hideNavigation();
//								return ;
//						    }
//					    }
//			    }
//		    }
//		    
//		    return;
//		}
		
		switch (position)
		{
			case TabFragmentAdapter.TAB_POSITION_HOME:
				_activityTitle = "";//getString(R.string.tab_title_home);
				break;
			case TabFragmentAdapter.TAB_POSITION_POPULAR:
				_activityTitle = getString(R.string.tab_title_popular);
				break;
			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
				_activityTitle = getString(R.string.tab_title_clip_tv);
				break;
			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
				_activityTitle = getString(R.string.tab_title_tv_shows);
				break;
//			case TabFragmentAdapter.TAB_POSITION_PROFILE:
//				_activityTitle = getString(R.string.tab_title_profile);
//				break;
			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
				_activityTitle = getString(R.string.tab_title_notifications);
				break;
				
		}
		
		setTitle();
		showNavigation();
	}

	private boolean setActionBarTitleForFragmentManager(FragmentManager childFragmentManager)
	{
	    List<Fragment> fragments = childFragmentManager.getFragments();
	    // find child title
	    if (fragments != null && fragments.size() > 0) {
	    	// if i == 0 - main tab fragment
		    for (int i = fragments.size() - 1; i >=0 ; i--) {
			    	Fragment childFragment = fragments.get(i);
			    	if (childFragment == null) continue;
			    	
				    if (childFragment instanceof BaseFragment && childFragment.isAdded()) { 
				    	_activityTitle = ((BaseFragment) childFragment).getActionBarTitle();
						setTitle();
						hideNavigation();
						return true;
				    }
			    }
	    }
		return false;
	}

	private void initNotificationAlarmManager()
	{

		long lastFetchNotifications = AppManager.getInstance().getPrefrences(Consts.PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE, 0);
		if (lastFetchNotifications == 0)
		{ // first time, initialize time to current
			setLastFetchNotifications(-1);
		}
		// iliya moved code to constructor of notificationManager
		
	}

	private void scheduleUserSyncUpdate()
	{
		AppManager.getInstance().getNotificationManager().scheduleUserSyncUpdate();
	}

	private void stopNotificationAlarm()
	{
		WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
//		if (notificationManager != null) {
//			notificationManager.cancelUserSyncUpdate();
//		}
	}

	private void showSettingsDialog(final Bundle bundle)
	{
		Context context = MainActivity.this;

		final Dialog dialog = new Dialog(context);
//		dialog.setCancelable(false);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(R.layout.dialog_layout);

		ListView dialogList = (ListView) dialog.findViewById(R.id.dialog_list);

		ArrayList<String> items = new ArrayList<String>();
		
		final boolean loggedIn = AppManager.getInstance().isLoggedIn();
		if (!loggedIn)
		{
			items.add(context.getResources().getString(R.string.about));
			items.add(context.getResources().getString(R.string.sign_in));
		}
		else
		{
			items.add(context.getResources().getString(R.string.my_profile));
			items.add(context.getResources().getString(R.string.about));
			items.add(context.getResources().getString(R.string.sign_out));
		}
		items.add(context.getString(R.string.dialog_cancel));

		final DialogAdapter adapter = new DialogAdapter(context, items);
		dialogList.setAdapter(adapter);

		dialogList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
			{
				dialog.dismiss();
				
				if (loggedIn) {
					switch (position) {
						case 0: // my profile
							String userId = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);
//							openUserProfileActivity(TabFragmentAdapter.TAB_POSITION_PROFILE, userId, true);
//							openUserProfileActivity(mPager.getCurrentItem(), userId, true);
//							openUserProfileAsChild(userId, true);
							openUserProfile(userId, true);
							break;
						case 1: // about
							showAbout();
							break;
						 
						case 2: // sign out
							signOut();
//							MainActivity.this.navigateToPosition(NavigationType.TRENDING.ordinal());
							break;
						
						default:
							break;
					}
					
				} else { // sing out
					switch (position) {
						case 0: // about
							showAbout();
							break;
							
						case 1: // sing in
							AppManager.getInstance().showLogin(MainActivity.this, REQUEST_CODE_LOGIN);
							break;
							
						default:
							break;
					}
				}
			}


			private void showAbout()
			{
				replaceFragmentContent(getFragmentClassByNavigationIndex(NavigationType.SETTINGS.ordinal()), true, R.anim.slide_in_right, R.anim.slide_out_right,
						R.anim.slide_in_right_exit, R.anim.slide_out_right, bundle);
			}
		});

		dialog.show();
	}

	public void signOut()
	{
		AppManager.getInstance().deleteUserDetails();
		MainActivity.this.resetProfileInformationUI();
		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
		
	}
	private void showDevSetting()
	{
		devSetting.show();

	}

	private Class<? extends BaseFragment> getFragmentClassByNavigationIndex(int position)
	{
//		if (position == NavigationType.LIKED.ordinal()) {
//			return PostsFragment.class;
//			
//		} else if (position == NavigationType.LIKED.ordinal()) {
//			return FeedFragment.class;
//			
//		} else if (position == NavigationType.LIKED.ordinal()) {
//			return RecentFragment.class;
//			
//		} else if (position == NavigationType.LIKED.ordinal()) {
//			return LikedPostsFragment.class;
//			
//		} else 
			
//		if (position == NavigationType.NOTIFICATIONS.ordinal()) {
//			return NotificationsFragment.class;
//			
//		} else
			
		if (position == NavigationType.SETTINGS.ordinal()) {
		
			return SettingsFragment.class;
			
		} if (position == NavigationType.PRIVACY_POLICY.ordinal()) {
			
			return PrivacyFragment.class;
		
		}else if (position == NavigationType.TERM_OF_SERVICE.ordinal()) {
			
			return TosFragment.class;
			
		} else{
			
			return PlaceholderFragment.class;
			
		} 
			
	}

	public void onSectionAttached(int number)
	{
		if (mPager != null) {
			updateActionTitleOnTabChanged(mPager.getCurrentItem());
		}
	}

	private void openMailIntent()
	{
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.setType("message/rfc822");
		//		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { getResources().getString(R.string.feedback_recipient) });
		//		intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_subject));
		//		intent.putExtra(Intent.EXTRA_TEXT, "Device: " + android.os.Build.MODEL + "(" + android.os.Build.DEVICE + ")" + "\nSDK: "
		//				+ android.os.Build.VERSION.SDK_INT + "\nApp: " + WhipClipApplication._appName + "\nVersion: " + WhipClipApplication.getVersionNumber());

		String uriText = "mailto:"
				+ Uri.encode(getResources().getString(R.string.feedback_recipient))
				+ "?subject="
				+ getString(R.string.feedback_subject)
				+ "&body="
				+ Uri.encode("Device: " + android.os.Build.MODEL + "(" + android.os.Build.DEVICE + ")" + "\nSDK: " + android.os.Build.VERSION.SDK_INT
						+ "\nApp: " + WhipClipApplication._appName + "\nVersion: " + WhipClipApplication.getVersionNumber());
		Uri uri = Uri.parse(uriText);

		intent.setData(uri);

		Intent mailer = Intent.createChooser(intent, "Send Email");
		startActivity(mailer);
//		selectNavigationItem(0,false);
		
	}

	public void selectNavigationItem(int position, boolean fromItemClick)
	{
		_navigationDrawerFragment.selectItem(0, false);
		
		int savePriviousPosition = _selectedPosition;
		_selectedPosition = position;
		//If we've selected the same draw, we won't open a new instance.
		if ((fromItemClick == false) || (fromItemClick && (savePriviousPosition != _selectedPosition
						|| _selectedPosition == NavigationType.DEVELOPMENT.ordinal() || _selectedPosition == NavigationType.SETTINGS.ordinal())))
		{
//			if ((//position == NavigationType.FEED.ordinal() || position == NavigationType.FEED.LIKED.ordinal() || 
//					position == NavigationType.NOTIFICATIONS.ordinal()) && !AppManager.getInstance().isLoggedIn())
//			{
//				AppManager.getInstance().showLogin(MainActivity.this, MainActivity.REQUEST_CODE_LOGIN);
//				_navigationDrawerFragment.closeDrawer();
//			}
//			else
//			{
				onNavigationDrawerItemSelected(position);
//			}
		}
	}
	
	@Override
	public void onNavigationDrawerItemSelected(int navigationPosition) {
		
		final Bundle bundle = new Bundle();
		bundle.putInt("section_number", navigationPosition);
		bundle.putBoolean(Consts.MENU_ITEM_SELECT, true);
//		if (navigationPosition == NavigationType.LIKED.ordinal())
//		{
//			bundle.putString(Consts.EXTRA_USER_ID, AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID));
//		}
//		if (navigationPosition == NavigationType.NOTIFICATIONS.ordinal())
//		{
//			_navigationDrawerFragment.setNotifications(0);
//		}
		
		if (navigationPosition == NavigationType.LOGIN.ordinal()) {
			
			if (AppManager.getInstance().isLoggedIn()) {
				signOut();
			} else {
				isLoggedIn(null, false);
//				AppManager.getInstance().showLogin(MainActivity.this,  REQUEST_CODE_LOGIN);
			}
			
		} else if (navigationPosition == NavigationType.FEEDBACK.ordinal())
		{
			openMailIntent();
		}
		else if (navigationPosition == NavigationType.SETTINGS.ordinal())
		{
			showSettingsDialog(bundle);
		}
		else if (navigationPosition == NavigationType.DEVELOPMENT.ordinal())
		{
			showDevSetting();
		}
		else if (navigationPosition == NavigationType.PRIVACY_POLICY.ordinal()) {
			
			replaceFragmentContent(getFragmentClassByNavigationIndex(NavigationType.PRIVACY_POLICY.ordinal()), true, R.anim.slide_in_right, R.anim.slide_out_right,
					R.anim.slide_in_right_exit, R.anim.slide_out_right, bundle);
		}  else if (navigationPosition == NavigationType.TERM_OF_SERVICE.ordinal()) {
			
			replaceFragmentContent(getFragmentClassByNavigationIndex(NavigationType.TERM_OF_SERVICE.ordinal()), true, R.anim.slide_in_right, R.anim.slide_out_right,
					R.anim.slide_in_right_exit, R.anim.slide_out_right, bundle);
		}
		else
		{
//			replaceFragmentContent(getFragmentClassByIndex(position), true, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit,
//					R.anim.slide_out_right, bundle);
			
//			if (position != NavigationType.TRENDING.ordinal()) {
//				setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//			} else {
				replaceFragmentContent(getFragmentClassByNavigationIndex(navigationPosition), true, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit,
					R.anim.slide_out_right, bundle);
//			}
		}
	}
	
	public void resetProfileInformationUI()
	{
		_navigationDrawerFragment.resetProfileInformationUI();
	}

	private void updateActionBarLogoNotifications()
	{
		// TODO ILIYA
		int notifications = _navigationDrawerFragment.getNotifications();
//		if (notifications > 0)
//		{
//			LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//			View v = inflater.inflate(R.layout.logo_notifications, null, true);
//			BitmapDrawable bitmap = getBitmapFromView(v, notifications);
//			if (bitmap != null)
//			{
//				getActionBar().setLogo(bitmap);
//			}
//		}
//		else
//		{
//			getActionBar().setLogo(R.drawable.logo);
			
//		}
			
		if (mPager.getCurrentItem() == 0) {	
			getActionBar().setLogo(R.drawable.logo_white);
		} else {
			getActionBar().setLogo(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		}
	}

	public void restoreActionBar()
	{
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowHomeEnabled(true);
		updateActionTitleOnTabChanged(mPager.getCurrentItem());
	}

	public void updateActionTitle() {
		updateActionTitleOnTabChanged(mPager.getCurrentItem());
	}
	
	public BitmapDrawable getBitmapFromView(View view, int notifications)
	{
		try
		{
			TextView tv = (TextView) view.findViewById(R.id.logo_notifications);
			tv.setText((notifications > 9 ? "" : " ") + notifications + (notifications > 9 ? "" : " "));
			tv.setTypeface(AppManager.getInstance().getTypeFaceMedium());
			view.measure(500, 100);

			Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
			Canvas c = new Canvas(b);
			view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
			view.draw(c);
			return new BitmapDrawable(b);
		}
		catch (Exception e)
		{

		}

		return null;

	}

	public void setTitle()
	{
		CharSequence title = getActionBar().getTitle();
		if (title != null && !title.toString().equals(_activityTitle)) {
			SpannableString titleSpan = new SpannableString(_activityTitle);
			titleSpan.setSpan(new TypefaceSpan(this, Consts.FONT_BENTON_SANS_REGULAR), 0, titleSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			getActionBar().setTitle(titleSpan);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (!_navigationDrawerFragment.isDrawerOpen())
		{
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			//			getMenuInflater().inflate(R.menu.main, menu);
			//			restoreActionBar();
		}

		return super.onCreateOptionsMenu(menu);
	}

	private boolean isCurrentFragment(String fragmentName)
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(fragmentName);
		return (fragment != null && fragment.isVisible());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (item.getItemId() == R.id.action_search)
		{
			if (isCurrentFragment(SearchFragment.class.getSimpleName()) == false) //Only if the current fragment isn't SearchFragment we'll open it.
			{
				replaceFragmentContent(SearchFragment.class, null);
			}
			return true;
		}
		else if (item.getItemId() == android.R.id.home )// && getSupportFragmentManager().getBackStackEntryCount() >= 1)
		{
			if (!onBackPressedProcess()) {
				return super.onOptionsItemSelected(item);
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void initViews()
	{

	}

	@Override
	public void initListeners()
	{

	}

	public void setFullScreen()
	{
		findViewById(R.id.frame1).setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		getActionBar().hide();
		mIndicator.setVisibility(View.GONE);
		_navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	public void switchOffFullScreen()
	{
		mIndicator.setVisibility(View.VISIBLE);
		findViewById(R.id.frame1).setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
		_navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getActionBar().show();
	}

	private void hideNavigation()
	{
		if (_navigationDrawerFragment != null) {
			_navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			_navigationDrawerFragment.getDrawerTOggle().setDrawerIndicatorEnabled(false);
		}
		
		getActionBar().setLogo(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
	}

	// hack, check tabview fragment onresume state 
	public void useNavigation(boolean useNavigation) {
		mUseNavigation  = useNavigation;
		
		if (!mUseNavigation) hideNavigation();
		else showNavigation();
		
	}
	
	
	private void showNavigation()
	{
		if (_currentFragment instanceof ComposeFragment || !mUseNavigation) 
			return;
		
		_navigationDrawerFragment.getDrawerTOggle().setDrawerIndicatorEnabled(true);
		_navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		
		updateActionBarLogoNotifications();
	}

	public void setActivityTitle(String title)
	{
		_activityTitle = title;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		WhipClipApplication.setCurrentActivity(this);
		_uiLifeCycleHlpr.onResume();

		restartTimer();

//		stopNotificationAlarm();
		
		//clear state
		RVSSdk sdk = RVSSdk.sharedRVSSdk();
	    if (sdk.isSignedOut() && sdk.isPendingSignIn()){
	    	RVSSdk.sharedRVSSdk().cancelPendingSignIn();
	    }

	}

	@Override
	public void onPause()
	{
		_uiLifeCycleHlpr.onPause();
		super.onPause();
		if (_dialog != null && _dialog.isShowing())
		{
			_dialog.dismiss();
		}
		cancelTimer();
		// Activate AlarmManager - if Logged-in
		AppManager.getInstance().clearImagesCache();
	
//		scheduleUserSyncUpdate();
	}

	private void restartTimer()
	{
		cancelTimer();
		if (AppManager.getInstance().isLoggedIn())
		{
			_notificationTimer = new Timer();
			_notificationTimerTask = new MyTimerTask();
			_notificationTimer.scheduleAtFixedRate(_notificationTimerTask, 0, 1000 * 60);
		}
	}

	@Override
	public void onDestroy()
	{
		_uiLifeCycleHlpr.onDestroy();
		AnalyticsManager.sharedManager().onDestroy();
		
		mPager.setAdapter(null);
		mPager = null;
		mAdapter = null;
		
		_navigationDrawerFragment = null;
		mIndicator = null;
				
		AppManager.getInstance().clearImagesCache();
		mSingUpBroadcastReceiver.unregisterReceiver();

		WhipClipApplication.setCurrentActivity(null);
		
		mHandler.removeCallbacksAndMessages(null);
		
		super.onDestroy();
	}
	
	public boolean openUserProfile(String userId, final boolean cameFromSideMenu) {
		
		boolean isProfilFragment = getCurrentFragment() instanceof UserProfileFragment;
		if (userId != null && !userId.isEmpty() && !_doingUser && 
				(isProfilFragment && !((UserProfileFragment) getCurrentFragment()).getUserId().equals(userId)) || !(isProfilFragment))
		{
			final UserProfileFragment profileFragment = new UserProfileFragment();
//			mAdapter.addChildFragment(tabPosition, profileFragment);
//			replaceFragmentContent(UserProfileFragment.class, null);
			replaceFragmentContent(profileFragment, null);
			hideNavigation();
			
			_doingUser = true;
			// Always get updated user profile
			RVSPromise<RVSUser, Error, Integer> prom = AppManager.getInstance().getSDK().userForUserId(userId);
			prom.then(new DoneCallback<RVSUser>()
			{
				@Override
				public void onDone(final RVSUser result)
				{
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							if (result != null)
							{
								AppManager.getInstance().saveSDKUser(result);
//								if (getCurrentFragment() instanceof UserProfileFragment)
//								{
								if (profileFragment.isAdded()) {
									profileFragment.initViewsAfterLoadingResults();
								}
//								}
								_doingUser = false;
							}
						}
					});
				}
			}, new FailCallback<Error>()
			{
				@Override
				public void onFail(Error result)
				{
					_doingUser = false;
				}
			});
			
			return true;
		}
		
		
		return false;
		
	}

	
//	@SuppressWarnings("unchecked")
//	public boolean openUserProfileAsChild(int tabPosition, final String userID, final boolean cameFromSideMenu)
//	{
//		boolean isProfilFragment = getCurrentFragment() instanceof UserProfileFragment;
//		if (userID != null && !userID.isEmpty() && !_doingUser && 
//				(isProfilFragment && !((UserProfileFragment) getCurrentFragment()).getUserId().equals(userID)) || !(isProfilFragment))
//		{
//			final UserProfileFragment profileFragment = new UserProfileFragment();
//			mAdapter.addChildFragment(tabPosition, profileFragment);
//			hideNavigation();
//			
//			_doingUser = true;
//			// Always get updated user profile
//			RVSPromise<RVSUser, Error, Integer> prom = AppManager.getInstance().getSDK().userForUserId(userID);
//			prom.then(new DoneCallback<RVSUser>()
//			{
//				@Override
//				public void onDone(final RVSUser result)
//				{
//					runOnUiThread(new Runnable()
//					{
//						@Override
//						public void run()
//						{
//							if (result != null)
//							{
//								AppManager.getInstance().saveSDKUser(result);
////								if (getCurrentFragment() instanceof UserProfileFragment)
////								{
//									profileFragment.initViewsAfterLoadingResults();
////								}
//								_doingUser = false;
//							}
//						}
//					});
//				}
//			}, new FailCallback<Error>()
//			{
//				@Override
//				public void onFail(Error result)
//				{
//					_doingUser = false;
//				}
//			});
//			
//			return true;
//			
//		}
//		
//		return false;
//		
//	}

	@SuppressWarnings("unchecked")
	public void openUserProfileActivity1(int tabPosition, final String userID, final boolean cameFromSideMenu)
	{
		boolean isProfilFragment = getCurrentFragment() instanceof UserProfileFragment;
		if (userID != null && !userID.isEmpty() && !_doingUser && 
				(isProfilFragment && !((UserProfileFragment) getCurrentFragment()).getUserId().equals(userID)) || !(isProfilFragment))
		{
			final Bundle bundle = new Bundle();
			if (cameFromSideMenu)
			{
				bundle.putBoolean(Consts.MENU_ITEM_SELECT, true);
			}
			replaceFragmentContent(UserProfileFragment.class, true, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit,
					R.anim.slide_out_right, bundle);

			_doingUser = true;
			// Always get updated user profile
			RVSPromise<RVSUser, Error, Integer> prom = AppManager.getInstance().getSDK().userForUserId(userID);
			prom.then(new DoneCallback<RVSUser>()
			{
				@Override
				public void onDone(final RVSUser result)
				{
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							if (result != null)
							{
								AppManager.getInstance().saveSDKUser(result);
								if (getCurrentFragment() instanceof UserProfileFragment)
								{
									((UserProfileFragment) getCurrentFragment()).initViewsAfterLoadingResults();
								}
								_doingUser = false;
							}
						}
					});
				}
			}, new FailCallback<Error>()
			{
				@Override
				public void onFail(Error result)
				{
					_doingUser = false;
				}
			});
		}
	}

	public void refreshSideMenu()
	{
		if (_navigationDrawerFragment != null)
		{
			_navigationDrawerFragment.initViewItems();
		}
	}

	public int getNotifications()
	{
		return _navigationDrawerFragment.getNotifications();
	}

	public void resetNotifications()
	{
		try
		{
			_navigationDrawerFragment.setNotifications(0);
			updateActionBarLogoNotifications();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.e(getClass().getSimpleName(), "Code: " + resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		_uiLifeCycleHlpr.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Consts.ACTIVITY_RESULT_EXIT) {
			super.onBackPressed();
			return;
		}
		
		if (requestCode ==  MainActivity.REQUEST_CODE_WIRELES_SETTINGS && _dialog != null && _dialog.isShowing())
		{
			_dialog.dismiss();
		}
		else if (resultCode == Consts.LOGIN_OK)
		{
			_navigationDrawerFragment.onLoggedIn(false);
			WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
			notificationManager.scheduleUserSyncUpdate();
			
		}
		else
		if (requestCode == REQUEST_CODE_LOGIN && mPagerPrevIndex != -1) {
			
			onLoggedIn();
			WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
			notificationManager.scheduleUserSyncUpdate();
			
		}
		
	    FragmentManager frgmtManager = getSupportFragmentManager();
		Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());
		if (fragment != null) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}

	private void onLoggedIn()
	{
		if (!AppManager.getInstance().isLoggedIn() && mSwitchTabOnLoggedInCancel) {
			setCurrentPageItem(mPagerPrevIndex);			
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		_uiLifeCycleHlpr.onSaveInstanceState(outState);
	}

	public boolean isLoggedIn(BaseFragment fragment, boolean switchTabOnCancel)
	{
		mSwitchTabOnLoggedInCancel  = switchTabOnCancel;
		
		if (AppManager.getInstance().isLoggedIn())
		{
			return true;
		}
		else
		{
			
			AppManager.getInstance().showLogin(MainActivity.this,  REQUEST_CODE_LOGIN);
			return false;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		
		
	}

	public int getActionBarHeight()
	{
		TypedValue tv = new TypedValue();
		int actionBarHeight = 0;
		if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
		}
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0)
		{
			result = getResources().getDimensionPixelSize(resourceId);
		}
		actionBarHeight += result;
		return actionBarHeight;
	}

	public void setLastFetchNotifications(long timestamp)
	{
		Log.i(getClass().getSimpleName(), "setLastFetchNotifications " + timestamp);
		if (timestamp > 0)
		{
			AppManager.getInstance().savePrefrences(Consts.PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE, timestamp);
		}
		else
		{
			AppManager.getInstance().savePrefrences(Consts.PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE, new Date().getTime());
		}

// 		TODO Iliya schedule notification		
//		scheduleNotificationAlarm();
	}

	private void cancelTimer()
	{
		if (_notificationTimer != null)
		{
			_notificationTimer.cancel();
			_notificationTimer.purge();
		}
		if (_notificationTimerTask != null)
		{
			_notificationTimerTask.cancel();
		}
	}

	public void noNetworkDialog()
	{
		if ((_dialog == null || _dialog.isShowing() == false) && AppManager.getInstance().hasInternetConnection() == false)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					final AlertDialog dialog = DialogUtils.buildShortMessageDialog(getString(R.string.connect_to_internet), getString(R.string.no_network),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									dialog.dismiss();
									if (AppManager.getInstance().hasInternetConnection() == false)
									{
										startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS),  MainActivity.REQUEST_CODE_WIRELES_SETTINGS);
									}
								}
							}, new DialogInterface.OnClickListener()
							{

								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									dialog.dismiss();
								}
							}, MainActivity.this);
					dialog.setCanceledOnTouchOutside(true);
					dialog.setCancelable(false);
					dialog.show();
				}
			});
		}
	}

	public void showFacebookDialog(PendingCall present)
	{
		_uiLifeCycleHlpr.trackPendingDialogCall(present);
	}

	@Override
	public void call(Session session, SessionState state, Exception exception)
	{
		Log.i(getClass().getSimpleName(), "Call");
		if (state.isOpened() && _pendingShare != null)
		{
			shareWeb(_pendingShare);
		}
	}

	public void pinIt(String string, String photoUrl)
	{
		PinItButton pinIt = new PinItButton(this);
		PinItButton.setPartnerId(Consts.PINTEREST_APP_ID);
		pinIt.setImageUrl(photoUrl);
		pinIt.setUrl(string);
		pinIt.setDescription("");
		pinIt.performClick();
	}

	public void shareTumblr(String string, String text, String imageURL)
	{
		AppManager.getInstance().getTumblrLogic().logOutOfTumblr();
		if (AppManager.getInstance().getTumblrLogic().isConnected())
		{
			tumblrShare(text + "\n" + string, imageURL);

		}
		else
		{
			AppManager.getInstance().getTumblrLogic().connect(this, this, string, imageURL);
		}
	}

	private void tumblrShare(final String string, final String imageURL)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				JumblrClient client = new JumblrClient(Consts.TUMBLR_CONSUMER_KEY, Consts.TUMBLR_CONSUMER_SECRET);
				client.setToken(AppManager.getInstance().getTumblrLogic().getToken(), AppManager.getInstance().getTumblrLogic().getTokenSecret());

				PhotoPost post = null;
				try
				{
					post = client.newPost(client.user().getBlogs().get(0).getName(), PhotoPost.class);
					post.setCaption(string);
					post.setPhoto(new Photo(imageURL));
					post.save();
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(MainActivity.this, getString(R.string.tumblr_share), Toast.LENGTH_SHORT).show();
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
					return;
				}

			}
		}).start();
	}

	public void shareWeb(Bundle params)
	{
		if (!Session.getActiveSession().isOpened())
		{
			new LoginButton(this).performClick();
			_pendingShare = params;
			return;
		}

		WebDialog feedDialog = new WebDialog.FeedDialogBuilder(this, Session.getActiveSession(), params).setOnCompleteListener(new OnCompleteListener()
		{
			@Override
			public void onComplete(Bundle values, FacebookException error)
			{
				_pendingShare = null;
				if (error == null)
				{
					// When the story is posted, echo the success
					// and the post Id.
					final String postId = values.getString(Consts.POST_ID);
					if (postId != null)
					{
						Log.i(getClass().getName(), "Posted story, id: " + postId);
					}
					else
					{
						Log.i(getClass().getName(), getString(R.string.publish_cancelled));
					}
				}
				else if (error instanceof FacebookOperationCanceledException)
				{
					Log.i(getClass().getName(), getString(R.string.publish_cancelled));
				}
				else
				{
					Toast.makeText(MainActivity.this, getString(R.string.error_posting_Story), Toast.LENGTH_SHORT).show();
				}
			}

		}).build();
		feedDialog.show();
	}
	
	@Override
	public void detailsArrived(UserDetails details)
	{
		tumblrShare(details.getUserName(), details.getUserID());
	}

	class MyTimerTask extends TimerTask
	{
		public void run()
		{
			runOnUiThread(new Runnable()
			{
				@SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
				public void run()
				{
					if (AppManager.getInstance().isLoggedIn())
					{
						try
						{
							String userId = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);
							
							if (userId == null || userId.isEmpty()) {
								Log.e(getClass().getName(), "get notifications, user logged in, but userId is null");
								return;
							}
							
							long lastFetchNotifications = AppManager.getInstance().getPrefrences(Consts.PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE, 0);
							RVSPromise promiss = AppManager.getInstance().getSDK()
									.notificationsCountForUser(userId, lastFetchNotifications);
							promiss.then(new AndroidDoneCallback<RVSFeedCountDetails>()
							{
								@Override
								public void onDone(final RVSFeedCountDetails result)
								{
									if (result != null)
									{
										int newNotifications = result.getCount();
										AppManager.getInstance().savePrefrences(Consts.PREF_NOTIFICATION_COUNT, "" + newNotifications);
										Log.i(getClass().getName(), "New Notifications = " + newNotifications);
										if (_navigationDrawerFragment != null) {
											_navigationDrawerFragment.setNotifications(newNotifications);
											updateActionBarLogoNotifications();
										}
										
									}
								}

								@Override
								public AndroidExecutionScope getExecutionScope()
								{
									return AndroidExecutionScope.UI;
								}
							});
						}
						catch (AssertionError e)
						{
							e.printStackTrace();
						}
					}
				}

				private long getLastFetchNotificationTime()
				{
					// TODO Auto-generated method stub
					return 0;
				}
			});
		}
	}

	@Override
	public void onWebViewClosed(boolean isSigning)
	{
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void onSignedUpStatusChanged(boolean isSignedIn, String errorMessage)
	{
	}
	
	@Override
	public void onSignedOut(String error) {
	    Log.i("MainActivity","onSignedOut, error: " + error);

    	if (RVSSdk.sharedRVSSdk().isSignedOut())
	    {
		    AppManager.getInstance().deleteUserDetails();
		    
		    MainActivity.this.resetProfileInformationUI();
		    setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_POPULAR);
//			MainActivity.this.navigateToPosition(NavigationType.TRENDING.ordinal());
			
			 // facebook
		    Session activeSession = Session.getActiveSession();
		    if (activeSession != null) {
		    	activeSession.closeAndClearTokenInformation();
		    }
		    
		    if (error != null && WhipClipApplication.getCurrentActivity() instanceof MainActivity)
//		    	(!isCurrentFragment(LoginManagerActivity.class.getSimpleName()) &&
//		    		!isCurrentFragment(LoginFragment.class.getSimpleName()) &&
//		    		!isCurrentFragment(SignUpFragment.class.getSimpleName())))// || (error.getErrorCode() == RVSErrorCodeSignInRequired)
		    {
		    	AppManager.getInstance().showLogin(MainActivity.this, REQUEST_CODE_LOGIN);
		    }
	    }
	}
	
	
	@Override
	public void onBackPressed() {
		
		if (!onBackPressedProcess()) {
			super.onBackPressed();
			useNavigation(true);
		}
	}
	
	private boolean onBackPressedProcess()
	{
		setCurrentFragment(null);
		
		dismissKeyboard();
		
	   	// We retrieve the fragment manager of the activity
	    FragmentManager frgmtManager = getSupportFragmentManager();
	    int tabsCount = 0;
	    for (Object obj : frgmtManager.getFragments()) {
	    	if (obj != null) tabsCount++;
	    }
	    
	    if (tabsCount > TabFragmentAdapter.TAB_POSITION_TOTAL ) { 
	    	frgmtManager.popBackStack();
	    	if (tabsCount-1 == TabFragmentAdapter.TAB_POSITION_TOTAL ) {
	    		useNavigation(true);
	    	}
	    	mHandler.postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					onSectionAttached(0);
				}
			}, 100);
	    	return true;
	    }
	    
	    // We retrieve the fragment container showed right now
	    // The viewpager assigns tags to fragment automatically like this
	    // mPager is our ViewPager instance
	    Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());

	    // And thanks to the fragment container, we retrieve its child fragment manager
	    // holding our fragment in the back stack
	    FragmentManager childFragmentManager = fragment.getChildFragmentManager();

	    // And here we go, if the back stack is empty, we let the back button doing its job
	    // Otherwise, we show the last entry in the back stack (our FragmentToShow) 
	    if(childFragmentManager.getBackStackEntryCount() > 0){
	    		childFragmentManager.popBackStack();
	    		mHandler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						onSectionAttached(0);
					}
				}, 100);
	    		
	    		return true;
	    }
	    
        return false;
	}

	public void popBackTabStack(int position) {
		
		dismissKeyboard();
		
	   	// We retrieve the fragment manager of the activity
	    FragmentManager frgmtManager = getSupportFragmentManager();
	    
	    // We retrieve the fragment container showed right now
	    // The viewpager assigns tags to fragment automatically like this
	    // mPager is our ViewPager instance
	    Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());
	    if (fragment == null) return;

	    // And thanks to the fragment container, we retrieve its child fragment manager
	    // holding our fragment in the back stack
	    FragmentManager childFragmentManager = fragment.getChildFragmentManager();

	    int popupAmount = childFragmentManager.getBackStackEntryCount();
	    
	    while (popupAmount > 0) {
	    	popupAmount--;
	    	try { // IllegalStateException
	    		childFragmentManager.popBackStack();
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		logger.error(e.getMessage());
	    	}
	    }
	}
	
	/**
	 * Replace main fragment, put the fragment over the tabviews 
	 * @param fragmentClass
	 * @param args
	 */
	public void replaceFragmentContent(BaseFragment fragment, Bundle args)
	{
		super.replaceFragmentContent(fragment, args);
		
		hideNavigation();
	}

	
	
	/**
	 * Replace main fragment, put the fragment over the tabviews 
	 * @param fragmentClass
	 * @param args
	 */
	public void replaceFragmentContent(Class<? extends BaseFragment> fragmentClass, Bundle args)
	{
		super.replaceFragmentContent(fragmentClass, args);
		
		hideNavigation();
	}

	/*
	 * Replace tab specific fragment
	 */
//	public void addChildFragment(Class<? extends BaseFragment> fragmentClass, Bundle args)
//	{
//		addChildFragment(mPager.getCurrentItem(), fragmentClass, args);
//		hideNavigation();
//	}
//	
//	public void addChildFragment(int tabPosition, Class<? extends BaseFragment> fragmentClass, Bundle args)
//	{
//		try
//		{
//			BaseFragment newFragment = fragmentClass.newInstance();
//			newFragment.setCurrentTabPosition(tabPosition);
//			newFragment.setArguments(args);
//			mAdapter.addChildFragment(tabPosition, newFragment);
//			hideNavigation();
//			_currentFragment = newFragment;
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			return;
//		}
//	}
	
	public Fragment getMainTabFragment(int position) {
	 	// We retrieve the fragment manager of the activity
	    
	    // We retrieve the fragment container showed right now
	    // The viewpager assigns tags to fragment automatically like this
	    // mPager is our ViewPager instance
		FragmentManager fragMan = getSupportFragmentManager();
	    return fragMan.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + position);
	}

	public int getTabBackStackEntryCount()
	{

	   	// We retrieve the fragment manager of the activity
	    FragmentManager frgmtManager = getSupportFragmentManager();
	    
	    // We retrieve the fragment container showed right now
	    // The viewpager assigns tags to fragment automatically like this
	    // mPager is our ViewPager instance
	    Fragment fragment = frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());

	    // And thanks to the fragment container, we retrieve its child fragment manager
	    // holding our fragment in the back stack
	    FragmentManager childFragmentManager = fragment.getChildFragmentManager(); 

	    // And here we go, if the back stack is empty, we let the back button doing its job
	    // Otherwise, we show the last entry in the back stack (our FragmentToShow) 
	    return childFragmentManager.getBackStackEntryCount();
		
	}
	
	public Fragment getCurrentTabFragment() {
		// We retrieve the fragment manager of the activity
	    FragmentManager frgmtManager = getSupportFragmentManager();
	    
	    // We retrieve the fragment container showed right now
	    // The viewpager assigns tags to fragment automatically like this
	    // mPager is our ViewPager instance
	    return frgmtManager.findFragmentByTag("android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem());
	}

//	public void openMyUserTab()
//	{
//		setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_PROFILE);
//	}

	public boolean isMyTabOpened(int position)
	{
		Fragment fragment = getCurrentTabFragment();
		
		boolean isOpened = isFragmentTabOpened(fragment, position);
		return isOpened;
		

	}
	
	private boolean isFragmentTabOpened(Fragment frg, int position) {
		boolean isOpened = false;
		switch (position)
		{
			case TabFragmentAdapter.TAB_POSITION_HOME:
				if (frg instanceof FeedFragment) isOpened = true;
				break;
			case TabFragmentAdapter.TAB_POSITION_POPULAR:
				if (frg instanceof PostsFragment) isOpened = true;
				break;
			case TabFragmentAdapter.TAB_POSITION_TV_SHOWS:
				if (frg instanceof TVShowsTabFragment) isOpened = true;
				break;
//			case TabFragmentAdapter.TAB_POSITION_PROFILE:
//				if (frg instanceof UserProfileFragment) isOpened = true;
//				break;
			case TabFragmentAdapter.TAB_POSITION_NOTIFICATIONS:
				if (frg instanceof NotificationsFragment) isOpened = true;
				break;			
			case TabFragmentAdapter.TAB_POSITION_CLIP_TV:
//				if (frg instanceof PostsFragment) isOpened = true;
				break;				
			default:
				break;
		}
		
		return isOpened;
	}

	public void showShowDetailsPage(String showId, String channelId)
	{
		logger.debug("Prefrences show detail, show_id: " + showId + ", channel_id: " + channelId);
		
		Bundle bundle = new Bundle();
		bundle.putString(ShowPostsFragment.SHOW_ID, showId);
		bundle.putString(ShowPostsFragment.CHANNEL_ID, channelId);
		replaceFragmentContent(ShowPostsFragment.class, bundle);

//		replaceTabFragmentContent(tabPosition, ShowPostsFragment.class, bundle);
		
	}

	public void setLaunchIntetUri(String str)
	{
		mLaunchIntetUri = str;
		
	}

	public void showShowDetailsPageByLaunchIntent()
	{
		if (mLaunchIntetUri == null) return;
		
		Intent intent;
		try
		{
			intent = Intent.parseUri(mLaunchIntetUri, 0);
			if (intent.getAction() != null && intent.getAction().equals(WCNotificationManager.ACTION_NOTIF_SHOW_NOTIFY))
			{
				
//				9242879, us_own, 1418450400000
				Bundle extras = intent.getExtras();
				String showId = extras.getString(WCNotificationManager.EXTRA_SHOW_ID);
				String channelId = extras.getString(WCNotificationManager.EXTRA_CHANNEL_ID);
				String startTime = extras.getString(WCNotificationManager.EXTRA_START_TIME);
				
//				String programId = extras.getString(WCNotificationManager.EXTRA_PROGRAM_ID);
				
				
				WCNotificationManager notificationManager = AppManager.getInstance().getNotificationManager();
				notificationManager.cancelLocalNotificationWithProgram (showId, channelId, startTime);
				
				setLaunchIntetUri(null);
				showShowDetailsPage( showId, channelId);
			}
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		
	}

	public void setActionBarTitle(String title)
	{
		setActivityTitle(title);
		 setTitle();
		
	}

	public void onBackPressedFromCompose()
	{
		
		setCurrentFragment(null);
		onBackPressed();
		onSectionAttached(0);
		useNavigation(true);
		
	}

	@Override
	public void onNavigationLoggedIn(int currentNavigationPosition)
	{
//		if (currentNavigationPosition == NavigationType.SETTINGS.ordinal())
//		{
			setCurrentPageItem(TabFragmentAdapter.TAB_POSITION_HOME);
//		}
//		} else if (currentNavigationPosition == NavigationType.NOTIFICATIONS.ordinal())
//		{
//			.onNavigationDrawerItemSelected(NavigationType.NOTIFICATIONS.ordinal());
//		}

		
	}

//	public void openUserProfileAsChild(String userID, boolean fromUser){
//		openUserProfileAsChild(mPager.getCurrentItem(), userID, fromUser);
//	}

	public void setPagerSwipeEnabled(boolean enabled)
	{
		mPager.setSwipeEnabled(enabled);
		
	}
	
	
	public void openShareMoreOptionsDialog(boolean createdPost, final RVSPost postItem) {
		
	
//		if (shareMoreOptionsDialog == null) {
		final Context _context = MainActivity.this;
			final Dialog  dialog = new Dialog(_context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_layout);
			ListView dialogList = (ListView) dialog.findViewById(R.id.dialog_list);
	
//			final Properties envProperties = loadProperties("env.properties");				
			// 0 - production,
			// 1 - integration (default), defined in env.properties file
			List<String> list = new ArrayList<String>();
			if (createdPost) {
				list.add(_context.getResources().getString(R.string.also_share_to_shared_a_clip));
			} else {
				list.add(_context.getResources().getString(R.string.also_share_to));
			}
			list.add(_context.getResources().getString(R.string.facebook));
			list.add(_context.getResources().getString(R.string.twitter));
			list.add(_context.getResources().getString(R.string.dialog_done));

			ShareMoreOptionsAdapter adapter = new ShareMoreOptionsAdapter(_context, postItem, list, MainActivity.this);

			dialogList.setAdapter(adapter);
			dialogList.setOnItemClickListener(new OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					switch (position)
					{
						case 0: 
//							showDevEnvDialog();
							break;
							
						case 1: 
						case 2:
							shareOn(position-1, postItem);
//							showOverrideIpDialog();
							break;
							
					}
					if (parent.getChildCount() == (position+1)) { // Done
//						shareMoreOptionsDialog.dismiss();
					}
					
					dialog.dismiss();
					
				}
			});
			
			dialog.setCanceledOnTouchOutside(true);
			dialog.setCancelable(true);
			dialog.show();
	}
	
	@SuppressWarnings("unchecked")
	private void shareOn(final int position, final RVSPost postItem)
	{
		final MainActivity activity = MainActivity.this;
		activity.showProgressDialog();

		RVSPromise sharePromise = AppManager.getInstance().getSDK().sharingUrlsForPost(postItem.getPostId());
		sharePromise.then(new DoneCallback<RVSSharingUrls>()
		{
			@Override
			public void onDone(final RVSSharingUrls result)
			{
				activity.dismissProgressDialog();
				if (result != null && result.getUrl() != null)
				{
					//						_adapter.shareImage(result.getUrl());
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							switch (position)
							{
								case 0: // facebook
								{
									try
									{
										AnalyticsManager.sharedManager().trackPostShareWithPost(postItem, 
												AnalyticsManager.RVSAppAnalyticsManagerShareTypeFacebook);

										if (FacebookDialog.canPresentShareDialog(MainActivity.this, FacebookDialog.ShareDialogFeature.SHARE_DIALOG))
										{
											//												FacebookDialog shareDialog1 = new FacebookDialog.ShareDialogBuilder((Activity) _context);
											//												shareDialog1.g
											//												FacebookLogic.shareOnFacebook(result, _postItem, false);
											FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(MainActivity.this).setLink(
													result.getUrl() + "").build();
											MainActivity.this.showFacebookDialog(shareDialog.present());
										}
										else
										{
											useFallBackWebDialog(result);
										}

									}
									catch (Exception e)
									{
										e.printStackTrace();
									}
									break;
								}
								case 1: // twitter
								{
									AnalyticsManager.sharedManager().trackPostShareWithPost(postItem, 
											AnalyticsManager.RVSAppAnalyticsManagerShareTypeTwitter);

									AppManager.getInstance().getTwitterLogic().postTwit(postItem.getText(), result.getUrl(), null);
									break;
								}
							}
						}
					}); // end of runonUithread

				}
				else
				{
					WhipClipApplication.getCurrentActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							Toast.makeText(WhipClipApplication.getCurrentActivity(), MainActivity.this.getString(R.string.error_compose), Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}, new FailCallback<Object>()
		{
			@Override
			public void onFail(Object result)
			{
				activity.dismissProgressDialog();
			}
		});
	}
	
	public void useFallBackWebDialog(final RVSSharingUrls result)
	{
		Bundle params = new Bundle();
		params.putString(Consts.LINK, result.getUrl() + "");

		shareWeb(params);

	}

//	// workaround - postpone remove child (drill down view)
//	public void popBackStack( final FragmentManager fragmentManager) {
//		
//		final Handler handler = mHandler;
//		 handler.postDelayed(new Runnable() {
//			
//			@Override
//			public void run()
//			{
//				 int count = fragmentManager.getBackStackEntryCount();
//				 int temp = count;
//				 while (count > 0) {
//					 fragmentManager.popBackStack();
//					 count --;
//				 }
//				 
//					 handler.postDelayed(new Runnable()
//						{
//							@Override
//							public void run()
//							{
//								showNavigation();
//								updateActionTitle();
//							}
//						}, 100);
//				
//			}
//			 
//		 }, 100);
//		 
//		 
//			
//		
//	}
}
