package com.whipclip.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;

import com.facebook.UiLifecycleHelper;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.fragments.BaseFragment;

public class SignUpActivity extends BaseActivity
{
	private final String		TAG_NAME	= getClass().getSimpleName();
	private UiLifecycleHelper	_uiLifeCycleHlpr;
	private NuxFragment	mNuxFragment;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.e(TAG_NAME, "onCreate! ");
		super.onCreate(savedInstanceState);
		WhipClipApplication.setCurrentActivity(this);
		
		
		getActionBar().hide();
		
		
		setContentView(R.layout.sign_up_activity);
		
		boolean isFirstTime = false;
		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			isFirstTime = intent.getExtras().getBoolean("first_time");
		}
		
		 mNuxFragment = new NuxFragment(isFirstTime);
		 
		getSupportFragmentManager().beginTransaction()
//		 .replace(R.id.frame1, new LoginFragment())
		
		 .replace(R.id.frame1,mNuxFragment)
		 .addToBackStack(null)
		 .commit(); 

		
////		showMainLogin(savedInstanceState);
//		replaceFragmentContent(LoginActivity.class, false, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit,
//				R.anim.slide_out_right, null);
	}

	@Override
	public void initViews()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stubq
		
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {
	    
		super.onActivityResult(requestCode, resultCode, data);
		if (_uiLifeCycleHlpr != null)  _uiLifeCycleHlpr.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		if (_uiLifeCycleHlpr != null) _uiLifeCycleHlpr.onResume();
	}

	@Override
	public void onPause()
	{
		if (_uiLifeCycleHlpr != null)  _uiLifeCycleHlpr.onPause();
		super.onPause();
	}
	
	public void setUiLifeCycleHlpr(UiLifecycleHelper uiLifeCycleHlpr){
		_uiLifeCycleHlpr = uiLifeCycleHlpr;
	}
	
	
	@Override
	public void onBackPressed() {
	    // if there is a fragment and the back stack of this fragment is not empty,
	    // then emulate 'onBackPressed' behaviour, because in default, it is not working
	    FragmentManager fm = getSupportFragmentManager();
	    if (fm.getBackStackEntryCount() > 1) { // first is NUX
	    	fm.popBackStack();
	    	return;
	    }
	    
	    for (Fragment frag : fm.getFragments()) {
	        if (frag != null && frag.isVisible()) {
	            FragmentManager childFm = frag.getChildFragmentManager();
	            if (childFm.getBackStackEntryCount() > 0) {
	                childFm.popBackStack();
	                return;
	            } else {
	            	if (frag instanceof BaseFragment) {
	            		if (((BaseFragment)frag).onBackPressed()) {
	            			return; 
	            		}
	            	}
	            }
	        }
	    }
	    super.onBackPressed();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		mNuxFragment = null;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	            // app icon in action bar clicked; goto parent activity.
	        	dismissKeyboard();
	            onBackPressed();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
