package com.whipclip.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.TabPageIndicator;
import com.whipclip.R;
import com.whipclip.fragments.NavigationDrawerFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.ui.adapters.TabFragmentAdapter;

public class MainActivityTest extends BaseActivity  implements NavigationDrawerFragment.NavigationDrawerCallbacks
{

	ViewPager mPager;
	TabPageIndicator mIndicator;
    int Number = 0;
	private FragmentPagerAdapter	mAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout1);
		
		mAdapter = new TabFragmentAdapter(getSupportFragmentManager());

		mPager = (ViewPager)findViewById(R.id.pager);
	    mPager.setAdapter(mAdapter);
	
	    mIndicator = (TabPageIndicator)findViewById(R.id.indicator);
	    mIndicator.setTabIconLocation (TabPageIndicator.LOCATION_UP);
	    mIndicator.setTypeFace(AppManager.getInstance().getTypeFaceLight());
	    mIndicator.setViewPager(mPager);
	}


	@Override
	public void initViews()
	{
		// TODO Auto-generated method stub
		
	}


	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNavigationDrawerItemSelected(int position)
	{
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNavigationLoggedIn(int currentNavigationPosition)
	{
		// TODO Auto-generated method stub
		
	}
}
