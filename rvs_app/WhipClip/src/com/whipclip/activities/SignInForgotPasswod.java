package com.whipclip.activities;

import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidFailCallback;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.whipclip.R;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.ui.DialogUtils;

public class SignInForgotPasswod extends BaseFragment<Object>
{
	private final String		TAG_NAME	= getClass().getSimpleName();
	
	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		super.onCreateView(inflater, container, savedInstanceState);
		
		View view = inflater.inflate(R.layout.sing_in_forgot_password, container, false);
		final ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progress_bar);
		progressBar.setVisibility(View.GONE);
		
		final Button button = (Button)view.findViewById(R.id.reset_password);
		button.setBackgroundColor(getResources().getColor(R.color.btn_text_orange_semi_transparent));

		final EditText editText = (EditText)view.findViewById(R.id.email_edit_text);
		
		editText.addTextChangedListener(new TextWatcher(){
	        
			public void afterTextChanged(Editable s) {
				if (s.toString().length() == 0) {
					button.setEnabled(false);
					button.setBackgroundColor(getResources().getColor(R.color.btn_text_orange_semi_transparent));
				}  else if (s.toString().length() > 1) {
					button.setEnabled(true);
					button.setBackgroundColor(getResources().getColor(R.color.btn_text_orange));
	        	}
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
	        public void onTextChanged(CharSequence s, int start, int before, int count){ }
	    }); 

		
		button.setOnClickListener(new View.OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				String email = editText.getText().toString();
			    if ((email == null || email.isEmpty()) && !android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText()).matches())
			    {
			    	showErrorDialog(getString(R.string.sign_up_error_message_invalid_email));
			    	return;
			    }
			    dismissKeyboard();
				showProgressBar(button, progressBar, true);
			    
			    RVSPromise promise = RVSSdk.sharedRVSSdk().resetPasswordWithUserEmail(email);
			    promise.then(new AndroidDoneCallback<Object>()
				{

					@Override
					public void onDone(Object result)
					{
						showProgressBar(button, progressBar, false);
						showSubmitCompleteDialog();
					}

					@Override
					public AndroidExecutionScope getExecutionScope()
					{
						return AndroidExecutionScope.UI;
					}
				}).fail(new AndroidFailCallback<Throwable>()
				{

					@Override
					public void onFail(Throwable error)
					{
						showProgressBar(button, progressBar, false);
						
						String message = "";
						 if (error != null && error instanceof RVSError) {
		                        String errorMessage = ((RVSError) error).getErrorCode();
		                        if (errorMessage.equals("UNKNOWN_EMAIL_ADDRESS")) {
		        					message = getString(R.string.reset_password_unknown_email_message);
		        				} else if (errorMessage.equals("INVALID_EMAIL")) {
		        					 message = getString(R.string.reset_password_invalid_email_message);
		        				} else {
		        					message = getString(R.string.reset_password_error_message);
		        				}
		                   } else {
		                	   message = getString(R.string.reset_password_error_message);
		                   }
					
		                   showErrorDialog(message);
					
					}

					@Override
					public AndroidExecutionScope getExecutionScope()
					{
						return AndroidExecutionScope.UI;
					}
				});
				
//				__weak __typeof(self)weakSelf = self;
//			    [self.resetPromise thenOnMain:^(id result) {
//			        [weakSelf handleSubmitCompleted];
//			    } error:^(NSError *error) {
//			        [weakSelf handleSubmitError:error];
//			    }];
			}
		});
		
		
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setTitle(R.string.login_reset_password);
	
		return view;
	}
	

	private void showErrorDialog(String message) {
		
		final Dialog dialog = DialogUtils.createSignleButtonAlertDialog(
				getActivity(),
				getString(R.string.reset_password),
				message,
				getString(R.string.ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	
	private void showSubmitCompleteDialog() {
		
		final Dialog dialog = DialogUtils.createSignleButtonAlertDialog(
				getActivity(),
				getString(R.string.reset_password),
				getString(R.string.reset_password_completed_title),
				getString(R.string.ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dismissKeyboard();
						dialog.dismiss();
						SignInForgotPasswod.this.getActivity().onBackPressed();
//						SignInForgotPasswod.this.getActivity().finish();
					}
				});
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onDone(Object result)
	{
	}


	@Override
	public void onFail(Throwable result)
	{
	}


	@Override
	public void initViews(View FfragmentView)
	{
	}

	@Override
	public void initListeners()
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onBackPressed() {
		return super.onBackPressed();
	}
	
	private void showProgressBar(Button resetButton, ProgressBar progressBar,  boolean enable) {
		resetButton.setBackgroundColor(enable ? getResources().getColor(R.color.btn_text_orange_semi_transparent) : getResources().getColor(R.color.btn_text_orange));
		resetButton.setEnabled(!enable);
		
		progressBar.setVisibility(enable ? View.VISIBLE : View.GONE);
	}
}
