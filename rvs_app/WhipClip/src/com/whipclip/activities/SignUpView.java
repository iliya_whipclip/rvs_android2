package com.whipclip.activities;

import java.util.List;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.widget.LoginButton;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.NuxFragment.OnFacebookStatusCallback;
import com.whipclip.fragments.SignUpEmailFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;
import com.whipclip.logic.SignUpBroadcastReceiver;
import com.whipclip.logic.SignUpBroadcastReceiver.SingUpStatusChanged;
import com.whipclip.logic.UserDetails;
import com.whipclip.logic.UserDetails.IOnDetailsArrived;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;

public class SignUpView extends RelativeLayout implements View.OnClickListener, StatusCallback, IOnDetailsArrived, SingUpStatusChanged, OnFacebookStatusCallback
{
	private final String		TAG_NAME	= getClass().getSimpleName();
	private Button				_signFacebookButton;
	private Button				_signTwitterButton;
	private Button				_signEmailButton;
	private Button				_logInButton;
	private Button				_cancelButton;

//	private UiLifecycleHelper	_uiLifeCycleHlpr;
	private LoginButton			_fbookButton;

	private ProgressBar			_loginProgressBar;
	private View				_fragmentView;
	private Activity			_activity;
	private RelativeLayout		_buttonsLayout;
	private UserDetails			mUserDetails;
	
	private SignUpBroadcastReceiver mSingUpBroadcastReceiver = new SignUpBroadcastReceiver(this);
	private boolean	mIsFirstTime;

	
	public SignUpView(Context context)
	{
		super(context);
	}

	public SignUpView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public SignUpView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public void init(View view, Activity activity, boolean isFirstTime)
	{
		_fragmentView = view;
		_activity = activity;
		mIsFirstTime = isFirstTime;
		// workaround - reset facebook login state
		AppManager.getInstance().deleteUserDetails();
		
		showMainLogin();
		_buttonsLayout.setVisibility(View.VISIBLE);
	}

	private void showMainLogin()
	{

		//		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		//		setContentView(R.layout.activity_login);

		initViews();
		initFacebook();

		_buttonsLayout.setVisibility(View.VISIBLE);
		if (AppManager.getInstance().isLoggedIn())
		{
		}
		else
		{
			if (Session.getActiveSession().isOpened())
			{
				new LoginButton(_activity).performClick();
			}
			_buttonsLayout.setVisibility(View.VISIBLE);
		}
		initListeners();

	}

	private void initViews()
	{
		_cancelButton = (Button) _fragmentView.findViewById(R.id.login_button_cancel);
		_cancelButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		_buttonsLayout  = (RelativeLayout)_fragmentView .findViewById(R.id.buttons_layout);
		_loginProgressBar  = (ProgressBar)_fragmentView .findViewById(R.id.login_progress_bar);
		
		_signFacebookButton = (Button) _fragmentView.findViewById(R.id.login_button_facebook);
		_signFacebookButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_signTwitterButton = (Button) _fragmentView.findViewById(R.id.login_button_twitter);
		_signTwitterButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_signEmailButton = (Button) _fragmentView.findViewById(R.id.login_button_email);
		_signEmailButton.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		_logInButton = (Button) _fragmentView.findViewById(R.id.login_button_log_in);
//		_logInButton.setPaintFlags(_logInButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

		Typeface typeFace = AppManager.getInstance().getTypeFaceRegular();
		_logInButton.setTypeface(typeFace);
		
		
		if (!mIsFirstTime) {
			

    		LinearLayout l = (LinearLayout)_fragmentView .findViewById(R.id.wrapper);
    		l.setGravity(Gravity.CENTER_VERTICAL);
		
	        		
			_fragmentView .findViewById(R.id.login_header_icon).setVisibility(View.VISIBLE);
			_fragmentView .findViewById(R.id.login_page_header).setVisibility(View.GONE);
			_fragmentView .findViewById(R.id.login_page_subtitle).setVisibility(View.GONE);
			_cancelButton.setText(R.string.cancel);
		}

	}

	public void initListeners()
	{
		_signFacebookButton.setOnClickListener(this);
		_signTwitterButton.setOnClickListener(this);
		_logInButton.setOnClickListener(this);
		_cancelButton.setOnClickListener(this);
		_signEmailButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		if (v.equals(_signFacebookButton))
		{
			_fbookButton.performClick();
		}
		else if (v.equals(_signTwitterButton))
		{
			if (AppManager.getInstance().isLoggedIn())
			{
				deleteUserDetails();
				_buttonsLayout.setVisibility(View.VISIBLE);
//				_signTwitterButton.setText(R.string.sign_in_twitter);
//				_signFacebookButton.setText(R.string.sign_in_facebook);
			}
			else
			{
				openTwitterLogin();
			}
		}
		else if (v.equals(_cancelButton))
		{
			_activity.finish();
		}
		else if (v.equals(_signEmailButton))
		{
			openSignUpFragment(SignUpEmailFragment.SING_UP_BY_EMAIL);
		}
		else if (v.equals(_logInButton))
		{
			openSignUpFragment(SignUpEmailFragment.SIGN_IN_EMAIL);
		}
	}
	
	
	public boolean onBackPressed() {
		
		if (AppManager.getInstance().isFirstTime()) { // didn't finish NUX
			_activity.setResult(Consts.ACTIVITY_RESULT_EXIT);
			_activity.finish();
		}
		
//		if (!mIsFirstTime) {
//			_activity.finish();
//		}
		
		return true;
		
	}

	private void openSignUpFragment(int type)
	{
		Bundle bundle = new Bundle();
		bundle.putInt("type",type);
		if (type == SignUpEmailFragment.SING_UP_UPDATE_USERNAME) {
			bundle.putString("userName", mUserDetails.getUserName()); 
		}
		if (_activity instanceof SignUpActivity)
		{
			LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(mSingUpBroadcastReceiver);
			((SignUpActivity) _activity).replaceFragmentContent(SignUpEmailFragment.class, false, R.anim.slide_in_right, R.anim.slide_out_right,
					R.anim.slide_in_right_exit, R.anim.slide_out_right_exit, bundle);
		}		
	}

	private void onSignedIn()
	{
//		Intent intent = new Intent(_activity, MainActivity.class);
//		startActivityForResult(intent, 1);
//		_activity.overridePendingTransition(R.anim.anim_alpha2, R.anim.anim_alpha);
		_activity.setResult(Consts.LOGIN_OK);
		_activity.finish();
	}

	private void openTwitterLogin()
	{
//		_loginProgressBar.setVisibility(View.VISIBLE);
		AppManager.getInstance().getTwitterLogic().connect(_activity, this);
//		try
//		{
//			Handler handler = new Handler();
//			handler.postDelayed(new Runnable()
//			{
//
//				@Override
//				public void run()
//				{
//					_loginProgressBar.setVisibility(View.GONE);
//				}
//			}, 5000);
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
	}


	//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  //
	//  ~~~~~~~~~~~~~~~~~~~~~~~  Facebook Methods  ~~~~~~~~~~~~~~~~~~~~~~~~  //
	//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  //

	protected void initFacebook()
	{
//		if (_uiLifeCycleHlpr == null) {
//			_uiLifeCycleHlpr = new UiLifecycleHelper(_activity, LoginPageView.this);
//			_uiLifeCycleHlpr.onCreate(savedInstanceState);
//		}
//	
//
//		if (_activity instanceof LoginManagerActivity)
//		{
//			((LoginManagerActivity) _activity).setUiLifeCycleHlpr(_uiLifeCycleHlpr);
//		}

		_fbookButton = (LoginButton) _fragmentView.findViewById(R.id.authButton);
		_fbookButton.setReadPermissions(Consts.FACEBOOK_PERMISSIONS_READ);
	}

	/**
	 * Get active session from Facebook. If needed make a login to Facebook by
	 * SSO or by web view (depends whether Facbook app is installed or not ).
	 */
	protected void doLogin()
	{
		Session session = Session.getActiveSession();

		// If not logged in or if not in a middle of a login process - do log in:
		if ((session == null) || (session.getState() == SessionState.CREATED) || (session.getState() == SessionState.CREATED_TOKEN_LOADED)
				|| (session.isClosed() == true))
		{
			Log.i(TAG_NAME, "FB session does not exist - doing login!");
			_fbookButton.performClick();//callOnClick();
		}
		Log.i(TAG_NAME, "FB session exists- Horray!");
	}

	@Override
	public void call(Session session, SessionState state, Exception exception)
	{
		if (session == null)
		{
			return;
		}

		List<String> permissions = session.getPermissions();

		Log.i(TAG_NAME, "call - state = " + state + " Original Permissions " + permissions.toString());

		// Login FAILED check:
		if (session.getState() == SessionState.CLOSED_LOGIN_FAILED && !(exception instanceof FacebookOperationCanceledException))
		{
			Toast.makeText(_activity, "Login Failed", Toast.LENGTH_SHORT).show();
		}

		// Session in OPEN state:
		else if (session.isOpened())
		{
			Log.i(TAG_NAME, "FB session isOpened");
			_buttonsLayout.setVisibility(View.GONE);
//			_signFacebookButton.setText(R.string.sign_out);
//			_signTwitterButton.setText(R.string.sign_out);
			// Get user's Facebook data:
			((BaseActivity) _activity).showProgressDialog();
			AppManager.getInstance().getFacebookLogic().getUserDetails(session, this, _activity);

		}
		// Session in CLOSE state:
		else if (session.getState() == SessionState.CLOSED)
		{
			Log.i(TAG_NAME, "facebook session is closed!");
			_buttonsLayout.setVisibility(View.VISIBLE);
//			_signFacebookButton.setText(R.string.sign_in_facebook);
//			_signTwitterButton.setText(R.string.sign_in_twitter);
			deleteUserDetails();
			
		}
	}

	private void deleteUserDetails()
	{
		AppManager.getInstance().deleteUserDetails();
		mUserDetails = null;
	}

	@Override
	public void detailsArrived(final UserDetails details)
	{
		if (details == null)
		{
			return;
		}
		
		mUserDetails = details;
		
		AppManager.getInstance().getFilesManager().save(Consts.USER_DETAILS, details);
		Log.i(TAG_NAME, "detailsArrived " + details.getUserName() + "  at: " + details.getAccessToken());
		_activity.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				_buttonsLayout.setVisibility(View.GONE);
//				_signFacebookButton.setText(R.string.sign_out);
//				_signTwitterButton.setText(R.string.sign_out);
				mSingUpBroadcastReceiver.registerReceiver();
//				LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(mSingUpBroadcastReceiver,
//						new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
//				LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(mSingUpBroadcastReceiver,
//						new IntentFilter(RVSServiceManager.RVSServiceSignInErrorNotification));
//				LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(mSingUpBroadcastReceiver,
//						new IntentFilter(RVSServiceManager.RVSServiceSignedInPendingNotification));
				
				if (details.getUserType() == UserDetails.USER_TYPE_FACEBOOK)
				{
					AppManager.getInstance().getSDK().signInWithFacebookToken(details.getAccessToken());
				}
				else
				{
					AppManager.getInstance().getSDK().signInWithTwitterToken(details.getAccessToken(), details.getTokenSecret());
				}
			}
		});
	}


	@Override
	public void onWebViewClosed(boolean isSigning)
	{
		if (isSigning) {
			_loginProgressBar.setVisibility(View.VISIBLE);
			_buttonsLayout.setVisibility(View.GONE);
		}
	}

	@Override
	public void onSignedUpStatusChanged(boolean isSignedIn, String errorMessage)
	{
		((BaseActivity)_activity).dismissProgressDialog();
		
		if (isSignedIn) {
			LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(mSingUpBroadcastReceiver);
			
			RVSSdk sdk = AppManager.getInstance().getSDK();
			String myUserId = sdk.getMyUserId();
			AppManager.getInstance().savePrefrences(Consts.PREF_SDK_USERID, myUserId);

			Log.i(TAG_NAME, "signed in");

			if (sdk.isNewExternalUser() || sdk.isEmailEmpty() || sdk.isHandleRequired())
	        {
				openSignUpFragment(SignUpEmailFragment.SING_UP_UPDATE_USERNAME);
	        } else {
			
				RVSPromise<RVSUser, Error, Integer> prom = sdk.userForUserId(myUserId);
				prom.then(new DoneCallback<RVSUser>()
				{
					@Override
					public void onDone(final RVSUser result)
					{
						if (result != null)
						{

							UserDetails userDetails = AppManager.getInstance().getUserDetails();
							userDetails.setHandle(result.getHandle());
							AppManager.getInstance().getFilesManager().save(Consts.USER_DETAILS, userDetails);
							
							AppManager.getInstance().saveSDKUser(result);
							String path = AppManager.getInstance().getSDKUser().getPofileImage()
									.getImageUrlForSize(new Point(40, 40));
							AppManager.getInstance().savePrefrences(Consts.PREF_SDK_USER_IMAGE, path);
						}
						onSignedIn();
					}
				}, new FailCallback<Error>()
				{
					@Override
					public void onFail(Error result)
					{
						_activity.finish();
					}
				});
	        }
		} else {
			
			deleteUserDetails();
			Toast.makeText(SignUpView.this._activity, "Login Failed", Toast.LENGTH_LONG).show();
			onSignedIn();
		}
	}

	@Override
	public void onSignedOut(String error)
	{
		
	}

	@Override
	public void onCall(Session session, SessionState state, Exception exception)
	{
		if (session == null)
		{
			return;
		}

		List<String> permissions = session.getPermissions();

		Log.i(TAG_NAME, "call - state = " + state + " Original Permissions " + permissions.toString());

		// Login FAILED check:
		if (session.getState() == SessionState.CLOSED_LOGIN_FAILED && !(exception instanceof FacebookOperationCanceledException))
		{
			Toast.makeText(SignUpView.this._activity, "Login Failed", Toast.LENGTH_SHORT).show();
		}

		// Session in OPEN state:
		else if (session.isOpened())
		{
			Log.i(TAG_NAME, "FB session isOpened");
			
			
			_buttonsLayout.setVisibility(View.GONE);
//			_signFacebookButton.setText(R.string.sign_out);
//			_signTwitterButton.setText(R.string.sign_out);
			// Get user's Facebook data:
			((BaseActivity) _activity).showProgressDialog();
			
			AppManager.getInstance().getFacebookLogic().getUserDetails(session, this, SignUpView.this._activity);

		}
		// Session in CLOSE state:
		else if (session.getState() == SessionState.CLOSED)
		{
			Log.i(TAG_NAME, "facebook session is closed!");
			_buttonsLayout.setVisibility(View.VISIBLE);
//			_signFacebookButton.setText(R.string.sign_in_facebook);
//			_signTwitterButton.setText(R.string.sign_in_twitter);
			deleteUserDetails();
			
		}
	}

	public void onDestroy()
	{
		_fragmentView = null;
		_activity = null;
	}
}
