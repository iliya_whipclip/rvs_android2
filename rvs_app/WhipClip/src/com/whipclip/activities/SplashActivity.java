package com.whipclip.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.AppManager;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;

public class SplashActivity extends Activity
{
	private final int	END_SPLASH	= 1;
	private ImageView	_splashImage;
	private TextView	_splashText;
	private TextView	_signinText;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		
		// TODO enable only in debug
		Crashlytics.start(this);
		
		setContentView(R.layout.splash_layout);
		_splashImage = (ImageView) findViewById(R.id.splash_image);
		_splashText = (TextView) findViewById(R.id.splash_text);
		_signinText = (TextView) findViewById(R.id.splash_sign_in);

		_splashText.setTypeface(AppManager.getInstance().getTypeFaceRegular());
		_signinText.setTypeface(AppManager.getInstance().getTypeFaceRegular());

		boolean isLoggenIn = !AppManager.getInstance().getSDK().isSignedOut();
		if (isLoggenIn)
		{
			LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(_loginMessageReceiver,
					new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
		}

		_handler.postAtTime(endSplashRunnable, this, SystemClock.uptimeMillis() + (isLoggenIn ? 5000 : 1500));

		animateImage();

	}
	
	@Override
	protected void onDestroy() {
		
		super.onDestroy();
		
		if (_loginMessageReceiver != null) {
			LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(_loginMessageReceiver);
		}

	}

	private void animateImage()
	{
		_splashImage.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		ObjectAnimator alpha = ObjectAnimator.ofFloat(_splashImage, View.ALPHA, 0F, 1F);
		ObjectAnimator scaleX = ObjectAnimator.ofFloat(_splashImage, View.SCALE_X, 0F, 1F);
		ObjectAnimator scaleY = ObjectAnimator.ofFloat(_splashImage, View.SCALE_Y, 0F, 1F);

		AnimatorSet set = new AnimatorSet();
		set.playTogether(alpha, scaleX, scaleY);
		set.setInterpolator(new BounceInterpolator());
		set.setDuration(1000);
		set.start();

		_splashText.setAlpha(0F);
		ObjectAnimator textAlpha = ObjectAnimator.ofFloat(_splashText, View.ALPHA, 0F, 1F);
		textAlpha.setStartDelay(800);
		textAlpha.start();
	}

	Runnable	endSplashRunnable	= new Runnable()
									{
										@Override
										public void run()
										{
											openNextActivity();
										}

									};

	@SuppressWarnings("unused")
	private void openNextActivity()
	{
		if (false)//AppManager.getInstance().isFirstTime(getClass().getSimpleName())) // Changedbecuase of issue AN-202
		{
			AppManager.getInstance().showLogin(SplashActivity.this, MainActivity.REQUEST_CODE_NONE);
		}
		else
		{
			
//			Intent intent = new Intent(SplashActivity.this, MainActivityTest.class);
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivityForResult(intent, END_SPLASH);
		}
		overridePendingTransition(R.anim.anim_alpha2, R.anim.anim_alpha);
		finish();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		onBackPressed();
	}

	public void onBackPressed()
	{
		_handler.removeCallbacks(endSplashRunnable);
		super.onBackPressed();
	}

	protected void onPause()
	{
		LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(_loginMessageReceiver);
		super.onPause();
	}

	Handler						_handler				= new Handler()
														{
															public void handleMessage(Message msg)
															{
																if (msg.what == END_SPLASH)
																{

																}
															};
														};

	private BroadcastReceiver	_loginMessageReceiver	= new BroadcastReceiver()
														{
															@SuppressWarnings("unchecked")
															@Override
															public void onReceive(Context context, Intent intent)
															{
																// Get extra data included in the Intent
																String action = intent.getAction();
																Log.i("Login Activity", "Got message: " + action);
																_handler.removeCallbacksAndMessages(SplashActivity.this);
																if (action.equals(RVSServiceManager.RVSServiceSignInErrorNotification))
																{
																	Log.e("Login Activity", "Got message: NOT OK");
																	openNextActivity();
																}
																else if (action.equals(RVSServiceManager.RVSServiceSignedInNotification))
																{
																	Log.i("Login Activity", "Got message: OK");
																	openNextActivity();
																}
															}
														};
}
