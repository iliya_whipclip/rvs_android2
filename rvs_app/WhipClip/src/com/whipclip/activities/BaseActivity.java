package com.whipclip.activities;

//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.fragments.BaseFragment;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.Consts;

public abstract class BaseActivity extends FragmentActivity
{
	protected BaseFragment	_currentFragment;
	private ProgressDialog	_progressDialog;

	public abstract void initViews();

	public abstract void initListeners();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	
	/**
	 * Replace main fragment, put the fragment over the tabviews 
	 * @param fragmentClass
	 * @param args
	 */
	public void replaceFragmentContent(BaseFragment fragment, Bundle args)
	{
		replaceFragmentContent(fragment, true, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit, R.anim.slide_out_right_exit,
				args);
	}
	
	
	/**
	 * Replace main fragment, put the fragment over the tabviews 
	 * @param fragmentClass
	 * @param args
	 */
	public void replaceFragmentContent(Class<? extends BaseFragment> fragmentClass, Bundle args)
	{
		replaceFragmentContent(fragmentClass, true, R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_right_exit, R.anim.slide_out_right_exit,
				args);
	}
	
	
	public void replaceFragmentContent(BaseFragment fragment, boolean fromHistory, 
			int enterAnim, int exitAnim, int backEnterAnim, int backExitAnim, Bundle args)
	{
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		BaseFragment newFrag = null;
		try
		{
			fragment.setArguments(args);
//			if (_currentFragment != null && args != null && args.getBoolean(Consts.MENU_ITEM_SELECT, false))
//			{
//				getFragmentManager().popBackStack();
//			}
			_currentFragment = fragment;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
//
//		FragmentTransaction transaction = fragmentManager.beginTransaction();
//		
//		if (enterAnim != -1 && backEnterAnim != -1)
//		{
//			transaction.setCustomAnimations(enterAnim, exitAnim, backEnterAnim, backExitAnim);
//		}
//		transaction.replace(R.id.frame1, newFrag, fragmentClass.getSimpleName());
//		transaction.addToBackStack(fragmentClass.getSimpleName()).commitAllowingStateLoss();
		
		getSupportFragmentManager().beginTransaction()
		 .replace(R.id.frame1, fragment)
		 .addToBackStack(null)
		 .commit(); 
	}

	public void replaceFragmentContent(Class<? extends BaseFragment> fragmentClass, boolean fromHistory, 
			int enterAnim, int exitAnim, int backEnterAnim, int backExitAnim, Bundle args)
	{
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		BaseFragment newFrag = null;
		try
		{
			newFrag = fragmentClass.newInstance();
			newFrag.setArguments(args);
//			if (_currentFragment != null && args != null && args.getBoolean(Consts.MENU_ITEM_SELECT, false))
//			{
//				getFragmentManager().popBackStack();
//			}
			_currentFragment = newFrag;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
//
//		FragmentTransaction transaction = fragmentManager.beginTransaction();
//		
//		if (enterAnim != -1 && backEnterAnim != -1)
//		{
//			transaction.setCustomAnimations(enterAnim, exitAnim, backEnterAnim, backExitAnim);
//		}
//		transaction.replace(R.id.frame1, newFrag, fragmentClass.getSimpleName());
//		transaction.addToBackStack(fragmentClass.getSimpleName()).commitAllowingStateLoss();
		
		getSupportFragmentManager().beginTransaction()
		 .replace(R.id.frame1, newFrag)
		 .addToBackStack(null)
		 .commit(); 
	}

	public BaseFragment getTopBackstackFragment()
	{
		if (getSupportFragmentManager().getBackStackEntryCount() > 0)
		{
			String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
			return (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
		}
		return null;
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		super.onRestoreInstanceState(savedInstanceState);
	}

	public synchronized void showProgressDialog()
	{
		if (_progressDialog == null)
		{
			_progressDialog = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.please_wait));
			try
			{
				((TextView) _progressDialog.findViewById(android.R.id.message)).setTypeface(AppManager.getInstance().getTypeFaceRegular());
				((TextView) _progressDialog.findViewById(getResources().getIdentifier("alertTitle", "id", "android"))).setTypeface(AppManager.getInstance()
						.getTypeFaceRegular());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			// dismissProgressDialog();
		}
	}

	public synchronized void showProgressDialog(String title, String message)
	{
		if (_progressDialog != null)
		{
			dismissProgressDialog();
		}
		_progressDialog = ProgressDialog.show(this, title, message);
	}

	public synchronized void dismissProgressDialog()
	{
		if (_progressDialog != null)
		{
			_progressDialog.dismiss();
			_progressDialog = null;
		}
	}

	public BaseFragment getCurrentFragment()
	{
		return _currentFragment;
	}

	public void setCurrentFragment(BaseFragment frag)
	{
		_currentFragment = frag;
	}

	public void showKeyboard()
	{
		View vFocus = getCurrentFocus();
		if (vFocus == null)
		{
			return;
		}
		InputMethodManager imm = (InputMethodManager) WhipClipApplication._context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInputFromWindow(vFocus.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
	}

	public void dismissKeyboard()
	{
		View vFocus = getCurrentFocus();
		if (vFocus == null)
		{
			return;
		}
		InputMethodManager imm = (InputMethodManager) WhipClipApplication._context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(vFocus.getApplicationWindowToken(), 0);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		dismissKeyboard();
	}

	// Fixes all the cases when back is pressed and the keyboard stays up
	// (back is called also by the code, in "send" from compose for example)
	@Override
	public void onBackPressed()
	{
		
		dismissKeyboard();
		if (getSupportFragmentManager().getBackStackEntryCount() == 0)
		{
			super.onBackPressed();
		}
		else if (getSupportFragmentManager().getBackStackEntryCount() == 1)
		{
			super.onBackPressed();
			finish();
		}
		else if (getCurrentFragment() != null)// && getCurrentFragment().onBackPressed())
		{

		}
		else
		{
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			Fragment currentFrag = getSupportFragmentManager().findFragmentByTag(getTopBackstackFragment().getClass().getSimpleName());

			if (currentFrag != null)
			{
				transaction.remove(currentFrag);
			}
			transaction.commitAllowingStateLoss();
			try
			{
				getFragmentManager().popBackStackImmediate();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
	}

}