package com.whipclip.logic.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.Consts;

@SuppressLint("SimpleDateFormat")
public class DateUtils
{
	private static HashMap<Long, String>	_timestampMapping;
	private static boolean					is24HourFormat	= DateFormat.is24HourFormat(WhipClipApplication._context);
	private static SimpleDateFormat			_sdf;

	private static HashMap<Long, String> getMap()
	{
		if (_timestampMapping == null)
		{
			_timestampMapping = new HashMap<Long, String>();
		}
		return _timestampMapping;
	}

	/**
	 * Returns the String representing the timestamp of a Grid Mail Message
	 * 
	 * @param timestamp
	 *            in millis
	 * @return String according to Grid document
	 */
	public static String getPostTimeStamp(long receivedTimeMillisec, boolean showToday)
	{
		if (!showToday && getMap().containsKey(receivedTimeMillisec))
		{
			return getMap().get(receivedTimeMillisec);
		}
		
		String timeStamp = null;
		if (_sdf == null)
		{
			_sdf = new SimpleDateFormat("", Locale.US);
		}

		Calendar current = Calendar.getInstance();

		Calendar received = Calendar.getInstance();
		received.setTimeInMillis(receivedTimeMillisec );
		if (isYesterday(received, current))
		{
			// received time is yesterday
			timeStamp = WhipClipApplication._context.getString(R.string.yesterday);
		}
		else // if (isToday(received, current))
		{
			if (showToday)
			{
				_sdf.applyPattern("h:mm a");
				timeStamp = _sdf.format(received.getTime());
				timeStamp = String.format("Today, %s", timeStamp);
			}
			else
			{
				// First let check if we are using 24 hour format
				if (is24HourFormat)
				{
					_sdf.applyPattern("HH:mm");
				}
				else
				{
					_sdf.applyPattern("h:mm a");
				}
				timeStamp = _sdf.format(received.getTime());
			}

		}
//		else if (received.after(firstDayOfThisWeek(current)))
//		{
//			// received time is in last week
//			_sdf.applyPattern("EEEE");
//			timeStamp = _sdf.format(received.getTime());
//		}
//		else if (received.before(firstDayOfThisWeek(current)))
//		{
//			// received time is older than last week
//			_sdf.applyPattern("MMMM dd");
//			timeStamp = _sdf.format(received.getTime());
//		}
		getMap().put(receivedTimeMillisec, timeStamp);
		return timeStamp;
	}

	public static String getFullTimeString(long receivedTime)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(receivedTime * 1000);

		String str = new SimpleDateFormat("MMM dd, yyyy").format(calendar.getTime());
		if (is24HourFormat)
		{
			str += " at " + new SimpleDateFormat("HH:mm").format(calendar.getTime());
		}
		else
		{
			str += " at " + new SimpleDateFormat("h:mm a").format(calendar.getTime());
		}

		return str;
	}

	private static boolean isYesterday(Calendar received, Calendar current)
	{
		received = (Calendar) received.clone();
		if (((current.get(Calendar.DAY_OF_MONTH) - received.get(Calendar.DAY_OF_MONTH)) == 1) && (current.get(Calendar.MONTH) == received.get(Calendar.MONTH))
				&& (current.get(Calendar.YEAR) == received.get(Calendar.YEAR)))
		{
			return true;
		}
		return false;
	}
	
	public static boolean isToday(long timeMillisec)
	{
		Calendar current = Calendar.getInstance();

		Calendar received = Calendar.getInstance();
		received.setTimeInMillis(timeMillisec);
		
		return isToday(received, current);
	}

	private static boolean isToday(Calendar received, Calendar current)
	{
		received = (Calendar) received.clone();
		if ((current.get(Calendar.DAY_OF_MONTH) == received.get(Calendar.DAY_OF_MONTH)) && (current.get(Calendar.MONTH) == received.get(Calendar.MONTH))
				&& (current.get(Calendar.YEAR) == received.get(Calendar.YEAR)))
		{
			return true;
		}
		return false;
	}

	private static Calendar firstDayOfThisWeek(Calendar calendar)
	{
		calendar = (Calendar) calendar.clone();
		// first day of this week
		calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);

		return calendar;
	}

	/**
	 * Returns the UTC (in seconds) according to GMT +00:00 time. (and not by
	 * the default time zone)
	 * 
	 * @param time
	 * @return
	 */
	public static long translateToGreenwichTime(Date time)
	{
		long retTime = time.getTime();
		int offset = TimeZone.getDefault().getOffset(retTime);
		retTime += offset;
		retTime /= 1000; // needed in order to comply with facebook format

		return retTime;
	}

	/**
	 * Returns the UTC (in seconds) according to GMT +00:00 time. (and not by
	 * the default time zone)
	 * 
	 * @param time
	 * @return
	 */
	public static long getFaceBookTime(String timeStr)
	{
		// cleaning the 'T' in the given timeStamp
		char[] chars = timeStr.toCharArray();
		chars[10] = ' ';
		String fixedDate = new String(chars, 0, 19);

		Timestamp time = Timestamp.valueOf(fixedDate);

		long retTime = time.getTime();
		int offset = TimeZone.getDefault().getOffset(retTime);
		retTime += offset;
		retTime /= 1000; // needed in order to comply with facebook format

		translateToGreenwichTime(time);

		return retTime;
	}

	public static String getMediumTime(long time)
	{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("hh:mm");

		String outputDate = sdf.format(time);

		return outputDate;

	}

	public static String getLocalizedTime(long time)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);

		return (new SimpleDateFormat("HH:mm:ss")).format(calendar.getTime());
	}

	public static String getShortTime(Date inputDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("h:mm a");

		String outputDate = sdf.format(inputDate.getTime());

		return outputDate;

	}

	public static String getDay(Date inputDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("EEEE");

		String outputDate = sdf.format(inputDate.getTime());

		return outputDate;

	}

	/**
	 * returns time text as follows:<br>
	 * - 1 - 59 Minutes ago <br>
	 * - 1 - 23 Hours ago <br>
	 * - Yesterday at hh:mm <br>
	 * - Sunday at hh:mm <br>
	 * 
	 * @param time
	 * @return
	 */
	public static String getStyledTime(long time)
	{
		if (getMap().containsKey(time))
		{
			return getMap().get(time);
		}
		String retVal = "";

		Date origDate = new Date(time);
		long origTimeStamp = origDate.getTime();
		long currentTimeStamp = (new Date()).getTime();
		long diffTime = (currentTimeStamp - origTimeStamp) / 1000;

		// 1 - 59 minutes:
		if (diffTime < Consts.TIME_CONSTS_HOUR)
		{
			if (diffTime <= Consts.TIME_CONSTS_MINUTE)
			{
				retVal = String.format(Consts.TIME_PATTERN_MINUTE, 1);
			}
			else
			{
				retVal = String.format(Consts.TIME_PATTERN_MINUTES, diffTime / Consts.TIME_CONSTS_MINUTE);
			}
		}
		// 1 - 23 hours:
		else if (diffTime < Consts.TIME_CONSTS_DAY)
		{
			// if (diffTime <= Consts.TIME_CONSTS_HOUR)
			if ((diffTime / Consts.TIME_CONSTS_HOUR) == 1)
			{
				retVal = String.format(Consts.TIME_PATTERN_HOUR, 1);
			}
			else
			{
				retVal = String.format(Consts.TIME_PATTERN_HOURS, diffTime / Consts.TIME_CONSTS_HOUR);
			}
		}
		// 24 - 47 hours:
		else if (diffTime < Consts.TIME_CONSTS_YESTERDAY)
		{
			retVal = "1d";//String.format(Consts.TIME_PATTERN_YESTERDAY, DateUtils.getShortTime(origDate));
		}
		// Above 48 hours:
		else
		{
			int days = (int) ((diffTime / (60 * 60 * 24)) % 7);
			retVal = days + "d";
			//			retVal = String.format(Consts.TIME_PATTERN_DAY, DateUtils.getDay(origDate), DateUtils.getShortTime(origDate));
		}

		_timestampMapping.put(time, retVal);

		return retVal;
	}

	private static String getStyledTimeAux(long time, boolean fullFormat)
	{

		if (getMap().containsKey(time))
		{
			return getMap().get(time);
		}
		String retVal = "";

		java.text.DateFormat dateFormat;

		final String format = Settings.System.getString(WhipClipApplication._context.getContentResolver(), Settings.System.DATE_FORMAT);

		if (TextUtils.isEmpty(format))
		{
			dateFormat = android.text.format.DateFormat.getMediumDateFormat(WhipClipApplication._context);
		}
		else
		{
			dateFormat = new SimpleDateFormat(format);
		}

		Date origDate = new Date(time);
		Date currentDate = new Date();

		int diff = getDateDiff(currentDate, origDate, Calendar.YEAR);
		if (diff > 0)
		{
			retVal += dateFormat.format(origDate);
		}

		// no month display needed in notification

		//		diff = getDateDiff(currentDate, origDate, Calendar.MONTH);
		//		if (("".equals(retVal)) && diff > 0)
		//		{
		//			retVal += fullFormat ? formatDiff(diff, fullFormat, "month") : formatDiff(diff, fullFormat, "m");
		//		}

		// check if num of weeks is smaller then 1 year , 52 weeks.
		diff = getDateDiff(currentDate, origDate, Calendar.WEEK_OF_YEAR);
		if (("".equals(retVal)) && diff > 0 && diff <= 52)
		{
			retVal += fullFormat ? formatDiff(diff, fullFormat, "week") : formatDiff(diff, fullFormat, "w");
		}
		diff = getDateDiff(currentDate, origDate, Calendar.DATE);
		if (("".equals(retVal)) && diff > 0)
		{
			retVal += fullFormat ? formatDiff(diff, fullFormat, "day") : formatDiff(diff, fullFormat, "d");
		}

		long timeDifference = (currentDate.getTime() - time);
		long diffHours = timeDifference / (60 * 60 * 1000);
		if (("".equals(retVal)) && diffHours > 0)
		{
			retVal += fullFormat ? formatDiff((int) diffHours, fullFormat, "hour") : formatDiff((int) diffHours, fullFormat, "h");
		}
		long diffMinutes = timeDifference / (60 * 1000);
		if (("".equals(retVal)) && diffMinutes > 0)
		{
			retVal += fullFormat ? formatDiff((int) diffMinutes, fullFormat, "minute") : formatDiff((int) diffMinutes, fullFormat, "m");
		}

		if (("".equals(retVal)))
		{
			if (fullFormat)
			{
				retVal = "just now";
			}
			else
			{
				long timeDiff = timeDifference / 1000;
				if (timeDiff >= 30 * 1000)
				{
					retVal = timeDiff + "s";
				}
				else
				{
					retVal = "now";
				}
			}
		}

		return retVal;
	}
	
	/**
	 * 
	 * @param time as long to parse.
	 * @param fullFormat - in case there's need for a full format representation : "1 day ago" or not : "1d".
	 * @param aired - in case there's need for "Aired " in the beginning of the time.
	 * @return formatted string
	 * basically, when fullFormat = false, aired doesn't matter, since in short format, aired initial isn't needed. 
	 */
	public static String getStyledTimeFullAgoFormat(long time, boolean fullFormat, boolean appendAgo)
	{
		String retVal = getStyledTimeAux(time, fullFormat);
		return retVal + ((appendAgo && "just now".equals(retVal) == false) ?
				" " + WhipClipApplication._context.getResources().getString(R.string.time_ago) :
				"");
	}

	/**
	 * 
	 * @param time as long to parse.
	 * @param fullFormat - in case there's need for a full format representation : "1 day ago" or not : "1d".
	 * @param aired - in case there's need for "Aired " in the beginning of the time.
	 * @return formatted string
	 * basically, when fullFormat = false, aired doesn't matter, since in short format, aired initial isn't needed. 
	 */
	public static String getStyledTimeFull(long time, boolean fullFormat, boolean aired)
	{
		String retVal = getStyledTimeAux(time, fullFormat);
		if (fullFormat && aired && retVal.equals("just now") == false)
		{
			retVal = "Aired " + retVal;
		}
		return retVal + ((fullFormat && "just now".equals(retVal) == false) ?
				" " + WhipClipApplication._context.getResources().getString(R.string.time_ago) :
				"");
	}

	private static String formatDiff(int diff, boolean fullFormat, String single)
	{
		if (diff > 0)
		{
			if (fullFormat)
			{
				return diff == 1 ? (diff + " " + single) : (diff + " " + single + "s");
			}
			else
			{
				return diff + single;
			}
		}

		return "";
	}

	// Use one of the constants from Calendar, e.g. DATE, WEEK_OF_YEAR,  
	//  or MONTH, as the calUnit variable.  Supply two Date objects.  
	//  This method returns the number of days, weeks, months, etc.  
	//  between the two dates.  In other words it returns the result of  
	//  subtracting two dates as the number of days, weeks, months, etc.  
	private static int getDateDiff(Date d1, Date d2, int calUnit)
	{
		if (d1.after(d2))
		{ // make sure d1 < d2, else swap them  
			Date temp = d1;
			d1 = d2;
			d2 = temp;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);
		for (int i = 1;; i++)
		{
			c1.add(calUnit, 1); // add one day, week, year, etc.  
			if (c1.after(c2)) return i - 1;
		}
	}

}
