package com.whipclip.logic.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.util.Log;

public class UnZipper// extends Observable
{
	private static final int	EOF					= -1;
	private static final int	DEFAULT_BUFFER_SIZE	= 1024 * 4;
	private static final String	TAG					= "UnZip";
	private String				_fileName;
	private String				_destinationPath;
	private IUnZipListener		_listener;

	public interface IUnZipListener
	{
		public void onUnzipSuccess(String fileName);

		public void onUnzipError(String err);

	}

	public UnZipper(IUnZipListener listener, String fileName, String destinationPath)
	{
		_fileName = fileName;
		_destinationPath = destinationPath;
		_listener = listener;
	}

	public String getFileName()
	{
		return _fileName;
	}

	public String getDe1981stinationPath()
	{
		return _destinationPath;
	}

	public void unzip()
	{
		Log.d(TAG, "unzipping " + _fileName + " to " + _destinationPath);
		UnZipTask unzipThread = new UnZipTask(_fileName, _destinationPath);
		unzipThread.start();
	}

	public void unzipSyncronized()
	{
		//boolean result = true;
		File archive = new File(_fileName);
		try
		{
			ZipFile zipfile = new ZipFile(archive);
			for (Enumeration e = zipfile.entries(); e.hasMoreElements();)
			{
				ZipEntry entry = (ZipEntry) e.nextElement();
				unzipEntry(zipfile, entry, _destinationPath);
			}

			archive.delete();
		}
		catch (Exception e)
		{
			Log.e(TAG, "Error while extracting file " + archive, e);
			//result = false;
		}
	}

	public static void createDir(File dir)
	{
		if (dir.exists())
		{
			return;
		}

		if (!dir.mkdirs())
		{
			//throw new RuntimeException("Can not create dir " + dir);
		}
	}

	public static void createDir(String path)
	{
		createDir(new File(path));
	}

	private class UnZipTask extends Thread
	{
		String	_filePath;
		String	_destinationPath1;

		public UnZipTask(String filePath, String destPath)
		{
			_filePath = filePath;
			_destinationPath1 = destPath;
		}

		@Override
		public void run()
		{
			boolean result = true;
			File archive = new File(_filePath);
			try
			{
				ZipFile zipfile = new ZipFile(archive);
				for (Enumeration e = zipfile.entries(); e.hasMoreElements();)
				{
					ZipEntry entry = (ZipEntry) e.nextElement();
					unzipEntry(zipfile, entry, _destinationPath1);
				}

				archive.delete();
			}
			catch (Exception e)
			{
				Log.e(TAG, "Error while extracting file " + archive, e);
				result = false;
			}

			if (_listener != null)
			{
				if (result == true)
				{
					_listener.onUnzipSuccess(_destinationPath1);
				}
				else
				{
					_listener.onUnzipError("ERROR");
				}
			}
			super.run();
		}
	}

	private static void unzipEntry(ZipFile zipfile, ZipEntry entry, String outputDir) throws IOException
	{
		if (entry.isDirectory())
		{
			createDir(new File(outputDir, entry.getName()));
			return;
		}

		File outputFile = new File(outputDir, entry.getName());
		if (!outputFile.getParentFile().exists())
		{
			createDir(outputFile.getParentFile());
		}

		Log.v(TAG, "Extracting: " + entry);
		BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

		try
		{
			byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
			long count = 0;
			int n = 0;
			while (EOF != (n = inputStream.read(buffer)))
			{
				outputStream.write(buffer, 0, n);
				count += n;
			}

			if (count > Integer.MAX_VALUE)
			{
				// return -1;
			}
		}
		finally
		{
			outputStream.close();
			inputStream.close();
		}
	}
}
