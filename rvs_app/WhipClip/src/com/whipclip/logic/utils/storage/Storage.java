package com.whipclip.logic.utils.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class Storage {

	private static final String TAG = "Storage";
	
	public final static String SHOWS_TABLE = "items";

	public final static String MULTI_VALUE_INDEX_TABLE = "mv_items";
	private final static String FOREIGN_KEY_COLUMN_NAME = "fk";
	private final static String NAME_COLUMN_NAME = "name";
	private final static String VALUE_COLUMN_NAME = "value";
    
    public static final String MD5_VALUE = "valueMd5";
    public static final String MD5_TABLE = "md5_hash";

    public static final String CORRECT_STATE = "mounted";
	private static final String	GUID_FIELD	= "guid";

//    private List<Field> fields = new ArrayList<Field>();

	private SQLiteDatabase db;

	public Storage(SQLiteDatabase db) {
		this.db = db;
	}


	private void dropTables() {
		 db.execSQL(String.format("drop table IF EXISTS %s;", SHOWS_TABLE));
        db.execSQL(String.format("drop table IF EXISTS %s;", MD5_TABLE));
//		db.execSQL(String.format("drop table IF EXISTS %s;", MULTI_VALUE_INDEX_TABLE));
	}

	private void createTables() {
		db.execSQL(String.format("create table %s (id integer primary key autoincrement, %s text, %s text, %s text);",
				MULTI_VALUE_INDEX_TABLE, FOREIGN_KEY_COLUMN_NAME, NAME_COLUMN_NAME, VALUE_COLUMN_NAME));
		String sql = String.format("create index if not exists %s_idx on %s (%s);", FOREIGN_KEY_COLUMN_NAME, MULTI_VALUE_INDEX_TABLE, FOREIGN_KEY_COLUMN_NAME);
		db.execSQL(sql);
		sql = String.format("create index if not exists %s_idx on %s (%s);", NAME_COLUMN_NAME, MULTI_VALUE_INDEX_TABLE,FOREIGN_KEY_COLUMN_NAME);
		db.execSQL(sql);
		sql = String.format("create index if not exists %s_idx on %s (%s);", VALUE_COLUMN_NAME, MULTI_VALUE_INDEX_TABLE, VALUE_COLUMN_NAME);
		db.execSQL(sql);
		//db.execSQL("drop table if exists watched;");
		db.execSQL("create table if not exists watched (watched_guid text);");
		db.execSQL("create unique index if not exists watched_idx on watched (watched_guid);");

        db.execSQL(String.format("create table %s (id integer primary key autoincrement, %s text);", MD5_TABLE, MD5_VALUE));
	}

	public void recreate() {
        
        if(!isTableExists(SHOWS_TABLE) ){
            dropTables();
            createTables();
            Log.d("VODS", SHOWS_TABLE + " DROP_CREATE");
        }
	}

	public void insert(String table, ContentValues values) {
        
		db.insert(table,null,values);
	}

	public void updateOrInsert(String table, ContentValues values) {
        
        String whereClause = String.format("%s=?", GUID_FIELD);
        String[] whereArgs = new String[]{(String) values.get(GUID_FIELD)};
        Log.d("Storage", String.format("updateOrInsert table: %s, whereClause:%s", table, whereClause));
        for(int i = 0; i < whereArgs.length; i++){
            Log.d("Storage", String.format("updateOrInsert whereArgs[%d] = %s", i, whereArgs[i]));
        }
        long res = db.update(table, values, whereClause, whereArgs);
        Log.d("Storage", String.format("updateOrInsert update res: %d", res));
        if(res <= 0){
            res = db.insert(table,null,values);
            Log.d("Storage", String.format("updateOrInsert insert res: %d", res));
        }
	}

//	public void setFields(List<Field> fields) {
//		this.fields = fields;
//	}

	public String getSql(int sortBy) {
	    String s = String.format("select a.id _id, "
					+" * from %s as a"
			        +" left outer join watched as b on a.guid = b.watched_guid"
			        ,SHOWS_TABLE);
		return s;
	}


    public Cursor getShows(String showId, String channelId) {

    	String sql = String.format("select a.id _id, * from %s as a left outer join watched as b on a.guid = b.watched_guid where a.%s = '%s'",SHOWS_TABLE,showId,channelId);
        return db.rawQuery(sql,null);
    }

    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
        db.endTransaction();

        Log.d("VODS_IS_GET_MD5", "TRANSACTION END");
    }

    public void beginTransaction() {
        db.beginTransactionNonExclusive();
    }

    public boolean isTableExists(String tableName) {

    	Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public String getPreviousVodsMd5Hash() {
        String md5Hash = null;
        try {
            Cursor cursor = db.query(MD5_TABLE, new String[]{MD5_VALUE},
                    null, null, null, null,
                    String.format("%s", MD5_VALUE));
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    md5Hash = cursor.getString(cursor.getColumnIndex(MD5_VALUE));
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException ignored) {};
        return md5Hash;
    }
}
