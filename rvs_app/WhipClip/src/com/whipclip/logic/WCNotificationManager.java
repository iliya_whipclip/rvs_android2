package com.whipclip.logic;

import java.util.HashMap;
import java.util.Map;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.MainActivity;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;

public class WCNotificationManager
{
	public static final String ACTION_NOTIF_SHOW_NOTIFY 		= "com.whipclip.show_notify_notification";
//	public static final String ACTION_NOTIF_CLIP_TV_NOTIFY		= "com.whipclip.clip_tv_notification";
	public static final String ACTION_USER_SYNC 				= "com.whipclip.user_sync";
	public static final String ACTION_USER_NOTIFICATIONS_SYNC 	= "com.whipclip.user_notifications_sync";


	
	public static final String EXTRA_SHOW_ID 		= "show_id";
	public static final String EXTRA_CHANNEL_ID 	= "channel_id";
	public static final String EXTRA_PROGRAM_ID 	= "program_id";
	public static final String EXTRA_START_TIME 	= "start_time";
	
	private int				PENDING_INTENT_ID	= 111;

	private PendingIntent	_pendingUserSyncPendingIntent;
	private AlarmManager	_notificationAlarmManager;
	
	private boolean isCancelEnabled = false;
	
	
	Map<String, PendingIntent> mPendingIntent = new HashMap<String, PendingIntent>();
	

	private Context	mContext;

	public WCNotificationManager() {
		mContext = WhipClipApplication._context;
		_notificationAlarmManager = (AlarmManager) WhipClipApplication._context.getSystemService(Context.ALARM_SERVICE);
		
		scheduleUserSyncUpdate();
	}
	
	public void scheduleLocalNotificationWithProgram(RVSProgram program, String showId, String showName, RVSChannel channel, String notificationAction)
	{
		if (program == null || channel == null) return; 
	    
		String startTime = String.valueOf(program.getStart());
		String notificationTag = getShowNotificationTag(showId, channel.getChannelId(), startTime);
		AppManager.getInstance().savePrefrences(notificationTag, "1");
		 
	    Notification notification = createShowNotifyNotification(program, showId, showName, channel.getChannelId(), notificationAction, startTime);
	    scheduleNotification(notificationTag, notification, program.getStartOffsetTime() + 60*1000); // add one minute
	}
	
	private Notification createShowNotifyNotification(RVSProgram program, String showId, String showName, String channelId, String notificationAction, String startTime) {
//		http://stackoverflow.com/questions/3009059/android-pending-intent-notification-problem
		Intent returnIntent = new Intent(mContext, MainActivity.class);
		returnIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		returnIntent.putExtra(EXTRA_SHOW_ID, showId);
		returnIntent.putExtra(EXTRA_CHANNEL_ID, channelId  != null ? channelId : "");
		returnIntent.putExtra(EXTRA_PROGRAM_ID, program != null ? program.getProgramId() : "");
		returnIntent.putExtra(EXTRA_START_TIME, startTime != null ? startTime : "");
		returnIntent.setAction(notificationAction);
		
		int requestID = (int) System.currentTimeMillis();
		PendingIntent notificationIntent = PendingIntent.getActivity(mContext, requestID, returnIntent, 0);

		Resources resources = WhipClipApplication._context.getResources();
	    if (showName == null || showName.isEmpty()) {
	        showName = resources.getString(R.string.default_your_show_name);
	    }
	    String alertBody = String.format(resources.getString(R.string.scheduled_program_notification_body_format), showName);

//		Notification notification = new Notification.Builder(mContext)
//				.setContentTitle(mContext.getResources().getString(R.string.app_name))
////				.setContentText(alertBody)
//				.setSmallIcon(R.drawable.ic_launcher)
//				.setStyle((Style)style)
//				.setContentIntent(notificationIntent).build();
		
		
		Notification notification = new NotificationCompat.Builder(mContext)
		.setContentTitle(mContext.getResources().getString(R.string.app_name))
		.setContentText(String.format(resources.getString(R.string.scheduled_program_notification_body_short_format), showName))
		.setSmallIcon(R.drawable.ic_launcher)
		.setStyle(new NotificationCompat.BigTextStyle().bigText(alertBody))
		.setContentIntent(notificationIntent).build();
		
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.priority = Notification.PRIORITY_MAX;
		return notification;
    }
	
	
	private void scheduleNotification(String notificationTag, Notification notification, long delayMillisec) {
		 
        Intent intent = new Intent(mContext, NotificationAlarmReciever.class);
        intent.putExtra(NotificationAlarmReciever.NOTIFICATION_TAG, notificationTag);
        intent.putExtra(NotificationAlarmReciever.NOTIFICATION, notification);
                
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        if (isCancelEnabled) {
        	mPendingIntent.put(notificationTag, pendingIntent);
        }
		
        long futureInMillis = SystemClock.elapsedRealtime() + delayMillisec;
        _notificationAlarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

	public void scheduleUserSyncUpdate()
	{
		if (AppManager.getInstance().isLoggedIn())
		{
			if (_pendingUserSyncPendingIntent == null) {
				Intent intentAlarm = new Intent(mContext, NotificationAlarmReciever.class);
				intentAlarm.setAction(WCNotificationManager.ACTION_USER_SYNC);
				_pendingUserSyncPendingIntent = PendingIntent.getBroadcast(mContext, PENDING_INTENT_ID, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
			}
			
			// sets alarm manager to repeat every 3 min and check for new notifications
			_notificationAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 0, 1000 * 60 * 3, _pendingUserSyncPendingIntent);
		}
	}

	public void cancelUserSyncUpdate()
	{
		_notificationAlarmManager.cancel(_pendingUserSyncPendingIntent);
	}

	public synchronized void cancelLocalNotificationWithProgram(String showId, String channelId, String startTime)
	{
		String showNotificationId = getShowNotificationTag(showId, channelId, startTime);
		AppManager.getInstance().removePrefrences(showNotificationId);

		if (isCancelEnabled && mPendingIntent.containsKey(showNotificationId)) {
			PendingIntent pendingIntent = mPendingIntent.get(showNotificationId);
			mPendingIntent.remove(showNotificationId);
			cancelAlarm(pendingIntent);
		}
	}
	
	public void cancelAlarm(PendingIntent sender) {
		
		_notificationAlarmManager.cancel(sender);
	}

//	public void cancelLocalNotificationWithProgram(String showId, String channelId)
//	{
//		String showNotificationId = getShowNotificationTag(showId, channelId);
//		AppManager.getInstance().removePrefrences(showNotificationId);
//		
//		// todo remove pending after delay
//		if (isCancelEnabled && mPendingIntent.containsKey(showNotificationId)) {
//			PendingIntent pendingIntent = mPendingIntent.get(showNotificationId);
//			mPendingIntent.remove(showNotificationId);
//			cancelAlarm(pendingIntent);
//		}
//	}

	
//	public String getShowNotificationTag(RVSShow show) {
//		return getShowNotificationTag(show.getShowId(), show.getChannel() != null ? show.getChannel().getChannelId() : "");
//	}
	
	public String getShowNotificationTag(String showId, String channelId, String startTime)
	{
		return Consts.PREF_SHOW_NOTIFY_PREIX + showId + "_" + channelId + "_" + startTime;
		
	}

}
