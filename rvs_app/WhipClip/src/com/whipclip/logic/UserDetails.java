package com.whipclip.logic;

import java.io.Serializable;

public class UserDetails implements Serializable
{
	private static final long	serialVersionUID	= -4297013418157094807L;
	public static final int		USER_TYPE_TWITTER	= 1;
	public static final int		USER_TYPE_FACEBOOK	= 2;
	public static final int		USER_TYPE_EMAIL	= 3;

	private String				_userID;
	private String				_userName;
	private String				_avatarRemoteUrl;
	private String				_avatarLocalPath;
	private String				_accessToken;
	private String				_tokenSecret;
	private String				_email;
	private int					_userType;
	private String	mHandle;

	public UserDetails(String userName, String userId, String email, String avatarUrl, int type)
	{
		_userID = userId;
		_userName = userName;
		_avatarRemoteUrl = avatarUrl;
		_userType = type;
	}

	public String getUserID()
	{
		return _userID;
	}

	public void setUserID(String userID)
	{
		this._userID = userID;
	}

	public String getUserName()
	{
		return _userName;
	}

	public void setUserName(String userName)
	{
		this._userName = userName;
	}

	public String getAvatarUrl()
	{
		return _avatarRemoteUrl;
	}

	public void setAvatarUrl(String avatarUrl)
	{
		this._avatarRemoteUrl = avatarUrl;
	}

	public String getAvatarLocalPath()
	{
		return _avatarLocalPath;
	}

	public void setAvatarLocalPath(String avatarPath)
	{
		this._avatarLocalPath = avatarPath;
	}

	public String getAccessToken()
	{
		return _accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this._accessToken = accessToken;
	}

	public String getTokenSecret()
	{
		return _tokenSecret;
	}

	public void setTokenSecret(String tokenSecret)
	{
		this._tokenSecret = tokenSecret;
	}
	
	public int getUserType()
	{
		return _userType;
	}

	public void setUserType(int userType)
	{
		_userType = userType;
	}

	public interface IOnDetailsArrived
	{
		public void detailsArrived(UserDetails details);
		
		public void onWebViewClosed(boolean isSigning);
	}

	public void setHandle(String handle)
	{
		mHandle = handle;
	}
	
	public String getHandle() {
		return mHandle;
	}

	
}
