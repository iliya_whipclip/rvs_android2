package com.whipclip.logic;

import java.util.Arrays;
import java.util.List;

import android.graphics.Point;

public class Consts
{
	public static final int				INNER_FRAGMENT							= 0;
	public static final int				TOP_LEVEL_FRAGMENT						= 1;

	public static final String			EXTRA_USER								= "EXTRA_USER";

	//  TUMBLR OAUTH
	public static final String			PREFERENCE_NAME							= "tumblr_oauth";
	public static final String			PREF_KEY_TOKEN_SECRET					= "oauth_token_secret";
	public static final String			PREF_KEY_TOKEN							= "oauth_token";
	public static final String			PREF_KEY_SCREEN_NAME					= "oauth_screen_name";
	public static final String			PREF_KEY_ACCESS_TOKEN_INFOS				= "oauth_access_token";
	public static final String			PREF_KEY_USER							= "tumblr_user";
	public static final String			PREF_KEY_PICTURE						= "tumblr_user_picture";
	public static final String			PREF_KEY_USER_NAME						= "tumblr_user_name";
	public static final String			PREF_KEY_USER_ID						= "tumblr_user_id";
	public static final String			PREF_SHOW_NOTIFY_PREIX					= "notify_show_id_";

	public static final String			TUMBLR_CALLBACK_URL						= "x-oauthflow-tumblr://tumblrlogin";
	public static final String			REQUEST_TOKEN_URL						= "http://www.tumblr.com/oauth/request_token";
	public static final String			ACCESS_TOKEN_URL						= "http://www.tumblr.com/oauth/access_token";
	public static final String			AUTH_URL								= "http://www.tumblr.com/oauth/authorize";

	public static String				TUMBLR_CONSUMER_KEY						= "qJhDVxSYqvAMy0SZyL9GrrsAVQAjGZUcTx7m1TJKVOcpSpdGnY"; // old "hprgz1Ff4H04cfKfDc1KlOPtcomoRid9IjEjusOJuH7gJXOob8";
	public static String				TUMBLR_CONSUMER_SECRET					= "45hV3P37ey9aLhMSwfHFDoLmzJcBmfkcEoJQVI5m7HKzaHTDkJ"; // old "q5plJ9SLa95s68s8MtE7szmtAgYVwjsRevm5K4huZTFyO1LYsK";
	public static String				PINTEREST_APP_ID						= "1439335";

	// Facebook Consts
	public static final String			FACEBOOK_APP_ID							= "708977959143377";
	public static final List<String>	FACEBOOK_PERMISSIONS_READ				= Arrays.asList("user_photos", "read_stream");
	public static final String			USER_DETAILS							= "USER_DETAILS".hashCode() + "";
	public static final int				ACTIVITY_RESULT_EXIT					= 999;
	public static final int				LOGIN_OK								= 123;
	public static final String			LINK									= "link";
	public static final String			POST_ID									= "post_id";

	// Twitter Consts
	public static String				CONSUMER_KEY							= "2TBxJP3BLE93XsgvYRPnxNQ9u";							//old - @"c8CI0RqOyWTCRAGq5ROT2cpN2"
	public static String				CONSUMER_SECRET							= "QsQrNC2tNfzeX9bKqMPx89OPCX6tMjeGnYlE6ECdboQq6eM8Kj"; //old - @"Dx72rm4MIR6Asa00DTGBhBUobrZVMfOFc0R64hs1EVAvtv6JaG"

	public static final String			CALLBACK_URL							= "twitterapp://connect";								// "http://www.retalix.com";// oauth://t4jsample";//
	public static final String			IEXTRA_AUTH_URL							= "auth_url";
	public static final String			IEXTRA_OAUTH_VERIFIER					= "oauth_verifier";
	public static final String			IEXTRA_OAUTH_TOKEN						= "oauth_token";

	public static final int				REQUEST_TYPE_NETWORK					= 100;

	public static final String			EXTRA_CLIENT_ID							= "EXTRA_CLIENT_ID";

	public static final String			PREF_USERNAME							= "PREF_USERNAME";
	public static final String			PREF_NOTIFICATION_COUNT					= "PREF_NOTIFICATION_COUNT";
	public static final String			PREF_SDK_USERID							= "PREF_SDK_USERID";
	public static final String			PREF_SDK_USER_IMAGE						= "PREF_SDK_USER_IMAGE";
	public static final String			PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE	= "PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE";
	public static final String			PREF_DEV_SETTING_ENVIROMENT				= "PREF_DEV_SETTING_ENVIROMENT";

	public static final String			DEFAULT_PREF_USERNAME					= "mobileguest@innovid.com";							//"eyal@innovid.com";						//

	public static final String			TIME_PATTERN_MINUTE						= "%sm";												// minute ago";
	public static final String			TIME_PATTERN_MINUTES					= "%sm";												// minutes ago";
	public static final String			TIME_PATTERN_HOUR						= "%sh";												// hour ago";
	public static final String			TIME_PATTERN_HOURS						= "%sh";												// hours ago";
	public static final String			TIME_PATTERN_TODAY						= "Today at %s";
	public static final String			TIME_PATTERN_YESTERDAY					= "Yesterday at %s";
	public static final String			TIME_PATTERN_DAY						= "%s at %s";
	public static final String			TIME_PATTERN_LAST_UPDATE				= "Last update %s";

	// Time Consts
	public static final int				TIME_CONSTS_MINUTE						= 60;
	public static final int				TIME_CONSTS_HOUR						= TIME_CONSTS_MINUTE * 60;
	public static final long			TIME_CONSTS_DAY							= TIME_CONSTS_HOUR * 24;
	public static final long			TIME_CONSTS_YESTERDAY					= 2 * TIME_CONSTS_DAY;
	public static final long			TIME_CONSTS_WEEK						= 7 * TIME_CONSTS_DAY;
	public static final long			TIME_CONSTS_MONTH						= 30 * TIME_CONSTS_DAY;
	public static final long			TIME_CONSTS_YEAR						= 12 * TIME_CONSTS_DAY;
	public static final String			PREF_LAST_UPDATE						= "PREF_LAST_UPDATE";

	public static final String			SIZE_B_FORMAT							= "%s B";
	public static final String			SIZE_KB_FORMAT							= "%s KB";
	public static final String			SIZE_MB_FORMAT							= "%s MB";
	public static final String			SIZE_GB_FORMAT							= "%s GB";

	//Fonts
//	public static final String			FONT_BENTON_SANS_BOLD					= "fonts/benton_sans_bold.otf";
	public static final String			FONT_BENTON_SANS_EXTRA_LIGHT			= "fonts/benton_sans_extra_light.otf";
	public static final String			FONT_BENTON_SANS_LIGHT					= "fonts/benton_sans_light.otf";
	public static final String			FONT_BENTON_SANS_MEDIUM					= "fonts/benton_sans_medium.otf";
	public static final String			FONT_BENTON_SANS_REGULAR				= "fonts/benton_sans_regular.otf";

	public static final String			MENU_ITEM_SELECT						= "menu_item_select";

	public static final String			COMMENTS_POST_ID						= "COMMENTS_POST_ID";
	public static final String			COMMENTS_NUM_ITEMS						= "COMMENTS_NUM_ITEMS";
	public static final String			TAB_POSITION							= "TAB_POSITION";

	//Search results limit
	/*
	 * !!!!!!!!!!!!! Watch out !!!!!!!!!!!!! 
	 * BY CHANGING THIS CONST TO HIGHER THAN 6, 
	 * WILL NEED TO ADJUST THE LAYOUT - search_results_indicators 
	 * AND THE CLASS SearchResultsMultiply.
	 */
	public static final int				SEARCH_RESULTS_MULTIPLY_LIMIT			= 6;

	//Compose preview image
	//sets the amount of images for the adapter to display on compose.
	public static final int				MAX_SEARCH_RESULTS_USERS				= 5;
	public static final int				MINUMUM_CUT_VIDEO_SECONDS				= 5;													//User can't cut a video shorter than 5 seconds. 
	public static final String			BUNDLE_CHANNEL_ID_COMPOSE				= "BUNDLE_CHANNEL_ID_COMPOSE";
	public static final String			BUNDLE_PROGRAML_ID_COMPOSE				= "BUNDLE_PROGRAML_ID_COMPOSE";
	public static final String			BUNDLE_PROGRAML_IS_LIVE					= "BUNDLE_PROGRAML_IS_LIVE";
	public static final String			BUNDLE_CHANNEL_SYN						= "BUNDLE_CHANNEL_SYN";
	public static final String			BUNDLE_TEXT_COMPOSE						= "BUNDLE_TEXT_COMPOSE";
	public static final String			BUNDLE_LATESTS_POST_ID					= "BUNDLE_LATESTS_POST_ID";

	public static final Point			IMAGE_POINT								= new Point(100, 100);

	public static final String			BUNDLE_USER_NAME						= "bundleUserName";
	public static final String			BUNDLE_POST_ID							= "bundlePostId";

	public static final String			EXTRA_USER_ID							= "EXTRA_USER_ID";

	public static final int				MAX_DISPLAY_COMMENTS					= 5;
	

}
