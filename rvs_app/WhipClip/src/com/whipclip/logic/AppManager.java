package com.whipclip.logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.facebook.Session;
import com.facebook.widget.LoginButton;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.whipclip.WhipClipApplication;
import com.whipclip.activities.SignUpActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSShow;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.util.FileManager;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.DrawableCache;
import com.whipclip.ui.DrawableCache.IDrawableCache;
import com.whipclip.ui.adapters.items.searchitems.PostInformation;
import com.whipclip.ui.adapters.items.searchitems.ScrollUsersInformation;
import com.whipclip.ui.remoteimageview.ImageHTTPThread.IOnFinishDownload;

public class AppManager implements IDrawableCache
{
	private static final String DATABASE_NAME = "whipclip_db";
	
	protected Object								_syncObj				= new Object();
	private static AppManager 						_instance;
	private FilesManager							_filesManager;
	private FacebookLogic							_facebookLogic;
	private TwitterLogic							_twitterLogic;
	private TumblrLogic								_tumblrLogic;
	private RVSSdk									_sdk;
	private RVSUser									_sdkUser;

	//Only applies to when the user presses a result in the search, and then goes to compose.
	//We need to save the RVSMediaContext of the item, in order to use it in the compose fragment.
	//onDestroy in ComposeFragment will set this item to be null again by calling AppManager.getInstance().setCurrentMediaContext(null);
	private RVSMediaContext							_currentMediaContext	= null;
	private long									_mediaContextStartOffsetSec = -1;
	private Typeface								_typeFaceMedium;
	private Typeface								_typeFaceLight;
	private Typeface								_typeFaceExtraLight;
	private Typeface								_typeFaceBold;
	private Typeface								_typeFaceRegular;

	/**
	 * Saved vars in order to pass in between fragments.
	 */
	private RelatedComments							_relatedComments;
	//In case the user is seeing the user he has searched for, and then presses "view all", then, changes the following on the users there,
	//We'd like to update the view pager's list to the updated following status.
	private HashMap<String, ScrollUsersInformation>	_realTimeScrollUsersInformation;
	//refresh posts screen in case the user has shared a video.
	private boolean									_refreshPosts			= false;
	private int										_currentFollowers		= -1;
	private String									_lastCommentedPostId	= null;

	private Tracker									_tracker1				= null;

	private ProfileInformation						_myProfileInformation;
	private Logger	logger = LoggerFactory.getLogger(AppManager.class);
	private DrawableCache 	mImagesCache;
	
//	private Storage	mStorage;

	private AppManager()
	{
		getSDK(); // init sdk
		_filesManager = new FilesManager();
		mImagesCache = new DrawableCache(); 	
//		  SQLiteDatabase db = createDatabase();
//		  mStorage = new Storage(db);
//		  mStorage.recreate();
	}

	public static AppManager getInstance()
	{
		if (_instance == null)
		{
			_instance = new AppManager();
		}
		return _instance;
	}
	
	public DrawableCache getImagesCache()
	{
		return mImagesCache;
	}
	
	 private SQLiteDatabase createDatabase() {

        	SQLiteOpenHelper  dbHelper = new SQLiteOpenHelper(WhipClipApplication._context, DATABASE_NAME, null, 1)  {

				@Override
				public void onCreate(SQLiteDatabase db) {}

				@Override
				public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
        	};
        	
            return dbHelper.getWritableDatabase();
	    }

	public boolean hasInternetConnection()
	{
		try
		{
			ConnectivityManager cm = (ConnectivityManager) WhipClipApplication._context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (cm != null)
			{
				if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected())
				{
					return true;
				}

				else if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected())
				{
					return true;
				}
			}
			return false;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public RVSSdk getSDK()
	{
		if (_sdk == null)
		{
			_sdk = RVSSdk.createRVSCore(WhipClipApplication._context);
		}
		return _sdk;
	}

	public FacebookLogic getFacebookLogic()
	{
		if (_facebookLogic == null)
		{
			_facebookLogic = new FacebookLogic();
		}
		return _facebookLogic;
	}

	public TwitterLogic getTwitterLogic()
	{
		if (_twitterLogic == null)
		{
			_twitterLogic = new TwitterLogic();
		}
		return _twitterLogic;
	}

	public TumblrLogic getTumblrLogic()
	{
		if (_tumblrLogic == null)
		{
			_tumblrLogic = new TumblrLogic();
		}
		return _tumblrLogic;
	}

	public FilesManager getFilesManager()
	{
		if (_filesManager == null)
		{
			_filesManager = new FilesManager();
		}
		return _filesManager;
	}

	public String getUserName()
	{
		return getPrefrences(Consts.PREF_USERNAME);//"eyal@innovid.com";//"yaniv@innovid.com";//
	}

	public String getPassword()
	{
		return "";
	}

	private void initServerValues()
	{
		if ("".equals(getPrefrences(Consts.PREF_USERNAME)))
		{
			savePrefrences(Consts.PREF_USERNAME, Consts.DEFAULT_PREF_USERNAME);
		}
	}

	public boolean isLoggedIn()
	{
//		return (UserDetails) getFilesManager().load(Consts.USER_DETAILS) != null;
		return !RVSSdk.sharedRVSSdk().isSignedOut();
	}

	public UserDetails getUserDetails()
	{
		return (UserDetails) getFilesManager().load(Consts.USER_DETAILS);
	}

	public void deleteUserDetails()
	{
		getFilesManager().delete(WhipClipApplication._context.getCacheDir() + "/images/" + getPrefrences(Consts.PREF_SDK_USERID).hashCode() + ".png");
		savePrefrences(Consts.PREF_SDK_USERID, "");
		savePrefrences(Consts.PREF_SDK_USER_IMAGE, "");
		if (!getSDK().isSignedOut())
		{
			getSDK().signOut();
		}
		_sdkUser = null;
		// Reset notifications
		if (WhipClipApplication.getCurrentActivity() instanceof MainActivity)
		{
			((MainActivity) WhipClipApplication.getCurrentActivity()).resetNotifications();
		}
		try
		{
			CookieSyncManager syncManager = CookieSyncManager.createInstance(WhipClipApplication._context);
			CookieManager.getInstance().removeAllCookie();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
//		if ((getUserDetails() != null && getUserDetails().getUserType() == UserDetails.USER_TYPE_FACEBOOK) || Session.getActiveSession().isOpened())
//		{
//			new LoginButton(WhipClipApplication.getCurrentActivity()).performClick();
//		}
		getFilesManager().delete(Consts.USER_DETAILS);
	}

	public int convertDpToPixel(float dp, Context context)
	{
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

	public int convertPixelsToDp(float px, Context context)
	{
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return (int) dp;
	}

	// ~~~~~~~~~~~~~~~~~~ Prefrences Methods ~~~~~~~~~~~~~~~~~~ //

	public void savePrefrences(String key, String value)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		prefs.edit().putString(key, value).commit();
	}

	public void savePrefrences(String key, long value)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		prefs.edit().putLong(key, value).commit();
	}

	public String getPrefrences(String key)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		return prefs.getString(key, ""); 
	}
	
	public boolean removePrefrences(String key)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		return prefs.edit().remove(key).commit(); 
	}

	public String getPrefrences(String key, String defaultValue)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		return prefs.getString(key, defaultValue);
	}

	public long getPrefrences(String key, long defaultValue)
	{
		SharedPreferences prefs = WhipClipApplication._context.getSharedPreferences(WhipClipApplication._context.getApplicationContext().getPackageName(), 0);
		return prefs.getLong(key, defaultValue);
	}

//	public boolean isFirstTime(String screenName)
//	{
//		String bool = getPrefrences("ISFIRSTTIME" + screenName, "true");
//		if ("true".equals(bool))
//		{
//			savePrefrences("ISFIRSTTIME" + screenName, "false");
//			return true;
//		}
//		return false;
//	}
	
	public boolean isFirstTime()
	{
		String bool = getPrefrences("ISFIRSTTIME", "true");
		if ("true".equals(bool)){
			return true;
		}
		
		return false;
	}
	
	public void setFirstTime(boolean isFirstTime)
	{
		savePrefrences("ISFIRSTTIME" , String.valueOf(isFirstTime));
	}
	

	public void saveSDKUser(RVSUser result)
	{
		_sdkUser = result;
	}

	public RVSUser getSDKUser()
	{
		return _sdkUser;
	}

	public void setCurrentMediaContext(RVSMediaContext mediaContext, long currentOffsetMillisec)
	{
		_currentMediaContext = mediaContext;
		_mediaContextStartOffsetSec = currentOffsetMillisec/1000;
	}
	
	public long getMediaContextStartOffsetSec() {
		return _mediaContextStartOffsetSec;
	}

	public void setMediaContextStartOffsetSec(long currentOffsetMillisec)
	{
		_mediaContextStartOffsetSec = currentOffsetMillisec;
	}
	
	public RVSMediaContext getCurrentMediaContext()
	{
		return _currentMediaContext;
	}

	public Typeface getTypeFaceMedium()
	{
		if (_typeFaceMedium == null)
		{
			_typeFaceMedium = Typeface.createFromAsset(WhipClipApplication._context.getAssets(), Consts.FONT_BENTON_SANS_MEDIUM);
		}
		return _typeFaceMedium;
	}

	public Typeface getTypeFaceLight()
	{
		if (_typeFaceLight == null)
		{
			_typeFaceLight = Typeface.createFromAsset(WhipClipApplication._context.getAssets(), Consts.FONT_BENTON_SANS_LIGHT);
		}
		return _typeFaceLight;
	}

	public Typeface getTypeFaceExtraLight()
	{
		if (_typeFaceExtraLight == null)
		{
			_typeFaceExtraLight = Typeface.createFromAsset(WhipClipApplication._context.getAssets(), Consts.FONT_BENTON_SANS_EXTRA_LIGHT);
		}
		return _typeFaceExtraLight;
	}

//	public Typeface getTypeFaceBold()
//	{
//		if (_typeFaceBold == null)
//		{
//			_typeFaceBold = Typeface.createFromAsset(WhipClipApplication._context.getAssets(), Consts.FONT_BENTON_SANS_BOLD);
//		}
//		return _typeFaceBold;
//	}

	public Typeface getTypeFaceRegular()
	{
		if (_typeFaceRegular == null)
		{
			_typeFaceRegular = Typeface.createFromAsset(WhipClipApplication._context.getAssets(), Consts.FONT_BENTON_SANS_REGULAR);
		}
		return _typeFaceRegular;
	}

	public void setRelatedComments(int relatedTotalComments, String relatedPostId, ArrayList<RVSComment> relatedComments)
	{
		if (relatedPostId == null || relatedComments == null)
		{
			_relatedComments = null;
		}
		else
		{
			_relatedComments = new RelatedComments(relatedTotalComments, relatedPostId, relatedComments);
		}
	}

	/**
	 * Must run a null check on this function, since if there's no change, this will return null.
	 * @return ArrayList<RVSCommentImp> representing the updated comments. 
	 */
	public RelatedComments getRelatedComments()
	{
		return _relatedComments;
	}

	/**
	 * _realTimePostInformation aux functions
	 */
	public void insertElementsToRealtimeScrollUsers(ArrayList<RVSUser> users)
	{
		if (_realTimeScrollUsersInformation == null)
		{
			_realTimeScrollUsersInformation = new HashMap<String, ScrollUsersInformation>();
		}
		for (RVSUser user : users)
		{
			if (_realTimeScrollUsersInformation.get(user.getUserId()) != null)
			{ //refreshing data, if item already exists.
				ScrollUsersInformation info = _realTimeScrollUsersInformation.get(user.getUserId());
				info.setFollowedByMe(user.isFollowedByMe());
				info.setFollowedCount(user.getFollowerCount());
				_realTimeScrollUsersInformation.remove(user.getUserId());
				_realTimeScrollUsersInformation.put(user.getUserId(), info);
			}
			else
			{
				_realTimeScrollUsersInformation.put(user.getUserId(), new ScrollUsersInformation(user));
			}
		}
	}

	public HashMap<String, ScrollUsersInformation> getRealTimeScrollUsersInformation()
	{
		if (_realTimeScrollUsersInformation == null)
		{
			_realTimeScrollUsersInformation = new HashMap<String, ScrollUsersInformation>();
		}
		return _realTimeScrollUsersInformation;
	}

	public void clearRealTimeScrollUsersInformation()
	{
		if (_realTimeScrollUsersInformation != null)
		{
			if (_realTimeScrollUsersInformation.size() > 0)
			{
				_currentFollowers = 0;
				for (ScrollUsersInformation info : _realTimeScrollUsersInformation.values())
				{
					_currentFollowers += info.isFollowedByMe() ? 1 : 0;
				}
			}
			_realTimeScrollUsersInformation.clear();
		}
	}

	public void updateFollowersInfo(String userId, int followerCount, boolean isFollowedByMe)
	{
		if (_realTimeScrollUsersInformation != null)
		{
			ScrollUsersInformation info = _realTimeScrollUsersInformation.get(userId);
			if (info != null)
			{
				info.setFollowedCount(followerCount);
				info.setFollowedByMe(isFollowedByMe);
			}
		}
	}

	public boolean getRefreshPosts()
	{
		return _refreshPosts;
	}

	public void setRefreshPosts(boolean value)
	{
		_refreshPosts = value;
	}

	private boolean	_callProfileBefore	= false;
	private RVSShow	mShow;

	private WCNotificationManager	mNotificationManager;

	private PostInformation	mCurrentPostInfo;

	private RVSMediaContext	mComposeMediaContext;

	private RVSPost	mOpenedCommentPost;

	public void setCallProfileFragmentBefore(boolean value)
	{
		_callProfileBefore = value;
	}

	public boolean getCallProfileFragmentBefore()
	{
		return _callProfileBefore;
	}

	public void init(Context context) {
		
		initGA(context);
		mNotificationManager = new WCNotificationManager();
	}
	
	public void initGA(Context context)
	{
		String trackerID = Utils.getConfigParameter("GATrackerID").trim();
		if (trackerID != null)
		{
			_tracker1 = GoogleAnalytics.getInstance(context).newTracker(trackerID);
				
//			GoogleAnalytics.getInstance(context).setLocalDispatchPeriod(3);
//			GoogleAnalytics.getInstance(context).getLogger().setLogLevel(LogLevel.VERBOSE);

		}
	}

	public void sendToGA(String name)
	{
		if (_tracker1 != null)
		{
			_tracker1.setScreenName(name);
			_tracker1.send(new HitBuilders.AppViewBuilder().build());
		}
		
	}

	public int getUserUpdatedInfo()
	{
		return _currentFollowers;
	}

	public void resetUserUpdatedInfo()
	{
		_currentFollowers = -1;
	}

	public String getLastPostCommented()
	{
		return _lastCommentedPostId;
	}

	public void setLastPostCommented(String lastCommentedPostId)
	{
		_lastCommentedPostId = lastCommentedPostId;
	}
	
	public void showLogin(Activity context,int requestCode ) {
		 showLogin(context , requestCode, false);
	}
	
	public void showLogin(Activity context,int requestCode, boolean firstTime) {
		
//		Intent intent = new Intent(context, LoginActivity.class);
		Intent intent = new Intent(context, SignUpActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra("first_time", firstTime);
		
		if (requestCode == -1) {
			context.startActivity(intent);
		} else {
			context.startActivityForResult(intent, 1);
		}
	}

	public void setShow(RVSShow show){
		mShow = show;
	}
	
	public RVSShow getShow() {
		return mShow;
	}

	public WCNotificationManager getNotificationManager()
	{
		return mNotificationManager;
		
	}

	public void clearImagesCache()
	{
		if (mImagesCache != null) {
			mImagesCache.clear();
		}
	}

	public void setOpenedCommentsViewForPost(PostInformation currentPostInfo)
	{
		mCurrentPostInfo = currentPostInfo;
	}


	public PostInformation getOpenedCommentsViewPost()
	{
		return mCurrentPostInfo;
	}
	
	public synchronized void saveImageToFile(File file, byte[] result, String _url, String _local, final  IOnFinishDownload _listener)
	{
//		logger.info("imagelabel save1 _local :" + _local );
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			try {
				Log.i(getClass().getName(), "download image finished, url " + _url + ", stored: " + _local);
				file.getParentFile().mkdirs();
				file.createNewFile();
				
				fos = new FileOutputStream(_local);
				fos.write(result, 0, result.length);
				fos.flush();
				
				if (_listener != null)
				{
					_listener.finished(_local, true);
				}
			}
			catch (Exception e)
			{
				Log.e(getClass().getName(), e.getMessage(), e);
				file.delete();
				
			} finally {
				
				
				fos.flush();
				fos.close();
			}
		}
		catch (Exception e)
		{
			Log.e(getClass().getName(), e.getMessage(), e);
			e.printStackTrace();
			file.delete();
		}
		
//		logger.info("imagelabel done1 _local :" + _local );
		
	}

	public synchronized boolean isFileExist(String _local)
	{
		
		File local = new File(_local); 
		if (local.exists() && local.length() > 0)
		{
//			logger.info("imagelabel isFileExist true _local :" + _local );
			return true;
		}
		
//		logger.info("imagelabel isFileExist false _local :" + _local );
		return false;
	}

	public synchronized void addToCache(IDrawableCache _cache, String local, BitmapDrawable drawable)
	{
//		logger.info("imagelabel addToCache _local :" + local );
		_cache.addToCache(local, drawable);
	}

	public synchronized Drawable getFromCache(IDrawableCache _cache, String _local)
	{

		 Drawable fromCache = _cache.getFromCache(_local);
//		 logger.info("imagelabel getFromCache " + (fromCache != null) + ", _local :" + _local );
		 return fromCache;
	}

	public synchronized void renameFile(String oldPath, String newPath)
	{
//		logger.info("imagelabel renameFile _local :" + newPath );
		FileManager.rename(oldPath, newPath, true);
	}

	public RVSMediaContext getComposeMediaContext()
	{
		return mComposeMediaContext;
	}
	
	public void setComposeMediaContext(RVSMediaContext compose)
	{
		mComposeMediaContext = compose;
	}

	public void setOpenedCommentPost(RVSPost _post)
	{
		mOpenedCommentPost = _post; 
	}
	
	public RVSPost getOpenedCommentPost()
	{
		return mOpenedCommentPost; 
	}

	@Override
	public void addToCache(String name, Drawable draw)
	{
		mImagesCache.addDrawableToMemoryCache(name, draw);
	}

	@Override
	public Drawable getFromCache(String name)
	{
		return mImagesCache.getDrawableFromMemCache(name);
	}

}
