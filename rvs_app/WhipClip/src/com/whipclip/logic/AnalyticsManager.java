package com.whipclip.logic;

import java.util.HashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.whipclip.rvs.sdk.RVSSdk;
import com.whipclip.rvs.sdk.dataObjects.RVSChannel;
import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.RVSEndCard;
import com.whipclip.rvs.sdk.dataObjects.RVSMediaContext;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSProgram;
import com.whipclip.rvs.sdk.dataObjects.RVSUser;
import com.whipclip.rvs.sdk.util.Utils;
import com.whipclip.ui.adapters.TabFragmentAdapter;

public class AnalyticsManager
{
	private static final String MIXPANEL_TOKEN 				= "c7c91f9a702950ad2226ff621f8c1c8c"; // prod01
	private static final String MIXPANEL_INTEGRATION_TOKEN 	= "054df9e8367455687e17bc3ff692ed9d";

	
	private Logger logger = LoggerFactory.getLogger(AnalyticsManager.class);

	public static final int RVSAppAnalyticsManagerSourceTypePost = 0;
	public static final int RVSAppAnalyticsManagerSourceTypeProgramAired = 1;
	public static final int RVSAppAnalyticsManagerSourceTypeProgramLive = 2;
	
	public static final int RVSAppAnalyticsManagerShareTypeWhipclip 	= 0;
	public static final int RVSAppAnalyticsManagerShareTypeFacebook		= 1;
	public static final int RVSAppAnalyticsManagerShareTypeTwitter		= 2;
	public static final int RVSAppAnalyticsManagerShareTypeEmail		= 3;
	public static final int RVSAppAnalyticsManagerShareTypeSMS			= 4;
	public static final int RVSAppAnalyticsManagerShareTypeTumblr		= 5;
	public static final int RVSAppAnalyticsManagerShareTypePinterest	= 6;
	public static final int RVSAppAnalyticsManagerShareTypeOther		= 7;

//	private String mEventClipPlay         = "clip_play";
//	private String mEventPostLike         = "post_like";
	private String mEventPostComment      = "post_comment";
	private String mEventPostShare        = "post_share";
	private String mEventPostCreate       = "post_create";
//	private String mEventEndCardView      = "endcard_view";
	private String mEventEndCardLinkClick = "endcard_link_click";

	//event user/device
	private String mPropEventUserId     = "event_user_id";
	private String mPropEventUserHandle = "event_user_handle";
	private String mPropEventUserName   = "event_user_name";
	private String mPropEventDeviceId   = "event_device_id";

	//media
//	private String mPropClipDuration = "clip_duration";

	//see RVSAppAnalyticsManagerSourceType
//	private String "source_type" = "source_type";

	//post id/user
//	private String mPropPostId         = "post_id";
//	private String mPropPostUserHandle = "post_user_handle";
//	private String mPropPostUserName   = "post_user_name";

	//program
//	private String mPropProgramId     = "program_id";
//	private String mPropContentId     = "content_id";
//	private String mPropShowId        = "show_id";
//	private String mPropShowName      = "show_name";
//	private String mPropSeasonNumber  = "season_number";
//	private String mPropEpisodeNumber = "episode_number";
//	private String mPropEpisodeName   = "episode_name";

	//channel
//	private String mPropChannelId   = "channel_id";
	private String mPropChannelName = "channel_name";

	//comment
	private String mPropCommentId = "comment_id";

	//see RVSAppAnalyticsManagerShareType
	private String mPropShareType = "share_type";

	//if srcType == RVSAppAnalyticsManagerSourceTypePost
	private String mPropSourcePostId         = "source_post_id";
	private String mPropSourcePostUserId     = "source_post_user_id";
	private String mPropSourcePostUserHandle = "source_post_user_handle";
	private String mPropSourcePostUserName   = "source_post_user_name";

	//end card
	private String mPropEndCardClickable         = "endcard_clickable";
	private String mPropEndCardTuneInInformation = "endcard_tune_in_information";
	private String mPropEndCardLinkDescription   = "endcard_link_description";
	private String mPropEndCardLinkURL           = "endcard_link_url";
	
		
	private static AnalyticsManager mSharedManager;
	private MixpanelAPI mMixPanel = null;
	
	private AnalyticsManager(Context context) {
		
		String baseUrlString = RVSSdk.sharedRVSSdk().requestManager().getBaseUrlString();
		if (baseUrlString.indexOf("integration") >= 0) {
			mMixPanel = MixpanelAPI.getInstance(context, MIXPANEL_INTEGRATION_TOKEN);
		} else {
			mMixPanel = MixpanelAPI.getInstance(context, MIXPANEL_TOKEN);	
		}
		 
	}
	 
	 static public AnalyticsManager sharedManager() {
        return mSharedManager;
    }

	 // should by run only one time
    static public AnalyticsManager createManager(Context context) {

        if (mSharedManager != null) {
            return null;
        }

        mSharedManager = new AnalyticsManager(context);
        return mSharedManager;
    }
	    
	
	public boolean mixpanelWillFlush(MixpanelAPI mixpanel)
	{
	    return true;
	}
	
	private String tabPositionToName(int position) {
		switch (position)
		{
			case 0: return "Home";
			case 1: return "Trending";
			case 2: return "Clip TV";
			case 3: return "TV Shows";
			case 4: return "Clip Music";
		}
		
		return String.valueOf(position);
	}
	
	// [+] Clip Playback
	public void trackClipPlaybackEventWithPost(RVSPost post, boolean isReplay, int tabPosition){
		
		HashMap properties	= new HashMap();
		properties.put("source_type", RVSAppAnalyticsManagerSourceTypePost);
		properties.put("event_location", tabPositionToName(tabPosition));
	    
	    internalTrackEvent("clip_play",post.getMediaContext(), post, post.getChannel(), post.getProgram(), properties);
	}
	
	public void trackClipReplayPlaybackEventWithPost(RVSPost post, boolean isReplay, int tabPosition){
		
		HashMap properties	= new HashMap();
		properties.put("source_type", RVSAppAnalyticsManagerSourceTypePost);
		properties.put("event_location", tabPositionToName(tabPosition));
	    
	    internalTrackEvent("clip_replay",post.getMediaContext(), post, post.getChannel(), post.getProgram(), properties);
	}
	
	/**
	 * @param sourceType One of {@link #RVSAppAnalyticsManagerSourceTypePost}, {@link #RVSAppAnalyticsManagerSourceTypeProgramAired}, or {@link #RVSAppAnalyticsManagerSourceTypeProgramLive}.
	 */
	public void trackClipPlaybackEventWithProgram(RVSProgram program,
	                                 RVSChannel channel,
	                                 RVSMediaContext mediaContext,
	                                 int sourceType,
	                                 boolean isReplay, int tabPosition)
	{
		HashMap properties	= new HashMap();
//		properties.put("source_type", stringWithSourceType(sourceType));
//		properties.put("is_replay", isReplay ? 1 : 0);
		properties.put("event_location", tabPositionToName(tabPosition));
	    internalTrackEvent("clip_play", mediaContext, null, channel, program, properties);
	}
	
	
	// [+] Post
	public void trackPostLikeEventWithPost(RVSPost post, int tabPosition){
		
		HashMap properties	= new HashMap();
		properties.put("event_location", tabPositionToName(tabPosition));
		
	    internalTrackPostEvent("post_like", post, properties);
	}
	
	public void trackPostCommentWithPost(RVSPost post, RVSComment comment, int tabPosition) {
	    
//		RVSParameterAssert(comment);
	    
	    HashMap properties = new HashMap();
	    properties.put(mPropCommentId, comment.getCommentId());
		properties.put("event_location", tabPositionToName(tabPosition));

	    internalTrackPostEvent(mEventPostComment, post, properties);
	}
	
	/**
	 * @param shareType One of {@link #RVSAppAnalyticsManagerShareTypeWhipclip}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeFacebook}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTwitter}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeEmail}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeSMS}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTumblr},
	 * 							{@link #RVSAppAnalyticsManagerShareTypePinterest}, 
	 * 							or {@link #RVSAppAnalyticsManagerShareTypeOther}.
	 **/
	public void trackPostShareWithPost(RVSPost post, int shareType) {
	    
		HashMap properties = new HashMap();
	    properties.put(mPropShareType, stringWithShareType(shareType));
	    
	    internalTrackPostEvent(mEventPostShare, post, properties);
	}
	
	
	/**
	 * @param shareType One of {@link #RVSAppAnalyticsManagerShareTypeWhipclip}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeFacebook}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTwitter}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeEmail}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeSMS}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTumblr},
	 * 							{@link #RVSAppAnalyticsManagerShareTypePinterest}, 
	 * 							or {@link #RVSAppAnalyticsManagerShareTypeOther}.
	 **/
	private void trackPostCreateWithPost(RVSPost post, int sourceType, RVSPost sourcePost)
	{
	    HashMap<String, String> properties = new HashMap<String, String>();
	    properties.put("source_type", stringWithSourceType(sourceType));
	    
	    if (sourceType == RVSAppAnalyticsManagerSourceTypePost && sourcePost != null)
	    {
	        properties.put(mPropSourcePostId, sourcePost.getPostId());
	        properties.put(mPropSourcePostUserId, sourcePost.getAuthor().getUserId());
	        properties.put(mPropSourcePostUserHandle, sourcePost.getAuthor().getHandle());
	        properties.put(mPropSourcePostUserName, sourcePost.getAuthor().getName());
	    }
	    
	    internalTrackPostEvent(mEventPostCreate, post, properties);
	}
	
	
	/**
	 *  internal track post event
	 *
	 *  @param eventName       event name
	 *  @param eventUser       event user
	 *  @param post            post
	 *  @param extraProperties extra properties
	 */
	private void internalTrackPostEvent(String eventName,
			RVSPost post,
			HashMap extraProperties) {
		Utils.assertInEmptyString(eventName);
//	    RVSParameterAssert(post);
	    
	    //track, use internal post properties
	    internalTrackEvent(eventName, post.getMediaContext(), post, post.getChannel(), post.getProgram(), extraProperties);
	}
	
	// [+] End Card
	
	public void trackEndCardViewEventWithPost(RVSPost post)
	{
	    internalTrackEndCardEvent("endcard_view", post.getProgram().getEndCard(), post.getMediaContext(), post, post.getChannel(), post.getProgram(),  RVSAppAnalyticsManagerSourceTypePost);
	}
	
	public void trackEndCardViewEventWithPost(RVSProgram program,
	                                RVSChannel channel,
	                           RVSMediaContext mediaContext) {
	    internalTrackEndCardEvent("endcard_view", program.getEndCard(), mediaContext, null, channel, program,  RVSAppAnalyticsManagerSourceTypeProgramAired);
	}
	
	public void trackEndCardLinkClickWithPost(RVSPost post) {
	    internalTrackEndCardEvent(mEventEndCardLinkClick, post.getProgram().getEndCard(), post.getMediaContext(), post,
	    		post.getChannel(), post.getProgram(), RVSAppAnalyticsManagerSourceTypePost);
	}
	
	private void trackEndCardLinkClickEventWithProgram(RVSProgram program,
			RVSChannel channel,
			RVSMediaContext mediaContext) {
		
	    internalTrackEndCardEvent(mEventEndCardLinkClick, program.getEndCard(), mediaContext, null, 
	    		channel, program, RVSAppAnalyticsManagerSourceTypeProgramAired);
	}
	
	/**
	 *  internal track end card event
	 *
	 *  @param eventName    event name
	 *  @param eventUser    event user
	 *  @param endCard      end card
	 *  @param mediaContext media context
	 *  @param post         post
	 *  @param channel      channel
	 *  @param program      program
	 *  @param sourceType   source type
	 */
	private void internalTrackEndCardEvent(String eventName,
			RVSEndCard endCard,
			RVSMediaContext mediaContext,
			RVSPost post,
			RVSChannel channel,
			RVSProgram program,
			int sourceType)
	{
	    HashMap<String, String> properties = new HashMap<String, String>();
	    
	    properties.put("source_type", stringWithSourceType(sourceType));
	    if (endCard != null) {
		    properties.put(mPropEndCardTuneInInformation, endCard.getTuneInInformation());
		    properties.put(mPropEndCardLinkDescription, endCard.getLinkDescription());
		    properties.put(mPropEndCardLinkURL, endCard.getLinkUrl());
		    
		    boolean clickable = (endCard.getLinkUrl() != null && !endCard.getLinkUrl().isEmpty() &&
		    		endCard.getLinkDescription() != null && !endCard.getLinkDescription().isEmpty());
		    properties.put(mPropEndCardClickable, clickable ? "true" : "false");
	    }
	    
	    internalTrackEvent(eventName, mediaContext, post, channel, program, properties);
	}
	
	/**
	 *  track an event
	 *
	 *  @param eventName       event name
	 *  @param mediaContext    media context
	 *  @param sourceType      source type
	 *  @param post            post
	 *  @param channel         channel
	 *  @param program         program
	 *  @param extraProperties extra properties
	 */
	private void internalTrackEvent(String eventName,
			RVSMediaContext mediaContext,
			RVSPost post,
			RVSChannel channel,
			RVSProgram program,
			HashMap<String, String> extraProperties) {
		
	    HashMap<String, String> properties = new HashMap<String, String>();
	    properties.putAll(extraProperties);
	    
	    RVSUser eventUser = null;
	    if (!RVSSdk.sharedRVSSdk().isSignedOut()) {
	    	eventUser = RVSSdk.sharedRVSSdk().getMyUser();
	    }
	    
	    if (eventUser != null)
	    {
	        properties.put("event_user_id", eventUser.getUserId());
	        properties.put("event_user_handle", eventUser.getHandle());
	        properties.put("event_user_name", eventUser.getName());
	    }

	    properties.put("event_device_id", RVSSdk.sharedRVSSdk().serviceManager().getUniqueDeviceIdentifier());
	    
	    if (mediaContext != null)
	    {
	        properties.put("clip_duration", String.valueOf(mediaContext.getDuration()));
	    }
	    
	    if (post != null)
	    {
	        properties.put("post_id", post.getPostId());
	        properties.put("post_message", post.getText());
	        if (post.getAuthor() != null) {
		        properties.put("post_user_id", post.getAuthor().getUserId());
		        properties.put("post_user_handle", post.getAuthor().getHandle());
		        properties.put("post_user_name", post.getAuthor().getName());
		        properties.put("post_user_is_editor", String.valueOf(post.getAuthor().isEditor()));
	        }
	    }
	    
	    if (program != null)
	    {
	        properties.put("program_id", program.getProgramId());
	        properties.put("content_id", program.getContentId());
	        properties.put("show_id", program.getShowId());
	        properties.put("show_name", program.getSpecialShowName());
	        if (program.getEpisodeMetadata() != null) {
	        	properties.put("season_number", String.valueOf(program.getEpisodeMetadata().getSeasonNumber()));
	        	properties.put("episode_number", String.valueOf(program.getEpisodeMetadata().getEpisodeNumber()));
	        	properties.put("episode_name", program.getEpisodeMetadata().getName());
	        }
	        
	    }
	    
	    if (channel != null)
	    {
	        properties.put("channel_id", channel.getChannelId());
	        properties.put("channel_name", channel.getName());
	    }
	    
	    //track
	    internalTrackEvent(eventName, properties);
	}
	
	/**
	 * @param sourceType One of {@link #RVSAppAnalyticsManagerSourceTypePost}, {@link #RVSAppAnalyticsManagerSourceTypeProgramAired}, or {@link #RVSAppAnalyticsManagerSourceTypeProgramLive}.
	 **/
	private String stringWithSourceType(int sourceType) {
	    
		switch (sourceType) {
	        case RVSAppAnalyticsManagerSourceTypePost:
	            return "POST";
	            
	        case RVSAppAnalyticsManagerSourceTypeProgramAired:
	            return "PROGRAM_AIRED";
	            
	        case RVSAppAnalyticsManagerSourceTypeProgramLive:
	            return "PROGRAM_LIVE";
	            
	        default:
	            return "";
	    }
	}
	
	/**
	 * @param shareType One of {@link #RVSAppAnalyticsManagerShareTypeWhipclip}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeFacebook}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTwitter}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeEmail}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeSMS}, 
	 * 							{@link #RVSAppAnalyticsManagerShareTypeTumblr},
	 * 							{@link #RVSAppAnalyticsManagerShareTypePinterest}, 
	 * 							or {@link #RVSAppAnalyticsManagerShareTypeOther}.
	 **/
	private String stringWithShareType(int shareType) {
	    
		switch (shareType) {
	        case RVSAppAnalyticsManagerShareTypeWhipclip:
	            return "WHIPCLIP";
	            
	        case RVSAppAnalyticsManagerShareTypeFacebook:
	            return "FACEBOOK";
	            
	        case RVSAppAnalyticsManagerShareTypeTwitter:
	            return "TWITTER";
	            
	        case RVSAppAnalyticsManagerShareTypeEmail:
	            return "EMAIL";
	            
	        case RVSAppAnalyticsManagerShareTypeSMS:
	            return "SMS";
	            
	        case RVSAppAnalyticsManagerShareTypeTumblr:
	            return "TUMBLR";
	            
	        case RVSAppAnalyticsManagerShareTypePinterest:
	            return "PINTEREST";
	            
	        case RVSAppAnalyticsManagerShareTypeOther:
	        default:
	            return "OTHER";
	    }
	}
	
	private void internalTrackEvent(String eventName, HashMap properties) {
	    
		mMixPanel.track(eventName, new JSONObject(properties));
	    mMixPanel.flush();
	    logger.info(String.format("will track event: %s with properties: %s", eventName, properties));
	}
	
	public void onDestroy() {
		if (mMixPanel != null) {
			mMixPanel.flush();
		}
	}
	

}




