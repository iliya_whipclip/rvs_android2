package com.whipclip.logic;

import java.util.ArrayList;

import com.whipclip.rvs.sdk.dataObjects.RVSComment;
import com.whipclip.rvs.sdk.dataObjects.imp.RVSCommentImp;

public class RelatedComments
{
	private int							_relatedTotalComments;
	private String						_relatedPostId;
	private ArrayList<RVSComment>	_relatedComments;

	public RelatedComments(int relatedTotalComments, String relatedPostId, ArrayList<RVSComment> relatedComments)
	{
		super();
		_relatedPostId = relatedPostId;
		_relatedComments = relatedComments;
		_relatedTotalComments = relatedTotalComments;
	}

	public int getRelatedTotalComments()
	{
		return _relatedTotalComments;
	}

//	public void setRelatedTotalComments(int relatedTotalComments)
//	{
//		_relatedTotalComments = relatedTotalComments;
//	}

	public String getRelatedPostId()
	{
		return _relatedPostId;
	}

//	public void setRelatedPostId(String relatedPostId)
//	{
//		_relatedPostId = relatedPostId;
//	}

	public ArrayList<RVSComment> getRelatedComments()
	{
		return _relatedComments;
	}

	//	public void setRelatedComments(ArrayList<RVSCommentImp> relatedComments)
	//	{
	//		_relatedComments = relatedComments;
	//	}
	
	

}
