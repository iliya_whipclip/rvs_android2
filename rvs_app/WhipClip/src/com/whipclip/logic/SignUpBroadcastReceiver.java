package com.whipclip.logic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.whipclip.WhipClipApplication;
import com.whipclip.rvs.sdk.managers.RVSServiceManager;

public class SignUpBroadcastReceiver extends BroadcastReceiver
{
	public interface SingUpStatusChanged {
		public void onSignedUpStatusChanged (boolean isSignedIn, String errorMessage);
		public void onSignedOut(String error);
	};
	
	private SingUpStatusChanged onSignUpListener;
	
	public SignUpBroadcastReceiver(SingUpStatusChanged listener) {
		
		super();
		setOnSignUpListener(listener);
	}
	
	public void registerReceiver() {
		LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(this,
				new IntentFilter(RVSServiceManager.RVSServiceSignedInNotification));
		LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(this,
				new IntentFilter(RVSServiceManager.RVSServiceSignInErrorNotification));
		LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(this,
				new IntentFilter(RVSServiceManager.RVSServiceSignedInPendingNotification));

		LocalBroadcastManager.getInstance(WhipClipApplication._context).registerReceiver(this,
				new IntentFilter(RVSServiceManager.RVSServiceSignedOutNotification));
	}
	
	public void unregisterReceiver() {
		LocalBroadcastManager.getInstance(WhipClipApplication._context).unregisterReceiver(this);
	}
	
	public void setOnSignUpListener(SingUpStatusChanged listener) {
		onSignUpListener = listener;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onReceive(Context context, Intent intent)
	{
		
		// Get extra data included in the Intent
		String action = intent.getAction();
		Log.i("SingUpBroadcastReceiver", "Got message: " + action);

		if (action.equals(RVSServiceManager.RVSServiceSignInErrorNotification))
		{
			String errorMsg = "";
			if (intent.getExtras() != null) {
				errorMsg = intent.getExtras().getString("error");
			}
			if (onSignUpListener != null) onSignUpListener.onSignedUpStatusChanged(false, errorMsg);
				
		}
		else if (action.equals(RVSServiceManager.RVSServiceSignedInNotification) || action.equals(RVSServiceManager.RVSServiceSignedInPendingNotification))
		{
			String error = intent.getStringExtra("error");
			Log.i("SingUpBroadcastReceiver", "sign in error:" + error);
			
			if (onSignUpListener != null) onSignUpListener.onSignedUpStatusChanged(true, null);
			
		}else if (action.equals(RVSServiceManager.RVSServiceSignedOutNotification)) {

			String error = intent.getStringExtra("error");
			Log.i("SingUpBroadcastReceiver", "sign in error:" + error);
			if (onSignUpListener != null) onSignUpListener.onSignedOut(error);
		}
		
	}

}

