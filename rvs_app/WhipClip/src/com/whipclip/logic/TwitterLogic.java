package com.whipclip.logic;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.whipclip.R;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.UserDetails.IOnDetailsArrived;
import com.whipclip.ui.OAuthDialog;
import com.whipclip.ui.OAuthDialog.TwDialogListener;

public class TwitterLogic extends BaseLogic implements TwDialogListener
{
	private final String		TAG	= getClass().getSimpleName();
	private Activity			_activity;
	private Twitter				twitter;
	private RequestToken		requestToken;
	private IOnDetailsArrived	_listener;

	public void connect(Activity activity, IOnDetailsArrived listener)
	{
		_listener = listener;
		_activity = activity;
		buildTwitter();

		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					requestToken = twitter.getOAuthRequestToken(Consts.CALLBACK_URL); 
					_activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								OAuthDialog dialog = new OAuthDialog(_activity, requestToken.getAuthenticationURL(), TwitterLogic.this);
								dialog.setCallbackUrl(Consts.CALLBACK_URL);
								dialog.show();
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
					});
				}
				catch (TwitterException e)
				{
					e.printStackTrace();
				}

				//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL()));
				//				_activity.startActivity(intent);
			}
		}).start();
	}

	private void buildTwitter()
	{
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(Consts.CONSUMER_KEY);
		configurationBuilder.setOAuthConsumerSecret(Consts.CONSUMER_SECRET);
		Configuration configuration = configurationBuilder.build();
		twitter = new TwitterFactory(configuration).getInstance();
	}

	@Override
	public void onComplete(final String value)
	{
		_listener.onWebViewClosed(true);
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					String verifier = getVerifier(value);
					AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
					User user = twitter.showUser(twitter.getId());
					String userName = user.getName();
					userName = userName != null && !"".equals(userName) ? userName : twitter.getScreenName();

					String avatarRemoteUrl = user.getProfileImageURL().toString();
					
					final UserDetails details = new UserDetails(userName != null && !"".equals(userName) ? userName : twitter.getScreenName(), twitter.getId()
							+ "", null, avatarRemoteUrl, UserDetails.USER_TYPE_TWITTER);
					details.setAccessToken(accessToken.getToken());
					details.setTokenSecret(accessToken.getTokenSecret());

					// Save Avatar
					String localPath = WhipClipApplication._context.getDir("fb", Context.MODE_PRIVATE) + "/avatar.png";
					getFilesManager().downloadFile(avatarRemoteUrl, localPath);
					details.setAvatarLocalPath(localPath);

					Log.i(TAG, "conncted! " + twitter.getScreenName() + " , UN: " + userName + " , ID: " + twitter.getId() + " ,photo: "
							+ user.getProfileImageURL().toString() + " \n " + " AT: " + accessToken.getToken());

					if (_listener != null)
					{
						_activity.runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								_listener.detailsArrived(details);
							}
						});
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}).start();
	}

	@Override
	public void onError(String value)
	{

	}

	@SuppressWarnings("deprecation")
	private String getVerifier(String callbackUrl)
	{
		String verifier = "";

		try
		{
			callbackUrl = callbackUrl.replace("twitterapp", "http");

			URL url = new URL(callbackUrl);
			String query = url.getQuery();

			String array[] = query.split("&");

			for (String parameter : array)
			{
				String v[] = parameter.split("=");

				if (URLDecoder.decode(v[0]).equals("oauth_verifier"))
				{
					verifier = URLDecoder.decode(v[1]);
					break;
				}
			}
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}

		return verifier;
	}
	
	private String twitterShareTextFromText(String text, String userName) {
		
		Resources resources = WhipClipApplication._context.getResources();
	    String signature = resources.getString(R.string.twitter_share_signature); 
	    int maxLength = 116 - signature.length();  //117 (max twitter text length with url) - length of signature
	    
	    if (userName != null && userName.length() > 0)
	    {
	        userName = resources.getString(R.string.share_via) + userName;
	        maxLength -= userName.length();
	    }
	    else
	    {
	        userName = "";
	    }
	    
	    if (text.length() > maxLength)
	    {
	    	text = String.format("%s...", text.substring(0,  maxLength -3)); // //max - 3 (length of "...")
	    }
	    
	    return String.format("%s%s%s", text,userName, signature);
	}
	
	public void postTwit(final String text, final String url, String username)
	{

		final String shareText = twitterShareTextFromText(text, username) + " " + url;
		buildTwitter();
		if (twitter.getAuthorization().isEnabled())
		{
			share(shareText);
		}
		else
		{
			connect(WhipClipApplication.getCurrentActivity(), new IOnDetailsArrived()
			{

				@Override
				public void detailsArrived(UserDetails details)
				{
					share(shareText);
				}

				@Override
				public void onWebViewClosed(boolean isSigning)
				{
					// TODO Auto-generated method stub
					
				}
			});
		}

	}

	private void share(final String twit)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					twitter.updateStatus(twit);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if (WhipClipApplication.getCurrentActivity() == null)
					{
						return;
					}
					WhipClipApplication.getCurrentActivity().runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							Toast.makeText(WhipClipApplication.getCurrentActivity(), "Internal Twitter error. Try again later.", Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}).start();
	}

}
