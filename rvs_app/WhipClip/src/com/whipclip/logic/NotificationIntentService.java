package com.whipclip.logic;

import java.util.List;

import org.jdeferred.DoneCallback;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.whipclip.R;
import com.whipclip.activities.MainActivity;
import com.whipclip.activities.SplashActivity;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSFeedCountDetails;

public class NotificationIntentService extends IntentService
{

	private int			EMPTY_MSG_PARAM	= 1;
	private MyHandler	_myHandler;

	private class MyHandler extends Handler
	{
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public void dispatchMessage(Message msg)
		{
			super.dispatchMessage(msg);
			if (msg.what == EMPTY_MSG_PARAM)
			{
				String userId = AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID);
				if (AppManager.getInstance().hasInternetConnection() && userId.length() > 0)
				{
					Log.i(getClass().getSimpleName(), "inside dispatchMessage ");
					long lastFetchNotifications = AppManager.getInstance().getPrefrences(Consts.PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE, 0);
					
					// PREF_LAST_TIME_NOTIFICATIONS_RETRIEVE MUST BE INITIALIZED WHEN APP IS OPEN
					if (lastFetchNotifications == 0) return; 
					
					RVSPromise promiss = AppManager.getInstance().getSDK()
							.notificationsCountForUser(AppManager.getInstance().getPrefrences(Consts.PREF_SDK_USERID), lastFetchNotifications);
					promiss.then(new DoneCallback<RVSFeedCountDetails>()
					{
						@Override
						public void onDone(final RVSFeedCountDetails result)
						{
							if (result != null)
							{

								int newNotifications = result.getCount();
								Log.i(getClass().getSimpleName(), "new notification count = " + newNotifications);

								String oldNotif = AppManager.getInstance().getPrefrences(Consts.PREF_NOTIFICATION_COUNT);
								int oldNotifications = 0;
								try
								{
									oldNotifications = Integer.valueOf(oldNotif);
								}
								catch (NumberFormatException e)
								{
									e.printStackTrace();
								}

								Log.i(getClass().getSimpleName(), "old notification count = " + oldNotifications);

								boolean isMainActivityInForeground = false;
								ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(ACTIVITY_SERVICE);
								List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
								Log.i(getClass().getName(), "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
								isMainActivityInForeground = (taskInfo.get(0).topActivity.getClassName().equals(MainActivity.class.getName()) || taskInfo
										.get(0).topActivity.getClassName().equals(SplashActivity.class.getName()));

								if (newNotifications > oldNotifications && !isMainActivityInForeground)
								{
									AppManager.getInstance().savePrefrences(Consts.PREF_NOTIFICATION_COUNT, "" + (newNotifications));
									showNotification();
								}

							}
						}
					});

				}
			}

		}
	}

	public NotificationIntentService()
	{
		super("NotificationIntentService");
		_myHandler = new MyHandler();

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void onHandleIntent(Intent intent)
	{
		_myHandler.sendEmptyMessage(EMPTY_MSG_PARAM);

	}

	private void showNotification()
	{
		Log.i(getClass().getSimpleName(), "inside showNotification ");
		Intent notifIntent = new Intent(getApplicationContext(), MainActivity.class);
		notifIntent.setAction(WCNotificationManager.ACTION_USER_NOTIFICATIONS_SYNC);
		
		PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, notifIntent, 0);

		Notification noti = new Notification.Builder(getApplicationContext())
				.setContentTitle(getString(R.string.app_name))
				.setContentText(getString(R.string.you_have_new_notifications))
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pIntent).build();

		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		noti.flags |= Notification.FLAG_AUTO_CANCEL;
		
		int requestID = (int) System.currentTimeMillis();
		notificationManager.notify(requestID, noti);
	}

}
