package com.whipclip.logic;

import java.util.List;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.types.Blog;
import com.tumblr.jumblr.types.User;
import com.whipclip.logic.UserDetails.IOnDetailsArrived;
import com.whipclip.ui.OAuthDialog;
import com.whipclip.ui.OAuthDialog.TwDialogListener;

public class TumblrLogic extends BaseLogic implements TwDialogListener
{
	public static final String			TAG									= "TumblrLogic";

	public static final int				TUMBLR_LOGIN_RESULT_CODE_SUCCESS	= 233;
	public static final int				TUMBLR_LOGIN_RESULT_CODE_FAILURE	= 234;

	public static final String			TUMBLR_EXTRA_TOKEN					= "extra_access_token_tumblr";
	public static final String			TUMBLR_EXTRA_TOKEN_SECRET			= "extra_access_token_secret_tumblr";

	public static final String			TUMBLR_CONSUMER_KEY					= "tumblr_consumer_key";
	public static final String			TUMBLR_CONSUMER_SECRET				= "tumblr_consumer_secret";

	public String						tumblrConsumerKey;
	public String						tumblrConsumerSecret;

	private String						authURL;

	private CommonsHttpOAuthConsumer	consumer;
	private CommonsHttpOAuthProvider	provider							= new CommonsHttpOAuthProvider(Consts.REQUEST_TOKEN_URL, Consts.ACCESS_TOKEN_URL,
																					Consts.AUTH_URL);

	private Activity					_activity;

	private IOnDetailsArrived			_listener;

	private String						_shareData;

	private String						_imageURL;

	public void connect(Activity activity, IOnDetailsArrived listener, String shareData, String imageURL)
	{
		_imageURL = imageURL;
		_shareData = shareData;
		_listener = listener;
		_activity = activity;
		tumblrConsumerKey = Consts.TUMBLR_CONSUMER_KEY;
		tumblrConsumerSecret = Consts.TUMBLR_CONSUMER_SECRET;
		consumer = new CommonsHttpOAuthConsumer(tumblrConsumerKey, tumblrConsumerSecret);
		askOAuth();
	}

	//====== TUMBLR HELPER METHODS ======

	public boolean isConnected()
	{
		return !AppManager.getInstance().getPrefrences(Consts.PREF_KEY_TOKEN, "").equals("");
	}

	public void logOutOfTumblr()
	{
		AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN, "");
		AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN_SECRET, "");
		AppManager.getInstance().savePrefrences(Consts.PREF_KEY_ACCESS_TOKEN_INFOS, "");
		AppManager.getInstance().savePrefrences(Consts.PREF_KEY_USER, "");
	}

	public String getToken()
	{
		return AppManager.getInstance().getPrefrences(Consts.PREF_KEY_TOKEN, "");
	}

	public String getTokenSecret()
	{
		return AppManager.getInstance().getPrefrences(Consts.PREF_KEY_TOKEN_SECRET, "");
	}

	public String getUsername()
	{
		return AppManager.getInstance().getPrefrences(Consts.PREF_KEY_USER_NAME, "");
	}

	public String getUserId()
	{
		return AppManager.getInstance().getPrefrences(Consts.PREF_KEY_USER_ID, "");
	}

	/**
	 * send RequestToken request to get the authURL 
	 */
	private void askOAuth()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					authURL = provider.retrieveRequestToken(consumer, Consts.TUMBLR_CALLBACK_URL);
					Log.i(TAG, "Auth url:" + authURL);
					Log.i(TAG, "accessToken       : " + consumer.getToken());
					Log.i(TAG, "accessTokenSecret : " + consumer.getTokenSecret());
					//save tokens in preferences
					AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN, consumer.getToken());
					AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN_SECRET, consumer.getTokenSecret());
				}
				catch (Exception e)
				{
					e.printStackTrace();
					return;
				}

				_activity.runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d("TumblrLogic", "LOADING AUTH URL : " + authURL);
						try
						{
							OAuthDialog dialog = new OAuthDialog(_activity, authURL, TumblrLogic.this);
							dialog.setCallbackUrl(Consts.TUMBLR_CALLBACK_URL);
							dialog.show();
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				});
			}
		}).start();
	}

	@Override
	public void onComplete(final String value)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					//retrieve token and tokenSecret saved in retrieveRequestToken() call
					String token = AppManager.getInstance().getPrefrences(Consts.PREF_KEY_TOKEN);
					String tokenSecret = AppManager.getInstance().getPrefrences(Consts.PREF_KEY_TOKEN_SECRET);
					Log.i(TAG, "token       : " + token);
					Log.i(TAG, "tokenSecret : " + tokenSecret);

					consumer.setTokenWithSecret(token, tokenSecret);

					Log.d(TAG, "consumer.token       : " + consumer.getToken());
					Log.d(TAG, "consumer.tokenSecret : " + consumer.getTokenSecret());

					Uri uri = Uri.parse(value);
					String oauthToken = uri.getQueryParameter(Consts.IEXTRA_OAUTH_TOKEN);//"oauth_token");
					String oauthVerifier = uri.getQueryParameter(Consts.IEXTRA_OAUTH_VERIFIER);//"oauth_verifier");
					

					Log.i(TAG, "Token:" + oauthToken);
					Log.i(TAG, "Verifier:" + oauthVerifier);

					provider.retrieveAccessToken(consumer, oauthVerifier);
					Log.i(TAG, "accessToken       retrieveAccessToken    : " + consumer.getToken());
					Log.i(TAG, "accessTokenSecret retrieveAccessToken    : " + consumer.getTokenSecret());

					// save the new access token and tokenSecret retrieved by provider
					AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN, consumer.getToken());
					AppManager.getInstance().savePrefrences(Consts.PREF_KEY_TOKEN_SECRET, consumer.getTokenSecret());

					// get user informations 
					JumblrClient client = new JumblrClient(tumblrConsumerKey, tumblrConsumerSecret);
					client.setToken(consumer.getToken(), consumer.getTokenSecret());
					User user = client.user();
					if (user != null)
					{
						Log.d(TAG, "name              : " + user.getName());
						Log.d(TAG, "defaultpostformat : " + user.getDefaultPostFormat());
						Log.d(TAG, "following         : " + user.getFollowingCount());
						Log.d(TAG, "likes             : " + user.getLikeCount());
						Log.d(TAG, "blogs             : " + user.getBlogs().size());
						
						List<Blog> blogs = user.getBlogs();
						for (Blog blog : blogs)
						{
							Log.d(TAG, "blogTitle         : " + blog.getTitle());
							Log.d(TAG, "blogAvatar        : " + blog.avatar());
							Log.d(TAG, "blogName          : " + blog.getName());
							Log.d(TAG, "blogDescription   : " + blog.getDescription());
						}
						AppManager.getInstance().savePrefrences(Consts.PREF_KEY_USER_NAME, user.getName());
						AppManager.getInstance().savePrefrences(Consts.PREF_KEY_USER_ID, user.getName());
					}
					else
					{
						Log.d(TAG, "tumblr user is null");
					}
					_listener.detailsArrived(new UserDetails(_shareData, _imageURL, null, "", 0));

				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}).start();

	}

	@Override
	public void onError(String value)
	{

	}

}
