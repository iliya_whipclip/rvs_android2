package com.whipclip.logic;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

import com.whipclip.WhipClipApplication;

public class FilesManager
{
	private static final String	FILE_ENDING	= ".wcl";

	public synchronized boolean save(String fileName, Object objToSave)
	{
		try
		{
			String fullfileName = getFullFileName(fileName);

			// save to file
			File file = new File(fullfileName);
			if (file.exists())
			{
				file.delete();
			}

			file.getParentFile().mkdirs();
			file.createNewFile();

			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			oos.writeObject(objToSave);
			oos.close();

			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public synchronized Object load(String fileName)
	{
		try
		{
			String fullfileName = getFullFileName(fileName);

			File file = new File(fullfileName);
			if (!file.exists())
			{
				return null;
			}

			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			Object savedObj = ois.readObject();
			ois.close();

			return savedObj;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public boolean delete(String fileName)
	{
		try
		{
			String fullfileName = getFullFileName(fileName);

			File file = new File(fullfileName);
			if (file.exists())
			{
				file.delete();
			}
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Call me from a new Thread!!
	 * @param sourceUrl
	 * @param targetPath
	 * @return
	 */
	public boolean downloadFile(final String sourceUrl, final String targetPath)
	{
		try
		{
			if ((sourceUrl == null) || "".equals(sourceUrl))
			{
				return false;
			}
			URL url = new URL(sourceUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(5000);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Chrome");
			conn.addRequestProperty("Referer", "google.com");

			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);

			boolean redirect = false;

			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK)
			{
				if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)
				{
					redirect = true;
				}
			}

			if (redirect)
			{

				String newUrl = conn.getHeaderField("Location");

				String cookies = conn.getHeaderField("Set-Cookie");

				conn = (HttpURLConnection) new URL(newUrl).openConnection();
				conn.setRequestProperty("Cookie", cookies);
				conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
				conn.addRequestProperty("User-Agent", "Mozilla");
				conn.addRequestProperty("Referer", "google.com");

				Log.i("FilesManager", "redirect: " + newUrl);

			}

			InputStream is = conn.getInputStream();
			BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

			File file = new File(targetPath);

			if (file.exists())
			{
				file.delete();
			}
			file.getParentFile().mkdirs();
			file.createNewFile();

			FileOutputStream outStream = new FileOutputStream(file);
			byte[] buff = new byte[5 * 1024];

			int len;
			while ((len = inStream.read(buff)) != -1)
			{
				outStream.write(buff, 0, len);
			}

			outStream.flush();
			outStream.close();
			inStream.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String getFullFileName(String fileName)
	{
		return WhipClipApplication._context.getCacheDir() + "/" + fileName + FILE_ENDING;
	}

}
