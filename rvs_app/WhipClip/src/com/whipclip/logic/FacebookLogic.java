package com.whipclip.logic;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.whipclip.WhipClipApplication;
import com.whipclip.logic.UserDetails.IOnDetailsArrived;
import com.whipclip.rvs.sdk.RVSError;
import com.whipclip.rvs.sdk.RVSPromise;
import com.whipclip.rvs.sdk.dataObjects.RVSPost;
import com.whipclip.rvs.sdk.dataObjects.RVSSharingUrls;

public class FacebookLogic extends BaseLogic
{

	private static final List<String>	PERMISSIONS						= Arrays.asList("publish_actions", "email", "user_friends");
//	private static final String			PENDING_PUBLISH_KEY				= "pendingPublishReauthorization";
	public static boolean				pendingPublishReauthorization	= false;

	public void getUserDetails(final Session session, final IOnDetailsArrived listener, final Activity activity)
	{
		if (session != null && session.isOpened())
		{
			// If the session is open, make an API call to get user data
			// and define a new callback to handle the response
			Request request = Request.newMeRequest(session, new Request.GraphUserCallback()
			{
				@Override
				public void onCompleted(GraphUser user, Response response)
				{
					// If the response is successful
					if (session == Session.getActiveSession())
					{
						if (user != null)
						{
							String userID = user.getId();
							String userName = user.getName();
							String email = (String) user.getProperty("email");

							final String avatarPath = "http://graph.facebook.com/" + userID + "/picture?type=large";

							final UserDetails details = new UserDetails(userName, userID, email, avatarPath, UserDetails.USER_TYPE_FACEBOOK);
							details.setAccessToken(session.getAccessToken());

							new Thread(new Runnable()
							{
								@Override
								public void run()
								{
									String localPath = WhipClipApplication._context.getDir("fb", Context.MODE_PRIVATE) + "/avatar.png";
									details.setAvatarLocalPath(localPath);
									//									getFilesManager().save(Consts.USER_DETAILS, details);
									activity.runOnUiThread(new Runnable()
									{
										@Override
										public void run()
										{
											listener.detailsArrived(details);
										}
									});
									getFilesManager().downloadFile(avatarPath, localPath);
								}
							}).start();
							Log.i("FacebookLogic", "FB user details - ID:  " + userID + "  Name: " + userName);
						}
					}
				}

			});
			Request.executeBatchAsync(request);
		}
	}

	public static RVSPromise shareOnFacebook(Activity activity, RVSSharingUrls sharingUrl, RVSPost post, boolean allowUI)
	{
		final RVSPromise aPromise = RVSPromise.createPromise();

		Session session = Session.getActiveSession();
		if (session == null)
		{
			// try to restore from cache
			session = Session.openActiveSessionFromCache(activity);
		}

		if (allowUI)
		{
			//	            // Check if the Facebook app is installed and we can present the share dialog
			//	            FBLinkShare *params = [[FBLinkShareParams alloc] init];
			//	            params.link = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [sharingUrl absoluteString]]];
			//	            
			//	            // If the Facebook app is installed and we can present the share dialog
			//	            if ([FBDialogs canPresentShareDialogWithParams:params]) {
			//	                [FBDialogs presentShareDialogWithLink:params.link
			//	                                              handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
			//	                                                  if(error) {
			//	                                                      // An error occurred, we need to handle the error
			//	                                                      // See: https://developers.facebook.com/docs/ios/errors
			//	                                                      [aPromise rejectWithReason:error];
			//	                                                  } else {
			//	                                                      // Success
			//	                                                      [aPromise fulfillWithValue:nil];
			//	                                                  }
			//	                                              }];
			//	            }
			//	            else
			//	            {
			//	                // Put together the dialog parameters
			//	                NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
			//	                                               [NSString stringWithFormat:@"%@", [result absoluteString]], @"link",
			//	                                               nil];
			//	                
			//	                // Show the feed dialog
			//	                [FBWebDialogs presentFeedDialogModallyWithSession:nil
			//	                                                       parameters:params
			//	                                                          handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
			//	                                                              if (error) {
			//	                                                                  // An error occurred, we need to handle the error
			//	                                                                  // See: https://developers.facebook.com/docs/ios/errors
			//	                                                                  [aPromise rejectWithReason:error];
			//	                                                              } else {
			//	                                                                  if (result == FBWebDialogResultDialogNotCompleted) {
			//	                                                                      // User cancelled.
			//	                                                                      [aPromise rejectWithReason:error];
			//	                                                                  } else {
			//	                                                                      [aPromise fulfillWithValue:nil];
			//	                                                                  }
			//	                                                              }
			//	                                                          }];
			//	            }
			//	            
		}
		else
		//UI not allowed
		{
			if (session != null)
			{

				// Check for publish permissions    
				List<String> permissions = session.getPermissions();
				if (!isSubsetOf(PERMISSIONS, permissions))
				{
					pendingPublishReauthorization = true;
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(activity, PERMISSIONS);
					session.requestNewPublishPermissions(newPermissionsRequest);
					return aPromise;
				}

				Bundle postParams = new Bundle();
				//	        	postParams.putString("name", "Facebook SDK for Android");
				//	        	postParams.putString("caption", "Build great social apps and get more installs.");
				//	        	postParams.putString("description", post.getText());
				postParams.putString("link", sharingUrl.getUrl());
				//	        	postParams.putString("picture", sharingUrl.getCoverWithOverlayUrl());
				//	        	postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

				Request.Callback callback = new Request.Callback()
				{

					public void onCompleted(Response response)
					{
						FacebookRequestError error = response.getError();
						if (error != null)
						{

							Log.e("FACEBOOK ERROR", "" + error.getErrorMessage());
							RVSError rvsError = new RVSError(error.getErrorMessage());
							aPromise.reject(rvsError);
						}
						else
						{
							//                            JSONObject graphResponse = response
							//                                    .getGraphObject()
							//                                    .getInnerJSONObject();
							//                            String postId = null;
							//                            try {
							//                                postId = graphResponse
							//                                        .getString("id");
							//                            } catch (JSONException e) {
							//                            }
							aPromise.resolve(response);
						}
					}
				};

				if (session != null && session.isOpened())
				{
					//	        	    publishOnFacebook(intent.getExtras());  

					Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
					RequestAsyncTask task = new RequestAsyncTask(request);
					task.execute();
				}
			}
		}

		return aPromise;
	}

	private static boolean isSubsetOf(Collection<String> subset, Collection<String> superset)
	{
		for (String string : subset)
		{
			if (!superset.contains(string))
			{
				return false;
			}
		}
		return true;
	}
}
