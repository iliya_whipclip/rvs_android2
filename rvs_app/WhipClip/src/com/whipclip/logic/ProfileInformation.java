package com.whipclip.logic;

public class ProfileInformation
{
	private int	_followers;
	private int	_following;
	private int	_likes;

	public ProfileInformation(int followers, int following, int likes)
	{
		_followers = followers;
		_following = following;
		_likes = likes;
	}

	public int getFollowers()
	{
		return _followers;
	}

	public void setFollowers(int followers)
	{
		_followers = followers;
	}

	public int getFollowing()
	{
		return _following;
	}

	public void set_following(int following)
	{
		_following = following;
	}

	public int getLikes()
	{
		return _likes;
	}

	public void setLikes(int likes)
	{
		_likes = likes;
	}

}
