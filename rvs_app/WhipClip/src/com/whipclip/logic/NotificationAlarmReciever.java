package com.whipclip.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationAlarmReciever extends BroadcastReceiver 
{
	public static String NOTIFICATION_TAG = "notification-tag";
	public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
	private Logger logger = LoggerFactory.getLogger(NotificationAlarmReciever.class);
 
	@Override
	public void onReceive(Context context, Intent intent)
	{
		logger.info("receive intent action: " + intent.getAction());
		if (intent.getAction() == WCNotificationManager.ACTION_USER_SYNC) {

			if (AppManager.getInstance().isLoggedIn()) {
				Intent i = new Intent(context, com.whipclip.logic.NotificationIntentService.class);
				context.startService(i);
			}
			
		} else {
			showNotification(context, intent);
		}
	}
	
	private void showNotification(Context context, Intent intent) {
	
    	logger.info("showNotification");
    	
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        if (notification != null && intent != null && intent.getExtras() != null) {
        	logger.info("showNotification intent action: " + intent.getAction());
        	
        	String tag = intent.getStringExtra(NOTIFICATION_TAG);
        	int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        	notificationManager.notify(tag, id, notification);
        }
		
		
	}
}
