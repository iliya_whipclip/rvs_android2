package com.whipclip;

//import org.acra.ACRA;
//import org.acra.ReportField;
//import org.acra.ReportingInteractionMode;
//import org.acra.annotation.ReportsCrashes;
//import org.acra.sender.HttpSender;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.whipclip.activities.BaseActivity;
import com.whipclip.activities.MainActivity;
import com.whipclip.logic.AppManager;
import com.whipclip.logic.NotificationIntentService;
import com.whipclip.rvs.sdk.util.ConfigureLog4J;

//@ReportsCrashes(
////https://acs.cloudant.com/acralyzer/_design/acralyzer/index.html
//formUri = "https://acs.cloudant.com/acra-whipclip/_design/acra-storage/_update/report", reportType = HttpSender.Type.JSON, httpMethod = HttpSender.Method.POST, formUriBasicAuthLogin = "tionevemessellediatiedin", formUriBasicAuthPassword = "dFN55RdlNfAAVcWrq0MaC8wm", formKey = "", // This is required for backward compatibility but not used
//customReportContent = { ReportField.REPORT_ID, ReportField.APP_VERSION_CODE, ReportField.ANDROID_VERSION, ReportField.PACKAGE_NAME, ReportField.BUILD,
//		ReportField.STACK_TRACE, ReportField.LOGCAT }, mode = ReportingInteractionMode.TOAST, resToastText = R.string.toast_crash)
public class WhipClipApplication extends Application
{
	private final static String	TAG_NAME			= "InnovidApplication";
	public static Context		_context;
	public static int			_screenWidth;
	public static int			_screenHeight;
	public static int			_screenDensity;
	private static BaseActivity	_currentActivity;
	public static int			_debugCounter		= 0;
	public static int			_fragmentCounter	= 0;
	public static String		_appName			= "Whipclip";

	@Override
	public void onCreate()
	{
		super.onCreate();


		WhipClipApplication._context = getApplicationContext();
		WindowManager manager = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		manager.getDefaultDisplay().getMetrics(metrics);
		_screenDensity = metrics.densityDpi;

		_screenWidth = metrics.widthPixels;
		_screenHeight = metrics.heightPixels;

		ConfigureLog4J.configure();

		AppManager.getInstance().init(this);
	}

	public static void setCurrentActivity(BaseActivity activity)
	{
		_currentActivity = activity;
	}

	public static BaseActivity getCurrentActivity()
	{
		return _currentActivity;
	}

	public static String getVersionNumber()
	{
		String retVal = null;
		try
		{
			PackageInfo packageInfo = _context.getPackageManager().getPackageInfo(_context.getPackageName(), 0);
			retVal = packageInfo.versionName;
		}
		catch (Exception e)
		{
			Log.d(TAG_NAME, e.getMessage());
		}
		return retVal;
	}
}
